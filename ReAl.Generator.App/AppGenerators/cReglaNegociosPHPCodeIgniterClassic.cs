﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using System;
using System.Data;
using System.IO;

namespace ReAl.Generator.App.AppGenerators
{
    public class cReglaNegociosPHPCodeIgniterClassic
    {
        public string CrearControllerPhpCodeIgniterClassic(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }
            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\")) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\"));
            }

            string pNameTablaAux = pNameClase;
            string pNameTabla = pNameClase.Replace("_", "");
            dynamic pNameClaseRn = formulario.txtPrefijoModelo.Text + pNameTabla;
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameTabla;

            switch (Principal.DBMS)
            {
                case TipoBD.SQLServer:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;

                case TipoBD.SQLServer2000:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;

                case TipoBD.Oracle:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;

                case TipoBD.SQLite:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    pNameTabla = pNameTabla.ToLower();
                    break;

                case TipoBD.PostgreSQL:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    pNameTabla = pNameTabla.ToLower();
                    pNameClaseRn = pNameClaseRn.ToLower();
                    break;

                case TipoBD.FireBird:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;
            }

            string strLlaves = "";
            string strLlavesWhere = "";
            bool bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString().ToLower();

                    strLlavesWhere = strLlavesWhere + Environment.NewLine + "\t\t$this->db->where('" + nameCol + "', $id);";

                    if (bEsPrimerElemento)
                    {
                        strLlaves = strLlaves + "$" + tipoCol + "_" + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlaves = strLlaves + ", $" + tipoCol + "_" + nameCol;
                    }
                }
            }

            string strApiTrans = "";
            string strApiEstado = "";
            string strUsuCre = "";
            string strFecCre = "";
            string strUsuMod = "";
            string strFecMod = "";
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strUsuCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strUsuMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strFecCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strFecMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                    strApiEstado = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                    strApiTrans = nameCol;
            }

            if (string.IsNullOrEmpty(strApiTrans))
                strApiTrans = "----";
            if (string.IsNullOrEmpty(strApiEstado))
                strApiEstado = "----";
            if (string.IsNullOrEmpty(strUsuCre))
                strUsuCre = "----";
            if (string.IsNullOrEmpty(strFecCre))
                strFecCre = "----";
            if (string.IsNullOrEmpty(strUsuMod))
                strUsuMod = "----";
            if (string.IsNullOrEmpty(strFecMod))
                strFecMod = "----";

            string FC = DateTime.Now.ToString("dd/MM/yyyy");
            string Creador = (string.IsNullOrEmpty(formulario.txtInfAutor.Text) ? "Automatico     " : formulario.txtInfAutor.Text);
            Creador = Creador.PadRight(15);
            string CabeceraTmp = "/***********************************************************************************************************\r\n\tNOMBRE:       %PAR_NOMBRE%\r\n\tDESCRIPCION:\r\n\t\t%PAR_DESCRIPCION%\r\n\r\n\tREVISIONES:\r\n\t\tVer        FECHA       Autor            Descripcion \r\n\t\t---------  ----------  ---------------  ------------------------------------\r\n\t\t1.0        " + FC + "  " + Creador + "  Creacion \r\n\r\n*************************************************************************************************************/\r\n";

            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase).Replace("#region", "").Replace("#endregion", "");
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que implementa los metodos y operaciones sobre la Tabla " + pNameTabla);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            newTexto.AppendLine("<?php");
            newTexto.AppendLine("");
            newTexto.AppendLine(CabeceraTmp);

            newTexto.AppendLine("");
            newTexto.AppendLine("class " + pNameClaseRn + " extends CI_Controller");
            newTexto.AppendLine("{");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tfunction __construct()");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t//load database in autoload libraries");
            newTexto.AppendLine("\t\tparent::__construct();");
            newTexto.AppendLine("\t\t$this->load->modelo('" + pNameClaseEnt + "');");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic function index()");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$rn = new " + pNameClaseEnt + ";");
            newTexto.AppendLine("\t\t$data['data']=$rn->listar_" + pNameTabla.ToLower() + "();");
            newTexto.AppendLine("\t\t$this->load->view('includes/header');");
            newTexto.AppendLine("\t\t$this->load->view('" + pNameTabla.ToLower().Replace("_", "") + "/list',$data);");
            newTexto.AppendLine("\t\t$this->load->view('includes/footer');");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic function create()");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$this->load->view('includes/header');");
            newTexto.AppendLine("\t\t$this->load->view('" + pNameTabla.ToLower().Replace("_", "") + "/create');");
            newTexto.AppendLine("\t\t$this->load->view('includes/footer');");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic function insert()");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$rn = new " + pNameClaseEnt + ";");
            newTexto.AppendLine("\t\t$rn->insert_" + pNameTabla.ToLower() + "();");
            newTexto.AppendLine("\t\tredirect(base_url('" + pNameTabla.ToLower().Replace("_", "") + "'));");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic function edit($id)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$rn = new " + pNameClaseEnt + ";");
            newTexto.AppendLine("\t\t$data['data']=$rn->obtener_" + pNameTabla.ToLower() + "($id);");
            newTexto.AppendLine("\t\t$this->load->view('includes/header');");
            newTexto.AppendLine("\t\t$this->load->view('" + pNameTabla.ToLower().Replace("_", "") + "/edit', $data);");
            newTexto.AppendLine("\t\t$this->load->view('includes/footer');");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic function update($id)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$rn = new " + pNameClaseEnt + ";");
            newTexto.AppendLine("\t\t$rn->update_" + pNameTabla.ToLower() + "($id);");
            newTexto.AppendLine("\t\tredirect(base_url('" + pNameTabla.ToLower().Replace("_", "") + "'));");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic function delete($id)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$rn = new " + pNameClaseEnt + ";");
            newTexto.AppendLine("\t\t$rn->delete_" + pNameTabla.ToLower() + "($id);");
            newTexto.AppendLine("\t\tredirect(base_url('" + pNameTabla.ToLower().Replace("_", "") + "'));");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\t");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), pNameClaseRn + ".php", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\" + pNameClaseRn + ".php";
        }
    }
}