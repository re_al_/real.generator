﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using System;
using System.Collections;
using System.Data;
using System.IO;

namespace ReAl.Generator.App.AppGenerators
{
    public class cReglaNegociosCSharp_WebApiClient
    {
        #region Methods

        public string CrearModelo(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            TipoEntorno miEntorno;
            Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);

            string paramClaseConeccion = (string.IsNullOrEmpty(formulario.txtClaseConn.Text) ? "cConn" : formulario.txtClaseConn.Text);
            string paramClaseTransaccion = (string.IsNullOrEmpty(formulario.txtClaseTransaccion.Text) ? "cTransaccion" : formulario.txtClaseTransaccion.Text);
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string namespaceClases = (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class");

            string nameCol = null;
            string tipoCol = null;
            bool permiteNull = false;
            string pNameTabla = pNameClase;

            pNameClase = pNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                         pNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                         pNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                         pNameTabla.Replace("_", "").Substring(4).ToLower();
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameClase;
            string pNameClaseIn = formulario.txtPrefijoInterface.Text + pNameClase;
            string pNameClaseRn = formulario.txtPrefijoModelo.Text + pNameClase;

            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoDal.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoDal.Text;
            }

            string TipoColNull = null;
            string DescripcionCol = null;
            string strAlias = null;
            try
            {
                DataTable dtPrefijo = new DataTable();

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.SQLServer2000:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.Oracle:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT ALIASSTA FROM SEGTABLAS WHERE UPPER(TABLASTA) = '" + pNameTabla.ToUpper() + "'");
                        break;

                    case TipoBD.PostgreSQL:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.aliassta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" + pNameTabla + "')");
                        if (dtPrefijo.Rows.Count == 0)
                        {
                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.alias FROM seg_tablas s WHERE UPPER(s.nombretabla) = UPPER('" + pNameTabla + "')");
                        }
                        break;
                }

                if (dtPrefijo.Rows.Count > 0)
                {
                    strAlias = dtPrefijo.Rows[0][0].ToString().ToLower();
                    strAlias = strAlias.Substring(0, 1).ToUpper() + strAlias.Substring(1, strAlias.Length - 1);
                }
                else
                {
                    strAlias = pNameTabla;
                }
            }
            catch (Exception ex)
            {
                strAlias = pNameTabla;
            }

            if (dtColumns.Rows.Count == 0)
                return "";
            if (dtColumns.Columns.Count == 0)
                return "";

            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\")) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\"));
            }

            //Obtenemos las columnas de la llave primaria
            bool bAdicionar = true;
            ArrayList llavePrimaria = new ArrayList();
            string strLlavePrimaria = "";
            string strLlavePrimariaNombre = "";
            string strLlavePrimariaVal = "";
            string strLlavePrimariaObj = "";
            dynamic bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();

                    if (bEsPrimerElemento)
                    {
                        strLlavePrimaria = strLlavePrimaria + tipoCol + " " + tipoCol + nameCol;
                        strLlavePrimariaNombre = strLlavePrimariaNombre +  tipoCol + nameCol;
                        strLlavePrimariaObj = strLlavePrimariaObj + "obj." + nameCol;
                        strLlavePrimariaVal = strLlavePrimariaVal + tipoCol + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlavePrimaria = strLlavePrimaria + ", " + tipoCol + " " + tipoCol + nameCol;
                        strLlavePrimariaNombre = strLlavePrimariaNombre + ", " + tipoCol + nameCol;
                        strLlavePrimariaObj = strLlavePrimariaObj + "+ \"/\" + obj." + nameCol;
                        strLlavePrimariaVal = strLlavePrimariaVal + "+ \"/\" + " + tipoCol + nameCol;
                    }
                    llavePrimaria.Add(nameCol);
                }
            }
            if (string.IsNullOrEmpty(strLlavePrimaria))
            {
                strLlavePrimaria = dtColumns.Rows[0][1].ToString() + " " + dtColumns.Rows[0][1].ToString() +
                                   dtColumns.Rows[0][0].ToString();
                llavePrimaria.Add(dtColumns.Rows[0][1].ToString() + dtColumns.Rows[0][0].ToString());
            }

            //Obtenemos las columnas de los APIs
            string strApiTrans = "";
            string strApiEstado = "";
            string strUsuCre = "";
            string strFecCre = "";
            string strUsuMod = "";
            string strFecMod = "";
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strUsuCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strUsuMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strFecCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strFecMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                    strApiEstado = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                    strApiTrans = nameCol;
            }

            //Empezamos con la creacion del archivo
            string strPermiteNull = null;
            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClaseRn);
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que implementa los metodos y operaciones sobre la Tabla " + pNameTabla);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();

            newTexto.AppendLine(CabeceraTmp);

            newTexto.AppendLine("");
            newTexto.AppendLine("#region");
            newTexto.AppendLine("using System;");
            newTexto.AppendLine("using System.Collections;");
            newTexto.AppendLine("using System.Collections.Generic;");            
            newTexto.AppendLine("using System.Data;");
            newTexto.AppendLine("using System.Linq;");
            newTexto.AppendLine("using System.Net;");
            newTexto.AppendLine("using Newtonsoft.Json;");
            newTexto.AppendLine("using RestSharp;");            
            newTexto.AppendLine("using JsonSerializer = System.Text.Json.JsonSerializer;");            
            newTexto.AppendLine("using " + formulario.txtInfProyectoDal.Text + "; ");
            if (formulario.chkReAlEntidades.Checked)
                newTexto.AppendLine("using " + formulario.txtInfProyectoDal.Text + "." + formulario.txtNamespaceEntidades.Text + ";");
            if (formulario.chkReAlInterface.Checked)
                newTexto.AppendLine("using " + formulario.txtInfProyectoDal.Text + "." + formulario.txtNamespaceInterface.Text + ";");
            newTexto.AppendLine("#endregion");
            newTexto.AppendLine("");

            newTexto.AppendLine("namespace " + formulario.txtInfProyectoDal.Text + "." + formulario.txtNamespaceNegocios.Text);
            newTexto.AppendLine("{");
            newTexto.AppendLine("\tpublic class " + pNameClaseRn);
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tprivate string strEndPoint = \"/api/"+ pNameTabla + "/\";");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic void Dispose()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tGC.SuppressFinalize(this);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// Funcion que obtiene los datos de un Objeto a partir de la llave primaria");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"intid\">Llave Primaria</param>");
            newTexto.AppendLine("\t\t/// <returns>Objeto que coincide con la llave primaria buscada</returns>");
            newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(" + strLlavePrimaria + ")");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar client = new RestClient(CParametrosApi.BaseUrl + strEndPoint + "+ strLlavePrimariaNombre + ")");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tTimeout = -1");
            newTexto.AppendLine("\t\t\t};");
            newTexto.AppendLine("\t\t\tvar request = new RestRequest(Method.GET);");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"accept\", \"application/json\");");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"Authorization\", \"bearer \" + CParametrosApi.ApiToken);");
            newTexto.AppendLine("\t\t\tvar response = client.Execute(request);");
            newTexto.AppendLine("\t\t\tswitch (response.StatusCode)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tcase HttpStatusCode.OK:");
            newTexto.AppendLine("\t\t\t\t\tvar obj = JsonSerializer.Deserialize<" + pNameClaseEnt + ">(response.Content);");
            newTexto.AppendLine("\t\t\t\t\treturn obj;");
            newTexto.AppendLine("\t\t\t\tcase HttpStatusCode.Unauthorized:");
            newTexto.AppendLine("\t\t\t\t\tthrow new MethodAccessException();");
            newTexto.AppendLine("\t\t\t\tdefault:");
            newTexto.AppendLine("\t\t\t\t\tthrow new ApplicationException(response.StatusCode + \": \" + response.Content);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\treturn ObtenerObjeto(arrColumnasWhere, arrValoresWhere, \"\");");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar list = ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t\treturn list.Count switch");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\t0 => throw new ApplicationException(\"La consulta no ha devuelto resultados.\"),");
            newTexto.AppendLine("\t\t\t\t> 1 => throw new ApplicationException(\"Se ha devuelto mas de una fila.\"),");
            newTexto.AppendLine("\t\t\t\t_ => list[0]");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(" + pNameClaseEnt + ".Fields searchField, object searchValue)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { searchField };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { searchValue };");
            newTexto.AppendLine("\t\t\treturn ObtenerObjeto(arrColumnasWhere, arrValoresWhere, \"\");");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { searchField };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { searchValue };");
            newTexto.AppendLine("\t\t\treturn ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic List<" + pNameClaseEnt + "> ObtenerLista(" + pNameClaseEnt + ".Fields searchField, object searchValue)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { searchField };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { searchValue };");
            newTexto.AppendLine("\t\t\treturn ObtenerLista(arrColumnasWhere, arrValoresWhere, \"\");");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic List<" + pNameClaseEnt + "> ObtenerLista(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { searchField };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { searchValue };");
            newTexto.AppendLine("\t\t\treturn ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// Funcion que obtiene un conjunto de Objetos");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <returns>Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros </returns>");
            newTexto.AppendLine("\t\tpublic List<" + pNameClaseEnt + "> ObtenerLista()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { 1 };");            
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { 1 };");
            newTexto.AppendLine("\t\t\treturn ObtenerLista(arrColumnasWhere, arrValoresWhere, \"\");");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic List<" + pNameClaseEnt + "> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");            
            newTexto.AppendLine("\t\t\treturn ObtenerLista(arrColumnasWhere, arrValoresWhere, \"\");");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic List<" + pNameClaseEnt + "> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar client = new RestClient(CParametrosApi.BaseUrl + strEndPoint + \"ObtenerLista\")");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tTimeout = -1");
            newTexto.AppendLine("\t\t\t};");
            newTexto.AppendLine("\t\t\tvar request = new RestRequest(Method.POST);");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"accept\", \"application/json\");");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"Authorization\", \"bearer \" + CParametrosApi.ApiToken);");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"Content-Type\", \"application/json-patch+json\");");
            newTexto.AppendLine("\t\t\tvar strColWhere = ApiHelper.SerializeArray(arrColumnasWhere);");
            newTexto.AppendLine("\t\t\tvar strValWhere = ApiHelper.SerializeArray(arrValoresWhere);");
            newTexto.AppendLine("\t\t\tvar body = \"{\\\"arr_col_where\\\":[\"+ strColWhere +\"],\\\"arr_val_where\\\":[\" + strValWhere + \"],\\\"str_param\\\":\\\"\" + strParamAdicionales + \"\\\"}\"; ");
            newTexto.AppendLine("\t\t\trequest.AddParameter(\"application/json-patch+json\", body,  ParameterType.RequestBody);");
            newTexto.AppendLine("\t\t\tvar response = client.Execute(request);");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tswitch (response.StatusCode)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tcase HttpStatusCode.OK:");
            newTexto.AppendLine("\t\t\t\t\tvar list = JsonSerializer.Deserialize<List<" + pNameClaseEnt + ">>(response.Content);");
            newTexto.AppendLine("\t\t\t\t\treturn list;");
            newTexto.AppendLine("\t\t\t\tcase HttpStatusCode.Unauthorized:");
            newTexto.AppendLine("\t\t\t\t\tthrow new MethodAccessException();");
            newTexto.AppendLine("\t\t\t\tdefault:");
            newTexto.AppendLine("\t\t\t\t\tthrow new ApplicationException(response.StatusCode + \": \" + response.Content);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(string condicionesWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { 1 };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { 1 };");
            newTexto.AppendLine("\t\t\treturn ObtenerDataTable(arrColumnasWhere, arrValoresWhere, condicionesWhere);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(" + pNameClaseEnt + ".Fields searchField, object searchValue)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { searchField };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { searchValue };");
            newTexto.AppendLine("\t\t\treturn ObtenerDataTable(arrColumnasWhere, arrValoresWhere, \"\");");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { searchField };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { searchValue };");
            newTexto.AppendLine("\t\t\treturn ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\treturn ObtenerDataTable(arrColumnasWhere, arrValoresWhere, \"\");");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar list = ObtenerLista(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);");
            newTexto.AppendLine("\t\t\treturn list.ToDataTable();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");



            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// Funcion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de un Objeto");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"obj\">Clase desde la que se van a insertar los valores a la tabla</param>");            
            newTexto.AppendLine("\t\t/// <returns>Valor TRUE or FALSE que indica el exito de la operacion</returns>");
            newTexto.AppendLine("\t\tpublic bool Insert(" + pNameClaseEnt + " obj)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar client = new RestClient(CParametrosApi.BaseUrl + strEndPoint)");            
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tTimeout = -1");
            newTexto.AppendLine("\t\t\t};");
            newTexto.AppendLine("\t\t\tvar request = new RestRequest(Method.POST);");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"accept\", \"application/json\");");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"Content-Type\", \"application/json-patch+json\");");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"Authorization\", \"bearer \" + CParametrosApi.ApiToken);");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tobj." + strApiEstado + " = CApi.Estado.ELABORADO.ToString();");
            newTexto.AppendLine("\t\t\tobj." + strApiTrans + " = CApi.Transaccion.CREAR.ToString();");
            newTexto.AppendLine("\t\t\tobj." + strFecCre + " = DateTime.Now;");
            newTexto.AppendLine("\t\t\tvar body = JsonConvert.SerializeObject(obj);");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\trequest.AddParameter(\"application/json-patch+json\", body,  ParameterType.RequestBody);");
            newTexto.AppendLine("\t\t\tvar response = client.Execute(request);");
            newTexto.AppendLine("\t\t\treturn response.StatusCode == HttpStatusCode.Created;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// Funcion que actualiza un registro en la tabla a partir de un Objeto");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"obj\">Clase desde la que se van a actualizar los valores a la tabla</param>");            
            newTexto.AppendLine("\t\t/// <returns>Valor TRUE or FALSE que indica el exito de la operacion</returns>");
            newTexto.AppendLine("\t\tpublic bool Update(" + pNameClaseEnt + " obj)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar client = new RestClient(CParametrosApi.BaseUrl + strEndPoint + obj." + llavePrimaria[0] + ")");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tTimeout = -1");
            newTexto.AppendLine("\t\t\t};");
            newTexto.AppendLine("\t\t\tvar request = new RestRequest(Method.PUT);");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"accept\", \"application/json\");");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"Authorization\", \"bearer \" + CParametrosApi.ApiToken);");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"Content-Type\", \"application/json-patch+json\");");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tobj."+ strFecMod +" = DateTime.Now;");
            newTexto.AppendLine("\t\t\tvar body = JsonConvert.SerializeObject(obj);");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\trequest.AddParameter(\"application/json-patch+json\", body,  ParameterType.RequestBody);");
            newTexto.AppendLine("\t\t\tvar response = client.Execute(request);");
            newTexto.AppendLine("\t\t\treturn response.StatusCode == HttpStatusCode.Accepted;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// Funcion que elimina un registro en la tabla a partir de un Objeto");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"obj\">Clase desde la que se van a eliminar los valores a la tabla</param>");            
            newTexto.AppendLine("\t\t/// <returns>Valor TRUE or FALSE que indica el exito de la operacion</returns>");
            newTexto.AppendLine("\t\tpublic bool Delete(" + pNameClaseEnt + " obj)");
            newTexto.AppendLine("\t\t{");            
            newTexto.AppendLine("\t\t\tvar client = new RestClient(CParametrosApi.BaseUrl + strEndPoint + obj." + llavePrimaria[0] + ")");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tTimeout = -1");
            newTexto.AppendLine("\t\t\t};");
            newTexto.AppendLine("\t\t\tvar request = new RestRequest(Method.DELETE);");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"accept\", \"*/*\");");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"Authorization\", \"bearer \" + CParametrosApi.ApiToken);");
            newTexto.AppendLine("\t\t\tvar response = client.Execute(request);");
            newTexto.AppendLine("\t\t\treturn response.StatusCode == HttpStatusCode.Accepted;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic bool Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar client = new RestClient(CParametrosApi.BaseUrl + strEndPoint)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tTimeout = -1");
            newTexto.AppendLine("\t\t\t};");
            newTexto.AppendLine("\t\t\tvar request = new RestRequest(Method.DELETE);");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"accept\", \"*/*\");");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"Authorization\", \"bearer \" + CParametrosApi.ApiToken);");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"Content-Type\", \"application/json-patch+json\");");
            newTexto.AppendLine("\t\t\tvar strColWhere = ApiHelper.SerializeArray(arrColumnasWhere);");
            newTexto.AppendLine("\t\t\tvar strValWhere = ApiHelper.SerializeArray(arrValoresWhere);");
            newTexto.AppendLine("\t\t\tvar body = \"{\\\"arr_col_where\\\":[\" + strColWhere +\"],\\\"arr_val_where\\\":[\" + strValWhere +\"]}\"; ");
            newTexto.AppendLine("\t\t\trequest.AddParameter(\"application/json-patch+json\", body,  ParameterType.RequestBody);");
            newTexto.AppendLine("\t\t\tvar response = client.Execute(request);");
            newTexto.AppendLine("\t\t\treturn response.StatusCode == HttpStatusCode.Accepted;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tpublic int FuncionesCount(" + pNameClaseEnt + ".Fields refField)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { 1 };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { 1 };");
            newTexto.AppendLine("\t\t\treturn FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tpublic int FuncionesCount(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { whereField.ToString() };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { valueField.ToString() };");
            newTexto.AppendLine("\t\t\treturn FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tpublic int FuncionesCount(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar dtTemp = ObtenerDataTable(arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t\treturn dtTemp.Rows.Count;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tpublic double FuncionesMin(" + pNameClaseEnt + ".Fields refField)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { 1 };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { 1 };");
            newTexto.AppendLine("\t\t\treturn FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tpublic double FuncionesMin(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { whereField.ToString() };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { valueField.ToString() };");
            newTexto.AppendLine("\t\t\treturn FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tpublic double FuncionesMin(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar dtTemp = ObtenerDataTable(arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t\treturn (from DataRow dr in dtTemp.Rows select dr.Field<int>(refField.ToString())).Prepend(int.MaxValue).Min();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tpublic double FuncionesMax(" + pNameClaseEnt + ".Fields refField)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { 1 };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { 1 };");
            newTexto.AppendLine("\t\t\treturn FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tpublic double FuncionesMax(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { whereField.ToString() };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { valueField.ToString() };");
            newTexto.AppendLine("\t\t\treturn FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tpublic double FuncionesMax(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar dtTemp = ObtenerDataTable(arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t\treturn (from DataRow dr in dtTemp.Rows select dr.Field<int>(refField.ToString())).Prepend(int.MinValue).Max();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tpublic double FuncionesSum(" + pNameClaseEnt + ".Fields refField)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { 1 };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { 1 };");
            newTexto.AppendLine("\t\t\treturn FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tpublic double FuncionesSum(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { whereField.ToString() };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { valueField.ToString() };");
            newTexto.AppendLine("\t\t\treturn FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tpublic double FuncionesSum(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar dtTemp = ObtenerDataTable( arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t\tvar sumObject = dtTemp.Compute(\"Sum([\" + refField + \"])\", string.Empty);");
            newTexto.AppendLine("\t\t\treturn double.Parse(sumObject.ToString() ?? \"0\");");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tpublic double FuncionesAvg(" + pNameClaseEnt + ".Fields refField)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { 1 };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { 1 };");
            newTexto.AppendLine("\t\t\treturn FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tpublic double FuncionesAvg(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar arrColumnasWhere = new ArrayList { whereField.ToString() };");
            newTexto.AppendLine("\t\t\tvar arrValoresWhere = new ArrayList { valueField.ToString() };");
            newTexto.AppendLine("\t\t\treturn FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tpublic double FuncionesAvg(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar dtTemp = ObtenerDataTable(arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t\tvar avgObject = dtTemp.Compute(\"AVG([\" + refField + \"])\", string.Empty);");
            newTexto.AppendLine("\t\t\treturn double.Parse(avgObject.ToString() ?? \"0\");");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), pNameClaseRn + ".cs", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\" + pNameClaseRn +
                   ".cs" + (formulario.chkClassParcial.Checked
                       ? "\r\n" + PathDesktop + "\\" +
                         formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\" + pNameClaseRn +
                         "Strings.cs"
                       : "");
        }

        #endregion Methods
    }
}