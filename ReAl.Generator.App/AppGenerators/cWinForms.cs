﻿using System;
using System.Data;
using System.IO;
using System.Text;
using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using ReAl.Generator.App.Properties;

namespace ReAl.Generator.App.AppGenerators
{
    public class cWinForms
    {
        public string Cabecera;

        public string CabeceraPlantillaCodigo;

        public string PathDesktop;

        public string infoNameProyApp;

        public string infoNameProyClass;

        public string infoNameProyConn;

        public string infoPathProyecto;

        public string infoNameAutor;

        public string paramClaseConeccion;

        public string paramClaseTransaccion;

        public string paramClaseParametros;

        public int idTabla;

        public string tipoCol;

        public string nameCol;

        public bool permiteNull;

        public bool esPK;

        public bool esForeign;

        public bool esAutoGenerada;

        public string formatoFecha;

        public string formatoFechaHora;

        public string prefijoEntidades;

        public string prefijoInterface;

        public string prefijoModelo;

        public string prefijoForms;

        public string prefijoControls;

        public string prefijoWebList;

        public string prefijoWebABM;

        public string prefijoWebMix;

        public string namespaceEntidades;

        public string namespaceInterface;

        public string namespaceModelo;

        public string namespaceVista;

        public string namespaceForms;

        public string namespaceControls;

        public string namespaceClases;

        public string namespaceWebVista;

        public string namespaceWebList;

        public string namespaceWebAbm;

        public string namespaceWebMix;

        public bool bMoverResultados;

        public cWinForms(FPrincipal formulario)
        {
            bMoverResultados = formulario.chkMoverResultados.Checked;
            infoPathProyecto = formulario.txtPathProyecto.Text;
            prefijoEntidades = formulario.txtPrefijoEntidades.Text;
            prefijoInterface = formulario.txtPrefijoInterface.Text;
            prefijoModelo = formulario.txtPrefijoModelo.Text;
            prefijoForms = "F"; //formulario.txtPrefixForm.Text;
            prefijoControls = "C"; //formulario.txtPrefijoCtr.Text;
            namespaceEntidades = formulario.txtNamespaceEntidades.Text;
            namespaceInterface = formulario.txtNamespaceInterface.Text;
            namespaceModelo = formulario.txtNamespaceNegocios.Text;
            namespaceVista = formulario.txtInfProyecto.Text;
            namespaceForms = formulario.txtInfProyecto.Text;
            namespaceControls = formulario.txtInfProyecto.Text;
            namespaceClases = "Clases";
            infoNameProyClass = formulario.txtInfProyectoClass.Text;
            infoNameProyApp = formulario.txtInfProyecto.Text;
            infoNameProyConn = formulario.txtInfProyectoConn.Text;
            paramClaseConeccion = formulario.txtClaseConn.Text == "" ? "cConn" : formulario.txtClaseConn.Text;
            paramClaseTransaccion = formulario.txtClaseTransaccion.Text == "" ? "cTransaccion" : formulario.txtClaseTransaccion.Text;
            paramClaseParametros = formulario.txtClaseParametros.Text == "" ? "cParametros" : formulario.txtClaseParametros.Text;
            formatoFechaHora = paramClaseParametros + ".parFormatoFechaHora";
            formatoFecha = paramClaseParametros + ".parFormatoFecha";
            //Cabecera = formulario.strCabeceraPlantillaCodigo;
            CabeceraPlantillaCodigo = formulario.strCabeceraPlantillaCodigo;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + infoNameProyClass;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + infoNameProyClass;
            }

            if (Directory.Exists(PathDesktop))
            {
                Directory.CreateDirectory(PathDesktop);
            }
        }

        public string CrearFormulario(DataTable dtCols, string pNameClase, string pNameTabla)
        {
            if (Directory.Exists(PathDesktop + "\\" + namespaceForms) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + namespaceForms);
            }

            var texto = "";
            var CabeceraTmp = CabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", prefijoWebList + pNameClase);
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que define los metodos y propiedades del Formulario " + prefijoForms + pNameClase);
            texto = CabeceraTmp + texto;
            texto = texto + "using " + infoNameProyClass + "." + namespaceClases + ";\r\n";
            texto = texto + "using " + infoNameProyClass + "." + namespaceEntidades + ";\r\n";
            texto = texto + "using " + infoNameProyClass + "." + namespaceModelo + ";\r\n";
            texto = texto + Resources.WIN_CS_FormCs;
            texto = texto.Replace("%NAMESPACE%", infoNameProyClass + "." + namespaceForms);
            texto = texto.Replace("%NAME_FORM%", prefijoForms + pNameClase);
            texto = texto.Replace("%MODELO_OBJETO%", prefijoModelo + pNameClase);
            texto = texto.Replace("%OBJETO%", prefijoEntidades + pNameClase);
            // Armamos las columnas
            var textoColsIns = "";
            var textoColsUpd = "";
            var textoColsMostrar = "";
            var textoArrayPK = "";
            var textoArrayValoresPK = "";
            var j = 0;
            for (var iCol = 0; iCol <= dtCols.Rows.Count - 1; iCol++)
            {
                nameCol = dtCols.Rows[iCol][0].ToString();
                tipoCol = dtCols.Rows[iCol][1].ToString();
                permiteNull = dtCols.Rows[iCol]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES";
                esAutoGenerada = dtCols.Rows[iCol]["EsIdentidad"].ToString().ToUpper() == "YES";
                if (esPK)
                {
                    textoArrayPK = textoArrayPK + "arrColumnasWhere.Add(" + '\"' + nameCol + '\"' + ");\r\n" + "\t\t\t\t" + '\t';
                    textoArrayValoresPK = textoArrayValoresPK + "arrValoresWhere.Add(_arrPrimaryKeys[" + j + "]);\r\n" + "\t\t\t\t" + '\t';
                    j = j + 1;
                }

                if (!esAutoGenerada)
                {
                    if (dtCols.Rows[iCol]["EsFK"].ToString() == "Yes")
                    {
                        if (tipoCol.ToUpper() == "STRING")
                        {
                            textoColsIns = textoColsIns + "obj." + nameCol + " = cmb" + nameCol + ".SelectedValue.ToString();" + ("\r\n" + ("\t\t\t\t" + '\t'));
                            textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = cmb" + nameCol + ".SelectedValue.ToString();" + ("\r\n" + ("\t\t\t\t" + '\t'));
                        }
                        else
                        {
                            textoColsIns = textoColsIns + "obj." + nameCol + " = " + tipoCol + ".Parse(cmb" + nameCol + ".SelectedValue.ToString());" + ("\r\n" + ("\t\t\t\t" + '\t'));
                            textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = " + tipoCol + ".Parse(cmb" + nameCol + ".SelectedValue.ToString());" + ("\r\n" + ("\t\t\t\t" + '\t'));
                        }
                    }
                    else
                    {
                        switch (tipoCol.ToUpper())
                        {
                            case "STRING":
                                textoColsIns = textoColsIns + "obj." + nameCol + " = txt" + nameCol + ".Text;" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = txt" + nameCol + ".Text;" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                textoColsMostrar = textoColsMostrar + "txt" + nameCol + ".Text = objExistente." + nameCol + ";" + ("\r\n" + ("\t\t\t\t"));
                                break;

                            case "INT":
                                textoColsIns = textoColsIns + "obj." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".Value.ToString());" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".Value.ToString());" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                if (permiteNull)
                                {
                                    textoColsMostrar = textoColsMostrar + "nud" + nameCol + ".Value = (string.IsNullOrEmpty(objExistente." + nameCol + ".ToString())) ? 0 : " + tipoCol + ".Parse(objExistente." + nameCol + ".ToString());" + ("\r\n" + ("\t\t\t\t"));
                                }
                                else
                                {
                                    textoColsMostrar = textoColsMostrar + "nud" + nameCol + ".Value = objExistente." + nameCol + ";" + ("\r\n" + ("\t\t\t\t"));
                                }

                                break;

                            case "INT32":
                                textoColsIns = textoColsIns + "obj." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".Value.ToString());" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".Value.ToString());" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                if (permiteNull)
                                {
                                    textoColsMostrar = textoColsMostrar + "nud" + nameCol + ".Value = (string.IsNullOrEmpty(objExistente." + nameCol + ".ToString())) ? 0 : " + tipoCol + ".Parse(objExistente." + nameCol + ".ToString());" + ("\r\n" + ("\t\t\t\t"));
                                }
                                else
                                {
                                    textoColsMostrar = textoColsMostrar + "nud" + nameCol + ".Value = objExistente." + nameCol + ";" + ("\r\n" + ("\t\t\t\t"));
                                }

                                break;

                            case "INT64":
                                textoColsIns = textoColsIns + "obj." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".Value.ToString());" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".Value.ToString());" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                if (permiteNull)
                                {
                                    textoColsMostrar = textoColsMostrar + "nud" + nameCol + ".Value = (string.IsNullOrEmpty(objExistente." + nameCol + ".ToString())) ? 0 : " + tipoCol + ".Parse(objExistente." + nameCol + ".ToString());" + ("\r\n" + ("\t\t\t\t"));
                                }
                                else
                                {
                                    textoColsMostrar = textoColsMostrar + "nud" + nameCol + ".Value = objExistente." + nameCol + ";" + ("\r\n" + ("\t\t\t\t"));
                                }

                                break;

                            case "DECIMAL":
                                textoColsIns = textoColsIns + "obj." + nameCol + " = " + tipoCol + ".Parse(nud" + ('\"' + '\"' + (".Value.ToString());" + ("\r\n" + ("\t\t\t\t" + '\t'))));
                                textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".Value.ToString());" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                if (permiteNull)
                                {
                                    textoColsMostrar = textoColsMostrar + "nud" + nameCol + ".Value = (string.IsNullOrEmpty(objExistente." + nameCol + ".ToString())) ? 0 : " + tipoCol + ".Parse(objExistente." + nameCol + ".ToString());" + ("\r\n" + ("\t\t\t\t"));
                                }
                                else
                                {
                                    textoColsMostrar = textoColsMostrar + "nud" + nameCol + ".Value = objExistente." + nameCol + ";" + ("\r\n" + ("\t\t\t\t"));
                                }

                                break;

                            case "DATETIME":
                                textoColsIns = textoColsIns + "obj." + nameCol + " = dtp" + nameCol + ".Value.Date;" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = dtp" + nameCol + ".Value.Date;" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                if (permiteNull)
                                {
                                    textoColsMostrar = textoColsMostrar + "dtp" + nameCol + ".Value = string.IsNullOrEmpty(objExistente." + nameCol + ".ToString()) ? DateTime.Now : Convert.ToDateTime(objExistente." + nameCol + ");" + ("\r\n" + ("\t\t\t\t"));
                                }
                                else
                                {
                                    textoColsMostrar = textoColsMostrar + "dtp" + nameCol + ".Value = objExistente." + nameCol + ";" + ("\r\n" + ("\t\t\t\t"));
                                }

                                break;

                            case "DATE":
                                textoColsIns = textoColsIns + "obj." + nameCol + " = dtp" + nameCol + ".Value.Date;" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = dtp" + nameCol + ".Value.Date;" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                if (permiteNull)
                                {
                                    textoColsMostrar = textoColsMostrar + "dtp" + nameCol + ".Value = string.IsNullOrEmpty(objExistente." + nameCol + ".ToString()) ? DateTime.Now : Convert.ToDateTime(objExistente." + nameCol + ");" + ("\r\n" + ("\t\t\t\t"));
                                }
                                else
                                {
                                    textoColsMostrar = textoColsMostrar + "dtp" + nameCol + ".Value = objExistente." + nameCol + ";" + ("\r\n" + ("\t\t\t\t"));
                                }

                                break;

                            case "BYTE[]":
                                textoColsIns = textoColsIns + "obj." + nameCol + " = new byte[0];" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = new byte[0];" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                break;

                            default:
                                textoColsIns = textoColsIns + "obj." + nameCol + " = obj." + nameCol + ";" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = obj." + nameCol + ";" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                textoColsMostrar = textoColsMostrar + "//txt" + nameCol + ".Text = objExistente." + nameCol + ";" + ("\r\n" + ("\t\t\t\t"));
                                break;
                        }
                    }
                }
            }

            texto = texto.Replace("%OBJETO_INSERT%", textoColsIns);
            texto = texto.Replace("%OBJETO_UPDATE%", textoColsUpd);
            texto = texto.Replace("%ARRAY_PK%", textoArrayPK);
            texto = texto.Replace("%ARRAY_VALORES_PK%", textoArrayValoresPK);
            texto = texto.Replace("%OBJETO_MOSTRAR%", textoColsMostrar);
            texto = Cabecera + texto;
            CFunciones.CrearArchivo(texto, prefijoForms + pNameClase + ".cs", PathDesktop + "\\" + namespaceForms + "\\");

            return namespaceForms + "\\" + prefijoForms + pNameClase + ".cs";
        }

        public string CrearSubFormularioVB(DataTable dtCols, string pNameClase, string pNameTabla)
        {
            if (bMoverResultados)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + infoNameProyApp;
            }
            else
            {
                PathDesktop = infoPathProyecto + "\\" + infoNameProyApp;
            }

            if (Directory.Exists(PathDesktop))
            {
                Directory.CreateDirectory(PathDesktop);
            }

            var newTexto = new StringBuilder();
            newTexto.Append(Resources.WIN_VB_SubFormVB);
            newTexto.Replace("%PAR_NOMBRE%", prefijoWebList + pNameClase);
            newTexto.Replace("%PAR_DESCRIPCION%", "Clase que define los metodos y propiedades del Formulario " + prefijoForms + pNameClase);
            newTexto.Replace("%NAMESPACE%", infoNameProyClass + "." + namespaceForms);
            newTexto.Replace("%NAME_FORM%", prefijoForms + pNameClase);
            newTexto.Replace("%MODELO_OBJETO%", prefijoModelo + pNameClase);
            newTexto.Replace("%OBJETO%", prefijoEntidades + pNameClase);
            // Armamos las columnas
            var textoColsIns = "";
            var textoColsUpd = "";
            var textoArrayPK = "";
            var textoArrayValoresPK = "";
            var strLimpiarCampos = "";
            var strListadoPK = "";
            var bEsPrimerElemento = true;
            var strUpdate = "";
            var strDeletePk = "";
            var strUpdatePk = "";
            var strApiTrans = "";
            var strApiEstado = "";
            var strUsuCre = "";
            var strFecCre = "";
            var strUsuMod = "";
            var strFecMod = "";
            var strFuncionesExtrasDesp = "";
            var strFuncionesExtras = "";
            string TablaForanea;
            var strReadOnlyPK = "";
            var j = 0;
            for (var iCol = 0; iCol
                               <= dtCols.Rows.Count - 1; iCol++)
            {
                nameCol = dtCols.Rows[iCol][0].ToString();
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                {
                    strUsuCre = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                {
                    strUsuMod = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                {
                    strFecCre = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                {
                    strFecMod = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                {
                    strApiEstado = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                {
                    strApiTrans = nameCol;
                }
            }

            for (var iCol = 0; iCol
                               <= dtCols.Rows.Count - 1; iCol++)
            {
                nameCol = dtCols.Rows[iCol][0].ToString();
                tipoCol = dtCols.Rows[iCol][1].ToString();
                permiteNull = dtCols.Rows[iCol]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES";
                esForeign = dtCols.Rows[iCol]["EsFK"].ToString() == "Yes";
                esAutoGenerada = dtCols.Rows[iCol]["EsIdentidad"].ToString().ToUpper() == "YES";
                TablaForanea = string.IsNullOrEmpty(dtCols.Rows[iCol]["TablaForanea"].ToString()) ? "" : dtCols.Rows[iCol]["TablaForanea"].ToString();
                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        TablaForanea = TablaForanea;
                        break;

                    case TipoBD.SQLServer2000:
                        TablaForanea = TablaForanea;
                        break;

                    case TipoBD.Oracle:
                        if (TablaForanea != "")
                        {
                            TablaForanea = TablaForanea.Substring(0, 1).ToUpper() + TablaForanea.Substring(1, 2).ToLower() + TablaForanea.Substring(3, 1).ToUpper() + TablaForanea.Substring(4).ToLower();
                        }

                        break;
                }
                // Si es Llave Primaria
                if (esPK)
                {
                    if (bEsPrimerElemento)
                    {
                        strListadoPK = tipoCol + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strListadoPK = strListadoPK + ", " + tipoCol + nameCol;
                    }

                    strUpdatePk = strUpdatePk + "Dim " + tipoCol + nameCol + " As " + tipoCol + " = row(" + ('\"'
                        + (nameCol + ('\"' + (")" + "\r\n\t\t\t"))));
                    strDeletePk = strDeletePk + "Dim " + tipoCol + nameCol + " As " + tipoCol + " = dtgListado.SelectedRows.Item(0).Cells.Item(" + ('\"'
                        + (nameCol + ('\"' + (").Value" + "\r\n\t\t\t"))));
                    strReadOnlyPK = strReadOnlyPK + "dtgListado.Columns(" + ('\"'
                                                                             + (nameCol + ('\"' + (").ReadOnly = True" + "\r\n\t\t\t"))));
                    textoArrayPK = textoArrayPK + "arrColumnasWhere.Add(" + ('\"'
                                                                             + (nameCol + ('\"' + (")" + "\r\n\t\t\t"))));
                    textoArrayValoresPK = textoArrayValoresPK + "arrValoresWhere.Add(_arrPrimaryKeys(" + (j + ("))" + "\r\n\t\t\t"));
                    j = j + 1;
                }

                if (!esAutoGenerada)
                {
                    if (esForeign)
                    {
                        strFuncionesExtras = strFuncionesExtras + "cargarCmb" + nameCol.Replace(pNameClase, "") + "()" + "\r\n\t\t\t";
                        strFuncionesExtrasDesp = strFuncionesExtrasDesp + "\r\n" + ("\t\t" + ("Private Sub cargarCmb" + nameCol.Replace(pNameClase, "") + "()" + "\r\n" + ("\t\t\t" + ("Try" + "\r\n" + ("\t\t\t\t" + ("Dim rn as New " + prefijoModelo + TablaForanea + "()" + "\r\n" + ("\t\t\t\t" + ("rn.CargarCombo(cmb" + nameCol.Replace(pNameClase, "") + ")" + "\r\n" + ("\t\t\t" + ("Catch ex As Exception" + "\r\n" + ("\t\t\t\t" + ("Using frm As New FErrores(ex, FErrores.tipoMsj._error, " + ('\"' + ("Error" + ('\"' + (")" + "\r\n" + ("\t\t\t\t" + '\t' + ("frm.ShowDialog()" + "\r\n" + ("\t\t\t\t" + ("End Using" + "\r\n" + ("\t\t\t" + ("End Try" + "\r\n" + ("\t\t" + ("End Sub" + "\r\n"))))))))))))))))))))))));
                    }

                    // Si NO es llave primaria
                    if (!esPK)
                    {
                        if (nameCol == strApiEstado)
                        {
                        }
                        else if (nameCol == strApiTrans)
                        {
                            textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = CApi.Transaccion.MODIFICAR" + "\r\n\t\t\t";
                        }
                        else if (nameCol == strUsuMod)
                        {
                            textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = cParametrosApp.AppUsuario.login" + "\r\n\t\t\t";
                        }
                        else if (nameCol == strFecMod)
                        {
                        }
                        else if (nameCol == strUsuCre)
                        {
                            textoColsIns = textoColsIns + "obj." + nameCol + " = cParametrosApp.AppUsuario.login" + "\r\n\t\t\t";
                        }
                        else if (nameCol == strFecCre)
                        {
                        }
                        else
                        {
                            switch (tipoCol.ToUpper())
                            {
                                case "STRING":
                                    if (permiteNull)
                                    {
                                        strUpdate = strUpdate + "If String.IsNullOrEmpty(row(" + ('\"'
                                            + (nameCol + ('\"' + (").ToString())" + ("\r\n" + ("\t\t"))))));
                                        strUpdate = strUpdate + "objExistente." + nameCol + " = " + ('\"' + '\"' + (")" + ("\r\n" + ("\t\t"))));
                                        strUpdate = strUpdate + "Else" + ("\r\n" + ("\t\t"));
                                        strUpdate = strUpdate + "objExistente." + nameCol + " = row(" + ('\"'
                                            + (nameCol + ('\"' + (")" + ("\r\n" + ("\t\t"))))));
                                        strUpdate = strUpdate + "End If" + ("\r\n" + ("\t\t"));
                                    }
                                    else
                                    {
                                        strUpdate = strUpdate + "objExistente." + nameCol + " = row(" + ('\"'
                                            + (nameCol + ('\"' + (")" + ("\r\n" + ("\t\t"))))));
                                    }

                                    break;

                                default:
                                    strUpdate = strUpdate + "objExistente." + nameCol + " = row(" + ('\"'
                                        + (nameCol + ('\"' + (")" + ("\r\n" + ("\t\t"))))));
                                    break;
                            }
                            if (esForeign)
                            {
                                textoColsIns = textoColsIns + "obj." + nameCol + " = cmb" + nameCol + ".SelectedValue" + "\r\n\t\t\t";
                                textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = cmb" + nameCol + ".SelectedValue" + "\r\n\t\t\t";
                            }
                            else
                            {
                                switch (tipoCol.ToUpper())
                                {
                                    case "BOOLEAN":
                                        strLimpiarCampos = strLimpiarCampos + "chk" + nameCol.Replace(pNameClase, "") + ".Checked = False" + "\r\n\t\t\t";
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = chk" + nameCol + ".Checked" + "\r\n\t\t\t";
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = chk" + nameCol + ".Checked" + "\r\n\t\t\t";
                                        break;

                                    case "BOOL":
                                        strLimpiarCampos = strLimpiarCampos + "chk" + nameCol.Replace(pNameClase, "") + ".Checked = False" + "\r\n\t\t\t";
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = chk" + nameCol + ".Checked" + "\r\n\t\t\t";
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = chk" + nameCol + ".Checked" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        break;

                                    case "STRING":
                                        strLimpiarCampos = strLimpiarCampos + "txt" + nameCol.Replace(pNameClase, "") + ".Text = String.Empty" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = txt" + nameCol + ".Text" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = txt" + nameCol + ".Text" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        break;

                                    case "INT":
                                        strLimpiarCampos = strLimpiarCampos + "nud" + nameCol.Replace(pNameClase, "") + ".Value = 0" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".Value.ToString())" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".Value.ToString())" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        break;

                                    case "INTEGER":
                                        strLimpiarCampos = strLimpiarCampos + "nud" + nameCol.Replace(pNameClase, "") + ".Value = 0" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".Value.ToString())" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".Value.ToString())" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        break;

                                    case "INT32":
                                        strLimpiarCampos = strLimpiarCampos + "nud" + nameCol.Replace(pNameClase, "") + ".Value = 0" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".Value.ToString())" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".Value.ToString())" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        break;

                                    case "INT64":
                                        strLimpiarCampos = strLimpiarCampos + "nud" + nameCol.Replace(pNameClase, "") + ".Value = 0" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".Value.ToString())" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".Value.ToString())" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        break;

                                    case "DECIMAL":
                                        strLimpiarCampos = strLimpiarCampos + "nud" + nameCol.Replace(pNameClase, "") + ".Value = 0" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = " + tipoCol + ".Parse(nud" + ('\"' + '\"' + (".Value.ToString())" + ("\r\n" + ("\t\t\t\t" + '\t'))));
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".Value.ToString())" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        break;

                                    case "DATETIME":
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = dtp" + nameCol + ".Value.Date" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = dtp" + nameCol + ".Value.Date" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        break;

                                    case "DATE":
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = dtp" + nameCol + ".Value.Date" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = dtp" + nameCol + ".Value.Date" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        break;

                                    case "BYTE[]":
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = new byte[0]" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = new byte[0]" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        break;

                                    default:
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = obj." + nameCol + "" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = objExistente." + nameCol + "" + ("\r\n" + ("\t\t\t\t" + '\t'));
                                        break;
                                }
                            }
                        }
                    }
                }
            }

            newTexto.Replace("%PROYECTO_CLASS%", infoNameProyClass);
            newTexto.Replace("%LIMPIAR_CAMPOS%", strLimpiarCampos);
            newTexto.Replace("%OBJETO_INSERT%", textoColsIns);
            newTexto.Replace("%ARRAY_PK%", textoArrayPK);
            newTexto.Replace("%ARRAY_VALORES_PK%", textoArrayValoresPK);
            newTexto.Replace("%CLASE_ACCESODATOS%", prefijoModelo + pNameClase);
            newTexto.Replace("%OBJETO_UPDATE%", strUpdate);
            newTexto.Replace("%OBJETO_DELETE%", strDeletePk);
            newTexto.Replace("%STRING_PK%", strListadoPK);
            newTexto.Replace("%OBJETO_UPDATE_PK%", strUpdatePk);
            newTexto.Replace("%USU_CRE%", strUsuCre);
            newTexto.Replace("%USU_MOD%", strUsuMod);
            newTexto.Replace("%FEC_CRE%", strFecCre);
            newTexto.Replace("%FEC_MOD%", strFecMod);
            newTexto.Replace("%API_TRANS%", strApiTrans);
            newTexto.Replace("%API_ESTADO%", strApiEstado);
            newTexto.Replace("%FUNCIONES_EXTRAS_DESP%", strFuncionesExtrasDesp);
            newTexto.Replace("%FUNCIONES_EXTRAS%", strFuncionesExtras);
            newTexto.Replace("%READONLY_PK%", strReadOnlyPK);
            if (Directory.Exists(PathDesktop + "\\" + namespaceForms) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + namespaceForms);
            }

            CFunciones.CrearArchivo(newTexto.ToString(), prefijoForms + pNameClase + ".vb", PathDesktop + "\\" + namespaceForms + "\\");
            return namespaceForms + "\\" + prefijoForms + pNameClase + ".vb";
        }

        public string CrearFormularioDesigner(DataTable dtCols, string pNameClase, string pNameTabla)
        {
            var texto = "";
            texto = texto + "namespace " + infoNameProyClass + "." + namespaceForms + "\r\n";
            texto = texto + "{" + "\r\n";
            texto = texto + ('\t' + ("partial class " + prefijoForms + pNameClase + "\r\n"));
            texto = texto + ('\t' + ("{" + "\r\n"));
            texto = texto + ("\t\t" + ("/// <summary>" + "\r\n"));
            texto = texto + ("\t\t" + ("/// Required designer variable." + "\r\n"));
            texto = texto + ("\t\t" + ("/// </summary>" + "\r\n"));
            texto = texto + ("\t\t" + ("private System.ComponentModel.IContainer components = null;\r\n"));
            texto = texto + "" + "\r\n";
            texto = texto + ("\t\t" + ("/// <summary>" + "\r\n"));
            texto = texto + ("\t\t" + ("/// Clean up any resources being used." + "\r\n"));
            texto = texto + ("\t\t" + ("/// </summary>" + "\r\n"));
            texto = texto + ("\t\t" + ("/// <param name=" + ('\"' + ("disposing" + ('\"' + (">true if managed resources should be disposed; otherwise, false.</param>" + "\r\n"))))));
            texto = texto + ("\t\t" + ("protected override void Dispose(bool disposing)" + "\r\n"));
            texto = texto + ("\t\t" + ("{" + "\r\n"));
            texto = texto + ("\t\t\t" + ("if (disposing && (components != null))" + "\r\n"));
            texto = texto + ("\t\t\t" + ("{" + "\r\n"));
            texto = texto + ("\t\t\t\t" + ("components.Dispose();\r\n"));
            texto = texto + ("\t\t\t" + ("}" + "\r\n"));
            texto = texto + ("\t\t\t" + ("base.Dispose(disposing);\r\n"));
            texto = texto + ("\t\t" + ("}" + "\r\n"));
            texto = texto + ("\t\t" + ("" + "\r\n"));
            // Initializa Component
            texto = texto + ("\t\t" + ("#region Windows Form Designer generated code" + "\r\n"));
            texto = texto + ("\t\t" + ("" + "\r\n"));
            texto = texto + ("\t\t" + ("/// <summary>" + "\r\n"));
            texto = texto + ("\t\t" + ("/// Required method for Designer support - do not modify" + "\r\n"));
            texto = texto + ("\t\t" + ("/// the contents of this method with the code editor." + "\r\n"));
            texto = texto + ("\t\t" + ("/// </summary>" + "\r\n"));
            texto = texto + ("\t\t" + ("private void InitializeComponent()" + "\r\n"));
            texto = texto + ("\t\t" + ("{" + "\r\n"));
            texto = texto + ("\t\t\t" + ("this.btnAceptar = new System.Windows.Forms.Button();\r\n"));
            texto = texto + ("\t\t\t" + ("this.btnCancelar = new System.Windows.Forms.Button();\r\n"));
            for (var i = 0; i
                            <= dtCols.Rows.Count - 1; i++)
            {
                nameCol = dtCols.Rows[i][0].ToString();
                tipoCol = dtCols.Rows[i][1].ToString();
                permiteNull = dtCols.Rows[i]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[i]["EsPK"].ToString().ToUpper() == "YES";
                esAutoGenerada = dtCols.Rows[i]["EsIdentidad"].ToString().ToUpper() == "YES";
                texto = texto + ("\t\t\t" + ("this.lbl" + dtCols.Rows[i]["name"] + " = new System.Windows.Forms.Label();\r\n"));
                if (dtCols.Rows[i]["EsFK"].ToString() == "Yes")
                {
                    texto = texto + ("\t\t\t" + ("this.cmb" + dtCols.Rows[i]["name"] + " = new System.Windows.Forms.ComboBox();\r\n"));
                }
                else if (esAutoGenerada)
                {
                    texto = texto + ("\t\t\t" + ("this.txt" + dtCols.Rows[i]["name"] + " = new System.Windows.Forms.TextBox();\r\n"));
                }
                else
                {
                    switch (tipoCol.ToUpper())
                    {
                        case "STRING":
                            texto = texto + ("\t\t\t" + ("this.txt" + dtCols.Rows[i]["name"] + " = new System.Windows.Forms.TextBox();\r\n"));
                            break;

                        case "INT":
                            texto = texto + ("\t\t\t" + ("this.nud" + dtCols.Rows[i]["name"] + " = new System.Windows.Forms.NumericUpDown();\r\n"));
                            break;

                        case "INT32":
                            texto = texto + ("\t\t\t" + ("this.nud" + dtCols.Rows[i]["name"] + " = new System.Windows.Forms.NumericUpDown();\r\n"));
                            break;

                        case "INT64":
                            texto = texto + ("\t\t\t" + ("this.nud" + dtCols.Rows[i]["name"] + " = new System.Windows.Forms.NumericUpDown();\r\n"));
                            break;

                        case "DECIMAL":
                            texto = texto + ("\t\t\t" + ("this.nud" + dtCols.Rows[i]["name"] + " = new System.Windows.Forms.NumericUpDown();\r\n"));
                            break;

                        case "DATETIME":
                            texto = texto + ("\t\t\t" + ("this.dtp" + dtCols.Rows[i]["name"] + " = new System.Windows.Forms.DateTimePicker();\r\n"));
                            break;

                        case "DATE":
                            texto = texto + ("\t\t\t" + ("this.dtp" + dtCols.Rows[i]["name"] + " = new System.Windows.Forms.DateTimePicker();\r\n"));
                            break;

                        default:
                            texto = texto + ("\t\t\t" + ("this.txt" + dtCols.Rows[i]["name"] + " = new System.Windows.Forms.TextBox();\r\n"));
                            break;
                    }
                }
            }

            texto = texto + ("\t\t\t" + ("" + "\r\n"));
            texto = texto + ("\t\t\t" + ("this.SuspendLayout();\r\n"));
            // Variables para dibujar
            var x = 18;
            var altoIntervalo = 25;
            var y1 = 12;
            var y2 = 100;
            var jTabIndex = 1;
            for (var i = 0; i
                            <= dtCols.Rows.Count - 1; i++)
            {
                nameCol = dtCols.Rows[i][0].ToString();
                tipoCol = dtCols.Rows[i][1].ToString();
                permiteNull = dtCols.Rows[i]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[i]["EsPK"].ToString().ToUpper() == "YES";
                esAutoGenerada = dtCols.Rows[i]["EsIdentidad"].ToString().ToUpper() == "YES";
                texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                texto = texto + ("\t\t\t" + ("//lbl" + nameCol + "\r\n"));
                texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                texto = texto + ("\t\t\t" + ("this.lbl" + nameCol + ".AutoSize = true;\r\n"));
                texto = texto + ("\t\t\t" + ("this.lbl" + nameCol + ".Location = new System.Drawing.Point(" + (y1 + (", "
                    + (x + 3 + (");\r\n"))))));
                texto = texto + ("\t\t\t" + ("this.lbl" + nameCol + ".Name = " + ('\"' + ("lbl" + nameCol + ('\"' + (";\r\n"))))));
                texto = texto + ("\t\t\t" + ("this.lbl" + nameCol + ".Size = new System.Drawing.Size(16, 13);\r\n"));
                texto = texto + ("\t\t\t" + ("this.lbl" + nameCol + ".TabIndex = " + (jTabIndex + (";\r\n"))));
                texto = texto + ("\t\t\t" + ("this.lbl" + nameCol + ".Text = " + ('\"'
                    + (nameCol + ":" + ('\"' + (";\r\n"))))));
                jTabIndex = jTabIndex + 1;
                if (dtCols.Rows[i]["EsFK"].ToString() == "Yes")
                {
                    texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                    texto = texto + ("\t\t\t" + ("//cmb" + nameCol + "\r\n"));
                    texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                    texto = texto + ("\t\t\t" + ("this.cmb" + nameCol + ".FormattingEnabled = true;\r\n"));
                    texto = texto + ("\t\t\t" + ("this.cmb" + nameCol + ".Location = new System.Drawing.Point(" + (y2 + (", "
                        + (x + (");\r\n"))))));
                    texto = texto + ("\t\t\t" + ("this.cmb" + nameCol + ".Name = " + ('\"' + ("cmb" + nameCol + ('\"' + (";\r\n"))))));
                    texto = texto + ("\t\t\t" + ("this.cmb" + nameCol + ".Size = new System.Drawing.Size(204, 21);\r\n"));
                    texto = texto + ("\t\t\t" + ("this.cmb" + nameCol + ".TabIndex = " + (jTabIndex + (";\r\n"))));
                }
                else if (esAutoGenerada)
                {
                    // Texto
                    texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                    texto = texto + ("\t\t\t" + ("//txt" + nameCol + "\r\n"));
                    texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                    texto = texto + ("\t\t\t" + ("this.txt" + nameCol + ".Location = new System.Drawing.Point(" + (y2 + (", "
                        + (x + (");\r\n"))))));
                    texto = texto + ("\t\t\t" + ("this.txt" + nameCol + ".Name = " + ('\"' + ("txt" + nameCol + ('\"' + (";\r\n"))))));
                    texto = texto + ("\t\t\t" + ("this.txt" + nameCol + ".Size = new System.Drawing.Size(204, 20);\r\n"));
                    texto = texto + ("\t\t\t" + ("this.txt" + nameCol + ".TabIndex = " + (jTabIndex + (";\r\n"))));
                    texto = texto + ("\t\t\t" + ("this.txt" + nameCol + ".ReadOnly = true;\r\n"));
                }
                else
                {
                    switch (tipoCol.ToUpper())
                    {
                        case "STRING":
                            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                            texto = texto + ("\t\t\t" + ("//txt" + nameCol + "\r\n"));
                            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                            texto = texto + ("\t\t\t" + ("this.txt" + nameCol + ".Location = new System.Drawing.Point(" + (y2 + (", "
                                + (x + (");\r\n"))))));
                            texto = texto + ("\t\t\t" + ("this.txt" + nameCol + ".Name = " + ('\"' + ("txt" + nameCol + ('\"' + (";\r\n"))))));
                            texto = texto + ("\t\t\t" + ("this.txt" + nameCol + ".Size = new System.Drawing.Size(204, 20);\r\n"));
                            texto = texto + ("\t\t\t" + ("this.txt" + nameCol + ".TabIndex = " + (jTabIndex + (";\r\n"))));
                            break;

                        case "INT":
                            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                            texto = texto + ("\t\t\t" + ("//nud" + nameCol + "\r\n"));
                            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".Location = new System.Drawing.Point(" + (y2 + (", "
                                + (x + (");\r\n"))))));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".Name = " + ('\"' + ("nud" + nameCol + ('\"' + (";\r\n"))))));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".Size = new System.Drawing.Size(204, 20);\r\n"));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".TabIndex = " + (jTabIndex + (";\r\n"))));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".TextAlign = System.Windows.Forms.HorizontalAlignment.Right;\r\n"));
                            break;

                        case "INT32":
                            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                            texto = texto + ("\t\t\t" + ("//nud" + nameCol + "\r\n"));
                            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".Location = new System.Drawing.Point(" + (y2 + (", "
                                + (x + (");\r\n"))))));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".Name = " + ('\"' + ("nud" + nameCol + ('\"' + (";\r\n"))))));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".Size = new System.Drawing.Size(204, 20);\r\n"));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".TabIndex = " + (jTabIndex + (";\r\n"))));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".TextAlign = System.Windows.Forms.HorizontalAlignment.Right;\r\n"));
                            break;

                        case "INT64":
                            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                            texto = texto + ("\t\t\t" + ("//nud" + nameCol + "\r\n"));
                            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".Location = new System.Drawing.Point(" + (y2 + (", "
                                + (x + (");\r\n"))))));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".Name = " + ('\"' + ("nud" + nameCol + ('\"' + (";\r\n"))))));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".Size = new System.Drawing.Size(204, 20);\r\n"));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".TabIndex = " + (jTabIndex + (";\r\n"))));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".TextAlign = System.Windows.Forms.HorizontalAlignment.Right;\r\n"));
                            break;

                        case "DECIMAL":
                            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                            texto = texto + ("\t\t\t" + ("//nud" + nameCol + "\r\n"));
                            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".Location = new System.Drawing.Point(" + (y2 + (", "
                                + (x + (");\r\n"))))));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".Name = " + ('\"' + ("nud" + nameCol + ('\"' + (";\r\n"))))));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".Size = new System.Drawing.Size(204, 20);\r\n"));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".TabIndex = " + (jTabIndex + (";\r\n"))));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".TextAlign = System.Windows.Forms.HorizontalAlignment.Right;\r\n"));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".DecimalPlaces = 2;\r\n"));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".Increment = new decimal(new int[] {1,0,0,131072});\r\n"));
                            texto = texto + ("\t\t\t" + ("this.nud" + nameCol + ".Maximum = new decimal(new int[] {9999,0,0,0});\r\n"));
                            break;

                        case "DATETIME":
                            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                            texto = texto + ("\t\t\t" + ("//dtp" + nameCol + "\r\n"));
                            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                            texto = texto + ("\t\t\t" + ("this.dtp" + nameCol + ".Format = System.Windows.Forms.DateTimePickerFormat.Short;\r\n"));
                            texto = texto + ("\t\t\t" + ("this.dtp" + nameCol + ".Location = new System.Drawing.Point(" + (y2 + (", "
                                + (x + (");\r\n"))))));
                            texto = texto + ("\t\t\t" + ("this.dtp" + nameCol + ".Name = " + ('\"' + ("dtp" + nameCol + ('\"' + (";\r\n"))))));
                            texto = texto + ("\t\t\t" + ("this.dtp" + nameCol + ".Size = new System.Drawing.Size(204, 20);\r\n"));
                            texto = texto + ("\t\t\t" + ("this.dtp" + nameCol + ".TabIndex = " + (jTabIndex + (";\r\n"))));
                            break;

                        case "DATE":
                            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                            texto = texto + ("\t\t\t" + ("//dtp" + nameCol + "\r\n"));
                            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                            texto = texto + ("\t\t\t" + ("this.dtp" + nameCol + ".Format = System.Windows.Forms.DateTimePickerFormat.Short;\r\n"));
                            texto = texto + ("\t\t\t" + ("this.dtp" + nameCol + ".Location = new System.Drawing.Point(" + (y2 + (", "
                                + (x + (");\r\n"))))));
                            texto = texto + ("\t\t\t" + ("this.dtp" + nameCol + ".Name = " + ('\"' + ("dtp" + nameCol + ('\"' + (";\r\n"))))));
                            texto = texto + ("\t\t\t" + ("this.dtp" + nameCol + ".Size = new System.Drawing.Size(204, 20);\r\n"));
                            texto = texto + ("\t\t\t" + ("this.dtp" + nameCol + ".TabIndex = " + (jTabIndex + (";\r\n"))));
                            break;

                        default:
                            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                            texto = texto + ("\t\t\t" + ("//txt" + nameCol + "\r\n"));
                            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
                            texto = texto + ("\t\t\t" + ("this.txt" + nameCol + ".Location = new System.Drawing.Point(" + (y2 + (", "
                                + (x + (");\r\n"))))));
                            texto = texto + ("\t\t\t" + ("this.txt" + nameCol + ".Name = " + ('\"' + ("txt" + nameCol + ('\"' + (";\r\n"))))));
                            texto = texto + ("\t\t\t" + ("this.txt" + nameCol + ".Size = new System.Drawing.Size(204, 20);\r\n"));
                            texto = texto + ("\t\t\t" + ("this.txt" + nameCol + ".TabIndex = " + (jTabIndex + (";\r\n"))));
                            break;
                    }
                }

                x = x + altoIntervalo;
                jTabIndex = jTabIndex + 1;
            }

            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
            texto = texto + ("\t\t\t//btnAceptar");
            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
            texto = texto + ("\t\t\t" + ("this.btnAceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bott" +
                                                   "om | System.Windows.Forms.AnchorStyles.Right)));\r\n"));
            texto = texto + ("\t\t\t" + ("this.btnAceptar.Location = new System.Drawing.Point(114, "
                                                   + (x + altoIntervalo + (20 - 44) + (");\r\n"))));
            texto = texto + ("\t\t\t" + ("this.btnAceptar.Name = " + ('\"' + ("btnAceptar" + ('\"' + (";\r\n"))))));
            texto = texto + ("\t\t\t" + ("this.btnAceptar.Size = new System.Drawing.Size(94, 33);\r\n"));
            texto = texto + ("\t\t\t" + ("this.btnAceptar.TabIndex = "
                                                   + (jTabIndex + (";\r\n"))));
            texto = texto + ("\t\t\t" + ("this.btnAceptar.Text = " + ('\"' + ("&Aceptar" + ('\"' + (";\r\n"))))));
            texto = texto + ("\t\t\t" + ("this.btnAceptar.UseVisualStyleBackColor = true;\r\n"));
            texto = texto + ("\t\t\t" + ("this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);\r\n"));
            jTabIndex = jTabIndex + 1;
            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
            texto = texto + ("\t\t\t" + ("// btnCancelar" + "\r\n"));
            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
            texto = texto + ("\t\t\t" + ("this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bot" +
                                                   "tom | System.Windows.Forms.AnchorStyles.Right)));\r\n"));
            texto = texto + ("\t\t\t" + ("this.btnCancelar.Location = new System.Drawing.Point(214, "
                                                   + (x + altoIntervalo + (20 - 44) + (");\r\n"))));
            texto = texto + ("\t\t\t" + ("this.btnCancelar.Name = " + ('\"' + ("btnCancelar" + ('\"' + (";\r\n"))))));
            texto = texto + ("\t\t\t" + ("this.btnCancelar.Size = new System.Drawing.Size(94, 33);\r\n"));
            texto = texto + ("\t\t\t" + ("this.btnCancelar.TabIndex = "
                                                   + (jTabIndex + (";\r\n"))));
            texto = texto + ("\t\t\t" + ("this.btnCancelar.Text = " + ('\"' + ("&Cancelar" + ('\"' + (";\r\n"))))));
            texto = texto + ("\t\t\t" + ("this.btnCancelar.UseVisualStyleBackColor = true;\r\n"));
            texto = texto + ("\t\t\t" + ("this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);\r\n"));
            texto = texto + ("\t\t\t" + ("" + "\r\n"));
            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
            texto = texto + ("\t\t\t" + ("//" + prefijoForms + pNameClase + "\r\n"));
            texto = texto + ("\t\t\t" + ("//" + "\r\n"));
            texto = texto + ("\t\t\t" + ("this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);\r\n"));
            texto = texto + ("\t\t\t" + ("this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;\r\n"));
            texto = texto + ("\t\t\t" + ("this.ClientSize = new System.Drawing.Size(320, "
                                                   + (x + altoIntervalo + 20 + (");\r\n"))));
            texto = texto + ("\t\t\t" + ("this.Controls.Add(this.btnAceptar);\r\n"));
            texto = texto + ("\t\t\t" + ("this.Controls.Add(this.btnCancelar);\r\n"));
            for (var i = 0; i
                            <= dtCols.Rows.Count - 1; i++)
            {
                nameCol = dtCols.Rows[i][0].ToString();
                tipoCol = dtCols.Rows[i][1].ToString();
                permiteNull = dtCols.Rows[i]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[i]["EsPK"].ToString().ToUpper() == "YES";
                esAutoGenerada = dtCols.Rows[i]["EsIdentidad"].ToString().ToUpper() == "YES";
                texto = texto + ("\t\t\t" + ("this.Controls.Add(this.lbl" + nameCol + ");\r\n"));
                if (dtCols.Rows[i]["EsFK"].ToString() == "Yes")
                {
                    texto = texto + ("\t\t\t" + ("this.Controls.Add(this.cmb" + nameCol + ");\r\n"));
                }
                else if (esAutoGenerada)
                {
                    texto = texto + ("\t\t\t" + ("this.Controls.Add(this.txt" + nameCol + ");\r\n"));
                }
                else
                {
                    switch (tipoCol.ToUpper())
                    {
                        case "STRING":
                            texto = texto + ("\t\t\t" + ("this.Controls.Add(this.txt" + nameCol + ");\r\n"));
                            break;

                        case "INT":
                            texto = texto + ("\t\t\t" + ("this.Controls.Add(this.nud" + nameCol + ");\r\n"));
                            break;

                        case "INT32":
                            texto = texto + ("\t\t\t" + ("this.Controls.Add(this.nud" + nameCol + ");\r\n"));
                            break;

                        case "INT64":
                            texto = texto + ("\t\t\t" + ("this.Controls.Add(this.nud" + nameCol + ");\r\n"));
                            break;

                        case "DECIMAL":
                            texto = texto + ("\t\t\t" + ("this.Controls.Add(this.nud" + nameCol + ");\r\n"));
                            break;

                        case "DATETIME":
                            texto = texto + ("\t\t\t" + ("this.Controls.Add(this.dtp" + nameCol + ");\r\n"));
                            break;

                        case "DATE":
                            texto = texto + ("\t\t\t" + ("this.Controls.Add(this.dtp" + nameCol + ");\r\n"));
                            break;

                        default:
                            texto = texto + ("\t\t\t" + ("this.Controls.Add(this.txt" + nameCol + ");\r\n"));
                            break;
                    }
                }
            }

            texto = texto + ("\t\t\t" + ("this.Name = " + ('\"'
                                                                     + (prefijoForms + pNameClase + ('\"' + (";\r\n"))))));
            texto = texto + ("\t\t\t" + ("this.Text = " + ('\"' + ("ABM de " + pNameTabla + ('\"' + (";\r\n"))))));
            texto = texto + ("\t\t\t" + ("this.Load += new System.EventHandler(this." + prefijoForms + pNameClase + "_Load);\r\n"));
            texto = texto + ("\t\t\t" + ("this.ResumeLayout(false);\r\n"));
            texto = texto + ("\t\t\t" + ("this.PerformLayout();\r\n"));
            texto = texto + ("\t\t\t" + ("" + "\r\n"));
            texto = texto + ("\t\t" + ("}" + "\r\n"));
            texto = texto + ("\t\t" + ("#endregion" + "\r\n"));
            texto = texto + ("\t\t" + ("" + "\r\n"));
            texto = texto + ("\t\t" + ("private System.Windows.Forms.Button btnAceptar;\r\n"));
            texto = texto + ("\t\t" + ("private System.Windows.Forms.Button btnCancelar;\r\n"));
            for (var i = 0; i
                            <= dtCols.Rows.Count - 1; i++)
            {
                nameCol = dtCols.Rows[i][0].ToString();
                tipoCol = dtCols.Rows[i][1].ToString();
                permiteNull = dtCols.Rows[i]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[i]["EsPK"].ToString().ToUpper() == "YES";
                esAutoGenerada = dtCols.Rows[i]["EsIdentidad"].ToString().ToUpper() == "YES";
                texto = texto + ("\t\t" + ("private System.Windows.Forms.Label lbl" + dtCols.Rows[i]["name"] + ";\r\n"));
                // Ahora el elemento
                texto = texto + ("\t\tprivate System.Windows.Forms.");
                if (dtCols.Rows[i]["EsFK"].ToString() == "Yes")
                {
                    texto = texto + "ComboBox cmb";
                }
                else if (esAutoGenerada)
                {
                    texto = texto + "TextBox txt";
                }
                else
                {
                    switch (tipoCol.ToUpper())
                    {
                        case "STRING":
                            texto = texto + "TextBox txt";
                            break;

                        case "INT":
                            texto = texto + "NumericUpDown nud";
                            break;

                        case "INT32":
                            texto = texto + "NumericUpDown nud";
                            break;

                        case "INT64":
                            texto = texto + "NumericUpDown nud";
                            break;

                        case "DECIMAL":
                            texto = texto + "NumericUpDown nud";
                            break;

                        case "DATETIME":
                            texto = texto + "DateTimePicker dtp";
                            break;

                        case "DATE":
                            texto = texto + "DateTimePicker dtp";
                            break;

                        default:
                            texto = texto + "TextBox txt";
                            break;
                    }
                }

                texto = texto + nameCol + ";\r\n";
                texto = texto + "\r\n";
            }

            texto = texto + ('\t' + ("}" + "\r\n"));
            texto = texto + "}" + "\r\n";
            CFunciones.CrearArchivo(texto, prefijoForms + pNameClase + ".Designer.cs", PathDesktop + "\\" + namespaceForms + "\\");
            return namespaceForms + "\\" + prefijoForms + pNameClase + ".Designer.cs";
        }

        public string CrearSubFormularioDesignerVB(DataTable dtCols, string pNameClase, string pNameTabla)
        {
            if (bMoverResultados)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + infoNameProyApp;
            }
            else
            {
                PathDesktop = infoPathProyecto + "\\" + infoNameProyApp;
            }

            if (Directory.Exists(PathDesktop))
            {
                Directory.CreateDirectory(PathDesktop);
            }

            var newTexto = new StringBuilder();
            newTexto.AppendLine("<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _");
            newTexto.AppendLine("Partial Class " + prefijoForms + pNameClase);
            newTexto.AppendLine("\tInherits System.Windows.Forms.Form");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\'Form reemplaza a Dispose para limpiar la lista de componentes.");
            newTexto.AppendLine("\t<System.Diagnostics.DebuggerNonUserCode()> _");
            newTexto.AppendLine("\tProtected Overrides Sub Dispose(ByVal disposing As Boolean)");
            newTexto.AppendLine("\t\tIf disposing AndAlso components IsNot Nothing Then");
            newTexto.AppendLine("\t\t\tcomponents.Dispose()");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tMyBase.Dispose(disposing)");
            newTexto.AppendLine("\tEnd Sub");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\'Requerido por el Dise�ador de Windows Forms");
            newTexto.AppendLine("\tPrivate components As System.ComponentModel.IContainer");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento");
            newTexto.AppendLine("\t\'Se puede modificar usando el Dise�ador de Windows Forms.  ");
            newTexto.AppendLine("\t\'No lo modifique con el editor de c�digo.");
            newTexto.AppendLine("\t<System.Diagnostics.DebuggerStepThrough()> _");
            newTexto.AppendLine("\tPrivate Sub InitializeComponent()");
            newTexto.AppendLine("\t\t" + ("Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.Component" +
                                               "ResourceManager(GetType(" + prefijoForms + pNameClase + "))"));
            newTexto.AppendLine("\t\tMe.btnAgregar = New System.Windows.Forms.Button()");
            newTexto.AppendLine("\t\tMe.NewToolStripButton = New System.Windows.Forms.ToolStripButton()");
            newTexto.AppendLine("\t\tMe.SaveToolStripButton = New System.Windows.Forms.ToolStripButton()");
            newTexto.AppendLine("\t\tMe.DeleteToolStripButton = New System.Windows.Forms.ToolStripButton()");
            newTexto.AppendLine("\t\tMe.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()");
            newTexto.AppendLine("\t\tMe.PrintToolStripButton = New System.Windows.Forms.ToolStripButton()");
            newTexto.AppendLine("\t\tMe.PrintPreviewToolStripButton = New System.Windows.Forms.ToolStripButton()");
            newTexto.AppendLine("\t\tMe.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()");
            newTexto.AppendLine("\t\tMe.SalirStripButton = New System.Windows.Forms.ToolStripButton()");
            newTexto.AppendLine("\t\tMe.ToolMenu = New System.Windows.Forms.ToolStrip()");
            newTexto.AppendLine("\t\tMe.UsrUsuario1 = New usrUsuario()");
            newTexto.AppendLine("\t\tMe.lblTitulo = New System.Windows.Forms.Label()");
            newTexto.AppendLine("\t\tMe.dtgListado = New System.Windows.Forms.DataGridView()");
            var strApiTrans = "";
            var strApiEstado = "";
            var strUsuCre = "";
            var strFecCre = "";
            var strUsuMod = "";
            var strFecMod = "";
            for (var iCol = 0; iCol
                               <= dtCols.Rows.Count - 1; iCol++)
            {
                nameCol = dtCols.Rows[iCol][0].ToString();
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                {
                    strUsuCre = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                {
                    strUsuMod = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                {
                    strFecCre = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                {
                    strFecMod = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                {
                    strApiEstado = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                {
                    strApiTrans = nameCol;
                }
            }

            for (var i = 0; i
                            <= dtCols.Rows.Count - 1; i++)
            {
                nameCol = dtCols.Rows[i][0].ToString();
                tipoCol = dtCols.Rows[i][1].ToString();
                permiteNull = dtCols.Rows[i]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[i]["EsPK"].ToString().ToUpper() == "YES";
                esAutoGenerada = dtCols.Rows[i]["EsIdentidad"].ToString().ToUpper() == "YES";
                esForeign = dtCols.Rows[i]["EsFK"].ToString() == "Yes";
                if (nameCol != strApiEstado && nameCol != strApiTrans && nameCol != strUsuCre && nameCol != strUsuMod && nameCol != strFecCre && nameCol != strFecMod)
                {
                    newTexto.AppendLine("\t\tMe.lbl" + nameCol + " = New System.Windows.Forms.Label()");
                    if (!esAutoGenerada)
                    {
                        if (esForeign)
                        {
                            newTexto.AppendLine("\t\tMe.cmb" + nameCol + " = New System.Windows.Forms.ComboBox()");
                        }
                        else
                        {
                            switch (tipoCol.ToUpper())
                            {
                                case "BOOLEAN":
                                    newTexto.AppendLine("\t\tMe.chk" + nameCol + " = New System.Windows.Forms.CheckBox()");
                                    break;

                                case "BOOL":
                                    newTexto.AppendLine("\t\tMe.chk" + nameCol + " = New System.Windows.Forms.CheckBox()");
                                    break;

                                case "STRING":
                                    newTexto.AppendLine("\t\tMe.txt" + nameCol + " = New System.Windows.Forms.TextBox()");
                                    break;

                                case "INT":
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + " = New System.Windows.Forms.NumericUpDown()");
                                    break;

                                case "INTEGER":
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + " = New System.Windows.Forms.NumericUpDown()");
                                    break;

                                case "INT32":
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + " = New System.Windows.Forms.NumericUpDown()");
                                    break;

                                case "INT64":
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + " = New System.Windows.Forms.NumericUpDown()");
                                    break;

                                case "DECIMAL":
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + " = New System.Windows.Forms.NumericUpDown()");
                                    break;

                                case "DATETIME":
                                    newTexto.AppendLine("\t\tMe.dtp" + nameCol + " = New System.Windows.Forms.DateTimePicker()");
                                    break;

                                case "DATE":
                                    newTexto.AppendLine("\t\tMe.dtp" + nameCol + " = New System.Windows.Forms.DateTimePicker()");
                                    break;

                                default:
                                    newTexto.AppendLine("\t\tMe.txt" + nameCol + " = New System.Windows.Forms.TextBox()");
                                    break;
                            }
                        }
                    }
                }
            }

            newTexto.AppendLine("\t\tCType(Me.dtgListado, System.ComponentModel.ISupportInitialize).BeginInit()");
            newTexto.AppendLine("\t\tMe.ToolMenu.SuspendLayout()");
            newTexto.AppendLine("\t\tMe.SuspendLayout()");
            // Variables para dibujar
            var yCol = 100;
            var altoIntervalo = 25;
            var xLabels = 14;
            var xControls = 120;
            var xColLab1 = 14;
            var xColCtr1 = 120;
            var xColLab2 = 400;
            var xColCtr2 = 506;
            var jTabIndex = 1;
            for (var i = 0; i
                            <= dtCols.Rows.Count - 1; i++)
            {
                nameCol = dtCols.Rows[i][0].ToString();
                tipoCol = dtCols.Rows[i][1].ToString();
                permiteNull = dtCols.Rows[i]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[i]["EsPK"].ToString().ToUpper() == "YES";
                esAutoGenerada = dtCols.Rows[i]["EsIdentidad"].ToString().ToUpper() == "YES";
                esForeign = dtCols.Rows[i]["EsFK"].ToString() == "Yes";
                if (nameCol != strApiEstado && nameCol != strApiTrans && nameCol != strUsuCre && nameCol != strUsuMod && nameCol != strFecCre && nameCol != strFecMod)
                {
                    if (!esAutoGenerada)
                    {
                        // Texto
                        newTexto.AppendLine("\t\t\'");
                        newTexto.AppendLine("\t\t\'lbl" + nameCol);
                        newTexto.AppendLine("\t\t\'");
                        newTexto.AppendLine("\t\t" + "Me.lbl" + nameCol + ".AutoSize = True");
                        newTexto.AppendLine("\t\t" + "Me.lbl" + nameCol + ".Location = New System.Drawing.Point(" + xLabels + ", " + yCol + 3 + ")");
                        newTexto.AppendLine("\t\t" + "Me.lbl" + nameCol + ".Name = \"lbl" + nameCol + "\"");
                        newTexto.AppendLine("\t\t" + "Me.lbl" + nameCol + ".Size = New System.Drawing.Size(16, 13)");
                        newTexto.AppendLine("\t\t" + "Me.lbl" + nameCol + ".TabIndex = " + jTabIndex + "");
                        jTabIndex = jTabIndex + 1;
                        if (esForeign)
                        {
                            newTexto.AppendLine("\t\tMe.lbl" + nameCol + ".Text = \"" + dtCols.Rows[i]["tablaforanea"] + ":\"");
                            newTexto.AppendLine("\t\t\'");
                            newTexto.AppendLine("\t\t\'cmb" + nameCol);
                            newTexto.AppendLine("\t\t\'");
                            newTexto.AppendLine("\t\tMe.cmb" + nameCol + ".FormattingEnabled = True");
                            newTexto.AppendLine("\t\tMe.cmb" + nameCol + ".Location = New System.Drawing.Point(" + xControls + ", " + yCol + ")");
                            newTexto.AppendLine("\t\tMe.cmb" + nameCol + ".Name = \"cmb" + nameCol + "\"");
                            newTexto.AppendLine("\t\tMe.cmb" + nameCol + ".Size = New System.Drawing.Size(204, 21)");
                            newTexto.AppendLine("\t\tMe.cmb" + nameCol + ".TabIndex = " + jTabIndex + "");
                            newTexto.AppendLine("\t\tMe.cmb" + nameCol + ".DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\tMe.lbl" + nameCol + ".Text = \"" + nameCol.Replace(pNameClase.ToLower(), "") + ":\"");
                            switch (tipoCol.ToUpper())
                            {
                                case "BOOLEAN":
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\t\'chk" + nameCol);
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\tMe.chk" + nameCol + ".AutoSize = True");
                                    newTexto.AppendLine("\t\tMe.chk" + nameCol + ".Location = New System.Drawing.Point(" + xControls + ", " + yCol + ")");
                                    newTexto.AppendLine("\t\tMe.chk" + nameCol + ".Name = \"chk" + nameCol + '\"' + "");
                                    newTexto.AppendLine("\t\tMe.chk" + nameCol + ".Size = New System.Drawing.Size(15,14)");
                                    newTexto.AppendLine("\t\tMe.chk" + nameCol + ".TabIndex = " + jTabIndex + "");
                                    newTexto.AppendLine("\t\tMe.chk" + nameCol + ".UseVisualStyleBackColor = True");
                                    break;

                                case "BOOL":
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\t\'chk" + nameCol);
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\tMe.chk" + nameCol + ".AutoSize = True");
                                    newTexto.AppendLine("\t\tMe.chk" + nameCol + ".Location = New System.Drawing.Point(" + xControls + ", " + yCol + ")");
                                    newTexto.AppendLine("\t\tMe.chk" + nameCol + ".Name = \"chk" + nameCol + '\"' + "");
                                    newTexto.AppendLine("\t\tMe.chk" + nameCol + ".Size = New System.Drawing.Size(15,14)");
                                    newTexto.AppendLine("\t\tMe.chk" + nameCol + ".TabIndex = " + jTabIndex + "");
                                    newTexto.AppendLine("\t\tMe.chk" + nameCol + ".UseVisualStyleBackColor = True");
                                    break;

                                case "STRING":
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\t\'txt" + nameCol);
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\tMe.txt" + nameCol + ".Location = New System.Drawing.Point(" + xControls + ", " + yCol + ")");
                                    newTexto.AppendLine("\t\tMe.txt" + nameCol + ".Name = \"txt" + nameCol + '\"' + "");
                                    newTexto.AppendLine("\t\tMe.txt" + nameCol + ".Size = New System.Drawing.Size(204, 20)");
                                    newTexto.AppendLine("\t\tMe.txt" + nameCol + ".TabIndex = " + jTabIndex + "");
                                    newTexto.AppendLine("\t\tMe.txt" + nameCol + ".MaxLength = " + dtCols.Rows[i]["LONGITUD"]);
                                    break;

                                case "INT":
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\t\'nud" + nameCol);
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Location = New System.Drawing.Point(" + xControls + ", " + yCol + ")");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Name = \"nud" + nameCol + '\"' + "");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Size = New System.Drawing.Size(204, 20)");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".TabIndex = " + jTabIndex + "");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".TextAlign = System.Windows.Forms.HorizontalAlignment.Right");
                                    break;

                                case "INT32":
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\t\'nud" + nameCol);
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Location = New System.Drawing.Point(" + xControls + ", " + yCol + ")");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Name = \"nud" + nameCol + '\"' + "");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Size = New System.Drawing.Size(204, 20)");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".TabIndex = " + jTabIndex + "");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".TextAlign = System.Windows.Forms.HorizontalAlignment.Right");
                                    break;

                                case "INT64":
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\t\'nud" + nameCol);
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Location = New System.Drawing.Point(" + xControls + ", " + yCol + ")");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Name = \"nud" + nameCol + '\"' + "");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Size = New System.Drawing.Size(204, 20)");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".TabIndex = " + jTabIndex + "");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".TextAlign = System.Windows.Forms.HorizontalAlignment.Right");
                                    break;

                                case "INTEGER":
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\t\'nud" + nameCol);
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Location = New System.Drawing.Point(" + xControls + ", " + yCol + ")");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Name = \"nud" + nameCol + '\"' + "");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Size = New System.Drawing.Size(204, 20)");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".TabIndex = " + jTabIndex + "");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".TextAlign = System.Windows.Forms.HorizontalAlignment.Right");
                                    break;

                                case "DECIMAL":
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\t\'nud" + nameCol);
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Location = New System.Drawing.Point(" + xControls + ", " + yCol + ")");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Name = \"nud" + nameCol + '\"' + "");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Size = New System.Drawing.Size(204, 20)");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".TabIndex = " + jTabIndex + "");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".TextAlign = System.Windows.Forms.HorizontalAlignment.Right");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".DecimalPlaces = 2");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Increment = New decimal(New int[] {1,0,0,131072})");
                                    newTexto.AppendLine("\t\tMe.nud" + nameCol + ".Maximum = New decimal(New int[] {9999,0,0,0})");
                                    break;

                                case "DATETIME":
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\t\'dtp" + nameCol);
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\tMe.dtp" + nameCol + ".Format = System.Windows.Forms.DateTimePickerFormat.Short");
                                    newTexto.AppendLine("\t\tMe.dtp" + nameCol + ".Location = New System.Drawing.Point(" + xControls + ", " + yCol + ")");
                                    newTexto.AppendLine("\t\tMe.dtp" + nameCol + ".Name = \"dtp" + nameCol + '\"' + "");
                                    newTexto.AppendLine("\t\tMe.dtp" + nameCol + ".Size = New System.Drawing.Size(204, 20)");
                                    newTexto.AppendLine("\t\tMe.dtp" + nameCol + ".TabIndex = " + jTabIndex + "");
                                    break;

                                case "DATE":
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\t\'dtp" + nameCol);
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\tMe.dtp" + nameCol + ".Format = System.Windows.Forms.DateTimePickerFormat.Short");
                                    newTexto.AppendLine("\t\tMe.dtp" + nameCol + ".Location = New System.Drawing.Point(" + xControls + ", " + yCol + ")");
                                    newTexto.AppendLine("\t\tMe.dtp" + nameCol + ".Name = \"dtp" + nameCol + '\"' + "");
                                    newTexto.AppendLine("\t\tMe.dtp" + nameCol + ".Size = New System.Drawing.Size(204, 20)");
                                    newTexto.AppendLine("\t\tMe.dtp" + nameCol + ".TabIndex = " + jTabIndex + "");
                                    break;

                                default:
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\t\'txt" + nameCol);
                                    newTexto.AppendLine("\t\t\'");
                                    newTexto.AppendLine("\t\tMe.txt" + nameCol + ".Location = New System.Drawing.Point(" + xControls + ", " + yCol + ")");
                                    newTexto.AppendLine("\t\tMe.txt" + nameCol + ".Name = \"txt" + nameCol + '\"' + "");
                                    newTexto.AppendLine("\t\tMe.txt" + nameCol + ".Size = New System.Drawing.Size(204, 20)");
                                    newTexto.AppendLine("\t\tMe.txt" + nameCol + ".TabIndex = " + jTabIndex + "");
                                    break;
                            }
                        }

                        jTabIndex = jTabIndex + 1;
                        xLabels = i % 2
                                  == 0 ? xColLab1 : xColLab2;
                        xControls = i % 2
                                    == 0 ? xColCtr1 : xColCtr2;
                        yCol = i % 2
                               == 0 ? yCol + altoIntervalo : yCol;
                    }
                }
            }

            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\t\'dtgListado");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\tMe.dtgListado.AllowUserToAddRows = False");
            newTexto.AppendLine("\t\tMe.dtgListado.AllowUserToDeleteRows = False");
            newTexto.AppendLine("\t\tMe.dtgListado.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _ ");
            newTexto.AppendLine("\t\t\tOr System.Windows.Forms.AnchorStyles.Left) _");
            newTexto.AppendLine("\t\t\tOr System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)");
            newTexto.AppendLine("\t\tMe.dtgListado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill");
            newTexto.AppendLine("\t\tMe.dtgListado.Location = New System.Drawing.Point(12, " + yCol + 50 + ")");
            newTexto.AppendLine("\t\tMe.dtgListado.MultiSelect = False");
            newTexto.AppendLine("\t\tMe.dtgListado.Name = \"dtgListado\"");
            newTexto.AppendLine("\t\tMe.dtgListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect");
            newTexto.AppendLine("\t\tMe.dtgListado.Size = New System.Drawing.Size(704, " + (460 - yCol + 50 - 12) + ")");
            newTexto.AppendLine("\t\tMe.dtgListado.TabIndex = " + jTabIndex + "");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\t\'btnAgregar");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\tMe.btnAgregar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)");
            newTexto.AppendLine("\t\tMe.btnAgregar.Location = New System.Drawing.Point(641, " + (yCol + 25) + ")");
            newTexto.AppendLine("\t\tMe.btnAgregar.Name = \"btnAgregar" + '\"');
            newTexto.AppendLine("\t\tMe.btnAgregar.Size = New System.Drawing.Size(75, 23)");
            newTexto.AppendLine("\t\tMe.btnAgregar.TabIndex = " + (jTabIndex + 1) + "");
            newTexto.AppendLine("\t\tMe.btnAgregar.Text = \"Agregar\"");
            newTexto.AppendLine("\t\tMe.btnAgregar.UseVisualStyleBackColor = True");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\t\'UsrUsuario1");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\tMe.UsrUsuario1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)");
            newTexto.AppendLine("\t\tMe.UsrUsuario1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D");
            newTexto.AppendLine("\t\tMe.UsrUsuario1.Location = New System.Drawing.Point(577, 26)");
            newTexto.AppendLine("\t\tMe.UsrUsuario1.Margin = New System.Windows.Forms.Padding(1)");
            newTexto.AppendLine("\t\tMe.UsrUsuario1.Name = \"UsrUsuario1\"");
            newTexto.AppendLine("\t\tMe.UsrUsuario1.Size = New System.Drawing.Size(151, 63)");
            newTexto.AppendLine("\t\tMe.UsrUsuario1.TabIndex = " + (jTabIndex + 2) + "");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\t\'lblTitulo");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\tMe.lblTitulo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _");
            newTexto.AppendLine("\t\t\tOr System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)");
            newTexto.AppendLine("\t\tMe.lblTitulo.Font = New System.Drawing.Font(\"Microsoft Sans Serif\", 9.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing." + "FontStyle))");
            newTexto.AppendLine("\t\tMe.lblTitulo.Location = New System.Drawing.Point(12, 26)");
            newTexto.AppendLine("\t\tMe.lblTitulo.Name = \"lblTitulo\"");
            newTexto.AppendLine("\t\tMe.lblTitulo.Size = New System.Drawing.Size(561, 53)");
            newTexto.AppendLine("\t\tMe.lblTitulo.TabIndex = " + (jTabIndex + 3) + "");
            newTexto.AppendLine("\t\tMe.lblTitulo.Text = \"Administraci�n de " + prefijoForms + pNameTabla + '\"' + "");
            newTexto.AppendLine("\t\tMe.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\t\'NewToolStripButton");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\tMe.NewToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image");
            newTexto.AppendLine("\t\tMe.NewToolStripButton.Image = CType(resources.GetObject(\"NewToolStripButton.Image\"), System.Drawing.Image)");
            newTexto.AppendLine("\t\tMe.NewToolStripButton.ImageTransparentColor = System.Drawing.Color.Black");
            newTexto.AppendLine("\t\tMe.NewToolStripButton.Name = \"NewToolStripButton" + '\"' + "");
            newTexto.AppendLine("\t\tMe.NewToolStripButton.Size = New System.Drawing.Size(23, 22)");
            newTexto.AppendLine("\t\tMe.NewToolStripButton.Text = \"Nuevo\"");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\t\'SaveToolStripButton");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\tMe.SaveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image");
            newTexto.AppendLine("\t\tMe.SaveToolStripButton.Image = CType(resources.GetObject(\"SaveToolStripButton.Image\"), System.Drawing.Image)");
            newTexto.AppendLine("\t\tMe.SaveToolStripButton.ImageTransparentColor = System.Drawing.Color.Black");
            newTexto.AppendLine("\t\tMe.SaveToolStripButton.Name = \"SaveToolStripButton\"");
            newTexto.AppendLine("\t\tMe.SaveToolStripButton.Size = New System.Drawing.Size(23, 22)");
            newTexto.AppendLine("\t\tMe.SaveToolStripButton.Text = \"Guardar\"");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\t\'DeleteToolStripButton");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\tMe.DeleteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image");
            newTexto.AppendLine("\t\tMe.DeleteToolStripButton.Image = CType(resources.GetObject(\"ElimnarButton.Image\"), System.Drawing.Image)");
            newTexto.AppendLine("\t\tMe.DeleteToolStripButton.ImageTransparentColor = System.Drawing.Color.Black");
            newTexto.AppendLine("\t\tMe.DeleteToolStripButton.Name = \"DeleteToolStripButton\"");
            newTexto.AppendLine("\t\tMe.DeleteToolStripButton.Size = New System.Drawing.Size(23, 22)");
            newTexto.AppendLine("\t\tMe.DeleteToolStripButton.Text = \"Eliminar seleccionado\"");
            newTexto.AppendLine("\t\tMe.DeleteToolStripButton.ToolTipText = \"Borrar Seleccionados\"");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\t\'ToolStripSeparator1");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\tMe.ToolStripSeparator1.Name = \"ToolStripSeparator1\"");
            newTexto.AppendLine("\t\tMe.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\t\'PrintToolStripButton");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\tMe.PrintToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image");
            newTexto.AppendLine("\t\tMe.PrintToolStripButton.Image = CType(resources.GetObject(\"PrintToolStripButton.Image\"), System.Drawing.Image)");
            newTexto.AppendLine("\t\tMe.PrintToolStripButton.ImageTransparentColor = System.Drawing.Color.Black");
            newTexto.AppendLine("\t\tMe.PrintToolStripButton.Name = \"PrintToolStripButton\"");
            newTexto.AppendLine("\t\tMe.PrintToolStripButton.Size = New System.Drawing.Size(23, 22)");
            newTexto.AppendLine("\t\tMe.PrintToolStripButton.Text = \"Imprimir\"");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\t\'PrintPreviewToolStripButton");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\tMe.PrintPreviewToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image");
            newTexto.AppendLine("\t\tMe.PrintPreviewToolStripButton.Image = CType(resources.GetObject(\"PrintPreviewToolStripButton.Image\"), System.Drawing.Image)");
            newTexto.AppendLine("\t\tMe.PrintPreviewToolStripButton.ImageTransparentColor = System.Drawing.Color.Black");
            newTexto.AppendLine("\t\tMe.PrintPreviewToolStripButton.Name = \"PrintPreviewToolStripButton\"");
            newTexto.AppendLine("\t\tMe.PrintPreviewToolStripButton.Size = New System.Drawing.Size(23, 22)");
            newTexto.AppendLine("\t\tMe.PrintPreviewToolStripButton.Text = \"Auditor�a\"");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\t\'ToolStripSeparator2");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\tMe.ToolStripSeparator2.Name = \"ToolStripSeparator2\"");
            newTexto.AppendLine("\t\tMe.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\t\'SalirStripButton");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\tMe.SalirStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image");
            newTexto.AppendLine("\t\tMe.SalirStripButton.Image = CType(resources.GetObject(\"SalirStripButton.Image\"), System.Drawing.Image)");
            newTexto.AppendLine("\t\tMe.SalirStripButton.ImageTransparentColor = System.Drawing.Color.Magenta");
            newTexto.AppendLine("\t\tMe.SalirStripButton.Name = \"SalirStripButton\"");
            newTexto.AppendLine("\t\tMe.SalirStripButton.Size = New System.Drawing.Size(23, 22)");
            newTexto.AppendLine("\t\tMe.SalirStripButton.Text = \"Salir\"");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\t\'ToolMenu");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\t" + @"Me.ToolMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewToolStripButton, Me.SaveToolStripButton, Me.DeleteToolStripButton, Me.ToolStripSeparator1, Me.PrintToolStripButton, Me.PrintPreviewToolStripButton, Me.ToolStripSeparator2, Me.SalirStripButton})");
            newTexto.AppendLine("\t\tMe.ToolMenu.Location = New System.Drawing.Point(0, 0)");
            newTexto.AppendLine("\t\tMe.ToolMenu.Name = \"ToolMenu\"");
            newTexto.AppendLine("\t\tMe.ToolMenu.Size = New System.Drawing.Size(400, 25)");
            newTexto.AppendLine("\t\tMe.ToolMenu.TabIndex = " + (jTabIndex + 3) + "");
            newTexto.AppendLine("\t\tMe.ToolMenu.Text = \"ToolStrip\"");
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\t" + ("\'" + prefijoForms + pNameClase));
            newTexto.AppendLine("\t\t\'");
            newTexto.AppendLine("\t\tMe.AcceptButton = Me.btnAgregar");
            newTexto.AppendLine("\t\tMe.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)");
            newTexto.AppendLine("\t\tMe.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font");
            newTexto.AppendLine("\t\tMe.ClientSize = New System.Drawing.Size(728, 460)");
            newTexto.AppendLine("\t\tMe.Controls.Add(Me.UsrUsuario1)");
            newTexto.AppendLine("\t\tMe.Controls.Add(Me.lblTitulo)");
            newTexto.AppendLine("\t\tMe.Controls.Add(Me.btnAgregar)");
            newTexto.AppendLine("\t\tMe.Controls.Add(Me.ToolMenu)");
            newTexto.AppendLine("\t\tMe.Controls.Add(Me.dtgListado)");
            for (var i = 0; i
                            <= dtCols.Rows.Count - 1; i++)
            {
                nameCol = dtCols.Rows[i][0].ToString();
                tipoCol = dtCols.Rows[i][1].ToString();
                permiteNull = dtCols.Rows[i]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[i]["EsPK"].ToString().ToUpper() == "YES";
                esAutoGenerada = dtCols.Rows[i]["EsIdentidad"].ToString().ToUpper() == "YES";
                esForeign = dtCols.Rows[i]["EsFK"].ToString() == "Yes";
                if (nameCol != strApiEstado && nameCol != strApiTrans && nameCol != strUsuCre && nameCol != strUsuMod && nameCol != strFecCre && nameCol != strFecMod)
                {
                    newTexto.AppendLine("\t\tMe.Controls.Add(Me.lbl" + nameCol + ")");
                    if (!esAutoGenerada)
                    {
                        if (esForeign)
                        {
                            newTexto.AppendLine("\t\tMe.Controls.Add(Me.cmb" + nameCol + ")");
                        }
                        else
                        {
                            switch (tipoCol.ToUpper())
                            {
                                case "BOOLEAN":
                                    newTexto.AppendLine("\t\tMe.Controls.Add(Me.chk" + nameCol + ")");
                                    break;

                                case "BOOL":
                                    newTexto.AppendLine("\t\tMe.Controls.Add(Me.chk" + nameCol + ")");
                                    break;

                                case "STRING":
                                    newTexto.AppendLine("\t\tMe.Controls.Add(Me.txt" + nameCol + ")");
                                    break;

                                case "INT":
                                    newTexto.AppendLine("\t\tMe.Controls.Add(Me.nud" + nameCol + ")");
                                    break;

                                case "INTEGER":
                                    newTexto.AppendLine("\t\tMe.Controls.Add(Me.nud" + nameCol + ")");
                                    break;

                                case "INT32":
                                    newTexto.AppendLine("\t\tMe.Controls.Add(Me.nud" + nameCol + ")");
                                    break;

                                case "INT64":
                                    newTexto.AppendLine("\t\tMe.Controls.Add(Me.nud" + nameCol + ")");
                                    break;

                                case "DECIMAL":
                                    newTexto.AppendLine("\t\tMe.Controls.Add(Me.nud" + nameCol + ")");
                                    break;

                                case "DATETIME":
                                    newTexto.AppendLine("\t\tMe.Controls.Add(Me.dtp" + nameCol + ")");
                                    break;

                                case "DATE":
                                    newTexto.AppendLine("\t\tMe.Controls.Add(Me.dtp" + nameCol + ")");
                                    break;

                                default:
                                    newTexto.AppendLine("\t\tMe.Controls.Add(Me.txt" + nameCol + ")");
                                    break;
                            }
                        }
                    }
                }
            }

            newTexto.AppendLine("\t\tMe.Name = \"" + prefijoForms + pNameClase + "\"");
            newTexto.AppendLine("\t\tMe.ShowIcon = False");
            newTexto.AppendLine("\t\tMe.Text = \"Administracion de " + pNameTabla + "\"");
            newTexto.AppendLine("\t\tMe.WindowState = System.Windows.Forms.FormWindowState.Maximized");
            newTexto.AppendLine("\t\tCType(Me.dtgListado, System.ComponentModel.ISupportInitialize).EndInit()");
            newTexto.AppendLine("\t\tMe.ToolMenu.ResumeLayout(False)");
            newTexto.AppendLine("\t\tMe.ToolMenu.PerformLayout()");
            newTexto.AppendLine("\t\tMe.ResumeLayout(False)");
            newTexto.AppendLine("\t\tMe.PerformLayout()");
            newTexto.AppendLine("\tEnd Sub");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tFriend WithEvents dtgListado As System.Windows.Forms.DataGridView");
            newTexto.AppendLine("\tFriend WithEvents btnAgregar As System.Windows.Forms.Button");
            newTexto.AppendLine("\tFriend WithEvents UsrUsuario1 As usrUsuario");
            newTexto.AppendLine("\tFriend WithEvents lblTitulo As System.Windows.Forms.Label");
            for (var i = 0; i
                            <= dtCols.Rows.Count - 1; i++)
            {
                nameCol = dtCols.Rows[i][0].ToString();
                tipoCol = dtCols.Rows[i][1].ToString();
                permiteNull = dtCols.Rows[i]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[i]["EsPK"].ToString().ToUpper() == "YES";
                esAutoGenerada = dtCols.Rows[i]["EsIdentidad"].ToString().ToUpper() == "YES";
                if (nameCol != strApiEstado && nameCol != strApiTrans && nameCol != strUsuCre && nameCol != strUsuMod && nameCol != strFecCre && nameCol != strFecMod)
                {
                    newTexto.AppendLine("\t\tFriend WithEvents lbl" + nameCol + " As System.Windows.Forms.Label");
                    if (dtCols.Rows[i]["EsFK"].ToString() == "Yes")
                    {
                        newTexto.AppendLine("\t\tFriend WithEvents cmb" + nameCol + " As System.Windows.Forms.ComboBox");
                    }
                    else if (esAutoGenerada)
                    {
                        newTexto.AppendLine("\t\tFriend WithEvents txt" + nameCol + " As System.Windows.Forms.TextBox");
                    }
                    else
                    {
                        switch (tipoCol.ToUpper())
                        {
                            case "BOOLEAN":
                                newTexto.AppendLine("\t\tFriend WithEvents chk" + nameCol + " As System.Windows.Forms.CheckBox");
                                break;

                            case "BOOL":
                                newTexto.AppendLine("\t\tFriend WithEvents chk" + nameCol + " As System.Windows.Forms.CheckBox");
                                break;

                            case "STRING":
                                newTexto.AppendLine("\t\tFriend WithEvents txt" + nameCol + " As System.Windows.Forms.TextBox");
                                break;

                            case "INT":
                                newTexto.AppendLine("\t\tFriend WithEvents nud" + nameCol + " As System.Windows.Forms.NumericUpDown");
                                break;

                            case "INTEGER":
                                newTexto.AppendLine("\t\tFriend WithEvents nud" + nameCol + " As System.Windows.Forms.NumericUpDown");
                                break;

                            case "INT32":
                                newTexto.AppendLine("\t\tFriend WithEvents nud" + nameCol + " As System.Windows.Forms.NumericUpDown");
                                break;

                            case "INT64":
                                newTexto.AppendLine("\t\tFriend WithEvents nud" + nameCol + " As System.Windows.Forms.NumericUpDown");
                                break;

                            case "DECIMAL":
                                newTexto.AppendLine("\t\tFriend WithEvents nud" + nameCol + " As System.Windows.Forms.NumericUpDown");
                                break;

                            case "DATETIME":
                                newTexto.AppendLine("\t\tFriend WithEvents dtp" + nameCol + " As System.Windows.Forms.DateTimePicker");
                                break;

                            case "DATE":
                                newTexto.AppendLine("\t\tFriend WithEvents dtp" + nameCol + " As System.Windows.Forms.DateTimePicker");
                                break;

                            default:
                                newTexto.AppendLine("\t\tFriend WithEvents txt" + nameCol + " As System.Windows.Forms.TextBox");
                                break;
                        }
                    }
                }
            }

            newTexto.AppendLine("\tFriend WithEvents NewToolStripButton As System.Windows.Forms.ToolStripButton");
            newTexto.AppendLine("\tFriend WithEvents SaveToolStripButton As System.Windows.Forms.ToolStripButton");
            newTexto.AppendLine("\tFriend WithEvents DeleteToolStripButton As System.Windows.Forms.ToolStripButton");
            newTexto.AppendLine("\tFriend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator");
            newTexto.AppendLine("\tFriend WithEvents PrintToolStripButton As System.Windows.Forms.ToolStripButton");
            newTexto.AppendLine("\tFriend WithEvents PrintPreviewToolStripButton As System.Windows.Forms.ToolStripButton");
            newTexto.AppendLine("\tFriend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator");
            newTexto.AppendLine("\tFriend WithEvents SalirStripButton As System.Windows.Forms.ToolStripButton");
            newTexto.AppendLine("\tFriend WithEvents ToolMenu As System.Windows.Forms.ToolStrip");
            newTexto.AppendLine("End Class");
            CFunciones.CrearArchivo(newTexto.ToString(), prefijoForms + pNameClase + ".Designer.vb", PathDesktop + "\\" + namespaceForms + "\\");
            return namespaceForms + "\\" + prefijoForms + pNameClase + "Designer.vb";
        }

        public string CrearCtr(DataTable dtCols, string pNameClase, string pNameTabla)
        {
            if (Directory.Exists(PathDesktop + "\\" + namespaceControls) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + namespaceControls);
            }

            var texto = "";
            var CabeceraTmp = CabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", prefijoWebList + pNameClase);
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que define los metodos y propiedades del Control " + prefijoControls + pNameClase);
            texto = CabeceraTmp + texto;
            texto = texto + "using " + infoNameProyClass + "." + namespaceClases + ";\r\n";
            texto = texto + "using " + infoNameProyClass + "." + namespaceEntidades + ";\r\n";
            texto = texto + "using " + infoNameProyClass + "." + namespaceModelo + ";\r\n";
            texto = texto + "using " + infoNameProyClass + "." + namespaceForms + ";\r\n";
            texto = texto + Resources.WEB_CS_ControlCS;
            var textoArrayPK = "";
            var textoArrayColumnasPK = "";
            var textoArrayValoresPK = "";
            for (var iCol = 0; iCol
                               <= dtCols.Rows.Count - 1; iCol++)
            {
                nameCol = dtCols.Rows[iCol][0].ToString();
                tipoCol = dtCols.Rows[iCol][1].ToString();
                permiteNull = dtCols.Rows[iCol]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES";
                esAutoGenerada = dtCols.Rows[iCol]["EsIdentidad"].ToString().ToUpper() == "YES";
                if (esPK)
                {
                    textoArrayPK = textoArrayPK + "arrPrimaryKeys.Add(dtgListado.SelectedRows[0].Cells[" + ('\"'
                        + (nameCol + ('\"' + ("]);" + ("\r\n" + ("\t\t\t\t" + '\t'))))));
                    textoArrayColumnasPK = textoArrayColumnasPK + "arrColumnasWhere.Add(" + ('\"'
                        + (nameCol + ('\"' + (");" + ("\r\n" + ("\t\t\t\t\t\t"))))));
                    textoArrayValoresPK = textoArrayValoresPK + "arrValoresWhere.Add(dtgListado.SelectedRows[0].Cells[" + ('\"'
                        + (nameCol + ('\"' + ("]);" + ("\r\n" + ("\t\t\t\t\t\t"))))));
                }
            }

            texto = texto.Replace("%NAMESPACE%", infoNameProyClass + "." + namespaceControls);
            texto = texto.Replace("%NAME_CONTROL%", prefijoControls + pNameClase);
            texto = texto.Replace("%MODELO_OBJETO%", prefijoModelo + pNameClase);
            texto = texto.Replace("%OBJETO%", prefijoEntidades + pNameClase);
            texto = texto.Replace("%FORMULARIO%", prefijoForms + pNameClase);
            texto = texto.Replace("%ARRAY_PK%", textoArrayPK);
            texto = texto.Replace("%ARRAY_COLUMNAS_PK%", textoArrayColumnasPK);
            texto = texto.Replace("%ARRAY_VALORES_PK%", textoArrayValoresPK);
            texto = Cabecera + texto;
            CFunciones.CrearArchivo(texto, prefijoControls + pNameClase + ".cs", PathDesktop + "\\" + namespaceControls + "\\");
            return namespaceForms + "\\" + prefijoForms + pNameClase + ".cs";
        }

        public string CrearCtrRecursos(string pNameClase, string pNameTabla)
        {
            var texto = "";
            texto = texto + Resources.WEB_CS_ControlResx;
            CFunciones.CrearArchivo(texto, prefijoControls + pNameClase + ".resx", PathDesktop + "\\" + namespaceControls + "\\");
            return namespaceForms + "\\" + prefijoForms + pNameClase + ".resx";
        }

        public string CrearCtrDesigner(string pNameClase, string pNameTabla)
        {
            var texto = "";
            texto = texto + Resources.WEB_CS_ControlDesigner;
            texto = texto.Replace("%NAMESPACE%", infoNameProyClass + "." + namespaceControls);
            texto = texto.Replace("%NAME_CONTROL%", prefijoControls + pNameClase);
            CFunciones.CrearArchivo(texto, prefijoControls + pNameClase + ".Designer.cs", PathDesktop + "\\" + namespaceControls + "\\");
            return namespaceForms + "\\" + prefijoForms + pNameClase + ".Designer.cs";
        }

        public string CrearFormularioResx(string pNameClase, string pNameTabla)
        {
            CFunciones.CrearArchivo(Resources.WIN_CS_FormResx, prefijoForms + pNameClase + ".resx", PathDesktop + "\\" + namespaceForms + "\\");
            return namespaceForms + "\\" + prefijoForms + pNameClase + ".resx";
        }

        public string CrearSubFormularioResxVB(string pNameClase, string pNameTabla)
        {
            if (bMoverResultados)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + infoNameProyApp;
            }
            else
            {
                PathDesktop = infoPathProyecto + "\\" + infoNameProyApp;
            }

            if (Directory.Exists(PathDesktop))
            {
                Directory.CreateDirectory(PathDesktop);
            }

            CFunciones.CrearArchivo(Resources.WIN_VB_SubFormsResx, prefijoForms + pNameClase + ".resx", PathDesktop + "\\" + namespaceForms + "\\");
            return namespaceForms + "\\" + prefijoForms + pNameClase + ".resx";
        }

        public string CrearDevFormularioDocDesigner(DataTable dtCols, string pNameClase, string pNameTabla)
        {
            prefijoForms = "D";
            if (bMoverResultados)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + infoNameProyApp;
            }
            else
            {
                PathDesktop = infoPathProyecto + "\\" + infoNameProyApp;
            }

            if (Directory.Exists(PathDesktop))
            {
                Directory.CreateDirectory(PathDesktop);
            }

            var strApiTrans = "";
            var strApiEstado = "";
            var strUsuCre = "";
            var strFecCre = "";
            var strUsuMod = "";
            var strFecMod = "";
            for (var iCol = 0; iCol
                               <= dtCols.Rows.Count - 1; iCol++)
            {
                nameCol = dtCols.Rows[iCol][0].ToString();
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                {
                    strUsuCre = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                {
                    strUsuMod = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                {
                    strFecCre = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                {
                    strFecMod = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                {
                    strApiEstado = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                {
                    strApiTrans = nameCol;
                }
            }

            var newTexto = new StringBuilder();
            newTexto.AppendLine("namespace " + infoNameProyApp + "." + namespaceForms);
            newTexto.AppendLine("{");
            newTexto.AppendLine('\t' + ("partial class " + prefijoForms + pNameClase));
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// Required designer variable.");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\tprivate System.ComponentModel.IContainer components = null;");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// Clean up any resources being used.");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t" + ("/// <param name=" + ('\"' + ("disposing" + ('\"' + ">true if managed resources should be disposed; otherwise, false.</param>")))));
            newTexto.AppendLine("\t\tprotected override void Dispose(bool disposing)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (disposing && (components != null))");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tcomponents.Dispose();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tbase.Dispose(disposing);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\t#region Windows Form Designer generated code");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// Required method for Designer support - do not modify");
            newTexto.AppendLine("\t\t/// the contents of this method with the code editor.");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\tprivate void InitializeComponent()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tthis.lblTitulo = new System.Windows.Forms.Label();");
            for (var i = 0; i
                            <= dtCols.Rows.Count - 1; i++)
            {
                nameCol = dtCols.Rows[i][0].ToString();
                tipoCol = dtCols.Rows[i][1].ToString();
                permiteNull = dtCols.Rows[i]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[i]["EsPK"].ToString().ToUpper() == "YES";
                esAutoGenerada = dtCols.Rows[i]["EsIdentidad"].ToString().ToUpper() == "YES";
                esForeign = dtCols.Rows[i]["EsFK"].ToString() == "Yes";
                if (nameCol != strApiEstado && nameCol != strApiTrans && nameCol != strUsuCre && nameCol != strUsuMod && nameCol != strFecCre && nameCol != strFecMod)
                {
                    newTexto.AppendLine("\t\t\t" + ("this.lbl" + nameCol + " = new System.Windows.Forms.Label();"));
                    if (!esAutoGenerada)
                    {
                        if (esForeign)
                        {
                            newTexto.AppendLine("\t\t\t" + ("this.cmb" + nameCol + " = new DevExpress.XtraEditors.LookUpEdit();"));
                        }
                        else
                        {
                            switch (tipoCol.ToUpper())
                            {
                                case "BOOLEAN":
                                    newTexto.AppendLine("\t\t\t" + ("this.chk" + nameCol + " = new DevExpress.XtraEditors.CheckEdit();"));
                                    break;

                                case "BOOL":
                                    newTexto.AppendLine("\t\t\t" + ("this.chk" + nameCol + " = new DevExpress.XtraEditors.CheckEdit();"));
                                    break;

                                case "STRING":
                                    newTexto.AppendLine("\t\t\t" + ("this.txt" + nameCol + " = new DevExpress.XtraEditors.TextEdit();"));
                                    break;

                                case "INT":
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + " = new DevExpress.XtraEditors.TextEdit();"));
                                    break;

                                case "INTEGER":
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + " = new DevExpress.XtraEditors.TextEdit();"));
                                    break;

                                case "INT32":
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + " = new DevExpress.XtraEditors.TextEdit();"));
                                    break;

                                case "INT64":
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + " = new DevExpress.XtraEditors.TextEdit();"));
                                    break;

                                case "DECIMAL":
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + " = new DevExpress.XtraEditors.TextEdit();"));
                                    break;

                                case "DATETIME":
                                    newTexto.AppendLine("\t\t\t" + ("this.dtp" + nameCol + " = new DevExpress.XtraEditors.DateEdit();"));
                                    break;

                                case "DATE":
                                    newTexto.AppendLine("\t\t\t" + ("this.dtp" + nameCol + " = new DevExpress.XtraEditors.DateEdit();"));
                                    break;

                                default:
                                    newTexto.AppendLine("\t\t\t" + ("this.txt" + nameCol + " = new DevExpress.XtraEditors.TextEdit();"));
                                    break;
                            }
                        }
                    }
                }
            }

            newTexto.AppendLine("\t\t\tthis.btnAceptar = new DevExpress.XtraEditors.SimpleButton();");
            newTexto.AppendLine("\t\t\tthis.btnCancelar = new DevExpress.XtraEditors.SimpleButton();");

            for (var i = 0; i
                            <= dtCols.Rows.Count - 1; i++)
            {
                nameCol = dtCols.Rows[i][0].ToString();
                tipoCol = dtCols.Rows[i][1].ToString();
                permiteNull = dtCols.Rows[i]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[i]["EsPK"].ToString().ToUpper() == "YES";
                esAutoGenerada = dtCols.Rows[i]["EsIdentidad"].ToString().ToUpper() == "YES";
                esForeign = dtCols.Rows[i]["EsFK"].ToString() == "Yes";
                if (nameCol != strApiEstado && nameCol != strApiTrans && nameCol != strUsuCre && nameCol != strUsuMod && nameCol != strFecCre && nameCol != strFecMod)
                {
                    if (!esAutoGenerada)
                    {
                        if (esForeign)
                        {
                            newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.cmb" + nameCol + ".Properties)).BeginInit();"));
                        }
                        else
                        {
                            switch (tipoCol.ToUpper())
                            {
                                case "BOOLEAN":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.chk" + nameCol + ".Properties)).BeginInit();"));
                                    break;

                                case "BOOL":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.chk" + nameCol + ".Properties)).BeginInit();"));
                                    break;

                                case "STRING":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.txt" + nameCol + ".Properties)).BeginInit();"));
                                    break;

                                case "INT":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.nud" + nameCol + ".Properties)).BeginInit();"));
                                    break;

                                case "INTEGER":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.nud" + nameCol + ".Properties)).BeginInit();"));
                                    break;

                                case "INT32":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.nud" + nameCol + ".Properties)).BeginInit();"));
                                    break;

                                case "INT64":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.nud" + nameCol + ".Properties)).BeginInit();"));
                                    break;

                                case "DECIMAL":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.nud" + nameCol + ".Properties)).BeginInit();"));
                                    break;

                                case "DATETIME":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.dtp" + nameCol + ".Properties.VistaTimeProperties)).BeginInit();"));
                                    break;

                                case "DATE":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.dtp" + nameCol + ".Properties.VistaTimeProperties)).BeginInit();"));
                                    break;

                                default:
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.txt" + nameCol + ".Properties)).BeginInit();"));
                                    break;
                            }
                        }
                    }
                }
            }

            newTexto.AppendLine("\t\t\tthis.SuspendLayout();");
            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\t// lblTitulo");
            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\tthis.lblTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top " +
                                               "| System.Windows.Forms.AnchorStyles.Left)");
            newTexto.AppendLine("\t\t\t            | System.Windows.Forms.AnchorStyles.Right)));");
            newTexto.AppendLine("\t\t\tthis.lblTitulo.Font = new System.Drawing.Font(" + ('\"' +
                ("Microsoft Sans Serif\", 9.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underl" +
                 "ine))));")));
            newTexto.AppendLine("\t\t\tthis.lblTitulo.Location = new System.Drawing.Point(12, 9);");
            newTexto.AppendLine("\t\t\t" + ("this.lblTitulo.Name = " + ('\"' + ("lblTitulo" + ('\"' + ";")))));
            newTexto.AppendLine("\t\t\tthis.lblTitulo.Size = new System.Drawing.Size(617, 53);");
            newTexto.AppendLine("\t\t\tthis.lblTitulo.TabIndex = 1;");
            newTexto.AppendLine("\t\t\t" + ("this.lblTitulo.Text = " + ('\"' + ("Administracion de " + pNameTabla + ('\"' + ";")))));
            newTexto.AppendLine("\t\t\tthis.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;");
            // Variables para dibujar
            var yCol = 75;
            var altoIntervalo = 25;
            var xLabels = 14;
            var xControls = 120;
            var xColLab1 = 14;
            var xColCtr1 = 120;
            var xColLab2 = 400;
            var xColCtr2 = 506;
            var jTabIndex = 2;
            for (var i = 0; i
                            <= dtCols.Rows.Count - 1; i++)
            {
                nameCol = dtCols.Rows[i][0].ToString();
                tipoCol = dtCols.Rows[i][1].ToString();
                permiteNull = dtCols.Rows[i]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[i]["EsPK"].ToString().ToUpper() == "YES";
                esAutoGenerada = dtCols.Rows[i]["EsIdentidad"].ToString().ToUpper() == "YES";
                esForeign = dtCols.Rows[i]["EsFK"].ToString() == "Yes";
                if (nameCol != strApiEstado && nameCol != strApiTrans && nameCol != strUsuCre && nameCol != strUsuMod && nameCol != strFecCre && nameCol != strFecMod)
                {
                    if (!esAutoGenerada)
                    {
                        jTabIndex = jTabIndex + 1;
                        xLabels = i % 2
                                  == 0 ? xColLab1 : xColLab2;
                        xControls = i % 2
                                    == 0 ? xColCtr1 : xColCtr2;
                        yCol = i % 2
                               == 0 ? yCol + altoIntervalo : yCol;
                        // Texto
                        newTexto.AppendLine("\t\t\t//");
                        newTexto.AppendLine("\t\t\t" + ("// lbl" + nameCol));
                        newTexto.AppendLine("\t\t\t//");
                        newTexto.AppendLine("\t\t\t" + ("this.lbl" + nameCol + ".AutoSize = true;"));
                        newTexto.AppendLine("\t\t\t" + ("this.lbl" + nameCol + ".Location = new System.Drawing.Point(" + (xLabels + (", "
                            + (yCol + 3
                                    + ");")))));
                        newTexto.AppendLine("\t\t\t" + ("this.lbl" + nameCol + ".Name = " + ('\"' + ("lbl" + nameCol + ('\"' + ";")))));
                        newTexto.AppendLine("\t\t\t" + ("this.lbl" + nameCol + ".Size = new System.Drawing.Size(16, 13);"));
                        newTexto.AppendLine("\t\t\t" + ("this.lbl" + nameCol + ".TabIndex = " + (jTabIndex + ";")));
                        jTabIndex = jTabIndex + 1;
                        if (esForeign)
                        {
                            newTexto.AppendLine("\t\t\t" + ("this.lbl" + nameCol + ".Text = " + ('\"'
                                + (dtCols.Rows[i]["tablaforanea"] + ":" + ('\"' + ";")))));
                            newTexto.AppendLine("\t\t\t// ");
                            newTexto.AppendLine("\t\t\t" + ("// cmb" + nameCol));
                            newTexto.AppendLine("\t\t\t// ");
                            newTexto.AppendLine("\t\t\t" + ("this.cmb" + nameCol + ".Location = new System.Drawing.Point(" + (xControls + (", "
                                + (yCol + ");")))));
                            newTexto.AppendLine("\t\t\t" + ("this.cmb" + nameCol + ".Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {"));
                            newTexto.AppendLine("\t\t" + ("\tnew DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Com" +
                                                               "bo)});"));
                            newTexto.AppendLine("\t\t\t" + ("this.cmb" + nameCol + ".Name = " + ('\"' + ("cmb" + nameCol + ('\"' + ";")))));
                            newTexto.AppendLine("\t\t\t" + ("this.cmb" + nameCol + ".Size = new System.Drawing.Size(204, 21);"));
                            newTexto.AppendLine("\t\t\t" + ("this.cmb" + nameCol + ".TabIndex = " + (jTabIndex + ";")));
                        }
                        else
                        {
                            newTexto.AppendLine("\t\t\t" + ("this.lbl" + nameCol + ".Text = " + ('\"'
                                + (nameCol.Replace(pNameClase.ToLower(), "") + ":" + ('\"' + ";")))));
                            switch (tipoCol.ToUpper())
                            {
                                case "BOOLEAN":
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("// chk" + nameCol));
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("this.chk" + nameCol + ".Location = new System.Drawing.Point(" + (xControls + (", "
                                        + (yCol + ");")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.chk" + nameCol + ".Name = " + ('\"' + ("chk" + nameCol + ('\"' + ";")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.chk" + nameCol + ".Properties.Caption = " + ('\"' + '\"' + ";")));
                                    newTexto.AppendLine("\t\t\t" + ("this.chk" + nameCol + ".Size = new System.Drawing.Size(15,14);"));
                                    newTexto.AppendLine("\t\t\t" + ("this.chk" + nameCol + ".TabIndex = " + (jTabIndex + ";")));
                                    break;

                                case "BOOL":
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("// chk" + nameCol));
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("this.chk" + nameCol + ".Location = new System.Drawing.Point(" + (xControls + (", "
                                        + (yCol + ");")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.chk" + nameCol + ".Name = " + ('\"' + ("chk" + nameCol + ('\"' + ";")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.chk" + nameCol + ".Properties.Caption = " + ('\"' + '\"' + ";")));
                                    newTexto.AppendLine("\t\t\t" + ("this.chk" + nameCol + ".Size = new System.Drawing.Size(15,14);"));
                                    newTexto.AppendLine("\t\t\t" + ("this.chk" + nameCol + ".TabIndex = " + (jTabIndex + ";")));
                                    break;

                                case "STRING":
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("// txt" + nameCol));
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("this.txt" + nameCol + ".Location = new System.Drawing.Point(" + (xControls + (", "
                                        + (yCol + ");")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.txt" + nameCol + ".Name = " + ('\"' + ("txt" + nameCol + ('\"' + ";")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.txt" + nameCol + ".Size = new System.Drawing.Size(204, 20);"));
                                    newTexto.AppendLine("\t\t\t" + ("this.txt" + nameCol + ".TabIndex = " + (jTabIndex + ";")));
                                    newTexto.AppendLine("\t\t\t" + ("this.txt" + nameCol + ".Properties.MaxLength = " + dtCols.Rows[i]["LONGITUD"] + ";"));
                                    break;

                                case "INT":
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("// nud" + nameCol));
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Location = new System.Drawing.Point(" + (xControls + (", "
                                        + (yCol + ");")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Name = " + ('\"' + ("nud" + nameCol + ('\"' + ";")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Properties.Mask.EditMask = " + ('\"' + ("n0" + ('\"' + ";")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;"));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Size = new System.Drawing.Size(204, 20);"));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".TabIndex = " + (jTabIndex + ";")));
                                    newTexto.AppendLine("\t\t\t" + ("//this.nud" + nameCol + ".Properties.MaxLength = " + dtCols.Rows[i]["LONGITUD"] + ";"));
                                    break;

                                case "INT32":
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("// nud" + nameCol));
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Location = new System.Drawing.Point(" + (xControls + (", "
                                        + (yCol + ");")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Name = " + ('\"' + ("nud" + nameCol + ('\"' + ";")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Properties.Mask.EditMask = " + ('\"' + ("n0" + ('\"' + ";")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;"));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Size = new System.Drawing.Size(204, 20);"));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".TabIndex = " + (jTabIndex + ";")));
                                    newTexto.AppendLine("\t\t\t" + ("//this.nud" + nameCol + ".Properties.MaxLength = " + dtCols.Rows[i]["LONGITUD"] + ";"));
                                    break;

                                case "INT64":
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("// nud" + nameCol));
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Location = new System.Drawing.Point(" + (xControls + (", "
                                        + (yCol + ");")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Name = " + ('\"' + ("nud" + nameCol + ('\"' + ";")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Properties.Mask.EditMask = " + ('\"' + ("n0" + ('\"' + ";")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;"));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Size = new System.Drawing.Size(204, 20);"));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".TabIndex = " + (jTabIndex + ";")));
                                    newTexto.AppendLine("\t\t\t" + ("//this.nud" + nameCol + ".Properties.MaxLength = " + dtCols.Rows[i]["LONGITUD"] + ";"));
                                    break;

                                case "INTEGER":
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("// nud" + nameCol));
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Location = new System.Drawing.Point(" + (xControls + (", "
                                        + (yCol + ");")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Name = " + ('\"' + ("nud" + nameCol + ('\"' + ";")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Properties.Mask.EditMask = " + ('\"' + ("n0" + ('\"' + ";")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;"));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Size = new System.Drawing.Size(204, 20);"));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".TabIndex = " + (jTabIndex + ";")));
                                    newTexto.AppendLine("\t\t\t" + ("//this.nud" + nameCol + ".Properties.MaxLength = " + dtCols.Rows[i]["LONGITUD"] + ";"));
                                    break;

                                case "DECIMAL":
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("// nud" + nameCol));
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".EditValue = 0;"));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Location = new System.Drawing.Point(" + (xControls + (", "
                                        + (yCol + ");")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Name = " + ('\"' + ("nud" + nameCol + ('\"' + ";")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Properties.Mask.EditMask = " + ('\"' + ("n2" + ('\"' + ";")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;"));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".Size = new System.Drawing.Size(204, 20);"));
                                    newTexto.AppendLine("\t\t\t" + ("this.nud" + nameCol + ".TabIndex = " + (jTabIndex + ";")));
                                    newTexto.AppendLine("\t\t\t" + ("//this.nud" + nameCol + ".Properties.MaxLength = " + dtCols.Rows[i]["LONGITUD"] + ";"));
                                    break;

                                case "DATETIME":
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("// dtp" + nameCol));
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("this.dtp" + nameCol + ".EditValue = null;"));
                                    newTexto.AppendLine("\t\t\t" + ("this.dtp" + nameCol + ".Location = new System.Drawing.Point(" + (xControls + (", "
                                        + (yCol + ");")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.dtp" + nameCol + ".Name = " + ('\"' + ("dtp" + nameCol + ('\"' + ";")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.dtp" + nameCol + ".Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {"));
                                    newTexto.AppendLine("\t\t" + ("\tnew DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Com" +
                                                                       "bo)});"));
                                    newTexto.AppendLine("\t\t\t" + ("this.dtp" + nameCol + ".Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {" + ""));
                                    newTexto.AppendLine("\t\t\tnew DevExpress.XtraEditors.Controls.EditorButton()});");
                                    newTexto.AppendLine("\t\t\t" + ("this.dtp" + nameCol + ".Size = new System.Drawing.Size(204, 20);"));
                                    newTexto.AppendLine("\t\t\t" + ("this.dtp" + nameCol + ".TabIndex = " + (jTabIndex + ";")));
                                    break;

                                case "DATE":
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("// dtp" + nameCol));
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("this.dtp" + nameCol + ".EditValue = null;"));
                                    newTexto.AppendLine("\t\t\t" + ("this.dtp" + nameCol + ".Location = new System.Drawing.Point(" + (xControls + (", "
                                        + (yCol + ");")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.dtp" + nameCol + ".Name = " + ('\"' + ("dtp" + nameCol + ('\"' + ";")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.dtp" + nameCol + ".Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {"));
                                    newTexto.AppendLine("\t\t" + ("\tnew DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Com" +
                                                                       "bo)});"));
                                    newTexto.AppendLine("\t\t\t" + ("this.dtp" + nameCol + ".Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {" + ""));
                                    newTexto.AppendLine("\t\t\tnew DevExpress.XtraEditors.Controls.EditorButton()});");
                                    newTexto.AppendLine("\t\t\t" + ("this.dtp" + nameCol + ".Size = new System.Drawing.Size(204, 20);"));
                                    newTexto.AppendLine("\t\t\t" + ("this.dtp" + nameCol + ".TabIndex = " + (jTabIndex + ";")));
                                    break;

                                default:
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("// txt" + nameCol));
                                    newTexto.AppendLine("\t\t\t//");
                                    newTexto.AppendLine("\t\t\t" + ("this.txt" + nameCol + ".Location = new System.Drawing.Point(" + (xControls + (", "
                                        + (yCol + ");")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.txt" + nameCol + ".Name = " + ('\"' + ("txt" + nameCol + ('\"' + ";")))));
                                    newTexto.AppendLine("\t\t\t" + ("this.txt" + nameCol + ".Size = new System.Drawing.Size(204, 20);"));
                                    newTexto.AppendLine("\t\t\t" + ("this.txt" + nameCol + ".TabIndex = " + (jTabIndex + ";")));
                                    break;
                            }
                        }
                    }
                }
            }

            yCol = yCol + altoIntervalo;
            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\t// btnAceptar");
            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\tthis.btnAceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));");
            newTexto.AppendLine("\t\t\tthis.btnAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;");
            newTexto.AppendLine("\t\t\tthis.btnAceptar.Location = new System.Drawing.Point(126, " + yCol + ");");
            newTexto.AppendLine("\t\t\tthis.btnAceptar.Name = \"btnAceptar\";");
            newTexto.AppendLine("\t\t\tthis.btnAceptar.Size = new System.Drawing.Size(108, 34);");
            newTexto.AppendLine("\t\t\tthis.btnAceptar.TabIndex = " + (jTabIndex + 1) + ";");
            newTexto.AppendLine("\t\t\tthis.btnAceptar.Text = \"&Registrar\";");
            newTexto.AppendLine("\t\t\tthis.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);");
            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\t// btnCancelar");
            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\tthis.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));");
            newTexto.AppendLine("\t\t\tthis.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;");
            newTexto.AppendLine("\t\t\tthis.btnCancelar.Location = new System.Drawing.Point(12, " + yCol + ");");
            newTexto.AppendLine("\t\t\tthis.btnCancelar.Name = \"btnCancelar\";");
            newTexto.AppendLine("\t\t\tthis.btnCancelar.Size = new System.Drawing.Size(108, 34);");
            newTexto.AppendLine("\t\t\tthis.btnCancelar.TabIndex = " + (jTabIndex + 2) + ";");
            newTexto.AppendLine("\t\t\tthis.btnCancelar.Text = \"&Cancelar\";");
            newTexto.AppendLine("\t\t\tthis.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);");
            newTexto.AppendLine("\t\t\t//");
            newTexto.AppendLine("\t\t\t// " + prefijoForms + pNameClase);
            newTexto.AppendLine("\t\t\t//");
            newTexto.AppendLine("\t\t\tthis.AcceptButton = this.btnAceptar;");
            newTexto.AppendLine("\t\t\tthis.CancelButton = this.btnCancelar;");
            newTexto.AppendLine("\t\t\tthis.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);");
            newTexto.AppendLine("\t\t\tthis.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;");
            newTexto.AppendLine("\t\t\tthis.ClientSize = new System.Drawing.Size(784, 562);");
            newTexto.AppendLine("\t\t\tthis.Controls.Add(this.btnAceptar);");
            newTexto.AppendLine("\t\t\tthis.Controls.Add(this.btnCancelar);");
            for (var i = 0; i
                            <= dtCols.Rows.Count - 1; i++)
            {
                nameCol = dtCols.Rows[i][0].ToString();
                tipoCol = dtCols.Rows[i][1].ToString();
                permiteNull = dtCols.Rows[i]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[i]["EsPK"].ToString().ToUpper() == "YES";
                esAutoGenerada = dtCols.Rows[i]["EsIdentidad"].ToString().ToUpper() == "YES";
                esForeign = dtCols.Rows[i]["EsFK"].ToString() == "Yes";
                if (nameCol != strApiEstado && nameCol != strApiTrans && nameCol != strUsuCre && nameCol != strUsuMod && nameCol != strFecCre && nameCol != strFecMod)
                {
                    newTexto.AppendLine("\t\t\t" + ("this.Controls.Add(this.lbl" + nameCol + ");"));
                    if (!esAutoGenerada)
                    {
                        if (esForeign)
                        {
                            newTexto.AppendLine("\t\t\t" + ("this.Controls.Add(this.cmb" + nameCol + ");"));
                        }
                        else
                        {
                            switch (tipoCol.ToUpper())
                            {
                                case "BOOLEAN":
                                    newTexto.AppendLine("\t\t\t" + ("this.Controls.Add(this.chk" + nameCol + ");"));
                                    break;

                                case "BOOL":
                                    newTexto.AppendLine("\t\t\t" + ("this.Controls.Add(this.chk" + nameCol + ");"));
                                    break;

                                case "STRING":
                                    newTexto.AppendLine("\t\t\t" + ("this.Controls.Add(this.txt" + nameCol + ");"));
                                    break;

                                case "INT":
                                    newTexto.AppendLine("\t\t\t" + ("this.Controls.Add(this.nud" + nameCol + ");"));
                                    break;

                                case "INTEGER":
                                    newTexto.AppendLine("\t\t\t" + ("this.Controls.Add(this.nud" + nameCol + ");"));
                                    break;

                                case "INT32":
                                    newTexto.AppendLine("\t\t\t" + ("this.Controls.Add(this.nud" + nameCol + ");"));
                                    break;

                                case "INT64":
                                    newTexto.AppendLine("\t\t\t" + ("this.Controls.Add(this.nud" + nameCol + ");"));
                                    break;

                                case "DECIMAL":
                                    newTexto.AppendLine("\t\t\t" + ("this.Controls.Add(this.nud" + nameCol + ");"));
                                    break;

                                case "DATETIME":
                                    newTexto.AppendLine("\t\t\t" + ("this.Controls.Add(this.dtp" + nameCol + ");"));
                                    break;

                                case "DATE":
                                    newTexto.AppendLine("\t\t\t" + ("this.Controls.Add(this.dtp" + nameCol + ");"));
                                    break;

                                default:
                                    newTexto.AppendLine("\t\t\t" + ("this.Controls.Add(this.txt" + nameCol + ");"));
                                    break;
                            }
                        }
                    }
                }
            }

            newTexto.AppendLine("\t\t\tthis.Controls.Add(this.lblTitulo);");
            newTexto.AppendLine("\t\t\tthis.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;");
            newTexto.AppendLine("\t\t\tthis.MaximizeBox = false;");
            newTexto.AppendLine("\t\t\tthis.MinimizeBox = false;");

            newTexto.AppendLine("\t\t\t" + "this.Name = " + '\"' + prefijoForms + pNameClase + '\"' + ";");
            newTexto.AppendLine("\t\t\tthis.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;");
            newTexto.AppendLine("\t\t\tthis.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;");
            newTexto.AppendLine("\t\t\t" + "this.Text = " + '\"' + "Administracion de " + pNameTabla + '\"' + ";");
            newTexto.AppendLine("\t\t\t" + "this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this." + prefijoForms + pNameClase + "_FormClosing);");
            newTexto.AppendLine("\t\t\t" + "this.Load += new System.EventHandler(this." + prefijoForms + pNameClase + "_Load);");

            newTexto.AppendLine("\t\t\t");
            for (var i = 0; i
                            <= dtCols.Rows.Count - 1; i++)
            {
                nameCol = dtCols.Rows[i][0].ToString();
                tipoCol = dtCols.Rows[i][1].ToString();
                permiteNull = dtCols.Rows[i]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[i]["EsPK"].ToString().ToUpper() == "YES";
                esAutoGenerada = dtCols.Rows[i]["EsIdentidad"].ToString().ToUpper() == "YES";
                esForeign = dtCols.Rows[i]["EsFK"].ToString() == "Yes";
                if (nameCol != strApiEstado && nameCol != strApiTrans && nameCol != strUsuCre && nameCol != strUsuMod && nameCol != strFecCre && nameCol != strFecMod)
                {
                    if (!esAutoGenerada)
                    {
                        if (esForeign)
                        {
                            newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.cmb" + nameCol + ".Properties)).EndInit();"));
                        }
                        else
                        {
                            switch (tipoCol.ToUpper())
                            {
                                case "BOOLEAN":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.chk" + nameCol + ".Properties)).EndInit();"));
                                    break;

                                case "BOOL":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.chk" + nameCol + ".Properties)).EndInit();"));
                                    break;

                                case "STRING":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.txt" + nameCol + ".Properties)).EndInit();"));
                                    break;

                                case "INT":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.nud" + nameCol + ".Properties)).EndInit();"));
                                    break;

                                case "INTEGER":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.nud" + nameCol + ".Properties)).EndInit();"));
                                    break;

                                case "INT32":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.nud" + nameCol + ".Properties)).EndInit();"));
                                    break;

                                case "INT64":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.nud" + nameCol + ".Properties)).EndInit();"));
                                    break;

                                case "DECIMAL":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.nud" + nameCol + ".Properties)).EndInit();"));
                                    break;

                                case "DATETIME":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.dtp" + nameCol + ".Properties.VistaTimeProperties)).EndInit();"));
                                    break;

                                case "DATE":
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.dtp" + nameCol + ".Properties.VistaTimeProperties)).EndInit();"));
                                    break;

                                default:
                                    newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.txt" + nameCol + ".Properties)).EndInit();"));
                                    break;
                            }
                        }
                    }
                }
            }

            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tthis.ResumeLayout(false);");
            newTexto.AppendLine("\t\t\tthis.PerformLayout();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t#endregion");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tinternal System.Windows.Forms.Label lblTitulo;");
            for (var i = 0; i
                            <= dtCols.Rows.Count - 1; i++)
            {
                nameCol = dtCols.Rows[i][0].ToString();
                tipoCol = dtCols.Rows[i][1].ToString();
                permiteNull = dtCols.Rows[i]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[i]["EsPK"].ToString().ToUpper() == "YES";
                esAutoGenerada = dtCols.Rows[i]["EsIdentidad"].ToString().ToUpper() == "YES";
                if (nameCol != strApiEstado && nameCol != strApiTrans && nameCol != strUsuCre && nameCol != strUsuMod && nameCol != strFecCre && nameCol != strFecMod)
                {
                    newTexto.AppendLine("\t\t" + ("internal System.Windows.Forms.Label lbl" + nameCol + ";"));
                    if (dtCols.Rows[i]["EsFK"].ToString() == "Yes")
                    {
                        newTexto.AppendLine("\t\t" + ("private DevExpress.XtraEditors.LookUpEdit cmb" + nameCol + ";"));
                    }
                    else if (esAutoGenerada)
                    {
                        newTexto.AppendLine("\t\t" + ("private DevExpress.XtraEditors.TextEdit txt" + nameCol + ";"));
                    }
                    else
                    {
                        switch (tipoCol.ToUpper())
                        {
                            case "BOOLEAN":
                                newTexto.AppendLine("\t\t" + ("private DevExpress.XtraEditors.CheckEdit chk" + nameCol + ";"));
                                break;

                            case "BOOL":
                                newTexto.AppendLine("\t\t" + ("private DevExpress.XtraEditors.CheckEdit chk" + nameCol + ";"));
                                break;

                            case "STRING":
                                newTexto.AppendLine("\t\t" + ("private DevExpress.XtraEditors.TextEdit txt" + nameCol + ";"));
                                break;

                            case "INT":
                                newTexto.AppendLine("\t\t" + ("private DevExpress.XtraEditors.TextEdit nud" + nameCol + ";"));
                                break;

                            case "INTEGER":
                                newTexto.AppendLine("\t\t" + ("private DevExpress.XtraEditors.TextEdit nud" + nameCol + ";"));
                                break;

                            case "INT32":
                                newTexto.AppendLine("\t\t" + ("private DevExpress.XtraEditors.TextEdit nud" + nameCol + ";"));
                                break;

                            case "INT64":
                                newTexto.AppendLine("\t\t" + ("private DevExpress.XtraEditors.TextEdit nud" + nameCol + ";"));
                                break;

                            case "DECIMAL":
                                newTexto.AppendLine("\t\t" + ("private DevExpress.XtraEditors.TextEdit nud" + nameCol + ";"));
                                break;

                            case "DATETIME":
                                newTexto.AppendLine("\t\t" + ("private DevExpress.XtraEditors.DateEdit dtp" + nameCol + ";"));
                                break;

                            case "DATE":
                                newTexto.AppendLine("\t\t" + ("private DevExpress.XtraEditors.DateEdit dtp" + nameCol + ";"));
                                break;

                            default:
                                newTexto.AppendLine("\t\t" + ("private DevExpress.XtraEditors.TextEdit txt" + nameCol + ";"));
                                break;
                        }
                    }
                }
            }

            newTexto.AppendLine("\t\tprivate DevExpress.XtraEditors.SimpleButton btnAceptar;");
            newTexto.AppendLine("\t\tprivate DevExpress.XtraEditors.SimpleButton btnCancelar;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("}");
            CFunciones.CrearArchivo(newTexto.ToString(), prefijoForms + pNameClase + ".Designer.cs", PathDesktop + "\\" + namespaceForms + "\\");
            return namespaceForms + "\\" + prefijoForms + pNameClase + ".Designer.cs";
        }

        public string CrearDevFormularioDocResx(string pNameClase, string pNameTabla)
        {
            prefijoForms = "D";
            if (bMoverResultados)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + infoNameProyApp;
            }
            else
            {
                PathDesktop = infoPathProyecto + "\\" + infoNameProyApp;
            }

            if (Directory.Exists(PathDesktop))
            {
                Directory.CreateDirectory(PathDesktop);
            }

            CFunciones.CrearArchivo(Resources.WIN_CS_DevExpressFormResx, prefijoForms + pNameClase + ".resx", PathDesktop + "\\" + namespaceForms + "\\");
            return namespaceForms + "\\" + prefijoForms + pNameClase + ".resx";
        }

        public string CrearDevFormularioDoc(DataTable dtCols, string pNameClase, string pNameTabla)
        {
            prefijoForms = "D";
            if (bMoverResultados)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + infoNameProyApp;
            }
            else
            {
                PathDesktop = infoPathProyecto + "\\" + infoNameProyApp;
            }

            if (Directory.Exists(PathDesktop))
            {
                Directory.CreateDirectory(PathDesktop);
            }

            var newTexto = new StringBuilder();
            newTexto.Append(Resources.WIN_CS_DevExpressFormCS_DOC);
            newTexto.Replace("%PAR_NOMBRE%", prefijoWebList + pNameClase);
            newTexto.Replace("%PAR_DESCRIPCION%", "Clase que define los metodos y propiedades del Formulario " + prefijoForms + pNameClase);
            newTexto.Replace("%NAMESPACE%", infoNameProyApp + "." + namespaceForms);
            newTexto.Replace("%NAME_FORM%", prefijoForms + pNameClase);
            newTexto.Replace("%NAME_TABLA%", pNameClase);
            newTexto.Replace("%MODELO_OBJETO%", prefijoModelo + pNameClase);
            newTexto.Replace("%OBJETO%", prefijoEntidades + pNameClase);

            // Armamos las columnas
            var textoColsIns = "";
            var textoColsUpd = "";
            var textoArrayPK = "";
            var textoArrayValoresPK = "";
            var strLimpiarCampos = "";
            var strListadoPK = "";
            var strMyObjListadoPK = "";
            var bEsPrimerElemento = true;
            var strUpdate = "";
            var strUpdateControls = "";
            var strAsignar = "";
            var strValidacionFila = "";
            var strDeletePk = "";
            var strUpdatePk = "";
            var strApiTrans = "";
            var strApiEstado = "";
            var strUsuCre = "";
            var strFecCre = "";
            var strUsuMod = "";
            var strFecMod = "";
            var strFuncionesExtrasDesp = "";
            var strFuncionesExtras = "";
            string TablaForanea;
            var strReadOnlyPK = "";
            var strCargarStrings = "";

            var j = 0;
            for (var iCol = 0; iCol
                               <= dtCols.Rows.Count - 1; iCol++)
            {
                nameCol = dtCols.Rows[iCol][0].ToString();
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                {
                    strUsuCre = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                {
                    strUsuMod = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                {
                    strFecCre = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                {
                    strFecMod = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                {
                    strApiEstado = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                {
                    strApiTrans = nameCol;
                }
            }

            strCargarStrings = strCargarStrings + "\r\n\t\t\tthis.Text = " + prefijoEntidades + pNameClase + ".StrNombreTabla;";
            strCargarStrings = strCargarStrings + "\r\n\t\t\tlblTitulo.Text = " + prefijoEntidades + pNameClase + ".StrNombreTabla;";

            for (var iCol = 0; iCol <= dtCols.Rows.Count - 1; iCol++)
            {
                nameCol = dtCols.Rows[iCol][0].ToString();
                tipoCol = dtCols.Rows[iCol][1].ToString();
                permiteNull = dtCols.Rows[iCol]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES";
                esForeign = dtCols.Rows[iCol]["EsFK"].ToString() == "Yes";
                esAutoGenerada = dtCols.Rows[iCol]["EsIdentidad"].ToString().ToUpper() == "YES";
                TablaForanea = string.IsNullOrEmpty(dtCols.Rows[iCol]["TablaForanea"].ToString()) ? "" : dtCols.Rows[iCol]["TablaForanea"].ToString();
                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        TablaForanea = TablaForanea;
                        break;

                    case TipoBD.SQLServer2000:
                        TablaForanea = TablaForanea;
                        break;

                    case TipoBD.Oracle:
                        if (TablaForanea != "")
                        {
                            TablaForanea = TablaForanea.Substring(0, 1).ToUpper() + TablaForanea.Substring(1, 2).ToLower() + TablaForanea.Substring(3, 1).ToUpper() + TablaForanea.Substring(4).ToLower();
                        }

                        break;
                }
                // Si es Llave Primaria
                if (esPK)
                {
                    if (bEsPrimerElemento)
                    {
                        strListadoPK = tipoCol + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strListadoPK = strListadoPK + ", " + tipoCol + nameCol;
                    }

                    textoArrayPK = textoArrayPK + "arrColumnasWhere.Add(" + '\"' + nameCol + '\"' + ");\r\n\t\t\t";
                    textoArrayValoresPK = textoArrayValoresPK + "arrValoresWhere.Add(_arrPrimaryKeys(" + j + "));\r\n\t\t\t";
                    j = j + 1;
                }

                if (!esAutoGenerada)
                {
                    if (esForeign)
                    {
                        TablaForanea = TablaForanea.Substring(0, 1).ToUpper() + TablaForanea.Substring(1, 2).ToLower() + TablaForanea.Substring(3, 1).ToUpper() + TablaForanea.Substring(4).ToLower();
                        strFuncionesExtras = strFuncionesExtras + "cargarCmb" + nameCol.Replace(pNameClase, "") + "();\r\n\t\t\t";
                        strFuncionesExtrasDesp = strFuncionesExtrasDesp + "\r\n" + ('\t' + ("private void cargarCmb" + nameCol.Replace(pNameClase, "") + "()" + "\r\n" + ("\t\t" + ("{" + "\r\n" + ("\t\t\t" + ("try" + "\r\n" + ("\t\t\t" + ("{" + "\r\n" + ("\t\t\t\t" + ("" + prefijoModelo + TablaForanea + " rn = new " + prefijoModelo + TablaForanea + "();\r\n" + ("\t\t\t\t" + ("DataTable dt = rn.ObtenerDataTable();;\r\n" + ("\t\t\t\t" + ("cmb" + nameCol.Replace(pNameClase, "") + ".Properties.DataSource = dt;\r\n" + ("\t\t\t\t" + ("//cmbidctp.Properties.ValueMember = ;\r\n" + ("\t\t\t" + ("}" + "\r\n" + ("\t\t\t" + ("catch (Exception exp)" + "\r\n" + ("\t\t\t" + ("{" + "\r\n" + ("\t\t\t\t" + ("throw exp;\r\n" + ("\t\t\t" + ("}" + "\r\n" + ("\t\t" + ("}" + "\r\n"))))))))))))))))))))))))))));
                    }

                    // Si NO es llave primaria
                    if (!esPK)
                    {
                        if (nameCol == strApiEstado)
                        {
                        }
                        else if (nameCol == strApiTrans)
                        {
                            textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = CApi.Transaccion.MODIFICAR.ToString();\r\n\t\t\t";
                        }
                        else if (nameCol == strUsuMod)
                        {
                            textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = cParametrosApp.AppUsuario.login;\r\n\t\t\t";
                        }
                        else if (nameCol == strFecMod)
                        {
                        }
                        else if (nameCol == strUsuCre)
                        {
                            textoColsIns = textoColsIns + "obj." + nameCol + " = cParametrosApp.AppUsuario.login;\r\n\t\t\t";
                        }
                        else if (nameCol == strFecCre)
                        {
                        }
                        else
                        {
                            if (esForeign)
                            {
                                strAsignar = strAsignar + "cmb" + nameCol.Replace(pNameClase, "") + ".EditValue = _myObj." + nameCol + ";" + "\r\n\t\t\t";
                                textoColsIns = textoColsIns + "obj." + nameCol + " = rn.GetColumnType(cmb" + nameCol +
                                               ".EditValue, " + prefijoEntidades + pNameClase + ".Fields." + nameCol +
                                               ");" + "\r\n\t\t\t\t\t";
                                textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = int.Parse(cmb" + nameCol +
                                               ".EditValue.ToString());" + "\r\n\t\t\t";
                            }
                            else
                            {
                                switch (tipoCol.ToUpper())
                                {
                                    case "BOOLEAN":
                                        strAsignar = strAsignar + "chk" + nameCol.Replace(pNameClase, "") + ".Checked = _myObj." + nameCol + ";" + "\r\n\t\t\t";
                                        strLimpiarCampos = strLimpiarCampos + "chk" + nameCol.Replace(pNameClase, "") + ".Checked = false;\r\n\t\t\t";
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = chk" + nameCol + ".Checked;\r\n\t\t\t";
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = chk" + nameCol + ".Checked;\r\n\t\t\t";
                                        break;

                                    case "BOOL":
                                        strAsignar = strAsignar + "chk" + nameCol.Replace(pNameClase, "") + ".Checked = _myObj." + nameCol + ";" + "\r\n\t\t\t";
                                        strLimpiarCampos = strLimpiarCampos + "chk" + nameCol.Replace(pNameClase, "") + ".Checked = false;\r\n\t\t\t";
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = chk" + nameCol + ".Checked;\r\n\t\t\t";
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = chk" + nameCol + ".Checked;\r\n\t\t\t";
                                        break;

                                    case "STRING":
                                        strAsignar = strAsignar + "txt" + nameCol.Replace(pNameClase, "") + ".Text = _myObj." + nameCol + ";" + "\r\n\t\t\t";
                                        strLimpiarCampos = strLimpiarCampos + "txt" + nameCol.Replace(pNameClase, "") + ".Text = string.Empty;\r\n\t\t\t";
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = txt" + nameCol + ".Text;\r\n\t\t\t";
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = txt" + nameCol + ".Text;\r\n\t\t\t";
                                        break;

                                    case "INT":
                                        strAsignar = strAsignar + "nud" + nameCol.Replace(pNameClase, "") + ".EditValue = _myObj." + nameCol + ";" + "\r\n\t\t\t";
                                        strLimpiarCampos = strLimpiarCampos + "nud" + nameCol.Replace(pNameClase, "") + ".EditValue = 0;\r\n\t\t\t";
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".EditValue.ToString());\r\n\t\t\t";
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".EditValue.ToString());\r\n\t\t\t";
                                        break;

                                    case "INTEGER":
                                        strAsignar = strAsignar + "nud" + nameCol.Replace(pNameClase, "") + ".EditValue = _myObj." + nameCol + ";" + "\r\n\t\t\t";
                                        strLimpiarCampos = strLimpiarCampos + "nud" + nameCol.Replace(pNameClase, "") + ".EditValue = 0;\r\n\t\t\t";
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".EditValue.ToString()):" + "\r\n\t\t\t";
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".EditValue.ToString());\r\n\t\t\t";
                                        break;

                                    case "INT32":
                                        strAsignar = strAsignar + "nud" + nameCol.Replace(pNameClase, "") + ".EditValue = _myObj." + nameCol + ";" + "\r\n\t\t\t";
                                        strLimpiarCampos = strLimpiarCampos + "nud" + nameCol.Replace(pNameClase, "") + ".EditValue = 0;\r\n\t\t\t";
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".EditValue.ToString());\r\n\t\t\t";
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".EditValue.ToString());\r\n\t\t\t";
                                        break;

                                    case "INT64":
                                        strAsignar = strAsignar + "nud" + nameCol.Replace(pNameClase, "") + ".EditValue = _myObj." + nameCol + ";" + "\r\n\t\t\t";
                                        strLimpiarCampos = strLimpiarCampos + "nud" + nameCol.Replace(pNameClase, "") + ".EditValue = 0;\r\n\t\t\t";
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".EditValue.ToString());\r\n\t\t\t";
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".EditValue.ToString());\r\n\t\t\t";
                                        break;

                                    case "DECIMAL":
                                        strAsignar = strAsignar + "nud" + nameCol.Replace(pNameClase, "") + ".EditValue = _myObj." + nameCol + ";" + "\r\n\t\t\t";
                                        strLimpiarCampos = strLimpiarCampos + "nud" + nameCol.Replace(pNameClase, "") + ".EditValue = 0;\r\n\t\t\t";
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = " + tipoCol + ".Parse(nud" + ('\"' + '\"' + (".EditValue.ToString());\r\n\t\t\t"));
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = " + tipoCol + ".Parse(nud" + nameCol + ".EditValue.ToString());\r\n\t\t\t";
                                        break;

                                    case "DATETIME":
                                        strAsignar = strAsignar + "dtp" + nameCol.Replace(pNameClase, "") + ".Value = _myObj." + nameCol + ";" + "\r\n\t\t\t";
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = dtp" + nameCol + ".Value.Date;\r\n\t\t\t";
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = dtp" + nameCol + ".Value.Date;\r\n\t\t\t";
                                        break;

                                    case "DATE":
                                        strAsignar = strAsignar + "dtp" + nameCol.Replace(pNameClase, "") + ".Value = _myObj." + nameCol + ";" + "\r\n\t\t\t";
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = dtp" + nameCol + ".Value.Date;\r\n\t\t\t";
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = dtp" + nameCol + ".Value.Date;\r\n\t\t\t";
                                        break;

                                    case "BYTE[]":
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = new byte[0];\r\n\t\t\t";
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = new byte[0];\r\n\t\t\t";
                                        break;

                                    default:
                                        strAsignar = strAsignar + "txt" + nameCol.Replace(pNameClase, "") + ".Value = _myObj." + nameCol + ";" + "\r\n\t\t\t";
                                        textoColsIns = textoColsIns + "obj." + nameCol + " = obj." + nameCol + ";\r\n\t\t\t";
                                        textoColsUpd = textoColsUpd + "objExistente." + nameCol + " = objExistente." + nameCol + ";\r\n\t\t\t";
                                        break;
                                }
                            }
                        }
                    }
                }
            }

            newTexto.Replace("%ASIGNAR_VALORES%", strAsignar);
            newTexto.Replace("%PROYECTO_CLASS%", infoNameProyClass);
            newTexto.Replace("%PROYECTO_CLASS_MODELO%", infoNameProyClass + "." + namespaceModelo);
            newTexto.Replace("%PROYECTO_CLASS_ENTIDADES%", infoNameProyClass + "." + namespaceEntidades);
            newTexto.Replace("%LIMPIAR_CAMPOS%", strLimpiarCampos);
            newTexto.Replace("%OBJETO_INSERT%", textoColsIns);
            newTexto.Replace("%ARRAY_PK%", textoArrayPK);
            newTexto.Replace("%ARRAY_VALORES_PK%", textoArrayValoresPK);
            newTexto.Replace("%CLASE_ACCESODATOS%", prefijoModelo + pNameClase);
            newTexto.Replace("%OBJETO_UPDATE%", strUpdate);
            newTexto.Replace("%OBJETO_UPDATE_CONTROLS%", textoColsUpd);
            newTexto.Replace("%OBJETO_DELETE%", strDeletePk);
            newTexto.Replace("%STRING_PK%", strListadoPK);
            newTexto.Replace("%MYOBJ_PK%", strMyObjListadoPK);
            newTexto.Replace("%OBJETO_UPDATE_PK%", strUpdatePk);
            newTexto.Replace("%USU_CRE%", strUsuCre);
            newTexto.Replace("%USU_MOD%", strUsuMod);
            newTexto.Replace("%FEC_CRE%", strFecCre);
            newTexto.Replace("%FEC_MOD%", strFecMod);
            newTexto.Replace("%API_TRANS%", strApiTrans);
            newTexto.Replace("%API_ESTADO%", strApiEstado);
            newTexto.Replace("%FUNCIONES_EXTRAS_DESP%", strFuncionesExtrasDesp);
            newTexto.Replace("%FUNCIONES_EXTRAS%", strFuncionesExtras);
            newTexto.Replace("%READONLY_PK%", strReadOnlyPK);
            newTexto.Replace("%LABEL_TEXT%", strCargarStrings);
            newTexto.Replace("%VALIDACION%", strValidacionFila);

            if (Directory.Exists(PathDesktop + "\\" + namespaceForms) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + namespaceForms);
            }

            CFunciones.CrearArchivo(newTexto.ToString(), prefijoForms + pNameClase + ".cs", PathDesktop + "\\" + namespaceForms + "\\");
            return namespaceForms + "\\" + prefijoForms + pNameClase + ".cs";
        }

        public string CrearDevFormularioListDesigner(DataTable dtCols, string pNameClase, string pNameTabla)
        {
            prefijoForms = "L";
            if (bMoverResultados)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + infoNameProyApp;
            }
            else
            {
                PathDesktop = infoPathProyecto + "\\" + infoNameProyApp;
            }

            if (Directory.Exists(PathDesktop))
            {
                Directory.CreateDirectory(PathDesktop);
            }

            var strApiTrans = "";
            var strApiEstado = "";
            var strUsuCre = "";
            var strFecCre = "";
            var strUsuMod = "";
            var strFecMod = "";
            for (var iCol = 0; iCol
                               <= dtCols.Rows.Count - 1; iCol++)
            {
                nameCol = dtCols.Rows[iCol][0].ToString();
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                {
                    strUsuCre = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                {
                    strUsuMod = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                {
                    strFecCre = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                {
                    strFecMod = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                {
                    strApiEstado = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                {
                    strApiTrans = nameCol;
                }
            }

            var newTexto = new StringBuilder();
            newTexto.AppendLine("namespace " + infoNameProyApp + "." + namespaceForms);
            newTexto.AppendLine("{");
            newTexto.AppendLine('\t' + ("partial class " + prefijoForms + pNameClase));
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// Required designer variable.");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\tprivate System.ComponentModel.IContainer components = null;");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// Clean up any resources being used.");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t" + ("/// <param name=" + ('\"' + ("disposing" + ('\"' + ">true if managed resources should be disposed; otherwise, false.</param>")))));
            newTexto.AppendLine("\t\tprotected override void Dispose(bool disposing)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (disposing && (components != null))");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tcomponents.Dispose();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tbase.Dispose(disposing);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\t#region Windows Form Designer generated code");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// Required method for Designer support - do not modify");
            newTexto.AppendLine("\t\t/// the contents of this method with the code editor.");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\tprivate void InitializeComponent()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t" + ("this.gc" + pNameClase + " = new DevExpress.XtraGrid.GridControl();"));
            newTexto.AppendLine("\t\t\t" + ("this.gv" + pNameClase + " = new DevExpress.XtraGrid.Views.Grid.GridView();"));
            newTexto.AppendLine("\t\t\tthis.lblTitulo = new System.Windows.Forms.Label();");
            newTexto.AppendLine("\t\t\tthis.btnModificar = new DevExpress.XtraEditors.SimpleButton();");
            newTexto.AppendLine("\t\t\tthis.btnAuditoría = new DevExpress.XtraEditors.SimpleButton();");
            newTexto.AppendLine("\t\t\tthis.btnNuevo = new DevExpress.XtraEditors.SimpleButton();");
            newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.gc" + pNameClase + ")).BeginInit();"));
            newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.gv" + pNameClase + ")).BeginInit();"));
            newTexto.AppendLine("\t\t\tthis.SuspendLayout();");
            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\t// lblTitulo");
            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\tthis.lblTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top " +
                                               "| System.Windows.Forms.AnchorStyles.Left)");
            newTexto.AppendLine("\t\t\t            | System.Windows.Forms.AnchorStyles.Right)));");
            newTexto.AppendLine("\t\t\tthis.lblTitulo.Font = new System.Drawing.Font(" + ('\"' +
                ("Microsoft Sans Serif\", 9.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underl" +
                 "ine))));")));
            newTexto.AppendLine("\t\t\tthis.lblTitulo.Location = new System.Drawing.Point(12, 9);");
            newTexto.AppendLine("\t\t\t" + ("this.lblTitulo.Name = " + ('\"' + ("lblTitulo" + ('\"' + ";")))));
            newTexto.AppendLine("\t\t\tthis.lblTitulo.Size = new System.Drawing.Size(617, 53);");
            newTexto.AppendLine("\t\t\tthis.lblTitulo.TabIndex = 1;");
            newTexto.AppendLine("\t\t\t" + ("this.lblTitulo.Text = " + ('\"' + ("Administracion de " + pNameTabla + ('\"' + ";")))));
            newTexto.AppendLine("\t\t\tthis.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;");

            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\t// btnModificar");
            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\tthis.btnModificar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right))); ");
            newTexto.AppendLine("\t\t\tthis.btnModificar.Location = new System.Drawing.Point(551, 517); ");
            newTexto.AppendLine("\t\t\tthis.btnModificar.Name = \"btnModificar\"; ");
            newTexto.AppendLine("\t\t\tthis.btnModificar.Size = new System.Drawing.Size(107, 33); ");
            newTexto.AppendLine("\t\t\tthis.btnModificar.TabIndex = 9; ");
            newTexto.AppendLine("\t\t\tthis.btnModificar.Text = \"&Modificar\"; ");
            newTexto.AppendLine("\t\t\tthis.btnModificar.Click += new System.EventHandler(this.btnModificar_Click); ");

            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\t// btnAuditoría");
            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\tthis.btnAuditoría.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right))); ");
            newTexto.AppendLine("\t\t\tthis.btnAuditoría.DialogResult = System.Windows.Forms.DialogResult.OK; ");
            newTexto.AppendLine("\t\t\tthis.btnAuditoría.Location = new System.Drawing.Point(437, 516); ");
            newTexto.AppendLine("\t\t\tthis.btnAuditoría.Name = \"btnAuditoría\"; ");
            newTexto.AppendLine("\t\t\tthis.btnAuditoría.Size = new System.Drawing.Size(108, 34);");
            newTexto.AppendLine("\t\t\tthis.btnAuditoría.TabIndex = 8;");
            newTexto.AppendLine("\t\t\tthis.btnAuditoría.Text = \"&Auditoría\"; ");
            newTexto.AppendLine("\t\t\tthis.btnAuditoría.Click += new System.EventHandler(this.btnAuditoría_Click); ");

            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\t// btnNuevo");
            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\tthis.btnNuevo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right))); ");
            newTexto.AppendLine("\t\t\tthis.btnNuevo.DialogResult = System.Windows.Forms.DialogResult.OK; ");
            newTexto.AppendLine("\t\t\tthis.btnNuevo.Location = new System.Drawing.Point(664, 516); ");
            newTexto.AppendLine("\t\t\tthis.btnNuevo.Name = \"btnNuevo\"; ");
            newTexto.AppendLine("\t\t\tthis.btnNuevo.Size = new System.Drawing.Size(108, 34); ");
            newTexto.AppendLine("\t\t\tthis.btnNuevo.TabIndex = 10; ");
            newTexto.AppendLine("\t\t\tthis.btnNuevo.Text = \"&Nuevo\"; ");
            newTexto.AppendLine("\t\t\tthis.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click); ");

            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\t" + ("// gc" + pNameClase + ""));
            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\t" + ("this.gc" + pNameClase + ".Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Wind" + "ows.Forms.AnchorStyles.Bottom)"));
            newTexto.AppendLine("\t\t\t            | System.Windows.Forms.AnchorStyles.Left)");
            newTexto.AppendLine("\t\t\t            | System.Windows.Forms.AnchorStyles.Right)));");
            newTexto.AppendLine("\t\t\tthis.gc" + pNameClase + ".Location = new System.Drawing.Point(0, 65);");
            newTexto.AppendLine("\t\t\t" + ("this.gc" + pNameClase + ".MainView = this.gv" + pNameClase + ";"));
            newTexto.AppendLine("\t\t\t" + ("this.gc" + pNameClase + ".Name = " + ('\"' + ("gc" + pNameClase + ('\"' + ";")))));
            newTexto.AppendLine("\t\t\tthis.gc" + pNameClase + ".Size = new System.Drawing.Size(784, 445);");
            newTexto.AppendLine("\t\t\t" + ("this.gc" + pNameClase + ".TabIndex = 0;"));
            newTexto.AppendLine("\t\t\t" + ("this.gc" + pNameClase + ".ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {"));
            newTexto.AppendLine("\t\t\t" + ("this.gv" + pNameClase + "});"));
            newTexto.AppendLine("\t\t\t" + ("this.gc" + pNameClase + ".MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.gc" + pNameClase + "_MouseDoubleClick);"));
            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\t" + ("// gv" + pNameClase + ""));
            newTexto.AppendLine("\t\t\t// ");
            newTexto.AppendLine("\t\t\t" + ("this.gv" + pNameClase + ".GridControl = this.gc" + pNameClase + ";"));
            newTexto.AppendLine("\t\t\t" + ("this.gv" + pNameClase + ".Name = " + ('\"' + ("gv" + pNameClase + ('\"' + ";")))));
            newTexto.AppendLine("\t\t\t" + ("this.gv" + pNameClase + ".InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gv" + pNameClase + "_InitNewRow);"));
            newTexto.AppendLine("\t\t\t" + ("this.gv" + pNameClase + ".RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gv" + pNameClase + "_RowUpdated);"));
            newTexto.AppendLine("\t\t\t" + ("this.gv" + pNameClase + ".KeyDown += new System.Windows.Forms.KeyEventHandler(this.gc" + pNameClase + "_KeyDown);"));
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\t//");
            newTexto.AppendLine("\t\t\t" + ("// " + prefijoForms + pNameClase));
            newTexto.AppendLine("\t\t\t//");
            newTexto.AppendLine("\t\t\tthis.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);");
            newTexto.AppendLine("\t\t\tthis.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;");
            newTexto.AppendLine("\t\t\tthis.ClientSize = new System.Drawing.Size(784, 562);");
            newTexto.AppendLine("\t\t\tthis.Controls.Add(this.btnModificar);");
            newTexto.AppendLine("\t\t\tthis.Controls.Add(this.btnAuditoría);");
            newTexto.AppendLine("\t\t\tthis.Controls.Add(this.btnNuevo);");

            newTexto.AppendLine("\t\t\tthis.Controls.Add(this.lblTitulo);");
            newTexto.AppendLine("\t\t\t" + ("this.Controls.Add(this.gc" + pNameClase + ");"));
            newTexto.AppendLine("\t\t\t" + ("this.Name = " + ('\"'
                                                                        + (prefijoForms + pNameClase + ('\"' + ";")))));
            newTexto.AppendLine("\t\t\t" + ("this.Text = " + ('\"' + ("Administracion de " + pNameTabla + ('\"' + ";")))));
            newTexto.AppendLine("\t\t\t" + ("this.Load += new System.EventHandler(this." + prefijoForms + pNameClase + "_Load);"));
            newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.gc" + pNameClase + ")).EndInit();"));
            newTexto.AppendLine("\t\t\t" + ("((System.ComponentModel.ISupportInitialize)(this.gv" + pNameClase + ")).EndInit();"));
            newTexto.AppendLine("\t\t\t");

            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tthis.ResumeLayout(false);");
            newTexto.AppendLine("\t\t\tthis.PerformLayout();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t#endregion");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t" + ("private DevExpress.XtraGrid.GridControl gc" + pNameClase + ";"));
            newTexto.AppendLine("\t\t" + ("private DevExpress.XtraGrid.Views.Grid.GridView gv" + pNameClase + ";"));
            newTexto.AppendLine("\t\tinternal System.Windows.Forms.Label lblTitulo;");

            newTexto.AppendLine("\t\tprivate DevExpress.XtraEditors.SimpleButton btnModificar;");
            newTexto.AppendLine("\t\tprivate DevExpress.XtraEditors.SimpleButton btnAuditoría;");
            newTexto.AppendLine("\t\tprivate DevExpress.XtraEditors.SimpleButton btnNuevo;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("}");
            CFunciones.CrearArchivo(newTexto.ToString(), prefijoForms + pNameClase + ".Designer.cs", PathDesktop + "\\" + namespaceForms + "\\");
            return namespaceForms + "\\" + prefijoForms + pNameClase + ".Designer.cs";
        }

        public string CrearDevFormularioListResx(string pNameClase, string pNameTabla)
        {
            prefijoForms = "L";
            if (bMoverResultados)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + infoNameProyApp;
            }
            else
            {
                PathDesktop = infoPathProyecto + "\\" + infoNameProyApp;
            }

            if (Directory.Exists(PathDesktop))
            {
                Directory.CreateDirectory(PathDesktop);
            }

            CFunciones.CrearArchivo(Resources.WIN_CS_DevExpressFormResx, prefijoForms + pNameClase + ".resx", PathDesktop + "\\" + namespaceForms + "\\");
            return namespaceForms + "\\" + prefijoForms + pNameClase + ".resx";
        }

        public string CrearDevFormularioList(DataTable dtCols, string pNameClase, string pNameTabla)
        {
            prefijoForms = "L";
            if (bMoverResultados)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + infoNameProyApp;
            }
            else
            {
                PathDesktop = infoPathProyecto + "\\" + infoNameProyApp;
            }

            if (Directory.Exists(PathDesktop))
            {
                Directory.CreateDirectory(PathDesktop);
            }

            var newTexto = new StringBuilder();
            newTexto.Append(Resources.WIN_CS_DevExpressFormCS_LIS);
            newTexto.Replace("%PAR_NOMBRE%", prefijoWebList + pNameClase);
            newTexto.Replace("%PAR_DESCRIPCION%", "Clase que define los metodos y propiedades del Formulario " + prefijoForms + pNameClase);
            newTexto.Replace("%NAMESPACE%", infoNameProyApp + "." + namespaceForms);
            newTexto.Replace("%NAME_FORM%", prefijoForms + pNameClase);
            newTexto.Replace("%NAME_TABLA%", pNameClase);
            newTexto.Replace("%MODELO_OBJETO%", prefijoModelo + pNameClase);
            newTexto.Replace("%OBJETO%", prefijoEntidades + pNameClase);

            // Armamos las columnas
            var textoArrayPK = "";
            var textoArrayValoresPK = "";
            var strListadoPK = "";
            var strMyObjListadoPK = "";
            var bEsPrimerElemento = true;
            var strUpdate = "";
            var strUpdateControls = "";
            var strValidacionFila = "";
            var strDeletePk = "";
            var strUpdatePk = "";
            var strApiTrans = "";
            var strApiEstado = "";
            var strUsuCre = "";
            var strFecCre = "";
            var strUsuMod = "";
            var strFecMod = "";
            string TablaForanea;
            var strReadOnlyPK = "";
            var strCargarStrings = "";

            var j = 0;
            for (var iCol = 0; iCol
                               <= dtCols.Rows.Count - 1; iCol++)
            {
                nameCol = dtCols.Rows[iCol][0].ToString();
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                {
                    strUsuCre = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                {
                    strUsuMod = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                {
                    strFecCre = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                {
                    strFecMod = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                {
                    strApiEstado = nameCol;
                }

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                {
                    strApiTrans = nameCol;
                }
            }

            strCargarStrings = strCargarStrings + "\r\n\t\t\tthis.Text = " + prefijoEntidades + pNameClase + ".StrNombreTabla;";
            strCargarStrings = strCargarStrings + "\r\n\t\t\tlblTitulo.Text = " + prefijoEntidades + pNameClase + ".StrNombreTabla;";

            for (var iCol = 0; iCol <= dtCols.Rows.Count - 1; iCol++)
            {
                nameCol = dtCols.Rows[iCol][0].ToString();
                tipoCol = dtCols.Rows[iCol][1].ToString();
                permiteNull = dtCols.Rows[iCol]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false;
                esPK = dtCols.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES";
                esForeign = dtCols.Rows[iCol]["EsFK"].ToString() == "Yes";
                esAutoGenerada = dtCols.Rows[iCol]["EsIdentidad"].ToString().ToUpper() == "YES";
                TablaForanea = string.IsNullOrEmpty(dtCols.Rows[iCol]["TablaForanea"].ToString()) ? "" : dtCols.Rows[iCol]["TablaForanea"].ToString();
                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        TablaForanea = TablaForanea;
                        break;

                    case TipoBD.SQLServer2000:
                        TablaForanea = TablaForanea;
                        break;

                    case TipoBD.Oracle:
                        if (TablaForanea != "")
                        {
                            TablaForanea = TablaForanea.Substring(0, 1).ToUpper() + TablaForanea.Substring(1, 2).ToLower() + TablaForanea.Substring(3, 1).ToUpper() + TablaForanea.Substring(4).ToLower();
                        }

                        break;
                }
                // Si es Llave Primaria
                if (esPK)
                {
                    if (bEsPrimerElemento)
                    {
                        strListadoPK = tipoCol + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strListadoPK = strListadoPK + ", " + tipoCol + nameCol;
                    }

                    strUpdatePk = strUpdatePk + tipoCol + " " + tipoCol + nameCol + " = row(" + ('\"'
                        + (nameCol + ('\"' + (");\r\n\t\t\t"))));
                    strDeletePk = strDeletePk + tipoCol + " " + tipoCol + nameCol + " = rn.GetColumnType(gv" + pNameClase + ".GetFocusedRowCellValue(" + ('\"'
                        + (nameCol + ('\"' + ("), " + prefijoEntidades + pNameClase + ".Fields." + nameCol + ");" + ("\r\n" + ("\t\t"))))));

                    strReadOnlyPK = strReadOnlyPK + "dtgListado.Columns(" + ('\"'
                                                                             + (nameCol + ('\"' + (").ReadOnly = true;\r\n\t\t\t"))));
                    textoArrayPK = textoArrayPK + "arrColumnasWhere.Add(" + ('\"'
                                                                             + (nameCol + ('\"' + (");\r\n\t\t\t"))));
                    textoArrayValoresPK = textoArrayValoresPK + "arrValoresWhere.Add(_arrPrimaryKeys(" + (j + ("));\r\n\t\t\t"));
                    j = j + 1;
                }

                if (!esAutoGenerada)
                {
                    if (esForeign)
                    {
                        TablaForanea = TablaForanea.Substring(0, 1).ToUpper() + TablaForanea.Substring(1, 2).ToLower() + TablaForanea.Substring(3, 1).ToUpper() + TablaForanea.Substring(4).ToLower();
                    }
                    if (!esPK)
                    {
                        strUpdate = strUpdate + "objExistente." + nameCol + " = rn.GetColumnType(gv" + pNameClase + ".GetFocusedRowCellValue(" + ('\"'
                            + (nameCol + ('\"' + ("), " + prefijoEntidades + pNameClase + ".Fields." + nameCol + ");" + ("\r\n" + ("\t\t"))))));
                    }
                }
            }

            newTexto.Replace("%PROYECTO_CLASS%", infoNameProyClass);
            newTexto.Replace("%PROYECTO_CLASS_MODELO%", infoNameProyClass + "." + namespaceModelo);
            newTexto.Replace("%PROYECTO_CLASS_ENTIDADES%", infoNameProyClass + "." + namespaceEntidades);
            newTexto.Replace("%ARRAY_PK%", textoArrayPK);
            newTexto.Replace("%ARRAY_VALORES_PK%", textoArrayValoresPK);
            newTexto.Replace("%CLASE_ACCESODATOS%", prefijoModelo + pNameClase);
            newTexto.Replace("%OBJETO_UPDATE%", strUpdate);
            newTexto.Replace("%OBJETO_UPDATE_CONTROLS%", strUpdateControls);
            newTexto.Replace("%OBJETO_DELETE%", strDeletePk);
            newTexto.Replace("%STRING_PK%", strListadoPK);
            newTexto.Replace("%MYOBJ_PK%", strMyObjListadoPK);
            newTexto.Replace("%OBJETO_UPDATE_PK%", strUpdatePk);
            newTexto.Replace("%OBJETO_UPDATE_CONTROLS%", strUpdateControls);
            newTexto.Replace("%STRING_PK%", strListadoPK);
            newTexto.Replace("%MYOBJ_PK%", strMyObjListadoPK);
            newTexto.Replace("%USU_CRE%", strUsuCre);
            newTexto.Replace("%USU_MOD%", strUsuMod);
            newTexto.Replace("%FEC_CRE%", strFecCre);
            newTexto.Replace("%FEC_MOD%", strFecMod);
            newTexto.Replace("%API_TRANS%", strApiTrans);
            newTexto.Replace("%API_ESTADO%", strApiEstado);
            newTexto.Replace("%READONLY_PK%", strReadOnlyPK);
            newTexto.Replace("%LABEL_TEXT%", strCargarStrings);
            newTexto.Replace("%VALIDACION%", strValidacionFila);

            if (Directory.Exists(PathDesktop + "\\" + namespaceForms) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + namespaceForms);
            }

            CFunciones.CrearArchivo(newTexto.ToString(), prefijoForms + pNameClase + ".cs", PathDesktop + "\\" + namespaceForms + "\\");
            return namespaceForms + "\\" + prefijoForms + pNameClase + ".cs";
        }
    }
}