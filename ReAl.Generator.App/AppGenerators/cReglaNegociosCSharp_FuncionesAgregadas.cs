﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using System;
using System.Collections;
using System.Data;
using System.Text;

namespace ReAl.Generator.App.AppGenerators
{
    public static class cReglaNegociosCSharp_FuncionesAgregadas
    {
        private static bool SetBoolean = false;
        private static bool SetByte = false;
        private static bool SetDateTime = true;
        private static bool SetDecimal = false;
        private static bool SetDefault = true;
        private static bool SetInt = false;
        private static bool SetInt32 = false;
        private static bool SetString = true;

        public static string CrearModelo(DataTable dtColumns, ref FPrincipal formulario, string pNameTabla, string pNameClaseEnt, string strLlavePrimaria, ArrayList llavePrimaria)
        {
            TipoEntorno miEntorno;
            Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);
            StringBuilder newTexto = new StringBuilder();
            bool bAdicionar = false;
            string nameCol = "";
            string tipoCol = "";

            string paramClaseConeccion = (string.IsNullOrEmpty(formulario.txtClaseConn.Text) ? "cConn" : formulario.txtClaseConn.Text);
            string paramClaseTransaccion = (string.IsNullOrEmpty(formulario.txtClaseTransaccion.Text) ? "cTransaccion" : formulario.txtClaseTransaccion.Text);
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string namespaceClases = (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class");

            string formatoFechaHora = paramClaseParametros + ".parFormatoFechaHora";
            string formatoFecha = paramClaseParametros + ".parFormatoFecha";

            string funcionCargarDataReaderAnd = null;
            string funcionCargarDataReaderOr = null;
            string funcionCargarDataTableAnd = null;
            string funcionCargarDataTableAndFromView = null;
            string funcionCargarDataTableOr = null;

            string strAlias = null;
            try
            {
                DataTable dtPrefijo = new DataTable();

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla.ToUpper() + "')");
                        break;

                    case TipoBD.SQLServer2000:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla.ToUpper() + "')");
                        break;

                    case TipoBD.Oracle:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT ALIASSTA FROM SEGTABLAS WHERE UPPER(TABLASTA) = '" + pNameTabla.ToUpper() + "'");
                        break;

                    case TipoBD.PostgreSQL:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.aliassta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" + pNameTabla.ToUpper() + "')");
                        if (dtPrefijo.Rows.Count == 0)
                        {
                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.alias FROM seg_tablas s WHERE UPPER(s.nombretabla) = UPPER('" + pNameTabla.ToUpper() + "')");
                        }
                        break;
                }

                if (dtPrefijo.Rows.Count > 0)
                {
                    strAlias = dtPrefijo.Rows[0][0].ToString().ToLower();
                    strAlias = strAlias.Substring(0, 1).ToUpper() + strAlias.Substring(1, strAlias.Length - 1);
                }
                else
                {
                    strAlias = pNameTabla;
                }
            }
            catch (Exception ex)
            {
                strAlias = pNameTabla;
            }

            string strAliasSel = "";
            if (formulario.chkUsarSpsSelect.Checked)
            {
                funcionCargarDataReaderAnd = "execStoreProcedureDataReaderSel(" + pNameClaseEnt + ".StrAliasTabla";
                funcionCargarDataReaderOr = "execStoreProcedureDataReaderSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                funcionCargarDataTableAnd = "execStoreProcedureSel(" + pNameClaseEnt + ".StrAliasTabla";
                funcionCargarDataTableAndFromView = "execStoreProcedureSel(strVista";
                funcionCargarDataTableOr = "execStoreProcedureSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                strAliasSel = strAlias;
            }
            else
            {
                if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                    miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                    miEntorno == TipoEntorno.CSharp_WebForms ||
                    miEntorno == TipoEntorno.CSharp_WebAPI ||
                    miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
                {
                    funcionCargarDataReaderAnd = "CargarDataReaderAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataReaderOr = "CargarDataReaderOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAnd = "CargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAndFromView = "CargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + "strVista";
                    funcionCargarDataTableOr = "CargarDataTableOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                }
                else
                {
                    funcionCargarDataReaderAnd = "cargarDataReaderAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataReaderOr = "cargarDataReaderOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAnd = "cargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAndFromView = "cargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "strVista";
                    funcionCargarDataTableOr = "cargarDataTableOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                }
                strAliasSel = pNameTabla;
            }

            if (miEntorno != TipoEntorno.CSharp_WCF)
            {
                //FUNCIONES DE AGREGADO: COUNT
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que devuelve el resultado de la funcion [SQL] COUNT");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo al que se va a aplicar la funcion");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic int FuncionesCount(" + pNameClaseEnt + ".Fields refField)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(1);");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(1);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\treturn FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("");

                //FUNCIONES DE AGREGADO: COUNT
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que devuelve el resultado de la funcion [SQL] COUNT");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo al que se va a aplicar la funcion");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"whereField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Columna que va a filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"System.Object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic int FuncionesCount(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(whereField.ToString());");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(valueField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\treturn FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("");

                //FUNCIONES DE AGREGADO: COUNT con arrays
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que devuelve el resultado de la funcion [SQL] COUNT");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo al que se va a aplicar la funcion");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic int FuncionesCount(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(\"count(\" + refField + \")\");");
                newTexto.AppendLine("\t\t\t\tDataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows.Count == 0)");
                newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"La consulta no ha devuelto resultados.\");");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows.Count > 1)");
                newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"Se ha devuelto mas de una fila.\");");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows[0][0] == null)");
                newTexto.AppendLine("\t\t\t\t\treturn 0;");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows[0][0].ToString() == \"\")");
                newTexto.AppendLine("\t\t\t\t\treturn 0;");
                newTexto.AppendLine("\t\t\t\treturn int.Parse(dtTemp.Rows[0][0].ToString());");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("");

                //FUNCIONES DE AGREGADO: MIN
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que devuelve el resultado de la funcion [SQL] MIN");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo al que se va a aplicar la funcion");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic int FuncionesMin(" + pNameClaseEnt + ".Fields refField)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(1);");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(1);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\treturn FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("");

                //FUNCIONES DE AGREGADO: MIN con FIELDS
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que devuelve el resultado de la funcion [SQL] MIN");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo al que se va a aplicar la funcion");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"whereField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Columna que va a filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"System.Object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic int FuncionesMin(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(whereField.ToString());");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(valueField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\treturn FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("");

                //FUNCIONES DE AGREGADO: MIN con arrays
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que devuelve el resultado de la funcion [SQL] MIN");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo al que se va a aplicar la funcion");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic int FuncionesMin(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(\"min(\" + refField + \")\");");
                newTexto.AppendLine("\t\t\t\tDataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows.Count == 0)");
                newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"La consulta no ha devuelto resultados.\");");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows.Count > 1)");
                newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"Se ha devuelto mas de una fila.\");");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows[0][0] == null)");
                newTexto.AppendLine("\t\t\t\t\treturn 0;");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows[0][0].ToString() == \"\")");
                newTexto.AppendLine("\t\t\t\t\treturn 0;");
                newTexto.AppendLine("\t\t\t\treturn int.Parse(dtTemp.Rows[0][0].ToString());");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("");

                //FUNCIONES DE AGREGADO: MAX
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que devuelve el resultado de la funcion [SQL] MAX");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo al que se va a aplicar la funcion");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic int FuncionesMax(" + pNameClaseEnt + ".Fields refField)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(1);");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(1);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\treturn FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("");

                //FUNCIONES DE AGREGADO: MAX con FIELDS
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que devuelve el resultado de la funcion [SQL] MAX");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo al que se va a aplicar la funcion");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"whereField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Columna que va a filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"System.Object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic int FuncionesMax(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(whereField.ToString());");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(valueField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\treturn FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("");

                //FUNCIONES DE AGREGADO: MAX con arrays
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que devuelve el resultado de la funcion [SQL] MAX");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo al que se va a aplicar la funcion");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic int FuncionesMax(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(\"max(\" + refField + \")\");");
                newTexto.AppendLine("\t\t\t\tDataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows.Count == 0)");
                newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"La consulta no ha devuelto resultados.\");");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows.Count > 1)");
                newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"Se ha devuelto mas de una fila.\");");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows[0][0] == null)");
                newTexto.AppendLine("\t\t\t\t\treturn 0;");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows[0][0].ToString() == \"\")");
                newTexto.AppendLine("\t\t\t\t\treturn 0;");
                newTexto.AppendLine("\t\t\t\treturn int.Parse(dtTemp.Rows[0][0].ToString());");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("");

                //FUNCIONES DE AGREGADO: SUM
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que devuelve el resultado de la funcion [SQL] SUM");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo al que se va a aplicar la funcion");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic int FuncionesSum(" + pNameClaseEnt + ".Fields refField)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(1);");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(1);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\treturn FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("");

                //FUNCIONES DE AGREGADO: SUM con FIELDS
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que devuelve el resultado de la funcion [SQL] SUM");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo al que se va a aplicar la funcion");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"whereField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Columna que va a filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"System.Object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic int FuncionesSum(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(whereField.ToString());");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(valueField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\treturn FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("");

                //FUNCIONES DE AGREGADO: SUM con arrays
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que devuelve el resultado de la funcion [SQL] SUM");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo al que se va a aplicar la funcion");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic int FuncionesSum(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(\"sum(\" + refField + \")\");");
                newTexto.AppendLine("\t\t\t\tDataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows.Count == 0)");
                newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"La consulta no ha devuelto resultados.\");");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows.Count > 1)");
                newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"Se ha devuelto mas de una fila.\");");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows[0][0] == null)");
                newTexto.AppendLine("\t\t\t\t\treturn 0;");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows[0][0].ToString() == \"\")");
                newTexto.AppendLine("\t\t\t\t\treturn 0;");
                newTexto.AppendLine("\t\t\t\treturn int.Parse(dtTemp.Rows[0][0].ToString());");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("");

                //FUNCIONES DE AGREGADO: AVG
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que devuelve el resultado de la funcion [SQL] AVG");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo al que se va a aplicar la funcion");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic int FuncionesAvg(" + pNameClaseEnt + ".Fields refField)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(1);");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(1);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\treturn FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("");

                //FUNCIONES DE AGREGADO: AVG con FIELDS
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que devuelve el resultado de la funcion [SQL] AVG");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo al que se va a aplicar la funcion");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"whereField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Columna que va a filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"System.Object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic int FuncionesAvg(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(whereField.ToString());");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(valueField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\treturn FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("");

                //FUNCIONES DE AGREGADO: AVG con arrays
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que devuelve el resultado de la funcion [SQL] AVG");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo al que se va a aplicar la funcion");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic int FuncionesAvg(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(\"avg(\" + refField + \")\");");
                newTexto.AppendLine("\t\t\t\tDataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows.Count == 0)");
                newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"La consulta no ha devuelto resultados.\");");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows.Count > 1)");
                newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"Se ha devuelto mas de una fila.\");");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows[0][0] == null)");
                newTexto.AppendLine("\t\t\t\t\treturn 0;");
                newTexto.AppendLine("\t\t\t\tif (dtTemp.Rows[0][0].ToString() == \"\")");
                newTexto.AppendLine("\t\t\t\t\treturn 0;");
                newTexto.AppendLine("\t\t\t\treturn int.Parse(dtTemp.Rows[0][0].ToString());");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("");
            }

            return newTexto.ToString();
        }
    }
}