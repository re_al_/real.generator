﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using ReAl.Generator.App.Properties;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace ReAl.Generator.App.AppGenerators
{
    public class cReglaNegociosJava
    {
        public string CrearModeloJavaClasico(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string nameCol = null;
            string tipoCol = null;


            string pNameTabla = pNameClase.Replace("_", "").ToLower();
            //pNameTabla = pNameTabla.Substring(0, 1).ToUpper & pNameTabla.Substring(1).ToLower

            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text +
                                  pNameClase.Replace("_", "").Substring(0, 1).ToUpper() +
                                  pNameClase.Replace("_", "").Substring(1);
            string pNameClaseIn = formulario.txtPrefijoInterface.Text +
                                  pNameClase.Replace("_", "").Substring(0, 1).ToUpper() +
                                  pNameClase.Replace("_", "").Substring(1);
            string pNameClaseRn = formulario.txtPrefijoModelo.Text +
                                  pNameClase.Replace("_", "").Substring(0, 1).ToUpper() +
                                  pNameClase.Replace("_", "").Substring(1);

            bool SetPropertiesInARow = true;
            string PermiteNull = null;
            string TipoColNull = null;
            string DescripcionCol = null;
            bool esPrimerCampo = true;
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace("[tabla]", pNameClase).Replace("_", "").Replace(".", "\\")) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace("[tabla]", pNameClase).Replace("_", "").Replace(".", "\\"));
            }

            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase).Replace("#region", "").Replace("#endregion", "");
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que define un objeto para la Tabla " + pNameTabla);

            //newTexto.AppendLine(CabeceraTmp)
            newTexto.AppendLine("package " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceNegocios.Text.Replace("[tabla]", pNameClase).Replace("_", "") + ";");
            newTexto.AppendLine("");
            newTexto.AppendLine("import " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceInterface.Text.Replace("[tabla]", pNameClase).Replace("_", "") + ";");
            newTexto.AppendLine("import " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase).Replace("_", "") + ";");
            newTexto.AppendLine("import io.swagger.v3.oas.annotations.Operation;");
            newTexto.AppendLine("import io.swagger.v3.oas.annotations.media.Content;");
            newTexto.AppendLine("import io.swagger.v3.oas.annotations.media.Schema;");
            newTexto.AppendLine("import io.swagger.v3.oas.annotations.responses.ApiResponse;");
            newTexto.AppendLine("import io.swagger.v3.oas.annotations.responses.ApiResponses;");
            newTexto.AppendLine("import io.swagger.v3.oas.annotations.security.SecurityRequirement;");
            newTexto.AppendLine("import io.swagger.v3.oas.annotations.tags.Tag;");
            newTexto.AppendLine("import org.apache.commons.logging.Log;");
            newTexto.AppendLine("import org.apache.commons.logging.LogFactory;");
            newTexto.AppendLine("import org.springframework.beans.factory.annotation.Autowired;");
            newTexto.AppendLine("import org.springframework.web.bind.annotation.*;");
            newTexto.AppendLine("import java.util.List;");
            newTexto.AppendLine("");
            newTexto.AppendLine("/*! ");
            newTexto.AppendLine(" *  Controller class for " + pNameTabla);
            newTexto.AppendLine(" *  @author " + formulario.txtInfAutor.Text);
            newTexto.AppendLine(" *  @version 1.0");
            newTexto.AppendLine(" */");
            newTexto.AppendLine("");
            newTexto.AppendLine("@Tag(name = \""+ pNameClase  + "\", description = \"Services for "+ pNameClase  + "\")");
            newTexto.AppendLine("@CrossOrigin(origins = \"*\", maxAge = 3600)");
            newTexto.AppendLine("@RestController");
            newTexto.AppendLine("@RequestMapping(\"/"+ formulario.txtInfProyectoClass.Text.Split('.').Last() + "\")");
            newTexto.AppendLine("public class " + pNameClaseRn + "Controller");
            newTexto.AppendLine("{");
            newTexto.AppendLine("\tprotected final Log logger = LogFactory.getLog(this.getClass());");
            newTexto.AppendLine("\t");            
            newTexto.AppendLine("\t@Autowired");            
            newTexto.AppendLine("\tprivate SecToolsService secToolsService;");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t@Autowired");
            newTexto.AppendLine("\tprivate TransactionService transactionService;");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t@Autowired");
            newTexto.AppendLine("\tprivate "+ pNameClaseIn + "Service thisService;");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t@Operation(summary = \"Get "+ pNameClase  + " list\", security = @SecurityRequirement(name = \"bearerAuth\"))");
            newTexto.AppendLine("\t@ApiResponses(value = {");
            newTexto.AppendLine("\t        @ApiResponse(responseCode = \"200\", description = \"Receipt\", content = {");
            newTexto.AppendLine("\t                @Content(mediaType = \"application/json\", schema = @Schema(implementation = AppResponse.class))");
            newTexto.AppendLine("\t        }),");
            newTexto.AppendLine("\t        @ApiResponse(responseCode = \"500\", description = \"Internal error\", content = @Content)");
            newTexto.AppendLine("\t})");
            newTexto.AppendLine("\t@RequestMapping(value = \"/"+ pNameClase + "\", method = RequestMethod.GET)");
            newTexto.AppendLine("\tpublic AppResponse "+ pNameClase + "GetAll() throws Exception {");
            newTexto.AppendLine("\t\tUser user = secToolsService.getUser();");
            newTexto.AppendLine("\t\tlogger.debug(\"/"+ pNameClase + "\");");
            newTexto.AppendLine("\t\tlogger.debug(\"userId: \" + user.getUserId());");
            newTexto.AppendLine("\t\tAppResponse response = new AppResponse();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\ttry{");
            newTexto.AppendLine("\t\t\tList<"+ pNameClaseEnt + "> res = thisService.getAll();");
            newTexto.AppendLine("\t\t\tresponse.setData(res);");
            newTexto.AppendLine("\t\t} catch(Exception e) {");
            newTexto.AppendLine("\t\t\tExceptionUtil.processException(user.getUsername(), e, null, null);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\treturn response;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t@Operation(summary = \"Get active " + pNameClase + " list\", security = @SecurityRequirement(name = \"bearerAuth\"))");
            newTexto.AppendLine("\t@ApiResponses(value = {");
            newTexto.AppendLine("\t        @ApiResponse(responseCode = \"200\", description = \"Receipt\", content = {");
            newTexto.AppendLine("\t                @Content(mediaType = \"application/json\", schema = @Schema(implementation = AppResponse.class))");
            newTexto.AppendLine("\t        }),");
            newTexto.AppendLine("\t        @ApiResponse(responseCode = \"500\", description = \"Internal error\", content = @Content)");
            newTexto.AppendLine("\t})");
            newTexto.AppendLine("\t@RequestMapping(value = \"/" + pNameClase + "\", method = RequestMethod.GET)");
            newTexto.AppendLine("\tpublic AppResponse " + pNameClase + "GetActive() throws Exception {");
            newTexto.AppendLine("\t\tUser user = secToolsService.getUser();");
            newTexto.AppendLine("\t\tlogger.debug(\"/" + pNameClase + "\");");
            newTexto.AppendLine("\t\tlogger.debug(\"userId: \" + user.getUserId());");
            newTexto.AppendLine("\t\tAppResponse response = new AppResponse();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\ttry{");
            newTexto.AppendLine("\t\t\tList<" + pNameClaseEnt + "> res = thisService.getActive();");
            newTexto.AppendLine("\t\t\tresponse.setData(res);");
            newTexto.AppendLine("\t\t} catch(Exception e) {");
            newTexto.AppendLine("\t\t\tExceptionUtil.processException(user.getUsername(), e, null, null);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\treturn response;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t@Operation(summary = \"Get "+ pNameClase + " by Id\", security = @SecurityRequirement(name = \"bearerAuth\"))");
            newTexto.AppendLine("\t@ApiResponses(value = {");
            newTexto.AppendLine("\t        @ApiResponse(responseCode = \"200\", description = \"\", content = {");
            newTexto.AppendLine("\t                @Content(mediaType = \"application/json\", schema = @Schema(implementation = AppResponse.class))");
            newTexto.AppendLine("\t        }),");
            newTexto.AppendLine("\t        @ApiResponse(responseCode = \"500\", description = \"Internal error.\", content = @Content)");
            newTexto.AppendLine("\t})");
            newTexto.AppendLine("\t@RequestMapping(value = \"/"+ pNameClase + "/{id}\", method = RequestMethod.GET)");
            newTexto.AppendLine("\tpublic AppResponse "+ pNameClase + "GetById(@PathVariable Long id) throws Exception {");
            newTexto.AppendLine("\t\tlogger.debug(\"/"+ pNameClase + "/\" + id);");
            newTexto.AppendLine("\t\tUser user = secToolsService.getUser();");
            newTexto.AppendLine("\t\tAppResponse response = new AppResponse();");
            newTexto.AppendLine("\t\ttry {");
            newTexto.AppendLine("\t\t\t"+ pNameClaseEnt  + " res = thisService.getById(id);");
            newTexto.AppendLine("\t\t\tresponse.setData(res);");
            newTexto.AppendLine("\t\t} catch (Exception e) {");
            newTexto.AppendLine("\t\t\tExceptionUtil.processException(user.getUsername(), e, null, null);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\treturn response;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t@Operation(summary = \"Update "+ pNameClase + "\", security = @SecurityRequirement(name = \"bearerAuth\"))");
            newTexto.AppendLine("\t@ApiResponses(value = {");
            newTexto.AppendLine("\t        @ApiResponse(responseCode = \"200\", description = \""+ pNameClase + " updated\", content = {");
            newTexto.AppendLine("\t                @Content(mediaType = \"application/json\", schema = @Schema(implementation = AppResponse.class))");
            newTexto.AppendLine("\t        }),");
            newTexto.AppendLine("\t        @ApiResponse(responseCode = \"500\", description = \"Internal error\", content = @Content)");
            newTexto.AppendLine("\t})");
            newTexto.AppendLine("\t@RequestMapping(value = \"/"+ pNameClase + "/\", method = RequestMethod.PUT)");
            newTexto.AppendLine("\tpublic AppResponse "+ pNameClase + "Update(@RequestBody "+ pNameClaseEnt + " param) throws Exception {");
            newTexto.AppendLine("\t\tlogger.debug(\"/"+ pNameClase + "/ \" + param);");
            newTexto.AppendLine("\t\tUser user = secToolsService.getUser();");
            newTexto.AppendLine("\t\tAppResponse response = new AppResponse();");
            newTexto.AppendLine("\t\tTransaction transaction = transactionService.beginTransaction(TransactionType."+ pNameClase.ToUpper() + "_UPDATE, param, user.getUserId());");
            newTexto.AppendLine("\t\tTransactionalColumns tran = transaction.getTransactionalColumns();");
            newTexto.AppendLine("\t\ttry {");
            newTexto.AppendLine("\t\t\tthisService.update(param, tran);");
            newTexto.AppendLine("\t\t\ttransactionService.endTransaction(transaction, null);");
            newTexto.AppendLine("\t\t} catch (Exception e) {");
            newTexto.AppendLine("\t\t\tExceptionUtil.processException(user.getUsername(), e, null, null);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\treturn response;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t@Operation (summary = \"Insert "+ pNameClase + "\", security = @SecurityRequirement(name=\"bearerAuth\"))");
            newTexto.AppendLine("\t@ApiResponses(value = {");
            newTexto.AppendLine("\t        @ApiResponse(responseCode = \"200\", description = \""+ pNameClase + " inserted\", content= {@Content(mediaType = \"application/json\", schema = @Schema(implementation = AppResponse.class))}),");
            newTexto.AppendLine("\t        @ApiResponse(responseCode = \"500\", description = \"Internal error.\", content = @Content)");
            newTexto.AppendLine("\t})");
            newTexto.AppendLine("\t@RequestMapping (value = \"/"+ pNameClase + "/\", method= RequestMethod.POST)");
            newTexto.AppendLine("\tpublic AppResponse "+ pNameClase + "Insert(@RequestBody "+ pNameClaseEnt + " param) throws Exception {");
            newTexto.AppendLine("\t\tUser user = secToolsService.getUser();");
            newTexto.AppendLine("\t\tlogger.debug(\"/"+ pNameClase + "/\");");
            newTexto.AppendLine("\t\tAppResponse response = new AppResponse();");
            newTexto.AppendLine("\t\tTransaction transaction = transactionService.beginTransaction(TransactionType."+ pNameClase.ToUpper() + "_INSERT, null, user.getUserId());");
            newTexto.AppendLine("\t\tTransactionalColumns tran = transaction.getTransactionalColumns();");
            newTexto.AppendLine("\t\ttry {");
            newTexto.AppendLine("\t\t\tparam.set"+ pNameClaseEnt + "Id(thisService.insert(param, tran));");
            newTexto.AppendLine("\t\t\tresponse.setData(param);");
            newTexto.AppendLine("\t\t\ttransactionService.endTransaction(transaction, param.toString());");
            newTexto.AppendLine("\t\t} catch(Exception e) {");
            newTexto.AppendLine("\t\t\tExceptionUtil.processException(user.getUsername(), e, null, null);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\treturn response;");
            newTexto.AppendLine("\t}");            
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t@Operation(summary = \"Delete "+ pNameClase + " by Id\", security = @SecurityRequirement(name = \"bearerAuth\"))");
            newTexto.AppendLine("\t@ApiResponses(value = {");
            newTexto.AppendLine("\t        @ApiResponse(responseCode = \"200\", description = \"Success Register Request\",");
            newTexto.AppendLine("\t                content = {@Content(mediaType = \"application/json\", schema = @Schema(implementation = AppResponse.class))}),");
            newTexto.AppendLine("\t        @ApiResponse(responseCode = \"500\", description = \"Internal error.\", content = @Content)");
            newTexto.AppendLine("\t})");
            newTexto.AppendLine("\t@RequestMapping(value = \"/"+ pNameClase + "/DeleteById/{param}\", method = RequestMethod.DELETE)");
            newTexto.AppendLine("\tpublic AppResponse DeleteAccreditationById(@PathVariable Integer param) throws Exception {");
            newTexto.AppendLine("\t\tlogger.info(\"/"+ pNameClase + "/DeleteById/\" + param);");
            newTexto.AppendLine("\t\tAppResponse response = new AppResponse();");
            newTexto.AppendLine("\t\tUser user = secToolsService.getUser();");
            newTexto.AppendLine("\t\tTransaction transaction = transactionService.beginTransaction(TransactionType."+ pNameClase.ToUpper() + "_DELETE, param, user.getUserId());");
            newTexto.AppendLine("\t\tTransactionalColumns tran = transaction.getTransactionalColumns();");
            newTexto.AppendLine("\t\ttry {");
            newTexto.AppendLine("\t\t\tthisService.deleteById(param, tran);");
            newTexto.AppendLine("\t\t} catch (Exception e) {");
            newTexto.AppendLine("\t\t\tExceptionUtil.processException(user.getUsername(), e, null, null);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\treturn response;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), pNameClaseRn + "Controller.java", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace("[tabla]", pNameClase).Replace("_", "").Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace("[tabla]", pNameClase).Replace("_", "").Replace(".", "\\") + "\\" + pNameClaseRn + "Controller.java";
        }

        public string CrearModeloJava(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            string pNameTabla = pNameClase.Replace("_", "");
            pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() +
                         pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
            //pNameTabla = pNameTabla.Substring(0, 1).ToUpper & pNameTabla.Substring(1).ToLower

            pNameClase = formulario.txtPrefijoModelo.Text + pNameTabla;
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameTabla;

            string strLlaves = "";
            string strLlavesQueryObj = "";
            string strLlavesQuery = "";
            string strLlavesArrColWhere = "";
            string strLlavesArrValWhere = "";
            bool bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString();

                    strLlavesArrColWhere = strLlavesArrColWhere + "arrColWhere.add(" + pNameClaseEnt + ".Fields." + nameCol + ".toString());\r\n";
                    strLlavesArrValWhere = strLlavesArrValWhere + "arrValWhere.add(String.valueOf(" + tipoCol + nameCol + "));\r\n";

                    if (bEsPrimerElemento)
                    {
                        strLlaves = strLlaves + tipoCol + " " + tipoCol + nameCol;
                        strLlavesQueryObj = strLlavesQueryObj + " " + pNameClaseEnt + ".Fields." + nameCol + ".toString() + \" = \" + obj.get" + nameCol + "()";
                        strLlavesQuery = strLlavesQuery + " " + pNameClaseEnt + ".Fields." + nameCol + ".toString() + \" = \" + " + tipoCol + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlaves = strLlaves + ", " + tipoCol + " " + tipoCol + nameCol;
                        strLlavesQueryObj = strLlavesQueryObj + "\"  AND \" + " + pNameClaseEnt + ".Fields." + nameCol + ".toString() + \" = \" + obj.get" + nameCol + "()";
                        strLlavesQuery = strLlavesQuery + "\"  AND \" + " + pNameClaseEnt + ".Fields." + nameCol + ".toString() + \" = \" + " + tipoCol + nameCol;
                    }
                }
            }

            //Obtenemos las columnas de APIs
            string strApiEstado = "--";
            string strApiTransaccion = "--";
            string strUsuCre = "--";
            string strFecCre = "--";
            string strUsuMod = "--";
            string strFecMod = "--";

            for (int iCol = 1; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                {
                    strApiEstado = nameCol;
                }
                else if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                {
                    strApiTransaccion = nameCol;
                }
                else if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                {
                    strUsuCre = nameCol;
                }
                else if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                {
                    strUsuMod = nameCol;
                }
                else if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                {
                    strFecCre = nameCol;
                }
                else if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                {
                    strFecMod = nameCol;
                }
            }

            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase).Replace("#region", "").Replace("#endregion", "");
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que implementa los metodos y operaciones sobre la Tabla " + pNameTabla);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            //newTexto.AppendLine(CabeceraTmp)

            newTexto.AppendLine("package " + formulario.txtInfProyectoClass.Text + ";");
            newTexto.AppendLine("");

            newTexto.AppendLine("import java.io.BufferedReader;");
            newTexto.AppendLine("import java.io.BufferedWriter;");
            newTexto.AppendLine("import java.io.DataInputStream;");
            newTexto.AppendLine("import java.io.File;");
            newTexto.AppendLine("import java.io.FileInputStream;");
            newTexto.AppendLine("import java.io.FileNotFoundException;");
            newTexto.AppendLine("import java.io.FileReader;");
            newTexto.AppendLine("import java.io.FileWriter;");
            newTexto.AppendLine("import java.io.IOException;");
            newTexto.AppendLine("import java.io.InputStreamReader;");
            newTexto.AppendLine("");
            newTexto.AppendLine("import java.sql.ResultSet;");
            newTexto.AppendLine("import java.sql.Statement;");
            newTexto.AppendLine("");
            newTexto.AppendLine("import java.util.ArrayList;");
            newTexto.AppendLine("import java.util.Hashtable;");
            newTexto.AppendLine("import java.util.StringTokenizer;");
            newTexto.AppendLine("import java.util.Date;");
            newTexto.AppendLine("");
            newTexto.AppendLine("import java.lang.reflect.Type;");
            newTexto.AppendLine("import java.lang.reflect.InvocationTargetException;");
            newTexto.AppendLine("import java.math.BigDecimal;");
            newTexto.AppendLine("");
            newTexto.AppendLine("import org.apache.commons.beanutils.BeanUtils;");
            newTexto.AppendLine("import org.apache.commons.beanutils.PropertyUtils;");
            newTexto.AppendLine("import org.apache.commons.beanutils.ConvertUtils;");
            newTexto.AppendLine("import org.apache.commons.beanutils.converters.BigDecimalConverter;");
            newTexto.AppendLine("import org.apache.commons.beanutils.converters.IntegerConverter;");
            newTexto.AppendLine("import org.apache.commons.beanutils.converters.DateConverter;");
            newTexto.AppendLine("");
            newTexto.AppendLine("/*! ");
            newTexto.AppendLine(" *  Clase DAL que contienen los métodos para interactuar con la Base de Datos y la Tabla " + pNameTabla);
            newTexto.AppendLine(" *  @author <A href=\"mailto:7.re.al.7@gmail.com\">Reynaldo Alonzo Vera Arias</A>");
            newTexto.AppendLine(" *  @version 1.0");
            newTexto.AppendLine(" */");

            newTexto.AppendLine("public class " + pNameClase);
            newTexto.AppendLine("{");

            newTexto.AppendLine("\tpublic Boolean insert(" + pNameClaseEnt + " obj) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColumnas = new ArrayList<String>();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.add(" + pNameClaseEnt + ".Fields." + nameCol + ".toString());");
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tArrayList<Object> arrValores = new ArrayList<Object>();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrValores.add(obj.get" + nameCol + "());");
            }
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + " myCon = new " + formulario.txtClaseConn.Text + "();");
            newTexto.AppendLine("\t\treturn myCon.insertBD(" + pNameClaseEnt + ".NOMBRE_TABLA, arrColumnas, arrValores);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Boolean update(" + pNameClaseEnt + " obj) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColumnas = new ArrayList<String>();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "NO"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    newTexto.AppendLine("\t\tarrColumnas.add(" + pNameClaseEnt + ".Fields." + nameCol + ".toString());");
                }
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tArrayList<Object> arrValores = new ArrayList<Object>();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "NO"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    newTexto.AppendLine("\t\tarrValores.add(obj.get" + nameCol + "());");
                }
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tArrayList<String> arrColumnasWhere = new ArrayList<String>();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    newTexto.AppendLine("\t\tarrColumnasWhere.add(" + pNameClaseEnt + ".Fields." + nameCol + ".toString());");
                }
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tArrayList<Object> arrValoresWhere = new ArrayList<Object>();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    newTexto.AppendLine("\t\tarrValoresWhere.add(obj.get" + nameCol + "());");
                }
            }
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + " myCon = new " + formulario.txtClaseConn.Text + "();");
            newTexto.AppendLine("\t\treturn myCon.updateBD(" + pNameClaseEnt + ".NOMBRE_TABLA, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Boolean delete(" + pNameClaseEnt + " obj) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColumnasWhere = new ArrayList<String>();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    newTexto.AppendLine("\t\tarrColumnasWhere.add(" + pNameClaseEnt + ".Fields." + nameCol + ".toString());");
                }
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tArrayList<Object> arrValoresWhere = new ArrayList<Object>();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    newTexto.AppendLine("\t\tarrValoresWhere.add(obj.get" + nameCol + "());");
                }
            }
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + " myCon = new " + formulario.txtClaseConn.Text + "();");
            newTexto.AppendLine("\t\treturn myCon.deleteBD(" + pNameClaseEnt + ".NOMBRE_TABLA, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic " + pNameClaseEnt + " obtenerObjeto(" + strLlaves + ") throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\t" + strLlavesArrColWhere);
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\t" + strLlavesArrValWhere);
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn obtenerObjeto(arrColWhere, arrValWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic " + pNameClaseEnt + " obtenerObjeto(" + pNameClaseEnt + ".Fields miField, String miValue) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(miField.toString());");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(miValue);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn obtenerObjeto(arrColWhere, arrValWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic " + pNameClaseEnt + " obtenerObjeto(ArrayList<String> arrColWhere, ArrayList<String> arrValWhere) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\treturn obtenerObjeto(arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic " + pNameClaseEnt + " obtenerObjeto(ArrayList<String> arrColWhere, ArrayList<String> arrValWhere, String strParamAdicionales) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<" + pNameClaseEnt + "> miLista = obtenerLista(arrColWhere, arrValWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tif (miLista.size() == 0)");
            newTexto.AppendLine("\t\t\treturn null;");
            newTexto.AppendLine("\t\telse if (miLista.size() > 1)");
            newTexto.AppendLine("\t\t\tthrow new Exception(\"Se ha devuelto más de un Objeto\");");
            newTexto.AppendLine("\t\telse");
            newTexto.AppendLine("\t\t\treturn miLista.get(0);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic ArrayList<" + pNameClaseEnt + "> obtenerLista() throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(\"1\");");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(\"1\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn obtenerLista(arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic ArrayList<" + pNameClaseEnt + "> obtenerLista(" + pNameClaseEnt + ".Fields miField, String miValue) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(miField.toString());");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(miValue);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn obtenerLista(arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic ArrayList<" + pNameClaseEnt + "> obtenerLista(ArrayList<String> arrColWhere, ArrayList<String> arrValWhere) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\treturn obtenerLista(arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic ArrayList<" + pNameClaseEnt + "> obtenerLista(ArrayList<String> arrColWhere, ArrayList<String> arrValWhere, String strParamAdicionales) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<" + pNameClaseEnt + "> arrLista = new ArrayList<" + pNameClaseEnt + ">(); ");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tArrayList<String> arrColumnas = new ArrayList<String>();");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.add(" + pNameClaseEnt + ".Fields." + nameCol + ".toString());");
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tString strQuery = \"SELECT \" + arrColumnas.toString().replace(\"[\", \"\").replace(\"]\", \"\") + \" FROM \" + " + pNameClaseEnt + ".NOMBRE_TABLA + \" WHERE\";");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tBoolean bPrimerElemento = true;");
            newTexto.AppendLine("\t\tfor (int i = 0; i < arrColWhere.size(); i++)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (bPrimerElemento)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tstrQuery = strQuery + \" \" + arrColWhere.get(i) + \" = \" + arrValWhere.get(i) + \"\";");
            newTexto.AppendLine("\t\t\t\tbPrimerElemento = false;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\telse");
            newTexto.AppendLine("\t\t\t\tstrQuery = strQuery + \" AND \" + arrColWhere.get(i) + \" = \" + arrValWhere.get(i) + \"\";");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tstrQuery = strQuery + \" \" + strParamAdicionales;");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + " miCon = new " + formulario.txtClaseConn.Text + "();");
            newTexto.AppendLine("\t\tStatement st = miCon.myConn.createStatement();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t//Obtenemos el ResultSet");
            newTexto.AppendLine("\t\tResultSet rs = st.executeQuery(strQuery);");
            newTexto.AppendLine("\t\twhile(rs.next())");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tarrLista.add(getObjFromResultSet(rs));");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\trs.close();");
            newTexto.AppendLine("\t\tst.close();");
            newTexto.AppendLine("\t\treturn arrLista;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Hashtable<Integer, " + pNameClaseEnt + "> obtenerHashTable() throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(\"1\");");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(\"1\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn obtenerHashTable(arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Hashtable<Integer, " + pNameClaseEnt + "> obtenerHashTable(" + pNameClaseEnt + ".Fields miField, String miValue) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(miField.toString());");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(miValue);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn obtenerHashTable(arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Hashtable<Integer, " + pNameClaseEnt + "> obtenerHashTable(ArrayList<String> arrColWhere, ArrayList<String> arrValWhere) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\treturn obtenerHashTable(arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Hashtable<Integer, " + pNameClaseEnt + "> obtenerHashTable(ArrayList<String> arrColWhere, ArrayList<String> arrValWhere, String strParamAdicionales) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tHashtable<Integer, " + pNameClaseEnt + "> dicLista = new Hashtable<Integer, " + pNameClaseEnt + ">(); ");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tArrayList<String> arrColumnas = new ArrayList<String>();");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.add(" + pNameClaseEnt + ".Fields." + nameCol + ".toString());");
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tString strQuery = \"SELECT \" + arrColumnas.toString().replace(\"[\", \"\").replace(\"]\", \"\") + \" FROM \" + " + pNameClaseEnt + ".NOMBRE_TABLA + \" WHERE\";");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tBoolean bPrimerElemento = true;");
            newTexto.AppendLine("\t\tfor (int i = 0; i < arrColWhere.size(); i++)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (bPrimerElemento)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tstrQuery = strQuery + \" \" + arrColWhere.get(i) + \" = \" + arrValWhere.get(i) + \"\";");
            newTexto.AppendLine("\t\t\t\tbPrimerElemento = false;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\telse");
            newTexto.AppendLine("\t\t\t\tstrQuery = strQuery + \" AND \" + arrColWhere.get(i) + \" = \" + arrValWhere.get(i) + \"\";");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tstrQuery = strQuery + \" \" + strParamAdicionales;");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + " miCon = new " + formulario.txtClaseConn.Text + "();");
            newTexto.AppendLine("\t\tStatement st = miCon.myConn.createStatement();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t//Obtenemos el ResultSet");
            newTexto.AppendLine("\t\tResultSet rs = st.executeQuery(strQuery);");
            newTexto.AppendLine("\t\twhile(rs.next())");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tdicLista.put(rs.getInt(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ".toString()), getObjFromResultSet(rs));");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\trs.close();");
            newTexto.AppendLine("\t\tst.close();");
            newTexto.AppendLine("\t\treturn dicLista;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Object funcionesMax(" + pNameClaseEnt + ".Fields miMaxField) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(\"1\");");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(\"1\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn funcionesMax(miMaxField, arrColWhere, arrValWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Object funcionesMax(" + pNameClaseEnt + ".Fields miMaxField, " + pNameClaseEnt + ".Fields miField, String miValue) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(miField.toString());");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(miValue);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn funcionesMax(miMaxField, arrColWhere, arrValWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Object funcionesMax(" + pNameClaseEnt + ".Fields miMaxField, ArrayList<String> arrColWhere, ArrayList<String> arrValWhere) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\treturn funcionesMax(miMaxField, arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Object funcionesMax(" + pNameClaseEnt + ".Fields miMaxField, ArrayList<String> arrColWhere, ArrayList<String> arrValWhere, String strParamAdicionales) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tObject idFieldMax = null; ");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tArrayList<String> arrColumnas = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColumnas.add(\"MAX(\" + miMaxField.toString() + \")\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tString strQuery = \"SELECT \" + arrColumnas.toString().replace(\"[\", \"\").replace(\"]\", \"\") + \" FROM \" + " + pNameClaseEnt + ".NOMBRE_TABLA + \" WHERE\";");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tBoolean bPrimerElemento = true;");
            newTexto.AppendLine("\t\tfor (int i = 0; i < arrColWhere.size(); i++)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (bPrimerElemento)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tstrQuery = strQuery + \" \" + arrColWhere.get(i) + \" = \" + arrValWhere.get(i) + \"\";");
            newTexto.AppendLine("\t\t\t\tbPrimerElemento = false;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\telse");
            newTexto.AppendLine("\t\t\t\tstrQuery = strQuery + \" AND \" + arrColWhere.get(i) + \" = \" + arrValWhere.get(i) + \"\";");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tstrQuery = strQuery + \" \" + strParamAdicionales;");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + " miCon = new " + formulario.txtClaseConn.Text + "();");
            newTexto.AppendLine("\t\tStatement st = miCon.myConn.createStatement();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t//Obtenemos el ResultSet");
            newTexto.AppendLine("\t\tResultSet rs = st.executeQuery(strQuery);");
            newTexto.AppendLine("\t\twhile(rs.next())");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tidFieldMax = rs.getObject(0);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\trs.close();");
            newTexto.AppendLine("\t\tst.close();");
            newTexto.AppendLine("\t\treturn idFieldMax;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Object funcionesMin(" + pNameClaseEnt + ".Fields miMinField) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(\"1\");");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(\"1\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn funcionesMin(miMinField, arrColWhere, arrValWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Object funcionesMin(" + pNameClaseEnt + ".Fields miMinField, " + pNameClaseEnt + ".Fields miField, String miValue) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(miField.toString());");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(miValue);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn funcionesMin(miMinField, arrColWhere, arrValWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Object funcionesMin(" + pNameClaseEnt + ".Fields miMinField, ArrayList<String> arrColWhere, ArrayList<String> arrValWhere) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\treturn funcionesMin(miMinField, arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Object funcionesMin(" + pNameClaseEnt + ".Fields miMinField, ArrayList<String> arrColWhere, ArrayList<String> arrValWhere, String strParamAdicionales) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tObject idFieldMax = null; ");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tArrayList<String> arrColumnas = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColumnas.add(\"MAX(\" + miMinField.toString() + \")\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tString strQuery = \"SELECT \" + arrColumnas.toString().replace(\"[\", \"\").replace(\"]\", \"\") + \" FROM \" + " + pNameClaseEnt + ".NOMBRE_TABLA + \" WHERE\";");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tBoolean bPrimerElemento = true;");
            newTexto.AppendLine("\t\tfor (int i = 0; i < arrColWhere.size(); i++)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (bPrimerElemento)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tstrQuery = strQuery + \" \" + arrColWhere.get(i) + \" = \" + arrValWhere.get(i) + \"\";");
            newTexto.AppendLine("\t\t\t\tbPrimerElemento = false;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\telse");
            newTexto.AppendLine("\t\t\t\tstrQuery = strQuery + \" AND \" + arrColWhere.get(i) + \" = \" + arrValWhere.get(i) + \"\";");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tstrQuery = strQuery + \" \" + strParamAdicionales;");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + " miCon = new " + formulario.txtClaseConn.Text + "();");
            newTexto.AppendLine("\t\tStatement st = miCon.myConn.createStatement();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t//Obtenemos el ResultSet");
            newTexto.AppendLine("\t\tResultSet rs = st.executeQuery(strQuery);");
            newTexto.AppendLine("\t\twhile(rs.next())");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tidFieldMax = rs.getObject(0);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\trs.close();");
            newTexto.AppendLine("\t\tst.close();");
            newTexto.AppendLine("\t\treturn idFieldMax;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic int funcionesSum(" + pNameClaseEnt + ".Fields miSumField) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(\"1\");");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(\"1\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn funcionesSum(miSumField, arrColWhere, arrValWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic int funcionesSum(" + pNameClaseEnt + ".Fields miSumField, " + pNameClaseEnt + ".Fields miField, String miValue) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(miField.toString());");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(miValue);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn funcionesSum(miSumField, arrColWhere, arrValWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic int funcionesSum(" + pNameClaseEnt + ".Fields miSumField, ArrayList<String> arrColWhere, ArrayList<String> arrValWhere) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\treturn funcionesSum(miSumField, arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic int funcionesSum(" + pNameClaseEnt + ".Fields miSumField, ArrayList<String> arrColWhere, ArrayList<String> arrValWhere, String strParamAdicionales) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tint idFieldMax = 0; ");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tArrayList<String> arrColumnas = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColumnas.add(\"SUM(\" + miSumField.toString() + \")\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tString strQuery = \"SELECT \" + arrColumnas.toString().replace(\"[\", \"\").replace(\"]\", \"\") + \" FROM \" + " + pNameClaseEnt + ".NOMBRE_TABLA + \" WHERE\";");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tBoolean bPrimerElemento = true;");
            newTexto.AppendLine("\t\tfor (int i = 0; i < arrColWhere.size(); i++)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (bPrimerElemento)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tstrQuery = strQuery + \" \" + arrColWhere.get(i) + \" = \" + arrValWhere.get(i) + \"\";");
            newTexto.AppendLine("\t\t\t\tbPrimerElemento = false;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\telse");
            newTexto.AppendLine("\t\t\t\tstrQuery = strQuery + \" AND \" + arrColWhere.get(i) + \" = \" + arrValWhere.get(i) + \"\";");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tstrQuery = strQuery + \" \" + strParamAdicionales;");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + " miCon = new " + formulario.txtClaseConn.Text + "();");
            newTexto.AppendLine("\t\tStatement st = miCon.myConn.createStatement();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t//Obtenemos el ResultSet");
            newTexto.AppendLine("\t\tResultSet rs = st.executeQuery(strQuery);");
            newTexto.AppendLine("\t\twhile(rs.next())");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tidFieldMax = rs.getInt(0);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\trs.close();");
            newTexto.AppendLine("\t\tst.close();");
            newTexto.AppendLine("\t\treturn idFieldMax;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic int funcionesCount(" + pNameClaseEnt + ".Fields miCountField) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(\"1\");");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(\"1\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn funcionesCount(miCountField, arrColWhere, arrValWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic int funcionesCount(" + pNameClaseEnt + ".Fields miCountField, " + pNameClaseEnt + ".Fields miField, String miValue) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(miField.toString());");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(miValue);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn funcionesCount(miCountField, arrColWhere, arrValWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic int funcionesCount(" + pNameClaseEnt + ".Fields miCountField, ArrayList<String> arrColWhere, ArrayList<String> arrValWhere) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\treturn funcionesCount(miCountField, arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic int funcionesCount(" + pNameClaseEnt + ".Fields miCountField, ArrayList<String> arrColWhere, ArrayList<String> arrValWhere, String strParamAdicionales) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tint idFieldMax = 0; ");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tArrayList<String> arrColumnas = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColumnas.add(\"SUM(\" + miCountField.toString() + \")\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tString strQuery = \"SELECT \" + arrColumnas.toString().replace(\"[\", \"\").replace(\"]\", \"\") + \" FROM \" + " + pNameClaseEnt + ".NOMBRE_TABLA + \" WHERE\";");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tBoolean bPrimerElemento = true;");
            newTexto.AppendLine("\t\tfor (int i = 0; i < arrColWhere.size(); i++)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (bPrimerElemento)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tstrQuery = strQuery + \" \" + arrColWhere.get(i) + \" = \" + arrValWhere.get(i) + \"\";");
            newTexto.AppendLine("\t\t\t\tbPrimerElemento = false;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\telse");
            newTexto.AppendLine("\t\t\t\tstrQuery = strQuery + \" AND \" + arrColWhere.get(i) + \" = \" + arrValWhere.get(i) + \"\";");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tstrQuery = strQuery + \" \" + strParamAdicionales;");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + " miCon = new " + formulario.txtClaseConn.Text + "();");
            newTexto.AppendLine("\t\tStatement st = miCon.myConn.createStatement();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t//Obtenemos el ResultSet");
            newTexto.AppendLine("\t\tResultSet rs = st.executeQuery(strQuery);");
            newTexto.AppendLine("\t\twhile(rs.next())");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tidFieldMax = rs.getInt(0);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\trs.close();");
            newTexto.AppendLine("\t\tst.close();");
            newTexto.AppendLine("\t\treturn idFieldMax;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            if (1 == 1)
            {
                newTexto.AppendLine("\t@SuppressWarnings(\"unchecked\")");
                newTexto.AppendLine("\tprivate <T> T cast(Object miValue, " + pNameClaseEnt + ".Fields miField) throws ClassNotFoundException, IllegalAccessException, InvocationTargetException, NoSuchMethodException");
                newTexto.AppendLine("\t{");
                newTexto.AppendLine("\t\tString clazz = PropertyUtils.getPropertyType(new " + pNameClaseEnt + "(), miField.toString()).getName();");
                newTexto.AppendLine("\t\treturn (T) Class.forName(clazz).cast(miValue);");
                newTexto.AppendLine("\t}");
                newTexto.AppendLine("\t");
                newTexto.AppendLine("\tprivate " + pNameClaseEnt + " getObjFromResultSet(ResultSet miCursor) throws Exception");
                newTexto.AppendLine("\t{");
                newTexto.AppendLine("\t\t//KB: http://livinginjava.blogspot.com/2011/03/how-to-configure-way-beanutils-converts.html");
                newTexto.AppendLine("\t\tConvertUtils.register(new IntegerConverter(null), Integer.class);");
                newTexto.AppendLine("\t\tConvertUtils.register(new BigDecimalConverter(null), BigDecimal.class);");
                newTexto.AppendLine("\t\tConvertUtils.register(new DateConverter(null), Date.class);");
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " obj = new " + pNameClaseEnt + "(); ");
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    newTexto.AppendLine("\t\tBeanUtils.setProperty(obj, " + pNameClaseEnt + ".Fields." + nameCol + ".toString(),cast(miCursor.getObject(" + pNameClaseEnt + ".Fields." + nameCol + ".toString()), " + pNameClaseEnt + ".Fields." + nameCol + "));");
                }
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\treturn obj;");
                newTexto.AppendLine("\t}");
            }
            else
            {
                newTexto.AppendLine("\tprivate " + pNameClaseEnt + " getObjFromResultSet(ResultSet miCursor) throws Exception");
                newTexto.AppendLine("\t{");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " obj = new " + pNameClaseEnt + "(); ");
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString();
                    switch (tipoCol.ToUpper())
                    {
                        case "INT":
                            newTexto.AppendLine("\t\tobj.set" + nameCol + "(miCursor.getInt(" + pNameClaseEnt + ".Fields." + nameCol + ".toString()));");
                            break;

                        case "FLOAT":
                            newTexto.AppendLine("\t\tobj.set" + nameCol + "(miCursor.getFloat(" + pNameClaseEnt + ".Fields." + nameCol + ".toString()));");
                            break;

                        case "STRING":
                            newTexto.AppendLine("\t\tobj.set" + nameCol + "(miCursor.getString(" + pNameClaseEnt + ".Fields." + nameCol + ".toString()));");
                            break;

                        case "BYTE[]":
                            newTexto.AppendLine("\t\tobj.set" + nameCol + "(miCursor.getString" + pNameClaseEnt + ".Fields." + nameCol + ".toString()));");
                            break;

                        case "LONG":
                            newTexto.AppendLine("\t\tobj.set" + nameCol + "(miCursor.getLong(" + pNameClaseEnt + ".Fields." + nameCol + ".toString()));");
                            break;

                        case "DATE":
                            newTexto.AppendLine("\t\tobj.set" + nameCol + "(miCursor.getDate(" + pNameClaseEnt + ".Fields." + nameCol + ".toString()));");
                            break;

                        default:
                            newTexto.AppendLine("\t\tobj.set" + nameCol + "(miCursor.getObject(" + pNameClaseEnt + ".Fields." + nameCol + ".toString())));");
                            break;
                    }
                }
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\treturn obj;");
                newTexto.AppendLine("\t}");
            }

            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), pNameClase + ".java", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\" + pNameClase + ".java";
        }

        public string crearcParams(ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();

            newTexto.AppendLine("package " + formulario.txtInfProyectoClass.Text + ";");
            newTexto.AppendLine("");
            newTexto.AppendLine("public final class cParametros");
            newTexto.AppendLine("{");
            newTexto.AppendLine("\tpublic static final String N_SERVER = \"" + Principal.Servidor + "\";");
            newTexto.AppendLine("\tpublic static final String N_PUERTO = \"" + Principal.Puerto + "\";");
            newTexto.AppendLine("\tpublic static final String N_BD_USER = \"" + Principal.UsuarioBD + "\";");
            newTexto.AppendLine("\tpublic static final String N_BD_PASS = \"" + Principal.PasswordBD + "\";");
            newTexto.AppendLine("\tpublic static final String N_BD_NAME = \"" + Principal.Catalogo + "\";");
            newTexto.AppendLine("\tpublic static final String N_BD_SCHEMA = \"\";");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), "cParametros.java", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\cParametros.java";
        }

        public string crearConn(ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            string newTexto = Resources.WIN_JAVA_cConn;

            newTexto = newTexto.Replace("%PAQUETE%", formulario.txtInfProyectoClass.Text);
            newTexto = newTexto.Replace("%CLASS_CONN%", formulario.txtClaseConn.Text);

            CFunciones.CrearArchivo(newTexto.ToString(), formulario.txtClaseConn.Text + ".java", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\" + formulario.txtClaseConn.Text + ".java";
        }
    }
}