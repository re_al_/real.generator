﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using System;
using System.Data;
using System.IO;

namespace ReAl.Generator.App.AppGenerators
{
    public class cReglaNegociosPHPCodeIgniterModelsAPI
    {
        public string CrearControllerPhpCodeIgniterModelsAPI(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }
            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\")) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\"));
            }

            string pNameTablaAux = pNameClase;
            string pNameTabla = pNameClase.Replace("_", "");
            dynamic pNameClaseRn = formulario.txtPrefijoModelo.Text +
                                    pNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                    pNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                    pNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                    pNameTabla.Replace("_", "").Substring(4).ToLower();
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text +
                                    pNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                    pNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                    pNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                    pNameTabla.Replace("_", "").Substring(4).ToLower() +
                                    "Model";

            switch (Principal.DBMS)
            {
                case TipoBD.SQLServer:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;

                case TipoBD.SQLServer2000:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;

                case TipoBD.Oracle:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;

                case TipoBD.SQLite:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    pNameTabla = pNameTabla.ToLower();
                    break;

                case TipoBD.PostgreSQL:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    pNameTabla = pNameTabla.ToLower();
                    //pNameClaseRn = pNameClaseRn.ToLower();
                    break;

                case TipoBD.FireBird:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;
            }

            string strLlaves = "";
            string strColumnaPk = "";
            string strLlavesWhere = "";
            bool bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString().ToLower();

                    strLlavesWhere = strLlavesWhere + Environment.NewLine + "\t\t$this->db->where('" + nameCol + "', $id);";
                    strColumnaPk = nameCol;

                    if (bEsPrimerElemento)
                    {
                        strLlaves = strLlaves + "$" + tipoCol + "_" + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlaves = strLlaves + ", $" + tipoCol + "_" + nameCol;
                    }
                }
            }

            string strApiTrans = "";
            string strApiEstado = "";
            string strUsuCre = "";
            string strFecCre = "";
            string strUsuMod = "";
            string strFecMod = "";
            string strUsuDel = "";
            string strFecDel = "";
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strUsuCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strUsuMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("del"))
                    strUsuDel = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strFecCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strFecMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("del"))
                    strFecDel = nameCol;
                if ((nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("ssi") || nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api")) && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                    strApiEstado = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                    strApiTrans = nameCol;
            }

            if (string.IsNullOrEmpty(strApiTrans))
                strApiTrans = "----";
            if (string.IsNullOrEmpty(strApiEstado))
                strApiEstado = "----";
            if (string.IsNullOrEmpty(strUsuCre))
                strUsuCre = "----";
            if (string.IsNullOrEmpty(strFecCre))
                strFecCre = "----";
            if (string.IsNullOrEmpty(strUsuMod))
                strUsuMod = "----";
            if (string.IsNullOrEmpty(strFecMod))
                strFecMod = "----";


            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            newTexto.AppendLine("<?php");
            newTexto.AppendLine("");
            newTexto.AppendLine("namespace App\\Controllers;");
            newTexto.AppendLine("");
            newTexto.AppendLine("use App\\Models\\" + pNameClaseEnt + ";");
            newTexto.AppendLine("use Exception;");
            newTexto.AppendLine("use Firebase\\JWT\\JWT;");
            newTexto.AppendLine("use Firebase\\JWT\\Key;");
            newTexto.AppendLine("");
            newTexto.AppendLine("class " + pNameClaseRn + " extends SsiApiResourceController");
            newTexto.AppendLine("{");

            newTexto.AppendLine("\t/**");
            newTexto.AppendLine("\t * Return an array of resource objects, themselves in array format");
            newTexto.AppendLine("\t *");
            newTexto.AppendLine("\t * @return mixed");
            newTexto.AppendLine("\t */");
            newTexto.AppendLine("\tpublic function index()");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$key = $this->getKey();");
            newTexto.AppendLine("\t\t$authHeader = $this->request->getHeader(\"Authorization\");");
            newTexto.AppendLine("\t\tif (!$authHeader) {");
            newTexto.AppendLine("\t\t\treturn $this->failUnauthorized();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t$authHeader = $authHeader->getValue();");
            newTexto.AppendLine("\t\t$token = $authHeader;");
            newTexto.AppendLine("\t\ttry {");
            newTexto.AppendLine("\t\t\t$decoded = JWT::decode($token, new Key($key, 'HS256'));");
            newTexto.AppendLine("\t\t\tif ($decoded) {");
            newTexto.AppendLine("\t\t\t\t$myModel = new " + pNameClaseEnt + "();");
            newTexto.AppendLine("\t\t\t\t$objdata = $myModel->findAll();");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tif ($objdata) {");
            newTexto.AppendLine("\t\t\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t\t\t'status' => 200,");
            newTexto.AppendLine("\t\t\t\t\t\t'error' => false,");
            newTexto.AppendLine("\t\t\t\t\t\t'messages' => 'object details',");
            newTexto.AppendLine("\t\t\t\t\t\t'data' => $objdata");
            newTexto.AppendLine("\t\t\t\t\t];");           
            newTexto.AppendLine("\t\t\t\t} else {");
            newTexto.AppendLine("\t\t\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t\t\t'status' => 200,");
            newTexto.AppendLine("\t\t\t\t\t\t'error' => false,");
            newTexto.AppendLine("\t\t\t\t\t\t'messages' => 'object details',");
            newTexto.AppendLine("\t\t\t\t\t\t'data' => []");
            newTexto.AppendLine("\t\t\t\t\t];");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\treturn $this->respond($response, 200);");
            newTexto.AppendLine("\t\t\t} else {");
            newTexto.AppendLine("\t\t\t\treturn $this->failUnauthorized();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t} catch (Exception $ex) {");
            newTexto.AppendLine("\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t'status' => 401,");
            newTexto.AppendLine("\t\t\t\t'error' => true,");
            newTexto.AppendLine("\t\t\t\t'messages' => $ex->getMessage(),");
            newTexto.AppendLine("\t\t\t\t'data' => []");
            newTexto.AppendLine("\t\t\t];");
            newTexto.AppendLine("\t\t\treturn $this->respond($response, 500);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\t/**");
            newTexto.AppendLine("\t * Return the properties of a resource object");
            newTexto.AppendLine("\t *");
            newTexto.AppendLine("\t * @return mixed");
            newTexto.AppendLine("\t */");
            newTexto.AppendLine("\tpublic function show($id = null)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$key = $this->getKey();");
            newTexto.AppendLine("\t\t$authHeader = $this->request->getHeader(\"Authorization\");");
            newTexto.AppendLine("\t\tif (!$authHeader) {");
            newTexto.AppendLine("\t\t\treturn $this->failUnauthorized();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t$authHeader = $authHeader->getValue();");
            newTexto.AppendLine("\t\t$token = $authHeader;");
            newTexto.AppendLine("\t\ttry {");
            newTexto.AppendLine("\t\t\t$decoded = JWT::decode($token, new Key($key, 'HS256'));");
            newTexto.AppendLine("\t\t\tif ($decoded) {");
            newTexto.AppendLine("\t\t\t\t$myModel = new " + pNameClaseEnt + "();");
            newTexto.AppendLine("\t\t\t\t$objdata = $myModel->find($id);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tif ($objdata) {");
            newTexto.AppendLine("\t\t\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t\t\t'status' => 200,");
            newTexto.AppendLine("\t\t\t\t\t\t'error' => false,");
            newTexto.AppendLine("\t\t\t\t\t\t'messages' => 'object details',");
            newTexto.AppendLine("\t\t\t\t\t\t'data' => $objdata");
            newTexto.AppendLine("\t\t\t\t\t];");
            newTexto.AppendLine("\t\t\t\t} else {");
            newTexto.AppendLine("\t\t\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t\t\t'status' => 200,");
            newTexto.AppendLine("\t\t\t\t\t\t'error' => false,");
            newTexto.AppendLine("\t\t\t\t\t\t'messages' => 'object details',");
            newTexto.AppendLine("\t\t\t\t\t\t'data' => null");
            newTexto.AppendLine("\t\t\t\t\t];");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\treturn $this->respond($response, 200);");
            newTexto.AppendLine("\t\t\t} else {");
            newTexto.AppendLine("\t\t\t\treturn $this->failUnauthorized();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t} catch (Exception $ex) {");
            newTexto.AppendLine("\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t'status' => 500,");
            newTexto.AppendLine("\t\t\t\t'error' => true,");
            newTexto.AppendLine("\t\t\t\t'messages' => $ex->getMessage(),");
            newTexto.AppendLine("\t\t\t\t'data' => []");
            newTexto.AppendLine("\t\t\t];");
            newTexto.AppendLine("\t\t\treturn $this->respond($response, 500);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("");

            newTexto.AppendLine("\t/**");
            newTexto.AppendLine("\t * Create a new resource object, from \"posted\" parameters");
            newTexto.AppendLine("\t *");
            newTexto.AppendLine("\t * @return mixed");
            newTexto.AppendLine("\t */");
            newTexto.AppendLine("\tpublic function create()");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine(getRulesAndMessages(dtColumns, strColumnaPk, strApiTrans, strApiEstado, strUsuCre, strFecCre, strUsuMod, strFecMod, strUsuDel, strFecDel, false, false));
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\tif (!$this->validate($rules, $messages)) {");
            newTexto.AppendLine("\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t'status' => 400,");
            newTexto.AppendLine("\t\t\t\t'error' => true,");
            newTexto.AppendLine("\t\t\t\t'message' => $this->validator->getErrors(),");
            newTexto.AppendLine("\t\t\t\t'data' => []");
            newTexto.AppendLine("\t\t\t];");
            newTexto.AppendLine("\t\t\treturn $this->respond($response, 400);");
            newTexto.AppendLine("\t\t} else {");
            newTexto.AppendLine("\t\t\t$key = $this->getKey();");
            newTexto.AppendLine("\t\t\t$authHeader = $this->request->getHeader(\"Authorization\");");
            newTexto.AppendLine("\t\t\tif (!$authHeader) {");
            newTexto.AppendLine("\t\t\t\treturn $this->failUnauthorized();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\t$authHeader = $authHeader->getValue();");
            newTexto.AppendLine("\t\t\t$token = $authHeader;");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\t\ttry {");
            newTexto.AppendLine("\t\t\t\t$decoded = JWT::decode($token, new Key($key, 'HS256'));");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\t\t\tif ($decoded) {");
            newTexto.AppendLine("\t\t\t\t\t$data = [");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                if (nameCol == strColumnaPk || nameCol == strApiTrans || nameCol == strApiEstado || nameCol == strUsuCre || nameCol == strFecCre || nameCol == strUsuMod || nameCol == strFecMod || nameCol == strUsuDel || nameCol == strFecDel)
                {
                    if (nameCol == strApiTrans)
                    {
                        newTexto.AppendLine("\t\t\t\t\t\t'"+ nameCol + "' => 'CREAR',");
                    }
                    if (nameCol == strApiEstado)
                    {
                        newTexto.AppendLine("\t\t\t\t\t\t'"+ nameCol + "' => 'ELABORADO',");
                    }
                }
                else
                {
                    newTexto.AppendLine("\t\t\t\t\t\t'" + nameCol + "' => $this->request->getVar('" + nameCol + "'),");
                }
            }            
            newTexto.AppendLine("\t\t\t\t\t\t'"+ strUsuCre + "' => $decoded->user");
            newTexto.AppendLine("\t\t\t\t\t];");
            newTexto.AppendLine("\t\t\t\t\t$myModel = new " + pNameClaseEnt + "();");
            newTexto.AppendLine("\t\t\t\t\tif ($myModel->insert($data)) {");
            newTexto.AppendLine("\t\t\t\t\t\t$data[$myModel->primaryKey] = $myModel->getInsertID();");
            newTexto.AppendLine("\t\t\t\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t\t\t\t'status' => 200,");
            newTexto.AppendLine("\t\t\t\t\t\t\t'error' => false,");
            newTexto.AppendLine("\t\t\t\t\t\t\t'messages' => 'Con éxito, los datos han sido registrados.',");
            newTexto.AppendLine("\t\t\t\t\t\t\t'data' => $data");
            newTexto.AppendLine("\t\t\t\t\t\t];");
            newTexto.AppendLine("\t\t\t\t\t\treturn $this->respondCreated($response);");
            newTexto.AppendLine("\t\t\t\t\t} else {");
            newTexto.AppendLine("\t\t\t\t\t\t$response = $this->getResponseFromException($myModel);");
            newTexto.AppendLine("\t\t\t\t\t\treturn $this->respond($response, 500);");
            newTexto.AppendLine("\t\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\t} else {");
            newTexto.AppendLine("\t\t\t\t\treturn $this->failUnauthorized();");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t} catch (Exception $ex) {");
            newTexto.AppendLine("\t\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t\t'status' => 500,");
            newTexto.AppendLine("\t\t\t\t\t'error' => true,");
            newTexto.AppendLine("\t\t\t\t\t'messages' => $ex->getMessage(),");
            newTexto.AppendLine("\t\t\t\t\t'data' => []");
            newTexto.AppendLine("\t\t\t\t];");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t\treturn $this->respond($response, 500);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("");

            newTexto.AppendLine("\t/**");
            newTexto.AppendLine("\t * Return the editable properties of a resource object");
            newTexto.AppendLine("\t *");
            newTexto.AppendLine("\t * @return mixed");
            newTexto.AppendLine("\t */");
            newTexto.AppendLine("\tpublic function edit($id = null)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$key = $this->getKey();");
            newTexto.AppendLine("\t\t$authHeader = $this->request->getHeader(\"Authorization\");");
            newTexto.AppendLine("\t\tif (!$authHeader) {");
            newTexto.AppendLine("\t\t\treturn $this->failUnauthorized();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t$authHeader = $authHeader->getValue();");
            newTexto.AppendLine("\t\t$token = $authHeader;");
            newTexto.AppendLine("\t\ttry {");
            newTexto.AppendLine("\t\t\t$decoded = JWT::decode($token, new Key($key, 'HS256'));");
            newTexto.AppendLine("\t\t\tif ($decoded) {");
            newTexto.AppendLine("\t\t\t\t$myModel = new " + pNameClaseEnt + "();");
            newTexto.AppendLine("\t\t\t\t$objdata = $myModel->find($id);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tif ($objdata) {");
            newTexto.AppendLine("\t\t\t\t\t//Remove readOnly data");
            newTexto.AppendLine("\t\t\t\t\tunset($objdata[$myModel->primaryKey]);");
            newTexto.AppendLine("\t\t\t\t\tunset($objdata[$myModel->createdField]);");
            newTexto.AppendLine("\t\t\t\t\tunset($objdata[$myModel->updatedField]);");
            newTexto.AppendLine("\t\t\t\t\tunset($objdata[$myModel->deletedField]);");
            newTexto.AppendLine("\t\t\t\t\tunset($objdata[$myModel->userCreateField]);");
            newTexto.AppendLine("\t\t\t\t\tunset($objdata[$myModel->userUpdateField]);");
            newTexto.AppendLine("\t\t\t\t\tunset($objdata[$myModel->userDeleteField]);");
            newTexto.AppendLine("\t\t\t\t\tunset($objdata[$myModel->apiEstado]);");
            newTexto.AppendLine("\t\t\t\t\tunset($objdata[$myModel->apiTransaccion]);");
            newTexto.AppendLine("");   
            newTexto.AppendLine("\t\t\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t\t\t'status' => 200,");
            newTexto.AppendLine("\t\t\t\t\t\t'error' => false,");
            newTexto.AppendLine("\t\t\t\t\t\t'messages' => 'Detalles del objeto',");
            newTexto.AppendLine("\t\t\t\t\t\t'data' => $objdata");
            newTexto.AppendLine("\t\t\t\t\t];");            
            newTexto.AppendLine("\t\t\t\t} else {");
            newTexto.AppendLine("\t\t\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t\t\t'status' => 200,");
            newTexto.AppendLine("\t\t\t\t\t\t'error' => false,");
            newTexto.AppendLine("\t\t\t\t\t\t'messages' => 'Detalles del objeto',");
            newTexto.AppendLine("\t\t\t\t\t\t'data' => null");
            newTexto.AppendLine("\t\t\t\t\t];");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\treturn $this->respond($response, 200);");
            newTexto.AppendLine("\t\t\t} else {");
            newTexto.AppendLine("\t\t\t\treturn $this->failUnauthorized();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t} catch (Exception $ex) {");
            newTexto.AppendLine("\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t'status' => 500,");
            newTexto.AppendLine("\t\t\t\t'error' => true,");
            newTexto.AppendLine("\t\t\t\t'messages' => $ex->getMessage(),");
            newTexto.AppendLine("\t\t\t\t'data' => []");
            newTexto.AppendLine("\t\t\t];");
            newTexto.AppendLine("\t\t\treturn $this->respond($response, 500);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("");

            newTexto.AppendLine("\t/**");
            newTexto.AppendLine("\t * Update a model resource, from \"posted\" properties");
            newTexto.AppendLine("\t *");
            newTexto.AppendLine("\t * @return mixed");
            newTexto.AppendLine("\t */");
            newTexto.AppendLine("\tpublic function update($id = null)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine(getRulesAndMessages(dtColumns, strColumnaPk, strApiTrans, strApiEstado, strUsuCre, strFecCre, strUsuMod, strFecMod, strUsuDel, strFecDel, true, true));
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\tif (!$this->validate($rules, $messages)) {");
            newTexto.AppendLine("\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t'status' => 400,");
            newTexto.AppendLine("\t\t\t\t'error' => true,");
            newTexto.AppendLine("\t\t\t\t'message' => $this->validator->getErrors(),");
            newTexto.AppendLine("\t\t\t\t'data' => []");
            newTexto.AppendLine("\t\t\t];");
            newTexto.AppendLine("\t\t\treturn $this->respond($response, 400);");
            newTexto.AppendLine("\t\t} else {");
            newTexto.AppendLine("\t\t\t$key = $this->getKey();");
            newTexto.AppendLine("\t\t\t$authHeader = $this->request->getHeader(\"Authorization\");");
            newTexto.AppendLine("\t\t\tif (!$authHeader) {");
            newTexto.AppendLine("\t\t\t\treturn $this->failUnauthorized();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\t$authHeader = $authHeader->getValue();");
            newTexto.AppendLine("\t\t\t$token = $authHeader;");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\t\ttry {");
            newTexto.AppendLine("\t\t\t\t$decoded = JWT::decode($token, new Key($key, 'HS256'));");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\t\t\tif ($decoded) {");
            newTexto.AppendLine("\t\t\t\t\t$data = [");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                if (nameCol == strColumnaPk || nameCol == strApiEstado || nameCol == strApiTrans || nameCol == strUsuCre || nameCol == strFecCre || nameCol == strUsuMod || nameCol == strFecMod || nameCol == strUsuDel || nameCol == strFecDel)
                {
                    if (nameCol == strApiTrans)
                    {
                        newTexto.AppendLine("\t\t\t\t\t\t'" + nameCol + "' => $this->request->getVar('" + nameCol + "'),");
                    }
                    else if (nameCol == strApiEstado)
                    {
                        newTexto.AppendLine("\t\t\t\t\t\t'" + nameCol + "' => $this->request->getVar('" + nameCol + "'),");
                    }
                }
                else
                {
                    newTexto.AppendLine("\t\t\t\t\t\t'" + nameCol + "' => $this->request->getVar('" + nameCol + "'),");
                }
            }
            newTexto.AppendLine("\t\t\t\t\t\t'"+ strUsuMod + "' => $decoded->user");
            newTexto.AppendLine("\t\t\t\t\t];");
            newTexto.AppendLine("\t\t\t\t\t$myModel = new " + pNameClaseEnt + "();");
            newTexto.AppendLine("\t\t\t\t\tif ($myModel->update($id, $data)) {");
            newTexto.AppendLine("\t\t\t\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t\t\t\t'status' => 200,");
            newTexto.AppendLine("\t\t\t\t\t\t\t'error' => false,");
            newTexto.AppendLine("\t\t\t\t\t\t\t'messages' => 'Con éxito, los datos han sido actualizados',");
            newTexto.AppendLine("\t\t\t\t\t\t\t'data' => $data");
            newTexto.AppendLine("\t\t\t\t\t\t];");
            newTexto.AppendLine("\t\t\t\t\t\treturn $this->respondUpdated($response);");
            newTexto.AppendLine("\t\t\t\t\t} else {");
            newTexto.AppendLine("\t\t\t\t\t\t$response = $this->getResponseFromException($myModel);");
            newTexto.AppendLine("\t\t\t\t\t\treturn $this->respond($response, 500);");
            newTexto.AppendLine("\t\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\t} else {");
            newTexto.AppendLine("\t\t\t\t\treturn $this->failUnauthorized();");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t} catch (Exception $ex) {");
            newTexto.AppendLine("\t\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t\t'status' => 500,");
            newTexto.AppendLine("\t\t\t\t\t'error' => true,");
            newTexto.AppendLine("\t\t\t\t\t'messages' => $ex->getMessage(),");
            newTexto.AppendLine("\t\t\t\t\t'data' => []");
            newTexto.AppendLine("\t\t\t\t];");
            newTexto.AppendLine("\t\t\t\treturn $this->respond($response, 500);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("");

            newTexto.AppendLine("\t/**");
            newTexto.AppendLine("\t * Delete the designated resource object from the model");
            newTexto.AppendLine("\t *");
            newTexto.AppendLine("\t * @return mixed");
            newTexto.AppendLine("\t */");
            newTexto.AppendLine("\tpublic function delete($id = null)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$key = $this->getKey();");
            newTexto.AppendLine("\t\t$authHeader = $this->request->getHeader(\"Authorization\");");
            newTexto.AppendLine("\t\t\tif (!$authHeader) {");
            newTexto.AppendLine("\t\t\t\treturn $this->failUnauthorized();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t$authHeader = $authHeader->getValue();");
            newTexto.AppendLine("\t\t$token = $authHeader;");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\ttry {");
            newTexto.AppendLine("\t\t\t$decoded = JWT::decode($token, new Key($key, 'HS256'));");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\t\tif ($decoded) {");
            newTexto.AppendLine("\t\t\t\t$myModel = new " + pNameClaseEnt + "();");
            if (strUsuDel == "----")
            {
                newTexto.AppendLine("\t\t\t\tif ($myModel->delete($id)) {");
            }
            else
            {
                newTexto.AppendLine("\t\t\t\t//if ($myModel->delete($id)) {");
                newTexto.AppendLine("\t\t\t\t$data = [");
                newTexto.AppendLine("\t\t\t\t\t'"+ strApiEstado + "' => 'ELIMINADO',");
                newTexto.AppendLine("\t\t\t\t\t'"+ strUsuDel + "' => $decoded->user");
                newTexto.AppendLine("\t\t\t\t];");
                newTexto.AppendLine("\t\t\t\tif ($myModel->update($id, $data)) {");

            }
            newTexto.AppendLine("\t\t\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t\t\t'status' => 200,");
            newTexto.AppendLine("\t\t\t\t\t\t'error' => false,");
            newTexto.AppendLine("\t\t\t\t\t\t'messages' => 'Con éxito, los datos han sido eliminados.',");
            newTexto.AppendLine("\t\t\t\t\t\t'data' => []");
            newTexto.AppendLine("\t\t\t\t\t];");
            newTexto.AppendLine("\t\t\t\t\treturn $this->respondDeleted($response);");
            newTexto.AppendLine("\t\t\t\t} else {");
            newTexto.AppendLine("\t\t\t\t\t$response = $this->getResponseFromException($myModel);");
            newTexto.AppendLine("\t\t\t\t\treturn $this->respond($response, 500);");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t} else {");
            newTexto.AppendLine("\t\t\t\treturn $this->failUnauthorized();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t} catch (Exception $ex) {");
            newTexto.AppendLine("\t\t\t$response = [");
            newTexto.AppendLine("\t\t\t\t'status' => 500,");
            newTexto.AppendLine("\t\t\t\t'error' => true,");
            newTexto.AppendLine("\t\t\t\t'messages' => $ex->getMessage(),");
            newTexto.AppendLine("\t\t\t\t'data' => []");
            newTexto.AppendLine("\t\t\t];");
            newTexto.AppendLine("\t\t\treturn $this->respond($response, 500);");
            newTexto.AppendLine("\t\t}");
            
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("}");



            CFunciones.CrearArchivo(newTexto.ToString(), pNameClaseRn + ".php", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\" + pNameClaseRn + ".php";
        }

        public string CrearOpenAPIDocPhpCodeIgniterModelsAPI(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }
            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\")) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\"));
            }

            string pNameTablaAux = pNameClase;
            string pNameTabla = pNameClase.Replace("_", "");
            dynamic pNameClaseRn = formulario.txtPrefijoModelo.Text +
                                    pNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                    pNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                    pNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                    pNameTabla.Replace("_", "").Substring(4).ToLower();
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text +
                                    pNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                    pNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                    pNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                    pNameTabla.Replace("_", "").Substring(4).ToLower() +
                                    "Model";

            string pNameClaseJson = pNameClase.Replace("_", "").Substring(3, 1).ToUpper() +
                                    pNameTabla.Replace("_", "").Substring(4).ToLower();

            switch (Principal.DBMS)
            {
                case TipoBD.SQLServer:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;

                case TipoBD.SQLServer2000:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;

                case TipoBD.Oracle:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;

                case TipoBD.SQLite:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    pNameTabla = pNameTabla.ToLower();
                    break;

                case TipoBD.PostgreSQL:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    pNameTabla = pNameTabla.ToLower();
                    //pNameClaseRn = pNameClaseRn.ToLower();
                    break;

                case TipoBD.FireBird:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;
            }

            string strLlaves = "";
            string strColumnaPk = "";
            string strLlavesWhere = "";
            bool bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString().ToLower();

                    strLlavesWhere = strLlavesWhere + Environment.NewLine + "\t\t$this->db->where('" + nameCol + "', $id);";
                    strColumnaPk = nameCol;

                    if (bEsPrimerElemento)
                    {
                        strLlaves = strLlaves + "$" + tipoCol + "_" + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlaves = strLlaves + ", $" + tipoCol + "_" + nameCol;
                    }
                }
            }

            string strApiTrans = "";
            string strApiEstado = "";
            string strUsuCre = "";
            string strFecCre = "";
            string strUsuMod = "";
            string strFecMod = "";
            string strUsuDel = "";
            string strFecDel = "";
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strUsuCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strUsuMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("del"))
                    strUsuDel = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strFecCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strFecMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("del"))
                    strFecDel = nameCol;
                if ((nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("ssi") || nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api")) && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                    strApiEstado = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                    strApiTrans = nameCol;
            }

            if (string.IsNullOrEmpty(strApiTrans))
                strApiTrans = "----";
            if (string.IsNullOrEmpty(strApiEstado))
                strApiEstado = "----";
            if (string.IsNullOrEmpty(strUsuCre))
                strUsuCre = "----";
            if (string.IsNullOrEmpty(strFecCre))
                strFecCre = "----";
            if (string.IsNullOrEmpty(strUsuMod))
                strUsuMod = "----";
            if (string.IsNullOrEmpty(strFecMod))
                strFecMod = "----";


            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            newTexto.AppendLine("");
            newTexto.AppendLine("  \"paths\": {");
            newTexto.AppendLine("    \"/" + pNameClaseJson.ToLower() + "\": {");
            newTexto.AppendLine("      \"get\": {");
            newTexto.AppendLine("        \"tags\": [");
            newTexto.AppendLine("          \"" + pNameClaseJson.ToLower() + "\"");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"summary\": \"Obtener todos los "+ pNameClaseJson + "\",");
            newTexto.AppendLine("        \"description\": \"\",");
            newTexto.AppendLine("        \"operationId\": \""+ pNameClaseJson  + "_index\",");
            newTexto.AppendLine("        \"produces\": [");
            newTexto.AppendLine("          \"application/json\"");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"responses\": {");
            newTexto.AppendLine("          \"200\": {");
            newTexto.AppendLine("            \"description\": \"Operación exitosa\",");
            newTexto.AppendLine("            \"schema\": {");
            newTexto.AppendLine("              \"$ref\": \"#/definitions/" + pNameClaseJson + "\"");
            newTexto.AppendLine("            }");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("          \"404\": {");
            newTexto.AppendLine("            \"description\": \"Elementos no encontrados\"");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("          \"500\": {");
            newTexto.AppendLine("            \"description\": \"Error en el servidor\"");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        },");
            newTexto.AppendLine("        \"security\": [");
            newTexto.AppendLine("          {");
            newTexto.AppendLine("            \"api_key\": []");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        ]");
            newTexto.AppendLine("      },");
            newTexto.AppendLine("      \"post\": {");
            newTexto.AppendLine("        \"tags\": [");
            newTexto.AppendLine("          \"" + pNameClaseJson.ToLower() + "\"");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"summary\": \"Agregar un nuevo registro de " + pNameClaseJson + "\",");
            newTexto.AppendLine("        \"description\": \"\",");
            newTexto.AppendLine("        \"operationId\": \"" + pNameClaseJson + "_create\",");
            newTexto.AppendLine("        \"consumes\": [");
            newTexto.AppendLine("          \"application/json\"");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"produces\": [");
            newTexto.AppendLine("          \"application/json\"");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"parameters\": [");
            newTexto.AppendLine("          {");
            newTexto.AppendLine("            \"in\": \"body\", ");
            newTexto.AppendLine("            \"name\": \"body\", ");
            newTexto.AppendLine("            \"description\": \"Objeto de tipo " + pNameClaseJson + " a ser registrado\", ");
            newTexto.AppendLine("            \"required\": true,");
            newTexto.AppendLine("            \"schema\": {");
            newTexto.AppendLine("              \"$ref\": \"#/definitions/" + pNameClaseJson + "Insert\"");
            newTexto.AppendLine("            }");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"responses\": {");
            newTexto.AppendLine("          \"201\": {");
            newTexto.AppendLine("            \"description\": \"Operación exitosa\",");
            newTexto.AppendLine("            \"schema\": {");
            newTexto.AppendLine("              \"$ref\": \"#/definitions/" + pNameClaseJson + "\"");
            newTexto.AppendLine("            }");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("          \"405\": {");
            newTexto.AppendLine("            \"description\": \"Parámetro inválido\"");
            newTexto.AppendLine("            },");
            newTexto.AppendLine("          \"500\": {");
            newTexto.AppendLine("            \"description\": \"Error en el servidor\"");
            newTexto.AppendLine("            }");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("        \"security\": [");
            newTexto.AppendLine("          {");
            newTexto.AppendLine("            \"api_key\": []");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        ]");
            newTexto.AppendLine("      }");
            newTexto.AppendLine("    },");

            newTexto.AppendLine("    \"/" + pNameClaseJson.ToLower() + "/{id}\": {");
            newTexto.AppendLine("      \"get\": {");
            newTexto.AppendLine("        \"tags\": [");
            newTexto.AppendLine("          \"" + pNameClaseJson.ToLower() + "\"");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"summary\": \"Obtener un registro de " + pNameClaseJson + " por ID\",");
            newTexto.AppendLine("        \"description\": \"\",");
            newTexto.AppendLine("        \"operationId\": \"" + pNameClaseJson + "_show\",");
            newTexto.AppendLine("        \"produces\": [");
            newTexto.AppendLine("          \"application/json\"");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"parameters\": [");
            newTexto.AppendLine("          {");
            newTexto.AppendLine("            \"name\": \"id\",");
            newTexto.AppendLine("            \"in\": \"path\",");
            newTexto.AppendLine("            \"description\": \"ID del registro\",");
            newTexto.AppendLine("            \"required\": true,");
            newTexto.AppendLine("            \"type\": \"integer\",");
            newTexto.AppendLine("            \"format\": \"int64\"");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"responses\": {");
            newTexto.AppendLine("          \"200\": {");
            newTexto.AppendLine("            \"description\": \"Operación exitosa\",");
            newTexto.AppendLine("            \"schema\": {");
            newTexto.AppendLine("              \"$ref\": \"#/definitions/" + pNameClaseJson + "\"");
            newTexto.AppendLine("            }");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("          \"400\": {");
            newTexto.AppendLine("            \"description\": \"ID proporcionado invalido\"");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("          \"404\": {");
            newTexto.AppendLine("            \"description\": \"Registro de " + pNameClaseJson + " no encontrado\"");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("          \"500\": {");
            newTexto.AppendLine("            \"description\": \"Error en el servidor\"");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        },");
            newTexto.AppendLine("        \"security\": [");
            newTexto.AppendLine("          {");
            newTexto.AppendLine("            \"api_key\": []");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        ]");
            newTexto.AppendLine("      },");
            newTexto.AppendLine("      \"put\": {");
            newTexto.AppendLine("        \"tags\": [");
            newTexto.AppendLine("          \"" + pNameClaseJson.ToLower() + "\"");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"summary\": \"Actualizar un registro de " + pNameClaseJson + " por ID\",");
            newTexto.AppendLine("        \"description\": \"\",");
            newTexto.AppendLine("        \"operationId\": \"" + pNameClaseJson + "_update\",");
            newTexto.AppendLine("        \"produces\": [");
            newTexto.AppendLine("          \"application/json\"");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"parameters\": [");
            newTexto.AppendLine("          {");
            newTexto.AppendLine("            \"name\": \"id\",");
            newTexto.AppendLine("            \"in\": \"path\",");
            newTexto.AppendLine("            \"description\": \"ID del registro\",");
            newTexto.AppendLine("            \"required\": true,");
            newTexto.AppendLine("            \"type\": \"integer\",");
            newTexto.AppendLine("            \"format\": \"int64\"");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("          {");
            newTexto.AppendLine("            \"in\": \"body\", ");
            newTexto.AppendLine("            \"name\": \"body\", ");
            newTexto.AppendLine("            \"description\": \"Objeto de tipo " + pNameClaseJson + " a ser registrado\", ");
            newTexto.AppendLine("            \"required\": true,");
            newTexto.AppendLine("            \"schema\": {");
            newTexto.AppendLine("              \"$ref\": \"#/definitions/" + pNameClaseJson + "Update\"");
            newTexto.AppendLine("            }");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"responses\": {");
            newTexto.AppendLine("          \"204\": {");
            newTexto.AppendLine("            \"description\": \"Operación exitosa\",");
            newTexto.AppendLine("            \"schema\": {");
            newTexto.AppendLine("              \"$ref\": \"#/definitions/" + pNameClaseJson + "\"");
            newTexto.AppendLine("            }");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("          \"400\": {");
            newTexto.AppendLine("            \"description\": \"ID proporcionado invalido\"");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("          \"404\": {");
            newTexto.AppendLine("            \"description\": \"Registro de " + pNameClaseJson + " no encontrado\"");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("          \"500\": {");
            newTexto.AppendLine("            \"description\": \"Error en el servidor\"");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        },");
            newTexto.AppendLine("        \"security\": [");
            newTexto.AppendLine("          {");
            newTexto.AppendLine("            \"api_key\": []");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        ]");
            newTexto.AppendLine("      },");            
            newTexto.AppendLine("      \"delete\": {");
            newTexto.AppendLine("        \"tags\": [");
            newTexto.AppendLine("          \"" + pNameClaseJson.ToLower() + "\"");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"summary\": \"Eleminar un registro de " + pNameClaseJson + " por ID\",");
            newTexto.AppendLine("        \"description\": \"\",");
            newTexto.AppendLine("        \"operationId\": \"" + pNameClaseJson + "_delete\",");
            newTexto.AppendLine("        \"produces\": [");
            newTexto.AppendLine("          \"application/json\"");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"parameters\": [");
            newTexto.AppendLine("          {");
            newTexto.AppendLine("            \"name\": \"id\",");
            newTexto.AppendLine("            \"in\": \"path\",");
            newTexto.AppendLine("            \"description\": \"ID del registro\",");
            newTexto.AppendLine("            \"required\": true,");
            newTexto.AppendLine("            \"type\": \"integer\",");
            newTexto.AppendLine("            \"format\": \"int64\"");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"responses\": {");
            newTexto.AppendLine("          \"202\": {");
            newTexto.AppendLine("            \"description\": \"Operación exitosa\",");
            newTexto.AppendLine("            \"schema\": {");
            newTexto.AppendLine("              \"$ref\": \"#/definitions/" + pNameClaseJson + "\"");
            newTexto.AppendLine("            }");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("          \"400\": {");
            newTexto.AppendLine("            \"description\": \"ID proporcionado invalido\"");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("          \"404\": {");
            newTexto.AppendLine("            \"description\": \"Registro de " + pNameClaseJson + " no encontrado\"");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("          \"500\": {");
            newTexto.AppendLine("            \"description\": \"Error en el servidor\"");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        },");
            newTexto.AppendLine("        \"security\": [");
            newTexto.AppendLine("          {");
            newTexto.AppendLine("            \"api_key\": []");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        ]");
            newTexto.AppendLine("      }");
            newTexto.AppendLine("    },");

            newTexto.AppendLine("    \"/" + pNameClaseJson.ToLower() + "/{id}/edit\": {");
            newTexto.AppendLine("      \"get\": {");
            newTexto.AppendLine("        \"tags\": [");
            newTexto.AppendLine("          \"" + pNameClaseJson.ToLower() + "\"");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"summary\": \"Obtener un registro de " + pNameClaseJson + " por ID\",");
            newTexto.AppendLine("        \"description\": \"\",");
            newTexto.AppendLine("        \"operationId\": \"" + pNameClaseJson + "_show_edit\",");
            newTexto.AppendLine("        \"produces\": [");
            newTexto.AppendLine("          \"application/json\"");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"parameters\": [");
            newTexto.AppendLine("          {");
            newTexto.AppendLine("            \"name\": \"id\",");
            newTexto.AppendLine("            \"in\": \"path\",");
            newTexto.AppendLine("            \"description\": \"ID del registro\",");
            newTexto.AppendLine("            \"required\": true,");
            newTexto.AppendLine("            \"type\": \"integer\",");
            newTexto.AppendLine("            \"format\": \"int64\"");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        ],");
            newTexto.AppendLine("        \"responses\": {");
            newTexto.AppendLine("          \"200\": {");
            newTexto.AppendLine("            \"description\": \"Operación exitosa\",");
            newTexto.AppendLine("            \"schema\": {");
            newTexto.AppendLine("              \"$ref\": \"#/definitions/" + pNameClaseJson + "\"");
            newTexto.AppendLine("            }");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("          \"400\": {");
            newTexto.AppendLine("            \"description\": \"ID proporcionado invalido\"");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("          \"404\": {");
            newTexto.AppendLine("            \"description\": \"Registro de " + pNameClaseJson + " no encontrado\"");
            newTexto.AppendLine("          },");
            newTexto.AppendLine("          \"500\": {");
            newTexto.AppendLine("            \"description\": \"Error en el servidor\"");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        },");
            newTexto.AppendLine("        \"security\": [");
            newTexto.AppendLine("          {");
            newTexto.AppendLine("            \"api_key\": []");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        ]");
            newTexto.AppendLine("      }");
            newTexto.AppendLine("    }");
            newTexto.AppendLine("  },");
            newTexto.AppendLine("");
            newTexto.AppendLine("");
            newTexto.AppendLine("");
            newTexto.AppendLine("");
            newTexto.AppendLine("");
            newTexto.AppendLine("");
            newTexto.AppendLine("");
            newTexto.AppendLine("  \"definitions\": {");
            newTexto.AppendLine("    \"" + pNameClaseJson + "\": {");
            newTexto.AppendLine("      \"type\": \"object\",");
            newTexto.AppendLine("      \"required\": [");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if (dtColumns.Rows[iCol]["permitenull"].ToString() == "No")
                {
                    string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                    newTexto.AppendLine("        \"" + nameCol + "\",");
                }
            }
            newTexto.AppendLine("      ],");
            newTexto.AppendLine("      \"properties\": {");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                string nameDesc = dtColumns.Rows[iCol]["col_desc"].ToString();
                string nameType = dtColumns.Rows[iCol]["tipo"].ToString().ToLower();
                if (nameCol == strApiEstado)
                {
                    newTexto.AppendLine("        \"" + nameCol + "\": {");
                    newTexto.AppendLine("          \"type\": \"string\",");
                    newTexto.AppendLine("          \"description\": \"" + nameDesc + "\",");
                    newTexto.AppendLine("          \"enum\": [");
                    newTexto.AppendLine("            \"INICIAL\",");
                    newTexto.AppendLine("            \"ELABORADO\",");
                    newTexto.AppendLine("            \"FINAL\"");
                    newTexto.AppendLine("          ]");
                    newTexto.AppendLine("        },");
                }
                else if (nameCol == strApiTrans)
                {
                    newTexto.AppendLine("        \"" + nameCol + "\": {");
                    newTexto.AppendLine("          \"type\": \"string\",");
                    newTexto.AppendLine("          \"description\": \"" + nameDesc + "\",");
                    newTexto.AppendLine("          \"enum\": [");
                    newTexto.AppendLine("            \"CREAR\",");
                    newTexto.AppendLine("            \"MODIFICAR\",");
                    newTexto.AppendLine("            \"ELIMINAR\"");
                    newTexto.AppendLine("          ]");
                    newTexto.AppendLine("        },");
                }
                else
                {
                    newTexto.AppendLine("        \"" + nameCol + "\": {");
                    if (nameType == "date")
                    {
                        newTexto.AppendLine("          \"type\": \"string\",");
                        newTexto.AppendLine("          \"format\": \"date-time\",");
                    }
                    else if (nameType == "datetime")
                    {
                        newTexto.AppendLine("          \"type\": \"string\",");
                        newTexto.AppendLine("          \"format\": \"date-time\",");
                    }
                    else
                    {
                        newTexto.AppendLine("          \"type\": \"" + nameType + "\",");
                    }
                    newTexto.AppendLine("          \"description\": \"" + nameDesc + "\"");
                    newTexto.AppendLine("        },");
                }
            }

            newTexto.AppendLine("      }");
            newTexto.AppendLine("    },");

            newTexto.AppendLine("    \"" + pNameClaseJson + "Insert\": {");
            newTexto.AppendLine("      \"type\": \"object\",");
            newTexto.AppendLine("      \"required\": [");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if (dtColumns.Rows[iCol]["permitenull"].ToString() == "No")
                {
                    string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                    if (nameCol == strApiEstado)
                    {
                        //Ignored
                    }
                    else if (nameCol == strApiTrans)
                    {
                        //Ignored
                    }
                    else if (nameCol == strUsuCre || nameCol == strUsuMod || nameCol == strUsuDel || nameCol == strFecCre || nameCol == strFecMod || nameCol == strFecDel)
                    {
                        //Ignored
                    }
                    else
                    {
                        newTexto.AppendLine("        \"" + nameCol + "\",");
                    }
                }
            }
            newTexto.AppendLine("      ],");
            newTexto.AppendLine("      \"properties\": {");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                string nameDesc = dtColumns.Rows[iCol]["col_desc"].ToString();
                string nameType = dtColumns.Rows[iCol]["tipo"].ToString().ToLower();
                if (nameCol == strApiEstado)
                {
                    //Ignored
                }
                else if (nameCol == strApiTrans)
                {
                    //Ignored
                }
                else if (nameCol == strUsuCre || nameCol == strUsuMod || nameCol == strUsuDel || nameCol == strFecCre || nameCol == strFecMod || nameCol == strFecDel)
                {
                    //Ignored
                }
                else
                {
                    newTexto.AppendLine("        \""+ nameCol + "\": {");
                    if (nameType == "date")
                    {
                        newTexto.AppendLine("          \"type\": \"string\",");
                        newTexto.AppendLine("          \"format\": \"date-time\",");
                    }
                    else if (nameType == "datetime")
                    {
                        newTexto.AppendLine("          \"type\": \"string\",");
                        newTexto.AppendLine("          \"format\": \"date-time\",");
                    }
                    else
                    {
                        newTexto.AppendLine("          \"type\": \"" + nameType + "\",");
                    }
                    newTexto.AppendLine("          \"description\": \""+ nameDesc + "\"");
                    newTexto.AppendLine("        },");
                }                
            }           

            newTexto.AppendLine("      }");
            newTexto.AppendLine("    },");

            newTexto.AppendLine("    \"" + pNameClaseJson + "Update\": {");
            newTexto.AppendLine("      \"type\": \"object\",");
            newTexto.AppendLine("      \"required\": [");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if (dtColumns.Rows[iCol]["permitenull"].ToString() == "No")
                {
                    string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                    if (nameCol == strApiEstado)
                    {
                        newTexto.AppendLine("        \"" + nameCol + "\",");
                    }
                    else if (nameCol == strApiTrans)
                    {
                        newTexto.AppendLine("        \"" + nameCol + "\",");
                    }
                    else if (nameCol == strUsuCre || nameCol == strUsuMod || nameCol == strUsuDel || nameCol == strFecCre || nameCol == strFecMod || nameCol == strFecDel)
                    {
                        //Ignored
                    }
                    else
                    {
                        newTexto.AppendLine("        \"" + nameCol + "\",");
                    }
                }
            }
            newTexto.AppendLine("      ],");
            newTexto.AppendLine("      \"properties\": {");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                string nameDesc = dtColumns.Rows[iCol]["col_desc"].ToString();
                string nameType = dtColumns.Rows[iCol]["tipo"].ToString().ToLower();
                if (nameCol == strApiEstado)
                {
                    newTexto.AppendLine("        \"" + nameCol + "\": {");
                    newTexto.AppendLine("          \"type\": \"string\",");
                    newTexto.AppendLine("          \"description\": \"" + nameDesc + "\",");
                    newTexto.AppendLine("          \"enum\": [");
                    newTexto.AppendLine("            \"INICIAL\",");
                    newTexto.AppendLine("            \"ELABORADO\",");
                    newTexto.AppendLine("            \"FINAL\"");
                    newTexto.AppendLine("          ]");
                    newTexto.AppendLine("        },");
                }
                else if (nameCol == strApiTrans)
                {
                    newTexto.AppendLine("        \"" + nameCol + "\": {");
                    newTexto.AppendLine("          \"type\": \"string\",");
                    newTexto.AppendLine("          \"description\": \"" + nameDesc + "\",");
                    newTexto.AppendLine("          \"enum\": [");
                    newTexto.AppendLine("            \"CREAR\",");
                    newTexto.AppendLine("            \"MODIFICAR\",");
                    newTexto.AppendLine("            \"ELIMINAR\"");
                    newTexto.AppendLine("          ]");
                    newTexto.AppendLine("        },");
                }
                else if (nameCol == strUsuCre || nameCol == strUsuMod || nameCol == strUsuDel || nameCol == strFecCre || nameCol == strFecMod || nameCol == strFecDel)
                {
                    //Ignored
                }
                else
                {
                    newTexto.AppendLine("        \"" + nameCol + "\": {");
                    if (nameType == "date")
                    {
                        newTexto.AppendLine("          \"type\": \"string\",");
                        newTexto.AppendLine("          \"format\": \"date-time\",");
                    }
                    else if (nameType == "datetime")
                    {
                        newTexto.AppendLine("          \"type\": \"string\",");
                        newTexto.AppendLine("          \"format\": \"date-time\",");
                    }
                    else
                    {
                        newTexto.AppendLine("          \"type\": \"" + nameType + "\",");
                    }
                    newTexto.AppendLine("          \"description\": \"" + nameDesc + "\"");
                    newTexto.AppendLine("        },");
                }
            }

            newTexto.AppendLine("      }");
            newTexto.AppendLine("    },");
            newTexto.AppendLine("    \"ApiResponse\": {");
            newTexto.AppendLine("      \"type\": \"object\",");
            newTexto.AppendLine("      \"properties\": {");
            newTexto.AppendLine("        \"status\": {");
            newTexto.AppendLine("          \"type\": \"integer\",");
            newTexto.AppendLine("          \"format\": \"int32\"");
            newTexto.AppendLine("        },");
            newTexto.AppendLine("        \"error\": {");
            newTexto.AppendLine("          \"type\": \"string\",");
            newTexto.AppendLine("          \"format\": \"int32\"");
            newTexto.AppendLine("        },");
            newTexto.AppendLine("        \"message\": {");
            newTexto.AppendLine("          \"type\": \"string\"");
            newTexto.AppendLine("        },");
            newTexto.AppendLine("        \"data\": {");
            newTexto.AppendLine("          \"type\": \"array\",");
            newTexto.AppendLine("          \"items\": {");
            newTexto.AppendLine("            \"type\": \"object\"");
            newTexto.AppendLine("          }");
            newTexto.AppendLine("        }");
            newTexto.AppendLine("      }");
            newTexto.AppendLine("    }");
            newTexto.AppendLine("  }");




            CFunciones.CrearArchivo(newTexto.ToString(), pNameClaseJson + ".json", PathDesktop + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + pNameClaseJson + ".json";
        }

        private string getRulesAndMessages(DataTable dtColumns, string strColumnaPk, string strApiTrans, string strApiEstado, string strUsuCre, string strFecCre, string strUsuMod, string strFecMod, string strUsuDel, string strFecDel, bool includeApiTrans, bool includeApiEstado)
        {
            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            newTexto.AppendLine("\t\t$rules = [");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                if (nameCol == strColumnaPk || nameCol == strApiTrans || nameCol == strApiEstado || nameCol == strUsuCre || nameCol == strFecCre || nameCol == strUsuMod || nameCol == strFecMod || nameCol == strUsuDel || nameCol == strFecDel)
                {
                    if (includeApiTrans && nameCol == strApiTrans)
                    {
                        newTexto.AppendLine("\t\t\t'" + strApiTrans + "' => 'required',");
                    }
                    else if (includeApiEstado && nameCol == strApiEstado)
                    {
                        newTexto.AppendLine("\t\t\t'" + strApiEstado + "' => 'required',");
                    }
                }                
                else
                {
                    newTexto.AppendLine("\t\t\t'" + nameCol + "' => 'required',");
                }
            }
            newTexto.AppendLine("\t\t];");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\t$messages = [");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                if (nameCol == strColumnaPk || nameCol == strApiTrans || nameCol == strApiEstado || nameCol == strUsuCre || nameCol == strFecCre || nameCol == strUsuMod || nameCol == strFecMod || nameCol == strUsuDel || nameCol == strFecDel)
                {
                    if (includeApiTrans && nameCol == strApiTrans)
                    {
                        newTexto.AppendLine("\t\t\t'" + strApiTrans + "' => [");
                        newTexto.AppendLine("\t\t\t\t'required' => '" + strApiTrans + " es obligatorio'");
                        newTexto.AppendLine("\t\t\t]");
                    }
                    else if (includeApiEstado && nameCol == strApiEstado)
                    {
                        newTexto.AppendLine("\t\t\t'" + strApiEstado + "' => [");
                        newTexto.AppendLine("\t\t\t\t'required' => '" + strApiEstado + " es obligatorio'");
                        newTexto.AppendLine("\t\t\t]");
                    }
                }
                else
                {
                    newTexto.AppendLine("\t\t\t'" + nameCol + "' => [");
                    newTexto.AppendLine("\t\t\t\t'required' => '" + nameCol + " es obligatorio'");
                    newTexto.AppendLine("\t\t\t],");
                }
            }
            newTexto.AppendLine("\t\t];");

            return newTexto.ToString();
        }


        public string CrearPhpStormApiTests(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }
            if (Directory.Exists(PathDesktop + "\\" + ("tests.unit").Replace(".", "\\")) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + ("tests.unit").Replace(".", "\\"));
            }

            string pNameTablaAux = pNameClase;
            string pNameTabla = pNameClase.Replace("_", "");
            dynamic pNameClaseRn = formulario.txtPrefijoModelo.Text +
                                    pNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                    pNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                    pNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                    pNameTabla.Replace("_", "").Substring(4).ToLower();
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text +
                                                               pNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                                               pNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                                               pNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                                               pNameTabla.Replace("_", "").Substring(4).ToLower() +
                                                               "Model";

            string pNameClaseApi = pNameClaseRn.Remove(0, 3).ToLower();

            string strLlaves = "";
            string strColumnaPk = "";
            string strLlavesWhere = "";
            bool bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString().ToLower();

                    strLlavesWhere = strLlavesWhere + Environment.NewLine + "\t\t$this->db->where('" + nameCol + "', $id);";
                    strColumnaPk = nameCol;

                    if (bEsPrimerElemento)
                    {
                        strLlaves = strLlaves + "$" + tipoCol + "_" + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlaves = strLlaves + ", $" + tipoCol + "_" + nameCol;
                    }
                }
            }

            string strApiTrans = "";
            string strApiEstado = "";
            string strUsuCre = "";
            string strFecCre = "";
            string strUsuMod = "";
            string strFecMod = "";
            string strUsuDel = "";
            string strFecDel = "";
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strUsuCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strUsuMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("del"))
                    strUsuDel = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strFecCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strFecMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("del"))
                    strFecDel = nameCol;
                if ((nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("ssi") || nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api")) && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                    strApiEstado = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                    strApiTrans = nameCol;
            }

            if (string.IsNullOrEmpty(strApiTrans))
                strApiTrans = "----";
            if (string.IsNullOrEmpty(strApiEstado))
                strApiEstado = "----";
            if (string.IsNullOrEmpty(strUsuCre))
                strUsuCre = "----";
            if (string.IsNullOrEmpty(strFecCre))
                strFecCre = "----";
            if (string.IsNullOrEmpty(strUsuMod))
                strUsuMod = "----";
            if (string.IsNullOrEmpty(strFecMod))
                strFecMod = "----";


            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            newTexto.AppendLine("GET http://localhost:8080/electrolineras/api/"+ pNameClaseApi + "");
            newTexto.AppendLine("Accept: application/json");
            newTexto.AppendLine("");
            newTexto.AppendLine("###");
            newTexto.AppendLine("");
            newTexto.AppendLine("GET http://localhost:8080/electrolineras/api/" + pNameClaseApi + "/1/edit");
            newTexto.AppendLine("Accept: application/json");
            newTexto.AppendLine("");
            newTexto.AppendLine("###");
            newTexto.AppendLine("");
            newTexto.AppendLine("POST http://localhost:8080/electrolineras/api/" + pNameClaseApi + "");
            newTexto.AppendLine("Content-Type: application/json");
            newTexto.AppendLine("Authorization: JWT_TOKEN_HERE");
            newTexto.AppendLine("");
            newTexto.AppendLine("{");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                if (nameCol == strColumnaPk ||  nameCol == strApiEstado || nameCol == strUsuCre || nameCol == strFecCre || nameCol == strUsuMod || nameCol == strFecMod || nameCol == strUsuDel || nameCol == strFecDel)
                {
                    //ignored
                }
                else if (nameCol == strApiTrans)
                {
                    //ignored
                }
                else
                {                    
                    newTexto.AppendLine("\t\"" + nameCol + "\": \"test_" + nameCol + "\", ");
                }
            }            
            newTexto.AppendLine("}");
            newTexto.AppendLine("");
            newTexto.AppendLine("###");
            newTexto.AppendLine("");
            newTexto.AppendLine("PUT http://localhost:8080/electrolineras/api/" + pNameClaseApi + "/8");
            newTexto.AppendLine("Content-Type: application/json");
            newTexto.AppendLine("Authorization: JWT_TOKEN_HERE");
            newTexto.AppendLine("");
            newTexto.AppendLine("{");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                if (nameCol == strColumnaPk || nameCol == strApiEstado || nameCol == strUsuCre || nameCol == strFecCre || nameCol == strUsuMod || nameCol == strFecMod || nameCol == strUsuDel || nameCol == strFecDel)
                {
                    //ignored
                }
                else if (nameCol == strApiTrans)
                {
                    newTexto.AppendLine("\t\""+ strApiTrans + "\": \"MODIFICAR\"");
                }
                else
                {
                    newTexto.AppendLine("\t\"" + nameCol + "\": \"test_" + nameCol + "\", ");
                }
            }            
            newTexto.AppendLine("}");
            newTexto.AppendLine("");
            newTexto.AppendLine("###");
            newTexto.AppendLine("");
            newTexto.AppendLine("DELETE http://localhost:8080/electrolineras/api/" + pNameClaseApi + "/9");
            newTexto.AppendLine("Accept: application/json");
            newTexto.AppendLine("Authorization: JWT_TOKEN_HERE");
            newTexto.AppendLine("");


            CFunciones.CrearArchivo(newTexto.ToString(), "JWT_" + pNameClaseRn + ".http", PathDesktop + "\\" + ("tests.unit").Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + ("tests.unit").Replace(".", "\\") + "\\JWT_" + pNameClaseRn + ".http";
        }
    }
}
