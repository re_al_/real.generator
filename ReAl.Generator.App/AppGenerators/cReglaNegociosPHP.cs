﻿using ReAl.Generator.App.AppCore;
using System;
using System.Data;
using System.Text;

namespace ReAl.Generator.App.AppGenerators
{
    public class cReglaNegociosPHP
    {
        public string CrearModeloPhp(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            string pConnClass = formulario.txtClaseConn.Text;

            string pNameTabla = pNameClase.Replace("_", "");
            pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() +
                         pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();

            pNameClase = formulario.txtPrefijoModelo.Text + pNameTabla;
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameTabla;

            string strLlaves = "";
            string strLlavesQueryObj = "";
            string strLlavesQuery = "";
            bool bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString();

                    if (bEsPrimerElemento)
                    {
                        strLlaves = strLlaves + tipoCol + " " + tipoCol + nameCol;
                        strLlavesQueryObj = strLlavesQueryObj + " " + pNameClaseEnt + ".Fields." + nameCol + ".toString() + \" = \" + obj.get" + nameCol + "()";
                        strLlavesQuery = strLlavesQuery + " " + pNameClaseEnt + ".Fields." + nameCol + ".toString() + \" = \" + " + tipoCol + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlaves = strLlaves + ", " + tipoCol + " " + tipoCol + nameCol;
                        strLlavesQueryObj = strLlavesQueryObj + "\"  AND \" + " + pNameClaseEnt + ".Fields." + nameCol + ".toString() + \" = \" + obj.get" + nameCol + "()";
                        strLlavesQuery = strLlavesQuery + "\"  AND \" + " + pNameClaseEnt + ".Fields." + nameCol + ".toString() + \" = \" + " + tipoCol + nameCol;
                    }
                }
            }

            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase).Replace("#region", "").Replace("#endregion", "");
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que implementa los metodos y operaciones sobre la Tabla " + pNameTabla);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();

            newTexto.AppendLine("<?php");
            newTexto.AppendLine("");
            newTexto.AppendLine(CabeceraTmp);
            newTexto.AppendLine("");

            newTexto.AppendLine("include_once '" + pConnClass + ".php';");
            newTexto.AppendLine("include_once '" + pNameClaseEnt + ".php';");
            newTexto.AppendLine("");

            newTexto.AppendLine("class " + pNameClase);
            newTexto.AppendLine("{");
            newTexto.AppendLine("\tfunction obtenerObjetoPk($p_id)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$arrCol = array(");
            bEsPrimerElemento = true;
            for (int iCol = 1; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                string tipoCol = dtColumns.Rows[iCol][1].ToString();
                if (bEsPrimerElemento)
                {
                    bEsPrimerElemento = false;
                    newTexto.AppendLine("\t\t" + pNameClaseEnt + "Fields::" + nameCol);
                }
                else
                {
                    newTexto.AppendLine("\t\t," + pNameClaseEnt + "Fields::" + nameCol);
                }
            }
            newTexto.AppendLine("\t\t);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t$arrColWhere = array(");
            bEsPrimerElemento = true;
            for (int iCol = 1; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                string tipoCol = dtColumns.Rows[iCol][1].ToString();
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    if (bEsPrimerElemento)
                    {
                        bEsPrimerElemento = false;
                        newTexto.AppendLine("\t\t" + pNameClaseEnt + "Fields::" + nameCol);
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t," + pNameClaseEnt + "Fields::" + nameCol);
                    }
                }
            }
            newTexto.AppendLine("\t\t);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t$$arrValWhere = array(");
            bEsPrimerElemento = true;
            for (int iCol = 1; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                string tipoCol = dtColumns.Rows[iCol][1].ToString();
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    if (bEsPrimerElemento)
                    {
                        bEsPrimerElemento = false;
                        newTexto.AppendLine("\t\t$p_" + nameCol);
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t,$p_" + nameCol);
                    }
                }
            }
            newTexto.AppendLine("\t\t$myCon = new " + pConnClass + "();");
            newTexto.AppendLine("\t\t$result = $myCon->select(" + pNameClaseEnt + "Fields::NAME_TABLE,$arrCol, $arrColWhere,$arrValWhere);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tif (!$result)");
            newTexto.AppendLine("\t\t\treturn null;");
            newTexto.AppendLine("\t\telse");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tforeach ($result as $var)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\t$obj = new " + pNameClaseEnt + "();");
            for (int iCol = 1; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t\t$obj->" + nameCol + " = $var[" + pNameClaseEnt + "Fields::" + nameCol + "];");
            }
            newTexto.AppendLine("\t\t\t\treturn $obj;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tfunction obtenerLista($varColWhere, $varValWhere, $strParam = null)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$arrCol = array(");
            bEsPrimerElemento = true;
            for (int iCol = 1; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                string tipoCol = dtColumns.Rows[iCol][1].ToString();
                if (bEsPrimerElemento)
                {
                    bEsPrimerElemento = false;
                    newTexto.AppendLine("\t\t" + pNameClaseEnt + "Fields::" + nameCol);
                }
                else
                {
                    newTexto.AppendLine("\t\t," + pNameClaseEnt + "Fields::" + nameCol);
                }
            }
            newTexto.AppendLine("\t\t);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tif (is_array($varColWhere))");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t$arrColWhere = $varColWhere;");
            newTexto.AppendLine("\t\t\t$arrValWhere = $varValWhere;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\telse");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t$arrColWhere = array($varColWhere);");
            newTexto.AppendLine("\t\t\t$arrValWhere = array($varValWhere);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t$myCon = new " + pConnClass + "();");
            newTexto.AppendLine("\t\t$result = $myCon->select(" + pNameClaseEnt + "Fields::NAME_TABLE,$arrCol, $arrColWhere,$arrValWhere);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tif (!$result)");
            newTexto.AppendLine("\t\t\treturn null;");
            newTexto.AppendLine("\t\telse");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t$myArray = array();");
            newTexto.AppendLine("\t\t\tforeach ($result as $var)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\t$obj = new " + pNameClaseEnt + "();");
            for (int iCol = 1; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t\t$obj->" + nameCol + " = $var[" + pNameClaseEnt + "Fields::" + nameCol + "];");
            }
            newTexto.AppendLine("\t\t\t\tarray_push($myArray, $obj);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\treturn $myArray;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tfunction insert($class)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$arrCol = array(");
            bEsPrimerElemento = true;
            for (int iCol = 1; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                if (bEsPrimerElemento)
                {
                    bEsPrimerElemento = false;
                    newTexto.AppendLine("\t\t" + pNameClaseEnt + "Fields::" + nameCol);
                }
                else
                {
                    newTexto.AppendLine("\t\t," + pNameClaseEnt + "Fields::" + nameCol);
                }
            }
            newTexto.AppendLine("\t\t);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t$arrVal = array(");
            bEsPrimerElemento = true;
            for (int iCol = 1; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                if (bEsPrimerElemento)
                {
                    bEsPrimerElemento = false;
                    newTexto.AppendLine("\t\t$class->" + nameCol);
                }
                else
                {
                    newTexto.AppendLine("\t\t,$class->" + nameCol);
                }
            }
            newTexto.AppendLine("\t\t);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t$myCon = new cConn();");
            newTexto.AppendLine("\t\treturn $myCon->insert(" + pNameClaseEnt + "Fields::NAME_TABLE, $arrCol, $arrVal);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tfunction update($class)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$arrCol = array(");
            bEsPrimerElemento = true;
            for (int iCol = 1; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "NO"))
                {
                    if (bEsPrimerElemento)
                    {
                        bEsPrimerElemento = false;
                        newTexto.AppendLine("\t\t" + pNameClaseEnt + "Fields::" + nameCol);
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t," + pNameClaseEnt + "Fields::" + nameCol);
                    }
                }
            }
            newTexto.AppendLine("\t\t);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t$arrVal = array(");
            bEsPrimerElemento = true;
            for (int iCol = 1; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "NO"))
                {
                    if (bEsPrimerElemento)
                    {
                        bEsPrimerElemento = false;
                        newTexto.AppendLine("\t\t$class->" + nameCol);
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t,$class->" + nameCol);
                    }
                }
            }
            newTexto.AppendLine("\t\t);");
            newTexto.AppendLine("\t\t$arrColWhere = array(");
            bEsPrimerElemento = true;
            for (int iCol = 1; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    if (bEsPrimerElemento)
                    {
                        bEsPrimerElemento = false;
                        newTexto.AppendLine("\t\t" + pNameClaseEnt + "Fields::" + nameCol);
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t," + pNameClaseEnt + "Fields::" + nameCol);
                    }
                }
            }
            newTexto.AppendLine("\t\t);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t$arrValWhere = array(");
            bEsPrimerElemento = true;
            for (int iCol = 1; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    if (bEsPrimerElemento)
                    {
                        bEsPrimerElemento = false;
                        newTexto.AppendLine("\t\t$class->" + nameCol);
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t,$class->" + nameCol);
                    }
                }
            }
            newTexto.AppendLine("\t\t);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t$myCon = new cConn();");
            newTexto.AppendLine("\t\treturn $myCon->update(" + pNameClaseEnt + "Fields::NAME_TABLE, $arrCol, $arrVal, $arrColWhere, $arrValWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), pNameClase + ".php", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\" + pNameClase + ".php";
        }

        public string crearConn(ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            StringBuilder newTexto = new StringBuilder();
            newTexto.AppendLine("<?php");
            newTexto.AppendLine("");
            newTexto.AppendLine("class cConn");
            newTexto.AppendLine("{");
            newTexto.AppendLine("\tprivate $myConn;");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tfunction __construct()");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t/** Definimos la Cadena de Conexion */");
            newTexto.AppendLine("\t\t$this->myConn = pg_connect('host=localhost port=5432 dbname=ehogares user=postgres password=Desa2013');");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic function select($table, $arrColumnas = null, $arrColWhere = null, $arrValWhere = null, $strParamAdicionales = null)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$query = 'SELECT ';");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tif($arrColumnas != null)");
            newTexto.AppendLine("\t\t\t$query .= implode(',', $arrColumnas);");
            newTexto.AppendLine("\t\telse");
            newTexto.AppendLine("\t\t\t$query .= '*';");
            newTexto.AppendLine("\t\t$query .= ' FROM ' .$table.' ';");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tif($arrColWhere != null)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t$query .= ' WHERE ';");
            newTexto.AppendLine("\t\t\tfor($i = 0; $i < count($arrColWhere); $i++)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\t$query .= $arrColWhere[$i].' = '.$arrValWhere[$i];");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tif($strParamAdicionales != null)");
            newTexto.AppendLine("\t\t\t$query .= $strParamAdicionales;");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t$result = array();");
            newTexto.AppendLine("\t\t$rows = pg_query ($this->myConn, $query);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tif ($rows)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\twhile ($data = pg_fetch_assoc($rows))");
            newTexto.AppendLine("\t\t\t\t$result[] = $data;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn (empty ($result) ? null : $result);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic function insert($table, $rows, $values)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$query = 'INSERT INTO '.$table;");
            newTexto.AppendLine("\t\tif($rows != null)");
            newTexto.AppendLine("\t\t\t$query .= ' ('.implode(',',$rows).')';");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tfor($i = 0; $i < count($values); $i++)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif(is_string($values[$i]))");
            newTexto.AppendLine("\t\t\t\t$values[$i] = \")'\".$values[$i].\"'\";");
            newTexto.AppendLine("\t\t\tif ($values[$i] === null)");
            newTexto.AppendLine("\t\t\t\t$values[$i] = 'null';");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t$values = implode(',',$values);");
            newTexto.AppendLine("\t\t$query .= ' VALUES ('.$values.')';");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\ttry");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\techo $query;");
            newTexto.AppendLine("\t\t\t$result = pg_query($this->myConn, $query);");
            newTexto.AppendLine("\t\t\t//pg_free_result($result);");
            newTexto.AppendLine("\t\t\tpg_close($this->myConn);");
            newTexto.AppendLine("\t\t\treturn true;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tcatch (PDOException $e)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\treturn $e->getMessage();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic function update($table, $rows, $values)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$query = 'UPDATE '.$table.' SET ';");
            newTexto.AppendLine("\t\t$esPrimerElemento = true;");
            newTexto.AppendLine("\t\tfor($i = 0; $i < count($arrVals); $i++)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif(is_string($arrVals[$i]))");
            newTexto.AppendLine("\t\t\t\t$arrVals[$i] = \"'\".$arrVals[$i].\"'\";");
            newTexto.AppendLine("\t\t\tif ($arrVals[$i] === null)");
            newTexto.AppendLine("\t\t\t\t$arrVals[$i] = 'null';");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tif ($esPrimerElemento)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\t$query .= $arrCols[$i] .' = '. $arrVals[$i];");
            newTexto.AppendLine("\t\t\t\t$esPrimerElemento = false;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\telse");
            newTexto.AppendLine("\t\t\t\t$query .= ','.$arrCols[$i] .' = '. $arrVals[$i];");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t$query .= ' WHERE ';");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t$esPrimerElemento = true;");
            newTexto.AppendLine("\t\tfor($i = 0; $i < count($arrValWhere); $i++)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif(is_string($arrValWhere[$i]))");
            newTexto.AppendLine("\t\t\t\t$arrValWhere[$i] = \"'\".$arrValWhere[$i].\"'\";");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tif ($esPrimerElemento)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\t$query .= $arrColWhere[$i] .' = '. $arrValWhere[$i];");
            newTexto.AppendLine("\t\t\t\t$esPrimerElemento = false;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\telse");
            newTexto.AppendLine("\t\t\t\t$query .= ' AND '.$arrColWhere[$i] .' = '. $arrValWhere[$i];");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\ttry");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\techo $query;");
            newTexto.AppendLine("\t\t\t$result = pg_query($this->myConn, $query);");
            newTexto.AppendLine("\t\t\t//pg_free_result($result);");
            newTexto.AppendLine("\t\t\tpg_close($this->myConn);");
            newTexto.AppendLine("\t\t\treturn true;");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\tcatch (PDOException $e)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\treturn $e->getMessage();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), formulario.txtClaseConn.Text + ".php", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\" + formulario.txtClaseConn.Text + ".php";
        }
    }
}