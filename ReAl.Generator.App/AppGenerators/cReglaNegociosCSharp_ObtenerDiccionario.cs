﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using System;
using System.Collections;
using System.Data;

namespace ReAl.Generator.App.AppGenerators
{
    public static class cReglaNegociosCSharp_ObtenerDiccionario
    {
        public static string CrearModelo(DataTable dtColumns, ref FPrincipal formulario, string pNameTabla, string pNameClaseEnt, string strLlavePrimaria, ArrayList llavePrimaria)
        {
            TipoEntorno miEntorno;
            Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            bool bAdicionar = false;
            string nameCol = "";
            string tipoCol = "";

            string paramClaseConeccion = (string.IsNullOrEmpty(formulario.txtClaseConn.Text) ? "cConn" : formulario.txtClaseConn.Text);
            string paramClaseTransaccion = (string.IsNullOrEmpty(formulario.txtClaseTransaccion.Text) ? "cTransaccion" : formulario.txtClaseTransaccion.Text);
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string namespaceClases = (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class");

            string formatoFechaHora = paramClaseParametros + ".parFormatoFechaHora";
            string formatoFecha = paramClaseParametros + ".parFormatoFecha";

            string funcionCargarDataReaderAnd = null;
            string funcionCargarDataReaderOr = null;
            string funcionCargarDataTableAnd = null;
            string funcionCargarDataTableAndFromView = null;
            string funcionCargarDataTableOr = null;

            string strAlias = null;
            try
            {
                DataTable dtPrefijo = new DataTable();

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.SQLServer2000:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.Oracle:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT ALIASSTA FROM SEGTABLAS WHERE UPPER(TABLASTA) = '" + pNameTabla.ToUpper() + "'");
                        break;

                    case TipoBD.PostgreSQL:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.aliassta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" + pNameTabla + "')");
                        if (dtPrefijo.Rows.Count == 0)
                        {
                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.alias FROM seg_tablas s WHERE UPPER(s.nombretabla) = UPPER('" + pNameTabla + "')");
                        }
                        break;
                }

                if (dtPrefijo.Rows.Count > 0)
                {
                    strAlias = dtPrefijo.Rows[0][0].ToString().ToLower();
                    strAlias = strAlias.Substring(0, 1).ToUpper() + strAlias.Substring(1, strAlias.Length - 1);
                }
                else
                {
                    strAlias = pNameTabla;
                }
            }
            catch (Exception ex)
            {
                strAlias = pNameTabla;
            }

            string strAliasSel = "";
            if (formulario.chkUsarSpsSelect.Checked)
            {
                if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                    miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                    miEntorno == TipoEntorno.CSharp_WebForms ||
                    miEntorno == TipoEntorno.CSharp_WebAPI ||
                    miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
                {
                    funcionCargarDataReaderAnd = "ExecStoreProcedureDataReaderSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataReaderOr = "ExecStoreProcedureDataReaderSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAnd = "ExecStoreProcedureSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAndFromView = "ExecStoreProcedureSel(strVista";
                    funcionCargarDataTableOr = "ExecStoreProcedureSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    strAliasSel = strAlias;
                }
                else
                {
                    funcionCargarDataReaderAnd = "execStoreProcedureDataReaderSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataReaderOr = "execStoreProcedureDataReaderSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAnd = "execStoreProcedureSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAndFromView = "execStoreProcedureSel(strVista";
                    funcionCargarDataTableOr = "execStoreProcedureSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    strAliasSel = strAlias;
                }
            }
            else
            {
                if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                    miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                    miEntorno == TipoEntorno.CSharp_WebForms ||
                    miEntorno == TipoEntorno.CSharp_WebAPI ||
                    miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
                {
                    funcionCargarDataReaderAnd = "CargarDataReaderAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataReaderOr = "CargarDataReaderOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAnd = "CargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAndFromView = "CargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + "strVista";
                    funcionCargarDataTableOr = "CargarDataTableOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                }
                else
                {
                    funcionCargarDataReaderAnd = "cargarDataReaderAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataReaderOr = "cargarDataReaderOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAnd = "cargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAndFromView = "cargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "strVista";
                    funcionCargarDataTableOr = "cargarDataTableOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                }
                strAliasSel = pNameTabla;
            }

            //Obtener Diccionario
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros");
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic Dictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\treturn ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Obtener Diccionario filtrada por Arrays
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros");
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic Dictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\treturn ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, \"\");");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Obtener Diccionario filtrada por Arrays y Parametros Adiccionales
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros");
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic Dictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
            }

            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\tDataTable table = local." + funcionCargarDataTableAnd + ", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tif (table.Rows.Count > 0)");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\treturn crearDiccionario(table);");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\telse");
            newTexto.AppendLine("\t\t\t\t\treturn new Dictionary<string, " + pNameClaseEnt + ">();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            if (miEntorno != TipoEntorno.CSharp_WCF)
            {
                //Obtener Diccionario filtrada por campo
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic Dictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario(" + pNameClaseEnt + ".Fields searchField, object searchValue)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\tarrValoresWhere.Add(searchValue);");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\treturn ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //Obtener Diccionario filtrada por campo y Parametros Adicionales
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic Dictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\tarrValoresWhere.Add(searchValue);");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\treturn ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");
            }

            //Obtener Diccionario filtrada por Arrays TRANS
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros");
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic Dictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref " + paramClaseTransaccion + " localTrans)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\treturn ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, \"\", ref localTrans);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Obtener Diccionario filtrada por Arrays y Parametros Adiccionales
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros");
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic Dictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
            }

            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\tDataTable table = local." + funcionCargarDataTableAnd + ", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tif (table.Rows.Count > 0)");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\treturn crearDiccionario(table);");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\telse");
            newTexto.AppendLine("\t\t\t\t\treturn new Dictionary<string, " + pNameClaseEnt + ">();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            if (miEntorno != TipoEntorno.CSharp_WCF)
            {
                //Obtener Diccionario filtrada por campo
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic Dictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario(" + pNameClaseEnt + ".Fields searchField, object searchValue, ref " + paramClaseTransaccion + " localTrans)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\tarrValoresWhere.Add(searchValue);");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\treturn ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //Obtener Diccionario filtrada por campo y Parametros Adicionales
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic Dictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\tarrValoresWhere.Add(searchValue);");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\treturn ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");
            }

            newTexto.AppendLine("\t\tpublic Dictionary<String, " + pNameClaseEnt + "> ObtenerDiccionarioKey(" + pNameClaseEnt + ".Fields dicKey)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\treturn ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t\tpublic Dictionary<String, " + pNameClaseEnt + "> ObtenerDiccionarioKey(String strParamAdic, " + pNameClaseEnt + ".Fields dicKey)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\treturn ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdic, dicKey);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t\tpublic Dictionary<String, " + pNameClaseEnt + "> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, " + pNameClaseEnt + ".Fields dicKey)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\treturn ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, \"\", dicKey);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t\tpublic Dictionary<String, " + pNameClaseEnt + "> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, " + pNameClaseEnt + ".Fields dicKey)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
            }

            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\tDataTable table = local." + funcionCargarDataTableAnd + ", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tif (table.Rows.Count > 0)");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\treturn crearDiccionario(table, dicKey);");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\telse");
            newTexto.AppendLine("\t\t\t\t\treturn new Dictionary<string, " + pNameClaseEnt + ">();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t\tpublic Dictionary<String, " + pNameClaseEnt + "> ObtenerDiccionarioKey(" + pNameClaseEnt + ".Fields searchField, object searchValue, " + pNameClaseEnt + ".Fields dicKey)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(searchField.ToString());");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\tarrValoresWhere.Add(searchValue);");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\treturn ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t\tpublic Dictionary<String, " + pNameClaseEnt + "> ObtenerDiccionarioKey(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales, " + pNameClaseEnt + ".Fields dicKey)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(searchField.ToString());");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\tarrValoresWhere.Add(searchValue);");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\treturn ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdicionales, dicKey);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            return newTexto.ToString();
        }
    }
}