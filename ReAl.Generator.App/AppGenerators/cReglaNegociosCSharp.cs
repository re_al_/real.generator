﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using System;
using System.Collections;
using System.Data;
using System.IO;

namespace ReAl.Generator.App.AppGenerators
{
    public class cReglaNegociosCSharp
    {
        #region Methods

        public string CrearModelo(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            cAltasBajasMod cABMs = new cAltasBajasMod();
            TipoEntorno miEntorno;
            Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);

            string paramClaseConeccion = (string.IsNullOrEmpty(formulario.txtClaseConn.Text) ? "cConn" : formulario.txtClaseConn.Text);
            string paramClaseTransaccion = (string.IsNullOrEmpty(formulario.txtClaseTransaccion.Text) ? "cTransaccion" : formulario.txtClaseTransaccion.Text);
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string namespaceClases = (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class");

            string nameCol = null;
            string tipoCol = null;
            bool permiteNull = false;
            string pNameTabla = pNameClase;

            pNameClase = pNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                         pNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                         pNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                         pNameTabla.Replace("_", "").Substring(4).ToLower();
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameClase;
            string pNameClaseIn = formulario.txtPrefijoInterface.Text + pNameClase;
            string pNameClaseRn = formulario.txtPrefijoModelo.Text + pNameClase;

            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoDal.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoDal.Text;
            }

            string TipoColNull = null;
            string DescripcionCol = null;
            string strAlias = null;
            try
            {
                DataTable dtPrefijo = new DataTable();

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.SQLServer2000:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.Oracle:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT ALIASSTA FROM SEGTABLAS WHERE UPPER(TABLASTA) = '" + pNameTabla.ToUpper() + "'");
                        break;

                    case TipoBD.PostgreSQL:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.aliassta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" + pNameTabla + "')");
                        if (dtPrefijo.Rows.Count == 0)
                        {
                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.alias FROM seg_tablas s WHERE UPPER(s.nombretabla) = UPPER('" + pNameTabla + "')");
                        }
                        break;
                }

                if (dtPrefijo.Rows.Count > 0)
                {
                    strAlias = dtPrefijo.Rows[0][0].ToString().ToLower();
                    strAlias = strAlias.Substring(0, 1).ToUpper() + strAlias.Substring(1, strAlias.Length - 1);
                }
                else
                {
                    strAlias = pNameTabla;
                }
            }
            catch (Exception ex)
            {
                strAlias = pNameTabla;
            }

            if (dtColumns.Rows.Count == 0)
                return "";
            if (dtColumns.Columns.Count == 0)
                return "";

            string funcionCargarDataReaderAnd = null;
            string funcionCargarDataReaderOr = null;
            string CargarDataTableAnd = null;
            string CargarDataTableAndFromView = null;
            string CargarDataTableOr = null;

            string strAliasSel = "";
            if (formulario.chkUsarSpsSelect.Checked)
            {
                funcionCargarDataReaderAnd = "execStoreProcedureDataReaderSel(" + pNameClaseEnt + ".StrAliasTabla";
                funcionCargarDataReaderOr = "execStoreProcedureDataReaderSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                CargarDataTableAnd = "execStoreProcedureSel(" + pNameClaseEnt + ".StrAliasTabla";
                CargarDataTableAndFromView = "execStoreProcedureSel(strVista";
                CargarDataTableOr = "execStoreProcedureSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                strAliasSel = strAlias;
            }
            else
            {
                if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                    miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                    miEntorno == TipoEntorno.CSharp_WebForms ||
                    miEntorno == TipoEntorno.CSharp_WebAPI ||
                    miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
                {
                    funcionCargarDataReaderAnd = "CargarDataReaderAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataReaderOr = "CargarDataReaderOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    CargarDataTableAnd = "CFuncionesBDs.CargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    CargarDataTableAndFromView = "CFuncionesBDs.CargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + "strVista";
                    CargarDataTableOr = "CFuncionesBDs.CargarDataTableOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    strAliasSel = pNameTabla;
                }
                else
                {
                    funcionCargarDataReaderAnd = "cargarDataReaderAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataReaderOr = "cargarDataReaderOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    CargarDataTableAnd = "CFuncionesBDs.CargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    CargarDataTableAndFromView = "CFuncionesBDs.CargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "strVista";
                    CargarDataTableOr = "CFuncionesBDs.CargarDataTableOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    strAliasSel = pNameTabla;
                }
            }

            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\")) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\"));
            }

            //Obtenemos las columnas de la llave primaria
            bool bAdicionar = true;
            ArrayList llavePrimaria = new ArrayList();
            string strLlavePrimaria = "";
            string strArrLlavePrimarias = "";
            string strArrLlavePrimariasVal = "";
            dynamic bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();

                    strArrLlavePrimarias = strArrLlavePrimarias + "\t\t\t\tarrColumnasWhere.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());\r\n";
                    strArrLlavePrimariasVal = strArrLlavePrimariasVal + "\t\t\t\tarrValoresWhere.Add(" + tipoCol + nameCol + ");\r\n";
                    if (bEsPrimerElemento)
                    {
                        strLlavePrimaria = strLlavePrimaria + tipoCol + " " + tipoCol + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlavePrimaria = strLlavePrimaria + ", " + tipoCol + " " + tipoCol + nameCol;
                    }
                    llavePrimaria.Add(tipoCol + nameCol);
                }
            }
            if (string.IsNullOrEmpty(strLlavePrimaria))
            {
                strLlavePrimaria = dtColumns.Rows[0][1].ToString() + " " + dtColumns.Rows[0][1].ToString() +
                                   dtColumns.Rows[0][0].ToString();
                llavePrimaria.Add(dtColumns.Rows[0][1].ToString() + dtColumns.Rows[0][0].ToString());
            }

            //Obtenemos las columnas de los APIs
            string strApiTrans = "";
            string strApiEstado = "";
            string strUsuCre = "";
            string strFecCre = "";
            string strUsuMod = "";
            string strFecMod = "";
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strUsuCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strUsuMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strFecCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strFecMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                    strApiEstado = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                    strApiTrans = nameCol;
            }

            //Empezamos con la creacion del archivo
            string strPermiteNull = null;
            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClaseRn);
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que implementa los metodos y operaciones sobre la Tabla " + pNameTabla);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();

            newTexto.AppendLine(CabeceraTmp);

            newTexto.AppendLine("");
            newTexto.AppendLine("#region");
            newTexto.AppendLine("using System;");
            newTexto.AppendLine("using System.Globalization;");
            newTexto.AppendLine("using System.Threading;");
            newTexto.AppendLine("using System.Numerics;");
            newTexto.AppendLine("using System.Collections;");
            newTexto.AppendLine("using System.Collections.Generic;");
            newTexto.AppendLine("using System.Data;");
            newTexto.AppendLine("using System.Data.Common;");
            newTexto.AppendLine("using System.ComponentModel;");
            newTexto.AppendLine("using " + formulario.txtInfProyectoDal.Text + "; ");
            newTexto.AppendLine("using " + formulario.txtInfProyectoConn.Text + "; ");

            if (formulario.chkReAlEntidades.Checked)
                newTexto.AppendLine("using " + formulario.txtInfProyectoDal.Text + "." + formulario.txtNamespaceEntidades.Text + ";");
            if (formulario.chkReAlInterface.Checked)
                newTexto.AppendLine("using " + formulario.txtInfProyectoDal.Text + "." + formulario.txtNamespaceInterface.Text + ";");
            if (miEntorno == TipoEntorno.CSharp_WCF)
            {
                newTexto.AppendLine("using System.ServiceModel;");
            }
            if (miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
            {
                newTexto.AppendLine("using System.Windows.Forms;");
            }
            if (miEntorno == TipoEntorno.CSharp_WebForms || miEntorno == TipoEntorno.CSharp)
            {
                newTexto.AppendLine("using System.Web.UI.WebControls;");
            }
            newTexto.AppendLine("#endregion");
            newTexto.AppendLine("");

            if (formulario.chkClassParcial.Checked && miEntorno != TipoEntorno.CSharp_NetCore_V2)
            {
                System.Text.StringBuilder newTextoParcial = new System.Text.StringBuilder();
                newTextoParcial.Append(newTexto.ToString());

                newTextoParcial.AppendLine("namespace " + formulario.txtInfProyectoDal.Text + "." + formulario.txtNamespaceNegocios.Text);
                newTextoParcial.AppendLine("{");
                newTextoParcial.AppendLine("\tpublic " + (formulario.chkClassParcial.Checked ? "partial " : "") + "class " + pNameClaseRn + ": " + pNameClaseIn);
                newTextoParcial.AppendLine("\t{");

                newTextoParcial.AppendLine("\t\tpublic const string strNombreLisForm = \"Listado de " + pNameTabla + "\";");
                newTextoParcial.AppendLine("\t\tpublic const string strNombreDocForm = \"Detalle de " + pNameTabla + "\";");
                newTextoParcial.AppendLine("\t\tpublic const string strNombreForm = \"Registro de " + pNameTabla + "\";");

                //GetDescripcion del Objeto
                newTextoParcial.AppendLine("\t\t/// <summary>");
                newTextoParcial.AppendLine("\t\t/// \tFuncion que obtiene la Descripcion de Determinado Campo");
                newTextoParcial.AppendLine("\t\t/// </summary>");
                newTextoParcial.AppendLine("\t\t/// <param name=\"myField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTextoParcial.AppendLine("\t\t///     <para>");
                newTextoParcial.AppendLine("\t\t/// \t\t Campo solicitado");
                newTextoParcial.AppendLine("\t\t///     </para>");
                newTextoParcial.AppendLine("\t\t/// </param>");
                newTextoParcial.AppendLine("\t\t/// <param name=\"idioma\" type=\"System.Globalization.CultureInfo\">");
                newTextoParcial.AppendLine("\t\t///     <para>");
                newTextoParcial.AppendLine("\t\t/// \t\t Idioma");
                newTextoParcial.AppendLine("\t\t///     </para>");
                newTextoParcial.AppendLine("\t\t/// </param>");
                newTextoParcial.AppendLine("\t\t/// <returns>");
                newTextoParcial.AppendLine("\t\t/// \tValor del Tipo STRING que describe al parametro");
                newTextoParcial.AppendLine("\t\t/// </returns>");
                newTextoParcial.AppendLine("\t\tpublic static string GetDesc(" + pNameClaseEnt + ".Fields myField, CultureInfo idioma = null)");
                newTextoParcial.AppendLine("\t\t{");
                newTextoParcial.AppendLine("\t\t\ttry");
                newTextoParcial.AppendLine("\t\t\t{");
                newTextoParcial.AppendLine("\t\t\t\tif (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;");
                newTextoParcial.AppendLine("\t\t\t\t");
                //newTextoParcial.AppendLine(vbTab & vbTab & vbTab & vbTab & "string strDesc = " & Chr(34) & Chr(34) & ";" )

                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();
                    strPermiteNull = dtColumns.Rows[iCol]["PermiteNull"].ToString();
                    DescripcionCol = dtColumns.Rows[iCol]["COL_DESC"].ToString();

                    TipoColNull = tipoCol;
                    if (tipoCol != "String")
                    {
                        if (strPermiteNull == "Yes")
                        {
                            if (TipoColNull.Contains("[]"))
                            {
                                TipoColNull = tipoCol;
                            }
                            else
                            {
                                TipoColNull = tipoCol + "?";
                            }
                        }
                    }

                    newTextoParcial.AppendLine("\t\t\t\tif (" + pNameClaseEnt + ".Fields." + nameCol + " == myField)");
                    newTextoParcial.AppendLine("\t\t\t\t{");
                    if (string.IsNullOrEmpty(DescripcionCol))
                    {
                        newTextoParcial.AppendLine("\t\t\t\t\tif (idioma.Name.ToUpper().StartsWith(\"ES\")) return \"Valor de tipo " + tipoCol + " para " + nameCol.Replace(pNameTabla, "") + "\";");
                        newTextoParcial.AppendLine("\t\t\t\t\tif (idioma.Name.ToUpper().StartsWith(\"EN\")) return \"Valor de tipo " + tipoCol + " para " + nameCol.Replace(pNameTabla, "") + "\";");
                        newTextoParcial.AppendLine("\t\t\t\t\treturn \"Valor de tipo " + tipoCol + " para " + nameCol.Replace(pNameTabla, "") + "\";");
                    }
                    else
                    {
                        newTextoParcial.AppendLine("\t\t\t\t\tif (idioma.Name.ToUpper().StartsWith(\"ES\")) return \"" + DescripcionCol.Replace("\r\n", "") + "\";");
                        newTextoParcial.AppendLine("\t\t\t\t\tif (idioma.Name.ToUpper().StartsWith(\"EN\")) return \"" + DescripcionCol.Replace("\r\n", "") + "\";");
                        newTextoParcial.AppendLine("\t\t\t\t\treturn \"" + DescripcionCol.Replace("\r\n", "") + "\";");
                    }
                    newTextoParcial.AppendLine("\t\t\t\t}");
                }

                newTextoParcial.AppendLine("\t\t\t\treturn string.Empty;");
                newTextoParcial.AppendLine("\t\t\t}");
                newTextoParcial.AppendLine("\t\t\tcatch (Exception exp)");
                newTextoParcial.AppendLine("\t\t\t{");
                newTextoParcial.AppendLine("\t\t\t\tthrow exp;");
                newTextoParcial.AppendLine("\t\t\t}");
                newTextoParcial.AppendLine("\t\t}");
                newTextoParcial.AppendLine("\t\t");

                //GetFieldRequired del Objeto
                newTextoParcial.AppendLine("\t\t/// <summary>");
                newTextoParcial.AppendLine("\t\t/// \tFuncion que obtiene el Error de Determinado Campo");
                newTextoParcial.AppendLine("\t\t/// </summary>");
                newTextoParcial.AppendLine("\t\t/// <param name=\"myField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTextoParcial.AppendLine("\t\t///     <para>");
                newTextoParcial.AppendLine("\t\t/// \t\t Campo solicitado");
                newTextoParcial.AppendLine("\t\t///     </para>");
                newTextoParcial.AppendLine("\t\t/// </param>");
                newTextoParcial.AppendLine("\t\t/// <param name=\"idioma\" type=\"System.Globalization.CultureInfo\">");
                newTextoParcial.AppendLine("\t\t///     <para>");
                newTextoParcial.AppendLine("\t\t/// \t\t Idioma");
                newTextoParcial.AppendLine("\t\t///     </para>");
                newTextoParcial.AppendLine("\t\t/// </param>");
                newTextoParcial.AppendLine("\t\t/// <returns>");
                newTextoParcial.AppendLine("\t\t/// \tValor del Tipo STRING que describe al parametro");
                newTextoParcial.AppendLine("\t\t/// </returns>");
                newTextoParcial.AppendLine("\t\tpublic static string GetError(" + pNameClaseEnt + ".Fields myField, CultureInfo idioma = null)");
                newTextoParcial.AppendLine("\t\t{");
                newTextoParcial.AppendLine("\t\t\ttry");
                newTextoParcial.AppendLine("\t\t\t{");
                newTextoParcial.AppendLine("\t\t\t\tif (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;");
                newTextoParcial.AppendLine("\t\t\t\t");
                //newTextoParcial.AppendLine(vbTab & vbTab & vbTab & vbTab & "string strDesc = " & Chr(34) & Chr(34) & ";" )

                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();
                    strPermiteNull = dtColumns.Rows[iCol]["PermiteNull"].ToString();
                    TipoColNull = tipoCol;
                    if (tipoCol != "String")
                    {
                        if (strPermiteNull == "Yes")
                        {
                            if (TipoColNull.Contains("[]"))
                            {
                                TipoColNull = tipoCol;
                            }
                            else
                            {
                                TipoColNull = tipoCol + "?";
                            }
                        }
                    }

                    newTextoParcial.AppendLine("\t\t\t\tif (" + pNameClaseEnt + ".Fields." + nameCol + " == myField)");
                    newTextoParcial.AppendLine("\t\t\t\t{");
                    newTextoParcial.AppendLine("\t\t\t\t\tif (idioma.Name.ToUpper().StartsWith(\"ES\")) return \"" + tipoCol + " para " + nameCol.Replace(pNameTabla, "") + "\";");
                    newTextoParcial.AppendLine("\t\t\t\t\tif (idioma.Name.ToUpper().StartsWith(\"EN\")) return \"" + tipoCol + " para " + nameCol.Replace(pNameTabla, "") + "\";");
                    newTextoParcial.AppendLine("\t\t\t\t\treturn \"" + tipoCol + " para " + nameCol.Replace(pNameTabla, "") + "\";");
                    newTextoParcial.AppendLine("\t\t\t\t}");
                }

                newTextoParcial.AppendLine("\t\t\t\treturn string.Empty;");
                newTextoParcial.AppendLine("\t\t\t}");
                newTextoParcial.AppendLine("\t\t\tcatch (Exception exp)");
                newTextoParcial.AppendLine("\t\t\t{");
                newTextoParcial.AppendLine("\t\t\t\tthrow exp;");
                newTextoParcial.AppendLine("\t\t\t}");
                newTextoParcial.AppendLine("\t\t}");
                newTextoParcial.AppendLine("\t\t");

                //GetFieldRequired del Objeto
                newTextoParcial.AppendLine("\t\t/// <summary>");
                newTextoParcial.AppendLine("\t\t/// \tFuncion que obtiene el Nombre de Determinado Campo");
                newTextoParcial.AppendLine("\t\t/// </summary>");
                newTextoParcial.AppendLine("\t\t/// <param name=\"myField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTextoParcial.AppendLine("\t\t///     <para>");
                newTextoParcial.AppendLine("\t\t/// \t\t Campo solicitado");
                newTextoParcial.AppendLine("\t\t///     </para>");
                newTextoParcial.AppendLine("\t\t/// </param>");
                newTextoParcial.AppendLine("\t\t/// <param name=\"idioma\" type=\"System.Globalization.CultureInfo\">");
                newTextoParcial.AppendLine("\t\t///     <para>");
                newTextoParcial.AppendLine("\t\t/// \t\t Idioma");
                newTextoParcial.AppendLine("\t\t///     </para>");
                newTextoParcial.AppendLine("\t\t/// </param>");
                newTextoParcial.AppendLine("\t\t/// <returns>");
                newTextoParcial.AppendLine("\t\t/// \tValor del Tipo STRING que describe al parametro");
                newTextoParcial.AppendLine("\t\t/// </returns>");
                newTextoParcial.AppendLine("\t\tpublic static string GetColumn(" + pNameClaseEnt + ".Fields myField, CultureInfo idioma = null)");
                newTextoParcial.AppendLine("\t\t{");
                newTextoParcial.AppendLine("\t\t\ttry");
                newTextoParcial.AppendLine("\t\t\t{");
                newTextoParcial.AppendLine("\t\t\t\tif (idioma == null) idioma = Thread.CurrentThread.CurrentCulture;");
                newTextoParcial.AppendLine("\t\t\t\t");
                //newTextoParcial.AppendLine(vbTab & vbTab & vbTab & vbTab & "string strDesc = " & Chr(34) & Chr(34) & ";" )

                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();
                    strPermiteNull = dtColumns.Rows[iCol]["PermiteNull"].ToString();
                    TipoColNull = tipoCol;
                    if (tipoCol != "String")
                    {
                        if (strPermiteNull == "Yes")
                        {
                            if (TipoColNull.Contains("[]"))
                            {
                                TipoColNull = tipoCol;
                            }
                            else
                            {
                                TipoColNull = tipoCol + "?";
                            }
                        }
                    }

                    newTextoParcial.AppendLine("\t\t\t\tif (" + pNameClaseEnt + ".Fields." + nameCol + " == myField)");
                    newTextoParcial.AppendLine("\t\t\t\t{");
                    newTextoParcial.AppendLine("\t\t\t\t\tif (idioma.Name.ToUpper().StartsWith(\"ES\")) return \"" + nameCol.Replace(pNameTabla, "") + ":\";");
                    newTextoParcial.AppendLine("\t\t\t\t\tif (idioma.Name.ToUpper().StartsWith(\"EN\")) return \"" + nameCol.Replace(pNameTabla, "") + ":\";");
                    newTextoParcial.AppendLine("\t\t\t\t\treturn \"" + nameCol.Replace(pNameTabla, "") + ":\";");
                    newTextoParcial.AppendLine("\t\t\t\t}");
                }

                newTextoParcial.AppendLine("\t\t\t\treturn string.Empty;");
                newTextoParcial.AppendLine("\t\t\t}");
                newTextoParcial.AppendLine("\t\t\tcatch (Exception exp)");
                newTextoParcial.AppendLine("\t\t\t{");
                newTextoParcial.AppendLine("\t\t\t\tthrow exp;");
                newTextoParcial.AppendLine("\t\t\t}");
                newTextoParcial.AppendLine("\t\t}");
                newTextoParcial.AppendLine("\t\t");

                newTextoParcial.AppendLine("\t}");
                newTextoParcial.AppendLine("}");

                if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "Strings") == false)
                {
                    Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "Strings");
                }

                if (miEntorno == TipoEntorno.CSharp_WCF)
                {
                    CFunciones.CrearArchivo(newTextoParcial.ToString(), pNameClaseRn + "Strings.svc.cs", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "Strings\\");
                }
                else
                {
                    CFunciones.CrearArchivo(newTextoParcial.ToString(), pNameClaseRn + "Strings.cs", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "Strings\\");
                }

                //Return newTexto.ToString()
                //Return PathDesktop & "\" & Replace(formulario.txtNamespaceNegocios.Text, ".", "\") & "Strings\" & pNameClase & "Strings.cs"
            }

            newTexto.AppendLine("namespace " + formulario.txtInfProyectoDal.Text + "." + formulario.txtNamespaceNegocios.Text);
            newTexto.AppendLine("{");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("\tpublic " + (formulario.chkClassParcial.Checked ? "partial " : "") + "class " + pNameClaseRn + ": " + pNameClaseIn);
                newTexto.AppendLine("\t{");
                newTexto.AppendLine("\t\t//Debe implementar la Interface (Alt + Shift + F10)\r\n");
                newTexto.AppendLine("\t\t#region " + pNameClaseIn + " Members\r\n");
            }
            else
            {
                newTexto.AppendLine("\tpublic " + (formulario.chkClassParcial.Checked ? "partial " : "") + "class " + pNameClaseRn);
                newTexto.AppendLine("\t{");
            }

            //newTexto.AppendLine(vbTab & vbTab & "public dynamic DynamicCast(object valor, Type destino)")
            //newTexto.AppendLine(vbTab & vbTab & "{")
            //newTexto.AppendLine(vbTab & vbTab & vbTab & "if (DBNull.Value.Equals(valor)) ")
            //newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "return null;")
            //newTexto.AppendLine(vbTab & vbTab & vbTab & "if (destino == null)")
            //newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "throw new ArgumentNullException(" & Chr(34) & "El parametro que define el Tipo destino no puede ser NULO." & Chr(34) & ");")
            //newTexto.AppendLine(vbTab & vbTab & vbTab & "")
            //newTexto.AppendLine(vbTab & vbTab & vbTab & "var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;")
            //newTexto.AppendLine(vbTab & vbTab & vbTab & "return Convert.ChangeType(valor, miTipo);")
            //newTexto.AppendLine(vbTab & vbTab & "}")
            //newTexto.AppendLine(vbTab & vbTab & "")

            newTexto.AppendLine("\t\t#region Reflection\r\n");

            if (miEntorno != TipoEntorno.CSharp_NetCore_V2)
            {
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// Metodo que devuelve el Script SQL de la Tabla");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <returns>Script SQL</returns>");
                newTexto.AppendLine("\t\tpublic string GetTableScript()");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tTableClass tabla = new TableClass(typeof(" + pNameClaseEnt + "));");
                newTexto.AppendLine("\t\t\treturn tabla.CreateTableScript();");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");
            }

            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// Metodo para castear Dinamicamente un Tipo");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"valor\">Tipo a ser casteado</param>");
            newTexto.AppendLine("\t\t/// <param name=\"myField\">Enum de la columna</param>");
            newTexto.AppendLine("\t\t/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>");

            //Metodo que devuelve un Tipo Parseado a partir de un Objeto
            newTexto.AppendLine("\t\tpublic dynamic GetColumnType(object valor, " + pNameClaseEnt + ".Fields myField)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (DBNull.Value.Equals(valor)) ");
            newTexto.AppendLine("\t\t\t\treturn null;");
            newTexto.AppendLine("\t\t\tType destino = typeof(" + pNameClaseEnt + ").GetProperty(myField.ToString()).PropertyType;");
            newTexto.AppendLine("\t\t\tvar miTipo = Nullable.GetUnderlyingType(destino) ?? destino;");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tTypeConverter tc = TypeDescriptor.GetConverter(miTipo);");
            newTexto.AppendLine("\t\t\t\treturn tc.ConvertFrom(valor);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\treturn Convert.ChangeType(valor, miTipo);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("");

            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// Metodo para castear Dinamicamente un Tipo");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"valor\">Tipo a ser casteado</param>");
            newTexto.AppendLine("\t\t/// <param name=\"strField\">Nombre de la columna</param>");
            newTexto.AppendLine("\t\t/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>");

            //Metodo que devuelve un Tipo Parseado a partir de un Objeto
            newTexto.AppendLine("\t\tpublic dynamic GetColumnType(object valor, string strField)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (DBNull.Value.Equals(valor)) ");
            newTexto.AppendLine("\t\t\t\treturn null;");
            newTexto.AppendLine("\t\t\tType destino = typeof(" + pNameClaseEnt + ").GetProperty(strField).PropertyType;");
            newTexto.AppendLine("\t\t\tvar miTipo = Nullable.GetUnderlyingType(destino) ?? destino;");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tTypeConverter tc = TypeDescriptor.GetConverter(miTipo);");
            newTexto.AppendLine("\t\t\t\treturn tc.ConvertFrom(valor);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\treturn Convert.ChangeType(valor, miTipo);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("");

            //Obtener datos por reflection
            newTexto.AppendLine("/// <summary>");
            newTexto.AppendLine("/// Inserta una valor a una propiedad de un objeto instanciado");
            newTexto.AppendLine("/// </summary>");
            newTexto.AppendLine("/// <param name=\"obj\">Objeto instanciado</param>");
            newTexto.AppendLine("/// <param name=\"strPropiedad\">Es el nombre de la propiedad</param>");
            newTexto.AppendLine("/// <param name=\"dynValor\">Es el valor que se insertara a la propiedad</param>");
            newTexto.AppendLine("public void SetDato(ref " + pNameClaseEnt + " obj, string strPropiedad, dynamic dynValor)");
            newTexto.AppendLine("{");
            newTexto.AppendLine("\tif (obj == null) throw new ArgumentNullException();");
            newTexto.AppendLine("\tobj.GetType().GetProperty(strPropiedad).SetValue(obj, GetColumnType(dynValor, strPropiedad), null);");
            newTexto.AppendLine("}");
            newTexto.AppendLine("");

            newTexto.AppendLine("/// <summary>");
            newTexto.AppendLine("/// Obtiene el valor de una propiedad de un objeto instanciado");
            newTexto.AppendLine("/// </summary>");
            newTexto.AppendLine("/// <param name=\"obj\">Objeto instanciado</param>");
            newTexto.AppendLine("/// <param name=\"strPropiedad\">El nombre de la propiedad de la que se obtendra el valor</param>");
            newTexto.AppendLine("/// <returns>Devuelve el valor del a propiedad seleccionada</returns>");
            newTexto.AppendLine("public dynamic GetDato(ref " + pNameClaseEnt + " obj, string strPropiedad)");
            newTexto.AppendLine("{");
            newTexto.AppendLine("\tif (obj == null) return null;");
            newTexto.AppendLine("\tvar propertyInfo = obj.GetType().GetProperty(strPropiedad);");
            newTexto.AppendLine("\treturn GetColumnType(propertyInfo.GetValue(obj, null), strPropiedad);");
            newTexto.AppendLine("}");
            newTexto.AppendLine("");

            //CreatePk
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que obtiene la llave primaria unica de la tabla " + pNameTabla + " a partir de una cadena");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"args\" type=\"string[]\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Cadena desde la que se construye el identificador unico de la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Identificador unico de la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");
            newTexto.AppendLine("\t\tpublic string CreatePk(string[] args)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\treturn args[0];");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t\t#endregion \r\n");

            //Metodos de ObtenerObjeto
            newTexto.AppendLine("\t\t#region ObtenerObjeto\r\n");
            newTexto.AppendLine(cReglaNegociosCSharp_ObtenerObjeto.CrearModelo(dtColumns, ref formulario, pNameTabla, pNameClaseEnt, strLlavePrimaria, llavePrimaria, strUsuCre));
            newTexto.AppendLine("\t\t#endregion\r\n");

            //Metodos de ObtenerLista
            newTexto.AppendLine("\t\t#region ObtenerLista\r\n");
            newTexto.AppendLine(cReglaNegociosCSharp_ObtenerLista.CrearModelo(dtColumns, ref formulario, pNameTabla, pNameClaseEnt, strLlavePrimaria, llavePrimaria));
            newTexto.AppendLine("\t\t#endregion \r\n");

            //Metodos de ObtenerCola y ObtenerPila
            newTexto.AppendLine("\t\t#region ObtenerCola y Obtener Pila\r\n");
            newTexto.AppendLine(cReglaNegociosCSharp_ObtenerCola.CrearModelo(dtColumns, ref formulario, pNameTabla, pNameClaseEnt, strLlavePrimaria, llavePrimaria));
            newTexto.AppendLine("\t\t#endregion \r\n");

            //Metodos de ObtenerCola y ObtenerPila
            newTexto.AppendLine("\t\t#region ObtenerDataTable\r\n");
            newTexto.AppendLine(cReglaNegociosCSharp_ObtenerDataTable.CrearModelo(dtColumns, ref formulario, pNameTabla, pNameClaseEnt, strLlavePrimaria, llavePrimaria));
            newTexto.AppendLine("\t\t#endregion \r\n");

            //Metodos de ObtenerDiccionario
            newTexto.AppendLine("\t\t#region ObtenerDiccionario\r\n");
            newTexto.AppendLine(cReglaNegociosCSharp_ObtenerDiccionario.CrearModelo(dtColumns, ref formulario, pNameTabla, pNameClaseEnt, strLlavePrimaria, llavePrimaria));
            newTexto.AppendLine("\t\t#endregion \r\n");

            //Metodos de ejecucion de SPs en el Objeto
            newTexto.AppendLine("\t\t#region ObjetoASp\r\n");
            newTexto.AppendLine(cReglaNegociosCSharp_ObjetoToSp.CrearModelo(dtColumns, ref formulario, pNameTabla, pNameClaseEnt, strLlavePrimaria, llavePrimaria));
            newTexto.AppendLine("\t\t#endregion \r\n");

            //Metodos de Funciones SUM MIN MAX AVG
            newTexto.AppendLine("\t\t#region FuncionesAgregadas\r\n");
            newTexto.AppendLine(cReglaNegociosCSharp_FuncionesAgregadas.CrearModelo(dtColumns, ref formulario, pNameTabla, pNameClaseEnt, strLlavePrimaria, llavePrimaria));
            newTexto.AppendLine("\t\t#endregion \r\n");

            //Metodos de Insercion

            if (formulario.chkUsarSps.Checked)
            {
                newTexto.AppendLine("\t\t#region ABMs SP\r\n");
                newTexto.AppendLine(cABMs.CrearABMsProcAlm(dtColumns, pNameTabla, ref formulario));
                newTexto.AppendLine("\t\t#endregion \r\n");
                newTexto.AppendLine("\t\t#region ABMs Query\r\n");
                newTexto.AppendLine(cABMs.CrearABMsCodigo(dtColumns, pNameTabla, ref formulario, true));
                newTexto.AppendLine("\t\t#endregion \r\n");
            }
            else
            {
                newTexto.AppendLine("\t\t#region ABMs\r\n");
                newTexto.AppendLine(cABMs.CrearABMsCodigo(dtColumns, pNameTabla, ref formulario, false));
                newTexto.AppendLine("\t\t#endregion \r\n");
            }

            //Metodos par allenar DropdownList COmboBox, DataGrids y DataGridViews
            newTexto.AppendLine("\t\t#region Llenado de elementos\r\n");
            newTexto.AppendLine(cReglaNegociosCSharp_GridCombo.CrearModelo(dtColumns, ref formulario, pNameTabla, pNameClaseEnt, strLlavePrimaria, llavePrimaria));
            newTexto.AppendLine("\t\t#endregion \r\n");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("\r\n\t\t#endregion\r\n");
            }

            newTexto.AppendLine("\t\t#region Funciones Internas\r\n");
            //Crear Objeto a partir de un DataRow
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que devuelve un objeto a partir de un DataRow");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"row\" type=\"System.Data.DataRow\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t DataRow con el conjunto de Datos recuperados ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Objeto " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tinternal " + pNameClaseEnt + " crearObjeto(DataRow row)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar obj = new " + pNameClaseEnt + "();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                permiteNull = (dtColumns.Rows[iCol]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false);
                newTexto.AppendLine("\t\t\tobj." + nameCol + " = GetColumnType(row[" + pNameClaseEnt + ".Fields." + nameCol + ".ToString()], " + pNameClaseEnt + ".Fields." + nameCol + ");");
            }
            if (formulario.chkPropertyEvents.Checked)
            {
                newTexto.AppendLine("\t\t\t//Gestion de Estados");
                newTexto.AppendLine("\t\t\tobj.EntityState = Microsoft.EntityFrameworkCore.EntityState.Unchanged;");
                newTexto.AppendLine("\t\t\t");
            }
            newTexto.AppendLine("\t\t\treturn obj;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("");

            //Crear Objeto a partir de un DataRow con columnas filtradas
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que devuelve un objeto a partir de un DataRow");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"row\" type=\"System.Data.DataRow\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t DataRow con el conjunto de Datos recuperados ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Objeto " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tinternal " + pNameClaseEnt + " crearObjetoRevisado(DataRow row)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar obj = new " + pNameClaseEnt + "();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                permiteNull = (dtColumns.Rows[iCol]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false);
                newTexto.AppendLine("\t\t\tif (row.Table.Columns.Contains(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString()))");
                newTexto.AppendLine("\t\t\t\tobj." + nameCol + " = GetColumnType(row[" + pNameClaseEnt + ".Fields." + nameCol + ".ToString()], " + pNameClaseEnt + ".Fields." + nameCol + ");");
            }
            if (formulario.chkPropertyEvents.Checked)
            {
                newTexto.AppendLine("\t\t\t//Gestion de Estados");
                newTexto.AppendLine("\t\t\tobj.EntityState = Microsoft.EntityFrameworkCore.EntityState.Unchanged;");
                newTexto.AppendLine("\t\t\t");
            }
            newTexto.AppendLine("\t\t\treturn obj;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("");

            //Crear Lista de Objetos
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que crea una Lista de objetos a partir de un DataTable");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dt" + pNameTabla + "\" type=\"System.Data.DateTable\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t DataTable con el conjunto de Datos recuperados ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Lista de Objetos " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tinternal List<" + pNameClaseEnt + "> crearLista(DataTable dt" + pNameTabla + ")");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar list = new List<" + pNameClaseEnt + ">();");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tforeach (DataRow row in dt" + pNameTabla + ".Rows)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tvar obj = crearObjeto(row);");
            newTexto.AppendLine("\t\t\t\tlist.Add(obj);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\treturn list;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Crear Lista de Objetos Segun columna
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dt" + pNameTabla + "\" type=\"System.Data.DateTable\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t DataTable con el conjunto de Datos recuperados ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Lista de Objetos " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tinternal List<" + pNameClaseEnt + "> crearListaRevisada(DataTable dt" + pNameTabla + ")");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tList<" + pNameClaseEnt + "> list = new List<" + pNameClaseEnt + ">();");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tforeach (DataRow row in dt" + pNameTabla + ".Rows)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tvar obj = crearObjetoRevisado(row);");
            newTexto.AppendLine("\t\t\t\tlist.Add(obj);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\treturn list;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Crear Cola de Objetos
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que crea una Lista de objetos a partir de un DataTable");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dt" + pNameTabla + "\" type=\"System.Data.DateTable\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t DataTable con el conjunto de Datos recuperados ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Cola de Objetos " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tinternal Queue<" + pNameClaseEnt + "> crearCola(DataTable dt" + pNameTabla + ")");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tQueue<" + pNameClaseEnt + "> cola = new Queue<" + pNameClaseEnt + ">();");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\t" + pNameClaseEnt + " obj = new " + pNameClaseEnt + "();");
            newTexto.AppendLine("\t\t\tforeach (DataRow row in dt" + pNameTabla + ".Rows)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tobj = crearObjeto(row);");
            newTexto.AppendLine("\t\t\t\tcola.Enqueue(obj);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\treturn cola;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Crear Pila de Objetos
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que crea una Lista de objetos a partir de un DataTable");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dt" + pNameTabla + "\" type=\"System.Data.DateTable\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t DataTable con el conjunto de Datos recuperados ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Pila de Objetos " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tinternal Stack<" + pNameClaseEnt + "> crearPila(DataTable dt" + pNameTabla + ")");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tStack<" + pNameClaseEnt + "> pila = new Stack<" + pNameClaseEnt + ">();");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tforeach (DataRow row in dt" + pNameTabla + ".Rows)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tvar obj = crearObjeto(row);");
            newTexto.AppendLine("\t\t\t\tpila.Push(obj);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\treturn pila;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Crear Diccionario de Objetos
            int iCountPk = 0;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    iCountPk = iCountPk + 1;
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                }
            }

            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que crea un Dicionario a partir de un DataTable");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dt" + pNameTabla + "\" type=\"System.Data.DateTable\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t DataTable con el conjunto de Datos recuperados ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Diccionario de Objetos " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tinternal Dictionary<String, " + pNameClaseEnt + "> crearDiccionario(DataTable dt" + pNameTabla + ")");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tDictionary<String, " + pNameClaseEnt + ">  miDic = new Dictionary<String, " + pNameClaseEnt + ">();");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tforeach (DataRow row in dt" + pNameTabla + ".Rows)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tvar obj = crearObjeto(row);");
            if (iCountPk == 1)
            {
                newTexto.AppendLine("\t\t\t\tmiDic.Add(obj." + nameCol + ".ToString(), obj);");
            }
            else
            {
                newTexto.AppendLine("\t\t\t\tmiDic.Add(obj.GetHashCode().ToString(), obj);");
            }
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\treturn miDic;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Crear HashTable
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que crea un Dicionario a partir de un DataTable");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dt" + pNameTabla + "\" type=\"System.Data.DateTable\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t DataTable con el conjunto de Datos recuperados ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t HashTable de Objetos " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tinternal Hashtable crearHashTable(DataTable dt" + pNameTabla + ")");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tHashtable miTabla = new Hashtable();");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tforeach (DataRow row in dt" + pNameTabla + ".Rows)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tvar obj = crearObjeto(row);");
            if (iCountPk == 1)
            {
                newTexto.AppendLine("\t\t\t\tmiTabla.Add(obj." + nameCol + ".ToString(), obj);");
            }
            else
            {
                newTexto.AppendLine("\t\t\t\tmiTabla.Add(obj.GetHashCode().ToString(), obj);");
            }
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\treturn miTabla;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Crear Diccionario de Objetos por columnas disponibles
            int iCountPkVista = 0;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    iCountPkVista = iCountPkVista + 1;
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();
                }
            }

            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dt" + pNameTabla + "\" type=\"System.Data.DateTable\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t DataTable con el conjunto de Datos recuperados ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Diccionario de Objetos " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tinternal Dictionary<String, " + pNameClaseEnt + "> crearDiccionarioRevisado(DataTable dt" + pNameTabla + ")");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tDictionary<String, " + pNameClaseEnt + ">  miDic = new Dictionary<String, " + pNameClaseEnt + ">();");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tforeach (DataRow row in dt" + pNameTabla + ".Rows)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tvar obj = crearObjetoRevisado(row);");
            if (iCountPkVista == 1)
            {
                newTexto.AppendLine("\t\t\t\tmiDic.Add(obj." + nameCol + ".ToString(), obj);");
            }
            else
            {
                newTexto.AppendLine("\t\t\t\tmiDic.Add(obj.GetHashCode().ToString(), obj);");
            }
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\treturn miDic;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t\tinternal Dictionary<String, " + pNameClaseEnt + "> crearDiccionario(DataTable dt" + pNameTabla + ", " + pNameClaseEnt + ".Fields dicKey)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tDictionary<String, " + pNameClaseEnt + ">  miDic = new Dictionary<String, " + pNameClaseEnt + ">();");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tforeach (DataRow row in dt" + pNameTabla + ".Rows)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tvar obj = crearObjeto(row);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tvar nameOfProperty = dicKey.ToString();");
            newTexto.AppendLine("\t\t\t\tvar propertyInfo = obj.GetType().GetProperty(nameOfProperty);");
            newTexto.AppendLine("\t\t\t\tvar value = propertyInfo.GetValue(obj, null);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tmiDic.Add(value.ToString(), obj);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\treturn miDic;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t\tpublic void Dispose()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tGC.SuppressFinalize(this);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tprotected void Finalize()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tDispose();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t#endregion\r\n");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("}");

            if (miEntorno == TipoEntorno.CSharp_WCF)
            {
                CFunciones.CrearArchivo(newTexto.ToString(), pNameClaseRn + ".svc.cs", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");
            }
            else
            {
                CFunciones.CrearArchivo(newTexto.ToString(), pNameClaseRn + ".cs", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");
            }

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\" + pNameClaseRn +
                   ".cs" + (formulario.chkClassParcial.Checked
                       ? "\r\n" + PathDesktop + "\\" +
                         formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\" + pNameClaseRn +
                         "Strings.cs"
                       : "");
        }

        #endregion Methods
    }
}