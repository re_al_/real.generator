﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using System;
using System.Collections;
using System.Data;

namespace ReAl.Generator.App.AppGenerators
{
    public static class cReglaNegociosCSharp_ObtenerObjeto
    {
        private static bool SetBoolean = false;
        private static bool SetByte = false;
        private static bool SetDateTime = true;
        private static bool SetDecimal = false;
        private static bool SetDefault = true;
        private static bool SetInt = false;
        private static bool SetInt32 = false;
        private static bool SetString = true;

        public static string CrearModelo(DataTable dtColumns, ref FPrincipal formulario, string pNameTabla, string pNameClaseEnt, string strLlavePrimaria, ArrayList llavePrimaria, string strUsuCreColum)
        {
            TipoEntorno miEntorno;
            Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            bool bAdicionar = false;
            string nameCol = "";
            string tipoCol = "";

            string paramClaseConeccion = (string.IsNullOrEmpty(formulario.txtClaseConn.Text) ? "cConn" : formulario.txtClaseConn.Text);
            string paramClaseTransaccion = (string.IsNullOrEmpty(formulario.txtClaseTransaccion.Text) ? "cTransaccion" : formulario.txtClaseTransaccion.Text);
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string namespaceClases = (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class");

            string formatoFechaHora = paramClaseParametros + ".parFormatoFechaHora";
            string formatoFecha = paramClaseParametros + ".parFormatoFecha";

            string funcionCargarDataReaderAnd = null;
            string funcionCargarDataReaderOr = null;
            string funcionCargarDataTableAnd = null;
            string funcionCargarDataTableAndFromView = null;
            string funcionCargarDataTableOr = null;

            string strAlias = null;
            try
            {
                DataTable dtPrefijo = new DataTable();

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.SQLServer2000:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.Oracle:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT ALIASSTA FROM SEGTABLAS WHERE UPPER(TABLASTA) = '" + pNameTabla.ToUpper() + "'");
                        break;

                    case TipoBD.PostgreSQL:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.aliassta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" + pNameTabla + "')");
                        if (dtPrefijo.Rows.Count == 0)
                        {
                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.alias FROM seg_tablas s WHERE UPPER(s.nombretabla) = UPPER('" + pNameTabla + "')");
                        }
                        break;
                }

                if (dtPrefijo.Rows.Count > 0)
                {
                    strAlias = dtPrefijo.Rows[0][0].ToString().ToLower();
                    strAlias = strAlias.Substring(0, 1).ToUpper() + strAlias.Substring(1, strAlias.Length - 1);
                }
                else
                {
                    strAlias = pNameTabla;
                }
            }
            catch (Exception ex)
            {
                strAlias = pNameTabla;
            }

            string strAliasSel = "";
            if (formulario.chkUsarSpsSelect.Checked)
            {
                if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                    miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                    miEntorno == TipoEntorno.CSharp_WebForms ||
                    miEntorno == TipoEntorno.CSharp_WebAPI ||
                    miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
                {
                    funcionCargarDataReaderAnd = "ExecStoreProcedureDataReaderSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataReaderOr = "ExecStoreProcedureDataReaderSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAnd = "ExecStoreProcedureSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAndFromView = "ExecStoreProcedureSel(strVista";
                    funcionCargarDataTableOr = "ExecStoreProcedureSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    strAliasSel = strAlias;
                }
                else
                {
                    funcionCargarDataReaderAnd = "execStoreProcedureDataReaderSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataReaderOr = "execStoreProcedureDataReaderSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAnd = "execStoreProcedureSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAndFromView = "execStoreProcedureSel(strVista";
                    funcionCargarDataTableOr = "execStoreProcedureSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    strAliasSel = strAlias;
                }
            }
            else
            {
                if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                    miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                    miEntorno == TipoEntorno.CSharp_WebForms ||
                    miEntorno == TipoEntorno.CSharp_WebAPI ||
                    miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
                {
                    funcionCargarDataReaderAnd = "CargarDataReaderAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataReaderOr = "CargarDataReaderOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAnd = "CargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAndFromView = "CargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + "strVista";
                    funcionCargarDataTableOr = "CargarDataTableOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                }
                else
                {
                    funcionCargarDataReaderAnd = "cargarDataReaderAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataReaderOr = "cargarDataReaderOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAnd = "cargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAndFromView = "cargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "strVista";
                    funcionCargarDataTableOr = "cargarDataTableOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                }
                strAliasSel = pNameTabla;
            }

            //Obtener Objeto por Llave primaria FK
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de la llave primaria");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("\t\t/// </returns>");

            if (miEntorno != TipoEntorno.CSharp_WCF)
            {
                newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(" + strLlavePrimaria + ")");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");

                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                    {
                        bAdicionar = false;
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
                    }
                }
                if (bAdicionar)
                {
                    newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ".ToString());");
                }

                bAdicionar = true;
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
                int iLlavePrimaria = 0;
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                    {
                        bAdicionar = false;
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        tipoCol = dtColumns.Rows[iCol][1].ToString();

                        switch (tipoCol.ToUpper())
                        {
                            case "INT":
                                if (SetInt == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "INT32":
                                if (SetInt32 == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "DECIMAL":
                                if (SetDecimal == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "BOOL":
                                if (SetBoolean == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "BOOLEAN":
                                if (SetBoolean == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "BYTE[]":
                                if (SetByte == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "BYTE":
                                if (SetByte == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "DATE":
                                if (SetDateTime == true)
                                {
                                    newTexto.AppendLine("\t\t\tif (obj." + nameCol + " == null)");
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(null);");
                                    newTexto.AppendLine("\t\t\telse");
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'\" + Convert.ToDateTime(" + llavePrimaria[iLlavePrimaria] + ").ToString(" + formatoFecha + ") + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "DATETIME":
                                if (SetDateTime == true)
                                {
                                    newTexto.AppendLine("\t\t\tif (" + llavePrimaria[iLlavePrimaria] + " == null)");
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(null);");
                                    newTexto.AppendLine("\t\t\telse");
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'\" + Convert.ToDateTime(" + llavePrimaria[iLlavePrimaria] + ").ToString(" + formatoFechaHora + ") + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "STRING":
                                if (SetString == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            default:
                                if (SetDefault == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;
                        }
                        iLlavePrimaria = iLlavePrimaria + 1;
                    }
                }
                if (bAdicionar)
                {
                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                }

                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\treturn ObtenerObjeto(arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");
            }
            else
            {
                newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(" + strLlavePrimaria + ")");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tArrayList arrColumnas = new ArrayList();");

                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    newTexto.AppendLine("\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
                }

                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                    {
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
                    }
                }
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
                int iLlavePrimaria = 0;
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                    {
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        tipoCol = dtColumns.Rows[iCol][1].ToString();

                        switch (tipoCol.ToUpper())
                        {
                            case "INT":
                                if (SetInt == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "INT32":
                                if (SetInt32 == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "DECIMAL":
                                if (SetDecimal == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "BOOL":
                                if (SetBoolean == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "BOOLEAN":
                                if (SetBoolean == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "BYTE[]":
                                if (SetByte == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "BYTE":
                                if (SetByte == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(e" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "DATE":
                                if (SetDateTime == true)
                                {
                                    newTexto.AppendLine("\t\t\tif (obj." + nameCol + " == null)");
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(null);");
                                    newTexto.AppendLine("\t\t\telse");
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'\" + Convert.ToDateTime(" + llavePrimaria[iLlavePrimaria] + ").ToString(" + formatoFecha + ") + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "DATETIME":
                                if (SetDateTime == true)
                                {
                                    newTexto.AppendLine("\t\t\tif (obj." + nameCol + " == null)");
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(null);");
                                    newTexto.AppendLine("\t\t\telse");
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'\" + Convert.ToDateTime(" + llavePrimaria[iLlavePrimaria] + ").ToString(" + formatoFechaHora + ") + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "STRING":
                                if (SetString == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            default:
                                if (SetDefault == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;
                        }
                        iLlavePrimaria = iLlavePrimaria + 1;
                    }
                }
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\treturn ObtenerObjeto(arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");
            }

            //Obtener ultimo Objeto insertado por Usuario
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir del usuario que inserta");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"strUsuCre\">Login o nombre de usuario</param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("\t\t/// </returns>");
            newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjetoInsertado(string strUsuCre)");
            newTexto.AppendLine("\t\t{");

            if (strUsuCreColum == "")
            {
                newTexto.AppendLine("\t\t\tthrow new NotImplementedException();");
            }
            else
            {
                //Contamos cuantas PKs tiene
                int iContadorPKs = 0;
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                        iContadorPKs++;
                }
                if (iContadorPKs == 1)
                {
                    newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                    newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(" + pNameClaseEnt + ".Fields." + strUsuCreColum + ".ToString());");
                    newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + strUsuCre + \"'\");");
                    newTexto.AppendLine("\t\t\t");
                    bAdicionar = true;
                    for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                    {
                        if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                        {
                            if (bAdicionar)
                            {
                                bAdicionar = false;
                                nameCol = dtColumns.Rows[iCol][0].ToString();
                                newTexto.AppendLine("\t\t\tint iIdInsertado = FuncionesMax(" + pNameClaseEnt + ".Fields." + nameCol + ", arrColumnasWhere, arrValoresWhere);");
                            }
                        }
                    }
                    if (bAdicionar)
                    {
                        newTexto.AppendLine("\t\t\tint iIdInsertado = FuncionesMax(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ", arrColumnasWhere, arrValoresWhere);");
                    }

                    newTexto.AppendLine("\t\t\t");
                    newTexto.AppendLine("\t\t\treturn ObtenerObjeto(iIdInsertado);");
                }
                else
                {
                    //Se tiene mas de una columna como PK
                    newTexto.AppendLine("\t\t\tthrow new NotImplementedException();");
                }
            }
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Obtener Objeto filtrado por Arrays
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\treturn ObtenerObjeto(arrColumnasWhere, arrValoresWhere, \"\");");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Obtener Objeto filtrado por Arrays y por Parametros adicionales
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
            }

            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\tDataTable table = local." + funcionCargarDataTableAnd + ", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tif (table.Rows.Count == 1)");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\t" + pNameClaseEnt + " obj = new " + pNameClaseEnt + "();");
            newTexto.AppendLine("\t\t\t\t\tobj = crearObjeto(table.Rows[0]);");
            newTexto.AppendLine("\t\t\t\t\treturn obj;");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\telse if (table.Rows.Count > 1)");
            newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"Se ha devuelto mas de un objeto\");");
            newTexto.AppendLine("\t\t\t\telse");
            newTexto.AppendLine("\t\t\t\t\treturn null;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Obtener Objeto filtrado por Arrays
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"htbFiltro\" type=\"System.Collections.Hashtable\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Hashtable que contienen los pares para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(Hashtable htbFiltro)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\treturn ObtenerObjeto(htbFiltro, \"\");");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Obtener Objeto filtrado por Arrays y por Parametros adicionales
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"htbFiltro\" type=\"System.Collections.Hashtable\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Hashtable que contienen los pares para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
            }

            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tforeach (DictionaryEntry entry in htbFiltro)");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\tarrColumnasWhere.Add(entry.Key.ToString());");
            newTexto.AppendLine("\t\t\t\t\tarrValoresWhere.Add(entry.Value.ToString());");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\t");

            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\tDataTable table = local." + funcionCargarDataTableAnd + ", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tif (table.Rows.Count == 1)");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\t" + pNameClaseEnt + " obj = new " + pNameClaseEnt + "();");
            newTexto.AppendLine("\t\t\t\t\tobj = crearObjeto(table.Rows[0]);");
            newTexto.AppendLine("\t\t\t\t\treturn obj;");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\telse if (table.Rows.Count > 1)");
            newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"Se ha devuelto mas de un objeto\");");
            newTexto.AppendLine("\t\t\t\telse");
            newTexto.AppendLine("\t\t\t\t\treturn null;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            if (miEntorno != TipoEntorno.CSharp_WCF)
            {
                //Obtener Objeto por Campo
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(" + pNameClaseEnt + ".Fields searchField, object searchValue)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\tarrValoresWhere.Add(searchValue);");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\treturn ObtenerObjeto(arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //Obtener Objeto por Campo y parametros adicionales
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\tarrValoresWhere.Add(searchValue);");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\treturn ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");
            }

            //Obtener Objeto por Llave primaria FK con Transaccion
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene un Business Object del Tipo " + pNameClaseEnt + " a partir de su llave promaria");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tObjeto del Tipo " + pNameClaseEnt);
            newTexto.AppendLine("\t\t/// </returns>");

            if (miEntorno != TipoEntorno.CSharp_WCF)
            {
                newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(" + strLlavePrimaria + ", ref " + paramClaseTransaccion + " localTrans )");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                    {
                        bAdicionar = false;
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
                    }
                }
                if (bAdicionar)
                {
                    newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ".ToString())");
                }

                bAdicionar = true;
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
                int iLlavePrimaria = 0;
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                    {
                        bAdicionar = false;
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        tipoCol = dtColumns.Rows[iCol][1].ToString();

                        switch (tipoCol.ToUpper())
                        {
                            case "INT":
                                if (SetInt == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "INT32":
                                if (SetInt32 == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "DECIMAL":
                                if (SetDecimal == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "BOOL":
                                if (SetBoolean == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "BOOLEAN":
                                if (SetBoolean == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "BYTE[]":
                                if (SetByte == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "BYTE":
                                if (SetByte == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(e" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "DATE":
                                if (SetDateTime == true)
                                {
                                    newTexto.AppendLine("\t\t\tif (obj." + nameCol + " == null)");
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(null);");
                                    newTexto.AppendLine("\t\t\telse");
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'\" + Convert.ToDateTime(" + llavePrimaria[iLlavePrimaria] + ").ToString(" + formatoFecha + ") + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "DATETIME":
                                if (SetDateTime == true)
                                {
                                    newTexto.AppendLine("\t\t\tif (" + llavePrimaria[iLlavePrimaria] + " == null)");
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(null);");
                                    newTexto.AppendLine("\t\t\telse");
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'\" + Convert.ToDateTime(" + llavePrimaria[iLlavePrimaria] + ").ToString(" + formatoFechaHora + ") + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "STRING":
                                if (SetString == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            default:
                                if (SetDefault == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;
                        }
                        iLlavePrimaria = iLlavePrimaria + 1;
                    }
                }
                if (bAdicionar)
                {
                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                }

                newTexto.AppendLine("\t\t\treturn ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");
            }
            else
            {
                newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(" + strLlavePrimaria + ", ref " + paramClaseTransaccion + " localTrans)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                    {
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
                    }
                }
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
                int iLlavePrimaria = 0;
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                    {
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        tipoCol = dtColumns.Rows[iCol][1].ToString();

                        switch (tipoCol.ToUpper())
                        {
                            case "INT":
                                if (SetInt == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "INT32":
                                if (SetInt32 == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "DECIMAL":
                                if (SetDecimal == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "BOOL":
                                if (SetBoolean == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "BOOLEAN":
                                if (SetBoolean == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "BYTE[]":
                                if (SetByte == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "BYTE":
                                if (SetByte == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(e" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "DATE":
                                if (SetDateTime == true)
                                {
                                    newTexto.AppendLine("\t\t\tif (obj." + nameCol + " == null)");
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(null);");
                                    newTexto.AppendLine("\t\t\telse");
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'\" + Convert.ToDateTime(" + llavePrimaria[iLlavePrimaria] + ").ToString(" + formatoFecha + ") + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "DATETIME":
                                if (SetDateTime == true)
                                {
                                    newTexto.AppendLine("\t\t\tif (obj." + nameCol + " == null)");
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(null);");
                                    newTexto.AppendLine("\t\t\telse");
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'\" + Convert.ToDateTime(" + llavePrimaria[iLlavePrimaria] + ").ToString(" + formatoFechaHora + ") + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            case "STRING":
                                if (SetString == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;

                            default:
                                if (SetDefault == true)
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ");");
                                }
                                break;
                        }
                        iLlavePrimaria = iLlavePrimaria + 1;
                    }
                }
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\treturn ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");
            }

            //Obtener Objeto filtrado por Arrays
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref " + paramClaseTransaccion + " localTrans)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\treturn ObtenerObjeto(arrColumnasWhere, arrValoresWhere, \"\", ref localTrans);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Obtener Objeto filtrado por Arrays
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"htbFiltro\" type=\"System.Collections.Hashtable\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Hashtable que contienen los pares para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(Hashtable htbFiltro, ref " + paramClaseTransaccion + " localTrans)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\treturn ObtenerObjeto(htbFiltro, \"\", ref localTrans);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Obtener Objeto filtrado por Arrays
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,  ref " + paramClaseTransaccion + " localTrans)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\treturn ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Obtener Objeto filtrado por Arrays y por Parametros adicionales
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"htbFiltro\" type=\"System.Collections.Hashtable\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Hashtable que contienen los pares para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
            }

            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tforeach (DictionaryEntry entry in htbFiltro)");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\tarrColumnasWhere.Add(entry.Key.ToString());");
            newTexto.AppendLine("\t\t\t\t\tarrValoresWhere.Add(entry.Value.ToString());");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\tDataTable table = local." + funcionCargarDataTableAnd + ", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tif (table.Rows.Count == 1)");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\t" + pNameClaseEnt + " obj = new " + pNameClaseEnt + "();");
            newTexto.AppendLine("\t\t\t\t\tobj = crearObjeto(table.Rows[0]);");
            newTexto.AppendLine("\t\t\t\t\treturn obj;");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\telse if (table.Rows.Count > 1)");
            newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"Se ha devuelto mas de un objeto\");");
            newTexto.AppendLine("\t\t\t\telse");
            newTexto.AppendLine("\t\t\t\t\treturn null;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            if (miEntorno != TipoEntorno.CSharp_WCF)
            {
                //Obtener Objeto por Campo
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(" + pNameClaseEnt + ".Fields searchField, object searchValue, ref " + paramClaseTransaccion + " localTrans)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\tarrValoresWhere.Add(searchValue);");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\treturn ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //Obtener Objeto por Campo y parametros adicionales
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic " + pNameClaseEnt + " ObtenerObjeto(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\tarrValoresWhere.Add(searchValue);");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\treturn ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");
            }

            return newTexto.ToString();
        }
    }
}