﻿using ReAl.Generator.App.AppClass;

namespace ReAl.Generator.App.AppGenerators
{
    public class cEntidadesParametros
    {
        public bool bUseEventProperties;
        public bool bUseBaseClass;
        public bool bUseDataAnnotations;
        public bool bMoverResultados;
        public bool bradWCF;

        public bool bEntidadesEncriptacion;
        public string strPathProyecto;
        public string strInfProyecto;
        public string strInfProyectoClass;
        public string strNamespaceEntidades;
        public string strNamespaceModelo;
        public string strPrefijoEntidades;
        public string strPrefijoModelo;

        public string strCabeceraPlantillaCodigo;
        public string strNameTabla;
        public string strNameClase;
        public string strNameClaseModelo;
        public string strNameSchema;
        public string strParamListadoSp;
        public string strParamBaseClass;

        public string strParamClaseApi;
        public string strParamClaseApiObject;

        public TipoEntorno miEntorno;
    }
}