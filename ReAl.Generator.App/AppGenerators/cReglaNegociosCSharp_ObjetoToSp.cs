﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using System;
using System.Collections;
using System.Data;

namespace ReAl.Generator.App.AppGenerators
{
    public static class cReglaNegociosCSharp_ObjetoToSp
    {
        public static string CrearModelo(DataTable dtColumns, ref FPrincipal formulario, string pNameTabla, string pNameClaseEnt, string strLlavePrimaria, ArrayList llavePrimaria)
        {
            TipoEntorno miEntorno;
            Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            bool bAdicionar = false;
            string nameCol = "";
            string tipoCol = "";

            string paramClaseConeccion = (string.IsNullOrEmpty(formulario.txtClaseConn.Text) ? "cConn" : formulario.txtClaseConn.Text);
            string paramClaseTransaccion = (string.IsNullOrEmpty(formulario.txtClaseTransaccion.Text) ? "cTransaccion" : formulario.txtClaseTransaccion.Text);
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string namespaceClases = (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class");

            string formatoFechaHora = paramClaseParametros + ".parFormatoFechaHora";
            string formatoFecha = paramClaseParametros + ".parFormatoFecha";

            string funcionCargarProcedure = null;
            string funcionCargarDataReaderAnd = null;
            string funcionCargarDataReaderOr = null;
            string funcionCargarDataTableAnd = null;
            string funcionCargarDataTableAndFromView = null;
            string funcionCargarDataTableOr = null;

            string strAlias = null;
            try
            {
                DataTable dtPrefijo = new DataTable();

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.SQLServer2000:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.Oracle:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT ALIASSTA FROM SEGTABLAS WHERE UPPER(TABLASTA) = '" + pNameTabla.ToUpper() + "'");
                        break;

                    case TipoBD.PostgreSQL:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.aliassta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" + pNameTabla + "')");
                        if (dtPrefijo.Rows.Count == 0)
                        {
                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.alias FROM seg_tablas s WHERE UPPER(s.nombretabla) = UPPER('" + pNameTabla + "')");
                        }
                        break;
                }

                if (dtPrefijo.Rows.Count > 0)
                {
                    strAlias = dtPrefijo.Rows[0][0].ToString().ToLower();
                    strAlias = strAlias.Substring(0, 1).ToUpper() + strAlias.Substring(1, strAlias.Length - 1);
                }
                else
                {
                    strAlias = pNameTabla;
                }
            }
            catch (Exception ex)
            {
                strAlias = pNameTabla;
            }

            string strAliasSel = "";
            if (formulario.chkUsarSpsSelect.Checked)
            {
                if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                    miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                    miEntorno == TipoEntorno.CSharp_WebForms ||
                    miEntorno == TipoEntorno.CSharp_WebAPI ||
                    miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
                {
                    funcionCargarProcedure = "ExecStoreProcedure";
                    funcionCargarDataReaderAnd = "ExecStoreProcedureDataReaderSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataReaderOr = "ExecStoreProcedureDataReaderSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAnd = "ExecStoreProcedureSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAndFromView = "ExecStoreProcedureSel(strVista";
                    funcionCargarDataTableOr = "ExecStoreProcedureSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                }
                else
                {
                    funcionCargarProcedure = "execStoreProcedure";
                    funcionCargarDataReaderAnd = "execStoreProcedureDataReaderSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataReaderOr = "execStoreProcedureDataReaderSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAnd = "execStoreProcedureSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAndFromView = "execStoreProcedureSel(strVista";
                    funcionCargarDataTableOr = "execStoreProcedureSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                }
                strAliasSel = strAlias;
            }
            else
            {
                if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                    miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                    miEntorno == TipoEntorno.CSharp_WebForms ||
                    miEntorno == TipoEntorno.CSharp_WebAPI ||
                    miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
                {
                    funcionCargarProcedure = "ExecStoreProcedure";
                    funcionCargarDataReaderAnd = "CargarDataReaderAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataReaderOr = "CargarDataReaderOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAnd = "CargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAndFromView = "CargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + "strVista";
                    funcionCargarDataTableOr = "CargarDataTableOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                }
                else
                {
                    funcionCargarProcedure = "execStoreProcedure";
                    funcionCargarDataReaderAnd = "cargarDataReaderAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataReaderOr = "cargarDataReaderOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAnd = "cargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAndFromView = "cargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "strVista";
                    funcionCargarDataTableOr = "cargarDataTableOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                }
                strAliasSel = pNameTabla;
            }

            //Objeto entero a un SP
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"strNombreSp\" type=\"System.string\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Nombre del Procedimiento a ejecutar sobre el SP");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a ejecutar el SP de la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Valor de registros afectados en el Procedimiento de la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");
            newTexto.AppendLine("\t\tpublic int EjecutarSpDesdeObjeto(string strNombreSp, " + pNameClaseEnt + " obj)");

            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine(cDiccionarios.arrValTodo(pNameClaseEnt, pNameTabla, dtColumns, formulario, miEntorno));
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t//Llamamos al Procedmiento Almacenado");
            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\treturn local." + funcionCargarProcedure + "(strNombreSp, arrNombreParam, arrValoresParam);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}\r\n");

            if (miEntorno != TipoEntorno.CSharp_WCF)
            {
                //INSERT TRANS
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"strNombreSp\" type=\"System.string\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Nombre del Procedimiento a ejecutar sobre el SP");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a ejecutar el SP de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \t Valor de registros afectados en el Procedimiento de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </returns>");
                newTexto.AppendLine("\t\tpublic int EjecutarSpDesdeObjeto(string strNombreSp, " + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans)");

                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine(cDiccionarios.arrValTodo(pNameClaseEnt, pNameTabla, dtColumns, formulario, miEntorno));
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t//Llamamos al Procedmiento Almacenado");
                newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
                newTexto.AppendLine("\t\t\t\treturn local." + funcionCargarProcedure + "(strNombreSp, arrNombreParam, arrValoresParam, ref localTrans);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}\r\n");
            }

            return newTexto.ToString();
        }
    }
}