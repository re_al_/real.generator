﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;

namespace ReAl.Generator.App.AppGenerators
{
    public class cReglaNegociosVB
    {
        private bool SetInt = false;
        private bool SetInt32 = false;
        private bool SetBoolean = false;
        private bool SetString = true;
        private bool SetDateTime = true;
        private bool SetByte = false;
        private bool SetDecimal = false;
        private bool SetDefault = true;

        public string CrearModeloVB(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            TipoEntorno miEntorno;
            Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);

            cAltasBajasMod cABMs = new cAltasBajasMod();

            string paramClaseConeccion = (string.IsNullOrEmpty(formulario.txtClaseConn.Text) ? "cConn" : formulario.txtClaseConn.Text);
            string paramClaseTransaccion = (string.IsNullOrEmpty(formulario.txtClaseTransaccion.Text) ? "cTransaccion" : formulario.txtClaseTransaccion.Text);
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string formatoFechaHora = paramClaseParametros + ".parFormatoFechaHora";
            string formatoFecha = paramClaseParametros + ".parFormatoFecha";
            string namespaceClases = (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class");

            string nameCol = null;
            string tipoCol = null;
            bool permiteNull = false;
            string pNameTabla = pNameClase.Replace("_", "");
            pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();

            pNameClase = formulario.txtPrefijoModelo.Text + pNameTabla;
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameTabla;
            string pNameClaseIn = formulario.txtPrefijoInterface.Text + pNameTabla;

            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            string TipoColNull = null;
            string strAlias = null;
            string DescripcionCol = null;
            try
            {
                DataTable dtPrefijo = new DataTable();

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE tablaSegTablas = '" + pNameTabla + "'");
                        break;

                    case TipoBD.SQLServer2000:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE tablaSegTablas = '" + pNameTabla + "'");
                        break;

                    case TipoBD.Oracle:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT ALIASSTA FROM SEGTABLAS WHERE UPPER(TABLASTA) = '" + pNameTabla.ToUpper() + "'");
                        break;

                    case TipoBD.PostgreSQL:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.aliassta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" + pNameTabla + "')");
                        break;
                }

                if (dtPrefijo.Rows.Count > 0)
                {
                    strAlias = dtPrefijo.Rows[0][0].ToString().ToLower();
                    strAlias = strAlias.Substring(0, 1).ToUpper() + strAlias.Substring(1, strAlias.Length - 1);
                }
                else
                {
                    strAlias = pNameTabla;
                }
            }
            catch (Exception exp)
            {
                strAlias = pNameTabla;
            }

            if (dtColumns.Rows.Count == 0)
                return "";
            if (dtColumns.Columns.Count == 0)
                return "";

            string funcionCargarDataTableAnd = null;
            string funcionCargarDataTableOr = null;
            string funcionCargarDataTableGeneral = null;

            string strAliasSel = "";
            if (formulario.chkUsarSpsSelect.Checked)
            {
                funcionCargarDataTableAnd = "execStoreProcedureSel";
                funcionCargarDataTableOr = "execStoreProcedureSelOr";
                funcionCargarDataTableGeneral = "execStoreProcedureSel";
                strAliasSel = strAlias;
            }
            else
            {
                funcionCargarDataTableAnd = "cargarDataTableAnd";
                funcionCargarDataTableOr = "cargarDataTableOr";
                funcionCargarDataTableGeneral = "cargarDataTable";
                strAliasSel = pNameTabla;
            }

            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\")) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\"));
            }

            bool bAdicionar = true;
            ArrayList llavePrimaria = new ArrayList();
            string strLlavePrimaria = string.Empty;
            dynamic bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();

                    if (bEsPrimerElemento)
                    {
                        strLlavePrimaria = strLlavePrimaria + "ByVal " + tipoCol + nameCol + " As " + tipoCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlavePrimaria = strLlavePrimaria + ", ByVal " + tipoCol + nameCol + " As " + tipoCol;
                    }

                    llavePrimaria.Add(tipoCol + nameCol);
                }
            }
            if (string.IsNullOrEmpty(strLlavePrimaria))
            {
                strLlavePrimaria = "ByVal " + dtColumns.Rows[0][1] + dtColumns.Rows[0][0] + " As " + dtColumns.Rows[0][1];
                llavePrimaria.Add(dtColumns.Rows[0][1] + dtColumns.Rows[0][0].ToString());
            }

            string strPermiteNull = null;
            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase);
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que implementa los metodos y operaciones sobre la Tabla " + pNameTabla);

            StringBuilder newTexto = new StringBuilder();

            //newTexto.AppendLine(CabeceraTmp)

            newTexto.AppendLine("");
            newTexto.AppendLine("#Region \"Imports\"");
            newTexto.AppendLine("Imports System");
            newTexto.AppendLine("Imports System.Numerics");
            newTexto.AppendLine("Imports System.Collections");
            newTexto.AppendLine("Imports System.Collections.Generic");
            newTexto.AppendLine("Imports System.Data");
            newTexto.AppendLine("Imports " + formulario.txtInfProyectoClass.Text + "." + namespaceClases + "");
            newTexto.AppendLine("Imports " + formulario.txtInfProyectoDal.Text + "");
            if (miEntorno == TipoEntorno.CSharp_WCF)
            {
                newTexto.AppendLine("Imports System.ServiceModel");
            }
            if (miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
            {
                newTexto.AppendLine("Imports System.Windows.Forms");
            }
            if (miEntorno == TipoEntorno.CSharp_WebForms)
            {
                newTexto.AppendLine("Imports System.Web.UI.WebControls");
            }
            if (formulario.chkReAlEntidades.Checked)
                newTexto.AppendLine("Imports " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceEntidades.Text + "");
            if (formulario.chkReAlInterface.Checked)
                newTexto.AppendLine("Imports " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceInterface.Text + "");

            newTexto.AppendLine("#End Region");
            newTexto.AppendLine("");

            //newTexto.AppendLine("Namespace " & formulario.txtPrefijoModelo.Text)

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Class " + pNameClase);
                newTexto.AppendLine("Implements " + pNameClaseIn);
                newTexto.AppendLine("'Debe implementar la Interface (Alt + Shift + F10)\r\n");
                newTexto.AppendLine("#Region \"" + pNameClaseIn + " Members\"\r\n");
            }
            else
            {
                newTexto.AppendLine("Public Class " + pNameClase);
            }

            //GetDescripcion del Objeto
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene la Descripcion de Determinado Campo");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"MYField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo solicitado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo STRING que describe al parametro");
            newTexto.AppendLine("''' </returns>");
            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function GetDesc(ByVal myField As " + pNameClaseEnt + ".Fields) As String Implements " + pNameClaseIn + ".GetDesc");
            }
            else
            {
                newTexto.AppendLine("Public Function GetDesc(ByVal myField As " + pNameClaseEnt + ".Fields) As String");
            }

            newTexto.AppendLine("\tTry");
            //newTexto.AppendLine( vbTab & vbTab & vbTab & "string strDesc = " & Chr(34) & Chr(34) & ";" )

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                strPermiteNull = dtColumns.Rows[iCol]["PermiteNull"].ToString();
                DescripcionCol = dtColumns.Rows[iCol]["COL_DESC"].ToString();

                TipoColNull = tipoCol;
                if (tipoCol != "String")
                {
                    if (strPermiteNull == "Yes")
                    {
                        if (TipoColNull.Contains("[]"))
                        {
                            TipoColNull = tipoCol;
                        }
                        else
                        {
                            TipoColNull = tipoCol + "?";
                        }
                    }
                }

                newTexto.AppendLine("\t\tIf " + pNameClaseEnt + ".Fields." + nameCol + ".ToString() = myField.ToString() Then");
                if (string.IsNullOrEmpty(DescripcionCol))
                {
                    newTexto.AppendLine("\t\t\tReturn \"Valor de tipo " + tipoCol + " para " + nameCol.Replace(pNameTabla, "") + "\"");
                }
                else
                {
                    newTexto.AppendLine("\t\t\tReturn \"" + DescripcionCol.Replace("\r\n", "") + "\"");
                }
                newTexto.AppendLine("\t\tEnd If");
            }

            newTexto.AppendLine("\t\tReturn string.Empty");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //GetFieldRequired del Objeto
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene el Error de Determinado Campo");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"myField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo solicitado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo STRING que describe al parametro");
            newTexto.AppendLine("''' </returns>");
            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function GetError(ByVal myField As " + pNameClaseEnt + ".Fields) As String Implements " + pNameClaseIn + ".GetError");
            }
            else
            {
                newTexto.AppendLine("Public Function GetError(ByVal myField As " + pNameClaseEnt + ".Fields) As String");
            }

            newTexto.AppendLine("\tTry");
            //newTexto.AppendLine( vbTab & vbTab & vbTab & "string strDesc = " & Chr(34) & Chr(34) & ";" )

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                strPermiteNull = dtColumns.Rows[iCol]["PermiteNull"].ToString();
                TipoColNull = tipoCol;
                if (tipoCol != "String")
                {
                    if (strPermiteNull == "Yes")
                    {
                        if (TipoColNull.Contains("[]"))
                        {
                            TipoColNull = tipoCol;
                        }
                        else
                        {
                            TipoColNull = tipoCol + "?";
                        }
                    }
                }

                newTexto.AppendLine("\t\tIf " + pNameClaseEnt + ".Fields." + nameCol + ".ToString() = myField.ToString() Then");
                newTexto.AppendLine("\t\t\tReturn \"" + tipoCol + " para " + nameCol.Replace(pNameTabla, "") + "\"");
                newTexto.AppendLine("\t\tEnd If");
            }

            newTexto.AppendLine("\t\tReturn string.Empty");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //GetFieldRequired del Objeto
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene el Nombre de Determinado Campo");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"myField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo solicitado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo STRING que describe al parametro");
            newTexto.AppendLine("''' </returns>");
            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function GetColumn(ByVal myField As " + pNameClaseEnt + ".Fields) As String Implements " + pNameClaseIn + ".GetColumn");
            }
            else
            {
                newTexto.AppendLine("Public Function GetColumn(ByVal myField As " + pNameClaseEnt + ".Fields) As String");
            }

            newTexto.AppendLine("\tTry");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                strPermiteNull = dtColumns.Rows[iCol]["PermiteNull"].ToString();
                TipoColNull = tipoCol;
                if (tipoCol != "String")
                {
                    if (strPermiteNull == "Yes")
                    {
                        if (TipoColNull.Contains("[]"))
                        {
                            TipoColNull = tipoCol;
                        }
                        else
                        {
                            TipoColNull = tipoCol + "?";
                        }
                    }
                }

                newTexto.AppendLine("\t\tIf " + pNameClaseEnt + ".Fields." + nameCol + ".ToString() = myField.ToString() Then");
                newTexto.AppendLine("\t\t\tReturn \"" + nameCol.Replace(pNameTabla, "") + ":\"");
                newTexto.AppendLine("\t\tEnd If");
            }

            newTexto.AppendLine("\t\tReturn string.Empty");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Objeto por Llave primaria FK
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");
            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(" + strLlavePrimaria + ") As " + pNameClaseEnt + " Implements " + pNameClaseIn + ".ObtenerObjeto");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(" + strLlavePrimaria + ") As " + pNameClaseEnt);
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            bAdicionar = true;
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    newTexto.AppendLine("\t\tarrColumnasWhere.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
                    bAdicionar = false;
                }
            }
            if (bAdicionar)
            {
                newTexto.AppendLine("\t\tarrColumnasWhere.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ".ToString())");
            }

            bAdicionar = true;
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");

            int iLlavePrimaria = 0;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();
                    bAdicionar = false;
                    switch (tipoCol.ToUpper())
                    {
                        case "INTEGER":
                            if (SetInt32 == true)
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                            }
                            break;

                        case "DECIMAL":
                            if (SetDecimal == true)
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                            }
                            break;

                        case "BOOL":
                            if (SetBoolean == true)
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                            }
                            break;

                        case "BOOLEAN":
                            if (SetBoolean == true)
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                            }
                            break;

                        case "BYTE()":
                            if (SetByte == true)
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                            }
                            break;

                        case "BYTE":
                            if (SetByte == true)
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(e" + llavePrimaria[iLlavePrimaria] + ")");
                            }
                            break;

                        case "DATE":
                            if (SetDateTime == true)
                            {
                                newTexto.AppendLine("\t\tIf IsNothing(obj." + nameCol + ") Then ");
                                newTexto.AppendLine("\t\t\tarrValoresWhere.Add(Nothing)");
                                newTexto.AppendLine("\t\tElse");
                                newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + Convert.ToDateTime(" + llavePrimaria[iLlavePrimaria] + ").ToString(" + formatoFecha + ") + \"'\")");
                                newTexto.AppendLine("\t\tEnd If");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                            }
                            break;

                        case "DATETIME":
                            if (SetDateTime == true)
                            {
                                newTexto.AppendLine("\t\tIf IsNothing(" + llavePrimaria[iLlavePrimaria] + ") Then");
                                newTexto.AppendLine("\t\t\tarrValoresWhere.Add(Nothing)");
                                newTexto.AppendLine("\t\tElse");
                                newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + Convert.ToDateTime(" + llavePrimaria[iLlavePrimaria] + ").ToString(" + formatoFechaHora + ") + \"'\")");
                                newTexto.AppendLine("\t\tEnd If");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                            }
                            break;

                        case "STRING":
                            if (SetString == true)
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                            }
                            break;

                        default:
                            if (SetDefault == true)
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                            }
                            break;
                    }
                    iLlavePrimaria = iLlavePrimaria + 1;
                }
            }
            newTexto.AppendLine("\t");
            if (bAdicionar)
            {
                newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf table.Rows.Count = 1 Then");
            newTexto.AppendLine("\t\t\tDim obj As " + pNameClaseEnt + " = new " + pNameClaseEnt);
            newTexto.AppendLine("\t\t\tobj = CrearObjeto(table.Rows(0))");
            newTexto.AppendLine("\t\t\tReturn obj");
            newTexto.AppendLine("\t\tElseIf table.Rows.Count > 1 Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de un objeto\")");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn Nothing");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tend Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Objeto filtrado por Arrays
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");
            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As " + pNameClaseEnt + " Implements " + pNameClaseIn + ".ObtenerObjeto");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As " + pNameClaseEnt);
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf table.Rows.Count = 1 Then");
            newTexto.AppendLine("\t\t\tDim obj As " + pNameClaseEnt + " = new " + pNameClaseEnt);
            newTexto.AppendLine("\t\t\tobj = CrearObjeto(table.Rows(0))");
            newTexto.AppendLine("\t\t\tReturn obj");
            newTexto.AppendLine("\t\tElseIf table.Rows.Count > 1 Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de un objeto\")");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn Nothing");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Objeto filtrado por Arrays y por Parametros adicionales
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String) As " + pNameClaseEnt + " Implements " + pNameClaseIn + ".ObtenerObjeto");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String) As " + pNameClaseEnt);
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf table.Rows.Count = 1 Then");
            newTexto.AppendLine("\t\t\tDim obj As " + pNameClaseEnt + " = new " + pNameClaseEnt);
            newTexto.AppendLine("\t\t\tobj = CrearObjeto(table.Rows(0))");
            newTexto.AppendLine("\t\t\tReturn obj");
            newTexto.AppendLine("\t\tElseIf (table.Rows.Count > 1) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de un objeto\")");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn Nothing");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Objeto por Campo
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object) As " + pNameClaseEnt + " Implements " + pNameClaseIn + ".ObtenerObjeto");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object) As " + pNameClaseEnt);
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count = 1) Then");
            newTexto.AppendLine("\t\t\tDim obj As " + pNameClaseEnt + " = new " + pNameClaseEnt);
            newTexto.AppendLine("\t\t\tobj = CrearObjeto(table.Rows(0))");
            newTexto.AppendLine("\t\t\tReturn obj");
            newTexto.AppendLine("\t\tElseIf (table.Rows.Count > 1)");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de un objeto\")");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn Nothing");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Objeto por Campo y parametros adicionales
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String) As " + pNameClaseEnt + " Implements " + pNameClaseIn + ".ObtenerObjeto");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String) As " + pNameClaseEnt);
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count = 1) Then");
            newTexto.AppendLine("\t\t\tDim obj As " + pNameClaseEnt + " = new " + pNameClaseEnt);
            newTexto.AppendLine("\t\t\tobj = CrearObjeto(table.Rows(0))");
            newTexto.AppendLine("\t\t\tReturn obj");
            newTexto.AppendLine("\t\tElseIf (table.Rows.Count > 1) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de un objeto\")");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn Nothing");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Objeto por Llave primaria FK con Transaccion
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene un Business Object del Tipo " + pNameClaseEnt + " a partir de su llave promaria");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tObjeto del Tipo " + pNameClaseEnt);
            newTexto.AppendLine("''' </returns>");

            if (miEntorno != TipoEntorno.VB_Net_WCF)
            {
                if (formulario.chkReAlInterface.Checked)
                {
                    newTexto.AppendLine("Public Function ObtenerObjeto(" + strLlavePrimaria + ", ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt + " Implements " + pNameClaseIn + ".ObtenerObjeto");
                }
                else
                {
                    newTexto.AppendLine("Public Function ObtenerObjeto(" + strLlavePrimaria + ", ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt);
                }

                newTexto.AppendLine("\tTry");
                newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
                }

                bAdicionar = true;
                newTexto.AppendLine("\t");
                newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                    {
                        bAdicionar = false;
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        newTexto.AppendLine("\t\tarrColumnasWhere.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
                    }
                }
                if (bAdicionar)
                {
                    newTexto.AppendLine("\t\tarrColumnasWhere.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ".ToString())");
                }

                bAdicionar = true;
                newTexto.AppendLine("\t");
                newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
                iLlavePrimaria = 0;
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                    {
                        bAdicionar = false;
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        tipoCol = dtColumns.Rows[iCol][1].ToString();

                        switch (tipoCol.ToUpper())
                        {
                            case "INTEGER":
                                if (SetInt32 == true)
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            case "DECIMAL":
                                if (SetDecimal == true)
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            case "BOOL":
                                if (SetBoolean == true)
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            case "BOOLEAN":
                                if (SetBoolean == true)
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            case "BYTE()":
                                if (SetByte == true)
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            case "BYTE":
                                if (SetByte == true)
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(e" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            case "DATE":
                                if (SetDateTime == true)
                                {
                                    newTexto.AppendLine("\t\tIf IsNothing(obj." + nameCol + ") Then");
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(Nothing)");
                                    newTexto.AppendLine("\t\tElse");
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + Convert.ToDateTime(" + llavePrimaria[iLlavePrimaria] + ").ToString(" + formatoFecha + ") + \"'\")");
                                    newTexto.AppendLine("\t\tEnd If");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            case "DATETIME":
                                if (SetDateTime == true)
                                {
                                    newTexto.AppendLine("\t\tIf IsNothing(" + llavePrimaria[iLlavePrimaria] + ") Then");
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(Nothing)");
                                    newTexto.AppendLine("\t\tElse");
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + Convert.ToDateTime(" + llavePrimaria[iLlavePrimaria] + ").ToString(" + formatoFechaHora + ") + \"'\")");
                                    newTexto.AppendLine("\t\tEnd If");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            case "STRING":
                                if (SetString == true)
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            default:
                                if (SetDefault == true)
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;
                        }
                        iLlavePrimaria = iLlavePrimaria + 1;
                    }
                }
                if (bAdicionar)
                {
                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                }

                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
                newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
                newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, localTrans)");
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\tIf (table.Rows.Count = 1) Then");

                newTexto.AppendLine("\t\t\tDim obj As " + pNameClaseEnt + " = new " + pNameClaseEnt);
                newTexto.AppendLine("\t\t\tobj = CrearObjeto(table.Rows(0))");
                newTexto.AppendLine("\t\t\tReturn obj");
                newTexto.AppendLine("\t\tElseIf (table.Rows.Count > 1)");
                newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de un objeto\")");
                newTexto.AppendLine("\t\tElse");
                newTexto.AppendLine("\t\t\tReturn Nothing");
                newTexto.AppendLine("\t\tEnd If");
                newTexto.AppendLine("\tCatch exp As Exception");
                newTexto.AppendLine("\t\tThrow exp");
                newTexto.AppendLine("\tEnd Try");
                newTexto.AppendLine("End Function");
                newTexto.AppendLine("");
            }
            else
            {
                if (formulario.chkReAlInterface.Checked)
                {
                    newTexto.AppendLine("Public Function ObtenerObjeto(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt + " Implements " + pNameClaseIn + ".ObtenerObjeto");
                }
                else
                {
                    newTexto.AppendLine("Public Function ObtenerObjeto(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt);
                }

                newTexto.AppendLine("\tTry");
                newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
                }

                newTexto.AppendLine("\t");
                newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                    {
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        newTexto.AppendLine("\t\tarrColumnasWhere.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
                    }
                }
                newTexto.AppendLine("\t");

                newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
                iLlavePrimaria = 0;
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                    {
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        tipoCol = dtColumns.Rows[iCol][1].ToString();

                        switch (tipoCol.ToUpper())
                        {
                            case "INTEGER":
                                if (SetInt32 == true)
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            case "DECIMAL":
                                if (SetDecimal == true)
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            case "BOOL":
                                if (SetBoolean == true)
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            case "BOOLEAN":
                                if (SetBoolean == true)
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            case "BYTE()":
                                if (SetByte == true)
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            case "BYTE":
                                if (SetByte == true)
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(e" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            case "DATE":
                                if (SetDateTime == true)
                                {
                                    newTexto.AppendLine("\t\tIf IsNothing(obj." + nameCol + ") Then");
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(Nothing)");
                                    newTexto.AppendLine("\t\tElse");
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + Convert.ToDateTime(" + llavePrimaria[iLlavePrimaria] + ").ToString(" + formatoFecha + ") + \"'\")");
                                    newTexto.AppendLine("\t\tEnd If");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            case "DATETIME":
                                if (SetDateTime == true)
                                {
                                    newTexto.AppendLine("\t\tIf IsNothing(obj." + nameCol + ") Then");
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(Nothing)");
                                    newTexto.AppendLine("\t\tElse");
                                    newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'\" + Convert.ToDateTime(" + llavePrimaria[iLlavePrimaria] + ").ToString(" + formatoFechaHora + ") + \"'\")");
                                    newTexto.AppendLine("\t\tEnd If");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            case "STRING":
                                if (SetString == true)
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;

                            default:
                                if (SetDefault == true)
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'\" + " + llavePrimaria[iLlavePrimaria] + "\"'\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\tarrValoresWhere.Add(" + llavePrimaria[iLlavePrimaria] + ")");
                                }
                                break;
                        }
                        iLlavePrimaria = iLlavePrimaria + 1;
                    }
                }
                newTexto.AppendLine("\t");

                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
                newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
                newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, localTrans)");
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\tIf (table.Rows.Count = 1) Then");

                newTexto.AppendLine("\t\t\tDim obj As " + pNameClaseEnt + " = new " + pNameClaseEnt);
                newTexto.AppendLine("\t\t\tobj = CrearObjeto(table.Rows(0))");
                newTexto.AppendLine("\t\t\tReturn obj");
                newTexto.AppendLine("\t\tElseIf (table.Rows.Count > 1) Then");
                newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de un objeto\")");
                newTexto.AppendLine("\t\tElse");
                newTexto.AppendLine("\t\t\tReturn Nothing");
                newTexto.AppendLine("\t\tEnd If");
                newTexto.AppendLine("\tCatch exp As Exception");
                newTexto.AppendLine("\t\tThrow exp");
                newTexto.AppendLine("\tEnd Try");
                newTexto.AppendLine("End Function");
                newTexto.AppendLine("");
            }

            //Obtener Objeto filtrado por Arrays
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt + " Implements " + pNameClaseIn + ".ObtenerObjeto");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt);
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, localTrans)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count = 1) Then");

            newTexto.AppendLine("\t\t\tDim obj As " + pNameClaseEnt + " = new " + pNameClaseEnt);
            newTexto.AppendLine("\t\t\tobj = CrearObjeto(table.Rows(0))");
            newTexto.AppendLine("\t\t\tReturn obj");
            newTexto.AppendLine("\t\tElseIf (table.Rows.Count > 1)");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de un objeto\")");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn Nothing");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Objeto filtrado por Arrays y por Parametros adicionales
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String, ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt + " Implements " + pNameClaseIn + ".ObtenerObjeto");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String, ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt);
            }

            newTexto.AppendLine("\tTry");

            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, localTrans)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count = 1) Then");

            newTexto.AppendLine("\t\t\tDim obj As " + pNameClaseEnt + " = new " + pNameClaseEnt);
            newTexto.AppendLine("\t\t\tobj = CrearObjeto(table.Rows(0))");
            newTexto.AppendLine("\t\t\tReturn obj");
            newTexto.AppendLine("\t\tElseIf (table.Rows.Count > 1)");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de un objeto\")");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn Nothing");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Objeto por Campo
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt + " Implements " + pNameClaseIn + ".ObtenerObjeto");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt);
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, localTrans)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count = 1) Then");

            newTexto.AppendLine("\t\t\tDim obj As " + pNameClaseEnt + " = new " + pNameClaseEnt);
            newTexto.AppendLine("\t\t\tobj = CrearObjeto(table.Rows(0))");
            newTexto.AppendLine("\t\t\tReturn obj");
            newTexto.AppendLine("\t\tElseIf (table.Rows.Count > 1)");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de un objeto\")");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn Nothing");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Objeto por Campo y parametros adicionales
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene los datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String, ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt + " Implements " + pNameClaseIn + ".ObtenerObjeto");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerObjeto(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String, ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt);
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, localTrans)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count = 1) Then");

            newTexto.AppendLine("\t\t\tDim obj As " + pNameClaseEnt + " = new " + pNameClaseEnt);
            newTexto.AppendLine("\t\t\tobj = CrearObjeto(table.Rows(0))");
            newTexto.AppendLine("\t\t\tReturn obj");
            newTexto.AppendLine("\t\tElseIf (table.Rows.Count > 1)");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de un objeto\")");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn Nothing");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Lista
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo List<" + pNameClaseEnt + "> que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerLista() As List(Of " + pNameClaseEnt + ") Implements " + pNameClaseIn + ".ObtenerLista");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerLista() As List(Of " + pNameClaseEnt + ")");
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'1'\")");

            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");

            newTexto.AppendLine("\t\t\tReturn CrearLista(table)");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn New List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Lista de determinadas columnas
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo List<" + pNameClaseEnt + "> que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal arrColumnas As ArrayList) As List(Of " + pNameClaseEnt + ") Implements " + pNameClaseIn + ".ObtenerLista");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal arrColumnas As ArrayList) As List(Of " + pNameClaseEnt + ")");
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");
            newTexto.AppendLine("\t\t\tReturn CrearLista(table)");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn New List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Lista de determinadas columnas y parametros adicionales
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo List<" + pNameClaseEnt + "> que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal arrColumnas As ArrayList, ByVal strParamAdicionales As String) As List(Of " + pNameClaseEnt + ") Implements " + pNameClaseIn + ".ObtenerLista");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal arrColumnas As ArrayList, ByVal strParamAdicionales As String) As List(Of " + pNameClaseEnt + ")");
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(1)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(1)");
            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");
            newTexto.AppendLine("\t\t\tReturn CrearLista(table)");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn New List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Lista filtrada por Arrays
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo List<" + pNameClaseEnt + "> que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As List(Of " + pNameClaseEnt + ") Implements " + pNameClaseIn + ".ObtenerLista");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As List(Of " + pNameClaseEnt + ")");
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");
            newTexto.AppendLine("\t\t\tReturn CrearLista(table)");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn New List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Lista filtrada por Arrays y Parametros Adiccionales
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo List<" + pNameClaseEnt + "> que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String) As List(Of " + pNameClaseEnt + ") Implements " + pNameClaseIn + ".ObtenerLista");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String) As List(Of " + pNameClaseEnt + ")");
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");
            newTexto.AppendLine("\t\t\tReturn CrearLista(table)");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn New List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Lista filtrada por campo
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo List<" + pNameClaseEnt + "> que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object) As List(Of " + pNameClaseEnt + ") Implements " + pNameClaseIn + ".ObtenerLista");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object) As List(Of " + pNameClaseEnt + ")");
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");
            newTexto.AppendLine("\t\t\tReturn CrearLista(table)");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn New List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Lista filtrada por campo y Parametros Adicionales
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo List<" + pNameClaseEnt + "> que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String) As List(Of " + pNameClaseEnt + ") Implements " + pNameClaseIn + ".ObtenerLista");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String) As List(Of " + pNameClaseEnt + ")");
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");
            newTexto.AppendLine("\t\t\tReturn CrearLista(table)");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn New List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Lista de determinadas columnas con TRANS
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo List<" + pNameClaseEnt + "> que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal arrColumnas As ArrayList, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ") Implements " + pNameClaseIn + ".ObtenerLista");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal arrColumnas As ArrayList, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ")");
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableGeneral + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, localTrans)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");
            newTexto.AppendLine("\t\t\tReturn CrearLista(table)");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn New List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Lista de determinadas columnas y parametros adicionales
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo List<" + pNameClaseEnt + "> que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal arrColumnas As ArrayList, ByVal strParamAdicionales As String, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ") Implements " + pNameClaseIn + ".ObtenerLista");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal arrColumnas As ArrayList, ByVal strParamAdicionales As String, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ")");
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(1)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(1)");
            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, localTrans)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");
            newTexto.AppendLine("\t\t\tReturn CrearLista(table)");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn New List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Lista filtrada por Arrays TRANS
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo List<" + pNameClaseEnt + "> que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ") Implements " + pNameClaseIn + ".ObtenerLista");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ")");
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, localTrans)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");
            newTexto.AppendLine("\t\t\tReturn CrearLista(table)");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn New List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Lista filtrada por Arrays y Parametros Adiccionales
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo List<" + pNameClaseEnt + "> que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ") Implements " + pNameClaseIn + ".ObtenerLista");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ")");
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, localTrans)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");
            newTexto.AppendLine("\t\t\tReturn CrearLista(table)");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn New List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Lista filtrada por campo
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo List<" + pNameClaseEnt + "> que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ") Implements " + pNameClaseIn + ".ObtenerLista");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ")");
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, localTrans)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");
            newTexto.AppendLine("\t\t\tReturn CrearLista(table)");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn New List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //Obtener Lista filtrada por campo y Parametros Adicionales
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene un conjunto de datos de una Clase " + pNameClaseEnt + " a partir de condiciones WHERE");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"localTrans\" type=\"" + namespaceClases + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo List<" + pNameClaseEnt + "> que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ") Implements " + pNameClaseIn + ".ObtenerLista");
            }
            else
            {
                newTexto.AppendLine("Public Function ObtenerLista(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ")");
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, localTrans)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");
            newTexto.AppendLine("\t\t\tReturn CrearLista(table)");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn New List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //CreatePk
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que obtiene la llave primaria unica de la tabla " + pNameTabla + " a partir de una cadena");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cod\" type=\"string\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Cadena desde la que se construye el identificador unico de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \t Identificador unico de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function CreatePk(ByVal args() As String) As String Implements " + pNameClaseIn + ".CreatePK");
            }
            else
            {
                newTexto.AppendLine("Public Function CreatePk(ByVal args() As String) As String");
            }
            newTexto.AppendLine("\tReturn args(0)");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            if (formulario.chkUsarSps.Checked)
            {
                newTexto.AppendLine(cABMs.CrearABMsProcAlmVB(dtColumns, pNameTabla, ref formulario));
            }
            else
            {
                newTexto.AppendLine(cABMs.CrearABMsCodigoVB(dtColumns, pNameTabla, ref formulario));
            }

            //NUEVO DATATABLE
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un DataTable con los registros de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function NuevoDataTable() As DataTable Implements " + pNameClaseIn + ".NuevoDataTable");
            }
            else
            {
                newTexto.AppendLine("Public Function NuevoDataTable() As DataTable");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim dc As DataColumn");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                newTexto.AppendLine("\t\tdc = new DataColumn(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString(), GetType(" + tipoCol + "))");
                newTexto.AppendLine("\t\ttable.Columns.Add(dc)\r\n");
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tReturn table");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //NUEVO DATATABLE CON ARRCOLS
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que genera un DataTable con determinadas columnas de una " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function NuevoDataTable(ByVal arrColumnas As ArrayList) As DataTable Implements " + pNameClaseIn + ".NuevoDataTable");
            }
            else
            {
                newTexto.AppendLine("Public Function NuevoDataTable(ByVal arrColumnas As ArrayList) As DataTable");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'2'\")");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");

            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tReturn table");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //DATATABLE
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un DataTable con los registros de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function CargarDataTable() As DataTable Implements " + pNameClaseIn + ".CargarDataTable");
            }
            else
            {
                newTexto.AppendLine("Public Function CargarDataTable() As DataTable");
            }
            newTexto.AppendLine("\tTry");

            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");

            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tReturn table");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //DATATABLE 1 CON CONDICIONES StringWhere
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"condicionesWhere\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal condicionesWhere As String) As DataTable Implements " + pNameClaseIn + ".CargarDataTable");
            }
            else
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal condicionesWhere As String) As DataTable");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");

            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, \" AND (\" + condicionesWhere + \")\")");

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tReturn table");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //DATATABLE 2 CON arrColumnas
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal arrColumnas As ArrayList) As DataTable Implements " + pNameClaseIn + ".CargarDataTable");
            }
            else
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal arrColumnas As ArrayList) As DataTable");
            }

            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");

            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tReturn table");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //DATATABLE 2.5 CON arrColumnas y condicionesWhere
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Parametros adicionales");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal arrColumnas As ArrayList, ByVal strParametrosAdicionales As String) As DataTable Implements " + pNameClaseIn + ".CargarDataTable");
            }
            else
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal arrColumnas As ArrayList, ByVal strParametrosAdicionales As String) As DataTable");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tReturn table");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //DATATABLE 3 CON arrCOlumnas, arrColumnasWhere y arrValoresWhere
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As DataTable Implements " + pNameClaseIn + ".CargarDataTable");
            }
            else
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As DataTable");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tReturn table");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //DATATABLE 3.5 CON arrColumnasWhere y arrValoresWhere
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As DataTable Implements " + pNameClaseIn + ".CargarDataTable");
            }
            else
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As DataTable");
            }
            newTexto.AppendLine("\tTry");

            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");

            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tReturn table");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");

            newTexto.AppendLine("");

            //DATATABLE 4 con arrColumnas, arrColumnasWhere, arrValoresWhere y condicionesWhere
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Parametros adicionales");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String) As DataTable Implements " + pNameClaseIn + ".CargarDataTable");
            }
            else
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String) As DataTable");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");

            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales)");

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tReturn table");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //DATATABLE 4.5 con arrColumnasWhere, arrValoresWhere y condicionesWhere
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Parametros adicionales");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String) As DataTable Implements " + pNameClaseIn + ".CargarDataTable");
            }
            else
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String) As DataTable");
            }

            newTexto.AppendLine("\tTry");

            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tReturn table");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //DATATABLE 5 con Objeto y valor
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene los datos a partir de un filtro realizado por algun campo");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tDataTable que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object) As DataTable Implements " + pNameClaseIn + ".CargarDataTable");
            }
            else
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object) As DataTable");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");
            newTexto.AppendLine("\t\t\tReturn table");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn new DataTable()");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //DATATABLE 5 con Objeto y valor y ByVal strParamAdicionales As String
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene los datos a partir de un filtro realizado por algun campo");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Parametros adicionales");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tDataTable que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParametrosAdicionales As String) As DataTable Implements " + pNameClaseIn + ".CargarDataTable");
            }
            else
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParametrosAdicionales As String) As DataTable");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");
            newTexto.AppendLine("\t\t\tReturn table");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn new DataTable()");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //DATATABLE 5.1 con ArrColumnas, Objeto y valor y ByVal strParamAdicionales As String
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene los datos a partir de un filtro realizado por algun campo");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Parametros adicionales");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tDataTable que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal arrColumnas As ArrayList, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParametrosAdicionales As String) As DataTable Implements " + pNameClaseIn + ".CargarDataTable");
            }
            else
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal arrColumnas As ArrayList, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParametrosAdicionales As String) As DataTable");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");
            newTexto.AppendLine("\t\t\tReturn table");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn new DataTable()");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //DATATABLE 5.2 con ArrColumnas, Objeto y valor
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que obtiene los datos a partir de un filtro realizado por algun campo");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tDataTable que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal arrColumnas As ArrayList, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object) As DataTable Implements " + pNameClaseIn + ".CargarDataTable");
            }
            else
            {
                newTexto.AppendLine("Public Function CargarDataTable(ByVal arrColumnas As ArrayList, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object) As DataTable");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tIf (table.Rows.Count > 0) Then");
            newTexto.AppendLine("\t\t\tReturn table");
            newTexto.AppendLine("\t\tElse");
            newTexto.AppendLine("\t\t\tReturn new DataTable()");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");
            newTexto.AppendLine("");

            //DATATABLE OR
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function CargarDataTableOr(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As DataTable Implements " + pNameClaseIn + ".CargarDataTableOr");
            }
            else
            {
                newTexto.AppendLine("Public Function CargarDataTableOr(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As DataTable");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableOr + "(\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tReturn table");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");

            newTexto.AppendLine("");

            //DATATABLE OR 1
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function CargarDataTableOr(ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As DataTable Implements " + pNameClaseIn + ".CargarDataTableOr");
            }
            else
            {
                newTexto.AppendLine("Public Function CargarDataTableOr(ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As DataTable");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableOr + "(\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tReturn table");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");

            newTexto.AppendLine("");

            //DATATABLE OR 2
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Parametros adicionales");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function CargarDataTableOr(ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String) As DataTable Implements " + pNameClaseIn + ".CargarDataTableOr");
            }
            else
            {
                newTexto.AppendLine("Public Function CargarDataTableOr(ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String) As DataTable");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableOr + "(\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tReturn table");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");

            newTexto.AppendLine("");

            string tipoCombo = null;
            string tipoComboMini = null;
            string tipoGrid = null;
            string tipoGridMini = null;
            string refreshCombo = null;
            string RefreshGrid = null;

            if (miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
            {
                tipoCombo = "System.Windows.Forms.ComboBox";
                tipoComboMini = "ComboBox";
                tipoGrid = "System.Windows.Forms.DataGridView";
                tipoGridMini = "DataGridView";

                refreshCombo = "\t\t\t\t\tcmb.ValueMember = table.Columns(0).ColumnName\r\n\t\t\t\t\tcmb.DisplayMember = table.Columns(1).ColumnName\r\n";

                RefreshGrid = "\t\t\t\tdtg.DataSource = table\r\n";
            }
            else
            {
                tipoCombo = "System.Web.UI.WebControls.DropDownList";
                tipoComboMini = "DropDownList";
                tipoGrid = "System.Web.UI.WebControls.GridView";
                tipoGridMini = "GridView";

                refreshCombo = "\t\t\t\t\tcmb.DataValueField = table.Columns(0).ColumnName\r\n\t\t\t\t\tcmb.DataTextField = table.Columns(1).ColumnName\r\n\t\t\t\t\tcmb.DataBind()\r\n";

                RefreshGrid = "\t\t\t\tdtg.DataSource = table\r\n\t\t\t\tdtg.DataBind()\r\n";
            }

            //Cargar COMBO
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ") Implements " + pNameClaseIn + ".CargarCombo");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ")");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ".ToString())");
            if (dtColumns.Rows.Count == 1)
            {
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ".ToString())");
            }
            else
            {
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[1][0] + ".ToString())");
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableGeneral + "(\"" + strAliasSel + "\", arrColumnas)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcmb.DataSource = table");
            newTexto.AppendLine("\t\tIf (table.Columns.Count > 0) Then");
            newTexto.AppendLine(refreshCombo);
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar COMBO 1.5
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal strParamAdicionales As String) Implements " + pNameClaseIn + ".CargarCombo");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal strParamAdicionales As String)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ".ToString())");
            if (dtColumns.Rows.Count == 1)
            {
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ".ToString())");
            }
            else
            {
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[1][0] + ".ToString())");
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(1)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(1)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcmb.DataSource = table");
            newTexto.AppendLine("\t\tIf (table.Columns.Count > 0) Then");
            newTexto.AppendLine(refreshCombo);
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar COMBO 2
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) Implements " + pNameClaseIn + ".CargarCombo");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ".ToString())");
            if (dtColumns.Rows.Count == 1)
            {
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ".ToString())");
            }
            else
            {
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[1][0] + ".ToString())");
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcmb.DataSource = table");
            newTexto.AppendLine("\t\tIf (table.Columns.Count > 0) Then");
            newTexto.AppendLine(refreshCombo);
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar COMBO 2.5
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String) Implements " + pNameClaseIn + ".CargarCombo");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ".ToString())");
            if (dtColumns.Rows.Count == 1)
            {
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ".ToString())");
            }
            else
            {
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[1][0] + ".ToString())");
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcmb.DataSource = table");
            newTexto.AppendLine("\t\tIf (table.Columns.Count > 0) Then");
            newTexto.AppendLine(refreshCombo);
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar COMBO 3
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"textField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields) Implements " + pNameClaseIn + ".CargarCombo");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(valueField.ToString())");
            newTexto.AppendLine("\t\tarrColumnas.Add(textField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableGeneral + "(\"" + strAliasSel + "\", arrColumnas)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcmb.DataSource = table");
            newTexto.AppendLine("\t\tIf (table.Columns.Count > 0) Then");
            newTexto.AppendLine(refreshCombo);
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar COMBO 3.25
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"textField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields, ByVal strParamAdicionales As String) Implements " + pNameClaseIn + ".CargarCombo");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields, ByVal strParamAdicionales As String)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(valueField.ToString())");
            newTexto.AppendLine("\t\tarrColumnas.Add(textField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(1)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(1)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcmb.DataSource = table");
            newTexto.AppendLine("\t\tIf (table.Columns.Count > 0) Then");
            newTexto.AppendLine(refreshCombo);
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar COMBO 3.5
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"textField\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String) Implements " + pNameClaseIn + ".CargarCombo");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(valueField.ToString())");
            newTexto.AppendLine("\t\tarrColumnas.Add(textField)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableGeneral + "(\"" + strAliasSel + "\", arrColumnas)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcmb.DataSource = table");
            newTexto.AppendLine("\t\tIf (table.Columns.Count > 0) Then");
            newTexto.AppendLine(refreshCombo);
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar COMBO 3.75
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"textField\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String, ByVal strParamAdicionales As String) Implements " + pNameClaseIn + ".CargarCombo");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String, ByVal strParamAdicionales As String)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(valueField.ToString())");
            newTexto.AppendLine("\t\tarrColumnas.Add(textField)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(1)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(1)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcmb.DataSource = table");
            newTexto.AppendLine("\t\tIf (table.Columns.Count > 0) Then");
            newTexto.AppendLine(refreshCombo);
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar COMBO 4
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"textField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object) Implements " + pNameClaseIn + ".CargarCombo");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(valueField.ToString())");
            newTexto.AppendLine("\t\tarrColumnas.Add(textField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcmb.DataSource = table");
            newTexto.AppendLine("\t\tIf (table.Columns.Count > 0) Then");
            newTexto.AppendLine(refreshCombo);
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar COMBO 4.25
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"textField\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object) Implements " + pNameClaseIn + ".CargarCombo");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(valueField.ToString())");
            newTexto.AppendLine("\t\tarrColumnas.Add(textField)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcmb.DataSource = table");
            newTexto.AppendLine("\t\tIf (table.Columns.Count > 0) Then");
            newTexto.AppendLine(refreshCombo);
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar COMBO 4.5
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"textField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String) Implements " + pNameClaseIn + ".CargarCombo");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(valueField.ToString())");
            newTexto.AppendLine("\t\tarrColumnas.Add(textField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcmb.DataSource = table");
            newTexto.AppendLine("\t\tIf (table.Columns.Count > 0) Then");
            newTexto.AppendLine(refreshCombo);
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar COMBO 4.75
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"textField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String) Implements " + pNameClaseIn + ".CargarCombo");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(valueField.ToString())");
            newTexto.AppendLine("\t\tarrColumnas.Add(textField)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcmb.DataSource = table");
            newTexto.AppendLine("\t\tIf (table.Columns.Count > 0) Then");
            newTexto.AppendLine(refreshCombo);
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar COMBO 5
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"textField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) Implements " + pNameClaseIn + ".CargarCombo");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(valueField.ToString())");
            newTexto.AppendLine("\t\tarrColumnas.Add(textField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcmb.DataSource = table");
            newTexto.AppendLine("\t\tIf (table.Columns.Count > 0) Then");
            newTexto.AppendLine(refreshCombo);
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar COMBO 5.25
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"textField\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) Implements " + pNameClaseIn + ".CargarCombo");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(valueField.ToString())");
            newTexto.AppendLine("\t\tarrColumnas.Add(textField)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcmb.DataSource = table");
            newTexto.AppendLine("\t\tIf (table.Columns.Count > 0) Then");
            newTexto.AppendLine(refreshCombo);
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar COMBO 5.5
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"textField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String) Implements " + pNameClaseIn + ".CargarCombo");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(valueField.ToString())");
            newTexto.AppendLine("\t\tarrColumnas.Add(textField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcmb.DataSource = table");
            newTexto.AppendLine("\t\tIf (table.Columns.Count > 0) Then");
            newTexto.AppendLine(refreshCombo);
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar COMBO 5.5
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"textField\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String) Implements " + pNameClaseIn + ".CargarCombo");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(valueField.ToString())");
            newTexto.AppendLine("\t\tarrColumnas.Add(textField)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcmb.DataSource = table");
            newTexto.AppendLine("\t\tIf (table.Columns.Count > 0) Then");
            newTexto.AppendLine(refreshCombo);
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            // Cargar GRID
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ") Implements " + pNameClaseIn + ".CargarGridView");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ")");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine(RefreshGrid);
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar GRID  1
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"condicionesWhere\" type=\"String\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t  Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal condicionesWhere As String) Implements " + pNameClaseIn + ".CargarGridView");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal condicionesWhere As String)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, \" AND (\" + condicionesWhere + \")\")");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine(RefreshGrid);
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar GRID  2
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnas As ArrayList) Implements " + pNameClaseIn + ".CargarGridView");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnas As ArrayList)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine(RefreshGrid);
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar GRID  2.5
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal strParametrosAdicionales As String) Implements " + pNameClaseIn + ".CargarGridView");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal strParametrosAdicionales As String)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(\"'1'\")");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine(RefreshGrid);
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");
            newTexto.AppendLine("");

            //Cargar GRID 3
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) Implements " + pNameClaseIn + ".CargarGridView");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine(RefreshGrid);
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");

            //Cargar GRID 3.5
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String) Implements " + pNameClaseIn + ".CargarGridView");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine(RefreshGrid);
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");

            //Cargar GRID  4
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) Implements " + pNameClaseIn + ".CargarGridView");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine(RefreshGrid);
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");

            //Cargar GRID  4.5
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String) Implements " + pNameClaseIn + ".CargarGridView");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine(RefreshGrid);
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");

            //Cargar GRID  5
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un GridView con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoGrid + "");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object) Implements " + pNameClaseIn + ".CargarGridView");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine(RefreshGrid);
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");

            //Cargar GRID  5.5
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un GridView con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoGrid + "");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParametrosAdicionales As String) Implements " + pNameClaseIn + ".CargarGridView");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParametrosAdicionales As String)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())");
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");

            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine(RefreshGrid);
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");

            //Cargar GRID  6
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un GridView con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoGrid + "");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object) Implements " + pNameClaseIn + ".CargarGridView");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine(RefreshGrid);
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");

            //Cargar GRID  6.5
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un GridView con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoGrid + "");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo por el que se va a filtrar la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"searchValue\" type=\"object\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Valor para la busqueda");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParametrosAdicionales As String) Implements " + pNameClaseIn + ".CargarGridView");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarGridView(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParametrosAdicionales As String)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrColumnasWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnasWhere.Add(searchField.ToString())");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim arrValoresWhere As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrValoresWhere.Add(searchValue)");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableAnd + "(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine(RefreshGrid);
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");

            //Cargar GRID  OR
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarGridViewOr(ByRef dtg As " + tipoGridMini + " , ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) Implements " + pNameClaseIn + ".CargarGridViewOr");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarGridViewOr(ByRef dtg As " + tipoGridMini + " , ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableOr + "(\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine(RefreshGrid);
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");

            //Cargar GRID Or 2
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Sub CargarGridViewOr(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String) Implements " + pNameClaseIn + ".CargarGridViewOr");
            }
            else
            {
                newTexto.AppendLine("Public Sub CargarGridViewOr(ByRef dtg As " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String)");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim table As DataTable = New DataTable");
            newTexto.AppendLine("\t\tDim local As " + paramClaseConeccion + " = new " + paramClaseConeccion);
            newTexto.AppendLine("\t\ttable = local." + funcionCargarDataTableOr + "(\"" + strAliasSel + "\", arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine(RefreshGrid);
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Sub");

            //FUNCIONES DE AGREGADO: COUNT
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que devuelve el resultado de la funcion [SQL] COUNT");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo al que se va a aplicar la funcion");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function FuncionesCount(ByVal refField As " + pNameClaseEnt + ".Fields) As Object Implements " + pNameClaseIn + ".FuncionesCount");
            }
            else
            {
                newTexto.AppendLine("Public Function FuncionesCount(ByVal refField As " + pNameClaseEnt + ".Fields) As Object");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(\"count(\" + refField + \")\")");
            newTexto.AppendLine("\t\tDim dtTemp As DataTable = CargarDataTable(arrColumnas)");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count = 0) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"La consulta no ha devuelto resultados.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count > 1) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de una fila.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tReturn dtTemp.Rows(0)(0)");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");

            //FUNCIONES DE AGREGADO: COUNT con arrays
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que devuelve el resultado de la funcion [SQL] COUNT");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo al que se va a aplicar la funcion");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function FuncionesCount(ByVal refField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As Object Implements " + pNameClaseIn + ".FuncionesCount");
            }
            else
            {
                newTexto.AppendLine("Public Function FuncionesCount(ByVal refField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As Object");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(\"count(\" + refField + \")\")");
            newTexto.AppendLine("\t\tDim dtTemp As DataTable = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count = 0) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"La consulta no ha devuelto resultados.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count > 1) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de una fila.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tReturn dtTemp.Rows(0)(0)");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");

            //FUNCIONES DE AGREGADO: MIN
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que devuelve el resultado de la funcion [SQL] MIN");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo al que se va a aplicar la funcion");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function FuncionesMin(ByVal refField As " + pNameClaseEnt + ".Fields) As Object Implements " + pNameClaseIn + ".FuncionesMin");
            }
            else
            {
                newTexto.AppendLine("Public Function FuncionesMin(ByVal refField As " + pNameClaseEnt + ".Fields) As Object");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(\"min(\" + refField + \")\")");
            newTexto.AppendLine("\t\tDim dtTemp As DataTable = CargarDataTable(arrColumnas)");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count = 0) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"La consulta no ha devuelto resultados.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count > 1) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de una fila.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tReturn dtTemp.Rows(0)(0)");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");

            //FUNCIONES DE AGREGADO: MIN con arrays
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que devuelve el resultado de la funcion [SQL] MIN");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo al que se va a aplicar la funcion");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function FuncionesMin(ByVal refField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As Object Implements " + pNameClaseIn + ".FuncionesMin");
            }
            else
            {
                newTexto.AppendLine("Public Function FuncionesMin(ByVal refField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As Object");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(\"min(\" + refField + \")\")");
            newTexto.AppendLine("\t\tDim dtTemp As DataTable = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count = 0) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"La consulta no ha devuelto resultados.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count > 1) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de una fila.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tReturn dtTemp.Rows(0)(0)");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");

            //FUNCIONES DE AGREGADO: MAX
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que devuelve el resultado de la funcion [SQL] MAX");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo al que se va a aplicar la funcion");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function FuncionesMax(ByVal refField As " + pNameClaseEnt + ".Fields) As Object Implements " + pNameClaseIn + ".FuncionesMax");
            }
            else
            {
                newTexto.AppendLine("Public Function FuncionesMax(ByVal refField As " + pNameClaseEnt + ".Fields) As Object");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(\"max(\" + refField + \")\")");
            newTexto.AppendLine("\t\tDim dtTemp As DataTable = CargarDataTable(arrColumnas)");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count = 0) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"La consulta no ha devuelto resultados.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count > 1) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de una fila.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tReturn dtTemp.Rows(0)(0)");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");

            //FUNCIONES DE AGREGADO: MAX con arrays
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que devuelve el resultado de la funcion [SQL] MAX");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo al que se va a aplicar la funcion");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function FuncionesMax(ByVal refField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As Object Implements " + pNameClaseIn + ".FuncionesMax");
            }
            else
            {
                newTexto.AppendLine("Public Function FuncionesMax(ByVal refField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As Object");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(\"max(\" + refField + \")\")");
            newTexto.AppendLine("\t\tDim dtTemp As DataTable = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count = 0) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"La consulta no ha devuelto resultados.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count > 1) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de una fila.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tReturn dtTemp.Rows(0)(0)");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");

            //FUNCIONES DE AGREGADO: SUM
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que devuelve el resultado de la funcion [SQL] SUM");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo al que se va a aplicar la funcion");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function FuncionesSum(ByVal refField As " + pNameClaseEnt + ".Fields) As Object Implements " + pNameClaseIn + ".FuncionesSum");
            }
            else
            {
                newTexto.AppendLine("Public Function FuncionesSum(ByVal refField As " + pNameClaseEnt + ".Fields) As Object");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(\"sum(\" + refField + \")\")");
            newTexto.AppendLine("\t\tDim dtTemp As DataTable = CargarDataTable(arrColumnas)");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count = 0) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"La consulta no ha devuelto resultados.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count > 1) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de una fila.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tReturn dtTemp.Rows(0)(0)");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");

            //FUNCIONES DE AGREGADO: SUM con arrays
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que devuelve el resultado de la funcion [SQL] SUM");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo al que se va a aplicar la funcion");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function FuncionesSum(ByVal refField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As Object Implements " + pNameClaseIn + ".FuncionesSum");
            }
            else
            {
                newTexto.AppendLine("Public Function FuncionesSum(ByVal refField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As Object");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(\"sum(\" + refField + \")\")");
            newTexto.AppendLine("\t\tDim dtTemp As DataTable = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count = 0) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"La consulta no ha devuelto resultados.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count > 1) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de una fila.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tReturn dtTemp.Rows(0)(0)");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");

            //FUNCIONES DE AGREGADO: AVG
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que devuelve el resultado de la funcion [SQL] AVG");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo al que se va a aplicar la funcion");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function FuncionesAvg(ByVal refField As " + pNameClaseEnt + ".Fields) As Object Implements " + pNameClaseIn + ".FuncionesAvg");
            }
            else
            {
                newTexto.AppendLine("Public Function FuncionesAvg(ByVal refField As " + pNameClaseEnt + ".Fields) As Object");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(\"avg(\" + refField + \")\")");
            newTexto.AppendLine("\t\tDim dtTemp As DataTable = CargarDataTable(arrColumnas)");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count = 0) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"La consulta no ha devuelto resultados.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count > 1) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de una fila.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tReturn dtTemp.Rows(0)(0)");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");

            //FUNCIONES DE AGREGADO: AVG con arrays
            newTexto.AppendLine("''' <summary>");
            newTexto.AppendLine("''' \tFuncion que devuelve el resultado de la funcion [SQL] AVG");
            newTexto.AppendLine("''' </summary>");
            newTexto.AppendLine("''' <param name=\"refField\" type=\"" + pNameClaseEnt + ".Fileds\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Campo al que se va a aplicar la funcion");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("'''     <para>");
            newTexto.AppendLine("''' \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("'''     </para>");
            newTexto.AppendLine("''' </param>");
            newTexto.AppendLine("''' <returns>");
            newTexto.AppendLine("''' \tValor del Tipo " + pNameClaseEnt + " que cumple con los filtros de los parametros");
            newTexto.AppendLine("''' </returns>");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("Public Function FuncionesAvg(ByVal refField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As Object Implements " + pNameClaseIn + ".FuncionesAvg");
            }
            else
            {
                newTexto.AppendLine("Public Function FuncionesAvg(ByVal refField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As Object");
            }
            newTexto.AppendLine("\tTry");
            newTexto.AppendLine("\t\tDim arrColumnas As ArrayList = New ArrayList");
            newTexto.AppendLine("\t\tarrColumnas.Add(\"avg(\" + refField + \")\")");
            newTexto.AppendLine("\t\tDim dtTemp As DataTable = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere)");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count = 0) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"La consulta no ha devuelto resultados.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tIf (dtTemp.Rows.Count > 1) Then");
            newTexto.AppendLine("\t\t\tThrow new Exception(\"Se ha devuelto mas de una fila.\")");
            newTexto.AppendLine("\t\tEnd If");
            newTexto.AppendLine("\t\tReturn dtTemp.Rows(0)(0)");
            newTexto.AppendLine("\tCatch exp As Exception");
            newTexto.AppendLine("\t\tThrow exp");
            newTexto.AppendLine("\tEnd Try");
            newTexto.AppendLine("End Function");

            if (formulario.chkReAlInterface.Checked)
            {
                newTexto.AppendLine("\r\n\t\t#End Region\r\n");
            }

            //Crear Objeto a Partir de un Datarow
            newTexto.AppendLine("\t''' <summary>");
            newTexto.AppendLine("\t''' \t Funcion que devuelve un Objeto a partir de un DataRow " + pNameTabla + " a partir de una cadena");
            newTexto.AppendLine("\t''' </summary>");
            newTexto.AppendLine("\t''' <param name=\"dt" + pNameTabla + "\" type=\"System.Data.DateRow\">");
            newTexto.AppendLine("\t'''     <para>");
            newTexto.AppendLine("\t''' \t\t DataRow con el schema similar al del Objeto ");
            newTexto.AppendLine("\t'''     </para>");
            newTexto.AppendLine("\t''' </param>");
            newTexto.AppendLine("\t''' <returns>");
            newTexto.AppendLine("\t''' \t Objeto " + pNameTabla);
            newTexto.AppendLine("\t''' </returns>");

            newTexto.AppendLine("\tFriend Function CrearObjeto(ByVal row As DataRow) As " + pNameClaseEnt + "");
            newTexto.AppendLine("\t\tDim obj As " + pNameClaseEnt + " = New " + pNameClaseEnt);
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                permiteNull = (dtColumns.Rows[iCol]["PermiteNull"].ToString().ToUpper() == "YES" ? true : false);

                if (permiteNull)
                {
                    newTexto.AppendLine("\t\tIf String.IsNullOrEmpty(row(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString()).ToString())Then");
                    newTexto.AppendLine("\t\t\tobj." + nameCol + " = Nothing");
                    newTexto.AppendLine("\t\tElse");
                    newTexto.AppendLine("\t\t\tobj." + nameCol + " = Convert.ChangeType(row(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString()), GetType(" + tipoCol + "))");
                    newTexto.AppendLine("\t\tEnd If");
                    newTexto.AppendLine("");
                }
                else
                {
                    newTexto.AppendLine("\t\tobj." + nameCol + " = Convert.ChangeType(row(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString()), (GetType(" + pNameClaseEnt + ").GetProperty(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString())).PropertyType())");
                }
            }
            newTexto.AppendLine("\tReturn obj");
            newTexto.AppendLine("End Function");

            //Crear Lista de Objetos
            newTexto.AppendLine("\t''' <summary>");
            newTexto.AppendLine("\t''' \t Funcion que crea una Lista de objetos a partir de un DataTable " + pNameTabla + " a partir de una cadena");
            newTexto.AppendLine("\t''' </summary>");
            newTexto.AppendLine("\t''' <param name=\"dt" + pNameTabla + "\" type=\"System.Data.DateTable\">");
            newTexto.AppendLine("\t'''     <para>");
            newTexto.AppendLine("\t''' \t\t DataTable con el conjunto de Datos recuperados ");
            newTexto.AppendLine("\t'''     </para>");
            newTexto.AppendLine("\t''' </param>");
            newTexto.AppendLine("\t''' <returns>");
            newTexto.AppendLine("\t''' \t Lista de Objetos " + pNameTabla);
            newTexto.AppendLine("\t''' </returns>");

            newTexto.AppendLine("\tFriend Function CrearLista(ByVal dt" + pNameTabla + " As DataTable) As List(Of " + pNameClaseEnt + ")");

            newTexto.AppendLine("\t\tDim list As List(Of " + pNameClaseEnt + ") = New List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tDim obj As " + pNameClaseEnt);
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tFor Each row As DataRow In dt" + pNameTabla + ".Rows");
            newTexto.AppendLine("\t\t\tobj = CrearObjeto(row)");
            newTexto.AppendLine("\t\t\tlist.Add(obj)");
            newTexto.AppendLine("\t\tNext");
            newTexto.AppendLine("\t\tReturn list");
            newTexto.AppendLine("\tEnd Function");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("");
            newTexto.AppendLine("End Class");
            //newTexto.AppendLine("End Namespace")

            if (miEntorno == TipoEntorno.VB_Net_WCF)
            {
                CFunciones.CrearArchivo(newTexto.ToString(), pNameClase + ".svc.vb", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");
            }
            else
            {
                CFunciones.CrearArchivo(newTexto.ToString(), pNameClase + ".vb", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");
            }

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\" + pNameClase + ".vb";
        }
    }
}