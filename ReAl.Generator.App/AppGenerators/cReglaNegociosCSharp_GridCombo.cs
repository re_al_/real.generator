﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using System;
using System.Collections;
using System.Data;

namespace ReAl.Generator.App.AppGenerators
{
    public static class cReglaNegociosCSharp_GridCombo
    {
        #region Methods

        public static string CrearModelo(DataTable dtColumns, ref FPrincipal formulario, string pNameTabla, string pNameClaseEnt, string strLlavePrimaria, ArrayList llavePrimaria)
        {
            TipoEntorno miEntorno;
            Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            string nameCol = "";

            string paramClaseConeccion = (string.IsNullOrEmpty(formulario.txtClaseConn.Text) ? "cConn" : formulario.txtClaseConn.Text);
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);

            string funcionCargarDataReaderAnd = null;
            string funcionCargarDataReaderOr = null;
            string funcionCargarDataTableAnd = null;
            string funcionCargarDataTableAndFromView = null;
            string funcionCargarDataTableOr = null;

            string strAlias = null;
            try
            {
                DataTable dtPrefijo = new DataTable();

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.SQLServer2000:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.Oracle:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT ALIASSTA FROM SEGTABLAS WHERE UPPER(TABLASTA) = '" + pNameTabla.ToUpper() + "'");
                        break;

                    case TipoBD.PostgreSQL:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.aliassta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" + pNameTabla + "')");
                        if (dtPrefijo.Rows.Count == 0)
                        {
                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.alias FROM seg_tablas s WHERE UPPER(s.nombretabla) = UPPER('" + pNameTabla + "')");
                        }
                        break;
                }

                if (dtPrefijo.Rows.Count > 0)
                {
                    strAlias = dtPrefijo.Rows[0][0].ToString().ToLower();
                    strAlias = strAlias.Substring(0, 1).ToUpper() + strAlias.Substring(1, strAlias.Length - 1);
                }
                else
                {
                    strAlias = pNameTabla;
                }
            }
            catch (Exception ex)
            {
                strAlias = pNameTabla;
            }

            string strAliasSel = "";
            if (formulario.chkUsarSpsSelect.Checked)
            {
                if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                    miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                    miEntorno == TipoEntorno.CSharp_WebForms ||
                    miEntorno == TipoEntorno.CSharp_WebAPI ||
                    miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
                {
                    funcionCargarDataReaderAnd = "ExecStoreProcedureDataReaderSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataReaderOr = "ExecStoreProcedureDataReaderSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAnd = "ExecStoreProcedureSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAndFromView = "ExecStoreProcedureSel(strVista";
                    funcionCargarDataTableOr = "ExecStoreProcedureSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    strAliasSel = strAlias;
                }
                else
                {
                    funcionCargarDataReaderAnd = "execStoreProcedureDataReaderSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataReaderOr = "execStoreProcedureDataReaderSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAnd = "execStoreProcedureSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAndFromView = "execStoreProcedureSel(strVista";
                    funcionCargarDataTableOr = "execStoreProcedureSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    strAliasSel = strAlias;
                }
            }
            else
            {
                if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                    miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                    miEntorno == TipoEntorno.CSharp_WebForms ||
                    miEntorno == TipoEntorno.CSharp_WebAPI ||
                    miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
                {
                    funcionCargarDataReaderAnd = "CargarDataReaderAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataReaderOr = "CargarDataReaderOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAnd = "CargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAndFromView = "CargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + "strVista";
                    funcionCargarDataTableOr = "CargarDataTableOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                }
                else
                {
                    funcionCargarDataReaderAnd = "cargarDataReaderAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataReaderOr = "cargarDataReaderOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAnd = "cargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAndFromView = "cargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "strVista";
                    funcionCargarDataTableOr = "cargarDataTableOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                }

                strAliasSel = pNameTabla;
            }

            string tipoCombo = null;
            string tipoComboMini = null;
            string tipoGrid = null;
            string tipoGridMini = null;
            string refreshCombo = null;
            string RefreshGrid = null;
            string nombreFuncionCombo = null;
            string nombreFuncionGrid = null;
            bool bRepetir = false;

            if (miEntorno == TipoEntorno.CSharp_WinForms)
            {
                tipoCombo = "System.Windows.Forms.ComboBox";
                tipoComboMini = "ComboBox";
                tipoGrid = "System.Windows.Forms.DataGridView";
                tipoGridMini = "DataGridView";
                nombreFuncionCombo = "CargarComboBox";
                nombreFuncionGrid = "CargarDataGrid";

                refreshCombo = "\t\t\t\t\tcmb.ValueMember = table.Columns[0].ColumnName;\r\n\t\t\t\t\tcmb.DisplayMember = table.Columns[1].ColumnName;\r\n\t\t\t\t\tcmb.DataSource = table;\r\n";

                RefreshGrid = "\t\t\t\tdtg.DataSource = dsReader;\r\n";
            }
            else if (miEntorno == TipoEntorno.CSharp_WebForms)
            {
                tipoCombo = "System.Web.UI.WebControls.DropDownList";
                tipoComboMini = "DropDownList";
                tipoGrid = "System.Web.UI.WebControls.GridView";
                tipoGridMini = "GridView";
                nombreFuncionCombo = "CargarDropDownList";
                nombreFuncionGrid = "CargarGridView";

                refreshCombo = "\t\t\t\t\tcmb.DataValueField = table.Columns[0].ColumnName;\r\n\t\t\t\t\tcmb.DataTextField = table.Columns[1].ColumnName;\r\n\t\t\t\t\tcmb.DataSource = table;\r\n\t\t\t\t\tcmb.DataBind();\r\n";

                RefreshGrid = "\t\t\t\tdtg.DataSource = dsReader;\r\n\t\t\t\tdtg.DataBind();\r\n";
            }
            else if (miEntorno == TipoEntorno.CSharp)
            {
                tipoCombo = "System.Web.UI.WebControls.DropDownList";
                tipoComboMini = "DropDownList";
                tipoGrid = "System.Web.UI.WebControls.GridView";
                tipoGridMini = "GridView";
                nombreFuncionCombo = "CargarDropDownList";
                nombreFuncionGrid = "CargarGridView";

                refreshCombo = "\t\t\t\t\tcmb.DataValueField = table.Columns[0].ColumnName;\r\n\t\t\t\t\tcmb.DataTextField = table.Columns[1].ColumnName;\r\n\t\t\t\t\tcmb.DataSource = table;\r\n\t\t\t\t\tcmb.DataBind();\r\n";

                RefreshGrid = "\t\t\t\tdtg.DataSource = dsReader;\r\n\t\t\t\tdtg.DataBind();\r\n";
                bRepetir = true;
                //Revisar la linea 5666
            }
            else
            {
                return "";
            }
        inicioComboGrid:

            //Cargar COMBO
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");

            newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(1);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(1);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t" + nombreFuncionCombo + "(ref cmb, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Cargar COMBO 1.5
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");

            newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, string strParamAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(1);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(1);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t" + nombreFuncionCombo + "(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Cargar COMBO 2
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");

            newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\t" + nombreFuncionCombo + "(ref cmb, arrColumnasWhere, arrValoresWhere, \"\");");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Cargar COMBO 2.5
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");

            newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ".ToString());");
            newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[1][0] + ".ToString());");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t" + nombreFuncionCombo + "(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            if (miEntorno != TipoEntorno.CSharp_WCF)
            {
                //Cargar COMBO 3
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"textField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, " + pNameClaseEnt + ".Fields textField)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\t" + nombreFuncionCombo + "(ref cmb, valueField, textField, \"\");");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //Cargar COMBO 3.25
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"textField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, " + pNameClaseEnt + ".Fields textField, string strParamAdicionales)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(valueField.ToString());");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(textField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(1);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(1);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t" + nombreFuncionCombo + "(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //Cargar COMBO 3.5
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"textField\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, String textField)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\t" + nombreFuncionCombo + "(ref cmb, valueField, textField, \"\");");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //Cargar COMBO 3.75
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"textField\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, String textField, String strParamAdicionales)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(valueField.ToString());");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(textField);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(1);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(1);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t" + nombreFuncionCombo + "(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //Cargar COMBO 4
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"textField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, " + pNameClaseEnt + ".Fields textField, " + pNameClaseEnt + ".Fields searchField, object searchValue)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\t" + nombreFuncionCombo + "(ref cmb, valueField, textField.ToString(), searchField, searchValue);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //Cargar COMBO 4.25
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"textField\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, String textField, " + pNameClaseEnt + ".Fields searchField, object searchValue)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(valueField.ToString());");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(textField);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(searchValue);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t" + nombreFuncionCombo + "(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, \"\");");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //Cargar COMBO 4.5
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"textField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, " + pNameClaseEnt + ".Fields textField, " + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(valueField.ToString());");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(textField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(searchValue);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t" + nombreFuncionCombo + "(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //Cargar COMBO 4.75
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"textField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, String textField, " + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(valueField.ToString());");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(textField);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(searchValue);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t" + nombreFuncionCombo + "(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //Cargar COMBO 5
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"textField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, " + pNameClaseEnt + ".Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(valueField.ToString());");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(textField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t" + nombreFuncionCombo + "(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, \"\");");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //Cargar COMBO 5.25
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"textField\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(valueField.ToString());");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(textField);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t" + nombreFuncionCombo + "(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, \"\");");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //Cargar COMBO 5.5
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"textField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, " + pNameClaseEnt + ".Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(valueField.ToString());");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(textField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t" + nombreFuncionCombo + "(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //Cargar COMBO 5.5
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"valueField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"textField\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(valueField.ToString());");
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(textField);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t" + nombreFuncionCombo + "(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //Cargar COMBO 6
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que carga un " + tipoComboMini + " con los valores de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"cmb\" type=\"" + tipoCombo + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo System.Windows.Forms." + tipoComboMini + " en el que se van a cargar los datos de la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las columnas");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
                newTexto.AppendLine("\t\t\t\tDataTable table = local." + funcionCargarDataTableAnd + ", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");

                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tif (table.Columns.Count > 0)");
                newTexto.AppendLine("\t\t\t\t{");
                newTexto.AppendLine(refreshCombo);

                newTexto.AppendLine("\t\t\t\t}");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");
            }

            // Cargar GRID
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");

            newTexto.AppendLine("\t\tpublic void " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
            }
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t" + nombreFuncionGrid + "(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);");

            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Cargar GRID  1
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t  Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");

            newTexto.AppendLine("\t\tpublic void " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, String strParamAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
            }
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t\t");

            newTexto.AppendLine("\t\t\t\t" + nombreFuncionGrid + "(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Cargar GRID  2
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");

            newTexto.AppendLine("\t\tpublic void " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, ArrayList arrColumnas)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t" + nombreFuncionGrid + "(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Cargar GRID  2.5
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"string\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");

            newTexto.AppendLine("\t\tpublic void " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, ArrayList arrColumnas, String strParamAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t" + nombreFuncionGrid + "(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //Cargar GRID 3
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");

            newTexto.AppendLine("\t\tpublic void " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
            }
            newTexto.AppendLine("\t\t\t");

            newTexto.AppendLine("\t\t\t\t" + nombreFuncionGrid + "(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");

            //Cargar GRID 3.5
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"string\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");

            newTexto.AppendLine("\t\tpublic void " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
            }
            newTexto.AppendLine("\t\t\t");

            newTexto.AppendLine("\t\t\t\t" + nombreFuncionGrid + "(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");

            //Cargar GRID  4
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");

            newTexto.AppendLine("\t\tpublic void " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\t" + nombreFuncionGrid + "(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, \"\");");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");

            //Cargar GRID  4.5
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"string\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");

            newTexto.AppendLine("\t\tpublic void " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");

            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\tDbDataReader dsReader = local." + funcionCargarDataReaderAnd + ", arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine(RefreshGrid);
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");

            if (miEntorno != TipoEntorno.CSharp_WCF)
            {
                //Cargar GRID  5
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que llena un GridView con los registro de una tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"dtg\" type=\"" + tipoGrid + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoGrid + "");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, " + pNameClaseEnt + ".Fields searchField, object searchValue)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
                }
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(searchValue);");

                newTexto.AppendLine("\t\t\t\t" + nombreFuncionGrid + "(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");

                //Cargar GRID  5.5
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que llena un GridView con los registro de una tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"dtg\" type=\"" + tipoGrid + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoGrid + "");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"string\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, " + pNameClaseEnt + ".Fields searchField, object searchValue, String strParamAdicionales)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
                }
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(searchValue);");

                newTexto.AppendLine("\t\t\t\t" + nombreFuncionGrid + "(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");

                //Cargar GRID  6
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que llena un GridView con los registro de una tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"dtg\" type=\"" + tipoGrid + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoGrid + "");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, ArrayList arrColumnas, " + pNameClaseEnt + ".Fields searchField, object searchValue)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(searchValue);");

                newTexto.AppendLine("\t\t\t\t" + nombreFuncionGrid + "(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");

                //Cargar GRID  6.5
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \t Funcion que llena un GridView con los registro de una tabla " + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"dtg\" type=\"" + tipoGrid + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoGrid + "");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"string\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");

                newTexto.AppendLine("\t\tpublic void " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, ArrayList arrColumnas, " + pNameClaseEnt + ".Fields searchField, object searchValue, String strParamAdicionales)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(searchValue);");

                newTexto.AppendLine("\t\t\t\t" + nombreFuncionGrid + "(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
            }

            //Cargar GRID  OR
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");

            newTexto.AppendLine("\t\tpublic void " + nombreFuncionGrid + "Or(ref " + tipoGridMini + " dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\t" + nombreFuncionGrid + "Or(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, \"\");");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");

            //Cargar GRID Or 2
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un " + tipoGridMini + " con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"dtg\" type=\"" + tipoGrid + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Control del tipo " + tipoGrid + " ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");

            newTexto.AppendLine("\t\tpublic void " + nombreFuncionGrid + "Or(ref " + tipoGridMini + " dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\tDbDataReader dsReader = local." + funcionCargarDataReaderOr + ", arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine(RefreshGrid);
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("");

            if (miEntorno == TipoEntorno.CSharp && bRepetir)
            {
                bRepetir = false;
                tipoCombo = "System.Windows.Forms.ComboBox";
                tipoComboMini = "ComboBox";
                tipoGrid = "System.Windows.Forms.DataGridView";
                tipoGridMini = "DataGridView";
                nombreFuncionCombo = "CargarComboBox";
                nombreFuncionGrid = "CargarDataGrid";

                refreshCombo = "\t\t\t\t\tcmb.ValueMember = table.Columns[0].ColumnName;\r\n\t\t\t\t\tcmb.DisplayMember = table.Columns[1].ColumnName;\r\n\t\t\t\t\tcmb.DataSource = table;\r\n";

                RefreshGrid = "\t\t\t\tdtg.DataSource = dsReader;\r\n";
                goto inicioComboGrid;
            }

            return newTexto.ToString();
        }

        #endregion Methods
    }
}