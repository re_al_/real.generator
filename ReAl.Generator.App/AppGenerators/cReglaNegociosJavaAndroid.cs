﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using ReAlFind_Control;
using System;
using System.Data;

namespace ReAl.Generator.App.AppGenerators
{
    internal class cReglaNegociosJavaAndroid
    {
        public string CrearModeloJavaAndroid(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            string pNameTabla = pNameClase.Replace("_", "");
            pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() +
                         pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
            //pNameTabla = pNameTabla.Substring(0, 1).ToUpper & pNameTabla.Substring(1).ToLower

            pNameClase = formulario.txtPrefijoModelo.Text + pNameTabla;
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameTabla;

            string strLlaves = "";
            string strLlavesQueryObj = "";
            string strLlavesQuery = "";
            string strLlavesArr = "";
            bool bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString();

                    strLlavesArr = strLlavesArr + "arrValWhere.add(String.valueOf(" + tipoCol + nameCol + "));";

                    if (bEsPrimerElemento)
                    {
                        strLlaves = strLlaves + tipoCol + " " + tipoCol + nameCol;
                        strLlavesQueryObj = strLlavesQueryObj + " " + pNameClaseEnt + ".Fields." + nameCol + ".toString() + \" = \" + obj.get" + nameCol + "()";
                        strLlavesQuery = strLlavesQuery + " " + pNameClaseEnt + ".Fields." + nameCol + ".toString() + \" = \" + " + tipoCol + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlaves = strLlaves + ", " + tipoCol + " " + tipoCol + nameCol;
                        strLlavesQueryObj = strLlavesQueryObj + "\"  AND \" + " + pNameClaseEnt + ".Fields." + nameCol + ".toString() + \" = \" + obj.get" + nameCol + "()";
                        strLlavesQuery = strLlavesQuery + "\"  AND \" + " + pNameClaseEnt + ".Fields." + nameCol + ".toString() + \" = \" + " + tipoCol + nameCol;
                    }
                }
            }

            //Obtenemos las columnas de APIs
            string strApiEstado = "--";
            string strApiTransaccion = "--";
            string strUsuCre = "--";
            string strFecCre = "--";
            string strUsuMod = "--";
            string strFecMod = "--";

            for (int iCol = 1; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();

                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                {
                    strApiEstado = nameCol;
                }
                else if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                {
                    strApiTransaccion = nameCol;
                }
                else if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                {
                    strUsuCre = nameCol;
                }
                else if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                {
                    strUsuMod = nameCol;
                }
                else if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                {
                    strFecCre = nameCol;
                }
                else if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                {
                    strFecMod = nameCol;
                }
            }

            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase).Replace("#region", "").Replace("#endregion", "");
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que implementa los metodos y operaciones sobre la Tabla " + pNameTabla);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            //newTexto.AppendLine(CabeceraTmp)

            newTexto.AppendLine("package " + formulario.txtInfProyectoClass.Text + ";");
            newTexto.AppendLine("");

            newTexto.AppendLine("import java.io.BufferedReader;");
            newTexto.AppendLine("import java.io.BufferedWriter;");
            newTexto.AppendLine("import java.io.DataInputStream;");
            newTexto.AppendLine("import java.io.File;");
            newTexto.AppendLine("import java.io.FileInputStream;");
            newTexto.AppendLine("import java.io.FileNotFoundException;");
            newTexto.AppendLine("import java.io.FileReader;");
            newTexto.AppendLine("import java.io.FileWriter;");
            newTexto.AppendLine("import java.io.IOException;");
            newTexto.AppendLine("import java.io.InputStreamReader;");
            newTexto.AppendLine("import java.util.ArrayList;");
            newTexto.AppendLine("import java.util.Hashtable;");
            newTexto.AppendLine("import java.util.StringTokenizer;");
            newTexto.AppendLine("import java.lang.reflect.Type;");
            newTexto.AppendLine("import android.content.ContentValues;");
            newTexto.AppendLine("import android.content.Context;");
            newTexto.AppendLine("import android.database.Cursor;");
            newTexto.AppendLine("import android.database.SQLException;");
            newTexto.AppendLine("import android.database.sqlite.SQLiteDatabase;");
            newTexto.AppendLine("import android.os.Environment;");
            newTexto.AppendLine("import android.util.Log;");
            newTexto.AppendLine("import com.google.gson.Gson;");
            newTexto.AppendLine("import com.google.gson.reflect.TypeToken;");
            newTexto.AppendLine("");
            newTexto.AppendLine("import " + formulario.txtInfProyectoClass.Text + ".cCsvWriter;");
            newTexto.AppendLine("");
            newTexto.AppendLine("/*! ");
            newTexto.AppendLine(" *  Clase DAL que contienen los métodos para interactuar con la Base de Datos y la Tabla " + pNameTabla);
            newTexto.AppendLine(" *  @author <A href=\"mailto:wmayta@gmail.com\">William Mayta Chura</A>");
            newTexto.AppendLine(" *  @author <A href=\"mailto:7.re.al.7@gmail.com\">Reynaldo Alonzo Vera Arias</A>");
            newTexto.AppendLine(" *  @version 1.0");
            newTexto.AppendLine(" */");

            newTexto.AppendLine("public class " + pNameClase);
            newTexto.AppendLine("{");
            newTexto.AppendLine("\tprivate final Context miContexto;");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic " + pNameClase + "(Context c)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tmiContexto = c;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic ArrayList<" + pNameClaseEnt + "> getFromJson(File miFileJson)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tStringBuilder textBuild = new StringBuilder();");
            newTexto.AppendLine("\t\ttry");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tBufferedReader br = new BufferedReader(new FileReader(miFileJson));");
            newTexto.AppendLine("\t\t\tString line;  ");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\twhile ((line = br.readLine()) != null)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\ttextBuild.append(line);");
            newTexto.AppendLine("\t\t\t\ttextBuild.append('\\n');");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tcatch (IOException e)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\te.printStackTrace();");
            newTexto.AppendLine("\t\t\ttextBuild.append(e.getMessage());");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn getFromJson(textBuild.toString());");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic ArrayList<" + pNameClaseEnt + "> getFromJson(String strJson)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tType collectionType = new TypeToken<ArrayList<" + pNameClaseEnt + ">>(){}.getType();");
            newTexto.AppendLine("\t\tGson parser = new Gson();");
            newTexto.AppendLine("\t\treturn (ArrayList<" + pNameClaseEnt + ">)parser.fromJson(strJson, collectionType);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic String setToJson(ArrayList<" + pNameClaseEnt + "> miLista)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tGson parser = new Gson();");
            newTexto.AppendLine("\t\treturn parser.toJson(miLista);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic void setToJsonFile(ArrayList<" + pNameClaseEnt + "> miLista, File miJsonFile)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tif (!miJsonFile.exists())");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tmiJsonFile.createNewFile();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (IOException e)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\te.printStackTrace();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tGson parser = new Gson();");
            newTexto.AppendLine("\t\tString strJson = parser.toJson(miLista);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\ttry");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tBufferedWriter buf = new BufferedWriter(new FileWriter(miJsonFile, true)); ");
            newTexto.AppendLine("\t\t\tbuf.append(strJson);");
            newTexto.AppendLine("\t\t\tbuf.newLine();");
            newTexto.AppendLine("\t\t\tbuf.close();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tcatch (IOException e)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\te.printStackTrace();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic int insertDataFromCsv(String strPath, String fileName, Boolean bTieneCabecera, Boolean bActualizarRegistrosExistentes) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tif (!bActualizarRegistrosExistentes)");
            newTexto.AppendLine("\t\t\tdeleteAll();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tFile externFile = new File(strPath, fileName);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tSQLiteDatabase nBD = null;");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + "  nCanal = new " + formulario.txtClaseConn.Text + " (miContexto);");
            newTexto.AppendLine("\t\tnBD = nCanal.getWritableDatabase();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tint cant = 0;");
            newTexto.AppendLine("\t\tContentValues paquete = new ContentValues();");
            newTexto.AppendLine("\t\tFileInputStream canalPro = new FileInputStream(externFile);");
            newTexto.AppendLine("\t\tDataInputStream dPro = new DataInputStream(canalPro);");
            newTexto.AppendLine("\t\tBufferedReader reader = new BufferedReader(new InputStreamReader(dPro));");
            newTexto.AppendLine("\t\tString line;");
            newTexto.AppendLine("\t\tString tokens[];");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\twhile((line = reader.readLine()) != null) ");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (bTieneCabecera)");
            newTexto.AppendLine("\t\t\t\tbTieneCabecera = false;");
            newTexto.AppendLine("\t\t\telse");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tpaquete = new ContentValues();");
            newTexto.AppendLine("\t\t\t\ttokens = line.split(\"\\\\|\",-1);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t//Buscamos el objeto");
            newTexto.AppendLine("\t\t\t\tBoolean bProcede = true;");
            newTexto.AppendLine("\t\t\t\tif (bActualizarRegistrosExistentes)");
            newTexto.AppendLine("\t\t\t\t\tbProcede = !(funcionesCount(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ", tokens[0]) > 0);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tif (bProcede)");
            newTexto.AppendLine("\t\t\t\t{");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                string tipoCol = dtColumns.Rows[iCol][1].ToString();
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    newTexto.AppendLine("\t\t\t\t\tpaquete.put(" + pNameClaseEnt + ".Fields." + nameCol + ".toString(), tokens[" + (iCol) + "].replace(\"\\\"\", \"\"));");
                }
                else
                {
                    newTexto.AppendLine("\t\t\t\t\tpaquete.put(" + pNameClaseEnt + ".Fields." + nameCol + ".toString(), (tokens[" + (iCol) + "] == null ? null : tokens[" + (iCol) + "].replace(\"\\\"\", \"\")));");
                }
            }
            newTexto.AppendLine("\t\t\t\t\tnBD.insert(cParametros.T_" + pNameTabla.ToUpper() + ",  null, paquete);");
            newTexto.AppendLine("\t\t\t\t\tcant++;");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tnBD.close();");
            newTexto.AppendLine("\t\tnCanal.close();");
            newTexto.AppendLine("\t\treader.close();");
            newTexto.AppendLine("\t\treturn cant;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic int insertBulkDataFromCsv(String strPath, String fileName, Boolean bTieneCabecera, Boolean bActualizarRegistrosExistentes) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tif (!bActualizarRegistrosExistentes)");
            newTexto.AppendLine("\t\t\tdeleteAll();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tFile externFile = new File(strPath, fileName);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tSQLiteDatabase nBD = null;");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + "  nCanal = new " + formulario.txtClaseConn.Text + " (miContexto);");
            newTexto.AppendLine("\t\tnBD = nCanal.getWritableDatabase();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tint cant = 0;");
            newTexto.AppendLine("\t\tContentValues paquete = new ContentValues();");
            newTexto.AppendLine("\t\tFileInputStream canalPro = new FileInputStream(externFile);");
            newTexto.AppendLine("\t\tDataInputStream dPro = new DataInputStream(canalPro);");
            newTexto.AppendLine("\t\tBufferedReader reader = new BufferedReader(new InputStreamReader(dPro));");
            newTexto.AppendLine("\t\tString line;");
            newTexto.AppendLine("\t\tString tokens[];");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tnBD.beginTransactionNonExclusive();");
            newTexto.AppendLine("\t\ttry");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tDatabaseUtils.InsertHelper ih = new DatabaseUtils.InsertHelper(nBD, cParametros.T_" + pNameTabla.ToUpper() + ");");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                string tipoCol = dtColumns.Rows[iCol][1].ToString();
                newTexto.AppendLine("\t\t\tfinal int column_" + nameCol + " = ih.getColumnIndex(" + pNameClaseEnt + ".Fields." + nameCol + ".toString());");
            }
            newTexto.AppendLine("\t\t\twhile((line = reader.readLine()) != null) ");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tif (bTieneCabecera)");
            newTexto.AppendLine("\t\t\t\t\tbTieneCabecera = false;");
            newTexto.AppendLine("\t\t\t\telse");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\tih.prepareForInsert();");
            newTexto.AppendLine("\t\t\t\t\ttokens = line.split(\"\\\\|\",-1);");
            newTexto.AppendLine("\t\t\t\t\tString cod = tokens.nextToken();");
            newTexto.AppendLine("\t\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t\t//Buscamos el objeto");
            newTexto.AppendLine("\t\t\t\t\tBoolean bProcede = true;");
            newTexto.AppendLine("\t\t\t\t\tif (bActualizarRegistrosExistentes)");
            newTexto.AppendLine("\t\t\t\t\t\tbProcede = !(funcionesCount(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ", cod) > 0);");
            newTexto.AppendLine("\t\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t\tif (bProcede) {");
            newTexto.AppendLine("\t\t\t\t\t\tih.bind(column_" + dtColumns.Rows[0][0] + ", cod);");
            for (int iCol = 1; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                string tipoCol = dtColumns.Rows[iCol][1].ToString();

                newTexto.AppendLine("\t\t\t\t\t\tif (tokens.hasMoreTokens()) {");
                newTexto.AppendLine("\t\t\t\t\t\t\tcod = tokens.nextToken();");
                newTexto.AppendLine("\t\t\t\t\t\t\tih.bind(column_" + nameCol + ", cod);");
                newTexto.AppendLine("\t\t\t\t\t\t}");
            }
            newTexto.AppendLine("\t\t\t\t\t\tih.execute();");
            newTexto.AppendLine("\t\t\t\t\t\tcant++;");
            newTexto.AppendLine("\t\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tnBD.setTransactionSuccessful();");
            newTexto.AppendLine("\t\t} finally {");
            newTexto.AppendLine("\t\t\tnBD.endTransaction();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tnBD.close();");
            newTexto.AppendLine("\t\tnCanal.close();");
            newTexto.AppendLine("\t\treader.close();");
            newTexto.AppendLine("\t\treturn cant;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic ArrayList<" + pNameClaseEnt + "> getDataFromCsv(String strPath, String fileName, Boolean bTieneCabecera) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<" + pNameClaseEnt + "> miLis = new ArrayList<" + pNameClaseEnt + ">();");
            newTexto.AppendLine("\t\tFile externFile = new File(strPath, fileName);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tSQLiteDatabase nBD;");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + "  nCanal = new " + formulario.txtClaseConn.Text + " (miContexto);");
            newTexto.AppendLine("\t\tnBD = nCanal.getWritableDatabase();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tint cant = 0;");
            newTexto.AppendLine("\t\tContentValues paquete = new ContentValues();");
            newTexto.AppendLine("\t\tFileInputStream canalPro = new FileInputStream(externFile);");
            newTexto.AppendLine("\t\tDataInputStream dPro = new DataInputStream(canalPro);");
            newTexto.AppendLine("\t\tBufferedReader reader = new BufferedReader(new InputStreamReader(dPro));");
            newTexto.AppendLine("\t\tString line;");
            newTexto.AppendLine("\t\tString tokens[];");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\twhile((line = reader.readLine()) != null) ");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (bTieneCabecera)");
            newTexto.AppendLine("\t\t\t\tbTieneCabecera = false;");
            newTexto.AppendLine("\t\t\telse");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tpaquete = new ContentValues();");
            newTexto.AppendLine("\t\t\t\ttokens = line.split(\"\\\\|\",-1);");
            newTexto.AppendLine("\t\t\t\t" + pNameClaseEnt + " obj = getObjFromPaquete(tokens);;");
            newTexto.AppendLine("\t\t\t\tmiLis.add(obj);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tnBD.close();");
            newTexto.AppendLine("\t\treader.close();");
            newTexto.AppendLine("\t\treturn miLis;");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic Boolean exportToCsv(String strPath, String fileName) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tFile exportDir = new File(strPath, \"\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tif (!exportDir.exists()) ");
            newTexto.AppendLine("\t\t\texportDir.mkdirs();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tSQLiteDatabase nBD = null;");
            newTexto.AppendLine("\t\tCursor curCSV = null;");
            newTexto.AppendLine("\t\tBoolean bExito = false;");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + " nCanal = new " + formulario.txtClaseConn.Text + " (miContexto);");
            newTexto.AppendLine("\t\tnBD = nCanal.getWritableDatabase();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tFile file = new File(exportDir, fileName);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\ttry");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tfile.createNewFile();");
            newTexto.AppendLine("\t\t\tcCsvWriter csvWrite = new cCsvWriter(new FileWriter(file));");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tArrayList<" + pNameClaseEnt + "> arrLista = new ArrayList<" + pNameClaseEnt + ">(); ");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tArrayList<String> arrColumnas = new ArrayList<String>();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                string tipoCol = dtColumns.Rows[iCol][1].ToString();

                if ((tipoCol.ToUpper().StartsWith("DATE")))
                {
                    newTexto.AppendLine("\t\t\tarrColumnas.add(\"datetime(\" + " + pNameClaseEnt + ".Fields." + nameCol + ".toString() + \", 'unixepoch', 'localtime') as " + nameCol + "\");");
                }
                else
                {
                    newTexto.AppendLine("\t\t\tarrColumnas.add(" + pNameClaseEnt + ".Fields." + nameCol + ".toString());");
                }
            }
            newTexto.AppendLine("\t\t\tString[] strColumnas = arrColumnas.toArray(new String[arrColumnas.size()]);");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\t//Obtenemos el Cursor");
            newTexto.AppendLine("\t\t\tCursor curCSV = nBD.query(cParametros.T_" + pNameTabla.ToUpper() + ", strColumnas, null, null, null, null, null);");
            newTexto.AppendLine("\t\t\tcsvWrite.writeNext(curCSV.getColumnNames());");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tif (curCSV.getCount() != 0) ");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tif (curCSV.moveToFirst())");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\tdo");
            newTexto.AppendLine("\t\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\t\tcsvWrite.writeNext(getArrayFromCursor(curCSV));");
            newTexto.AppendLine("\t\t\t\t\t} while (curCSV.moveToNext());");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tcsvWrite.close();");
            newTexto.AppendLine("\t\t\tbExito = true;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tcatch(SQLException sqlEx) ");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tLog.e(\"" + pNameClase + "\", sqlEx.getMessage(), sqlEx);");
            newTexto.AppendLine("\t\t\tbExito = false;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tcatch (IOException e) ");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tLog.e(\"" + pNameClase + "\", e.getMessage(), e);");
            newTexto.AppendLine("\t\t\tbExito = false;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tfinally ");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tif (curCSV != null)");
            newTexto.AppendLine("\t\t\t\t\tcurCSV.close();");
            newTexto.AppendLine("\t\t\t\tnBD.close();");
            newTexto.AppendLine("\t\t\t\tnCanal.close();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch(Exception ex){}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\treturn bExito;");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic Boolean exportToCsv(String strPath, String fileName, ArrayList<String> arrColWhere, ArrayList<String> arrValWhere, String strParamAdicionales) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tFile exportDir = new File(strPath, \"\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tif (!exportDir.exists()) ");
            newTexto.AppendLine("\t\t\texportDir.mkdirs();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tSQLiteDatabase nBD;");
            newTexto.AppendLine("\t\tCursor curCSV = null;");
            newTexto.AppendLine("\t\tBoolean bExito = false;");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + "  nCanal = new " + formulario.txtClaseConn.Text + " (miContexto);");
            newTexto.AppendLine("\t\tnBD = nCanal.getWritableDatabase();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tFile file = new File(exportDir, fileName);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\ttry");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tfile.createNewFile();");
            newTexto.AppendLine("\t\t\tcCsvWriter csvWrite = new cCsvWriter(new FileWriter(file));");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tArrayList<" + pNameClaseEnt + "> arrLista = new ArrayList<" + pNameClaseEnt + ">(); ");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tArrayList<String> arrColumnas = new ArrayList<String>();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                string tipoCol = dtColumns.Rows[iCol][1].ToString();

                if ((tipoCol.ToUpper().StartsWith("DATE")))
                {
                    newTexto.AppendLine("\t\t\tarrColumnas.add(\"datetime(\" + " + pNameClaseEnt + ".Fields." + nameCol + ".toString() + \", 'unixepoch', 'localtime') as " + nameCol + "\");");
                }
                else
                {
                    newTexto.AppendLine("\t\t\tarrColumnas.add(" + pNameClaseEnt + ".Fields." + nameCol + ".toString());");
                }
            }
            newTexto.AppendLine("\t\t\tString[] strColumnas = arrColumnas.toArray(new String[arrColumnas.size()]);");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tBoolean bPrimerElemento = true;");
            newTexto.AppendLine("\t\t\tString strQuery = \"\";");
            newTexto.AppendLine("\t\t\tfor (int i = 0; i < arrColumnas.size(); i++)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tif (bPrimerElemento)");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\tstrQuery = strQuery + \" \" + arrColWhere.get(i) + \" = '\" + arrValWhere.get(i) + \"'\";");
            newTexto.AppendLine("\t\t\t\t\tbPrimerElemento = false;");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\telse");
            newTexto.AppendLine("\t\t\t\t\tstrQuery = strQuery + \" AND \" + arrColWhere.get(i) + \" = '\" + arrValWhere.get(i) + \"'\";");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tstrQuery = strQuery + \" \" + strParamAdicionales;");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\t//Obtenemos el Cursor");
            newTexto.AppendLine("\t\t\tCursor curCSV = nBD.query(cParametros.T_" + pNameTabla.ToUpper() + ", strColumnas, strQuery, null, null, null, null);");
            newTexto.AppendLine("\t\t\tcsvWrite.writeNext(curCSV.getColumnNames());");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tif (curCSV.getCount() != 0) ");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tif (curCSV.moveToFirst())");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\tdo");
            newTexto.AppendLine("\t\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\t\tcsvWrite.writeNext(getArrayFromCursor(curCSV));");
            newTexto.AppendLine("\t\t\t\t\t} while (curCSV.moveToNext());");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tcsvWrite.close();");
            newTexto.AppendLine("\t\t\tbExito = true;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tcatch(SQLException sqlEx) ");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tLog.e(\"" + pNameClase + "\", sqlEx.getMessage(), sqlEx);");
            newTexto.AppendLine("\t\t\tbExito = false;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tcatch (IOException e) ");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tLog.e(\"" + pNameClase + "\", e.getMessage(), e);");
            newTexto.AppendLine("\t\t\tbExito = false;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tfinally ");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tif (curCSV != null)");
            newTexto.AppendLine("\t\t\t\t\tcurCSV.close();");
            newTexto.AppendLine("\t\t\t\tnBD.close();");
            newTexto.AppendLine("\t\t\t\tnCanal.close();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch(Exception ex){}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\treturn bExito;");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic Boolean exportToCsv(String strPath, String fileName, ArrayList<String> arrColWhere, ArrayList<String> arrValWhere) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\treturn exportToCsv(strPath, fileName, arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic long insert(" + pNameClaseEnt + " obj) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tlong id = 0;");
            newTexto.AppendLine("\t\tSQLiteDatabase nBD = null;");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + "  nCanal = new " + formulario.txtClaseConn.Text + " (miContexto);");
            newTexto.AppendLine("\t\ttry");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tnBD = nCanal.getWritableDatabase();");
            newTexto.AppendLine("\t\t\tnBD.beginTransactionNonExclusive();");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tContentValues miPaquete = new ContentValues();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                string tipoCol = dtColumns.Rows[iCol][1].ToString();
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    newTexto.AppendLine("\t\t\t//miPaquete.put(" + pNameClaseEnt + ".Fields." + nameCol + ".toString(), obj.get" + nameCol + "());");
                }
                else
                {
                    if (nameCol == strApiEstado)
                    {
                    }
                    else if (nameCol == strApiTransaccion)
                    {
                    }
                    else if (nameCol == strUsuCre)
                    {
                        newTexto.AppendLine("\t\t\tmiPaquete.put(" + pNameClaseEnt + ".Fields." + nameCol + ".toString(), obj.get" + nameCol + "());");
                    }
                    else if (nameCol == strFecCre)
                    {
                    }
                    else if (nameCol == strUsuMod)
                    {
                    }
                    else if (nameCol == strFecMod)
                    {
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t\tmiPaquete.put(" + pNameClaseEnt + ".Fields." + nameCol + ".toString(), obj.get" + nameCol + "());");
                    }
                }
            }

            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tid = nBD.insert(cParametros.T_" + pNameTabla.ToUpper() + ", null, miPaquete);");
            newTexto.AppendLine("\t\t\tnBD.setTransactionSuccessful();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tfinally");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tnBD.endTransaction();");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tnBD.close();");
            newTexto.AppendLine("\t\t\t\tnCanal.close();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch(Exception exp){}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn id;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic void update(" + pNameClaseEnt + " obj) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tSQLiteDatabase nBD = null;");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + "  nCanal = new " + formulario.txtClaseConn.Text + " (miContexto);");
            newTexto.AppendLine("\t\ttry");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tnBD = nCanal.getWritableDatabase();");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tContentValues miPaquete = new ContentValues();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                string tipoCol = dtColumns.Rows[iCol][1].ToString();

                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "NO"))
                {
                    if (nameCol == strApiEstado)
                    {
                        newTexto.AppendLine("\t\t\tmiPaquete.put(" + pNameClaseEnt + ".Fields." + nameCol + ".toString(), obj.get" + nameCol + "());");
                    }
                    else if (nameCol == strApiTransaccion)
                    {
                        newTexto.AppendLine("\t\t\tmiPaquete.put(" + pNameClaseEnt + ".Fields." + nameCol + ".toString(), obj.get" + nameCol + "());");
                    }
                    else if (nameCol == strUsuCre)
                    {
                    }
                    else if (nameCol == strFecCre)
                    {
                    }
                    else if (nameCol == strUsuMod)
                    {
                        newTexto.AppendLine("\t\t\tmiPaquete.put(" + pNameClaseEnt + ".Fields." + nameCol + ".toString(), obj.get" + nameCol + "());");
                    }
                    else if (nameCol == strFecMod)
                    {
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t\tmiPaquete.put(" + pNameClaseEnt + ".Fields." + nameCol + ".toString(), obj.get" + nameCol + "());");
                    }
                }
            }

            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tnBD.update(cParametros.T_" + pNameTabla.ToUpper() + ", miPaquete,  " + strLlavesQueryObj + " , null);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tfinally");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tnBD.close();");
            newTexto.AppendLine("\t\t\t\tnCanal.close();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch(Exception exp){}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic long delete(" + pNameClaseEnt + " obj) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tSQLiteDatabase nBD;");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + "  nCanal = new " + formulario.txtClaseConn.Text + " (miContexto);");
            newTexto.AppendLine("\t\tnBD = nCanal.getWritableDatabase();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tlong id = nBD.delete(cParametros.T_" + pNameTabla.ToUpper() + ", " + strLlavesQueryObj + " , null);");
            newTexto.AppendLine("\t\tnBD.close();");
            newTexto.AppendLine("\t\tnCanal.close();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn id;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic long deleteAll() throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tSQLiteDatabase nBD;");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + "  nCanal = new " + formulario.txtClaseConn.Text + " (miContexto);");
            newTexto.AppendLine("\t\tnBD = nCanal.getWritableDatabase();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tlong id = nBD.delete(cParametros.T_" + pNameTabla.ToUpper() + ", null , null);");
            newTexto.AppendLine("\t\t//long idIdentity = nBD.delete(\"sqlite_sequence\", \"name = '\" + cParametros.T_" + pNameTabla.ToUpper() + "\"'\", null);");
            newTexto.AppendLine("\t\tnBD.close();");
            newTexto.AppendLine("\t\tnCanal.close();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn id;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic " + pNameClaseEnt + " obtenerObjeto(" + strLlaves + ") throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "NO"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    newTexto.AppendLine("\t\tarrColWhere.add(" + pNameClaseEnt + ".Fields." + nameCol + ".toString());");
                }
            }
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\t" + strLlavesArr);
            newTexto.AppendLine("\t\treturn obtenerObjeto(arrColWhere, arrValWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic " + pNameClaseEnt + " obtenerObjeto(" + pNameClaseEnt + ".Fields miField, String miValue) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(miField.toString());");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(miValue);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn obtenerObjeto(arrColWhere, arrValWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic " + pNameClaseEnt + " obtenerObjeto(ArrayList<String> arrColWhere, ArrayList<String> arrValWhere) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\treturn obtenerObjeto(arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic " + pNameClaseEnt + " obtenerObjeto(ArrayList<String> arrColWhere, ArrayList<String> arrValWhere, String strParamAdicionales) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<" + pNameClaseEnt + "> miLis = obtenerLista(arrColWhere, arrValWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tif (miLis.size() == 0)");
            newTexto.AppendLine("\t\t\treturn null;");
            newTexto.AppendLine("\t\telse if (miLis.size() == 0)");
            newTexto.AppendLine("\t\t\treturn miLis.get(0);");
            newTexto.AppendLine("\t\telse");
            newTexto.AppendLine("\t\t\tthrow new Exception(\"Se ha devuelto mas de un Objeto\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic ArrayList<" + pNameClaseEnt + "> obtenerLista() throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(\"'1'\");");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(\"1\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn obtenerLista(arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic ArrayList<" + pNameClaseEnt + "> obtenerLista(" + pNameClaseEnt + ".Fields miField, String miValue) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(miField.toString());");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(miValue);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn obtenerLista(arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic ArrayList<" + pNameClaseEnt + "> obtenerLista(ArrayList<String> arrColWhere, ArrayList<String> arrValWhere) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\treturn obtenerLista(arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic ArrayList<" + pNameClaseEnt + "> obtenerLista(ArrayList<String> arrColWhere, ArrayList<String> arrValWhere, String strParamAdicionales) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<" + pNameClaseEnt + "> arrLista = new ArrayList<" + pNameClaseEnt + ">(); ");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tArrayList<String> arrColumnas = new ArrayList<String>();");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.add(" + pNameClaseEnt + ".Fields." + nameCol + ".toString());");
            }
            newTexto.AppendLine("\t\tString[] strColumnas = arrColumnas.toArray(new String[arrColumnas.size()]);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tBoolean bPrimerElemento = true;");
            newTexto.AppendLine("\t\tString strQuery = \"\";");
            newTexto.AppendLine("\t\tfor (int i = 0; i < arrColWhere.size(); i++)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (bPrimerElemento)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tstrQuery = strQuery + \" \" + arrColWhere.get(i) + \" = '\" + arrValWhere.get(i) + \"'\";");
            newTexto.AppendLine("\t\t\t\tbPrimerElemento = false;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\telse");
            newTexto.AppendLine("\t\t\t\tstrQuery = strQuery + \" AND \" + arrColWhere.get(i) + \" = '\" + arrValWhere.get(i) + \"'\";");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tstrQuery = strQuery + \" \" + strParamAdicionales;");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tSQLiteDatabase nBD;");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + "  nCanal = new " + formulario.txtClaseConn.Text + " (miContexto);");
            newTexto.AppendLine("\t\tnBD = nCanal.getWritableDatabase();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t//Obtenemos el Cursor");
            newTexto.AppendLine("\t\tCursor cursor = nBD.query(cParametros.T_" + pNameTabla.ToUpper() + ", strColumnas, strQuery, null, null, null, null);");
            newTexto.AppendLine("\t\tif (cursor.getCount() != 0) ");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (cursor.moveToFirst())");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tdo");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\tarrLista.add(getObjFromCursor(cursor));");
            newTexto.AppendLine("\t\t\t\t} while (cursor.moveToNext());");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tnBD.close();");
            newTexto.AppendLine("\t\tnCanal.close();");
            newTexto.AppendLine("\t\treturn arrLista;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Hashtable<Integer, " + pNameClaseEnt + "> obtenerHashTable() throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(\"'1'\");");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(\"1\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn obtenerHashTable(arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Hashtable<Integer, " + pNameClaseEnt + "> obtenerHashTable(" + pNameClaseEnt + ".Fields miField, String miValue) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(miField.toString());");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(miValue);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn obtenerHashTable(arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Hashtable<Integer, " + pNameClaseEnt + "> obtenerHashTable(ArrayList<String> arrColWhere, ArrayList<String> arrValWhere) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\treturn obtenerHashTable(arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic Hashtable<Integer, " + pNameClaseEnt + "> obtenerHashTable(ArrayList<String> arrColWhere, ArrayList<String> arrValWhere, String strParamAdicionales) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tHashtable<Integer, " + pNameClaseEnt + "> dicLista = new Hashtable<Integer, " + pNameClaseEnt + ">(); ");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tArrayList<String> arrColumnas = new ArrayList<String>();");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.add(" + pNameClaseEnt + ".Fields." + nameCol + ".toString());");
            }
            newTexto.AppendLine("\t\tString[] strColumnas = arrColumnas.toArray(new String[arrColumnas.size()]);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tBoolean bPrimerElemento = true;");
            newTexto.AppendLine("\t\tString strQuery = \"\";");
            newTexto.AppendLine("\t\tfor (int i = 0; i < arrColWhere.size(); i++)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (bPrimerElemento)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tstrQuery = strQuery + \" \" + arrColWhere.get(i) + \" = '\" + arrValWhere.get(i) + \"'\";");
            newTexto.AppendLine("\t\t\t\tbPrimerElemento = false;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\telse");
            newTexto.AppendLine("\t\t\t\tstrQuery = strQuery + \" AND \" + arrColWhere.get(i) + \" = '\" + arrValWhere.get(i) + \"'\";");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tstrQuery = strQuery + \" \" + strParamAdicionales;");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tSQLiteDatabase nBD;");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + "  nCanal = new " + formulario.txtClaseConn.Text + " (miContexto);");
            newTexto.AppendLine("\t\tnBD = nCanal.getWritableDatabase();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t//Obtenemos el Cursor");
            newTexto.AppendLine("\t\tCursor cursor = nBD.query(cParametros.T_" + pNameTabla.ToUpper() + ", strColumnas, strQuery, null, null, null, null);");
            newTexto.AppendLine("\t\tif (cursor.getCount() != 0) ");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (cursor.moveToFirst())");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tdo");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\tdicLista.put(cursor.getInt(cursor.getColumnIndex(" + pNameClaseEnt + ".Fields." + dtColumns.Rows[0][0] + ".toString())), getObjFromCursor(cursor));");
            newTexto.AppendLine("\t\t\t\t} while (cursor.moveToNext());");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tnBD.close();");
            newTexto.AppendLine("\t\tnCanal.close();");
            newTexto.AppendLine("\t\treturn dicLista;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            //FUNCIONES MAX
            newTexto.AppendLine("\tpublic int funcionesMax(" + pNameClaseEnt + ".Fields miMaxField) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(\"'1'\");");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(\"1\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn funcionesMax(miMaxField, arrColWhere, arrValWhere);");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\tpublic int funcionesMax(" + pNameClaseEnt + ".Fields miMaxField, " + pNameClaseEnt + ".Fields miField, String miValue) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(miField.toString());");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(miValue);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn funcionesMax(miMaxField, arrColWhere, arrValWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic int funcionesMax(" + pNameClaseEnt + ".Fields miMaxField, ArrayList<String> arrColWhere, ArrayList<String> arrValWhere) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\treturn funcionesMax(miMaxField, arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic int funcionesMax(" + pNameClaseEnt + ".Fields miMaxField, ArrayList<String> arrColWhere, ArrayList<String> arrValWhere, String strParamAdicionales) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tint idFieldMax = -1; ");
            newTexto.AppendLine("\t\tSQLiteDatabase nBD;");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + "  nCanal = new " + formulario.txtClaseConn.Text + " (miContexto);");
            newTexto.AppendLine("\t\ttry");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tArrayList<String> arrColumnas = new ArrayList<String>();");
            newTexto.AppendLine("\t\t\tarrColumnas.add(\"MAX(\" + miMaxField.toString() + \")\");");
            newTexto.AppendLine("\t\t\tString[] strColumnas = arrColumnas.toArray(new String[arrColumnas.size()]);");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tBoolean bPrimerElemento = true;");
            newTexto.AppendLine("\t\t\tString strQuery = \"\";");
            newTexto.AppendLine("\t\t\tfor (int i = 0; i < arrColWhere.size(); i++)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tif (bPrimerElemento)");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\tstrQuery = strQuery + \" \" + arrColWhere.get(i) + \" = '\" + arrValWhere.get(i) + \"'\";");
            newTexto.AppendLine("\t\t\t\t\tbPrimerElemento = false;");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\telse");
            newTexto.AppendLine("\t\t\t\t\tstrQuery = strQuery + \" AND \" + arrColWhere.get(i) + \" = '\" + arrValWhere.get(i) + \"'\";");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tstrQuery = strQuery + \" \" + strParamAdicionales;");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tnBD = nCanal.getWritableDatabase();");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\t//Obtenemos el Cursor");
            newTexto.AppendLine("\t\t\tCursor cursor = nBD.query(cParametros.T_" + pNameTabla.ToUpper() + ", strColumnas, strQuery, null, null, null, null);");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tif (cursor.getCount() == 0) ");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tif (cursor != null)");
            newTexto.AppendLine("\t\t\t\t\tcursor.close();");
            newTexto.AppendLine("\t\t\t\tnBD.close();");
            newTexto.AppendLine("\t\t\t\tnCanal.close();");
            newTexto.AppendLine("\t\t\t\treturn idFieldMax;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\telse");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tif (cursor.getCount() > 1)");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\tif (cursor != null)");
            newTexto.AppendLine("\t\t\t\t\t\tcursor.close();");
            newTexto.AppendLine("\t\t\t\t\tnBD.close();");
            newTexto.AppendLine("\t\t\t\t\tnCanal.close();");
            newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"Se ha devuelto mas de un Resultado\");");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\telse");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\tif (cursor.moveToFirst())");
            newTexto.AppendLine("\t\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\t\tdo");
            newTexto.AppendLine("\t\t\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\t\t\tidFieldMax = cursor.getInt(0);");
            newTexto.AppendLine("\t\t\t\t\t\t} while (cursor.moveToNext());");
            newTexto.AppendLine("\t\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tfinally");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tif (cursor != null)");
            newTexto.AppendLine("\t\t\t\t\tcursor.close();");
            newTexto.AppendLine("\t\t\t\tnBD.close();");
            newTexto.AppendLine("\t\t\t\tnCanal.close();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch(Exception exp){}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\treturn idFieldMax;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            //FUNCIONES COUNT
            newTexto.AppendLine("\tpublic int funcionesCount(" + pNameClaseEnt + ".Fields miCountField) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(\"'1'\");");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(\"1\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn funcionesMax(miCountField, arrColWhere, arrValWhere);");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\tpublic int funcionesCount(" + pNameClaseEnt + ".Fields miCountField, " + pNameClaseEnt + ".Fields miField, String miValue) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tArrayList<String> arrColWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrColWhere.add(miField.toString());");
            newTexto.AppendLine("\t\tArrayList<String> arrValWhere = new ArrayList<String>();");
            newTexto.AppendLine("\t\tarrValWhere.add(miValue);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn funcionesMax(miCountField, arrColWhere, arrValWhere);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic int funcionesCount(" + pNameClaseEnt + ".Fields miCountField, ArrayList<String> arrColWhere, ArrayList<String> arrValWhere) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\treturn funcionesMax(miCountField, arrColWhere, arrValWhere, \"\");");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic int funcionesCount(" + pNameClaseEnt + ".Fields miCountField, ArrayList<String> arrColWhere, ArrayList<String> arrValWhere, String strParamAdicionales) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tint idFieldCount = -1; ");
            newTexto.AppendLine("\t\tSQLiteDatabase nBD;");
            newTexto.AppendLine("\t\t" + formulario.txtClaseConn.Text + "  nCanal = new " + formulario.txtClaseConn.Text + " (miContexto);");
            newTexto.AppendLine("\t\ttry");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tArrayList<String> arrColumnas = new ArrayList<String>();");
            newTexto.AppendLine("\t\t\tarrColumnas.add(\"MAX(\" + miCountField.toString() + \")\");");
            newTexto.AppendLine("\t\t\tString[] strColumnas = arrColumnas.toArray(new String[arrColumnas.size()]);");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tBoolean bPrimerElemento = true;");
            newTexto.AppendLine("\t\t\tString strQuery = \"\";");
            newTexto.AppendLine("\t\t\tfor (int i = 0; i < arrColWhere.size(); i++)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tif (bPrimerElemento)");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\tstrQuery = strQuery + \" \" + arrColWhere.get(i) + \" = '\" + arrValWhere.get(i) + \"'\";");
            newTexto.AppendLine("\t\t\t\t\tbPrimerElemento = false;");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\telse");
            newTexto.AppendLine("\t\t\t\t\tstrQuery = strQuery + \" AND \" + arrColWhere.get(i) + \" = '\" + arrValWhere.get(i) + \"'\";");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tstrQuery = strQuery + \" \" + strParamAdicionales;");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tnBD = nCanal.getWritableDatabase();");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\t//Obtenemos el Cursor");
            newTexto.AppendLine("\t\t\tCursor cursor = nBD.query(cParametros.T_" + pNameTabla.ToUpper() + ", strColumnas, strQuery, null, null, null, null);");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tif (cursor.getCount() == 0) ");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tif (cursor != null)");
            newTexto.AppendLine("\t\t\t\t\tcursor.close();");
            newTexto.AppendLine("\t\t\t\tnBD.close();");
            newTexto.AppendLine("\t\t\t\tnCanal.close();");
            newTexto.AppendLine("\t\t\t\treturn idFieldMax;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\telse");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tif (cursor.getCount() > 1)");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\tif (cursor != null)");
            newTexto.AppendLine("\t\t\t\t\t\tcursor.close();");
            newTexto.AppendLine("\t\t\t\t\tnBD.close();");
            newTexto.AppendLine("\t\t\t\t\tnCanal.close();");
            newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"Se ha devuelto mas de un Resultado\");");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\telse");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\tif (cursor.moveToFirst())");
            newTexto.AppendLine("\t\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\t\tdo");
            newTexto.AppendLine("\t\t\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\t\t\tidFieldCount = cursor.getInt(0);");
            newTexto.AppendLine("\t\t\t\t\t\t} while (cursor.moveToNext());");
            newTexto.AppendLine("\t\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\tfinally");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tif (cursor != null)");
            newTexto.AppendLine("\t\t\t\t\tcursor.close();");
            newTexto.AppendLine("\t\t\t\tnBD.close();");
            newTexto.AppendLine("\t\t\t\tnCanal.close();");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch(Exception exp){}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\treturn idFieldCount;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tprivate String[] getArrayFromCursor(Cursor miCursor) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tString arrStr[] ={");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                string tipoCol = dtColumns.Rows[iCol][1].ToString();

                if (iCol == dtColumns.Rows.Count - 1)
                {
                    newTexto.AppendLine("\t\t\tmiCursor.getString(miCursor.getColumnIndex(" + pNameClaseEnt + ".Fields." + nameCol + ".toString())) + \"\"");
                }
                else
                {
                    newTexto.AppendLine("\t\t\tmiCursor.getString(miCursor.getColumnIndex(" + pNameClaseEnt + ".Fields." + nameCol + ".toString())) + \"\",");
                }
            }
            newTexto.AppendLine("\t\t\t};");
            newTexto.AppendLine("\t\treturn arrStr;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tprivate " + pNameClaseEnt + " getObjFromCursor(Cursor miCursor) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t" + pNameClaseEnt + " obj = new " + pNameClaseEnt + "(); ");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                string tipoCol = dtColumns.Rows[iCol][1].ToString();
                switch (tipoCol.ToUpper())
                {
                    case "INT":
                        newTexto.AppendLine("\t\tobj.set" + nameCol + "(miCursor.getInt(miCursor.getColumnIndex(" + pNameClaseEnt + ".Fields." + nameCol + ".toString())));");
                        break;

                    case "FLOAT":
                        newTexto.AppendLine("\t\tobj.set" + nameCol + "(miCursor.getFloat(miCursor.getColumnIndex(" + pNameClaseEnt + ".Fields." + nameCol + ".toString())));");
                        break;

                    case "STRING":
                        newTexto.AppendLine("\t\tobj.set" + nameCol + "(miCursor.getString(miCursor.getColumnIndex(" + pNameClaseEnt + ".Fields." + nameCol + ".toString())));");
                        break;

                    case "BYTE[]":
                        newTexto.AppendLine("\t\tobj.set" + nameCol + "(miCursor.getString(miCursor.getColumnIndex(" + pNameClaseEnt + ".Fields." + nameCol + ".toString())));");
                        break;

                    case "LONG":
                        newTexto.AppendLine("\t\tobj.set" + nameCol + "(miCursor.getLong(miCursor.getColumnIndex(" + pNameClaseEnt + ".Fields." + nameCol + ".toString())));");
                        break;

                    case "DATE":
                        newTexto.AppendLine("\t\tString strFecha" + iCol + " = miCursor.getString(miCursor.getColumnIndex(" + pNameClaseEnt + ".Fields." + nameCol + ".toString()));");
                        newTexto.AppendLine("\t\tobj.set" + nameCol + "(new Date(strFecha" + iCol + "));");
                        break;

                    default:
                        newTexto.AppendLine("\t\tobj.set" + nameCol + "(miCursor.getString(miCursor.getColumnIndex(" + pNameClaseEnt + ".Fields." + nameCol + ".toString())));");
                        break;
                }
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn obj;");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tprivate " + pNameClaseEnt + " getObjFromPaquete(String[] miPaquete) throws Exception");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t" + pNameClaseEnt + " obj = new " + pNameClaseEnt + "(); ");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                string tipoCol = dtColumns.Rows[iCol][1].ToString();
                switch (tipoCol.ToUpper())
                {
                    case "INT":
                        newTexto.AppendLine("\t\tobj.set" + nameCol + "(Integer.parseInt(miPaquete[" + iCol + "].replace(\"\\\"\", \"\")));");
                        break;

                    case "FLOAT":
                        newTexto.AppendLine("\t\tobj.set" + nameCol + "(Float.parseFloat(miPaquete[" + iCol + "].replace(\"\\\"\", \"\")));");
                        break;

                    case "STRING":
                        newTexto.AppendLine("\t\tobj.set" + nameCol + "(String.valueOf(miPaquete[" + iCol + "].replace(\"\\\"\", \"\")));");
                        break;

                    case "BYTE[]":
                        newTexto.AppendLine("\t\tobj.set" + nameCol + "(String.valueOf(miPaquete[" + iCol + "].replace(\"\\\"\", \"\")));");
                        break;

                    case "LONG":
                        newTexto.AppendLine("\t\tobj.set" + nameCol + "(Long.parseLong(miPaquete[" + iCol + "].replace(\"\\\"\", \"\")));");
                        break;

                    case "DATE":
                        newTexto.AppendLine("\t\tString strFecha" + iCol + " = String.valueOf(miPaquete[" + iCol + "].replace(\"\\\"\", \"\"));");
                        newTexto.AppendLine("\t\tobj.set" + nameCol + "(new Date(strFecha" + iCol + "));");
                        break;

                    default:
                        newTexto.AppendLine("\t\tobj.set" + nameCol + "(String.valueOf(miPaquete[" + iCol + "].replace(\"\\\"\", \"\")));");
                        break;
                }
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn obj;");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), pNameClase + ".java", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\" + pNameClase + ".java";
        }

        public string crearcParams(ref ReAlListView listaTablas, ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();

            newTexto.AppendLine("package " + formulario.txtInfProyectoClass.Text + ";");
            newTexto.AppendLine("");
            newTexto.AppendLine("public class cParametros");
            newTexto.AppendLine("{");
            newTexto.AppendLine("\tpublic static final String N_BD = \"NeuroDiagnostico.db\";");
            newTexto.AppendLine("\tpublic static final int VERSION_BD = 1;");
            newTexto.AppendLine("\t");
            for (int i = 0; i <= listaTablas.Items.Count - 1; i++)
            {
                if (listaTablas.Items[i].Checked == true)
                {
                    string nameTabla = listaTablas.Items[i].SubItems[0].Text;

                    newTexto.AppendLine("\tpublic static final String T_" + nameTabla.Replace("_", "").ToUpper() + " = \"" + nameTabla + "\";");
                }
            }
            newTexto.AppendLine("\t");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), "cParametros.java", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\cParametros.java";
        }

        public string crearConn(ref ReAlListView listaTablas, ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();

            newTexto.AppendLine("package " + formulario.txtInfProyectoClass.Text + ";");
            newTexto.AppendLine("");
            newTexto.AppendLine("import android.content.Context;");
            newTexto.AppendLine("import android.database.sqlite.SQLiteDatabase;");
            newTexto.AppendLine("import android.database.sqlite.SQLiteOpenHelper;");
            newTexto.AppendLine("");
            newTexto.AppendLine("");
            newTexto.AppendLine("public class " + formulario.txtClaseConn.Text + "  extends SQLiteOpenHelper");
            newTexto.AppendLine("{");
            newTexto.AppendLine("\tpublic " + formulario.txtClaseConn.Text + " (Context c)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tsuper(c, cParametros.N_BD, null, cParametros.VERSION_BD);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic void onCreate(SQLiteDatabase db)");
            newTexto.AppendLine("\t{");

            string queryTables1 = "SELECT name AS name, sql AS sql FROM sqlite_master WHERE type = 'table' ORDER BY name";
            DataTable dt1 = new DataTable();
            dt1 = CFuncionesBDs.CargarDataTable(queryTables1);

            foreach (DataRow row in dt1.Rows)
            {
                string nameTabla = row["name"].ToString();
                string sqlTabla = row["sql"] + ";";

                sqlTabla = sqlTabla.Replace("[" + nameTabla + "]", "\" + cParametros.T_" + nameTabla.ToUpper() + "\"");

                //Obtenemos las columnas de la tabla
                DataTable dtColumns = CFuncionesBDs.ObtenerListadoColumnas("", nameTabla, Languaje.JavaAndroid, TipoColumnas.Minusculas);

                foreach (DataRow rowCol in dtColumns.Rows)
                {
                    string nameCol = rowCol["name"].ToString();
                    string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + nameTabla.Substring(0, 1).ToUpper() +
                                           nameTabla.Substring(1, 2).ToLower() + nameTabla.Substring(3, 1).ToUpper() +
                                           nameTabla.Substring(4).ToLower();
                    sqlTabla = sqlTabla.Replace("[" + nameCol + "]", "\" + \r\n\t\t\t\t\t" + pNameClaseEnt + ".Fields." + nameCol + ".toString() + \"");
                }

                newTexto.AppendLine("\t\tdb.execSQL(\"" + sqlTabla + "\");");
            }

            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic void onUpgrade(SQLiteDatabase db, int oVer, int nVer)");
            newTexto.AppendLine("\t{");
            foreach (DataRow row in dt1.Rows)
            {
                string nameTabla = row["name"].ToString();
                newTexto.AppendLine("\t\tdb.execSQL(\"DROP TABLE IF EXISTS \" + cParametros.T_" + nameTabla.ToUpper() + "\";\");");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tonCreate(db);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), formulario.txtClaseConn.Text + ".java", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\" + formulario.txtClaseConn.Text + ".java";
        }
    }
}