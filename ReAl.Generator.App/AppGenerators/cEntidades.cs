﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using System;
using System.Data;
using System.IO;

namespace ReAl.Generator.App.AppGenerators
{
    public class cEntidades
    {
        #region Methods

        public string CrearEntidadesAngular(DataTable dtColumns, ref cEntidadesParametros misParams)
        {
            string nameCol = null;
            string tipoCol = null;

            bool esPrimerCampo = true;
            string PathDesktop = null;
            if (misParams.bMoverResultados)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + misParams.strInfProyecto;
            }
            else
            {
                PathDesktop = misParams.strPathProyecto + "\\" + misParams.strInfProyecto;
            }

            if (Directory.Exists(PathDesktop + "\\app\\entidades") == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\app\\entidades");
            }

            //Estandar de Angular
            misParams.strNameTabla = misParams.strNameTabla.Substring(0, 1).ToUpper() +
                                     misParams.strNameTabla.Substring(1, 2).ToLower() +
                                     misParams.strNameTabla.Substring(3, 1).ToUpper() +
                                     misParams.strNameTabla.Substring(4).ToLower();

            //Empezamos con el archivo
            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            newTexto.AppendLine("export class " + misParams.strNameTabla + " {");
            newTexto.AppendLine("\tconstructor(");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                if (esPrimerCampo)
                {
                    newTexto.AppendLine("\t\t\tpublic " + nameCol + ":" + tipoCol);
                    esPrimerCampo = false;
                }
                else
                {
                    newTexto.AppendLine("\t\t\t,public " + nameCol + ":" + tipoCol);
                }
            }
            newTexto.AppendLine("\t) {}");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), misParams.strNameTabla.ToLower() + ".ts", PathDesktop + "\\app\\entidades\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\app\\entidades\\" + misParams.strNameTabla.ToLower() + ".ts";
        }

        public string CrearEntidadesCodeIgniterSparkPhp(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }
            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase).Replace(".", "\\")) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase).Replace(".", "\\"));
            }

            string pNameTablaAux = pNameClase;
            string pNameTabla = pNameClase.Replace("_", "");
            dynamic pNameClaseRn = formulario.txtPrefijoModelo.Text + pNameTabla;
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text +
                                                               pNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                                               pNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                                               pNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                                               pNameTabla.Replace("_", "").Substring(4).ToLower() +
                                                               "Model";

            switch (Principal.DBMS)
            {
                case TipoBD.SQLServer:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;

                case TipoBD.SQLServer2000:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;

                case TipoBD.Oracle:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;

                case TipoBD.SQLite:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    pNameTabla = pNameTabla.ToLower();
                    break;

                case TipoBD.PostgreSQL:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    pNameTabla = pNameTabla.ToLower();
                    pNameClaseRn = pNameClaseRn.ToLower();
                    break;

                case TipoBD.FireBird:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;
            }

            string strLlaves = "";
            string strColumnaPk = "";
            string strLlavesWhere = "";
            bool bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString().ToLower();

                    strLlavesWhere = strLlavesWhere + Environment.NewLine + "\t\t$this->db->where('" + nameCol + "', $id);";
                    strColumnaPk = nameCol;

                    if (bEsPrimerElemento)
                    {
                        strLlaves = strLlaves + "$" + tipoCol + "_" + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlaves = strLlaves + ", $" + tipoCol + "_" + nameCol;
                    }
                }
            }

            string strApiTrans = "";
            string strApiEstado = "";
            string strUsuCre = "";
            string strFecCre = "";
            string strUsuMod = "";
            string strFecMod = "";
            string strUsuDel = "";
            string strFecDel = "";
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strUsuCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strUsuMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("del"))
                    strUsuDel = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strFecCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strFecMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("del"))
                    strFecDel = nameCol;
                if ((nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("ssi") || nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api")) && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                    strApiEstado = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                    strApiTrans = nameCol;
            }

            if (string.IsNullOrEmpty(strApiTrans))
                strApiTrans = "----";
            if (string.IsNullOrEmpty(strApiEstado))
                strApiEstado = "----";
            if (string.IsNullOrEmpty(strUsuCre))
                strUsuCre = "----";
            if (string.IsNullOrEmpty(strFecCre))
                strFecCre = "----";
            if (string.IsNullOrEmpty(strUsuMod))
                strUsuMod = "----";
            if (string.IsNullOrEmpty(strFecMod))
                strFecMod = "----";

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            newTexto.AppendLine("<?php");
            newTexto.AppendLine("");
            newTexto.AppendLine("namespace App\\Models;");
            newTexto.AppendLine("");
            newTexto.AppendLine("use CodeIgniter\\Model;");
            newTexto.AppendLine("");
            newTexto.AppendLine("class " + pNameClaseEnt + " extends Model");
            newTexto.AppendLine("{");
            newTexto.AppendLine("\tprotected $DBGroup          = 'default';");
            newTexto.AppendLine("\tprotected $table            = '"+ pNameTablaAux + "';");
            newTexto.AppendLine("\tprotected $primaryKey       = '"+ strColumnaPk + "';");
            newTexto.AppendLine("\tprotected $useAutoIncrement = true;");
            newTexto.AppendLine("\tprotected $insertID         = 0;");
            newTexto.AppendLine("\tprotected $returnType       = 'array';");
            newTexto.AppendLine("\tprotected $useSoftDeletes   = false;");
            newTexto.AppendLine("\tprotected $protectFields    = true;");
            newTexto.AppendLine("\tprotected $allowedFields    = [");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol]["name_original"].ToString();
                newTexto.AppendLine("\t\t'"+ nameCol + "',");
            }
            newTexto.AppendLine("\t];");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t// Dates");
            newTexto.AppendLine("\tprotected $useTimestamps    = false;");
            newTexto.AppendLine("\tprotected $dateFormat       = 'datetime';");
            newTexto.AppendLine("\tprotected $createdField     = '" + strFecCre + "';");
            newTexto.AppendLine("\tprotected $updatedField     = '" + strFecMod + "';");
            newTexto.AppendLine("\tprotected $deletedField     = '" + strFecDel + "';");
            newTexto.AppendLine("\tprotected $userCreateField  = '" + strUsuCre + "';");
            newTexto.AppendLine("\tprotected $userUpdateField  = '" + strUsuMod + "';");
            newTexto.AppendLine("\tprotected $userDeleteField  = '" + strUsuDel + "';");
            newTexto.AppendLine("\tprotected $apiEstado        = '" + strApiEstado + "';");
            newTexto.AppendLine("\tprotected $apiTransaccion   = '" + strApiTrans + "';");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t// Validation");
            newTexto.AppendLine("\tprotected $validationRules      = [];");
            newTexto.AppendLine("\tprotected $validationMessages   = [];");
            newTexto.AppendLine("\tprotected $skipValidation       = false;");
            newTexto.AppendLine("\tprotected $cleanValidationRules = true;");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t// Callbacks");
            newTexto.AppendLine("\tprotected $allowCallbacks = true;");
            newTexto.AppendLine("\tprotected $beforeInsert   = [];");
            newTexto.AppendLine("\tprotected $afterInsert    = [];");
            newTexto.AppendLine("\tprotected $beforeUpdate   = [];");
            newTexto.AppendLine("\tprotected $afterUpdate    = [];");
            newTexto.AppendLine("\tprotected $beforeFind     = [];");
            newTexto.AppendLine("\tprotected $afterFind      = [];");
            newTexto.AppendLine("\tprotected $beforeDelete   = [];");
            newTexto.AppendLine("\tprotected $afterDelete    = [];");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), pNameClaseEnt + ".php", PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase).Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase).Replace(".", "\\") + "\\" + pNameClaseEnt + ".php";
        }

        public string CrearEntidadesCodeIgniterClassicPhp(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }
            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase).Replace(".", "\\")) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase).Replace(".", "\\"));
            }

            string pNameTablaAux = pNameClase;
            string pNameTabla = pNameClase.Replace("_", "");
            dynamic pNameClaseRn = formulario.txtPrefijoModelo.Text + pNameTabla;
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameTabla;


            switch (Principal.DBMS)
            {
                case TipoBD.SQLServer:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;

                case TipoBD.SQLServer2000:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;

                case TipoBD.Oracle:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;

                case TipoBD.SQLite:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    pNameTabla = pNameTabla.ToLower();
                    break;

                case TipoBD.PostgreSQL:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    pNameTabla = pNameTabla.ToLower();
                    pNameClaseRn = pNameClaseRn.ToLower();
                    break;

                case TipoBD.FireBird:
                    pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
                    break;
            }

            string strLlaves = "";
            string strLlavesWhere = "";
            bool bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString().ToLower();

                    strLlavesWhere = strLlavesWhere + Environment.NewLine + "\t\t$this->db->where('" + nameCol + "', $id);";

                    if (bEsPrimerElemento)
                    {
                        strLlaves = strLlaves + "$" + tipoCol + "_" + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlaves = strLlaves + ", $" + tipoCol + "_" + nameCol;
                    }
                }
            }

            string strApiTrans = "";
            string strApiEstado = "";
            string strUsuCre = "";
            string strFecCre = "";
            string strUsuMod = "";
            string strFecMod = "";
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strUsuCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strUsuMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strFecCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strFecMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                    strApiEstado = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                    strApiTrans = nameCol;
            }

            if (string.IsNullOrEmpty(strApiTrans))
                strApiTrans = "----";
            if (string.IsNullOrEmpty(strApiEstado))
                strApiEstado = "----";
            if (string.IsNullOrEmpty(strUsuCre))
                strUsuCre = "----";
            if (string.IsNullOrEmpty(strFecCre))
                strFecCre = "----";
            if (string.IsNullOrEmpty(strUsuMod))
                strUsuMod = "----";
            if (string.IsNullOrEmpty(strFecMod))
                strFecMod = "----";

            string FC = DateTime.Now.ToString("dd/MM/yyyy");
            string Creador = (string.IsNullOrEmpty(formulario.txtInfAutor.Text) ? "Automatico     " : formulario.txtInfAutor.Text);
            Creador = Creador.PadRight(15);
            string CabeceraTmp = "/***********************************************************************************************************\r\n\tNOMBRE:       %PAR_NOMBRE%\r\n\tDESCRIPCION:\r\n\t\t%PAR_DESCRIPCION%\r\n\r\n\tREVISIONES:\r\n\t\tVer        FECHA       Autor            Descripcion \r\n\t\t---------  ----------  ---------------  ------------------------------------\r\n\t\t1.0        " + FC + "  " + Creador + "  Creacion \r\n\r\n*************************************************************************************************************/\r\n";

            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase).Replace("#region", "").Replace("#endregion", "");
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que implementa los metodos y operaciones sobre la Tabla " + pNameTabla);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            newTexto.AppendLine("<?php");
            newTexto.AppendLine("");
            newTexto.AppendLine(CabeceraTmp);
            newTexto.AppendLine("");
            newTexto.AppendLine("$ci = get_instance();");
            newTexto.AppendLine("$ci->load->helper('BasicEnum');");
            newTexto.AppendLine("");
            newTexto.AppendLine("//KB: http://stackoverflow.com/questions/254514/php-and-enumerations?answertab=active#tab-top");
            newTexto.AppendLine("//KB: http://php.net/manual/en/class.splenum.php");
            newTexto.AppendLine("abstract class " + pNameClase + "_fields extends BasicEnum");
            newTexto.AppendLine("{");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\tconst " + nameCol + " = \"" + nameCol + "\";");
            }
            newTexto.AppendLine("}");

            newTexto.AppendLine("");
            newTexto.AppendLine("class " + pNameClaseEnt + " extends CI_Model");
            newTexto.AppendLine("{");
            newTexto.AppendLine("\t//Las columnas de la tabla");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("var $" + nameCol + ";");
            }
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t//Parametros de la tabla");
            newTexto.AppendLine("\tconst StrNombreTabla = \"" + pNameTablaAux + "\";");
            newTexto.AppendLine("\tconst StrAliasTabla = \"" + pNameTablaAux + "\";");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tfunction __construct()");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tparent::__construct();");
            newTexto.AppendLine("\t\t$this->load->helper('BasicEnum');");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic function insert_" + pNameTabla.ToLower() + "(self $obj)");
            newTexto.AppendLine("\t{");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                string tipoCol = dtColumns.Rows[iCol][1].ToString();

                if (nameCol == strUsuCre)
                {
                    newTexto.AppendLine("\t\t$data->" + nameCol + " = $db['default']['username'];");
                }
                else if (nameCol == strFecCre)
                {
                    newTexto.AppendLine("\t\t$data->" + nameCol + " = now();");
                }
                else if (nameCol == strUsuMod)
                {
                }
                else if (nameCol == strFecMod)
                {
                }
                else if (nameCol == strApiEstado)
                {
                }
                else if (nameCol == strApiTrans)
                {
                }
                else
                {
                    newTexto.AppendLine("\t\t$data->" + nameCol + " = $obj->" + nameCol + ";");
                }
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t$this->db->insert(self::StrNombreTabla, $data);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic function update_" + pNameTabla.ToLower() + "(self $obj)");
            newTexto.AppendLine("\t{");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "NO"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString();

                    if (nameCol == strUsuCre)
                    {
                    }
                    else if (nameCol == strFecCre)
                    {
                    }
                    else if (nameCol == strUsuMod)
                    {
                        newTexto.AppendLine("\t\t$data->" + nameCol + " = $db['default']['username'];");
                    }
                    else if (nameCol == strFecMod)
                    {
                        newTexto.AppendLine("\t\t$data->" + nameCol + " = now();");
                    }
                    else if (nameCol == strApiEstado)
                    {
                    }
                    else if (nameCol == strApiTrans)
                    {
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t$data->" + nameCol + " = $obj->" + nameCol + ";");
                    }
                }
            }

            newTexto.AppendLine("\t\t");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString();

                    newTexto.AppendLine("\t\t$this->db->where('" + nameCol + "', $obj->" + nameCol + ");");
                }
            }
            newTexto.AppendLine("\t\t$this->db->update(self::StrNombreTabla, $data);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic function delete_" + pNameTabla.ToLower() + "(self $obj)");
            newTexto.AppendLine("\t{");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "NO"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString();

                    if (nameCol == strUsuCre)
                    {
                    }
                    else if (nameCol == strFecCre)
                    {
                    }
                    else if (nameCol == strUsuMod)
                    {
                        newTexto.AppendLine("\t\t$data->" + nameCol + " = $db['default']['username'];");
                    }
                    else if (nameCol == strFecMod)
                    {
                        newTexto.AppendLine("\t\t$data->" + nameCol + " = now();");
                    }
                    else if (nameCol == strApiEstado)
                    {
                    }
                    else if (nameCol == strApiTrans)
                    {
                    }
                    else
                    {
                    }
                }
            }

            newTexto.AppendLine("\t\t");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString();

                    newTexto.AppendLine("\t\t$this->db->where('" + nameCol + "', $obj->" + nameCol + ");");
                }
            }
            newTexto.AppendLine("\t\t$this->db->delete(self::StrNombreTabla, $data);");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic function obtener_" + pNameTabla.ToLower() + "(" + strLlaves + ")");
            newTexto.AppendLine("\t{");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString();

                    newTexto.AppendLine("\t\t$this->db->where('" + nameCol + "', $" + tipoCol + "_" + nameCol + ");");
                }
            }
            newTexto.AppendLine("\t\t$query = $this->db->get(self::StrNombreTabla);");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tif ($query->num_rows() > 0)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t//KB: http://stackoverflow.com/questions/13916194/how-to-get-class-object-with-query-row-on-codeigniter");
            newTexto.AppendLine("\t\t\treturn $query->row(0, '" + pNameClaseEnt + "');");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\treturn null;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic function obtener_" + pNameTabla.ToLower() + "_fila(" + strLlaves + ")");
            newTexto.AppendLine("\t{");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString();

                    newTexto.AppendLine("\t\t$this->db->where('" + nameCol + "', $" + tipoCol + "_" + nameCol + ");");
                }
            }
            newTexto.AppendLine("\t\t$query = $this->db->get(self::StrNombreTabla);");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t\tif ($query->num_rows() > 0)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t//KB: http://stackoverflow.com/questions/4280235/codeigniter-return-only-one-row");
            newTexto.AppendLine("\t\t\t$row = $query->first_row('array');");
            newTexto.AppendLine("\t\t\treturn $row; //Para acceder a un elemento usar $resultado_funcion[nombre_campo]");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\treturn null;");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic function listar_" + pNameTabla.ToLower() + "()");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t$query = $this->db->get(self::StrNombreTabla);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn $query->result_array(); // Para iterar usar: foreach ($resultado_funcion as $row) { $row[nombre_campo] }");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");

            newTexto.AppendLine("\tpublic function filtrar_" + pNameTabla.ToLower() + "($columna, $valor)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t//Validamos el campo y la columna si es necesario, segun el ENUM");
            newTexto.AppendLine("\t\tif (!" + pNameClase + "_fields::isValidName($campo))");
            newTexto.AppendLine("\t\t\treturn null;");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t$this->db->where($columna, $valor);");
            newTexto.AppendLine("\t\t$query = $this->db->get(self::StrNombreTabla);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t//KB: https://ellislab.com/codeigniter/user-guide/database/results.html");
            newTexto.AppendLine("\t\treturn $query->result_array(); // Para iterar usar: foreach ($resultado_funcion as $row) { $row[nombre_campo]}");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\tpublic function contar_" + pNameTabla.ToLower() + "($campo, $columna = null, $valor = null)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t//Validamos el campo y la columna si es necesario, segun el ENUM");
            newTexto.AppendLine("\t\tif (!" + pNameClase + "_fields::isValidName($campo))");
            newTexto.AppendLine("\t\t\treturn null;");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t//Ejecutamos el query");
            newTexto.AppendLine("\t\t$this->db->select('COUNT('.$campo.') as '.$campo);");
            newTexto.AppendLine("\t\tif (empty($columna))");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (!segusuario_fields::isValidName($columna))");
            newTexto.AppendLine("\t\t\t\treturn null;");
            newTexto.AppendLine("\t\t\t$this->db->where($columna, $valor);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t$query = $this->db->get(self::StrNombreTabla, $data);");
            newTexto.AppendLine("\t\t$row = $query->first_row('array');");
            newTexto.AppendLine("\t\treturn $row[$campo]; ");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\tpublic function sum_" + pNameTabla.ToLower() + "($campo, $columna = null, $valor = null)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t//Validamos el campo y la columna si es necesario, segun el ENUM");
            newTexto.AppendLine("\t\tif (!" + pNameClase + "_fields::isValidName($campo))");
            newTexto.AppendLine("\t\t\treturn null;");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t//Ejecutamos el query");
            newTexto.AppendLine("\t\t$this->db->select_sum($campo);");
            newTexto.AppendLine("\t\tif (empty($columna))");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (!segusuario_fields::isValidName($columna))");
            newTexto.AppendLine("\t\t\t\treturn null;");
            newTexto.AppendLine("\t\t\t$this->db->where($columna, $valor);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t$query = $this->db->get(self::StrNombreTabla, $data);");
            newTexto.AppendLine("\t\t$row = $query->first_row('array');");
            newTexto.AppendLine("\t\treturn $row[$campo]; ");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\tpublic function avg_" + pNameTabla.ToLower() + "($campo, $columna = null, $valor = null)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t//Validamos el campo y la columna si es necesario, segun el ENUM");
            newTexto.AppendLine("\t\tif (!" + pNameClase + "_fields::isValidName($campo))");
            newTexto.AppendLine("\t\t\treturn null;");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t//Ejecutamos el query");
            newTexto.AppendLine("\t\t$this->db->select_avg($campo);");
            newTexto.AppendLine("\t\tif (empty($columna))");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (!segusuario_fields::isValidName($columna))");
            newTexto.AppendLine("\t\t\t\treturn null;");
            newTexto.AppendLine("\t\t\t$this->db->where($columna, $valor);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t$query = $this->db->get(self::StrNombreTabla, $data);");
            newTexto.AppendLine("\t\t$row = $query->first_row('array');");
            newTexto.AppendLine("\t\treturn $row[$campo]; ");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\tpublic function min_" + pNameTabla.ToLower() + "($campo, $columna = null, $valor = null)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t//Validamos el campo y la columna si es necesario, segun el ENUM");
            newTexto.AppendLine("\t\tif (!" + pNameClase + "_fields::isValidName($campo))");
            newTexto.AppendLine("\t\t\treturn null;");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t//Ejecutamos el query");
            newTexto.AppendLine("\t\t$this->db->select_min($campo);");
            newTexto.AppendLine("\t\tif (empty($columna))");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (!segusuario_fields::isValidName($columna))");
            newTexto.AppendLine("\t\t\t\treturn null;");
            newTexto.AppendLine("\t\t\t$this->db->where($columna, $valor);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t$query = $this->db->get(self::StrNombreTabla, $data);");
            newTexto.AppendLine("\t\t$row = $query->first_row('array');");
            newTexto.AppendLine("\t\treturn $row[$campo]; ");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\tpublic function max_" + pNameTabla.ToLower() + "($campo, $columna = null, $valor = null)");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t//Validamos el campo y la columna si es necesario, segun el ENUM");
            newTexto.AppendLine("\t\tif (!" + pNameClase + "_fields::isValidName($campo))");
            newTexto.AppendLine("\t\t\treturn null;");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t//Ejecutamos el query");
            newTexto.AppendLine("\t\t$this->db->select_max($campo);");
            newTexto.AppendLine("\t\tif (empty($columna))");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (!segusuario_fields::isValidName($columna))");
            newTexto.AppendLine("\t\t\t\treturn null;");
            newTexto.AppendLine("\t\t\t$this->db->where($columna, $valor);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t$query = $this->db->get(self::StrNombreTabla, $data);");
            newTexto.AppendLine("\t\t$row = $query->first_row('array');");
            newTexto.AppendLine("\t\treturn $row[$campo]; ");
            newTexto.AppendLine("\t}");

            newTexto.AppendLine("\t");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), pNameClaseEnt + ".php", PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase).Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase).Replace(".", "\\") + "\\" + pNameClaseEnt + ".php";
        }

        public string CrearEntidadesCS(DataTable dtColumns, ref cEntidadesParametros misParams)
        {
            string nameCol = null;
            string tipoCol = null;

            string PermiteNull = null;
            string TipoColNull = null;
            string DescripcionCol = null;
            bool esPrimerCampo = true;
            string PathDesktop = null;
            if (misParams.bMoverResultados)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + misParams.strInfProyectoClass;
            }
            else
            {
                PathDesktop = misParams.strPathProyecto + "\\" + misParams.strInfProyectoClass;
            }

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            if (Directory.Exists(PathDesktop + "\\" + misParams.strNamespaceEntidades) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + misParams.strNamespaceEntidades);
            }

            string strAlias = misParams.strNameTabla;
            string strDescripcion = misParams.strNameTabla;
            try
            {
                DataTable dtPrefijo = new DataTable();

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        dtPrefijo = CFuncionesBDs.CargarDataTable(
                            "SELECT aliassta, descripcionsta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" +
                            misParams.strNameTabla.ToUpper() + "')");
                        break;

                    case TipoBD.SQLServer2000:
                        dtPrefijo = CFuncionesBDs.CargarDataTable(
                            "SELECT aliassta, descripcionsta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" +
                            misParams.strNameTabla.ToUpper() + "')");
                        break;

                    case TipoBD.Oracle:
                        dtPrefijo = CFuncionesBDs.CargarDataTable(
                            "SELECT ALIASSTA, DESCRIPCIONSTA FROM SEGTABLAS WHERE UPPER(TABLASTA) = '" +
                            misParams.strNameTabla.ToUpper() + "'");
                        break;

                    case TipoBD.PostgreSQL:
                        dtPrefijo = CFuncionesBDs.CargarDataTable(
                            "SELECT s.aliassta, s.descripcionsta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" +
                            misParams.strNameTabla.ToUpper() + "')");
                        if (dtPrefijo.Rows.Count == 0)
                        {
                            dtPrefijo = CFuncionesBDs.CargarDataTable(
                                "SELECT s.alias, s.descripcion FROM seg_tablas s WHERE UPPER(s.nombretabla) = UPPER('" +
                                misParams.strNameTabla.ToUpper() + "')");
                        }

                        break;
                }

                if (dtPrefijo.Rows.Count > 0)
                {
                    strAlias = dtPrefijo.Rows[0][0].ToString().ToLower();
                    strDescripcion = dtPrefijo.Rows[0][1].ToString().ToLower();
                    //strAlias = strAlias.Substring(0, 1).ToUpper() + strAlias.Substring(1, strAlias.Length - 1);
                }
                else
                {
                    strAlias = misParams.strNameTabla;
                    strDescripcion = misParams.strNameTabla;
                }
            }
            catch (Exception ex)
            {
                strAlias = misParams.strNameTabla;
                strDescripcion = misParams.strNameTabla;
            }

            //Obtenemos la llave primaria
            string strLlavePrimaria = "";
            string strLlavePrimariaSinTipos = "";
            dynamic bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();
                    if (bEsPrimerElemento)
                    {
                        strLlavePrimaria = strLlavePrimaria + tipoCol + " " + tipoCol + nameCol;
                        strLlavePrimariaSinTipos = strLlavePrimariaSinTipos + tipoCol + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlavePrimaria = strLlavePrimaria + ", " + tipoCol + " " + tipoCol + nameCol;
                        strLlavePrimariaSinTipos = strLlavePrimariaSinTipos + ", " + tipoCol + nameCol;
                    }
                }
            }
            if (string.IsNullOrEmpty(strLlavePrimaria))
            {
                strLlavePrimaria = dtColumns.Rows[0][1] + " " + dtColumns.Rows[0][1] + dtColumns.Rows[0][0];
                strLlavePrimariaSinTipos = dtColumns.Rows[0][1].ToString() + dtColumns.Rows[0][0].ToString();
            }

            //Empezamos con el archivo
            string CabeceraTmp = misParams.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", misParams.strNameClase);
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que define un objeto para la Tabla " + misParams.strNameTabla);

            newTexto.AppendLine(CabeceraTmp);
            newTexto.AppendLine("#region");
            newTexto.AppendLine("using System;");
            newTexto.AppendLine("using System.Collections.Generic;");
            if ((misParams.bUseEventProperties))
            {
                newTexto.AppendLine("using System.ComponentModel;");
            }
            if (misParams.bUseDataAnnotations)
            {
                newTexto.AppendLine("using System.ComponentModel.DataAnnotations;");
                if (misParams.miEntorno == TipoEntorno.CSharp_NetCore_V2
                    || misParams.miEntorno == TipoEntorno.CSharp_NetCore_V5
                    || misParams.miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2
                    || misParams.miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger
                    || misParams.miEntorno == TipoEntorno.CSharp_NetCore_V5_WebAPI_Client)
                    newTexto.AppendLine("using System.ComponentModel.DataAnnotations.Schema;");
            }
            newTexto.AppendLine("using System.IO;");
            newTexto.AppendLine("using System.Numerics;");
            newTexto.AppendLine("using System.Reflection;");
            newTexto.AppendLine("using System.Security.Cryptography;");
            newTexto.AppendLine("using System.Text;");
            newTexto.AppendLine("using System.Xml.Serialization;");
            newTexto.AppendLine("using System.Runtime.Serialization;");
            //newTexto.AppendLine("using " + misParams.strInfProyectoClass + "." + misParams.strNamespaceModelo + ";");
            newTexto.AppendLine("#endregion");
            newTexto.AppendLine("");
            newTexto.AppendLine("namespace " + misParams.strInfProyectoClass + "." + misParams.strNamespaceEntidades);
            newTexto.AppendLine("{");
            if (misParams.bradWCF)
            {
                newTexto.AppendLine("\t[DataContract]");
            }

            if (misParams.bUseBaseClass)
            {
                if (misParams.bUseDataAnnotations)
                    if (misParams.miEntorno == TipoEntorno.CSharp_NetCore_V2
                        || misParams.miEntorno == TipoEntorno.CSharp_NetCore_V5
                        || misParams.miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger
                        || misParams.miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2)
                        newTexto.AppendLine("\t[Table(\"" + misParams.strNameTabla + "\")]");
                newTexto.AppendLine("\tpublic class " + misParams.strNameClase + " : " + misParams.strParamBaseClass);
            }
            else
            {
                if (misParams.bUseEventProperties)
                {
                    if (misParams.bUseDataAnnotations)
                        newTexto.AppendLine("\t[Table(\"" + misParams.strNameTabla + "\")]");
                    newTexto.AppendLine("\tpublic class " + misParams.strNameClase + " : INotifyPropertyChanged");
                }
                else
                {
                    if (misParams.bUseDataAnnotations)
                        newTexto.AppendLine("\t[Table(\"" + misParams.strNameTabla + "\")]");
                    newTexto.AppendLine("\tpublic class " + misParams.strNameClase);
                }
            }

            newTexto.AppendLine("\t{");

            if (misParams.bradWCF == false)
            {
                if (string.IsNullOrEmpty(misParams.strNameSchema.Trim()))
                {
                    newTexto.AppendLine("\t\tpublic const string StrNombreTabla = \"" +
                        misParams.strNameTabla.Substring(0, 1).ToUpper() +
                        misParams.strNameTabla.Substring(1, 2).ToLower() +
                        misParams.strNameTabla.Substring(3, 1).ToUpper() +
                        misParams.strNameTabla.Substring(4) +
                        "\";");
                }
                else if (misParams.strNameSchema.ToLower() != "public" & misParams.strNameSchema.ToLower() != "dbo")
                {
                    newTexto.AppendLine("\t\tpublic const string StrNombreTabla = \"" + misParams.strNameSchema + "." +
                        misParams.strNameTabla.Substring(0, 1).ToUpper() +
                        misParams.strNameTabla.Substring(1, 2).ToLower() +
                        misParams.strNameTabla.Substring(3, 1).ToUpper() +
                        misParams.strNameTabla.Substring(4) +
                        "\";");
                }
                else
                {
                    newTexto.AppendLine("\t\tpublic const string StrNombreTabla = \"" +
                        misParams.strNameTabla.Substring(0, 1).ToUpper() +
                        misParams.strNameTabla.Substring(1, 2).ToLower() +
                        misParams.strNameTabla.Substring(3, 1).ToUpper() +
                        misParams.strNameTabla.Substring(4) +
                        "\";");
                }

                newTexto.AppendLine("\t\tpublic const string StrAliasTabla = \"" + strAlias + "\";");
                newTexto.AppendLine("\t\tpublic const string StrDescripcionTabla = \"" + strDescripcion + "\";");
                newTexto.AppendLine("\t\tpublic const string StrNombreLisForm = \"Listado de " + misParams.strNameTabla + "\";");
                newTexto.AppendLine("\t\tpublic const string StrNombreDocForm = \"Detalle de " + misParams.strNameTabla + "\";");
                newTexto.AppendLine("\t\tpublic const string StrNombreForm = \"Registro de " + misParams.strNameTabla + "\";");

                //ENUM
                if (misParams.bradWCF)
                    newTexto.AppendLine("\t\t[DataContract]");
                newTexto.AppendLine("\t\tpublic enum Fields");
                newTexto.AppendLine("\t\t{");
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();
                    if (esPrimerCampo)
                    {
                        if (misParams.bradWCF)
                            newTexto.AppendLine("\t\t\t[EnumMember]");

                        newTexto.AppendLine("\t\t\t" + nameCol);
                        esPrimerCampo = false;
                    }
                    else
                    {
                        if (misParams.bradWCF)
                            newTexto.AppendLine("\t\t\t[EnumMember]");
                        newTexto.AppendLine("\t\t\t," + nameCol);
                    }
                }
                newTexto.AppendLine("\r\n\t\t}");
                newTexto.AppendLine("\t\t");
            }

            //CONSTRUCTORES
            newTexto.AppendLine("\t\t#region Constructoress");
            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t\tpublic " + misParams.strNameClase + "()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t//Inicializacion de Variables");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                PermiteNull = dtColumns.Rows[iCol]["PermiteNull"].ToString();
                if (tipoCol.ToUpper() == "STRING")
                {
                    newTexto.AppendLine("\t\t\t" + nameCol + " = null;");
                }
                else if (tipoCol.ToUpper() == "BOOL")
                {
                    newTexto.AppendLine("\t\t\t//" + nameCol + " = false;");
                }
                else
                {
                    if (PermiteNull == "Yes")
                    {
                        newTexto.AppendLine("\t\t\t" + nameCol + " = null;");
                    }
                }
            }
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic " + misParams.strNameClase + "(" + misParams.strNameClase + " obj)");
            newTexto.AppendLine("\t\t{");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t" + nameCol + " = obj." + nameCol + ";");
            }

            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t#endregion");
            newTexto.AppendLine("\t\t");

            //METODOS ESTATICOS
            if (misParams.bradWCF == false)
            {
                newTexto.AppendLine("\t\t#region Metodos Estaticos");
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// Funcion para obtener un objeto para el API y sus datos");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <returns>Objeto de tipo " + misParams.strParamClaseApiObject + " con los datos para enviar al API</returns>");
                newTexto.AppendLine("\t\tpublic " + misParams.strParamClaseApiObject + " CreateApiObject()");
                newTexto.AppendLine("\t\t{");

                if (misParams.miEntorno == TipoEntorno.CSharp_NetCore_V5)
                {
                    newTexto.AppendLine("\t\t\tvar objApi = new " + misParams.strParamClaseApiObject + " {Nombre = StrNombreTabla, Datos = this};");
                }
                else
                {
                    newTexto.AppendLine("\t\t\tvar objApi = new " + misParams.strParamClaseApiObject + "();");
                    newTexto.AppendLine("\t\t\tobjApi.Nombre = " + misParams.strNameClase + ".StrNombreTabla;");
                    newTexto.AppendLine("\t\t\tobjApi.Datos = this;");
                }
                newTexto.AppendLine("\t\t\treturn objApi;");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");
                //Solo si estan en el mismo NameSpace
                if (misParams.strNamespaceEntidades == misParams.strNamespaceModelo)
                {
                    //Obtener Objeto por Llave primaria FK
                    newTexto.AppendLine("\t\t/// <summary>");
                    newTexto.AppendLine("\t\t/// \tFuncion Estatica que obtiene los datos de una Clase " +
                                        misParams.strNameClase + " a partir de condiciones WHERE");
                    newTexto.AppendLine("\t\t/// </summary>");
                    newTexto.AppendLine("\t\t/// <returns>");
                    newTexto.AppendLine("\t\t/// \tValor del Tipo " + misParams.strNameClase +
                                        " que cumple con los filtros de los parametros");
                    newTexto.AppendLine("\t\t/// </returns>");
                    newTexto.AppendLine("\t\tpublic static " + misParams.strNameClase + " ObtenerObjeto(" +
                                        strLlavePrimaria + ")");
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\t" + misParams.strNameClaseModelo + " rn = new " +
                                        misParams.strNameClaseModelo + "();");
                    newTexto.AppendLine("\t\t\treturn rn.ObtenerObjeto(" + strLlavePrimariaSinTipos + ");");
                    newTexto.AppendLine("\t\t}");
                    newTexto.AppendLine("\t\t");
                    newTexto.AppendLine("\t\t/// <summary>");
                    newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " +
                                        misParams.strNameTabla + " a partir de una clase del tipo E" +
                                        misParams.strNameTabla);
                    newTexto.AppendLine("\t\t/// </summary>");
                    newTexto.AppendLine("\t\t/// <returns>");
                    newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" +
                                        misParams.strNameTabla);
                    newTexto.AppendLine("\t\t/// </returns>");
                    newTexto.AppendLine("\t\tpublic bool Insert()");
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\t" + misParams.strNameClaseModelo + " rn = new " +
                                        misParams.strNameClaseModelo + "();");
                    newTexto.AppendLine("\t\t\treturn rn.Insert(this);");
                    newTexto.AppendLine("\t\t}");
                    newTexto.AppendLine("\t\t");
                    newTexto.AppendLine("\t\t/// <summary>");
                    newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " +
                                        misParams.strNameTabla + " a partir de una clase del tipo E" +
                                        misParams.strNameTabla);
                    newTexto.AppendLine("\t\t/// </summary>");
                    newTexto.AppendLine("\t\t/// <returns>");
                    newTexto.AppendLine("\t\t/// \t Valor que indica la cantidad de registros actualizados en " +
                                        misParams.strNameTabla);
                    newTexto.AppendLine("\t\t/// </returns>");
                    newTexto.AppendLine("\t\tpublic int Update()");
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\t" + misParams.strNameClaseModelo + " rn = new " +
                                        misParams.strNameClaseModelo + "();");
                    newTexto.AppendLine("\t\t\treturn rn.Update(this);");
                    newTexto.AppendLine("\t\t}");
                    newTexto.AppendLine("\t\t");
                    newTexto.AppendLine("\t\t/// <summary>");
                    newTexto.AppendLine("\t\t/// \t Funcion que elimina un registro en la tabla " +
                                        misParams.strNameTabla + " a partir de una clase del tipo " +
                                        misParams.strNameTabla + " y su respectiva PK");
                    newTexto.AppendLine("\t\t/// </summary>");
                    newTexto.AppendLine("\t\t/// <returns>");
                    newTexto.AppendLine("\t\t/// \t Cantidad de registros afectados por el exito de la operacion" +
                                        misParams.strNameTabla);
                    newTexto.AppendLine("\t\t/// </returns>");
                    newTexto.AppendLine("\t\tpublic int Delete()");
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\t" + misParams.strNameClaseModelo + " rn = new " +
                                        misParams.strNameClaseModelo + "();");
                    newTexto.AppendLine("\t\t\treturn rn.Delete(this);");
                    newTexto.AppendLine("\t\t}");
                    newTexto.AppendLine("\t\t");
                }
                newTexto.AppendLine("\t\t#endregion");
                newTexto.AppendLine("\t\t");
            }

            if (misParams.miEntorno != TipoEntorno.CSharp_NetCore_V2)
            {
                if (!misParams.bUseBaseClass)
                {
                    //Ahora los metodos de DataContract
                    newTexto.AppendLine("\t\t#region Metodos Privados");
                    newTexto.AppendLine("\t\t");
                    newTexto.AppendLine("\t\t/// <summary>");
                    newTexto.AppendLine("\t\t/// Obtiene el Hash a partir de un array de Bytes");
                    newTexto.AppendLine("\t\t/// </summary>");
                    newTexto.AppendLine("\t\t/// <param name=\"objectAsBytes\"></param>");
                    newTexto.AppendLine("\t\t/// <returns>string</returns>");
                    newTexto.AppendLine("\t\tprivate string ComputeHash(byte[] objectAsBytes)");
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\tMD5 md5 = new MD5CryptoServiceProvider();");
                    newTexto.AppendLine("\t\t\ttry");
                    newTexto.AppendLine("\t\t\t{");
                    newTexto.AppendLine("\t\t\t\tbyte[] result = md5.ComputeHash(objectAsBytes);");
                    newTexto.AppendLine("\t\t\t\t");
                    newTexto.AppendLine("\t\t\t\tStringBuilder sb = new StringBuilder();");
                    newTexto.AppendLine("\t\t\t\tfor (int i = 0; i < result.Length; i++)");
                    newTexto.AppendLine("\t\t\t\t{");
                    newTexto.AppendLine("\t\t\t\t\tsb.Append(result[i].ToString(\"X2\"));");
                    newTexto.AppendLine("\t\t\t\t}");
                    newTexto.AppendLine("\t\t\t\t");
                    newTexto.AppendLine("\t\t\t\treturn sb.ToString();");
                    newTexto.AppendLine("\t\t\t}");
                    newTexto.AppendLine("\t\t\tcatch (ArgumentNullException ane)");
                    newTexto.AppendLine("\t\t\t{");
                    newTexto.AppendLine("\t\t\t\treturn null;");
                    newTexto.AppendLine("\t\t\t}");
                    newTexto.AppendLine("\t\t}");
                    newTexto.AppendLine("\t\t");

                    newTexto.AppendLine("\t\t/// <summary>");
                    newTexto.AppendLine("\t\t///     Obtienen el Hash basado en algun algoritmo de Encriptación");
                    newTexto.AppendLine("\t\t/// </summary>");
                    newTexto.AppendLine("\t\t/// <typeparam name=\"T\">");
                    newTexto.AppendLine("\t\t///     Algoritmo de encriptación");
                    newTexto.AppendLine("\t\t/// </typeparam>");
                    newTexto.AppendLine("\t\t/// <param name=\"cryptoServiceProvider\">");
                    newTexto.AppendLine("\t\t///     Provvedor de Servicios de Criptografía");
                    newTexto.AppendLine("\t\t/// </param>");
                    newTexto.AppendLine("\t\t/// <returns>");
                    newTexto.AppendLine("\t\t///     String que representa el Hash calculado");
                    newTexto.AppendLine("\t\t        /// </returns>");
                    newTexto.AppendLine("\t\tprivate string computeHash<T>( T cryptoServiceProvider) where T : HashAlgorithm, new()");
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\tDataContractSerializer serializer = new DataContractSerializer(this.GetType());");
                    newTexto.AppendLine("\t\t\tusing (MemoryStream memoryStream = new MemoryStream())");
                    newTexto.AppendLine("\t\t\t{");
                    newTexto.AppendLine("\t\t\t\tserializer.WriteObject(memoryStream, this);");
                    newTexto.AppendLine("\t\t\t\tcryptoServiceProvider.ComputeHash(memoryStream.ToArray());");
                    newTexto.AppendLine("\t\t\t\treturn Convert.ToBase64String(cryptoServiceProvider.Hash);");
                    newTexto.AppendLine("\t\t\t}");
                    newTexto.AppendLine("\t\t}");
                    newTexto.AppendLine("\t\t");
                    newTexto.AppendLine("\t\t#endregion");
                    newTexto.AppendLine("\t\t");

                    newTexto.AppendLine("\t\t#region Overrides");
                    newTexto.AppendLine("\t\t");

                    newTexto.AppendLine("\t\t/// <summary>");
                    newTexto.AppendLine("\t\t/// Devuelve un String que representa al Objeto");
                    newTexto.AppendLine("\t\t/// </summary>");
                    newTexto.AppendLine("\t\t/// <returns>string</returns>");
                    newTexto.AppendLine("\t\tpublic override string ToString()");
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\tstring hashString;");
                    newTexto.AppendLine("\t\t\t");
                    newTexto.AppendLine("\t\t\t//Evitar parametros NULL");
                    newTexto.AppendLine("\t\t\tif (this == null)");
                    newTexto.AppendLine("\t\t\t\tthrow new ArgumentNullException(\"Parametro NULL no es valido\");");
                    newTexto.AppendLine("\t\t\t");
                    newTexto.AppendLine("\t\t\t//Se verifica si el objeto es serializable.");
                    newTexto.AppendLine("\t\t\ttry");
                    newTexto.AppendLine("\t\t\t{");
                    newTexto.AppendLine("\t\t\t\tMemoryStream memStream = new MemoryStream();");
                    newTexto.AppendLine("\t\t\t\tXmlSerializer serializer = new XmlSerializer(typeof(" + misParams.strNameClase + "));");
                    newTexto.AppendLine("\t\t\t\tserializer.Serialize(memStream, this);");
                    newTexto.AppendLine("\t\t\t\t");
                    newTexto.AppendLine("\t\t\t\t//Ahora se obtiene el Hash del Objeto.");
                    newTexto.AppendLine("\t\t\t\thashString = ComputeHash(memStream.ToArray());");
                    newTexto.AppendLine("\t\t\t\t");
                    newTexto.AppendLine("\t\t\t\treturn hashString;");
                    newTexto.AppendLine("\t\t\t}");
                    newTexto.AppendLine("\t\t\tcatch (AmbiguousMatchException ame)");
                    newTexto.AppendLine("\t\t\t{");
                    newTexto.AppendLine("\t\t\t\tthrow new ApplicationException(\"El Objeto no es Serializable. Message:\" + ame.Message);");
                    newTexto.AppendLine("\t\t\t}");
                    newTexto.AppendLine("\t\t}");
                    newTexto.AppendLine("\t\t");
                    newTexto.AppendLine("\t\t/// <summary>");
                    newTexto.AppendLine("\t\t/// Verifica que dos objetos sean identicos");
                    newTexto.AppendLine("\t\t/// </summary>");
                    newTexto.AppendLine("\t\tpublic static bool operator ==(" + misParams.strNameClase + " first, " + misParams.strNameClase + " second)");
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\t// Verifica si el puntero en memoria es el mismo");
                    newTexto.AppendLine("\t\t\tif (Object.ReferenceEquals(first, second))");
                    newTexto.AppendLine("\t\t\t\treturn true;");
                    newTexto.AppendLine("\t\t\t");
                    newTexto.AppendLine("\t\t\t// Verifica valores nulos");
                    newTexto.AppendLine("\t\t\tif ((object) first == null || (object) second == null)");
                    newTexto.AppendLine("\t\t\t\treturn false;");
                    newTexto.AppendLine("\t\t\t");
                    newTexto.AppendLine("\t\t\treturn first.GetHashCode() == second.GetHashCode();");
                    newTexto.AppendLine("\t\t}");
                    newTexto.AppendLine("\t\t");
                    newTexto.AppendLine("\t\t/// <summary>");
                    newTexto.AppendLine("\t\t/// Verifica que dos objetos sean distintos");
                    newTexto.AppendLine("\t\t/// </summary>");
                    newTexto.AppendLine("\t\tpublic static bool operator !=(" + misParams.strNameClase + " first, " + misParams.strNameClase + " second)");
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\treturn !(first == second);");
                    newTexto.AppendLine("\t\t}");
                    newTexto.AppendLine("\t\t");
                    newTexto.AppendLine("\t\t/// <summary>");
                    newTexto.AppendLine("\t\t/// Compara este objeto con otro");
                    newTexto.AppendLine("\t\t/// </summary>");
                    newTexto.AppendLine("\t\t/// <param name=\"obj\">El objeto a comparar</param>");
                    newTexto.AppendLine("\t\t/// <returns>Devuelve Verdadero si ambos objetos son iguales</returns>");
                    newTexto.AppendLine("\t\tpublic override bool Equals(object obj)");
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\tif (obj == null)");
                    newTexto.AppendLine("\t\t\t\treturn false;");
                    newTexto.AppendLine("\t\t\t");
                    newTexto.AppendLine("\t\t\tif (obj.GetType() == this.GetType())");
                    newTexto.AppendLine("\t\t\t\treturn obj.GetHashCode() == this.GetHashCode();");
                    newTexto.AppendLine("\t\t\t");
                    newTexto.AppendLine("\t\t\treturn false;");
                    newTexto.AppendLine("\t\t}");
                    newTexto.AppendLine("\t\t");

                    newTexto.AppendLine("\t\t#endregion");
                    newTexto.AppendLine("\t\t");

                    if ((misParams.bEntidadesEncriptacion))
                    {
                        newTexto.AppendLine("\t\t#region Encriptacion");
                        newTexto.AppendLine("\t\t");
                        newTexto.AppendLine("\t\t/// <summary>");
                        newTexto.AppendLine("\t\t/// Un valor unico que identifica cada instancia de este objeto");
                        newTexto.AppendLine("\t\t/// </summary>");
                        newTexto.AppendLine("\t\t/// <returns>Unico valor entero</returns>");
                        newTexto.AppendLine("\t\tpublic override int GetHashCode()");
                        newTexto.AppendLine("\t\t{");
                        newTexto.AppendLine("\t\t\treturn (this.GetSHA512Hash().GetHashCode());");
                        newTexto.AppendLine("\t\t}");
                        newTexto.AppendLine("\t\t");

                        newTexto.AppendLine("\t\t/// <summary>");
                        newTexto.AppendLine("\t\t///     Obtiene el Hash de la Instancia Actual");
                        newTexto.AppendLine("\t\t/// </summary>");
                        newTexto.AppendLine("\t\t/// <typeparam name=\"T\">");
                        newTexto.AppendLine("\t\t///     Tipo de Proveedor de servicios criptográficos");
                        newTexto.AppendLine("\t\t/// </typeparam>");
                        newTexto.AppendLine("\t\t/// <returns>");
                        newTexto.AppendLine("\t\t///     String en Base64 que representa el Hash");
                        newTexto.AppendLine("\t\t/// </returns>");
                        newTexto.AppendLine("\t\tpublic string GetHash<T>() where T : HashAlgorithm, new()");
                        newTexto.AppendLine("\t\t{");
                        newTexto.AppendLine("\t\t\tT cryptoServiceProvider = new T();");
                        newTexto.AppendLine("\t\t\treturn computeHash(cryptoServiceProvider);");
                        newTexto.AppendLine("\t\t}");
                        newTexto.AppendLine("\t\t");

                        newTexto.AppendLine("\t\t/// <summary>");
                        newTexto.AppendLine("\t\t///     Obtiene el codigo Hash en Base al Algoritmo SHA1");
                        newTexto.AppendLine("\t\t/// </summary>");
                        newTexto.AppendLine("\t\t/// <returns>");
                        newTexto.AppendLine("\t\t///     String en Base64 que representa el Hash en SHA1");
                        newTexto.AppendLine("\t\t/// </returns>");
                        newTexto.AppendLine("\t\tpublic string GetSHA1Hash()");
                        newTexto.AppendLine("\t\t{");
                        newTexto.AppendLine("\t\t\treturn GetHash<SHA1CryptoServiceProvider>();");
                        newTexto.AppendLine("\t\t}");
                        newTexto.AppendLine("\t\t");

                        newTexto.AppendLine("\t\t/// <summary>");
                        newTexto.AppendLine("\t\t///     Obtiene el codigo Hash en Base al Algoritmo SHA512");
                        newTexto.AppendLine("\t\t/// </summary>");
                        newTexto.AppendLine("\t\t/// <returns>");
                        newTexto.AppendLine("\t\t///     String en Base64 que representa el Hash en SHA512");
                        newTexto.AppendLine("\t\t/// </returns>");
                        newTexto.AppendLine("\t\tpublic string GetSHA512Hash()");
                        newTexto.AppendLine("\t\t{");
                        newTexto.AppendLine("\t\t\treturn GetHash<SHA512CryptoServiceProvider>();");
                        newTexto.AppendLine("\t\t}");
                        newTexto.AppendLine("\t\t");

                        newTexto.AppendLine("\t\t/// <summary>");
                        newTexto.AppendLine("\t\t///     Obtiene el codigo Hash en Base al Algoritmo MD5");
                        newTexto.AppendLine("\t\t/// </summary>");
                        newTexto.AppendLine("\t\t/// <returns>");
                        newTexto.AppendLine("\t\t///     String en Base64 que representa el Hash en MD5");
                        newTexto.AppendLine("\t\t/// </returns>");
                        newTexto.AppendLine("\t\tpublic string GetMD5Hash()");
                        newTexto.AppendLine("\t\t{");
                        newTexto.AppendLine("\t\t\treturn GetHash<MD5CryptoServiceProvider>();");
                        newTexto.AppendLine("\t\t}");
                        newTexto.AppendLine("\t\t");
                        newTexto.AppendLine("\t\t");
                        newTexto.AppendLine("\t\t#endregion");
                        newTexto.AppendLine("\t\t");
                    }

                    if (misParams.bUseDataAnnotations)
                    {
                        newTexto.AppendLine("\t\t#region DataAnnotations");
                        newTexto.AppendLine("\t\t");

                        newTexto.AppendLine("\t\t/// <summary>");
                        newTexto.AppendLine("\t\t/// Obtiene los errores basado en los DataAnnotations ");
                        newTexto.AppendLine("\t\t/// </summary>");
                        newTexto.AppendLine("\t\t/// <returns>Devuelve un IList del tipo ValidationResult con los errores obtenidos</returns>");
                        newTexto.AppendLine("\t\tpublic IList<ValidationResult> ValidationErrors()");
                        newTexto.AppendLine("\t\t{");
                        newTexto.AppendLine("\t\t\tValidationContext context = new ValidationContext(this, null, null);");
                        newTexto.AppendLine("\t\t\tIList<ValidationResult> errors = new List<ValidationResult>();");
                        newTexto.AppendLine("\t\t\t");
                        newTexto.AppendLine("\t\t\tif (!Validator.TryValidateObject(this, context, errors, true))");
                        newTexto.AppendLine("\t\t\t\treturn errors;");
                        newTexto.AppendLine("\t\t\t");
                        newTexto.AppendLine("\t\t\treturn new List<ValidationResult>(0);");
                        newTexto.AppendLine("\t\t}");
                        newTexto.AppendLine("\t\t");
                        newTexto.AppendLine("\t\t/// <summary>");
                        newTexto.AppendLine("\t\t/// Obtiene los errores basado en los DataAnnotations ");
                        newTexto.AppendLine("\t\t/// </summary>");
                        newTexto.AppendLine("\t\t/// <returns>Devuelve un String con los errores obtenidos</returns>");
                        newTexto.AppendLine("\t\tpublic string ValidationErrorsString()");
                        newTexto.AppendLine("\t\t{");
                        newTexto.AppendLine("\t\t\tstring strErrors = \"\";");
                        newTexto.AppendLine("\t\t\tValidationContext context = new ValidationContext(this, null, null);");
                        newTexto.AppendLine("\t\t\tIList<ValidationResult> errors = new List<ValidationResult>();");
                        newTexto.AppendLine("\t\t\t");
                        newTexto.AppendLine("\t\t\tif (!Validator.TryValidateObject(this, context, errors, true))");
                        newTexto.AppendLine("\t\t\t{");
                        newTexto.AppendLine("\t\t\t\tforeach (ValidationResult result in errors)");
                        newTexto.AppendLine("\t\t\t\t\tstrErrors = strErrors + result.ErrorMessage + Environment.NewLine;");
                        newTexto.AppendLine("\t\t\t}");
                        newTexto.AppendLine("\t\t\treturn strErrors;");
                        newTexto.AppendLine("\t\t}");
                        newTexto.AppendLine("\t\t");
                        newTexto.AppendLine("\t\t/// <summary>");
                        newTexto.AppendLine("\t\t/// Funcion que determina si un objeto es valido o no");
                        newTexto.AppendLine("\t\t/// </summary>");
                        newTexto.AppendLine("\t\t/// <returns>Resultado de la validacion</returns>");
                        newTexto.AppendLine("\t\tpublic bool IsValid()");
                        newTexto.AppendLine("\t\t{");
                        newTexto.AppendLine("\t\t\tValidationContext context = new ValidationContext(this, null, null);");
                        newTexto.AppendLine("\t\t\tIList<ValidationResult> errors = new List<ValidationResult>();");
                        newTexto.AppendLine("\t\t\t");
                        newTexto.AppendLine("\t\t\treturn Validator.TryValidateObject(this, context, errors, true);");
                        newTexto.AppendLine("\t\t}");
                        newTexto.AppendLine("\t\t");

                        newTexto.AppendLine("\t\t#endregion");
                        newTexto.AppendLine("\t\t");
                    }
                }
            }

            //newTexto.AppendLine(vbTab & " #region Encapsulamiento" )
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                PermiteNull = dtColumns.Rows[iCol]["PermiteNull"].ToString();
                DescripcionCol = dtColumns.Rows[iCol]["COL_DESC"].ToString();

                if (!misParams.bUseEventProperties)
                {
                    TipoColNull = tipoCol;
                    if (tipoCol.ToUpper() != "STRING")
                    {
                        if (PermiteNull == "Yes")
                        {
                            if (TipoColNull.Contains("[]"))
                            {
                                TipoColNull = tipoCol;
                            }
                            else
                            {
                                TipoColNull = tipoCol + "?";
                            }
                        }
                    }

                    newTexto.AppendLine("\t\t/// <summary>");
                    if (string.IsNullOrEmpty(DescripcionCol))
                    {
                        newTexto.AppendLine("\t\t/// Propiedad publica de tipo " + tipoCol + " que representa a la columna " + nameCol + " de la Tabla " + misParams.strNameTabla);
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t/// " + DescripcionCol.Replace("\r\n", ""));
                    }
                    newTexto.AppendLine("\t\t/// Permite Null: " + dtColumns.Rows[iCol]["PermiteNull"]);
                    newTexto.AppendLine("\t\t/// Es Calculada: " + dtColumns.Rows[iCol]["EsCalculada"]);
                    newTexto.AppendLine("\t\t/// Es RowGui: " + dtColumns.Rows[iCol]["EsRowGui"]);
                    newTexto.AppendLine("\t\t/// Es PrimaryKey: " + dtColumns.Rows[iCol]["EsPK"]);
                    newTexto.AppendLine("\t\t/// Es ForeignKey: " + dtColumns.Rows[iCol]["EsFK"]);
                    newTexto.AppendLine("\t\t/// </summary>");
                    if (misParams.bradWCF && misParams.bUseDataAnnotations)
                        newTexto.AppendLine("\t\t[DataMemberAttribute]");

                    if (misParams.bUseDataAnnotations)
                    {
                        switch (tipoCol.ToUpper())
                        {
                            case "STRING":
                                int iLongitud = 0;
                                if (int.TryParse(dtColumns.Rows[iCol]["LONGITUD"].ToString(), out iLongitud))
                                {
                                    if (iLongitud > 0)
                                    {
                                        newTexto.AppendLine("\t\t[StringLength(" + iLongitud.ToString() + ", MinimumLength=0)]");
                                    }
                                }
                                break;

                            case "INT":
                            case "INTEGER":
                                //newTexto.AppendLine("\t\t[RegularExpression(\"([0-9]+)\", ErrorMessage = \"La expresion no es un enero valido\")]");
                                newTexto.AppendLine("\t\t[Range(0, int.MaxValue, ErrorMessage = \"Por favor ingresa un valor numérico válido\")]");
                                break;

                            case "FLOAT":
                                newTexto.AppendLine("\t\t[Range(0, float.MaxValue, ErrorMessage = \"Por favor ingresa un valor numérico válido\")]");
                                break;

                            case "DOUBLE":
                                newTexto.AppendLine("\t\t[Range(0, double.MaxValue, ErrorMessage = \"Por favor ingresa un valor numérico válido\")]");
                                break;

                            case "DATE":
                                newTexto.AppendLine("\t\t[DataType(DataType.Date, ErrorMessage = \"Fecha invalida\")]");
                                newTexto.AppendLine("\t\t[DisplayFormat(DataFormatString = \"{ 0:dd/MM/yyyy}\", ApplyFormatInEditMode = true)]");
                                break;

                            case "DATETIME":
                                newTexto.AppendLine("\t\t[DataType(DataType.DateTime, ErrorMessage = \"Fecha invalida\")]");
                                newTexto.AppendLine("\t\t[DisplayFormat(DataFormatString = \"{ 0:dd/MM/yyyy HH:mm:ss.ffffff}\", ApplyFormatInEditMode = true)]");
                                break;
                        }

                        if (string.IsNullOrEmpty(DescripcionCol))
                        {
                            newTexto.AppendLine("\t\t[Display(Name = \"" + nameCol + "\", Description = \" Propiedad publica de tipo " + tipoCol + " que representa a la columna " + nameCol + " de la Tabla " + misParams.strNameTabla + "\")]");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\t[Display(Name = \"" + nameCol + "\", Description = \"" + DescripcionCol.Replace("\r\n", "") + "\")]");
                        }

                        if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("api") & nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("estado"))
                        {
                            if (misParams.miEntorno != TipoEntorno.CSharp_NetCore_V2
                                && misParams.miEntorno != TipoEntorno.CSharp_NetCore_WebAPI_V2
                                && misParams.miEntorno != TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger)
                                newTexto.AppendLine("\t\t[EnumDataType(typeof(" + misParams.strParamClaseApi + ".Estado))]");
                        }
                        else if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("api") & nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("transaccion"))
                        {
                            if (misParams.miEntorno != TipoEntorno.CSharp_NetCore_V2
                                && misParams.miEntorno != TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger
                                && misParams.miEntorno != TipoEntorno.CSharp_NetCore_WebAPI_V2)
                                newTexto.AppendLine("\t\t[EnumDataType(typeof(" + misParams.strParamClaseApi + ".Transaccion))]");
                        }
                        else if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("usu") & nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("cre"))
                        {
                        }
                        else if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("usu") & nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("mod"))
                        {
                        }
                        else if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("fec") & nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("cre"))
                        {
                        }
                        else if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("fec") & nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("mod"))
                        {
                        }
                        else
                        {
                            if (PermiteNull == "No")
                            {
                                if (tipoCol.ToUpper() == "STRING")
                                {
                                    newTexto.AppendLine("\t\t[Required(AllowEmptyStrings = true, ErrorMessage = \"Se necesita un valor para -" + nameCol + "- porque es un campo requerido.\")]");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t[Required(ErrorMessage = \"Se necesita un valor para -" + nameCol + "- porque es un campo requerido.\")]");
                                }
                            }
                        }

                        if (dtColumns.Rows[iCol]["EsPK"].ToString() == "Yes")
                        {
                            newTexto.AppendLine("\t\t[Key]");
                        }
                    }

                    newTexto.AppendLine("\t\tpublic " + TipoColNull + " " + nameCol + " { get; set; } \r\n");
                }
                else
                {
                    TipoColNull = tipoCol;
                    if (tipoCol.ToUpper() != "STRING")
                    {
                        if (PermiteNull == "Yes")
                        {
                            if (TipoColNull.Contains("[]"))
                            {
                                TipoColNull = tipoCol;
                            }
                            else
                            {
                                TipoColNull = tipoCol + "?";
                            }
                        }
                    }
                    newTexto.AppendLine("\t\t/// <summary>");
                    newTexto.AppendLine("\t\t/// \t Variable local de tipo " + tipoCol + " que representa a la columna " + nameCol + " de la Tabla " + misParams.strNameTabla);
                    newTexto.AppendLine("\t\t/// </summary>");
                    newTexto.AppendLine("\t\tprivate " + TipoColNull + " _" + nameCol.ToLower() + ";");
                    newTexto.AppendLine("\t\t/// <summary>");
                    if (string.IsNullOrEmpty(DescripcionCol))
                    {
                        newTexto.AppendLine("\t\t/// \t Propiedad publica de tipo " + tipoCol + " que representa a la columna " + nameCol + " de la Tabla " + misParams.strNameTabla);
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t/// \t " + DescripcionCol.Replace("\r\n", ""));
                    }
                    newTexto.AppendLine("\t\t/// \t Permite Null: " + dtColumns.Rows[iCol]["PermiteNull"]);
                    newTexto.AppendLine("\t\t/// \t Es Calculada: " + dtColumns.Rows[iCol]["EsCalculada"]);
                    newTexto.AppendLine("\t\t/// \t Es RowGui: " + dtColumns.Rows[iCol]["EsRowGui"]);
                    newTexto.AppendLine("\t\t/// \t Es PrimaryKey: " + dtColumns.Rows[iCol]["EsPK"]);
                    newTexto.AppendLine("\t\t/// \t Es ForeignKey: " + dtColumns.Rows[iCol]["EsFK"]);
                    newTexto.AppendLine("\t\t/// </summary>");
                    if (misParams.bUseDataAnnotations)
                    {
                        switch (tipoCol.ToUpper())
                        {
                            case "STRING":
                                int iLongitud = 0;
                                if (int.TryParse(dtColumns.Rows[iCol]["LONGITUD"].ToString(), out iLongitud))
                                {
                                    if (iLongitud > 0)
                                    {
                                        newTexto.AppendLine("\t\t[StringLength(" + iLongitud.ToString() + ", MinimumLength=0)]");
                                    }
                                }

                                break;
                        }
                        if (string.IsNullOrEmpty(DescripcionCol))
                        {
                            newTexto.AppendLine("\t\t[Display(Name = \"" + nameCol + "\", Description = \" Propiedad publica de tipo " + tipoCol + " que representa a la columna " + nameCol + " de la Tabla " + misParams.strNameTabla + "\")]");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\t[Display(Name = \"" + nameCol + "\", Description = \"" + DescripcionCol.Replace("\r\n", "") + "\")]");
                        }

                        if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("api") & nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("estado"))
                        {
                            newTexto.AppendLine("\t\t[EnumDataType(typeof(" + misParams.strParamClaseApi + ".Estado))]");
                        }
                        else if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("api") & nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("transaccion"))
                        {
                            newTexto.AppendLine("\t\t[EnumDataType(typeof(" + misParams.strParamClaseApi + ".Transaccion))]");
                        }
                        else if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("usu") & nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("cre"))
                        {
                        }
                        else if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("usu") & nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("mod"))
                        {
                        }
                        else if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("fec") & nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("cre"))
                        {
                        }
                        else if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("fec") & nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("mod"))
                        {
                        }
                        else
                        {
                            if (PermiteNull == "No")
                            {
                                if (tipoCol.ToUpper() == "STRING")
                                {
                                    newTexto.AppendLine("\t\t[Required(AllowEmptyStrings = true, ErrorMessage = \"Se necesita un valor para -" + nameCol + "- porque es un campo requerido.\")]");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t[Required(ErrorMessage = \"Se necesita un valor para -" + nameCol + "- porque es un campo requerido.\")]");
                                }
                            }
                        }

                        if (dtColumns.Rows[iCol]["EsPK"].ToString() == "Yes")
                        {
                            newTexto.AppendLine("\t\t[Key]");
                        }
                    }

                    newTexto.AppendLine("\t\tpublic " + TipoColNull + " " + nameCol);
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\tget {return _" + nameCol.ToLower() + ";}");
                    if (misParams.bUseEventProperties)
                    {
                        newTexto.AppendLine("\t\t\tset");
                        newTexto.AppendLine("\t\t\t{");
                        newTexto.AppendLine("\t\t\t\tif (_" + nameCol.ToLower() + " != value)");
                        newTexto.AppendLine("\t\t\t\t{");
                        newTexto.AppendLine("\t\t\t\t\tRaisePropertyChanging(Fields." + nameCol + ".ToString());");
                        newTexto.AppendLine("\t\t\t\t\t_" + nameCol.ToLower() + " = value;");
                        newTexto.AppendLine("\t\t\t\t\tRaisePropertyChanged(Fields." + nameCol + ".ToString());");
                        newTexto.AppendLine("\t\t\t\t}");
                        newTexto.AppendLine("\t\t\t}");
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t\tset {_" + nameCol + " = value;}");
                    }

                    newTexto.AppendLine("\t\t}\r\n");
                    newTexto.AppendLine("");
                }
            }
            //newTexto.AppendLine(vbTab & " #endregion" & vbCrLf )

            if (!misParams.bUseBaseClass)
            {
                if (misParams.bUseEventProperties)
                {
                    newTexto.AppendLine("\t\t#region Eventos");
                    newTexto.AppendLine("\t\t");
                    newTexto.AppendLine("\t\tpublic event PropertyChangedEventHandler PropertyChanged;");
                    newTexto.AppendLine("\t\tprivate void RaisePropertyChanged(string prop)");
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\tif (PropertyChanged != null)");
                    newTexto.AppendLine("\t\t\t\tPropertyChanged(this, new PropertyChangedEventArgs(prop));");
                    newTexto.AppendLine("\t\t}");
                    newTexto.AppendLine("\t\t");
                    newTexto.AppendLine("\t\tpublic event PropertyChangingEventHandler PropertyChanging;");
                    newTexto.AppendLine("\t\tprivate void RaisePropertyChanging(string prop)");
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\tif (PropertyChanging != null)");
                    newTexto.AppendLine("\t\t\t\tPropertyChanging(this, new PropertyChangingEventArgs(prop));");
                    newTexto.AppendLine("\t\t}");
                    newTexto.AppendLine("\t\t");
                    newTexto.AppendLine("\t\t#endregion");
                }
            }

            newTexto.AppendLine("\t}");
            newTexto.AppendLine("}");
            CFunciones.CrearArchivo(newTexto.ToString(), misParams.strNameClase + ".cs", PathDesktop + "\\" + misParams.strNamespaceEntidades + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + misParams.strNamespaceEntidades + "\\" + misParams.strNameClase + ".cs";
        }

        public string CrearEntidadesFeatherJsSequelize(DataTable dtColumns, ref cEntidadesParametros misParams)
        {
            string nameCol = null;
            string tipoCol = null;

            bool esPrimerCampo = true;
            string PathDesktop = null;
            if (misParams.bMoverResultados)
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + misParams.strInfProyecto;
            else
                PathDesktop = misParams.strPathProyecto + "\\" + misParams.strInfProyecto;

            if (Directory.Exists(PathDesktop + "\\src\\models") == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\src\\models");
            }

            //Estandar de Sequelizae
            misParams.strNameTabla = misParams.strNameTabla.ToLower();

            //Empezamos con el archivo
            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            newTexto.AppendLine("// See http://docs.sequelizejs.com/en/latest/docs/models-definition/");
            newTexto.AppendLine("// for more of what you can do here.");
            newTexto.AppendLine("const Sequelize = require('sequelize');");
            newTexto.AppendLine("");
            newTexto.AppendLine("module.exports = function (app) {");
            newTexto.AppendLine("\tconst sequelizeClient = app.get('sequelizeClient');");
            newTexto.AppendLine("\tconst " + misParams.strNameTabla + " = sequelizeClient.define('messages', {");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                if (esPrimerCampo)
                {
                    newTexto.AppendLine("\t\t" + nameCol + ": {");
                    newTexto.AppendLine("\t\t\ttype: Sequelize." + tipoCol.ToUpper() + ",");
                    newTexto.AppendLine("\t\t\tallowNull: false");
                    newTexto.AppendLine("\t\t}");
                    esPrimerCampo = false;
                }
                else
                {
                    newTexto.AppendLine("\t\t,");
                    newTexto.AppendLine("\t\t" + nameCol + ": {");
                    newTexto.AppendLine("\t\t\ttype: Sequelize." + tipoCol.ToUpper() + ",");
                    newTexto.AppendLine("\t\t\tallowNull: false");
                    newTexto.AppendLine("\t\t}");
                }
            }

            newTexto.AppendLine("\t}, {");
            newTexto.AppendLine("\t\thooks: {");
            newTexto.AppendLine("\t\t\tbeforeCount(options) {");
            newTexto.AppendLine("\t\t\t\toptions.raw = true;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t});");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t" + misParams.strNameTabla + ".associate = function (models) { // eslint-disable-line no-unused-vars");
            newTexto.AppendLine("\t\t// Define associations here");
            newTexto.AppendLine("\t\t// See http://docs.sequelizejs.com/en/latest/docs/associations/");
            newTexto.AppendLine("\t};");
            newTexto.AppendLine("");
            newTexto.AppendLine("\treturn " + misParams.strNameTabla + ";");
            newTexto.AppendLine("};");

            CFunciones.CrearArchivo(newTexto.ToString(), misParams.strNameTabla.ToLower() + ".model.js", PathDesktop + "\\src\\models\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\src\\models\\" + misParams.strNameTabla.ToLower() + ".model.js";
        }

        public string CrearEntidadesJava(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string nameCol = null;
            string tipoCol = null;
            

            string pNameTabla = pNameClase.Replace("_", "").ToLower();
            //pNameTabla = pNameTabla.Substring(0, 1).ToUpper & pNameTabla.Substring(1).ToLower
            
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text +
                                  pNameClase.Replace("_", "").Substring(0, 1).ToUpper() +
                                  pNameClase.Replace("_", "").Substring(1);            

            bool SetPropertiesInARow = true;
            string PermiteNull = null;
            string TipoColNull = null;
            string DescripcionCol = null;
            bool esPrimerCampo = true;
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase).Replace("_", "").Replace(".", "\\")) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase).Replace("_", "").Replace(".","\\"));
            }

            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase).Replace("#region", "").Replace("#endregion", "");
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que define un objeto para la Tabla " + pNameTabla);

            //newTexto.AppendLine(CabeceraTmp)
            newTexto.AppendLine("package " + formulario.txtInfProyectoClass.Text + "."+ formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase).Replace("_", "") + ";");
            newTexto.AppendLine("");
            newTexto.AppendLine("import com.fasterxml.jackson.annotation.JsonInclude;");
            newTexto.AppendLine("import lombok.Data;");
            newTexto.AppendLine("import java.util.Date;");            
            newTexto.AppendLine("");
            newTexto.AppendLine("/*! ");
            newTexto.AppendLine(" *  Entity class for " + pNameTabla);
            newTexto.AppendLine(" *  @author " + formulario.txtInfAutor.Text);
            newTexto.AppendLine(" *  @version 1.0");
            newTexto.AppendLine(" */");
            newTexto.AppendLine("");
            newTexto.AppendLine("@Data");
            newTexto.AppendLine("@JsonInclude(JsonInclude.Include.NON_NULL)");
            newTexto.AppendLine("public class " + pNameClaseEnt);
            newTexto.AppendLine("{");

            //ENUM
            /*
            newTexto.AppendLine("\tpublic final static String NOMBRE_TABLA = \"" + pNameTabla + "\";");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic enum Fields");
            newTexto.AppendLine("\t{");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                if (esPrimerCampo)
                {
                    newTexto.AppendLine("\t\t" + nameCol);
                    esPrimerCampo = false;
                }
                else
                {
                    newTexto.AppendLine("\t\t," + nameCol);
                }
            }
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("");
            */
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                PermiteNull = dtColumns.Rows[iCol]["PermiteNull"].ToString();
                DescripcionCol = dtColumns.Rows[iCol]["COL_DESC"].ToString();

                newTexto.AppendLine("\tprivate " + tipoCol + " " + nameCol + ";");                
            }
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tprivate String statusDescription;");
            newTexto.AppendLine("}");
            
            CFunciones.CrearArchivo(newTexto.ToString(), pNameClaseEnt + ".java", PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase).Replace("_", "").Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase).Replace("_", "").Replace(".", "\\") + "\\" + pNameClaseEnt + ".java";
        }

        public string CrearEntidadesJavaAndroid(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string nameCol = null;
            string tipoCol = null;

            string pNameTabla = pNameClase.Replace("_", "");
            pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() +
                         pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();
            //pNameTabla = pNameTabla.Substring(0, 1).ToUpper & pNameTabla.Substring(1).ToLower

            pNameClase = formulario.txtPrefijoEntidades.Text + pNameTabla;

            bool SetPropertiesInARow = true;
            string PermiteNull = null;
            string TipoColNull = null;
            string DescripcionCol = null;
            bool esPrimerCampo = true;
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            else
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase)) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase));
            }

            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase).Replace("#region", "").Replace("#endregion", "");
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que define un objeto para la Tabla " + pNameTabla);

            //newTexto.AppendLine(CabeceraTmp)
            newTexto.AppendLine("package " + formulario.txtInfProyectoClass.Text + ";");
            newTexto.AppendLine("");
            newTexto.AppendLine("import java.util.Date;");
            newTexto.AppendLine("import java.util.Calendar;");
            newTexto.AppendLine("");
            newTexto.AppendLine("/*! ");
            newTexto.AppendLine(" *  Clase que contienen todas las propiedades de la Tabla " + pNameTabla);
            newTexto.AppendLine(" *  @author <A href=\"mailto:wmayta@gmail.com\">William Mayta Chura</A>");
            newTexto.AppendLine(" *  @author <A href=\"mailto:7.re.al.7@gmail.com\">Reynaldo Alonzo Vera Arias</A>");
            newTexto.AppendLine(" *  @version 1.0");
            newTexto.AppendLine(" */");
            newTexto.AppendLine("");
            newTexto.AppendLine("public class " + pNameClase);
            newTexto.AppendLine("{");

            //ENUM
            TipoEntorno miEntorno;
            Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);
            if (miEntorno == TipoEntorno.CSharp_WCF)
                newTexto.AppendLine("\t\t[DataContract]");
            newTexto.AppendLine("\tpublic enum Fields");
            newTexto.AppendLine("\t{");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                if (esPrimerCampo)
                {
                    newTexto.AppendLine("\t\t" + nameCol);
                    esPrimerCampo = false;
                }
                else
                {
                    newTexto.AppendLine("\t\t," + nameCol);
                }
            }
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                PermiteNull = dtColumns.Rows[iCol]["PermiteNull"].ToString();
                DescripcionCol = dtColumns.Rows[iCol]["COL_DESC"].ToString();

                newTexto.AppendLine("\tprivate " + tipoCol + " _" + nameCol + ";");
                newTexto.AppendLine("\tpublic " + tipoCol + " get" + nameCol + "()");
                newTexto.AppendLine("\t{");
                newTexto.AppendLine("\t\treturn this._" + nameCol + ";");
                newTexto.AppendLine("\t}");
                newTexto.AppendLine("\tpublic void set" + nameCol + "(" + tipoCol + " value)");
                newTexto.AppendLine("\t{");
                newTexto.AppendLine("\t\tthis._" + nameCol + " = value;");
                newTexto.AppendLine("\t}");

                if (dtColumns.Rows[iCol]["tipo_col"].ToString().ToUpper().StartsWith("DATE"))
                {
                    newTexto.AppendLine("");
                    newTexto.AppendLine("\tpublic Date get" + nameCol + "_format()");
                    newTexto.AppendLine("\t{");
                    newTexto.AppendLine("\t\treturn new Date(this._" + nameCol + ");");
                    newTexto.AppendLine("\t}");
                    newTexto.AppendLine("");
                    newTexto.AppendLine("\tpublic Calendar get" + nameCol + "_cal()");
                    newTexto.AppendLine("\t{");
                    newTexto.AppendLine("\t\tDate miFecha = new Date(this._" + nameCol + ");");
                    newTexto.AppendLine("\t\tCalendar cal = Calendar.getInstance();");
                    newTexto.AppendLine("\t\tcal.setTime(miFecha);");
                    newTexto.AppendLine("\t\t");
                    newTexto.AppendLine("\t\treturn cal;");
                    newTexto.AppendLine("\t}");
                }
                newTexto.AppendLine("");
            }

            newTexto.AppendLine("\t//private ArrayList<entTablaHijo> _TablaHijo;");
            newTexto.AppendLine("\t//public ArrayList<entTablaHijo> getTablaHijo()");
            newTexto.AppendLine("\t//{");
            newTexto.AppendLine("\t//\treturn this._TablaHijo;");
            newTexto.AppendLine("\t//}");
            newTexto.AppendLine("\t//public void setTablaHijo(ArrayList<entTablaHijo> value)");
            newTexto.AppendLine("\t//{");
            newTexto.AppendLine("\t//\tthis._TablaHijo = value;");
            newTexto.AppendLine("\t//}");

            newTexto.AppendLine("}");
            CFunciones.CrearArchivo(newTexto.ToString(), pNameClase + ".java", PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase) + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase) + "\\" + pNameClase + ".java";
        }

        public string CrearEntidadesPhp(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string nameCol = null;
            string tipoCol = null;

            string pNameTabla = pNameClase.Replace("_", "");
            pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() +
                         pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();

            pNameClase = formulario.txtPrefijoEntidades.Text + pNameTabla;

            bool SetPropertiesInARow = true;
            string PermiteNull = null;
            string TipoColNull = null;
            string DescripcionCol = null;
            bool esPrimerCampo = true;
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase)) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase));
            }

            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase).Replace("#region", "").Replace("#endregion", "");
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que define un objeto para la Tabla " + pNameTabla);

            newTexto.AppendLine("<?php");
            newTexto.AppendLine(CabeceraTmp);
            newTexto.AppendLine("");

            //ENUM
            newTexto.AppendLine("class " + pNameClase + "Fields");
            newTexto.AppendLine("{");
            newTexto.AppendLine("\tconst NAME_TABLE = '" + pNameTabla + "';");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                if (esPrimerCampo)
                {
                    newTexto.AppendLine("\tconst " + nameCol + " = '" + nameCol + "';");
                    esPrimerCampo = false;
                }
                else
                {
                    newTexto.AppendLine("\t,const " + nameCol + " = '" + nameCol + "';");
                }
            }
            newTexto.AppendLine("}");
            newTexto.AppendLine("");

            newTexto.AppendLine("class " + pNameClase + "Fields");
            newTexto.AppendLine("{");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                newTexto.AppendLine("\t// \t Propiedad publica de tipo " + tipoCol + " que representa a la columna " + nameCol + " de la Tabla " + pNameTabla);
                newTexto.AppendLine("\tpublic $" + nameCol + ";");
                newTexto.AppendLine("");
            }
            newTexto.AppendLine("}");
            newTexto.AppendLine("");

            CFunciones.CrearArchivo(newTexto.ToString(), pNameClase + ".java", PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase) + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase) + "\\" + pNameClase + ".java";
        }

        public string CrearEntidadesPython(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string nameCol = null;
            string tipoCol = null;

            string pNameTabla = pNameClase.Replace("_", "");
            pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() +
                         pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();

            pNameClase = formulario.txtPrefijoEntidades.Text + pNameTabla;

            bool SetPropertiesInARow = true;
            string PermiteNull = null;
            string TipoColNull = null;
            string DescripcionCol = null;
            bool esPrimerCampo = true;
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase)) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase));
            }

            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase).Replace("#region", "").Replace("#endregion", "");
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que define un objeto para la Tabla " + pNameTabla);
            string Creador = (string.IsNullOrEmpty(formulario.txtInfAutor.Text) ? "Automatico     " : formulario.txtInfAutor.Text);
            string FC = DateTime.Now.ToString("dd/MM/yyyy");

            //ENUM
            newTexto.AppendLine("__author__ = '" + Creador + "'");
            newTexto.AppendLine("__copyright__ = 'Copyright " + DateTime.Now.ToString("yyyy") + "'");
            newTexto.AppendLine("__date__ = '" + FC + "'");
            newTexto.AppendLine("__version__ = '1.0.0'");
            newTexto.AppendLine("");
            newTexto.AppendLine("class " + pNameClase + "Fields:");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t" + nameCol + " = '" + nameCol + "'");
            }
            newTexto.AppendLine("");
            newTexto.AppendLine("from " + formulario.txtNamespaceNegocios.Text + "." + formulario.txtClaseParametros.Text + " import " + formulario.txtClaseParametros.Text + "");
            newTexto.AppendLine("");
            newTexto.AppendLine("");

            newTexto.AppendLine("class " + pNameClase + "(object):");
            newTexto.AppendLine("\t\"\"\"Clase que define un objeto para la Tabla " + pNameTabla + "\"\"\"");
            newTexto.AppendLine("\tNAME_TABLE = cParametros.N_BD_SCHEMA + '" + pNameTabla + "'");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                newTexto.AppendLine("");
                newTexto.AppendLine("\t__" + nameCol + " = \"\"");
                newTexto.AppendLine("\t@property");
                newTexto.AppendLine("\tdef " + nameCol + "(self):");
                newTexto.AppendLine("\t\treturn self.__" + nameCol);
                newTexto.AppendLine("\t@" + nameCol + ".setter");
                newTexto.AppendLine("\tdef " + nameCol + "(self, val):");
                newTexto.AppendLine("\t\tself.__" + nameCol + "= val");
                newTexto.AppendLine("");
            }
            newTexto.AppendLine("");

            CFunciones.CrearArchivo(newTexto.ToString(), pNameClase + ".py", PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase) + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase) + "\\" + pNameClase + ".py";
        }

        public string CrearEntidadesVB(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string paramClaseApi = (string.IsNullOrEmpty(formulario.txtClaseParametrosClaseApi.Text) ? "cApi" : formulario.txtClaseParametrosClaseApi.Text);
            string nameCol = null;
            string tipoCol = null;
            bool bUseDataAnnotations = formulario.chkDataAnnotations.Checked;

            string pNameTabla = pNameClase.Replace("_", "");
            pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() +
                         pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();

            pNameClase = formulario.txtPrefijoEntidades.Text + pNameTabla;

            bool SetPropertiesInARow = true;
            string PermiteNull = null;
            string TipoColNull = null;
            string DescripcionCol = null;
            bool esPrimerCampo = true;
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase)) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase));
            }

            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase);
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que define un objeto para la Tabla " + pNameTabla);

            //newTexto.AppendLine(CabeceraTmp)
            newTexto.AppendLine("#Region \"Imports\"");
            newTexto.AppendLine("Imports System");
            newTexto.AppendLine("Imports System.Collections.Generic");
            newTexto.AppendLine("Imports System.ComponentModel.DataAnnotations");
            newTexto.AppendLine("Imports System.IO");
            newTexto.AppendLine("Imports System.Reflection");
            newTexto.AppendLine("Imports System.Security.Cryptography");
            newTexto.AppendLine("Imports System.Text");
            newTexto.AppendLine("Imports System.Xml.Serialization");
            newTexto.AppendLine("#End Region");

            newTexto.AppendLine("");
            //newTexto.AppendLine("Namespace " & namespaceEntidades)

            newTexto.AppendLine("\tPublic Class " + pNameClase);

            //ENUM
            newTexto.AppendLine("\t\tPublic Enum Fields");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                newTexto.AppendLine("\t\t\t" + nameCol);
            }
            newTexto.AppendLine("\r\n\t\tEnd Enum");
            newTexto.AppendLine("\t\t");

            //Ahora los metodos de DataContract

            newTexto.AppendLine("\t\t''' <summary>");
            newTexto.AppendLine("\t\t''' Devuelve un String que representa al Objeto");
            newTexto.AppendLine("\t\t''' </summary>");
            newTexto.AppendLine("\t\t''' <returns>string</returns>");
            newTexto.AppendLine("\t\tPublic Overrides Function ToString() As String");
            newTexto.AppendLine("\t\t\tDim hashString As String");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\t'Evitar parametros NULL");
            newTexto.AppendLine("\t\t\tIf (Me Is Nothing) Then");
            newTexto.AppendLine("\t\t\t\tThrow New ArgumentNullException(\"Parametro NULL no es valido\")");
            newTexto.AppendLine("\t\t\tEnd If");
            newTexto.AppendLine("\t\t\t'Se verifica si el objeto es serializable.");
            newTexto.AppendLine("\t\t\tTry");
            newTexto.AppendLine("\t\t\t\tDim memStream As MemoryStream = New MemoryStream");
            newTexto.AppendLine("\t\t\t\tDim serializer As XmlSerializer = New XmlSerializer(GetType(" + pNameClase + "))");
            newTexto.AppendLine("\t\t\t\tserializer.Serialize(memStream, Me)");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t'Ahora se obtiene el Hash del Objeto.");
            newTexto.AppendLine("\t\t\t\thashString = ComputeHash(memStream.ToArray)");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tReturn hashString");
            newTexto.AppendLine("\t\t\tCatch ame As AmbiguousMatchException");
            newTexto.AppendLine("\t\t\t\tThrow New ApplicationException(\"El Objeto no es Serializable. Message:\" + ame.Message)");
            newTexto.AppendLine("\t\t\tEnd Try");
            newTexto.AppendLine("\t\tEnd Function");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t''' <summary>");
            newTexto.AppendLine("\t\t''' Obtiene el Hash a partir de un array de Bytes");
            newTexto.AppendLine("\t\t''' </summary>");
            newTexto.AppendLine("\t\t''' <param name=\"objectAsBytes\"></param>");
            newTexto.AppendLine("\t\t''' <returns>string</returns>");
            newTexto.AppendLine("\t\tPrivate Shared Function ComputeHash(ByVal objectAsBytes() As Byte) As String");
            newTexto.AppendLine("\t\t\tDim md5 As MD5 = New MD5CryptoServiceProvider");
            newTexto.AppendLine("\t\t\tTry");
            newTexto.AppendLine("\t\t\t\tDim result() As Byte = md5.ComputeHash(objectAsBytes)");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tDim sb As StringBuilder = New StringBuilder");
            newTexto.AppendLine("\t\t\t\tDim i As Integer = 0");
            newTexto.AppendLine("\t\t\t\tDo While (i < result.Length)");
            newTexto.AppendLine("\t\t\t\t\tsb.Append(result(i).ToString(\"X2\"))");
            newTexto.AppendLine("\t\t\t\t\ti = (i + 1)");
            newTexto.AppendLine("\t\t\t\tLoop");
            newTexto.AppendLine("\t\t\t\tReturn sb.ToString");
            newTexto.AppendLine("\t\t\tCatch ane As ArgumentNullException");
            newTexto.AppendLine("\t\t\t\tReturn Nothing");
            newTexto.AppendLine("\t\t\tEnd Try");
            newTexto.AppendLine("\t\tEnd Function");
            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t\t''' <summary>");
            newTexto.AppendLine("\t\t''' Un valor unico que identifica cada iunstancia de este objeto");
            newTexto.AppendLine("\t\t''' </summary>");
            newTexto.AppendLine("\t\t''' <returns>Unico valor entero</returns>");
            newTexto.AppendLine("\t\tPublic Overrides Function GetHashCode() As Integer");
            bool bPrimerElemento = true;
            string strHash = "";
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                if (dtColumns.Rows[iCol]["EsPK"].ToString() == "Yes")
                {
                    if (bPrimerElemento)
                    {
                        strHash = strHash + "Me." + nameCol + ".GetHashCode";
                        bPrimerElemento = false;
                    }
                    else
                    {
                        strHash = strHash + " + Me." + nameCol + ".GetHashCode";
                    }
                }
            }

            newTexto.AppendLine("\t\t\tReturn (" + (string.IsNullOrEmpty(strHash) ? "0" : strHash) + ")");
            newTexto.AppendLine("\t\tEnd Function");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t''' <summary>");
            newTexto.AppendLine("\t\t''' Verifica que dos objetos sean identicos");
            newTexto.AppendLine("\t\t''' </summary>");
            newTexto.AppendLine("\t\tPublic Overloads Shared Operator = (ByVal first As " + pNameClase + " , ByVal second As " + pNameClase + ") As Boolean");
            newTexto.AppendLine("\t\t\t' Verifica si el puntero en memoria es el mismo");
            newTexto.AppendLine("\t\t\tIf Object.ReferenceEquals(first, second) Then");
            newTexto.AppendLine("\t\t\t\tReturn true");
            newTexto.AppendLine("\t\t\tEnd If");
            newTexto.AppendLine("\t\t\t' Verifica valores nulos");
            newTexto.AppendLine("\t\t\tIf ((CType(first,Object) Is Nothing) OrElse (CType(second,Object) Is Nothing)) Then");
            newTexto.AppendLine("\t\t\t\tReturn false");
            newTexto.AppendLine("\t\t\tEnd If");
            newTexto.AppendLine("\t\t\tReturn (first.GetHashCode = second.GetHashCode)");
            newTexto.AppendLine("\t\tEnd Operator");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t''' <summary>");
            newTexto.AppendLine("\t\t''' Verifica que dos objetos sean distintos");
            newTexto.AppendLine("\t\t''' </summary>");
            newTexto.AppendLine("\t\tPublic Overloads Shared Operator <> (ByVal first As " + pNameClase + ", ByVal second As " + pNameClase + ") As Boolean");
            newTexto.AppendLine("\t\t\tReturn Not (first = second)");
            newTexto.AppendLine("\t\tEnd Operator");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t''' <summary>");
            newTexto.AppendLine("\t\t''' Compara este objeto con otro");
            newTexto.AppendLine("\t\t''' </summary>");
            newTexto.AppendLine("\t\t''' <param name=\"obj\">El objeto a comparar</param>");
            newTexto.AppendLine("\t\t''' <returns>Devuelve Verdadero si ambos objetos son iguales</returns>");
            newTexto.AppendLine("\t\tPublic Overrides Function Equals(ByVal obj As Object) As Boolean");
            newTexto.AppendLine("\t\t\tIf (obj Is Nothing) Then");
            newTexto.AppendLine("\t\t\t\tReturn false");
            newTexto.AppendLine("\t\t\tEnd If");
            newTexto.AppendLine("\t\t\tIf (obj.GetType = Me.GetType) Then");
            newTexto.AppendLine("\t\t\t\tReturn (obj.GetHashCode = Me.GetHashCode)");
            newTexto.AppendLine("\t\t\tEnd If");
            newTexto.AppendLine("\t\t\tReturn false");
            newTexto.AppendLine("\t\tEnd Function");
            newTexto.AppendLine("\t\t");

            if (bUseDataAnnotations)
            {
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' Obtiene los errores basado en los DataAnnotations ");
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <returns>Devuelve un IList del tipo ValidationResult con los errores obtenidos</returns>");
                newTexto.AppendLine("\t\tPublic Function ValidationErrors() As IList(Of ValidationResult)");
                newTexto.AppendLine("\t\t\tDim context As ValidationContext = New ValidationContext(Me, Nothing, Nothing)");
                newTexto.AppendLine("\t\t\tDim errors As IList(Of ValidationResult) = New List(Of ValidationResult)");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\tIf Not Validator.TryValidateObject(Me, context, errors, true) Then");
                newTexto.AppendLine("\t\t\t\tReturn errors");
                newTexto.AppendLine("\t\t\tEnd If");
                newTexto.AppendLine("\t\t\tReturn New List(Of ValidationResult)(0)");
                newTexto.AppendLine("\t\tEnd Function");
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' Obtiene los errores basado en los DataAnnotations ");
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <returns>Devuelve un String con los errores obtenidos</returns>");
                newTexto.AppendLine("\t\tPublic Function ValidationErrorsString() As String");
                newTexto.AppendLine("\t\t\tDim strErrors As String = \"\"");
                newTexto.AppendLine("\t\t\tDim context As ValidationContext = New ValidationContext(Me, Nothing, Nothing)");
                newTexto.AppendLine("\t\t\tDim errors As IList(Of ValidationResult) = New List(Of ValidationResult)");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\tIf Not Validator.TryValidateObject(Me, context, errors, true) Then");
                newTexto.AppendLine("\t\t\t\tFor Each result As ValidationResult In errors");
                newTexto.AppendLine("\t\t\t\t\tstrErrors = (strErrors + (result.ErrorMessage & Environment.NewLine))");
                newTexto.AppendLine("\t\t\t\tNext");
                newTexto.AppendLine("\t\t\tEnd If");
                newTexto.AppendLine("\t\t\tReturn strErrors");
                newTexto.AppendLine("\t\tEnd Function");
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' Funcion que determina si un objeto es valido o no");
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <returns>Resultado de la Validacion</returns>");
                newTexto.AppendLine("\t\tPublic Function IsValid() As Boolean");
                newTexto.AppendLine("\t\t\tDim context As ValidationContext = New ValidationContext(Me, Nothing, Nothing)");
                newTexto.AppendLine("\t\t\tDim errors As IList(Of ValidationResult) = New List(Of ValidationResult)");
                newTexto.AppendLine("\t\t\t");
                newTexto.AppendLine("\t\t\tReturn Validator.TryValidateObject(Me, context, errors, true)");
                newTexto.AppendLine("\t\tEnd Function");
                newTexto.AppendLine("\t\t");
            }

            //newTexto.AppendLine(vbTab & " #region Encapsulamiento" )
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                PermiteNull = dtColumns.Rows[iCol]["PermiteNull"].ToString();
                DescripcionCol = dtColumns.Rows[iCol]["COL_DESC"].ToString();

                if (SetPropertiesInARow == true)
                {
                    if (PermiteNull == "Yes")
                    {
                        if (tipoCol.ToUpper() != "STRING")
                        {
                            if (tipoCol.Contains("[]"))
                            {
                                tipoCol = tipoCol;
                            }
                            else
                            {
                                tipoCol = tipoCol + "?";
                            }
                        }
                    }

                    newTexto.AppendLine("\t\tPrivate _" + nameCol + " As " + tipoCol);
                    newTexto.AppendLine("\t\t''' <summary>");
                    if (string.IsNullOrEmpty(DescripcionCol))
                    {
                        newTexto.AppendLine("\t\t''' \t Propiedad publica de tipo " + tipoCol + " que representa a la columna " + nameCol + " de la Tabla " + pNameTabla);
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t''' \t " + DescripcionCol.Replace("\r\n", ""));
                    }
                    newTexto.AppendLine("\t\t''' \t Permite Null: " + dtColumns.Rows[iCol]["PermiteNull"]);
                    newTexto.AppendLine("\t\t''' \t Es Calculada: " + dtColumns.Rows[iCol]["EsCalculada"]);
                    newTexto.AppendLine("\t\t''' \t Es RowGui: " + dtColumns.Rows[iCol]["EsRowGui"]);
                    newTexto.AppendLine("\t\t''' \t Es PrimaryKey: " + dtColumns.Rows[iCol]["EsPK"]);
                    newTexto.AppendLine("\t\t''' \t Es ForeignKey: " + dtColumns.Rows[iCol]["EsFK"]);
                    newTexto.AppendLine("\t\t''' </summary>");

                    switch (tipoCol.ToUpper())
                    {
                        case "STRING":
                            break;
                            //newTexto.AppendLine(vbTab & vbTab & "[StringLength(60)]")
                    }
                    if (string.IsNullOrEmpty(DescripcionCol))
                    {
                        newTexto.AppendLine("\t\t<Display(Name:=\"" + nameCol + "\", Description:=\" Propiedad publica de tipo " + tipoCol + " que representa a la columna " + nameCol + " de la Tabla " + pNameTabla + "\")> _");
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t<Display(Name:=\"" + nameCol + "\", Description:=\"" + DescripcionCol.Replace("\r\n", "") + "\")> _");
                    }

                    if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                    {
                        newTexto.AppendLine("\t\t<EnumDataType(GetType(" + paramClaseApi + ".Estado))> _");
                    }
                    else if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                    {
                        newTexto.AppendLine("\t\t<EnumDataType(GetType(" + paramClaseApi + ".Transaccion))> _");
                    }
                    else if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    {
                    }
                    else if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    {
                    }
                    else if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    {
                    }
                    else if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") & nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    {
                    }
                    else
                    {
                        if (PermiteNull == "No")
                        {
                            if (tipoCol.ToUpper() == "STRING")
                            {
                                newTexto.AppendLine("\t\t<Required(AllowEmptyStrings = true, ErrorMessage:= \"Se necesita un valor para -" + nameCol + "- porque es un campo requerido.\")> _");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t<Required(ErrorMessage:=\"Se necesita un valor para -" + nameCol + "- porque es un campo requerido.\")> _");
                            }
                        }
                    }
                    if (dtColumns.Rows[iCol]["EsPK"].ToString() == "Yes")
                    {
                        newTexto.AppendLine("\t\t<Key> _");
                    }
                    newTexto.AppendLine("\t\tPublic Property " + nameCol + "() As " + tipoCol);
                    newTexto.AppendLine("\t\t\tGet");
                    newTexto.AppendLine("\t\t\t\tReturn _" + nameCol);
                    newTexto.AppendLine("\t\t\tEnd Get");
                    newTexto.AppendLine("\t\t\tSet(ByVal value As " + tipoCol + ")");
                    newTexto.AppendLine("\t\t\t\t_" + nameCol + " = value");
                    newTexto.AppendLine("\t\t\tEnd Set");
                    newTexto.AppendLine("\t\tEnd Property\r\n");
                }
                else
                {
                    newTexto.AppendLine("\t\t''' <summary>");
                    newTexto.AppendLine("\t\t''' \t Variable local de tipo " + tipoCol + " que representa a la columna " + nameCol + " de la Tabla " + pNameTabla);
                    newTexto.AppendLine("\t\t''' </summary>");
                    newTexto.AppendLine("\t\tPrivate _" + nameCol + " As " + tipoCol);
                    newTexto.AppendLine("\t\t''' <summary>");
                    newTexto.AppendLine("\t\t''' \t Propiedad publica de tipo " + tipoCol + " que representa a la columna " + nameCol + " de la Tabla " + pNameTabla);
                    newTexto.AppendLine("\t\t''' </summary>");
                    newTexto.AppendLine("\t\tPublic Property " + nameCol + "() As " + tipoCol);
                    newTexto.AppendLine("\t\t\tGet");
                    newTexto.AppendLine("\t\t\t\tReturn _" + nameCol);
                    newTexto.AppendLine("\t\t\tEnd Get");
                    newTexto.AppendLine("\t\t\tSet(ByVal value As " + tipoCol + ")");
                    newTexto.AppendLine("\t\t\t\t_" + nameCol + " = value");
                    newTexto.AppendLine("\t\t\tEnd Set");
                    newTexto.AppendLine("\t\tEnd Property\r\n");
                }
            }
            //newTexto.AppendLine(vbTab & " #endregion" & vbCrLf )

            newTexto.AppendLine("\tEnd Class");
            //newTexto.AppendLine("End Namespace")
            CFunciones.CrearArchivo(newTexto.ToString(), pNameClase + ".vb", PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase) + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase) + "\\" + pNameClase + ".vb";
        }

        public string CrearEnumEntityFramework(DataTable dtColumns, DataTable dtColumnsForaneas,
            ref cEntidadesParametros misParams)
        {
            string nameCol = null;
            string tipoCol = null;
            string strPermiteNull = null;

            string PermiteNull = null;
            string TipoColNull = null;
            string DescripcionCol = null;
            bool esPrimerCampo = true;
            string PathDesktop = null;
            if (misParams.bMoverResultados)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" +
                              misParams.strInfProyectoClass;
            }
            else
            {
                PathDesktop = misParams.strPathProyecto + "\\" + misParams.strInfProyectoClass;
            }

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            if (Directory.Exists(PathDesktop + "\\" + misParams.strNamespaceEntidades) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + misParams.strNamespaceEntidades);
            }

            string strAlias = misParams.strNameTabla;
            string strDescripcion = misParams.strNameTabla;
            try
            {
                DataTable dtPrefijo = new DataTable();

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        dtPrefijo = CFuncionesBDs.CargarDataTable(
                            "SELECT aliassta, descripcionsta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" +
                            misParams.strNameTabla.ToUpper() + "')");
                        break;

                    case TipoBD.SQLServer2000:
                        dtPrefijo = CFuncionesBDs.CargarDataTable(
                            "SELECT aliassta, descripcionsta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" +
                            misParams.strNameTabla.ToUpper() + "')");
                        break;

                    case TipoBD.Oracle:
                        dtPrefijo = CFuncionesBDs.CargarDataTable(
                            "SELECT ALIASSTA, DESCRIPCIONSTA FROM SEGTABLAS WHERE UPPER(TABLASTA) = '" +
                            misParams.strNameTabla.ToUpper() + "'");
                        break;

                    case TipoBD.PostgreSQL:
                        dtPrefijo = CFuncionesBDs.CargarDataTable(
                            "SELECT s.aliassta, s.descripcionsta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" +
                            misParams.strNameTabla.ToUpper() + "')");
                        if (dtPrefijo.Rows.Count == 0)
                        {
                            dtPrefijo = CFuncionesBDs.CargarDataTable(
                                "SELECT s.alias, s.descripcion FROM seg_tablas s WHERE UPPER(s.nombretabla) = UPPER('" +
                                misParams.strNameTabla.ToUpper() + "')");
                        }

                        break;
                }

                if (dtPrefijo.Rows.Count > 0)
                {
                    strAlias = dtPrefijo.Rows[0][0].ToString().ToLower();
                    strDescripcion = dtPrefijo.Rows[0][1].ToString().ToLower();
                    //strAlias = strAlias.Substring(0, 1).ToUpper() + strAlias.Substring(1, strAlias.Length - 1);
                }
                else
                {
                    strAlias = misParams.strNameTabla;
                    strDescripcion = misParams.strNameTabla;
                }
            }
            catch (Exception ex)
            {
                strAlias = misParams.strNameTabla;
                strDescripcion = misParams.strNameTabla;
            }

            //Obtenemos la llave primaria
            string strLlavePrimaria = "";
            string strLlavePrimariaSinTipos = "";
            string strColsLlavePrimaria = "";
            dynamic bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();
                    if (bEsPrimerElemento)
                    {
                        strLlavePrimaria = strLlavePrimaria + tipoCol + " " + tipoCol + nameCol;
                        strLlavePrimariaSinTipos = strLlavePrimariaSinTipos + tipoCol + nameCol;
                        strColsLlavePrimaria = strColsLlavePrimaria + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlavePrimaria = strLlavePrimaria + ", " + tipoCol + " " + tipoCol + nameCol;
                        strLlavePrimariaSinTipos = strLlavePrimariaSinTipos + ", " + tipoCol + nameCol;
                        strColsLlavePrimaria = strColsLlavePrimaria + ", " + nameCol;
                    }
                }
            }

            if (string.IsNullOrEmpty(strLlavePrimaria))
            {
                strLlavePrimaria = dtColumns.Rows[0][1] + " " + dtColumns.Rows[0][1] + dtColumns.Rows[0][0];
                strLlavePrimariaSinTipos = dtColumns.Rows[0][1].ToString() + dtColumns.Rows[0][0].ToString();
            }

            //Empezamos con el archivo
            string CabeceraTmp = misParams.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", misParams.strNameClase);
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%",
                "Clase que define un objeto para la Tabla " + misParams.strNameTabla);

            newTexto.AppendLine(CabeceraTmp);
            newTexto.AppendLine("#region");
            newTexto.AppendLine("using System.Collections.Generic;");
            newTexto.AppendLine("using System.ComponentModel.DataAnnotations;");
            newTexto.AppendLine("using System.Globalization;");
            newTexto.AppendLine("using System.Linq;");
            newTexto.AppendLine("using System.Text;");
            newTexto.AppendLine("using System.Threading;");
            newTexto.AppendLine("#endregion");
            newTexto.AppendLine("");

            newTexto.AppendLine("namespace " + misParams.strInfProyectoClass + "." + misParams.strNamespaceEntidades);
            newTexto.AppendLine("{");
            newTexto.AppendLine("\tpublic partial class " + misParams.strNameClase);
            newTexto.AppendLine("\t{");

            if (string.IsNullOrEmpty(misParams.strNameSchema.Trim()))
            {
                newTexto.AppendLine("\t\tpublic const string StrNombreTabla = \"" +
                                    misParams.strNameTabla.Substring(0, 1).ToUpper() +
                                    misParams.strNameTabla.Substring(1, 2).ToLower() +
                                    misParams.strNameTabla.Substring(3, 1).ToUpper() +
                                    misParams.strNameTabla.Substring(4) +
                                    "\";");
            }
            else if (misParams.strNameSchema.ToLower() != "public" & misParams.strNameSchema.ToLower() != "dbo")
            {
                newTexto.AppendLine("\t\tpublic const string StrNombreTabla = \"" + misParams.strNameSchema + "." +
                                    misParams.strNameTabla.Substring(0, 1).ToUpper() +
                                    misParams.strNameTabla.Substring(1, 2).ToLower() +
                                    misParams.strNameTabla.Substring(3, 1).ToUpper() +
                                    misParams.strNameTabla.Substring(4) +
                                    "\";");
            }
            else
            {
                newTexto.AppendLine("\t\tpublic const string StrNombreTabla = \"" + misParams.strNameTabla + "\";");
            }

            newTexto.AppendLine("\t\tpublic const string StrAliasTabla = \"" + strAlias + "\";");
            newTexto.AppendLine("\t\tpublic const string StrDescripcionTabla = \"" + strDescripcion + "\";");
            newTexto.AppendLine("\t\tpublic const string StrNombreLisForm = \"Listado de " + misParams.strNameTabla + "\";");
            newTexto.AppendLine("\t\tpublic const string StrNombreDocForm = \"Detalle de " + misParams.strNameTabla + "\";");
            newTexto.AppendLine("\t\tpublic const string StrNombreForm = \"Registro de " + misParams.strNameTabla + "\";");

            //ENUM
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\tpublic enum Fields");
            newTexto.AppendLine("\t\t{");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                if (esPrimerCampo)
                {
                    newTexto.AppendLine("\t\t\t" + nameCol);
                    esPrimerCampo = false;
                }
                else
                {
                    newTexto.AppendLine("\t\t\t," + nameCol);
                }
            }

            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("");

            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// Funcion que determina si un objeto es valido o no");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <returns>Resultado de la validacion</returns>");
            newTexto.AppendLine("\t\tpublic bool IsValid()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar context = new ValidationContext(this, null, null);");
            newTexto.AppendLine("\t\t\tIList<ValidationResult> errors = new List<ValidationResult>();");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\treturn Validator.TryValidateObject(this, context, errors, true);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// Obtiene los errores basado en los DataAnnotations");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <returns>Devuelve un IList del tipo ValidationResult con los errores obtenidos</returns>");
            newTexto.AppendLine("\t\tpublic IList<ValidationResult> ValidationErrors()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar context = new ValidationContext(this, null, null);");
            newTexto.AppendLine("\t\t\tIList<ValidationResult> errors = new List<ValidationResult>();");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\treturn !Validator.TryValidateObject(this, context, errors, true) ? errors : new List<ValidationResult>(0);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// Obtiene los errores basado en los DataAnnotations");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <returns>Devuelve un String con los errores obtenidos</returns>");
            newTexto.AppendLine("\t\tpublic string ValidationErrorsString()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar strErrors = new StringBuilder();");
            newTexto.AppendLine("\t\t\tvar context = new ValidationContext(this, null, null);");
            newTexto.AppendLine("\t\t\tIList<ValidationResult> errors = new List<ValidationResult>();");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tif (Validator.TryValidateObject(this, context, errors, true)) return strErrors.ToString();");
            newTexto.AppendLine("\t\t\tforeach (var result in errors)");
            newTexto.AppendLine("\t\t\t\tstrErrors.AppendLine(result.MemberNames.FirstOrDefault() + \": \" + result.ErrorMessage);");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\treturn strErrors.ToString();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //GetDescripcion del Objeto
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene la Descripcion de Determinado Campo");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"myField\" type=\"" + misParams.strNameClase + ".Fileds\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Campo solicitado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"idioma\" type=\"System.Globalization.CultureInfo\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Idioma");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo STRING que describe al parametro");
            newTexto.AppendLine("\t\t/// </returns>");
            newTexto.AppendLine("\t\tpublic static string GetDesc(Fields myField, CultureInfo idioma = null)");
            newTexto.AppendLine("\t\t{");

            newTexto.AppendLine("\t\t\tidioma ??= Thread.CurrentThread.CurrentCulture;");
            newTexto.AppendLine("\t\t\treturn myField switch");
            newTexto.AppendLine("\t\t\t{");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                strPermiteNull = dtColumns.Rows[iCol]["PermiteNull"].ToString();
                DescripcionCol = dtColumns.Rows[iCol]["COL_DESC"].ToString();

                TipoColNull = tipoCol;
                if (tipoCol != "String")
                {
                    if (strPermiteNull == "Yes")
                    {
                        if (TipoColNull.Contains("[]"))
                        {
                            TipoColNull = tipoCol;
                        }
                        else
                        {
                            TipoColNull = tipoCol + "?";
                        }
                    }
                }

                if (string.IsNullOrEmpty(DescripcionCol))
                    newTexto.AppendLine("\t\t\t\tFields." + nameCol + " when idioma.Name.ToUpper().StartsWith(\"ES\") => \"Valor de tipo " + tipoCol + " para " + nameCol.Replace(strAlias, "") + "\", ");
                else
                    newTexto.AppendLine("\t\t\t\tFields." + nameCol + " when idioma.Name.ToUpper().StartsWith(\"ES\") => \"" + DescripcionCol.Replace("\r\n", "") + "\", ");
            }

            newTexto.AppendLine("\t\t\t\t_ => string.Empty");
            newTexto.AppendLine("\t\t\t};");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //GetFieldRequired del Objeto
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene el Error de Determinado Campo");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"myField\" type=\"" + misParams.strNameClase + ".Fileds\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Campo solicitado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"idioma\" type=\"System.Globalization.CultureInfo\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Idioma");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo STRING que describe al parametro");
            newTexto.AppendLine("\t\t/// </returns>");
            newTexto.AppendLine("\t\tpublic static string GetError(Fields myField, CultureInfo idioma = null)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tidioma ??= Thread.CurrentThread.CurrentCulture;");
            newTexto.AppendLine("\t\t\treturn myField switch");
            newTexto.AppendLine("\t\t\t{");
            //newTextoParcial.AppendLine(vbTab & vbTab & vbTab & vbTab & "string strDesc = " & Chr(34) & Chr(34) & ";" )

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                strPermiteNull = dtColumns.Rows[iCol]["PermiteNull"].ToString();
                TipoColNull = tipoCol;
                if (tipoCol != "String")
                {
                    if (strPermiteNull == "Yes")
                    {
                        if (TipoColNull.Contains("[]"))
                        {
                            TipoColNull = tipoCol;
                        }
                        else
                        {
                            TipoColNull = tipoCol + "?";
                        }
                    }
                }

                newTexto.AppendLine("\t\t\t\tFields." + nameCol + " when idioma.Name.ToUpper().StartsWith(\"ES\") => \"" + tipoCol + " para " + nameCol.Replace(strAlias, "") + "\", ");
            }

            newTexto.AppendLine("\t\t\t\t_ => string.Empty");
            newTexto.AppendLine("\t\t\t};");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //GetFieldRequired del Objeto
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que obtiene el Nombre de Determinado Campo");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"myField\" type=\"" + misParams.strNameClase + ".Fileds\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Campo solicitado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"idioma\" type=\"System.Globalization.CultureInfo\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Idioma");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \tValor del Tipo STRING que describe al parametro");
            newTexto.AppendLine("\t\t/// </returns>");
            newTexto.AppendLine("\t\tpublic static string GetColumn(Fields myField, CultureInfo idioma = null)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tidioma ??= Thread.CurrentThread.CurrentCulture;");
            newTexto.AppendLine("\t\t\treturn myField switch");
            newTexto.AppendLine("\t\t\t{");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                strPermiteNull = dtColumns.Rows[iCol]["PermiteNull"].ToString();
                TipoColNull = tipoCol;
                if (tipoCol != "String")
                {
                    if (strPermiteNull == "Yes")
                    {
                        if (TipoColNull.Contains("[]"))
                        {
                            TipoColNull = tipoCol;
                        }
                        else
                        {
                            TipoColNull = tipoCol + "?";
                        }
                    }
                }
                newTexto.AppendLine("\t\t\t\tFields." + nameCol + " when idioma.Name.ToUpper().StartsWith(\"ES\") => \"" + nameCol.Replace(strAlias, "") + ":\", ");
            }

            newTexto.AppendLine("\t\t\t\t_ => string.Empty");
            newTexto.AppendLine("\t\t\t};");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            if (false)
            {
                //Constructores
                newTexto.AppendLine("");
                newTexto.AppendLine("\t\t#region Constructoress");
                newTexto.AppendLine("\t\t");

                newTexto.AppendLine("\t\tpublic " + misParams.strNameClase + "()");
                newTexto.AppendLine("\t\t{");
                if (!strColsLlavePrimaria.Contains(","))
                    for (int iCol = 0; iCol <= dtColumnsForaneas.Rows.Count - 1; iCol++)
                    {
                        string tablaFOranea = dtColumnsForaneas.Rows[iCol]["tabla"].ToString();

                        string objetoForaneo = misParams.strPrefijoEntidades +
                                               tablaFOranea.ToPascalCase();

                        newTexto.AppendLine("\t\t\t" + objetoForaneo + " = new HashSet<" + objetoForaneo + ">();");
                    }

                newTexto.AppendLine("");
                newTexto.AppendLine("\t\t\t//Inicializacion de Variables");
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();
                    PermiteNull = dtColumns.Rows[iCol]["PermiteNull"].ToString();
                    if (tipoCol.ToUpper() == "STRING")
                    {
                        newTexto.AppendLine("\t\t\t" + nameCol + " = null;");
                    }
                    else if (tipoCol.ToUpper() == "BOOL")
                    {
                        newTexto.AppendLine("\t\t\t//" + nameCol + " = false;");
                    }
                    else
                    {
                        if (PermiteNull == "Yes")
                        {
                            newTexto.AppendLine("\t\t\t" + nameCol + " = null;");
                        }
                    }
                }

                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\tpublic " + misParams.strNameClase + "(" + misParams.strNameClase +
                                    " obj)");
                newTexto.AppendLine("\t\t{");
                if (!strColsLlavePrimaria.Contains(","))
                    for (int iCol = 0; iCol <= dtColumnsForaneas.Rows.Count - 1; iCol++)
                    {
                        string tablaFOranea = dtColumnsForaneas.Rows[iCol]["tabla"].ToString();

                        string objetoForaneo = misParams.strPrefijoEntidades +
                                               tablaFOranea.ToPascalCase();

                        newTexto.AppendLine("\t\t\t" + objetoForaneo + " = new HashSet<" + objetoForaneo + ">();");
                    }

                newTexto.AppendLine("");
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    newTexto.AppendLine("\t\t\t" + nameCol + " = obj." + nameCol + ";");
                }

                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\t#endregion");
                newTexto.AppendLine("\t\t");

                //ENCAPSULAMIENTO
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();
                    PermiteNull = dtColumns.Rows[iCol]["PermiteNull"].ToString();
                    DescripcionCol = dtColumns.Rows[iCol]["COL_DESC"].ToString();
                    string nameColOriginal = dtColumns.Rows[iCol]["name_original"].ToString();

                    TipoColNull = tipoCol;
                    if (tipoCol.ToUpper() != "STRING")
                    {
                        if (PermiteNull == "Yes")
                        {
                            if (TipoColNull.Contains("[]"))
                            {
                                TipoColNull = tipoCol;
                            }
                            else
                            {
                                TipoColNull = tipoCol + "?";
                            }
                        }
                    }

                    newTexto.AppendLine("\t\t/// <summary>");
                    if (string.IsNullOrEmpty(DescripcionCol))
                    {
                        newTexto.AppendLine("\t\t/// \t Propiedad publica de tipo " + tipoCol +
                                            " que representa a la columna " + nameCol + " de la Tabla " +
                                            misParams.strNameTabla);
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t/// \t " + DescripcionCol.Replace("\r\n", ""));
                    }

                    newTexto.AppendLine("\t\t/// \t Permite Null: " + dtColumns.Rows[iCol]["PermiteNull"]);
                    newTexto.AppendLine("\t\t/// \t Es Calculada: " + dtColumns.Rows[iCol]["EsCalculada"]);
                    newTexto.AppendLine("\t\t/// \t Es RowGui: " + dtColumns.Rows[iCol]["EsRowGui"]);
                    newTexto.AppendLine("\t\t/// \t Es PrimaryKey: " + dtColumns.Rows[iCol]["EsPK"]);
                    newTexto.AppendLine("\t\t/// \t Es ForeignKey: " + dtColumns.Rows[iCol]["EsFK"]);
                    newTexto.AppendLine("\t\t/// </summary>");
                    if (misParams.bUseDataAnnotations)
                    {
                        newTexto.AppendLine("\t\t[Column(\"" + nameColOriginal + "\")]");
                        switch (tipoCol.ToUpper())
                        {
                            case "STRING":
                                int iLongitud = 0;
                                if (int.TryParse(dtColumns.Rows[iCol]["LONGITUD"].ToString(), out iLongitud))
                                {
                                    if (iLongitud > 0)
                                    {
                                        newTexto.AppendLine("\t\t[StringLength(" + iLongitud.ToString() +
                                                            ", MinimumLength=0)]");
                                    }
                                }

                                break;
                        }

                        if (string.IsNullOrEmpty(DescripcionCol))
                        {
                            newTexto.AppendLine("\t\t[Display(Name = \"" + nameCol + "\", ShortName = \"" +
                                                nameColOriginal +
                                                "\", Description = \" Propiedad publica de tipo " + tipoCol +
                                                " que representa a la columna " + nameCol + " de la Tabla " +
                                                misParams.strNameTabla + "\")]");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\t[Display(Name = \"" + nameCol + "\", ShortName = \"" +
                                                nameColOriginal + "\", Description = \"" +
                                                DescripcionCol.Replace("\r\n", "") + "\")]");
                        }

                        if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("api") &
                            nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("estado"))
                        {
                            //newTexto.AppendLine("\t\t[EnumDataType(typeof(" + misParams.strParamClaseApi + ".Estado))]");
                        }
                        else if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("api") &
                                 nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "")
                                     .Contains("transaccion"))
                        {
                            //newTexto.AppendLine("\t\t[EnumDataType(typeof(" + misParams.strParamClaseApi + ".Transaccion))]");
                        }
                        else if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("usu") &
                                 nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("cre"))
                        {
                        }
                        else if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("usu") &
                                 nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("mod"))
                        {
                        }
                        else if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("fec") &
                                 nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("cre"))
                        {
                        }
                        else if (nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("fec") &
                                 nameCol.ToLower().Replace(misParams.strNameTabla.ToLower(), "").Contains("mod"))
                        {
                        }
                        else
                        {
                            if (PermiteNull == "No")
                            {
                                if (tipoCol.ToUpper() == "STRING")
                                {
                                    newTexto.AppendLine(
                                        "\t\t[Required(AllowEmptyStrings = true, ErrorMessage = \"Se necesita un valor para -" +
                                        nameCol + "- porque es un campo requerido.\")]");
                                }
                                else
                                {
                                    newTexto.AppendLine(
                                        "\t\t[Required(ErrorMessage = \"Se necesita un valor para -" + nameCol +
                                        "- porque es un campo requerido.\")]");
                                }
                            }
                        }

                        if (dtColumns.Rows[iCol]["EsPK"].ToString() == "Yes")
                        {
                            newTexto.AppendLine("\t\t[Key]");
                        }
                    }

                    newTexto.AppendLine("\t\tpublic " + TipoColNull + " " + nameCol + " { get; set; }");
                    newTexto.AppendLine("");
                }

                //Navigation
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();
                    string esFk = dtColumns.Rows[iCol]["esfk"].ToString();
                    string tablaFOranea = dtColumns.Rows[iCol]["tablaforanea"].ToString();

                    if (esFk == "Yes")
                    {
                        string objetoForaneo = misParams.strPrefijoEntidades +
                                               tablaFOranea.ToPascalCase();

                        newTexto.AppendLine("\t\t[ForeignKey(\"" + nameCol + "\")]");
                        newTexto.AppendLine("\t\t[InverseProperty(\"" + misParams.strNameClase + "\")]");
                        newTexto.AppendLine("\t\tpublic virtual " + objetoForaneo + " " + nameCol +
                                            "Navigation { get; set; }");
                    }
                }

                newTexto.AppendLine("");

                //Navigation 2
                if (!strColsLlavePrimaria.Contains(","))
                    for (int iCol = 0; iCol <= dtColumnsForaneas.Rows.Count - 1; iCol++)
                    {
                        nameCol = dtColumnsForaneas.Rows[iCol][0].ToString();
                        string tablaFOranea = dtColumnsForaneas.Rows[iCol]["tabla"].ToString();

                        string objetoForaneo = misParams.strPrefijoEntidades +
                                               tablaFOranea.ToPascalCase();

                        newTexto.AppendLine("\t\t//" + dtColumnsForaneas.Rows[iCol]["col_desc"].ToString());
                        newTexto.AppendLine("\t\t[InverseProperty(\"" + nameCol + "Navigation\")]");
                        newTexto.AppendLine("\t\tpublic virtual ICollection<" + objetoForaneo + "> " +
                                            objetoForaneo + " { get; set; }");
                    }

                newTexto.AppendLine("");
            }

            newTexto.AppendLine("\t}");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), misParams.strNameClase + ".Extended.cs",
                PathDesktop + "\\" + misParams.strNamespaceEntidades + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + misParams.strNamespaceEntidades + "\\" + misParams.strNameClase + ".cs";
        }

        #endregion Methods
    }
}