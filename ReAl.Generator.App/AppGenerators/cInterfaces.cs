﻿using DocumentFormat.OpenXml.Drawing;
using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlTypes;
using System.IO;
using System.Text;

namespace ReAl.Generator.App.AppGenerators
{
    public class cInterfaces
    {
        public string CrearInterfacesJavaClasico(DataTable dtColumns, string pSchema, string pNameClase, ref FPrincipal formulario)
        {
            string nameCol = null;
            string tipoCol = null;


            string pNameTabla = pNameClase.Replace("_", "").ToLower();
            //pNameTabla = pNameTabla.Substring(0, 1).ToUpper & pNameTabla.Substring(1).ToLower

            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text +
                                  pNameClase.Replace("_", "").Substring(0, 1).ToUpper() +
                                  pNameClase.Replace("_", "").Substring(1);
            string pNameClaseIn = formulario.txtPrefijoInterface.Text + 
                                  pNameClase.Replace("_", "").Substring(0, 1).ToUpper() +
                                  pNameClase.Replace("_", "").Substring(1);

            bool SetPropertiesInARow = true;
            string PermiteNull = null;
            string TipoColNull = null;
            string DescripcionCol = null;
            bool esPrimerCampo = true;
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceInterface.Text.Replace("[tabla]", pNameClase).Replace("_", "").Replace(".", "\\")) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceInterface.Text.Replace("[tabla]", pNameClase).Replace("_", "").Replace(".", "\\"));
            }

            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase).Replace("#region", "").Replace("#endregion", "");
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que define un objeto para la Tabla " + pNameTabla);

            string strLlavePrimaria = "";
            var strLlavePrimariaWhere = new StringBuilder();              
            var strLlavePrimariaParams = new StringBuilder();
            var strInsertColumns = new StringBuilder();
            var strInsertValues = new StringBuilder();
            var strInsertValuesPs = new StringBuilder();            
            var strUpdateClause = new StringBuilder();
            var strUpdateWhere = new StringBuilder();
            var strUpdateValuesPs = new StringBuilder();
            var strUpdateWherePs = new StringBuilder();
            dynamic bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    var nameOriginal = dtColumns.Rows[iCol]["name_original"].ToString();
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();                    

                    strLlavePrimariaParams.AppendLine("\t\tparams.add(" + tipoCol + nameCol + ");");
                    if (bEsPrimerElemento)
                    {
                        strLlavePrimaria = strLlavePrimaria + tipoCol + " " + tipoCol + nameCol;
                        strLlavePrimariaWhere.AppendLine("\t\t        \"WHERE " + nameOriginal + " = ? \" + ");
                        strUpdateWhere.AppendLine("\t\t        \"WHERE " + nameOriginal + " = ? \" + ");
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlavePrimaria = strLlavePrimaria + ", " + tipoCol + " " + tipoCol + nameCol;
                        strLlavePrimariaWhere.AppendLine("\t\t        + \"AND " + nameOriginal + " = ? \" + ");
                        strUpdateWhere.AppendLine("\t\t        + \"AND " + nameOriginal + " = ? \" + ");
                    }
                }                
            }

            bEsPrimerElemento = true;
            int counter = 1;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "NO"))
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    var nameOriginal = dtColumns.Rows[iCol]["name_original"].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();
                    var nameColPascal = dtColumns.Rows[iCol][0].ToString().Substring(0, 1).ToUpper() +
                                        dtColumns.Rows[iCol][0].ToString().Substring(1);

                    if (nameOriginal.ToUpper().EndsWith("TRAN_ID"))
                    {
                        switch (tipoCol.ToUpper())
                        {
                            case "INT":
                            case "INT32":
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setInt(" + (counter) + ", tran.tranId);");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setInt(" + (counter) + ", tran.tranId);");
                                break;

                            case "LONG":
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setLong(" + (counter) + ", tran.tranId);");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setLong(" + (counter) + ", tran.tranId);");
                                break;

                            default:
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setString(" + (counter) + ", tran.tranId);");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setString(" + (counter) + ", tran.tranId);");
                                break;
                        }
                    }
                    else if(nameOriginal.ToUpper().EndsWith("TRAN_DATE"))
                    {
                        switch (tipoCol.ToUpper())
                        {
                            case "DATETIME":
                            case "DATE":
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setTimestamp(" + (counter) + ", DateTools.toTimestamp(tran.createdDate));");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setTimestamp(" + (counter) + ", DateTools.toTimestamp(tran.createdDate));");
                                break;
                            default:
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setString(" + (counter) + ", tran.createdDate);");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setString(" + (counter) + ", tran.createdDate);");
                                break;
                        }
                    }
                    else if (nameOriginal.ToUpper().EndsWith("TRAN_PERIOD"))
                    {
                        switch (tipoCol.ToUpper())
                        {
                            case "STRING":
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setString(" + (counter) + ", tran.period);");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setString(" + (counter) + ", tran.period);");
                                break;

                            default:
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setString(" + (counter) + ", tran.period);");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setString(" + (counter) + ", tran.period);");
                                break;
                        }
                    }
                    else if (nameOriginal.ToUpper().EndsWith("USER_ID"))
                    {
                        switch (tipoCol.ToUpper())
                        {                            
                            case "INT":
                            case "INT32":
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setInt(" + (counter) + ", tran.createdBy);");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setInt(" + (counter) + ", tran.createdBy);");
                                break;
                            case "LONG":
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setLong(" + (counter) + ", tran.createdBy);");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setLong(" + (counter) + ", tran.createdBy);");
                                break;
                            default:
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setString(" + (counter) + ", tran.createdBy);");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setString(" + (counter) + ", tran.createdBy);");
                                break;
                        }
                    }
                    else
                    {                        
                        switch (tipoCol.ToUpper())
                        {
                            case "INT":
                            case "INT32":
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setInt(" + (counter) + ", param.get" + nameColPascal + "());");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setInt(" + (counter) + ", param.get" + nameColPascal + "());");
                                break;

                            case "LONG":
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setLong(" + (counter) + ", param.get" + nameColPascal + "());");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setLong(" + (counter) + ", param.get" + nameColPascal + "());");
                                break;

                            case "DOUBLE":
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setDouble(" + (counter) + ", param.get" + nameColPascal + "());");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setDouble(" + (counter) + ", param.get" + nameColPascal + "());");
                                break;

                            case "BOOL":
                            case "BOOLEAN":
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setBoolean(" + (counter) + ", param.get" + nameColPascal + "());");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setBoolean(" + (counter) + ", param.get" + nameColPascal + "());");
                                break;

                            case "DATETIME":
                            case "DATE":
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setTimestamp(" + (counter) + ", DateTools.toTimestamp(param.get" + nameColPascal + "()));");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setTimestamp(" + (counter) + ", DateTools.toTimestamp(param.get" + nameColPascal + "()));");
                                break;

                            case "STRING":
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setString(" + (counter) + ", param.get" + nameColPascal + "());");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setString(" + (counter) + ", param.get" + nameColPascal + "());");
                                break;

                            default:
                                strInsertValuesPs.AppendLine("\t\t\t\tps.setString(" + (counter) + ", param.get" + nameColPascal + "());");
                                strUpdateValuesPs.AppendLine("\t\t\t\tps.setString(" + (counter) + ", param.get" + nameColPascal + "());");
                                break;
                        }
                    }

                    if (bEsPrimerElemento)
                    {
                        strUpdateClause.AppendLine("\t\t        \"  " + nameOriginal + " = ?\\n\" +");
                        strInsertColumns.AppendLine("\t\t        \""+ nameOriginal + "\\n\" +");
                        strInsertValues.AppendLine("\t\t        \"?\\n\" +");
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strUpdateClause.AppendLine("\t\t        \"  ," + nameOriginal + " = ?\\n\" +");
                        strInsertColumns.AppendLine("\t\t        \"," + nameOriginal + "\\n\" +");
                        strInsertValues.AppendLine("\t\t        \",?\\n\" +");
                    }
                    counter++;
                }
            }
            if (string.IsNullOrEmpty(strLlavePrimaria))
            {
                strLlavePrimaria = dtColumns.Rows[0][1].ToString() + " " + dtColumns.Rows[0][1].ToString() + dtColumns.Rows[0][0].ToString();
            }
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    var nameOriginal = dtColumns.Rows[iCol]["name_original"].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();
                    var nameColPascal = dtColumns.Rows[iCol][0].ToString().Substring(0, 1).ToUpper() +
                                        dtColumns.Rows[iCol][0].ToString().Substring(1);

                    switch (tipoCol.ToUpper())
                    {
                        case "INT":
                        case "INT32":
                            strUpdateWherePs.AppendLine("\t\t\t\tps.setInt(" + (counter) + ", param.get" + nameColPascal + "());");                            
                            break;

                        case "LONG":
                            strUpdateWherePs.AppendLine("\t\t\t\tps.setLong(" + (counter) + ", param.get" + nameColPascal + "());");                            
                            break;

                        case "DOUBLE":
                            strUpdateWherePs.AppendLine("\t\t\t\tps.setDouble(" + (counter) + ", param.get" + nameColPascal + "());");                            
                            break;

                        case "BOOL":
                        case "BOOLEAN":
                            strUpdateWherePs.AppendLine("\t\t\t\tps.setBoolean(" + (counter) + ", param.get" + nameColPascal + "());");                            
                            break;

                        case "DATETIME":
                        case "DATE":
                            strUpdateWherePs.AppendLine("\t\t\t\tps.setTimestamp(" + (counter) + ", DateTools.toTimestamp(param.get" + nameColPascal + "()));");                            
                            break;

                        case "STRING":
                            strUpdateWherePs.AppendLine("\t\t\t\tps.setString(" + (counter) + ", param.get" + nameColPascal + "());");                            
                            break;

                        default:
                            strUpdateWherePs.AppendLine("\t\t\t\tps.setString(" + (counter) + ", param.get" + nameColPascal + "());");                            
                            break;
                    }
                    counter++;
                }
            }

            //newTexto.AppendLine(CabeceraTmp)
            newTexto.AppendLine("package " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceInterface.Text.Replace("[tabla]", pNameClase).Replace("_", "") + ";");
            newTexto.AppendLine("");
            newTexto.AppendLine("import " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceEntidades.Text.Replace("[tabla]", pNameClase).Replace("_", "") + ";");
            newTexto.AppendLine("import org.apache.commons.io.IOUtils;");
            newTexto.AppendLine("import org.apache.commons.logging.Log;");
            newTexto.AppendLine("import org.apache.commons.logging.LogFactory;");
            newTexto.AppendLine("import org.jsoup.Jsoup;");
            newTexto.AppendLine("import org.jsoup.nodes.Document;");
            newTexto.AppendLine("import org.jsoup.nodes.Element;");
            newTexto.AppendLine("import org.springframework.beans.factory.annotation.Autowired;");
            newTexto.AppendLine("import org.springframework.dao.EmptyResultDataAccessException;");
            newTexto.AppendLine("import org.springframework.jdbc.core.BeanPropertyRowMapper;");
            newTexto.AppendLine("import org.springframework.jdbc.core.JdbcTemplate;");
            newTexto.AppendLine("import org.springframework.jdbc.core.PreparedStatementCreator;");
            newTexto.AppendLine("import org.springframework.jdbc.core.PreparedStatementSetter;");
            newTexto.AppendLine("import org.springframework.jdbc.support.GeneratedKeyHolder;");
            newTexto.AppendLine("import org.springframework.stereotype.Repository;");
            newTexto.AppendLine("import org.xhtmlrenderer.layout.SharedContext;");
            newTexto.AppendLine("import org.xhtmlrenderer.pdf.ITextRenderer;");
            newTexto.AppendLine("import java.io.*;");
            newTexto.AppendLine("import java.sql.*;");
            newTexto.AppendLine("import java.util.ArrayList;");
            newTexto.AppendLine("import java.util.Base64;");
            newTexto.AppendLine("import java.util.List;");
            newTexto.AppendLine("import java.util.Map;");
            newTexto.AppendLine("");
            newTexto.AppendLine("/*! ");
            newTexto.AppendLine(" *  Service class for " + pNameTabla);
            newTexto.AppendLine(" *  @author " + formulario.txtInfAutor.Text);
            newTexto.AppendLine(" *  @version 1.0");
            newTexto.AppendLine(" */");
            newTexto.AppendLine("");            
            newTexto.AppendLine("@Repository");
            newTexto.AppendLine("public class " + pNameClaseIn + "Service");
            newTexto.AppendLine("{");
            
            newTexto.AppendLine("\tprotected final Log logger = LogFactory.getLog(this.getClass());");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t@Autowired");
            newTexto.AppendLine("\tJdbcTemplate jdbcTemplate;");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\t@Autowired");
            newTexto.AppendLine("\tCatalogueService catalogueService;");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic " + pNameClaseEnt  + " getById(" + strLlavePrimaria + "){");
            newTexto.AppendLine("\t\tStringBuilder sql = new StringBuilder();");
            newTexto.AppendLine("\t\tList<Object> params = new ArrayList<Object>();");
            newTexto.AppendLine("\t\tsql.append(\"SELECT *\\n\" +");
            newTexto.AppendLine("\t\t        \"FROM "+ pSchema +"."+ pNameClase + "\\n\" +");            
            newTexto.Append(strLlavePrimariaWhere.ToString());
            newTexto.AppendLine("\t\t        \"\");");
            newTexto.Append(strLlavePrimariaParams.ToString());
            newTexto.AppendLine("\t\tlogger.debug(\"Executing: \" + sql + \" params:\" + params);");
            newTexto.AppendLine("\t\ttry {");
            newTexto.AppendLine("\t\t\t"+ pNameClaseEnt + " res = this.jdbcTemplate.queryForObject(sql.toString(), new BeanPropertyRowMapper<>("+ pNameClaseEnt + ".class), params.toArray());");
            newTexto.AppendLine("\t\t\tlogger.debug(\"Out: \" + res);");
            newTexto.AppendLine("\t\t\treturn res;");
            newTexto.AppendLine("\t\t} catch (EmptyResultDataAccessException e) {");
            newTexto.AppendLine("\t\t\tlogger.debug(\"Out: null\");");
            newTexto.AppendLine("\t\t\treturn null;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic List<"+ pNameClaseEnt + "> getAll(){");
            newTexto.AppendLine("\t\tStringBuilder sql = new StringBuilder();");
            newTexto.AppendLine("\t\tsql.append(\"SELECT *,\\n\" +");
            newTexto.AppendLine("\t\t		 \"    CASE `" + pNameClase + "`.`status` \\n\" +");
            newTexto.AppendLine("\t\t		 \"        WHEN 'A' THEN 'Habilitado' \\n\" +");
            newTexto.AppendLine("\t\t		 \"        WHEN 'DIS' THEN 'Deshabilitado' \\n\" +");
            newTexto.AppendLine("\t\t		 \"        WHEN 'I' THEN 'Deshabilitado' \\n\" +");
            newTexto.AppendLine("\t\t		 \"    END AS status_description\\n\" +");
            newTexto.AppendLine("\t\t        \"FROM " + pSchema + "." + pNameClase + "\\n\" +");
            newTexto.AppendLine("\t\t        \"\");");
            newTexto.AppendLine("\t\tlogger.debug(\"Executing: \" + sql );");
            newTexto.AppendLine("\t\ttry {");
            newTexto.AppendLine("\t\t\tList<"+ pNameClaseEnt + "> res = this.jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<>("+ pNameClaseEnt + ".class));");
            newTexto.AppendLine("\t\t\tlogger.debug(\"Out: \" + res);");
            newTexto.AppendLine("\t\t\treturn res;");
            newTexto.AppendLine("\t\t} catch (EmptyResultDataAccessException e) {");
            newTexto.AppendLine("\t\t\tlogger.debug(\"Out: null\");");
            newTexto.AppendLine("\t\t\treturn null;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic List<" + pNameClaseEnt + "> getActive(){");
            newTexto.AppendLine("\t\tStringBuilder sql = new StringBuilder();");
            newTexto.AppendLine("\t\tsql.append(\"SELECT *\\n\" +");
            newTexto.AppendLine("\t\t		 \"    CASE `" + pNameClase + "`.`status` \\n\" +");
            newTexto.AppendLine("\t\t		 \"        WHEN 'A' THEN 'Habilitado' \\n\" +");
            newTexto.AppendLine("\t\t		 \"        WHEN 'DIS' THEN 'Deshabilitado' \\n\" +");
            newTexto.AppendLine("\t\t		 \"        WHEN 'I' THEN 'Deshabilitado' \\n\" +");
            newTexto.AppendLine("\t\t		 \"    END AS status_description\\n\" +");
            newTexto.AppendLine("\t\t        \"FROM " + pSchema + "." + pNameClase + "\\n\" +");
            newTexto.AppendLine("\t\t        \"WHERE " + pNameClase + ".status = 'A'; \");");
            newTexto.AppendLine("\t\tlogger.debug(\"Executing: \" + sql );");
            newTexto.AppendLine("\t\ttry {");
            newTexto.AppendLine("\t\t\tList<" + pNameClaseEnt + "> res = this.jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<>(" + pNameClaseEnt + ".class));");
            newTexto.AppendLine("\t\t\tlogger.debug(\"Out: \" + res);");
            newTexto.AppendLine("\t\t\treturn res;");
            newTexto.AppendLine("\t\t} catch (EmptyResultDataAccessException e) {");
            newTexto.AppendLine("\t\t\tlogger.debug(\"Out: null\");");
            newTexto.AppendLine("\t\t\treturn null;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic Long insert("+ pNameClaseEnt + " param, TransactionalColumns tran) {");
            newTexto.AppendLine("\t\tfinal StringBuilder sql = new StringBuilder();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tsql.append(\"INSERT INTO " + pSchema + "." + pNameClase + "\\n\" +");
            newTexto.AppendLine("\t\t        \"(\\n\" +");
            newTexto.Append(strInsertColumns.ToString());
            newTexto.AppendLine("\t\t        \")\\n\" +");
            newTexto.AppendLine("\t\t        \"VALUES\\n\" +");
            newTexto.AppendLine("\t\t        \"(\\n\" +");
            newTexto.Append(strInsertValues.ToString());
            newTexto.AppendLine("\t\t        \")\\n\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tlogger.debug(\"Executing:\" + sql + \" ; param: \" + param);");
            newTexto.AppendLine("\t\tGeneratedKeyHolder holder = new GeneratedKeyHolder();");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tjdbcTemplate.update( new PreparedStatementCreator() {");
            newTexto.AppendLine("\t\t\t@Override");
            newTexto.AppendLine("\t\t\tpublic PreparedStatement createPreparedStatement(Connection connection) throws SQLException {");
            newTexto.AppendLine("\t\t\t\tPreparedStatement ps = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);");
            newTexto.Append(strInsertValuesPs.ToString());
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\treturn ps;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}, holder);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn holder.getKey().longValue();");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic void update("+ pNameClaseEnt + " param, TransactionalColumns tran) {");
            newTexto.AppendLine("\t\tlogger.debug(\"In: param:\" + param);");
            newTexto.AppendLine("\t\tfinal StringBuilder sql = new StringBuilder();");
            newTexto.AppendLine("\t\tsql.append(\"UPDATE \\n\" +");
            newTexto.AppendLine("\t\t        \"  " + pSchema + "." + pNameClase + "\\n\" +");
            newTexto.AppendLine("\t\t        \"SET\\n\" +");
            newTexto.Append(strUpdateClause.ToString());
            newTexto.Append(strUpdateWhere.ToString());
            newTexto.AppendLine("\t\t        \"\");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tlogger.debug(\"Executing:\" + sql + \" ;  param:\" + param);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tjdbcTemplate.update(sql.toString(), new PreparedStatementSetter() {");
            newTexto.AppendLine("\t\t\t@Override");
            newTexto.AppendLine("\t\t\tpublic void setValues(PreparedStatement ps) throws SQLException {");
            newTexto.AppendLine(strUpdateValuesPs.ToString());
            newTexto.AppendLine(strUpdateWherePs.ToString());
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t});");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tpublic void deleteById(Integer id, TransactionalColumns tran) {");
            newTexto.AppendLine("\t\tfinal StringBuilder sql = new StringBuilder();");
            newTexto.AppendLine("\t\tsql.append(\" UPDATE " + pSchema + "." + pNameClase + " \\n\");");
            newTexto.AppendLine("\t\tsql.append(\" SET status = ?, \\n\");");
            newTexto.AppendLine("\t\tsql.append(\" tran_id = ?, \\n\");");
            newTexto.AppendLine("\t\tsql.append(\" tran_date = ?, \\n\");");
            newTexto.AppendLine("\t\tsql.append(\" tran_period = ?, \\n\");");
            newTexto.AppendLine("\t\tsql.append(\" user_id  = ? \\n\");");
            newTexto.AppendLine("\t\tsql.append(\" WHERE "+ dtColumns.Rows[0]["name_original"] +" = ? \");");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tlogger.debug(\"Executing:\" + sql + \";\" + \""+ dtColumns.Rows[0]["name_original"] +": \" + id);");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tjdbcTemplate.update(sql.toString(), new PreparedStatementSetter() {");
            newTexto.AppendLine("\t\t\t@Override");
            newTexto.AppendLine("\t\t\tpublic void setValues(PreparedStatement ps) throws SQLException {");
            newTexto.AppendLine("\t\t\t\tps.setString(1, Constants.DELETED_STATUS);");
            newTexto.AppendLine("\t\t\t\tps.setInt(2, tran.tranId);");
            newTexto.AppendLine("\t\t\t\tps.setTimestamp(3, DateTools.toTimestamp(tran.updatedDate));");
            newTexto.AppendLine("\t\t\t\tps.setString(4, tran.period);");
            newTexto.AppendLine("\t\t\t\tps.setInt(5, tran.updatedBy);");
            newTexto.AppendLine("\t\t\t\tps.setInt(6, id);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t});");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), pNameClaseIn + "Service.java", PathDesktop + "\\" + formulario.txtNamespaceInterface.Text.Replace("[tabla]", pNameClase).Replace("_", "").Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceInterface.Text.Replace("[tabla]", pNameClase).Replace("_", "").Replace(".", "\\") + "\\" + pNameClaseIn + "Service.java";
        }

        public string CrearInterfacesCS(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            try
            {
                string nameCol = null;
                string tipoCol = null;
                string paramClaseTransaccion = (string.IsNullOrEmpty(formulario.txtClaseTransaccion.Text) ? "cTransaccion" : formulario.txtClaseTransaccion.Text);

                string pNameTabla = pNameClase;

                pNameClase = pNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                             pNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                             pNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                             pNameTabla.Replace("_", "").Substring(4).ToLower();
                string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameClase;
                string pNameClaseIn = formulario.txtPrefijoInterface.Text + pNameClase;

                string PathDesktop = null;
                if (formulario.chkMoverResultados.Checked)
                {
                    PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoDal.Text;
                }
                else
                {
                    PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoDal.Text;
                }

                if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceInterface.Text) == false)
                {
                    Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceInterface.Text.Replace(".", "\\"));
                }

                string strLlavePrimaria = "";
                dynamic bEsPrimerElemento = true;
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                    {
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        tipoCol = dtColumns.Rows[iCol][1].ToString();

                        if (bEsPrimerElemento)
                        {
                            strLlavePrimaria = strLlavePrimaria + tipoCol + " " + tipoCol + nameCol;
                            bEsPrimerElemento = false;
                        }
                        else
                        {
                            strLlavePrimaria = strLlavePrimaria + ", " + tipoCol + " " + tipoCol + nameCol;
                        }
                    }
                }
                if (string.IsNullOrEmpty(strLlavePrimaria))
                {
                    strLlavePrimaria = dtColumns.Rows[0][1].ToString() + " " + dtColumns.Rows[0][1].ToString() + dtColumns.Rows[0][0].ToString();
                }

                System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
                string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;

                CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClaseIn);
                CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que define los metodos y operaciones sobre la Tabla " + pNameTabla);

                newTexto.AppendLine(CabeceraTmp);

                newTexto.AppendLine("");
                newTexto.AppendLine("#region");
                newTexto.AppendLine("using System;");
                newTexto.AppendLine("using System.Globalization;");
                newTexto.AppendLine("using System.Threading;");
                newTexto.AppendLine("using System.Numerics;");

                newTexto.AppendLine("using System.Collections;");
                newTexto.AppendLine("using System.Collections.Generic;");
                newTexto.AppendLine("using System.Data;");
                newTexto.AppendLine("using " + formulario.txtInfProyectoDal.Text + "; ");
                newTexto.AppendLine("using " + formulario.txtInfProyectoConn.Text + "; ");
                newTexto.AppendLine("using " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceEntidades.Text + ";");

                TipoEntorno miEntorno;
                Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);
                if (miEntorno == TipoEntorno.CSharp_WCF)
                {
                    newTexto.AppendLine("using System.ServiceModel;");
                }
                if (miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
                {
                    newTexto.AppendLine("using System.Windows.Forms;");
                }
                if (miEntorno == TipoEntorno.CSharp_WebForms || miEntorno == TipoEntorno.CSharp)
                {
                    newTexto.AppendLine("using System.Web.UI.WebControls;");
                }
                newTexto.AppendLine("#endregion");
                newTexto.AppendLine("");

                newTexto.AppendLine("namespace " + formulario.txtInfProyectoDal.Text + "." + formulario.txtNamespaceInterface.Text);

                newTexto.AppendLine("{");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                {
                    newTexto.AppendLine("\t[ServiceContract]");
                }

                newTexto.AppendLine("\tpublic interface " + pNameClaseIn + ": IDisposable");
                newTexto.AppendLine("\t{");

                if (miEntorno != TipoEntorno.CSharp_NetCore_V2)
                {
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"GetTableScript\")]");
                    newTexto.AppendLine("\t\tstring GetTableScript();");
                }

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"GetColumnType\")]");
                newTexto.AppendLine("\t\tdynamic GetColumnType(object valor," + pNameClaseEnt + ".Fields myField);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"GetColumnTypeStr\")]");
                newTexto.AppendLine("\t\tdynamic GetColumnType(object valor, string strField);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"SetDato\")]");
                newTexto.AppendLine("\t\tvoid SetDato(ref " + pNameClaseEnt + " obj, string strPropiedad, dynamic dynValor);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"GetDato\")]");
                newTexto.AppendLine("\t\tdynamic GetDato(ref " + pNameClaseEnt + " obj, string strPropiedad);");

                //If formulario.chkClassParcial.Checked Then
                //    If formulario.radWCF.Checked Then newTexto.AppendLine(vbTab & vbTab & "[OperationContract(Name = " + Chr(34) + "GetDesc" + Chr(34) + ")]")
                //    newTexto.AppendLine(vbTab & vbTab & "string GetDesc(" & pNameClaseEnt & ".Fields myField);")

                //    If formulario.radWCF.Checked Then newTexto.AppendLine(vbTab & vbTab & "[OperationContract(Name = " + Chr(34) + "GetError" + Chr(34) + ")]")
                //    newTexto.AppendLine(vbTab & vbTab & "string GetError(" & pNameClaseEnt & ".Fields myField);")

                //    If formulario.radWCF.Checked Then newTexto.AppendLine(vbTab & vbTab & "[OperationContract(Name = " + Chr(34) + "GetColumn" + Chr(34) + ")]")
                //    newTexto.AppendLine(vbTab & vbTab & "string GetColumn(" & pNameClaseEnt & ".Fields myField);")
                //End If

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerObjetoInsertado\")]");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjetoInsertado(string strUsuCre);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerObjetoStrStr\")]");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(" + strLlavePrimaria + ");");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerObjetoStrStrTrans\")]");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(" + strLlavePrimaria + ", ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerObjetoArrArr\")]");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerObjetoHashTable\")]");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(Hashtable htbFiltro);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerObjetoArrArrTrans\")]");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerObjetoHashTableTrans\")]");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(Hashtable htbFiltro, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerObjetoArrArrStr\")]");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerObjetoHashTableStr\")]");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerObjetoArrArrStrTrans\")]");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerObjetoHashTableStrTrans\")]");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno != TipoEntorno.CSharp_WCF)
                {
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerObjeto\")]");
                    newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(" + pNameClaseEnt + ".Fields searchField, object searchValue);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerObjetoTrans\")]");
                    newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(" + pNameClaseEnt + ".Fields searchField, object searchValue, ref " + paramClaseTransaccion + " localTrans);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerObjetoFldObjStr\")]");
                    newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerObjetoFldObjStrTrans\")]");
                    newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans);");
                }

                newTexto.AppendLine("\t\t");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerDiccionario\")]");
                newTexto.AppendLine("\t\tDictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario();");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerDiccionarioArrArr\")]");
                newTexto.AppendLine("\t\tDictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerDiccionarioArrArrTrans\")]");
                newTexto.AppendLine("\t\tDictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerDiccionarioArrArrStr\")]");
                newTexto.AppendLine("\t\tDictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerDiccionarioArrArrStrTrans\")]");
                newTexto.AppendLine("\t\tDictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno != TipoEntorno.CSharp_WCF)
                {
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerDiccionarioFldObj\")]");
                    newTexto.AppendLine("\t\tDictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario(" + pNameClaseEnt + ".Fields searchField, object searchValue);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerDiccionarioFldObjTrans\")]");
                    newTexto.AppendLine("\t\tDictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario(" + pNameClaseEnt + ".Fields searchField, object searchValue, ref " + paramClaseTransaccion + " localTrans);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerDiccionarioFldObjStr\")]");
                    newTexto.AppendLine("\t\tDictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerDiccionarioFldObjStrTrans\")]");
                    newTexto.AppendLine("\t\tDictionary<String, " + pNameClaseEnt + "> ObtenerDiccionario(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans);");
                }

                //If formulario.radWCF.Checked Then newTexto.AppendLine(vbTab & vbTab & "[OperationContract(Name = " + Chr(34) + "ObtenerDiccionarioArr" + Chr(34) + ")]")
                //newTexto.AppendLine(vbTab & vbTab & "Dictionary<String, " & pNameClaseEnt & "> ObtenerDiccionario(ArrayList arrColumnas);")

                //If formulario.radWCF.Checked Then newTexto.AppendLine(vbTab & vbTab & "[OperationContract(Name = " + Chr(34) + "ObtenerDiccionarioArrStr" + Chr(34) + ")]")
                //newTexto.AppendLine(vbTab & vbTab & "Dictionary<String, " & pNameClaseEnt & "> ObtenerDiccionario(ArrayList arrColumnas, string strParamAdicionales);")

                //If formulario.radWCF.Checked Then newTexto.AppendLine(vbTab & vbTab & "[OperationContract(Name = " + Chr(34) + "ObtenerDiccionarioArrTrans" + Chr(34) + ")]")
                //newTexto.AppendLine(vbTab & vbTab & "Dictionary<String, " & pNameClaseEnt & "> ObtenerDiccionario(ArrayList arrColumnas, ref " & paramClaseTransaccion & " localTrans);")

                //If formulario.radWCF.Checked Then newTexto.AppendLine(vbTab & vbTab & "[OperationContract(Name = " + Chr(34) + "ObtenerDiccionarioArrStrTrans" + Chr(34) + ")]")
                //newTexto.AppendLine(vbTab & vbTab & "Dictionary<String, " & pNameClaseEnt & "> ObtenerDiccionario(ArrayList arrColumnas, string strParamAdicionales, ref " & paramClaseTransaccion & " localTrans);")

                newTexto.AppendLine("\t\t");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerLista\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista();");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaArrArr\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaArrArrTrans\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaArrArrStr\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaArrArrStrTrans\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno != TipoEntorno.CSharp_WCF)
                {
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaFldObj\")]");
                    newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(" + pNameClaseEnt + ".Fields searchField, object searchValue);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaFldObjTrans\")]");
                    newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(" + pNameClaseEnt + ".Fields searchField, object searchValue, ref " + paramClaseTransaccion + " localTrans);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaFldObjStr\")]");
                    newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaFldObjStrTrans\")]");
                    newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans);");
                }

                newTexto.AppendLine("\t\t");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaHashTable\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(Hashtable htbFiltro);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaHashTableStr\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaHashTableTrans\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(Hashtable htbFiltro, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaHashTableStrTans\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans);");

                newTexto.AppendLine("\t\t");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaDesdeVista\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerListaDesdeVista(String strVista);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaDesdeVistaArrArr\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaDesdeVistaArrArrTrans\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaDesdeVistaArrArrStr\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaDesdeVistaArrArrStrTrans\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaDesdeVistaHashTable\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaDesdeVistaHashTableTrans\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaDesdeVistaHashTableStr\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaDesdeVistaHashTableStrTrans\")]");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno != TipoEntorno.CSharp_WCF)
                {
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaDesdeVistaFldObj\")]");
                    newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerListaDesdeVista(String strVista, " + pNameClaseEnt + ".Fields searchField, object searchValue);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaDesdeVistaFldObjTrans\")]");
                    newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerListaDesdeVista(String strVista, " + pNameClaseEnt + ".Fields searchField, object searchValue, ref " + paramClaseTransaccion + " localTrans);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaDesdeVistaFldObjStr\")]");
                    newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerListaDesdeVista(String strVista, " + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaDesdeVistaFldObjStrTrans\")]");
                    newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerListaDesdeVista(String strVista, " + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans);");
                }

                newTexto.AppendLine("\t\t");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerCola\")]");
                newTexto.AppendLine("\t\tQueue<" + pNameClaseEnt + "> ObtenerCola();");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerColaArrArr\")]");
                newTexto.AppendLine("\t\tQueue<" + pNameClaseEnt + "> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerColaArrArrTrans\")]");
                newTexto.AppendLine("\t\tQueue<" + pNameClaseEnt + "> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerColaArrArrStr\")]");
                newTexto.AppendLine("\t\tQueue<" + pNameClaseEnt + "> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerColaArrArrStrTrans\")]");
                newTexto.AppendLine("\t\tQueue<" + pNameClaseEnt + "> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno != TipoEntorno.CSharp_WCF)
                {
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerColaFldObj\")]");
                    newTexto.AppendLine("\t\tQueue<" + pNameClaseEnt + "> ObtenerCola(" + pNameClaseEnt + ".Fields searchField, object searchValue);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerColaFldObjTrans\")]");
                    newTexto.AppendLine("\t\tQueue<" + pNameClaseEnt + "> ObtenerCola(" + pNameClaseEnt + ".Fields searchField, object searchValue, ref " + paramClaseTransaccion + " localTrans);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerColaFldObjStr\")]");
                    newTexto.AppendLine("\t\tQueue<" + pNameClaseEnt + "> ObtenerCola(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerColaFldObjStrTrans\")]");
                    newTexto.AppendLine("\t\tQueue<" + pNameClaseEnt + "> ObtenerCola(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans);");
                }

                newTexto.AppendLine("\t\t");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerPila\")]");
                newTexto.AppendLine("\t\tStack<" + pNameClaseEnt + "> ObtenerPila();");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerPilaArrArr\")]");
                newTexto.AppendLine("\t\tStack<" + pNameClaseEnt + "> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerPilaArrArrTrans\")]");
                newTexto.AppendLine("\t\tStack<" + pNameClaseEnt + "> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerPilaArrArrStr\")]");
                newTexto.AppendLine("\t\tStack<" + pNameClaseEnt + "> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerPilaArrArrStrTrans\")]");
                newTexto.AppendLine("\t\tStack<" + pNameClaseEnt + "> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans);");

                if (miEntorno != TipoEntorno.CSharp_WCF)
                {
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerPilaFldObj\")]");
                    newTexto.AppendLine("\t\tStack<" + pNameClaseEnt + "> ObtenerPila(" + pNameClaseEnt + ".Fields searchField, object searchValue);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerPilaFldObjTrans\")]");
                    newTexto.AppendLine("\t\tStack<" + pNameClaseEnt + "> ObtenerPila(" + pNameClaseEnt + ".Fields searchField, object searchValue, ref " + paramClaseTransaccion + " localTrans);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerPilaFldObjStr\")]");
                    newTexto.AppendLine("\t\tStack<" + pNameClaseEnt + "> ObtenerPila(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerPilaFldObjStrTrans\")]");
                    newTexto.AppendLine("\t\tStack<" + pNameClaseEnt + "> ObtenerPila(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales, ref " + paramClaseTransaccion + " localTrans);");
                }

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"EjecutarSpDesdeObjeto\")]");
                newTexto.AppendLine("\t\tint EjecutarSpDesdeObjeto(string strNombreSp, " + pNameClaseEnt + " obj);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"EjecutarSpDesdeObjetoTrans\")]");
                newTexto.AppendLine("\t\tint EjecutarSpDesdeObjeto(string strNombreSp, " + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans);");

                //If formulario.radWCF.Checked Then newTexto.AppendLine(vbTab & vbTab & "[OperationContract(Name = " + Chr(34) + "ObtenerListaArr" + Chr(34) + ")]")
                //newTexto.AppendLine(vbTab & vbTab & "List<" & pNameClaseEnt & "> ObtenerLista(ArrayList arrColumnas);")

                //If formulario.radWCF.Checked Then newTexto.AppendLine(vbTab & vbTab & "[OperationContract(Name = " + Chr(34) + "ObtenerListaArrStr" + Chr(34) + ")]")
                //newTexto.AppendLine(vbTab & vbTab & "List<" & pNameClaseEnt & "> ObtenerLista(ArrayList arrColumnas, string strParamAdicionales);")

                //If formulario.radWCF.Checked Then newTexto.AppendLine(vbTab & vbTab & "[OperationContract(Name = " + Chr(34) + "ObtenerListaArrTrans" + Chr(34) + ")]")
                //newTexto.AppendLine(vbTab & vbTab & "List<" & pNameClaseEnt & "> ObtenerLista(ArrayList arrColumnas, ref " & paramClaseTransaccion & " localTrans);")

                //If formulario.radWCF.Checked Then newTexto.AppendLine(vbTab & vbTab & "[OperationContract(Name = " + Chr(34) + "ObtenerListaArrStrTrans" + Chr(34) + ")]")
                //newTexto.AppendLine(vbTab & vbTab & "List<" & pNameClaseEnt & "> ObtenerLista(ArrayList arrColumnas, string strParamAdicionales, ref " & paramClaseTransaccion & " localTrans);")

                newTexto.AppendLine("\t\t");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract]");
                newTexto.AppendLine("\t\tstring CreatePk(string[] args);");
                newTexto.AppendLine("\t\t");

                if (!formulario.chkUsarSps.Checked)
                {
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract]");
                    if (formulario.chkUsarSpsIdentity.Checked)
                    {
                        newTexto.AppendLine("\t\tbool Insert(ref " + pNameClaseEnt + " obj);");
                    }
                    else
                    {
                        newTexto.AppendLine("\t\tbool Insert(" + pNameClaseEnt + " obj);");
                    }

                    if (miEntorno != TipoEntorno.CSharp_WCF)
                    {
                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"InsertTrans\")]");
                        if (formulario.chkUsarSpsIdentity.Checked)
                        {
                            newTexto.AppendLine("\t\tbool Insert(ref " + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans);");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\tbool Insert(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans);");
                        }
                    }

                    if (formulario.chkUsarSps.Checked)
                    {
                        if (miEntorno == TipoEntorno.CSharp_WCF)
                        {
                            newTexto.AppendLine("\t\t[OperationContract]");
                        }
                        newTexto.AppendLine("\t\tint Update(" + pNameClaseEnt + " obj);");

                        if (miEntorno != TipoEntorno.CSharp_WCF)
                        {
                            if (miEntorno == TipoEntorno.CSharp_WCF)
                                newTexto.AppendLine("\t\t[OperationContract(Name = \"UpdateTrans\")]");
                            newTexto.AppendLine("\t\tint Update(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans);");
                        }
                    }
                    else
                    {
                        if (miEntorno == TipoEntorno.CSharp_WCF)
                        {
                            newTexto.AppendLine("\t\t[OperationContract]");
                        }
                        newTexto.AppendLine("\t\tint Update(" + pNameClaseEnt + " obj);");

                        if (miEntorno == TipoEntorno.CSharp_WCF)
                        {
                            newTexto.AppendLine("\t\t[OperationContract]");
                        }
                        newTexto.AppendLine("\t\tint UpdateAll(" + pNameClaseEnt + " obj);");

                        if (miEntorno != TipoEntorno.CSharp_WCF)
                        {
                            if (miEntorno == TipoEntorno.CSharp_WCF)
                            {
                                newTexto.AppendLine("\t\t[OperationContract(Name = \"UpdateTrans\")]");
                            }
                            newTexto.AppendLine("\t\tint Update(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans);");

                            if (miEntorno == TipoEntorno.CSharp_WCF)
                            {
                                newTexto.AppendLine("\t\t[OperationContract(Name = \"UpdateAllTrans\")]");
                            }
                            newTexto.AppendLine("\t\tint UpdateAll(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans);");
                        }
                    }

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract]");
                    newTexto.AppendLine("\t\tint Delete(" + pNameClaseEnt + " obj);");

                    if (miEntorno != TipoEntorno.CSharp_WCF)
                    {
                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"DeleteTrans\")]");
                        newTexto.AppendLine("\t\tint Delete(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans);");
                    }

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"DeleteArrArr\")]");
                    newTexto.AppendLine("\t\tint Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                    if (miEntorno != TipoEntorno.CSharp_WCF)
                    {
                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"DeleteArrArrTrans\")]");
                        newTexto.AppendLine("\t\tint Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref " + paramClaseTransaccion + " localTrans);");
                    }
                }
                else
                {
                    //Obtenemos el nombre del SP en base al prefijo
                    DataTable dtParametros = default(DataTable);
                    string prefijo = pNameTabla;
                    try
                    {
                        DataTable dtPrefijo = new DataTable();

                        switch (Principal.DBMS)
                        {
                            case TipoBD.SQLServer:
                                dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE tablaSegTablas = '" + pNameTabla + "'");
                                break;

                            case TipoBD.SQLServer2000:
                                dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE tablaSegTablas = '" + pNameTabla + "'");
                                break;

                            case TipoBD.Oracle:
                                dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT ALIASSTA FROM SEGTABLAS WHERE UPPER(TABLASTA) = '" + pNameTabla.ToUpper() + "'");
                                break;

                            case TipoBD.PostgreSQL:

                                dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.aliassta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" + pNameTabla + "')");

                                if (dtPrefijo.Rows.Count == 0)
                                {
                                    dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.alias FROM seg_tablas s WHERE UPPER(s.nombretabla) = UPPER('" + pNameTabla + "')");
                                }
                                break;
                        }

                        if (dtPrefijo.Rows.Count > 0)
                        {
                            prefijo = dtPrefijo.Rows[0][0].ToString().ToString();
                            prefijo = prefijo.Substring(0, 1).ToUpper() + prefijo.Substring(1, prefijo.Length - 1);
                        }
                        else
                        {
                            prefijo = pNameTabla;
                        }
                    }
                    catch (Exception ex)
                    {
                        prefijo = pNameTabla;
                    }

                    switch (formulario.cmbTipoColumnas.SelectedIndex)
                    {
                        case 0:
                            dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins", TipoColumnas.PrimeraMayuscula);
                            break;

                        case 1:
                            dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins", TipoColumnas.Minusculas);
                            break;

                        case 2:
                            dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins", TipoColumnas.Mayusculas);
                            break;
                    }
                    if (dtParametros.Rows.Count > 0)
                    {
                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract]");
                        newTexto.AppendLine("\t\tbool Insert(" + pNameClaseEnt + " obj, bool bValidar = true);");

                        if (miEntorno != TipoEntorno.CSharp_WCF)
                        {
                            if (miEntorno == TipoEntorno.CSharp_WCF)
                                newTexto.AppendLine("\t\t[OperationContract(Name = \"InsertTrans\")]");
                            newTexto.AppendLine("\t\tbool Insert(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans, bool bValidar = true);");
                        }
                    }

                    switch (formulario.cmbTipoColumnas.SelectedIndex)
                    {
                        case 0:
                            dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd", TipoColumnas.PrimeraMayuscula);
                            break;

                        case 1:
                            dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd", TipoColumnas.Minusculas);
                            break;

                        case 2:
                            dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd", TipoColumnas.Mayusculas);
                            break;
                    }
                    if (dtParametros.Rows.Count > 0)
                    {
                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract]");
                        newTexto.AppendLine("\t\tint Update(" + pNameClaseEnt + " obj, bool bValidar = true);");

                        if (miEntorno != TipoEntorno.CSharp_WCF)
                        {
                            if (miEntorno == TipoEntorno.CSharp_WCF)
                                newTexto.AppendLine("\t\t[OperationContract(Name = \"UpdateTrans\")]");
                            newTexto.AppendLine("\t\tint Update(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans, bool bValidar = true);");
                        }
                    }

                    switch (formulario.cmbTipoColumnas.SelectedIndex)
                    {
                        case 0:
                            dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del", TipoColumnas.PrimeraMayuscula);
                            break;

                        case 1:
                            dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del", TipoColumnas.Minusculas);
                            break;

                        case 2:
                            dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del", TipoColumnas.Mayusculas);
                            break;
                    }
                    if (dtParametros.Rows.Count > 0)
                    {
                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract]");
                        newTexto.AppendLine("\t\tint Delete(" + pNameClaseEnt + " obj, bool bValidar = true);");

                        if (miEntorno != TipoEntorno.CSharp_WCF)
                        {
                            if (miEntorno == TipoEntorno.CSharp_WCF)
                                newTexto.AppendLine("\t\t[OperationContract(Name = \"DeleteTrans\")]");
                            newTexto.AppendLine("\t\tint Delete(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans, bool bValidar = true);");
                        }
                    }

                    if (formulario.chkUsarSps.Checked)
                    {
                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract]");
                        newTexto.AppendLine("\t\tint InsertUpdate(" + pNameClaseEnt + " obj);");

                        if (miEntorno != TipoEntorno.CSharp_WCF)
                        {
                            if (miEntorno == TipoEntorno.CSharp_WCF)
                                newTexto.AppendLine("\t\t[OperationContract(Name = \"InsertUpdateTrans\")]");
                            newTexto.AppendLine("\t\tint InsertUpdate(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans);");
                        }
                    }
                }

                newTexto.AppendLine("\t\t");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"NuevoDataTable\")]");
                newTexto.AppendLine("\t\tDataTable NuevoDataTable();");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"NuevoDataTableArr\")]");
                newTexto.AppendLine("\t\tDataTable NuevoDataTable(ArrayList arrColumnas);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTable\")]");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTable();");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableStr\")]");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTable(String condicionesWhere);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableArr\")]");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTable(ArrayList arrColumnas);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableArrStr\")]");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableArrArrArr\")]");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableArrHashTable\")]");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableArrArr\")]");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableHashTable\")]");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTable(Hashtable htbFiltro);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableArrArrArrStr\")]");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableArrHashTableStr\")]");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableArrArrStr\")]");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableHashTableStr\")]");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);");

                if (miEntorno != TipoEntorno.CSharp_WCF)
                {
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableFldObj\")]");
                    newTexto.AppendLine("\t\tDataTable ObtenerDataTable(" + pNameClaseEnt + ".Fields searchField, object searchValue);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTable\")]");
                    newTexto.AppendLine("\t\tDataTable ObtenerDataTable(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableArrFldObj\")]");
                    newTexto.AppendLine("\t\tDataTable ObtenerDataTable(ArrayList arrColumnas, " + pNameClaseEnt + ".Fields searchField, object searchValue);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableArrFldObjStr\")]");
                    newTexto.AppendLine("\t\tDataTable ObtenerDataTable(ArrayList arrColumnas, " + pNameClaseEnt + ".Fields searchField, object searchValue, string strParametrosAdicionales);");
                }

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableOrArrArr\")]");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableOrArrArrArr\")]");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                if (miEntorno == TipoEntorno.CSharp_WCF)
                    newTexto.AppendLine("\t\t[OperationContract(Name = \"CFuncionesBDs.CargarDataTableOrArrArrArrStr\")]");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);");

                newTexto.AppendLine("\t\t");

                if (miEntorno != TipoEntorno.CSharp_WCF
                    && miEntorno != TipoEntorno.CSharp_NetCore_V2
                    && miEntorno != TipoEntorno.CSharp_NetCore_WebAPI_V2
                    && miEntorno != TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger
                    && miEntorno != TipoEntorno.CSharp_NetCore_V5)
                {
                    string tipoCombo = null;
                    string tipoComboMini = null;
                    string tipoGrid = null;
                    string tipoGridMini = null;
                    string nombreFuncionCombo = null;
                    string nombreFuncionGrid = null;
                    bool bRepetir = false;

                    if (miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
                    {
                        tipoCombo = "System.Windows.Forms.ComboBox";
                        tipoComboMini = "ComboBox";
                        tipoGrid = "System.Windows.Forms.DataGridView";
                        tipoGridMini = "DataGridView";
                        nombreFuncionCombo = "CargarComboBox";
                        nombreFuncionGrid = "CargarDataGrid";
                    }
                    else if (miEntorno == TipoEntorno.CSharp_WebForms)
                    {
                        tipoCombo = "System.Web.UI.WebControls.DropDownList";
                        tipoComboMini = "DropDownList";
                        tipoGrid = "System.Web.UI.WebControls.GridView";
                        tipoGridMini = "GridView";
                        nombreFuncionCombo = "CargarDropDownList";
                        nombreFuncionGrid = "CargarGridView";
                    }
                    else if (miEntorno == TipoEntorno.CSharp)
                    {
                        tipoCombo = "System.Web.UI.WebControls.DropDownList";
                        tipoComboMini = "DropDownList";
                        tipoGrid = "System.Web.UI.WebControls.GridView";
                        tipoGridMini = "GridView";
                        nombreFuncionCombo = "CargarDropDownList";
                        nombreFuncionGrid = "CargarGridView";

                        bRepetir = true;
                        //Revisar la linea 5666
                    }
                inicioComboGrid:

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionCombo + "\")]");
                    newTexto.AppendLine("\t\tvoid " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionCombo + "Str\")]");
                    newTexto.AppendLine("\t\tvoid " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, string strParamAdicionales);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionCombo + "ArrArr\")]");
                    newTexto.AppendLine("\t\tvoid " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionCombo + "ArrArrStr\")]");
                    newTexto.AppendLine("\t\tvoid " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);");

                    if (miEntorno != TipoEntorno.CSharp_WCF)
                    {
                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionCombo + "FldFld\")]");
                        newTexto.AppendLine("\t\tvoid " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, " + pNameClaseEnt + ".Fields textField);");

                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionCombo + "FldFldStr\")]");
                        newTexto.AppendLine("\t\tvoid " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, " + pNameClaseEnt + ".Fields textField, string strParamAdicionales);");

                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionCombo + "FldStr\")]");
                        newTexto.AppendLine("\t\tvoid " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, String textField);");

                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionCombo + "FldStrStr\")]");
                        newTexto.AppendLine("\t\tvoid " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, String textField, string strParamAdicionales);");

                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionCombo + "FldStrArrArr\")]");
                        newTexto.AppendLine("\t\tvoid " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionCombo + "FldStrArrArrStr\")]");
                        newTexto.AppendLine("\t\tvoid " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);");

                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionCombo + "FldStrFldObj\")]");
                        newTexto.AppendLine("\t\tvoid " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, String textField, " + pNameClaseEnt + ".Fields searchField, object searchValue);");

                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionCombo + "FldStrFldObjStr\")]");
                        newTexto.AppendLine("\t\tvoid " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, String textField, " + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales);");

                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionCombo + "FldFldArrArr\")]");
                        newTexto.AppendLine("\t\tvoid " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, " + pNameClaseEnt + ".Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionCombo + "FldFldArrArrStr\")]");
                        newTexto.AppendLine("\t\tvoid " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, " + pNameClaseEnt + ".Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);");

                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"CargarComboFldFldFldObj\")]");
                        newTexto.AppendLine("\t\tvoid " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, " + pNameClaseEnt + ".Fields textField, " + pNameClaseEnt + ".Fields searchField, object searchValue);");

                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionCombo + "FldFldFldObjStr\")]");
                        newTexto.AppendLine("\t\tvoid " + nombreFuncionCombo + "(ref " + tipoComboMini + " cmb, " + pNameClaseEnt + ".Fields valueField, " + pNameClaseEnt + ".Fields textField, " + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales);");
                    }

                    newTexto.AppendLine("\t\t");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionGrid + "\")]");
                    newTexto.AppendLine("\t\tvoid " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionGrid + "Str\")]");
                    newTexto.AppendLine("\t\tvoid " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, String condicionesWhere);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionGrid + "Arr\")]");
                    newTexto.AppendLine("\t\tvoid " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, ArrayList arrColumnas);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionGrid + "ArrStr\")]");
                    newTexto.AppendLine("\t\tvoid " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, ArrayList arrColumnas, String condicionesWhere);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionGrid + "ArrArr\")]");
                    newTexto.AppendLine("\t\tvoid " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionGrid + "ArrArrStr\")]");
                    newTexto.AppendLine("\t\tvoid " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionGrid + "ArrArrArr\")]");
                    newTexto.AppendLine("\t\tvoid " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionGrid + "ArrArrArrStr\")]");
                    newTexto.AppendLine("\t\tvoid " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);");

                    if (miEntorno != TipoEntorno.CSharp_WCF)
                    {
                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionGrid + "FldObj\")]");
                        newTexto.AppendLine("\t\tvoid " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, " + pNameClaseEnt + ".Fields searchField, object searchValue);");

                        if (miEntorno == TipoEntorno.CSharp_WCF)
                            newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionGrid + "FldObjStr\")]");
                        newTexto.AppendLine("\t\tvoid " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, " + pNameClaseEnt + ".Fields searchField, object searchValue, string strParametrosAdicionales);");
                    }

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionGrid + "OrArrArrArr\")]");
                    newTexto.AppendLine("\t\tvoid " + nombreFuncionGrid + "Or(ref " + tipoGridMini + " dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionGrid + "OrArrArrArrStr\")]");
                    newTexto.AppendLine("\t\tvoid " + nombreFuncionGrid + "Or(ref " + tipoGridMini + " dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionGrid + "ArrFldObj\")]");
                    newTexto.AppendLine("\t\tvoid " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, ArrayList arrColumnas, " + pNameClaseEnt + ".Fields searchField, object searchValue);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"" + nombreFuncionGrid + "ArrFldObjStr\")]");
                    newTexto.AppendLine("\t\tvoid " + nombreFuncionGrid + "(ref " + tipoGridMini + " dtg, ArrayList arrColumnas, " + pNameClaseEnt + ".Fields searchField, object searchValue, string strParametrosAdicionales);");

                    newTexto.AppendLine("\t\t");

                    if (miEntorno == TipoEntorno.CSharp && bRepetir)
                    {
                        bRepetir = false;
                        tipoCombo = "System.Windows.Forms.ComboBox";
                        tipoComboMini = "ComboBox";
                        tipoGrid = "System.Windows.Forms.DataGridView";
                        tipoGridMini = "DataGridView";
                        nombreFuncionCombo = "CargarComboBox";
                        nombreFuncionGrid = "CargarDataGrid";

                        goto inicioComboGrid;
                    }
                }

                if (miEntorno != TipoEntorno.CSharp_WCF)
                {
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"FuncionesCount\")]");
                    newTexto.AppendLine("\t\tint FuncionesCount(" + pNameClaseEnt + ".Fields refField);");
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"FuncionesCountFields\")]");
                    newTexto.AppendLine("\t\tint FuncionesCount(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField);");
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"FuncionesCountArrArr\")]");
                    newTexto.AppendLine("\t\tint FuncionesCount(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"FuncionesMin\")]");
                    newTexto.AppendLine("\t\tint FuncionesMin(" + pNameClaseEnt + ".Fields refField);");
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"FuncionesMinFields\")]");
                    newTexto.AppendLine("\t\tint FuncionesMin(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField);");
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"FuncionesMinArrArr\")]");
                    newTexto.AppendLine("\t\tint FuncionesMin(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"FuncionesMax\")]");
                    newTexto.AppendLine("\t\tint FuncionesMax(" + pNameClaseEnt + ".Fields refField);");
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"FuncionesMaxFields\")]");
                    newTexto.AppendLine("\t\tint FuncionesMax(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField);");
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"FuncionesMaxArrArr\")]");
                    newTexto.AppendLine("\t\tint FuncionesMax(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"FuncionesSum\")]");
                    newTexto.AppendLine("\t\tint FuncionesSum(" + pNameClaseEnt + ".Fields refField);");
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"FuncionesSumFields\")]");
                    newTexto.AppendLine("\t\tint FuncionesSum(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField);");
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"FuncionesSumArrArr\")]");
                    newTexto.AppendLine("\t\tint FuncionesSum(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"FuncionesAvg\")]");
                    newTexto.AppendLine("\t\tint FuncionesAvg(" + pNameClaseEnt + ".Fields refField);");
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"FuncionesAvgFields\")]");
                    newTexto.AppendLine("\t\tint FuncionesAvg(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField);");
                    if (miEntorno == TipoEntorno.CSharp_WCF)
                        newTexto.AppendLine("\t\t[OperationContract(Name = \"FuncionesAvgArrArr\")]");
                    newTexto.AppendLine("\t\tint FuncionesAvg(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");
                }

                newTexto.AppendLine("\t}");
                newTexto.AppendLine("}");

                CFunciones.CrearArchivo(newTexto.ToString(), pNameClaseIn + ".cs", PathDesktop + "\\" + formulario.txtNamespaceInterface.Text.Replace(".", "\\") + "\\");

                //Return newTexto.ToString()
                return PathDesktop + "\\" + formulario.txtNamespaceInterface.Text.Replace(".", "\\") + "\\" + pNameClaseIn + ".cs";
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public string CrearInterfacesCSWebClient(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            try
            {
                string nameCol = null;
                string tipoCol = null;
                string paramClaseTransaccion = (string.IsNullOrEmpty(formulario.txtClaseTransaccion.Text) ? "cTransaccion" : formulario.txtClaseTransaccion.Text);

                string pNameTabla = pNameClase;

                pNameClase = pNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                             pNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                             pNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                             pNameTabla.Replace("_", "").Substring(4).ToLower();
                string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameClase;
                string pNameClaseIn = formulario.txtPrefijoInterface.Text + pNameClase;

                string PathDesktop = null;
                if (formulario.chkMoverResultados.Checked)
                {
                    PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoDal.Text;
                }
                else
                {
                    PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoDal.Text;
                }

                if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceInterface.Text) == false)
                {
                    Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceInterface.Text.Replace(".", "\\"));
                }

                string strLlavePrimaria = "";
                dynamic bEsPrimerElemento = true;
                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                    {
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        tipoCol = dtColumns.Rows[iCol][1].ToString();

                        if (bEsPrimerElemento)
                        {
                            strLlavePrimaria = strLlavePrimaria + tipoCol + " " + tipoCol + nameCol;
                            bEsPrimerElemento = false;
                        }
                        else
                        {
                            strLlavePrimaria = strLlavePrimaria + ", " + tipoCol + " " + tipoCol + nameCol;
                        }
                    }
                }
                if (string.IsNullOrEmpty(strLlavePrimaria))
                {
                    strLlavePrimaria = dtColumns.Rows[0][1].ToString() + " " + dtColumns.Rows[0][1].ToString() + dtColumns.Rows[0][0].ToString();
                }

                System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
                string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;

                CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClaseIn);
                CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que define los metodos y operaciones sobre la Tabla " + pNameTabla);

                newTexto.AppendLine(CabeceraTmp);

                newTexto.AppendLine("");
                newTexto.AppendLine("#region");
                newTexto.AppendLine("using System;");
                newTexto.AppendLine("using System.Globalization;");
                newTexto.AppendLine("using System.Threading;");
                newTexto.AppendLine("using System.Numerics;");

                newTexto.AppendLine("using System.Collections;");
                newTexto.AppendLine("using System.Collections.Generic;");
                newTexto.AppendLine("using System.Data;");
                newTexto.AppendLine("using " + formulario.txtInfProyectoDal.Text + "; ");
                newTexto.AppendLine("using " + formulario.txtInfProyectoConn.Text + "; ");
                newTexto.AppendLine("using " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceEntidades.Text + ";");

                TipoEntorno miEntorno;
                Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);
                if (miEntorno == TipoEntorno.CSharp_WCF)
                {
                    newTexto.AppendLine("using System.ServiceModel;");
                }
                if (miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
                {
                    newTexto.AppendLine("using System.Windows.Forms;");
                }
                if (miEntorno == TipoEntorno.CSharp_WebForms || miEntorno == TipoEntorno.CSharp)
                {
                    newTexto.AppendLine("using System.Web.UI.WebControls;");
                }
                newTexto.AppendLine("#endregion");
                newTexto.AppendLine("");

                newTexto.AppendLine("namespace " + formulario.txtInfProyectoDal.Text + "." + formulario.txtNamespaceInterface.Text);

                newTexto.AppendLine("{");                
                newTexto.AppendLine("\tpublic interface " + pNameClaseIn + ": IDisposable");
                newTexto.AppendLine("\t{");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(" + strLlavePrimaria + ");");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(" + pNameClaseEnt + ".Fields searchField, object searchValue);");
                newTexto.AppendLine("\t\t" + pNameClaseEnt + " ObtenerObjeto(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales);");
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista();");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(" + pNameClaseEnt + ".Fields searchField, object searchValue);");
                newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales);");
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\tbool Insert(" + pNameClaseEnt + " obj);");
                newTexto.AppendLine("\t\tbool Update(" + pNameClaseEnt + " obj);");
                newTexto.AppendLine("\t\tbool Delete(" + pNameClaseEnt + " obj);");
                newTexto.AppendLine("\t\tbool Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTable(String condicionesWhere);");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTable(" + pNameClaseEnt + ".Fields searchField, object searchValue);");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTable(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParamAdicionales);");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");
                newTexto.AppendLine("\t\tDataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");
                newTexto.AppendLine("\t\t");
                newTexto.AppendLine("\t\tint FuncionesCount(" + pNameClaseEnt + ".Fields refField);");
                newTexto.AppendLine("\t\tint FuncionesCount(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField);");
                newTexto.AppendLine("\t\tint FuncionesCount(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");
                newTexto.AppendLine("\t\tdouble FuncionesMin(" + pNameClaseEnt + ".Fields refField);");
                newTexto.AppendLine("\t\tdouble FuncionesMin(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField);");
                newTexto.AppendLine("\t\tdouble FuncionesMin(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");
                newTexto.AppendLine("\t\tdouble FuncionesMax(" + pNameClaseEnt + ".Fields refField);");
                newTexto.AppendLine("\t\tdouble FuncionesMax(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField);");
                newTexto.AppendLine("\t\tdouble FuncionesMax(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");
                newTexto.AppendLine("\t\tdouble FuncionesSum(" + pNameClaseEnt + ".Fields refField);");
                newTexto.AppendLine("\t\tdouble FuncionesSum(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField);");
                newTexto.AppendLine("\t\tdouble FuncionesSum(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");
                newTexto.AppendLine("\t\tdouble FuncionesAvg(" + pNameClaseEnt + ".Fields refField);");
                newTexto.AppendLine("\t\tdouble FuncionesAvg(" + pNameClaseEnt + ".Fields refField, " + pNameClaseEnt + ".Fields whereField, object valueField);");
                newTexto.AppendLine("\t\tdouble FuncionesAvg(" + pNameClaseEnt + ".Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");
                newTexto.AppendLine("}");

                CFunciones.CrearArchivo(newTexto.ToString(), pNameClaseIn + ".cs", PathDesktop + "\\" + formulario.txtNamespaceInterface.Text.Replace(".", "\\") + "\\");

                //Return newTexto.ToString()
                return PathDesktop + "\\" + formulario.txtNamespaceInterface.Text.Replace(".", "\\") + "\\" + pNameClaseIn + ".cs";
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public string CrearInterfacesStoredProcedure(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string nameCol = null;
            string tipoCol = null;
            string paramClaseTransaccion = (string.IsNullOrEmpty(formulario.txtClaseTransaccion.Text) ? "cTransaccion" : formulario.txtClaseTransaccion.Text);

            string pNameTabla = pNameClase.Replace("_", "");
            //pNameTabla = pNameTabla.Substring(0, 1).ToUpper & pNameTabla.Substring(1, 2).ToLower & pNameTabla.Substring(3, 1).ToUpper & pNameTabla.Substring(4).ToLower

            pNameClase = formulario.txtPrefijoInterface.Text + pNameTabla;
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameTabla;

            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoDal.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoDal.Text;
            }

            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceInterface.Text) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceInterface.Text.Replace(".", "\\"));
            }

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase);
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que define los metodos y operaciones sobre la Tabla " + pNameTabla);

            newTexto.AppendLine(CabeceraTmp);

            newTexto.AppendLine("");
            newTexto.AppendLine("#region");
            newTexto.AppendLine("using System;");
            newTexto.AppendLine("using System.Web.UI.WebControls;");
            newTexto.AppendLine("using System.Collections;");
            newTexto.AppendLine("using System.Collections.Generic;");
            newTexto.AppendLine("using System.Data;");
            newTexto.AppendLine("using " + formulario.txtInfProyectoDal.Text + "; ");
            newTexto.AppendLine("using " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceEntidades.Text + ";");

            TipoEntorno miEntorno;
            Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);
            if (miEntorno == TipoEntorno.CSharp_WCF)
            {
                newTexto.AppendLine("using System.ServiceModel;");
            }
            newTexto.AppendLine("#endregion");
            newTexto.AppendLine("");

            newTexto.AppendLine("namespace " + formulario.txtInfProyectoDal.Text + "." + formulario.txtNamespaceInterface.Text);
            newTexto.AppendLine("{");

            if (miEntorno == TipoEntorno.CSharp_WCF)
            {
                newTexto.AppendLine("\t[ServiceContract]");
            }

            newTexto.AppendLine("\tpublic interface " + pNameClase);
            newTexto.AppendLine("\t{");

            string strLlavePrimaria = "";
            dynamic bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();

                    if (bEsPrimerElemento)
                    {
                        strLlavePrimaria = strLlavePrimaria + tipoCol + " " + tipoCol + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlavePrimaria = strLlavePrimaria + ", " + tipoCol + " " + tipoCol + nameCol;
                    }
                }
            }

            if (miEntorno == TipoEntorno.CSharp_WCF)
                newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerLista\")]");
            newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista();");

            if (miEntorno == TipoEntorno.CSharp_WCF)
                newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaArrArr\")]");
            newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);");

            if (miEntorno == TipoEntorno.CSharp_WCF)
                newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaArrArrTrans\")]");
            newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref " + paramClaseTransaccion + " localTrans);");

            if (miEntorno == TipoEntorno.CSharp_WCF)
                newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaArr\")]");
            newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(ArrayList arrValoresWhere);");

            if (miEntorno == TipoEntorno.CSharp_WCF)
                newTexto.AppendLine("\t\t[OperationContract(Name = \"ObtenerListaArrTrans\")]");
            newTexto.AppendLine("\t\tList<" + pNameClaseEnt + "> ObtenerLista(ArrayList arrValoresWhere, ref " + paramClaseTransaccion + " localTrans);");

            newTexto.AppendLine("\t}");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), pNameClase + ".cs", PathDesktop + "\\" + formulario.txtNamespaceInterface.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceInterface.Text.Replace(".", "\\") + "\\" + pNameClase + ".cs";
        }

        public string CrearInterfacesVB(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string nameCol = null;
            string tipoCol = null;
            string paramClaseTransaccion = (string.IsNullOrEmpty(formulario.txtClaseTransaccion.Text) ? "cTransaccion" : formulario.txtClaseTransaccion.Text);

            string pNameTabla = pNameClase.Replace("_", "");
            pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();

            pNameClase = formulario.txtPrefijoInterface.Text + pNameTabla;
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameTabla;

            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoDal.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoDal.Text;
            }

            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceInterface.Text) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceInterface.Text.Replace(".", "\\"));
            }

            ArrayList llavePrimaria = new ArrayList();
            string strLlavePrimaria = string.Empty;
            dynamic bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();

                    if (bEsPrimerElemento)
                    {
                        strLlavePrimaria = strLlavePrimaria + "ByVal " + tipoCol + nameCol + " As " + tipoCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlavePrimaria = strLlavePrimaria + ", ByVal " + tipoCol + nameCol + " As " + tipoCol;
                    }

                    llavePrimaria.Add(tipoCol + nameCol);
                }
            }
            if (string.IsNullOrEmpty(strLlavePrimaria))
            {
                strLlavePrimaria = "ByVal " + dtColumns.Rows[0][1].ToString() + dtColumns.Rows[0][0].ToString() + " As " + dtColumns.Rows[0][1].ToString();
                llavePrimaria.Add(dtColumns.Rows[0][1].ToString() + dtColumns.Rows[0][0].ToString());
            }

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase);
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que define los metodos y operaciones sobre la Tabla " + pNameTabla);

            //newTexto.AppendLine(CabeceraTmp)

            newTexto.AppendLine("#Region \"Imports\"");
            newTexto.AppendLine("Imports System");
            newTexto.AppendLine("Imports System.Numerics");

            newTexto.AppendLine("Imports System.Collections");
            newTexto.AppendLine("Imports System.Collections.Generic");
            newTexto.AppendLine("Imports System.Data");
            newTexto.AppendLine("Imports " + formulario.txtInfProyectoDal.Text + "");
            newTexto.AppendLine("Imports " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceEntidades.Text + "");
            TipoEntorno miEntorno;
            Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);
            if (miEntorno == TipoEntorno.CSharp_WCF)
            {
                newTexto.AppendLine("Imports System.ServiceModel");
            }
            if (miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
            {
                newTexto.AppendLine("Imports System.Windows.Forms");
            }
            if (miEntorno == TipoEntorno.CSharp_WebForms)
            {
                newTexto.AppendLine("Imports System.Web.UI.WebControls");
            }

            newTexto.AppendLine("#End Region");
            newTexto.AppendLine("");

            //newTexto.AppendLine("Namespace " & formulario.txtNamespaceInterface.Text)

            newTexto.AppendLine("\tPublic Interface " + pNameClase);

            newTexto.AppendLine("\t\tFunction GetDesc(ByVal myField As " + pNameClaseEnt + ".Fields) As String");
            newTexto.AppendLine("\t\tFunction GetError(ByVal myField As " + pNameClaseEnt + ".Fields) As String");
            newTexto.AppendLine("\t\tFunction GetColumn(ByVal myField As " + pNameClaseEnt + ".Fields) As String");
            newTexto.AppendLine("\t\tFunction ObtenerObjeto(" + strLlavePrimaria + ") As " + pNameClaseEnt);
            newTexto.AppendLine("\t\tFunction ObtenerObjeto(" + strLlavePrimaria + ", ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt);
            newTexto.AppendLine("\t\tFunction ObtenerObjeto(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As " + pNameClaseEnt);
            newTexto.AppendLine("\t\tFunction ObtenerObjeto(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt);
            newTexto.AppendLine("\t\tFunction ObtenerObjeto(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String) As " + pNameClaseEnt);
            newTexto.AppendLine("\t\tFunction ObtenerObjeto(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String, ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt);

            newTexto.AppendLine("\t\tFunction ObtenerObjeto(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object) As " + pNameClaseEnt);
            newTexto.AppendLine("\t\tFunction ObtenerObjeto(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt);
            newTexto.AppendLine("\t\tFunction ObtenerObjeto(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String) As " + pNameClaseEnt);
            newTexto.AppendLine("\t\tFunction ObtenerObjeto(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String, ByRef localTrans As " + paramClaseTransaccion + ") As " + pNameClaseEnt);

            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t\tFunction ObtenerLista() As List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tFunction ObtenerLista(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tFunction ObtenerLista(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tFunction ObtenerLista(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String) As List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tFunction ObtenerLista(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tFunction ObtenerLista(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object) As List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tFunction ObtenerLista(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tFunction ObtenerLista(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String) As List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tFunction ObtenerLista(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tFunction ObtenerLista(ByVal arrColumnas As ArrayList) As List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tFunction ObtenerLista(ByVal arrColumnas As ArrayList, ByVal strParamAdicionales As String) As List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tFunction ObtenerLista(ByVal arrColumnas As ArrayList, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ")");
            newTexto.AppendLine("\t\tFunction ObtenerLista(ByVal arrColumnas As ArrayList, ByVal strParamAdicionales As String, ByRef localTrans As " + paramClaseTransaccion + ") As List(Of " + pNameClaseEnt + ")");

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tFunction CreatePk(ByVal args() As String) As String");
            newTexto.AppendLine("\t\t");

            if (!formulario.chkUsarSps.Checked)
            {
                if (formulario.chkUsarSpsIdentity.Checked)
                {
                    newTexto.AppendLine("\t\tFunction Insert(ByRef obj As " + pNameClaseEnt + ") As Boolean");
                }
                else
                {
                    newTexto.AppendLine("\t\tFunction Insert(ByVal obj As " + pNameClaseEnt + ") As Boolean");
                }

                if (formulario.chkUsarSpsIdentity.Checked)
                {
                    newTexto.AppendLine("\t\tFunction Insert(ByRef obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Boolean");
                }
                else
                {
                    newTexto.AppendLine("\t\tFunction Insert(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Boolean");
                }

                newTexto.AppendLine("\t\tFunction Update(ByVal obj As " + pNameClaseEnt + ") As Integer");
                newTexto.AppendLine("\t\tFunction Update(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Integer");
                newTexto.AppendLine("\t\tFunction Delete(ByVal obj As " + pNameClaseEnt + ") As Integer");
                newTexto.AppendLine("\t\tFunction Delete(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Integer");
                newTexto.AppendLine("\t\tFunction Delete(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As Integer");
                newTexto.AppendLine("\t\tFunction Delete(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByRef localTrans As " + paramClaseTransaccion + ") As Integer");
            }
            else
            {
                //Obtenemos el nombre del SP en base al prefijo
                DataTable dtParametros = default(DataTable);
                string prefijo = pNameTabla;
                try
                {
                    DataTable dtPrefijo = new DataTable();

                    switch (Principal.DBMS)
                    {
                        case TipoBD.SQLServer:
                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE tablaSegTablas = '" + pNameTabla + "'");
                            break;

                        case TipoBD.SQLServer2000:
                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE tablaSegTablas = '" + pNameTabla + "'");
                            break;

                        case TipoBD.Oracle:
                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT ALIASSTA FROM SEGTABLAS WHERE UPPER(TABLASTA) = '" + pNameTabla.ToUpper() + "'");
                            break;

                        case TipoBD.PostgreSQL:
                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.aliassta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" + pNameTabla + "')");
                            break;
                    }

                    if (dtPrefijo.Rows.Count > 0)
                    {
                        prefijo = dtPrefijo.Rows[0][0].ToString().ToString();
                        prefijo = prefijo.Substring(0, 1).ToUpper() + prefijo.Substring(1, prefijo.Length - 1);
                    }
                    else
                    {
                        prefijo = pNameTabla;
                    }
                }
                catch (Exception ex)
                {
                    prefijo = pNameTabla;
                }

                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins", TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins", TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins", TipoColumnas.Mayusculas);
                        break;
                }
                if (dtParametros.Rows.Count > 0)
                {
                    newTexto.AppendLine("\t\tFunction Insert(ByVal obj As " + pNameClaseEnt + ") As Boolean");
                    newTexto.AppendLine("\t\tFunction Insert(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Boolean");
                }

                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd", TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd", TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd", TipoColumnas.Mayusculas);
                        break;
                }
                if (dtParametros.Rows.Count > 0)
                {
                    newTexto.AppendLine("\t\tFunction Update(ByVal obj As " + pNameClaseEnt + ") As Integer");
                    newTexto.AppendLine("\t\tFunction Update(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Integer");
                }

                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del", TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del", TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del", TipoColumnas.Mayusculas);
                        break;
                }
                if (dtParametros.Rows.Count > 0)
                {
                    newTexto.AppendLine("\t\tFunction Delete(ByVal obj As " + pNameClaseEnt + ") As Integer");
                    newTexto.AppendLine("\t\tFunction Delete(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Integer");
                }

                if (formulario.chkUsarSps.Checked)
                {
                    newTexto.AppendLine("\t\tFunction InsertUpdate(ByVal obj As " + pNameClaseEnt + ") As Integer");
                    newTexto.AppendLine("\t\tFunction InsertUpdate(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Integer");
                }
            }

            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t\tFunction NuevoDataTable() As DataTable");
            newTexto.AppendLine("\t\tFunction NuevoDataTable(ByVal arrColumnas As ArrayList) As DataTable");
            newTexto.AppendLine("\t\tFunction CFuncionesBDs.CargarDataTable() As DataTable");
            newTexto.AppendLine("\t\tFunction CFuncionesBDs.CargarDataTable(ByVal condicionesWhere As String) As DataTable");
            newTexto.AppendLine("\t\tFunction CFuncionesBDs.CargarDataTable(ByVal arrColumnas As ArrayList) As DataTable");
            newTexto.AppendLine("\t\tFunction CFuncionesBDs.CargarDataTable(ByVal arrColumnas As ArrayList, ByVal strParametrosAdicionales As String) As DataTable");
            newTexto.AppendLine("\t\tFunction CFuncionesBDs.CargarDataTable(ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As DataTable");
            newTexto.AppendLine("\t\tFunction CFuncionesBDs.CargarDataTable(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As DataTable");
            newTexto.AppendLine("\t\tFunction CFuncionesBDs.CargarDataTable(ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String) As DataTable");
            newTexto.AppendLine("\t\tFunction CFuncionesBDs.CargarDataTable(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String) As DataTable");
            newTexto.AppendLine("\t\tFunction CFuncionesBDs.CargarDataTable(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object) As DataTable");
            newTexto.AppendLine("\t\tFunction CFuncionesBDs.CargarDataTable(ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParametrosAdicionales As String) As DataTable");
            newTexto.AppendLine("\t\tFunction CFuncionesBDs.CargarDataTable(ByVal arrColumnas As ArrayList, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object) As DataTable");
            newTexto.AppendLine("\t\tFunction CFuncionesBDs.CargarDataTable(ByVal arrColumnas As ArrayList, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParametrosAdicionales As String) As DataTable");

            newTexto.AppendLine("\t\tFunction CFuncionesBDs.CargarDataTableOr(ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As DataTable");
            newTexto.AppendLine("\t\tFunction CFuncionesBDs.CargarDataTableOr(ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As DataTable");
            newTexto.AppendLine("\t\tFunction CFuncionesBDs.CargarDataTableOr(ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String) As DataTable");

            newTexto.AppendLine("\t\t");

            string tipoComboMini = "";
            string tipoGridMini = "";

            if (miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
            {
                tipoComboMini = "ComboBox";
                tipoGridMini = "DataGridView";
            }
            else
            {
                tipoComboMini = "DropDownList";
                tipoGridMini = "GridView";
            }

            newTexto.AppendLine("\t\tSub CargarCombo(ByRef cmb As " + tipoComboMini + ")");
            newTexto.AppendLine("\t\tSub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal strParamAdicionales As String)");
            newTexto.AppendLine("\t\tSub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList)");
            newTexto.AppendLine("\t\tSub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String)");
            newTexto.AppendLine("\t\tSub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields)");
            newTexto.AppendLine("\t\tSub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields, ByVal strParamAdicionales As String)");
            newTexto.AppendLine("\t\tSub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String)");
            newTexto.AppendLine("\t\tSub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String, ByVal strParamAdicionales As String)");
            newTexto.AppendLine("\t\tSub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList)");
            newTexto.AppendLine("\t\tSub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String)");
            newTexto.AppendLine("\t\tSub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String, ByVal searchField As " + pNameClaseEnt + ".Fields , ByVal searchValue As Object)");
            newTexto.AppendLine("\t\tSub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As String, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String)");
            newTexto.AppendLine("\t\tSub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList)");
            newTexto.AppendLine("\t\tSub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParamAdicionales As String)");
            newTexto.AppendLine("\t\tSub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object)");
            newTexto.AppendLine("\t\tSub CargarCombo(ByRef cmb As " + tipoComboMini + ", ByVal valueField As " + pNameClaseEnt + ".Fields, ByVal textField As " + pNameClaseEnt + ".Fields, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParamAdicionales As String)");

            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t\tSub CargarGridView(ByRef dtg As " + tipoGridMini + ")");
            newTexto.AppendLine("\t\tSub CargarGridView(ByRef dtg As  " + tipoGridMini + ", ByVal condicionesWhere As String)");
            newTexto.AppendLine("\t\tSub CargarGridView(ByRef dtg As  " + tipoGridMini + ", ByVal arrColumnas As ArrayList)");
            newTexto.AppendLine("\t\tSub CargarGridView(ByRef dtg As  " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal strParametrosAdicionales As String)");
            newTexto.AppendLine("\t\tSub CargarGridView(ByRef dtg As  " + tipoGridMini + ", ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList)");
            newTexto.AppendLine("\t\tSub CargarGridView(ByRef dtg As  " + tipoGridMini + ", ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String)");
            newTexto.AppendLine("\t\tSub CargarGridView(ByRef dtg As  " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList)");
            newTexto.AppendLine("\t\tSub CargarGridView(ByRef dtg As  " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String)");
            newTexto.AppendLine("\t\tSub CargarGridView(ByRef dtg As  " + tipoGridMini + ", ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object)");
            newTexto.AppendLine("\t\tSub CargarGridView(ByRef dtg As  " + tipoGridMini + ", ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParametrosAdicionales As String)");

            newTexto.AppendLine("\t\tSub CargarGridViewOr(ByRef dtg As  " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList)");
            newTexto.AppendLine("\t\tSub CargarGridViewOr(ByRef dtg As  " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList, ByVal strParametrosAdicionales As String)");
            newTexto.AppendLine("\t\tSub CargarGridView(ByRef dtg As  " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object)");
            newTexto.AppendLine("\t\tSub CargarGridView(ByRef dtg As  " + tipoGridMini + ", ByVal arrColumnas As ArrayList, ByVal searchField As " + pNameClaseEnt + ".Fields, ByVal searchValue As Object, ByVal strParametrosAdicionales As String)");

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tFunction FuncionesCount(ByVal refField As " + pNameClaseEnt + ".Fields) As Object");
            newTexto.AppendLine("\t\tFunction FuncionesCount(ByVal refField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As Object");
            newTexto.AppendLine("\t\tFunction FuncionesMin(ByVal refField As " + pNameClaseEnt + ".Fields) As Object");
            newTexto.AppendLine("\t\tFunction FuncionesMin(ByVal refField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As Object");
            newTexto.AppendLine("\t\tFunction FuncionesMax(ByVal refField As " + pNameClaseEnt + ".Fields) As Object");
            newTexto.AppendLine("\t\tFunction FuncionesMax(ByVal refField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As Object");
            newTexto.AppendLine("\t\tFunction FuncionesSum(ByVal refField As " + pNameClaseEnt + ".Fields) As Object");
            newTexto.AppendLine("\t\tFunction FuncionesSum(ByVal refField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As Object");
            newTexto.AppendLine("\t\tFunction FuncionesAvg(ByVal refField As " + pNameClaseEnt + ".Fields) As Object");
            newTexto.AppendLine("\t\tFunction FuncionesAvg(ByVal refField As " + pNameClaseEnt + ".Fields, ByVal arrColumnasWhere As ArrayList, ByVal arrValoresWhere As ArrayList) As Object");

            newTexto.AppendLine("\tEnd Interface");
            //newTexto.AppendLine("End Namespace")

            CFunciones.CrearArchivo(newTexto.ToString(), pNameClase + ".vb", PathDesktop + "\\" + formulario.txtNamespaceInterface.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceInterface.Text.Replace(".", "\\") + "\\" + pNameClase + ".vb";
        }
    }
}