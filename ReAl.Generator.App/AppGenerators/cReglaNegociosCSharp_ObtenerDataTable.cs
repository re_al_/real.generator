﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using System;
using System.Collections;
using System.Data;

namespace ReAl.Generator.App.AppGenerators
{
    public static class cReglaNegociosCSharp_ObtenerDataTable
    {
        public static string CrearModelo(DataTable dtColumns, ref FPrincipal formulario, string pNameTabla, string pNameClaseEnt, string strLlavePrimaria, ArrayList llavePrimaria)
        {
            TipoEntorno miEntorno;
            Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            string nameCol = "";

            string paramClaseConeccion = (string.IsNullOrEmpty(formulario.txtClaseConn.Text) ? "cConn" : formulario.txtClaseConn.Text);

            string funcionCargarDataReaderAnd = null;
            string funcionCargarDataReaderOr = null;
            string funcionCargarDataTableAnd = null;
            string funcionCargarDataTableAndFromView = null;
            string funcionCargarDataTableOr = null;

            string strAlias = null;
            try
            {
                DataTable dtPrefijo = new DataTable();

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.SQLServer2000:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.Oracle:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT ALIASSTA FROM SEGTABLAS WHERE UPPER(TABLASTA) = '" + pNameTabla.ToUpper() + "'");
                        break;

                    case TipoBD.PostgreSQL:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.aliassta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" + pNameTabla + "')");
                        if (dtPrefijo.Rows.Count == 0)
                        {
                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.alias FROM seg_tablas s WHERE UPPER(s.nombretabla) = UPPER('" + pNameTabla + "')");
                        }
                        break;
                }

                if (dtPrefijo.Rows.Count > 0)
                {
                    strAlias = dtPrefijo.Rows[0][0].ToString().ToLower();
                    strAlias = strAlias.Substring(0, 1).ToUpper() + strAlias.Substring(1, strAlias.Length - 1);
                }
                else
                {
                    strAlias = pNameTabla;
                }
            }
            catch (Exception ex)
            {
                strAlias = pNameTabla;
            }

            string strAliasSel = "";
            if (formulario.chkUsarSpsSelect.Checked)
            {
                if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                    miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                    miEntorno == TipoEntorno.CSharp_WebForms ||
                    miEntorno == TipoEntorno.CSharp_WebAPI ||
                    miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
                {
                    funcionCargarDataReaderAnd = "ExecStoreProcedureDataReaderSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataReaderOr = "ExecStoreProcedureDataReaderSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAnd = "ExecStoreProcedureSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAndFromView = "ExecStoreProcedureSel(strVista";
                    funcionCargarDataTableOr = "ExecStoreProcedureSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    strAliasSel = strAlias;
                }
                else
                {
                    funcionCargarDataReaderAnd = "execStoreProcedureDataReaderSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataReaderOr = "execStoreProcedureDataReaderSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAnd = "execStoreProcedureSel(" + pNameClaseEnt + ".StrAliasTabla";
                    funcionCargarDataTableAndFromView = "execStoreProcedureSel(strVista";
                    funcionCargarDataTableOr = "execStoreProcedureSelOr(" + pNameClaseEnt + ".StrAliasTabla";
                    strAliasSel = strAlias;
                }
            }
            else
            {
                if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                    miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                    miEntorno == TipoEntorno.CSharp_WebForms ||
                    miEntorno == TipoEntorno.CSharp_WebAPI ||
                    miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
                {
                    funcionCargarDataReaderAnd = "CargarDataReaderAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataReaderOr = "CargarDataReaderOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAnd = "CargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAndFromView = "CargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + "strVista";
                    funcionCargarDataTableOr = "CargarDataTableOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "CParametros.Schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                }
                else
                {
                    funcionCargarDataReaderAnd = "cargarDataReaderAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataReaderOr = "cargarDataReaderOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAnd = "cargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                    funcionCargarDataTableAndFromView = "cargarDataTableAnd(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + "strVista";
                    funcionCargarDataTableOr = "cargarDataTableOr(" + (formulario.chkUsarSchemaInModelo.Checked ? "cParametros.schema + " : "") + pNameClaseEnt + ".StrNombreTabla";
                }
                strAliasSel = pNameTabla;
            }

            //NUEVO DATATABLE
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un DataTable con los registros de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable NuevoDataTable()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tDataTable table = new DataTable ();");
            newTexto.AppendLine("\t\t\t\tDataColumn dc;");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t\tdc = new DataColumn(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString(),typeof(" + pNameClaseEnt + ").GetProperty(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString()).PropertyType);");
                newTexto.AppendLine("\t\t\t\ttable.Columns.Add(dc);\r\n");
            }
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\treturn table;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //NUEVO DATATABLE CON ARRCOLS
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que genera un DataTable con determinadas columnas de una " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable NuevoDataTable(ArrayList arrColumnas)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'2'\");");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\tDataTable table = local." + funcionCargarDataTableAnd + ", arrColumnas, arrColumnasWhere, arrValoresWhere);");

            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\treturn table;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //DATATABLE
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un DataTable con los registros de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\treturn ObtenerDataTable(arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //DATATABLE 1 CON CONDICIONES StringWhere
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"strParamAdicionales\" type=\"String\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Condiciones que van en la clausula WHERE. Deben ir sin WHERE");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(String strParamAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\tarrColumnasWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\tarrValoresWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\treturn ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //DATATABLE 2 CON arrColumnas
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(ArrayList arrColumnas)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\treturn ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //DATATABLE 2.5 CON arrColumnas y condicionesWhere
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Parametros adicionales");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'1'\");");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\treturn ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //DATATABLE 3 CON arrCOlumnas, arrColumnasWhere y arrValoresWhere
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\treturn ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, \"\");");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //DATATABLE 3 CON arrCOlumnas, arrColumnasWhere y arrValoresWhere
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"htbFiltro\" type=\"System.Collections.Hashtable\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Hashtable que contienen los pares para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\treturn ObtenerDataTable(arrColumnas, htbFiltro, \"\");");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");

            newTexto.AppendLine("\t\t");

            //DATATABLE 3.5 CON arrColumnasWhere y arrValoresWhere
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\treturn ObtenerDataTable(arrColumnasWhere, arrValoresWhere, \"\");");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //DATATABLE 3.5 CON arrColumnasWhere y arrValoresWhere
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"htbFiltro\" type=\"System.Collections.Hashtable\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Hashtable que contienen los pares para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(Hashtable htbFiltro)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\treturn ObtenerDataTable(htbFiltro, \"\");");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //DATATABLE 4 con arrColumnas, arrColumnasWhere, arrValoresWhere y condicionesWhere
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Parametros adicionales");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\tDataTable table = local." + funcionCargarDataTableAnd + ", arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\treturn table;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"htbFiltro\" type=\"System.Collections.Hashtable\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Hashtable que contienen los pares para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Parametros adicionales");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tforeach (DictionaryEntry entry in htbFiltro)");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\tarrColumnasWhere.Add(entry.Key.ToString());");
            newTexto.AppendLine("\t\t\t\t\tarrValoresWhere.Add(entry.Value.ToString());");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\tDataTable table = local." + funcionCargarDataTableAnd + ", arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\treturn table;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //DATATABLE 4.5 con arrColumnasWhere, arrValoresWhere y condicionesWhere
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Parametros adicionales");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
            }
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\treturn ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //DATATABLE 4.5 con arrColumnasWhere, arrValoresWhere y condicionesWhere
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"htbFiltro\" type=\"System.Collections.Hashtable\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Hashtable que contienen los pares para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Parametros adicionales");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tforeach (DictionaryEntry entry in htbFiltro)");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\tarrColumnasWhere.Add(entry.Key.ToString());");
            newTexto.AppendLine("\t\t\t\t\tarrValoresWhere.Add(entry.Value.ToString());");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
            }
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\treturn ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //DATATABLE OR
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
            }
            newTexto.AppendLine("\t\t\t\treturn ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, \"\");");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");

            newTexto.AppendLine("\t\t");

            //DATATABLE OR 1
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\treturn ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, \"\");");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");

            newTexto.AppendLine("\t\t");

            //DATATABLE OR 2
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que llena un DataTable con los registro de una tabla " + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las columnas WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"System.Collections.ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de las valores WHERE para filtrar el resultado");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"strParametrosAdicionales\" type=\"string\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Parametros adicionales");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t DataTable con los datos obtenidos de " + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\tDataTable table = local." + funcionCargarDataTableOr + ", arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\treturn table;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            if (miEntorno != TipoEntorno.CSharp_WCF)
            {
                //DATATABLE 5 con Objeto y valor
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos a partir de un filtro realizado por algun campo");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tDataTable que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(" + pNameClaseEnt + ".Fields searchField, object searchValue)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(searchValue);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\treturn ObtenerDataTable(arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //DATATABLE 5 con Objeto y valor y String strParamAdicionales
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos a partir de un filtro realizado por algun campo");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"strParametrosAdicionales\" type=\"string\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Parametros adicionales");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tDataTable que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(" + pNameClaseEnt + ".Fields searchField, object searchValue, string strParametrosAdicionales)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(searchValue);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\treturn ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //DATATABLE 5.1 con ArrColumnas, Objeto y valor y String strParamAdicionales
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos a partir de un filtro realizado por algun campo");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"strParametrosAdicionales\" type=\"string\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Parametros adicionales");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tDataTable que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(ArrayList arrColumnas, " + pNameClaseEnt + ".Fields searchField, object searchValue, string strParametrosAdicionales)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(searchValue);");
                newTexto.AppendLine("\t\t\t\t");

                newTexto.AppendLine("\t\t\t\treturn ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");

                //DATATABLE 5.2 con ArrColumnas, Objeto y valor
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que obtiene los datos a partir de un filtro realizado por algun campo");
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"arrColumnas\" type=\"System.Collections.ArrayList\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Array de las columnas que se va a seleccionar para mostrarlas en el resultado");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchField\" type=\"" + pNameClaseEnt + ".Fileds\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Campo por el que se va a filtrar la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"searchValue\" type=\"object\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Valor para la busqueda");
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \tDataTable que cumple con los filtros de los parametros");
                newTexto.AppendLine("\t\t/// </returns>");

                newTexto.AppendLine("\t\tpublic DataTable ObtenerDataTable(ArrayList arrColumnas, " + pNameClaseEnt + ".Fields searchField, object searchValue)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(searchField.ToString());");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(searchValue);");
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\treturn ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}");
                newTexto.AppendLine("\t\t");
            }
            return newTexto.ToString();
        }
    }
}