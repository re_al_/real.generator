﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using System;
using System.Data;
using System.IO;

namespace ReAl.Generator.App.AppGenerators
{
    public class cUnitTest
    {
        public string CrearUnitTestCS(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string paramClaseConeccion = (string.IsNullOrEmpty(formulario.txtClaseConn.Text) ? "cConn" : formulario.txtClaseConn.Text);
            string paramClaseTransaccion = (string.IsNullOrEmpty(formulario.txtClaseTransaccion.Text) ? "cTransaccion" : formulario.txtClaseTransaccion.Text);
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string formatoFechaHora = paramClaseParametros + ".parFormatoFechaHora";
            string formatoFecha = paramClaseParametros + ".parFormatoFecha";

            string nameCol = null;
            string tipoCol = null;
            bool permiteNull = false;
            string pNameTabla = pNameClase.Replace("_", "");
            pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() +
                         pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();

            pNameClase = formulario.txtPrefijoUnitTest.Text + pNameTabla;

            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameTabla;
            string pNameClaseRn = formulario.txtPrefijoModelo.Text + pNameTabla;

            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoUnitTest.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoUnitTest.Text;
            }

            //Los ProcAlm ya deberia star creados, si no esto no funciona bien
            DataTable dtParametros = new DataTable();
            int intAux = 0;

            string TipoColNull = null;
            string strAlias = null;
            string DescripcionCol = null;

            if (dtColumns.Rows.Count == 0)
                return "";
            if (dtColumns.Columns.Count == 0)
                return "";

            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceUnitTest.Text.Replace(".", "\\")) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceUnitTest.Text.Replace(".", "\\"));
            }

            string strPermiteNull = null;
            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase);
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que implementa las pruebas unitarias sobre la Tabla " + pNameTabla);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();

            newTexto.AppendLine(CabeceraTmp);

            newTexto.AppendLine("");
            newTexto.AppendLine("#region");
            newTexto.AppendLine("using System.Collections.Generic;");
            newTexto.AppendLine("using System.Data;");
            newTexto.AppendLine("using Microsoft.VisualStudio.TestTools.UnitTesting;");
            newTexto.AppendLine("//Reference Microsoft.VisualStudio.QualityTools.UnitTestFramework");
            newTexto.AppendLine("using " + formulario.txtInfProyectoDal.Text + "; ");
            newTexto.AppendLine("using " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceEntidades.Text + ";");
            newTexto.AppendLine("using " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceNegocios.Text + ";");
            newTexto.AppendLine("#endregion");
            newTexto.AppendLine("");

            newTexto.AppendLine("namespace " + formulario.txtInfProyectoUnitTest.Text + "." + formulario.txtNamespaceUnitTest.Text);
            newTexto.AppendLine("{");
            newTexto.AppendLine("\t[TestClass()]");
            newTexto.AppendLine("\tpublic class " + pNameClase);
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tprivate TestContext testContextInstance;");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t///Gets or sets the test context which provides");
            newTexto.AppendLine("\t\t///information about and functionality for the current test run.");
            newTexto.AppendLine("\t\t///</summary>");
            newTexto.AppendLine("\t\tpublic TestContext TestContext");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tget");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\treturn testContextInstance;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tset");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\ttestContextInstance = value;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\t#region Additional test attributes");
            newTexto.AppendLine("\t\t// ");
            newTexto.AppendLine("\t\t//You can use the following additional attributes as you write your tests:");
            newTexto.AppendLine("\t\t//");
            newTexto.AppendLine("\t\t//Use ClassInitialize to run code before running the first test in the class");
            newTexto.AppendLine("\t\t//[ClassInitialize()]");
            newTexto.AppendLine("\t\t//public static void MyClassInitialize(TestContext testContext)");
            newTexto.AppendLine("\t\t//{");
            newTexto.AppendLine("\t\t//}");
            newTexto.AppendLine("\t\t//");
            newTexto.AppendLine("\t\t//Use ClassCleanup to run code after all tests in a class have run");
            newTexto.AppendLine("\t\t//[ClassCleanup()]");
            newTexto.AppendLine("\t\t//public static void MyClassCleanup()");
            newTexto.AppendLine("\t\t//{");
            newTexto.AppendLine("\t\t//}");
            newTexto.AppendLine("\t\t//");
            newTexto.AppendLine("\t\t//Use TestInitialize to run code before running each test");
            newTexto.AppendLine("\t\t[TestInitialize()]");
            newTexto.AppendLine("\t\tpublic void MyTestInitialize()");
            newTexto.AppendLine("\t\t{");

            if (Principal.DBMS == TipoBD.Oracle)
            {
                newTexto.AppendLine("\t\t\tcParametros.server = cParametrosTest.server;");
                newTexto.AppendLine("\t\t\tcParametros.user = cParametrosTest.userDefault;");
                newTexto.AppendLine("\t\t\tcParametros.pass = cParametrosTest.passDefault;");
                newTexto.AppendLine("\t\t\tcParametros.bd = cParametrosTest.bd;");
                newTexto.AppendLine("\t\t\tcParametros.schema = cParametrosTest.schema;");
            }
            else
            {
                newTexto.AppendLine("\t\t\tcParametros.bChangeUserOnLogon = cParametrosTest.bChangeUserOnLogon;");
                newTexto.AppendLine("\t\t\tcParametros.bUseIntegratedSecurity = cParametrosTest.bUseIntegratedSecurity;");
                newTexto.AppendLine("\t\t\tcParametros.server = cParametrosTest.server;");
                newTexto.AppendLine("\t\t\tcParametros.puerto = cParametrosTest.puerto;");
                newTexto.AppendLine("\t\t\tcParametros.userDefault = cParametrosTest.userDefault;");
                newTexto.AppendLine("\t\t\tcParametros.passDefault = cParametrosTest.passDefault;");
                newTexto.AppendLine("\t\t\tcParametros.bd = cParametrosTest.bd;");
                newTexto.AppendLine("\t\t\tcParametros.schema = cParametrosTest.schema;");
            }

            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t//");
            newTexto.AppendLine("\t\t//Use TestCleanup to run code after each test has run");
            newTexto.AppendLine("\t\t//[TestCleanup()]");
            newTexto.AppendLine("\t\t//public void MyTestCleanup()");
            newTexto.AppendLine("\t\t//{");
            newTexto.AppendLine("\t\t//}");
            newTexto.AppendLine("\t\t//");
            newTexto.AppendLine("\t\t#endregion");
            newTexto.AppendLine("");

            //INSERT
            //Obtenemos el nombre del SP en base al prefijo
            string prefijo = pNameTabla;
            try
            {
                DataTable dtPrefijo = new DataTable();

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE tablaSegTablas = '" + pNameTabla + "'");
                        break;

                    case TipoBD.SQLServer2000:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE tablaSegTablas = '" + pNameTabla + "'");
                        break;

                    case TipoBD.Oracle:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT ALIASSTA FROM SEGTABLAS WHERE UPPER(TABLASTA) = '" + pNameTabla.ToUpper() + "'");
                        break;

                    case TipoBD.PostgreSQL:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.aliassta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" + pNameTabla + "')");
                        break;
                }

                if (dtPrefijo.Rows.Count > 0)
                {
                    prefijo = dtPrefijo.Rows[0][0].ToString();
                    prefijo = prefijo.Substring(0, 1).ToUpper() + prefijo.Substring(1, prefijo.Length - 1);
                }
                else
                {
                    prefijo = pNameTabla;
                }
            }
            catch (Exception ex)
            {
                prefijo = pNameTabla;
            }

            switch (formulario.cmbTipoColumnas.SelectedIndex)
            {
                case 0:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins", TipoColumnas.PrimeraMayuscula);
                    break;

                case 1:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins", TipoColumnas.Minusculas);
                    break;

                case 2:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins", TipoColumnas.Mayusculas);
                    break;
            }
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t///Prueba para el metodo Insert(" + pNameClaseRn + ")");
            newTexto.AppendLine("\t\t///</summary>");
            newTexto.AppendLine("\t\t[TestMethod()]");
            newTexto.AppendLine("\t\tpublic void InsertTest()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t" + pNameClaseRn + " target = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\t" + pNameClaseEnt + " obj = new " + pNameClaseEnt + "();");
            newTexto.AppendLine("\t\t\tbool expected = true;");
            newTexto.AppendLine("\t\t\tbool actual;");
            newTexto.AppendLine("\t\t\t");

            for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
            {
                dynamic bParametroIngresado = false;
                dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                for (int iColumna = intAux; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                {
                    nameCol = dtColumns.Rows[iColumna][0].ToString();
                    tipoCol = dtColumns.Rows[iColumna][1].ToString();
                    permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"] == "Yes" ? true : false);

                    if (dtColumns.Rows[iColumna]["EsIdentidad"].ToString() == "No")
                    {
                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            if (permiteNull)
                            {
                                newTexto.AppendLine("\t\t\t//obj." + nameCol + " = null;");
                            }
                            else
                            {
                                switch (tipoCol.ToUpper())
                                {
                                    case "INT":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "INT32":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "DECIMAL":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "BOOL":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = True;");
                                        break;

                                    case "BOOLEAN":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = True;");
                                        break;

                                    case "BYTE[]":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new Byte[1];");
                                        break;

                                    case "BYTE":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new Byte();");
                                        break;

                                    case "DATE":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = DateTime.Now;");
                                        break;

                                    case "DATETIME":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = DateTime.Now;");
                                        break;

                                    case "STRING":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = \"\";");
                                        break;

                                    default:
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new " + tipoCol + "();");
                                        break;
                                }
                            }
                            bParametroIngresado = true;
                        }
                    }
                }
            }

            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tactual = target.Insert(obj);");
            newTexto.AppendLine("\t\t\tAssert.AreEqual(expected, actual);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t///Prueba para el metodo Insert(" + pNameClaseRn + ", " + paramClaseTransaccion + ")");
            newTexto.AppendLine("\t\t///</summary>");
            newTexto.AppendLine("\t\t[TestMethod()]");
            newTexto.AppendLine("\t\tpublic void InsertTestTrans()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t" + pNameClaseRn + " target = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\t" + pNameClaseEnt + " obj = new " + pNameClaseEnt + "();");
            newTexto.AppendLine("\t\t\t" + paramClaseTransaccion + " localTrans = new " + paramClaseTransaccion + "();");
            newTexto.AppendLine("\t\t\tbool expected = true;");
            newTexto.AppendLine("\t\t\tbool actual;");
            newTexto.AppendLine("\t\t\t");

            for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
            {
                dynamic bParametroIngresado = false;
                dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                for (int iColumna = intAux; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                {
                    nameCol = dtColumns.Rows[iColumna][0].ToString();
                    tipoCol = dtColumns.Rows[iColumna][1].ToString();
                    permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"] == "Yes" ? true : false);

                    if (dtColumns.Rows[iColumna]["EsIdentidad"].ToString() == "No")
                    {
                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            if (permiteNull)
                            {
                                newTexto.AppendLine("\t\t\t//obj." + nameCol + " = null;");
                            }
                            else
                            {
                                switch (tipoCol.ToUpper())
                                {
                                    case "INT":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "INT32":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "DECIMAL":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "BOOL":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = True;");
                                        break;

                                    case "BOOLEAN":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = True;");
                                        break;

                                    case "BYTE[]":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new Byte[1];");
                                        break;

                                    case "BYTE":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new Byte();");
                                        break;

                                    case "DATE":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = DateTime.Now;");
                                        break;

                                    case "DATETIME":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = DateTime.Now;");
                                        break;

                                    case "STRING":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = \"\";");
                                        break;

                                    default:
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new " + tipoCol + "();");
                                        break;
                                }
                            }
                            bParametroIngresado = true;
                        }
                    }
                }
            }

            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tactual = target.Insert(obj, ref localTrans);");
            newTexto.AppendLine("\t\t\tAssert.AreEqual(expected, actual);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //UPDATE
            switch (formulario.cmbTipoColumnas.SelectedIndex)
            {
                case 0:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd", TipoColumnas.PrimeraMayuscula);
                    break;

                case 1:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd", TipoColumnas.Minusculas);
                    break;

                case 2:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd", TipoColumnas.Mayusculas);
                    break;
            }
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t///Prueba para el metodo Update(" + pNameClaseRn + ")");
            newTexto.AppendLine("\t\t///</summary>");
            newTexto.AppendLine("\t\t[TestMethod()]");
            newTexto.AppendLine("\t\tpublic void UpdateTest()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t" + pNameClaseRn + " target = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\t" + pNameClaseEnt + " obj = new " + pNameClaseEnt + "();");
            newTexto.AppendLine("\t\t\tint expected = 1;");
            newTexto.AppendLine("\t\t\tint actual;");
            newTexto.AppendLine("\t\t\t");

            for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
            {
                dynamic bParametroIngresado = false;
                dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                for (int iColumna = intAux; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                {
                    nameCol = dtColumns.Rows[iColumna][0].ToString();
                    tipoCol = dtColumns.Rows[iColumna][1].ToString();
                    permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"].ToString() == "Yes" ? true : false);

                    if (dtColumns.Rows[iColumna]["EsIdentidad"].ToString() == "No")
                    {
                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            if (permiteNull)
                            {
                                newTexto.AppendLine("\t\t\t//obj." + nameCol + " = null;");
                            }
                            else
                            {
                                switch (tipoCol.ToUpper())
                                {
                                    case "INT":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "INT32":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "DECIMAL":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "BOOL":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = True;");
                                        break;

                                    case "BOOLEAN":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = True;");
                                        break;

                                    case "BYTE[]":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new Byte[1];");
                                        break;

                                    case "BYTE":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new Byte();");
                                        break;

                                    case "DATE":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = DateTime.Now;");
                                        break;

                                    case "DATETIME":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = DateTime.Now;");
                                        break;

                                    case "STRING":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = \"\";");
                                        break;

                                    default:
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new " + tipoCol + "();");
                                        break;
                                }
                            }
                            bParametroIngresado = true;
                        }
                    }
                }
            }

            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tactual = target.Update(obj);");
            newTexto.AppendLine("\t\t\tAssert.AreNotEqual(expected, actual);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t///Prueba para el metodo Update(" + pNameClaseRn + ", " + paramClaseTransaccion + ")");
            newTexto.AppendLine("\t\t///</summary>");
            newTexto.AppendLine("\t\t[TestMethod()]");
            newTexto.AppendLine("\t\tpublic void UpdateTestTrans()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t" + pNameClaseRn + " target = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\t" + pNameClaseEnt + " obj = new " + pNameClaseEnt + "();");
            newTexto.AppendLine("\t\t\t" + paramClaseTransaccion + " localTrans = new " + paramClaseTransaccion + "();");
            newTexto.AppendLine("\t\t\tint expected = 0;");
            newTexto.AppendLine("\t\t\tint actual;");
            newTexto.AppendLine("\t\t\t");

            for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
            {
                dynamic bParametroIngresado = false;
                dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                for (int iColumna = intAux; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                {
                    nameCol = dtColumns.Rows[iColumna][0].ToString();
                    tipoCol = dtColumns.Rows[iColumna][1].ToString();
                    permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"].ToString() == "Yes" ? true : false);

                    if (dtColumns.Rows[iColumna]["EsIdentidad"].ToString() == "No")
                    {
                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            if (permiteNull)
                            {
                                newTexto.AppendLine("\t\t\t//obj." + nameCol + " = null;");
                            }
                            else
                            {
                                switch (tipoCol.ToUpper())
                                {
                                    case "INT":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "INT32":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "DECIMAL":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "BOOL":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = True;");
                                        break;

                                    case "BOOLEAN":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = True;");
                                        break;

                                    case "BYTE[]":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new Byte[1];");
                                        break;

                                    case "BYTE":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new Byte();");
                                        break;

                                    case "DATE":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = DateTime.Now;");
                                        break;

                                    case "DATETIME":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = DateTime.Now;");
                                        break;

                                    case "STRING":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = \"\";");
                                        break;

                                    default:
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new " + tipoCol + "();");
                                        break;
                                }
                            }
                            bParametroIngresado = true;
                        }
                    }
                }
            }

            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tactual = target.Update(obj, ref localTrans);");
            newTexto.AppendLine("\t\t\tAssert.AreNotEqual(expected, actual);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //DELETE
            switch (formulario.cmbTipoColumnas.SelectedIndex)
            {
                case 0:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del", TipoColumnas.PrimeraMayuscula);
                    break;

                case 1:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del", TipoColumnas.Minusculas);
                    break;

                case 2:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del", TipoColumnas.Mayusculas);
                    break;
            }
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t///Prueba para el metodo Delete(" + pNameClaseRn + ")");
            newTexto.AppendLine("\t\t///</summary>");
            newTexto.AppendLine("\t\t[TestMethod()]");
            newTexto.AppendLine("\t\tpublic void DeleteTest()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t" + pNameClaseRn + " target = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\t" + pNameClaseEnt + " obj = new " + pNameClaseEnt + "();");
            newTexto.AppendLine("\t\t\tint expected = 1;");
            newTexto.AppendLine("\t\t\tint actual;");
            newTexto.AppendLine("\t\t\t");

            for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
            {
                dynamic bParametroIngresado = false;
                dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                for (int iColumna = intAux; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                {
                    nameCol = dtColumns.Rows[iColumna][0].ToString();
                    tipoCol = dtColumns.Rows[iColumna][1].ToString();
                    permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"].ToString() == "Yes" ? true : false);

                    if (dtColumns.Rows[iColumna]["EsIdentidad"].ToString() == "No")
                    {
                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            if (permiteNull)
                            {
                                newTexto.AppendLine("\t\t\t//obj." + nameCol + " = null;");
                            }
                            else
                            {
                                switch (tipoCol.ToUpper())
                                {
                                    case "INT":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "INT32":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "DECIMAL":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "BOOL":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = True;");
                                        break;

                                    case "BOOLEAN":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = True;");
                                        break;

                                    case "BYTE[]":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new Byte[1];");
                                        break;

                                    case "BYTE":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new Byte();");
                                        break;

                                    case "DATE":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = DateTime.Now;");
                                        break;

                                    case "DATETIME":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = DateTime.Now;");
                                        break;

                                    case "STRING":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = \"\";");
                                        break;

                                    default:
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new " + tipoCol + "();");
                                        break;
                                }
                            }
                            bParametroIngresado = true;
                        }
                    }
                }
            }

            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tactual = target.Delete(obj);");
            newTexto.AppendLine("\t\t\tAssert.AreNotEqual(expected, actual);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t///Prueba para el metodo Delete(" + pNameClaseRn + ", " + paramClaseTransaccion + ")");
            newTexto.AppendLine("\t\t///</summary>");
            newTexto.AppendLine("\t\t[TestMethod()]");
            newTexto.AppendLine("\t\tpublic void DeleteTestTrans()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t" + pNameClaseRn + " target = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\t" + pNameClaseEnt + " obj = new " + pNameClaseEnt + "();");
            newTexto.AppendLine("\t\t\t" + paramClaseTransaccion + " localTrans = new " + paramClaseTransaccion + "();");
            newTexto.AppendLine("\t\t\tint expected = 0;");
            newTexto.AppendLine("\t\t\tint actual;");
            newTexto.AppendLine("\t\t\t");

            for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
            {
                dynamic bParametroIngresado = false;
                dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                for (int iColumna = intAux; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                {
                    nameCol = dtColumns.Rows[iColumna][0].ToString();
                    tipoCol = dtColumns.Rows[iColumna][1].ToString();
                    permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"].ToString() == "Yes" ? true : false);

                    if (dtColumns.Rows[iColumna]["EsIdentidad"].ToString() == "No")
                    {
                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            if (permiteNull)
                            {
                                newTexto.AppendLine("\t\t\t//obj." + nameCol + " = null;");
                            }
                            else
                            {
                                switch (tipoCol.ToUpper())
                                {
                                    case "INT":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "INT32":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "DECIMAL":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = 0;");
                                        break;

                                    case "BOOL":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = True;");
                                        break;

                                    case "BOOLEAN":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = True;");
                                        break;

                                    case "BYTE[]":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new Byte[1];");
                                        break;

                                    case "BYTE":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new Byte();");
                                        break;

                                    case "DATE":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = DateTime.Now;");
                                        break;

                                    case "DATETIME":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = DateTime.Now;");
                                        break;

                                    case "STRING":
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = \"\";");
                                        break;

                                    default:
                                        newTexto.AppendLine("\t\t\t//obj." + nameCol + " = new " + tipoCol + "();");
                                        break;
                                }
                            }
                            bParametroIngresado = true;
                        }
                    }
                }
            }

            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\tactual = target.Delete(obj, ref localTrans);");
            newTexto.AppendLine("\t\t\tAssert.AreNotEqual(expected, actual);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //OTROS
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t///Prueba para el método ObtenerLista()");
            newTexto.AppendLine("\t\t///</summary>");
            newTexto.AppendLine("\t\t[TestMethod()]");
            newTexto.AppendLine("\t\tpublic void ObtenerListaTest()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t" + pNameClaseRn + " target = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\tList<" + pNameClaseEnt + "> notExpected = null;");
            newTexto.AppendLine("\t\t\tList<" + pNameClaseEnt + "> actual = target.ObtenerLista();");
            newTexto.AppendLine("\t\t\tAssert.AreNotEqual(notExpected, actual);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t///Prueba para el método CargarDataTable()");
            newTexto.AppendLine("\t\t///</summary>");
            newTexto.AppendLine("\t\t[TestMethod()]");
            newTexto.AppendLine("\t\tpublic void CargarDataTableTest()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t" + pNameClaseRn + " target = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\tint notExpected = 0; ");
            newTexto.AppendLine("\t\t\tint actualColumns = target.ObtenerDataTable().Columns.Count;");
            newTexto.AppendLine("\t\t\tAssert.AreNotEqual(notExpected, actualColumns);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            newTexto.AppendLine("\t}");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), pNameClase + ".cs", PathDesktop + "\\" + formulario.txtNamespaceUnitTest.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceUnitTest.Text.Replace(".", "\\") + "\\" + pNameClase + ".cs";
        }
    }
}