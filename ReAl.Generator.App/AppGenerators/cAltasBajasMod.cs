﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using ReAlFind_Control;
using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;

namespace ReAl.Generator.App.AppGenerators
{
    public class cAltasBajasMod
    {
        #region Fields

        private bool SetBoolean = false;
        private bool SetByte = false;
        private bool SetDateTime = true;
        private bool SetDecimal = false;
        private bool SetDefault = true;
        private bool SetInt = false;
        private bool SetInt32 = false;
        private bool SetString = true;

        #endregion Fields

        #region Methods

        public string CrearABMsCodigo(DataTable dtColumns, string pNameTabla, ref FPrincipal formulario, bool bUsaNombreQuery)
        {
            TipoEntorno miEntorno;
            Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);

            string paramClaseConeccion = (string.IsNullOrEmpty(formulario.txtClaseConn.Text) ? "cConn" : formulario.txtClaseConn.Text);
            string paramClaseTransaccion = (string.IsNullOrEmpty(formulario.txtClaseTransaccion.Text) ? "cTransaccion" : formulario.txtClaseTransaccion.Text);
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);

            string nameCol = null;
            string tipoCol = null;
            bool permiteNull = false;
            string pNameTablaCamel = pNameTabla.Replace("_", "");
            pNameTablaCamel = pNameTablaCamel.Substring(0, 1).ToUpper() + pNameTablaCamel.Substring(1, 2).ToLower() +
                              pNameTablaCamel.Substring(3, 1).ToUpper() + pNameTablaCamel.Substring(4).ToLower();

            string pNameClaseMod = formulario.txtPrefijoModelo.Text + pNameTablaCamel;
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameTablaCamel;
            string pNameClaseIn = formulario.txtPrefijoInterface.Text + pNameTablaCamel;

            string funcionCconnInsertar = "insertBD";
            string funcionCconnUpdate = "updateBD";
            string funcionCconnDelete = "deleteBD";
            string nombreFuncionInsertar = "Insert";
            string nombreFuncionUpdate = "Update";
            string nombreFuncionDelete = "Delete";

            if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                miEntorno == TipoEntorno.CSharp_WebForms ||
                miEntorno == TipoEntorno.CSharp_WebAPI ||
                miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
            {
                funcionCconnInsertar = "InsertBd";
                funcionCconnUpdate = "UpdateBd";
                funcionCconnDelete = "DeleteBd";
            }

            if (bUsaNombreQuery)
            {
                nombreFuncionInsertar = nombreFuncionInsertar + "Query";
                nombreFuncionUpdate = nombreFuncionUpdate + "Query";
                nombreFuncionDelete = nombreFuncionDelete + "Query";
            }

            ArrayList llavePrimaria = new ArrayList();
            string strLlavePrimaria = "";
            string strArrLlavePrimarias = "";
            string strArrLlavePrimariasVal = "";
            dynamic bEsPrimerElemento = true;

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    tipoCol = dtColumns.Rows[iCol][1].ToString();

                    strArrLlavePrimarias = strArrLlavePrimarias + "\t\t\t\tarrColumnasWhere.Add(" +
                                           pNameClaseEnt + ".Fields." + nameCol + ".ToString());\r\n";
                    strArrLlavePrimariasVal = strArrLlavePrimariasVal + "\t\t\t\t" +
                                              "arrValoresWhere.Add(" + tipoCol + nameCol + ");\r\n";
                    if (bEsPrimerElemento)
                    {
                        strLlavePrimaria = strLlavePrimaria + "obj." + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlavePrimaria = strLlavePrimaria + ", obj." + nameCol;
                    }
                    llavePrimaria.Add(tipoCol + nameCol);
                }
            }
            if (string.IsNullOrEmpty(strLlavePrimaria))
            {
                strLlavePrimaria = dtColumns.Rows[0][1].ToString() + " " + dtColumns.Rows[0][1].ToString() + dtColumns.Rows[0][0].ToString();
                llavePrimaria.Add(dtColumns.Rows[0][1].ToString() + dtColumns.Rows[0][0].ToString());
            }

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            //INSERT
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic bool " + nombreFuncionInsertar + "(" + pNameClaseEnt + " obj)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");

            newTexto.AppendLine(cDiccionarios.arrValInsert(pNameClaseEnt, pNameTabla, dtColumns, formulario, miEntorno));

            newTexto.AppendLine("\t\t\t");

            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\treturn local." + funcionCconnInsertar + "(" + pNameClaseEnt + ".StrNombreTabla, arrColumnas, arrValores);");

            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //INSERT 2
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic bool " + nombreFuncionInsertar + "(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");

            newTexto.AppendLine(cDiccionarios.arrValInsert(pNameClaseEnt, pNameTabla, dtColumns, formulario, miEntorno));

            newTexto.AppendLine("\t\t\t");

            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\treturn local." + funcionCconnInsertar + "(" + pNameClaseEnt + ".StrNombreTabla, arrColumnas, arrValores, ref localTrans);");

            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //INSERT 3
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic bool " + nombreFuncionInsertar + "Identity(" + pNameClaseEnt + " obj)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");

            newTexto.AppendLine(cDiccionarios.arrValInsert(pNameClaseEnt, pNameTabla, dtColumns, formulario, miEntorno));

            newTexto.AppendLine("\t\t\t");

            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\tint intIdentidad = -1;");
            newTexto.AppendLine("\t\t\t\tbool res = local." + funcionCconnInsertar + "(" + pNameClaseEnt + ".StrNombreTabla, arrColumnas, arrValores, ref intIdentidad);");
            newTexto.AppendLine("\t\t\t\tobj." + dtColumns.Rows[0][0].ToString() + " = intIdentidad;");
            newTexto.AppendLine("\t\t\t\treturn res;");

            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //INSERT 4
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic bool " + nombreFuncionInsertar + "Identity(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");

            newTexto.AppendLine(cDiccionarios.arrValInsert(pNameClaseEnt, pNameTabla, dtColumns, formulario, miEntorno));
            newTexto.AppendLine("\t\t\t");

            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\tint intIdentidad = -1;");
            newTexto.AppendLine("\t\t\t\tbool res = local." + funcionCconnInsertar + "(" + pNameClaseEnt + ".StrNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);");
            newTexto.AppendLine("\t\t\t\tobj." + dtColumns.Rows[0][0].ToString() + " = intIdentidad;");
            newTexto.AppendLine("\t\t\t\treturn res;");

            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //UPDATE
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que actualiza un registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a actualizar los valores a la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Cantidad de registros afectados por el exito de la operacion" + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic int " + nombreFuncionUpdate + "All(" + pNameClaseEnt + " obj)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");

            newTexto.AppendLine(cDiccionarios.arrValUpdate(pNameClaseEnt, pNameTabla, dtColumns, formulario, miEntorno));
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine(cDiccionarios.arrWhereUpdate(pNameClaseEnt, pNameTabla, dtColumns, formulario, miEntorno));

            newTexto.AppendLine("\t\t\t");

            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\treturn local." + funcionCconnUpdate + "(" + pNameClaseEnt + ".StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //UPDATE 2
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que actualiza un registro en la tabla " + pNameTabla + " a partir de una clase del tipo e" + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a actualizar los valores a la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Exito de la operacion");
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic int " + nombreFuncionUpdate + "All(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");

            newTexto.AppendLine(cDiccionarios.arrValUpdate(pNameClaseEnt, pNameTabla, dtColumns, formulario, miEntorno));
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine(cDiccionarios.arrWhereUpdate(pNameClaseEnt, pNameTabla, dtColumns, formulario, miEntorno));

            newTexto.AppendLine("\t\t\t");

            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\treturn local." + funcionCconnUpdate + "(" + pNameClaseEnt + ".StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //UPDATE SOLO CAMBIOS
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que actualiza un registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a actualizar los valores a la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Cantidad de registros afectados por el exito de la operacion" + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic int " + nombreFuncionUpdate + "(" + pNameClaseEnt + " obj)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\t//Obtenemos el Objeto original");
            newTexto.AppendLine("\t\t\t\t" + pNameClaseEnt + " objOriginal = this.ObtenerObjeto(" + strLlavePrimaria + ");");
            newTexto.AppendLine("\t\t\t\t");

            newTexto.AppendLine(cDiccionarios.arrValUpdateCambios(pNameClaseEnt, pNameTabla, dtColumns, formulario, miEntorno));
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine(cDiccionarios.arrWhereUpdate(pNameClaseEnt, pNameTabla, dtColumns, formulario, miEntorno));

            newTexto.AppendLine("\t\t\t");

            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\treturn local." + funcionCconnUpdate + "(" + pNameClaseEnt + ".StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //UPDATE SOLO CAMBIOS 2
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que actualiza un registro en la tabla " + pNameTabla + " a partir de una clase del tipo e" + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a actualizar los valores a la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Exito de la operacion");
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic int " + nombreFuncionUpdate + "(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\t//Obtenemos el Objeto original");
            newTexto.AppendLine("\t\t\t\t" + pNameClaseEnt + " objOriginal = this.ObtenerObjeto(" + strLlavePrimaria + ", ref localTrans);");
            newTexto.AppendLine("\t\t\t\t");

            newTexto.AppendLine(cDiccionarios.arrValUpdateCambios(pNameClaseEnt, pNameTabla, dtColumns, formulario, miEntorno));
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine(cDiccionarios.arrWhereUpdate(pNameClaseEnt, pNameTabla, dtColumns, formulario, miEntorno));

            newTexto.AppendLine("\t\t\t");

            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\treturn local." + funcionCconnUpdate + "(" + pNameClaseEnt + ".StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //DELETE
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que elimina un registro en la tabla " + pNameTabla + " a partir de una clase del tipo " + pNameClaseEnt + " y su respectiva PK");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Cantidad de registros afectados por el exito de la operacion" + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic int " + nombreFuncionDelete + "(" + pNameClaseEnt + " obj)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");

            newTexto.AppendLine(cDiccionarios.arrWhereUpdate(pNameClaseEnt, pNameTabla, dtColumns, formulario, miEntorno));

            newTexto.AppendLine("\t\t\t");

            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\treturn local." + funcionCconnDelete + "(" + pNameClaseEnt + ".StrNombreTabla, arrColumnasWhere, arrValoresWhere);");

            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //DELETE 2
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que elimina un registro en la tabla " + pNameTabla + " a partir de una clase del tipo " + pNameClaseEnt + " y su PK");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + ".e" + pNameTabla + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Exito de la operacion" + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic int " + nombreFuncionDelete + "(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");

            newTexto.AppendLine(cDiccionarios.arrWhereUpdate(pNameClaseEnt, pNameTabla, dtColumns, formulario, miEntorno));

            newTexto.AppendLine("\t\t\t");

            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\treturn local." + funcionCconnDelete + "(" + pNameClaseEnt + ".StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);");

            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //DELETE 3
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que elimina un registro en la tabla " + pNameTabla + " a partir de una clase del tipo e" + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de Columnas en la clausa WHERE ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de Valores para cada una de las columnas en la clausa WHERE ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Cantidad de registros afectados por el exito de la operacion" + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic int " + nombreFuncionDelete + "(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");

            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\treturn local." + funcionCconnDelete + "(\"" + pNameTabla + "\", arrColumnasWhere, arrValoresWhere);");

            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            //DELETE 4
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// \t Funcion que elimina un registro en la tabla " + pNameTabla + " a partir de una clase del tipo e" + pNameTabla);
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"arrColumnasWhere\" type=\"ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de Columnas en la clausa WHERE ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"arrValoresWhere\" type=\"ArrayList\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Array de Valores para cada una de las columnas en la clausa WHERE ");
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
            newTexto.AppendLine("\t\t///     <para>");
            newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
            newTexto.AppendLine("\t\t///     </para>");
            newTexto.AppendLine("\t\t/// </param>");
            newTexto.AppendLine("\t\t/// <returns>");
            newTexto.AppendLine("\t\t/// \t Exito de la operacion" + pNameTabla);
            newTexto.AppendLine("\t\t/// </returns>");

            newTexto.AppendLine("\t\tpublic int " + nombreFuncionDelete + "(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref " + paramClaseTransaccion + " localTrans)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");

            newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
            newTexto.AppendLine("\t\t\t\treturn local." + funcionCconnDelete + "(" + pNameClaseEnt + ".StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);");

            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception exp)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tthrow exp;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");

            return newTexto.ToString();
        }

        public string CrearABMsCodigoVB(DataTable dtColumns, string pNameTabla, ref FPrincipal formulario)
        {
            throw new Exception("No se ha creado el metodo para Vb.Net");
        }

        public string CrearABMsProcAlm(DataTable dtColumns, string pNameTabla, ref FPrincipal formulario)
        {
            TipoEntorno miEntorno;
            Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);

            string paramClaseListadoSp = (string.IsNullOrEmpty(formulario.txtClaseParametrosListadoSp.Text) ? "cListadoSp" : formulario.txtClaseParametrosListadoSp.Text);
            string paramClaseConeccion = (string.IsNullOrEmpty(formulario.txtClaseConn.Text) ? "cConn" : formulario.txtClaseConn.Text);
            string paramClaseTransaccion = (string.IsNullOrEmpty(formulario.txtClaseTransaccion.Text) ? "cTransaccion" : formulario.txtClaseTransaccion.Text);
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string formatoFechaHora = paramClaseParametros + ".parFormatoFechaHora";
            string formatoFecha = paramClaseParametros + ".parFormatoFecha";

            string nameCol = null;
            string tipoCol = null;
            bool permiteNull = false;
            string pNameClase = pNameTabla.Replace("_", "").Substring(0, 1).ToUpper() + pNameTabla.Replace("_", "").Substring(1, 2).ToLower() + pNameTabla.Replace("_", "").Substring(3, 1).ToUpper() + pNameTabla.Replace("_", "").Substring(4).ToLower();

            string pNameClaseMod = formulario.txtPrefijoModelo.Text + pNameClase;
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameClase;
            string pNameClaseIn = formulario.txtPrefijoInterface.Text + pNameClase;

            //Los ProcAlm ya deberia star creados, si no esto no funciona bien
            DataTable dtParametros = new DataTable();
            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            int intAux = 0;

            //Obtenemos el nombre del SP en base al prefijo
            string prefijo = pNameTabla;
            try
            {
                DataTable dtPrefijo = new DataTable();

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.SQLServer2000:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.Oracle:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT UPPER(SUBSTR(ALIASSTA,1,1)) || LOWER(SUBSTR(ALIASSTA,2,LENGTH(ALIASSTA)-1)) FROM SEGTABLAS WHERE UPPER(TABLASTA) = UPPER('" + pNameTabla.ToUpper() + "')");
                        break;

                    case TipoBD.PostgreSQL:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.aliassta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" + pNameTabla + "')");
                        if (dtPrefijo.Rows.Count == 0)
                        {
                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.alias FROM seg_tablas s WHERE UPPER(s.nombretabla) = UPPER('" + pNameTabla + "')");
                        }
                        break;
                }

                if (dtPrefijo.Rows.Count > 0)
                {
                    prefijo = dtPrefijo.Rows[0][0].ToString().ToString();
                    prefijo = prefijo.Substring(0, 1).ToUpper() + prefijo.Substring(1, prefijo.Length - 1);
                }
                else
                {
                    try
                    {
                        switch (Principal.DBMS)
                        {
                            case TipoBD.SQLServer:
                                dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE UPPER(TablaSegTablas) = UPPER('" + pNameTabla.ToUpper() + "')");
                                break;

                            case TipoBD.SQLServer2000:
                                dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE UPPER(TablaSegTablas) = UPPER('" + pNameTabla.ToUpper() + "')");
                                break;

                            case TipoBD.Oracle:
                                dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT UPPER(SUBSTR(AliasSegTablas,1,1)) || LOWER(SUBSTR(AliasSegTablas,2,LENGTH(AliasSegTablas)-1)) FROM SEGTABLAS WHERE UPPER(TablaSegTablas) = '" + pNameTabla.ToUpper() + "'");
                                break;

                            case TipoBD.PostgreSQL:
                                dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.AliasSegTablas FROM segtablas s WHERE UPPER(s.TablaSegTablas) = UPPER('" + pNameTabla.ToUpper() + "')");
                                if (dtPrefijo.Rows.Count == 0)
                                {
                                    dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.AliasSegTablas FROM seg_tablas s WHERE UPPER(s.TablaSegTablas) = UPPER('" + pNameTabla + "')");
                                }
                                break;
                        }

                        if (dtPrefijo.Rows.Count > 0)
                        {
                            prefijo = dtPrefijo.Rows[0][0].ToString().ToString();
                            prefijo = prefijo.Substring(0, 1).ToUpper() + prefijo.Substring(1, prefijo.Length - 1);
                        }
                        else
                        {
                            prefijo = pNameTabla;
                        }
                    }
                    catch (Exception ex2)
                    {
                        prefijo = pNameTabla;
                    }
                }
            }
            catch (Exception ex)
            {
                prefijo = pNameTabla;
            }

            //Para cada operacion obtenemos los parametros del Proc Almacenado
            switch (formulario.cmbTipoColumnas.SelectedIndex)
            {
                case 0:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins", TipoColumnas.PrimeraMayuscula);
                    break;

                case 1:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins", TipoColumnas.Minusculas);
                    break;

                case 2:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins", TipoColumnas.Mayusculas);
                    break;
            }

            if (dtParametros.Rows.Count > 0)
            {
                //INSERT
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t/// </returns>");
                if (formulario.chkUsarSpsIdentity.Checked)
                {
                    newTexto.AppendLine("\t\tpublic bool Insert(ref " + pNameClaseEnt + " obj, bool bValidar = true)");
                }
                else
                {
                    newTexto.AppendLine("\t\tpublic bool Insert(" + pNameClaseEnt + " obj, bool bValidar = true)");
                }

                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                if (formulario.chkDataAnnotations.Checked)
                {
                    newTexto.AppendLine("\t\t\t\tif (bValidar)");
                    newTexto.AppendLine("\t\t\t\t\tif (!obj.IsValid())");
                    newTexto.AppendLine("\t\t\t\t\t\tthrow new System.ComponentModel.DataAnnotations.ValidationException(obj.ValidationErrorsString());");
                }
                newTexto.AppendLine("\t\t\t\tArrayList arrNombreParam = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresParam = new ArrayList();");

                for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
                {
                    dynamic bParametroIngresado = false;
                    dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                    //Para InsertIdentity
                    if (formulario.chkUsarSpsIdentity.Checked)
                    {
                        nameCol = dtColumns.Rows[0][0].ToString();
                        tipoCol = dtColumns.Rows[0][1].ToString();
                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            //If DBMS = TipoBD.Oracle.ToString() Then
                            //    If formulario.chkParametroPa.Checked Then
                            //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                            //    Else
                            //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & nameParametro & Chr(34) & ");")
                            //    End If
                            //Else
                            //    If formulario.chkParametroPa.Checked Then
                            //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                            //    Else
                            //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & pNameClaseEnt & ".Fields." & nameParametro & ".ToString());")
                            //    End If
                            //End If
                            if (formulario.chkParametroPa.Checked)
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameParametro + "\");");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(" + pNameClaseEnt + ".Fields." + nameParametro + ".ToString());");
                            }

                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ");");
                            newTexto.AppendLine("\t\t\t\t");
                            bParametroIngresado = true;
                        }
                    }

                    if (formulario.chkUsarSpsIdentity.Checked)
                    {
                        intAux = 1;
                    }
                    else
                    {
                        intAux = 0;
                    }

                    for (int iColumna = intAux; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                    {
                        nameCol = dtColumns.Rows[iColumna][0].ToString();
                        tipoCol = dtColumns.Rows[iColumna][1].ToString();
                        permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"] == "Yes" ? true : false);

                        if (dtColumns.Rows[iColumna]["EsIdentidad"].ToString() == "No")
                        {
                            if (nameCol.ToUpper() == nameParametro.ToUpper())
                            {
                                if (!permiteNull & formulario.chkUsarSpsCheck.Checked)
                                {
                                    newTexto.AppendLine("\t\t\t\tif (obj." + nameCol + " == null)");
                                    newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"El Parametro " + nameCol + " no puede ser NULO.\");");
                                }

                                //If DBMS = TipoBD.Oracle.ToString() Then
                                //    If formulario.chkParametroPa.Checked Then
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                                //    Else
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & nameParametro & Chr(34) & ");")
                                //    End If
                                //Else
                                //    If formulario.chkParametroPa.Checked Then
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                                //    Else
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & pNameClaseEnt & ".Fields." & nameParametro & ".ToString());")
                                //    End If
                                //End If
                                if (formulario.chkParametroPa.Checked)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameParametro + "\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(" + pNameClaseEnt + ".Fields." + nameParametro + ".ToString());");
                                }
                                newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ");");
                                newTexto.AppendLine("\t\t\t\t");
                                bParametroIngresado = true;
                            }
                        }
                    }

                    if (!bParametroIngresado)
                    {
                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\");");
                        newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(null);");
                    }
                }

                if (formulario.chkUsarSpsIdentity.Checked)
                {
                    nameCol = dtColumns.Rows[0][0].ToString();
                    newTexto.AppendLine("\t\t\t\t");
                    newTexto.AppendLine("\t\t\t\t//Llamamos al Procedmiento Almacenado");
                    newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
                    newTexto.AppendLine("\t\t\t\tstring nombreSp = " + paramClaseListadoSp + "." + pNameClase + "." + formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins.ToString();");
                    newTexto.AppendLine("\t\t\t\tobj." + nameCol + " = (local.ExecStoreProcedureIdentity(nombreSp, arrNombreParam, arrValoresParam)).ToString();");
                    newTexto.AppendLine("\t\t\t\treturn (!string.IsNullOrEmpty(obj." + nameCol + ".ToString()));");
                }
                else
                {
                    newTexto.AppendLine("\t\t\t\t");
                    newTexto.AppendLine("\t\t\t\t//Llamamos al Procedmiento Almacenado");
                    newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
                    newTexto.AppendLine("\t\t\t\tstring nombreSp = " + paramClaseListadoSp + "." + pNameClase + "." + formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins.ToString();");
                    newTexto.AppendLine("\t\t\t\treturn (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam) > 0);");
                }

                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}\r\n");

                if (miEntorno != TipoEntorno.CSharp_WCF)
                {
                    //INSERT TRANS
                    newTexto.AppendLine("\t\t/// <summary>");
                    newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                    newTexto.AppendLine("\t\t/// </summary>");
                    newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                    newTexto.AppendLine("\t\t///     <para>");
                    newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                    newTexto.AppendLine("\t\t///     </para>");
                    newTexto.AppendLine("\t\t/// </param>");
                    newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                    newTexto.AppendLine("\t\t///     <para>");
                    newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                    newTexto.AppendLine("\t\t///     </para>");
                    newTexto.AppendLine("\t\t/// </param>");
                    newTexto.AppendLine("\t\t/// <returns>");
                    newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                    newTexto.AppendLine("\t\t/// </returns>");
                    if (formulario.chkUsarSpsIdentity.Checked)
                    {
                        newTexto.AppendLine("\t\tpublic bool Insert(ref " + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans, bool bValidar = true)");
                    }
                    else
                    {
                        newTexto.AppendLine("\t\tpublic bool Insert(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans, bool bValidar = true)");
                    }

                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\ttry");
                    newTexto.AppendLine("\t\t\t{");
                    if (formulario.chkDataAnnotations.Checked)
                    {
                        newTexto.AppendLine("\t\t\t\tif (bValidar)");
                        newTexto.AppendLine("\t\t\t\t\tif (!obj.IsValid())");
                        newTexto.AppendLine("\t\t\t\t\t\tthrow new System.ComponentModel.DataAnnotations.ValidationException(obj.ValidationErrorsString());");
                    }
                    newTexto.AppendLine("\t\t\t\tArrayList arrNombreParam = new ArrayList();");
                    newTexto.AppendLine("\t\t\t\tArrayList arrValoresParam = new ArrayList();");

                    for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
                    {
                        dynamic bParametroIngresado = false;
                        dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                        //Para InsertIdentity
                        if (formulario.chkUsarSpsIdentity.Checked)
                        {
                            nameCol = dtColumns.Rows[0][0].ToString();
                            tipoCol = dtColumns.Rows[0][1].ToString();

                            if (nameCol.ToUpper() == nameParametro.ToUpper())
                            {
                                //If DBMS = TipoBD.Oracle.ToString() Then
                                //    If formulario.chkParametroPa.Checked Then
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                                //    Else
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & nameParametro & Chr(34) & ");")
                                //    End If
                                //Else
                                //    If formulario.chkParametroPa.Checked Then
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                                //    Else
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & pNameClaseEnt & ".Fields." & nameParametro & ".ToString());")
                                //    End If
                                //End If
                                if (formulario.chkParametroPa.Checked)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameParametro + "\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(" + pNameClaseEnt + ".Fields." + nameParametro + ".ToString());");
                                }

                                newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ");");
                                newTexto.AppendLine("\t\t\t\t");
                                bParametroIngresado = true;
                            }
                        }

                        if (formulario.chkUsarSpsIdentity.Checked)
                        {
                            intAux = 1;
                        }
                        else
                        {
                            intAux = 0;
                        }

                        for (int iColumna = intAux; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                        {
                            nameCol = dtColumns.Rows[iColumna][0].ToString();
                            tipoCol = dtColumns.Rows[iColumna][1].ToString();
                            permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"] == "Yes" ? true : false);
                            if (dtColumns.Rows[iColumna]["EsIdentidad"].ToString() == "No")
                            {
                                if (nameCol.ToUpper() == nameParametro.ToUpper())
                                {
                                    if (!permiteNull & formulario.chkUsarSpsCheck.Checked)
                                    {
                                        newTexto.AppendLine("\t\t\t\tif (obj." + nameCol + " == null)");
                                        newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"El Parametro " + nameCol + " no puede ser NULO.\");");
                                    }

                                    //If DBMS = TipoBD.Oracle.ToString() Then
                                    //    If formulario.chkParametroPa.Checked Then
                                    //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                                    //    Else
                                    //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & nameParametro & Chr(34) & ");")
                                    //    End If
                                    //Else
                                    //    If formulario.chkParametroPa.Checked Then
                                    //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                                    //    Else
                                    //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & pNameClaseEnt & ".Fields." & nameParametro & ".ToString());")
                                    //    End If
                                    //End If
                                    if (formulario.chkParametroPa.Checked)
                                    {
                                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameParametro + "\");");
                                    }
                                    else
                                    {
                                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(" + pNameClaseEnt + ".Fields." + nameParametro + ".ToString());");
                                    }

                                    newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ");");
                                    newTexto.AppendLine("\t\t\t\t");
                                    bParametroIngresado = true;
                                }
                            }
                        }

                        if (!bParametroIngresado)
                        {
                            newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\");");
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"\");");
                        }
                    }

                    if (formulario.chkUsarSpsIdentity.Checked)
                    {
                        nameCol = dtColumns.Rows[0][0].ToString();
                        newTexto.AppendLine("\t\t\t\t");
                        newTexto.AppendLine("\t\t\t\t//Llamamos al Procedmiento Almacenado");
                        newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
                        newTexto.AppendLine("\t\t\t\tstring nombreSp = " + paramClaseListadoSp + "." + pNameClase + "." + formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins.ToString();");
                        newTexto.AppendLine("\t\t\t\tobj." + nameCol + " = (local.ExecStoreProcedureIdentity(nombreSp, arrNombreParam, arrValoresParam, ref localTrans)).ToString();");
                        newTexto.AppendLine("\t\t\t\treturn (!string.IsNullOrEmpty(obj." + nameCol + ".ToString()));");
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t\t\t");
                        newTexto.AppendLine("\t\t\t\t//Llamamos al Procedmiento Almacenado");
                        newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
                        newTexto.AppendLine("\t\t\t\tstring nombreSp = " + paramClaseListadoSp + "." + pNameClase + "." + formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins.ToString();");
                        newTexto.AppendLine("\t\t\t\treturn (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans) > 0);");
                    }

                    newTexto.AppendLine("\t\t\t}");
                    newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                    newTexto.AppendLine("\t\t\t{");
                    newTexto.AppendLine("\t\t\t\tthrow exp;");
                    newTexto.AppendLine("\t\t\t}");
                    newTexto.AppendLine("\t\t}\r\n");
                }
            }
            else
            {
                //No existen los Proc Almacenados
                //INSERT
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t/// </returns>");
                newTexto.AppendLine("\t\tpublic bool Insert(" + pNameClaseEnt + " obj)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tthrow new Exception(\"No existe el Procedimiento Almacenado " + formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins.\");");
                newTexto.AppendLine("\t\t}\r\n");

                //INSERT TRANS
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t/// </returns>");
                newTexto.AppendLine("\t\tpublic bool Insert(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tthrow new Exception(\"No existe el Procedimiento Almacenado " + formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins.\");");
                newTexto.AppendLine("\t\t}\r\n");
            }

            switch (formulario.cmbTipoColumnas.SelectedIndex)
            {
                case 0:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd", TipoColumnas.PrimeraMayuscula);
                    break;

                case 1:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd", TipoColumnas.Minusculas);
                    break;

                case 2:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd", TipoColumnas.Mayusculas);
                    break;
            }
            if (dtParametros.Rows.Count > 0)
            {
                //UPDATE
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \t Valor que indica la cantidad de registros actualizados en " + pNameTabla);
                newTexto.AppendLine("\t\t/// </returns>");
                newTexto.AppendLine("\t\tpublic int Update(" + pNameClaseEnt + " obj, bool bValidar = true)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                if (formulario.chkDataAnnotations.Checked)
                {
                    newTexto.AppendLine("\t\t\t\tif (bValidar)");
                    newTexto.AppendLine("\t\t\t\t\tif (!obj.IsValid())");
                    newTexto.AppendLine("\t\t\t\t\t\tthrow new System.ComponentModel.DataAnnotations.ValidationException(obj.ValidationErrorsString());");
                }
                newTexto.AppendLine("\t\t\t\tArrayList arrNombreParam = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresParam = new ArrayList();");

                for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
                {
                    dynamic bParametroIngresado = false;
                    dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                    for (int iColumna = 0; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                    {
                        nameCol = dtColumns.Rows[iColumna][0].ToString();
                        tipoCol = dtColumns.Rows[iColumna][1].ToString();
                        permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"] == "Yes" ? true : false);

                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            if (!permiteNull & formulario.chkUsarSpsCheck.Checked)
                            {
                                newTexto.AppendLine("\t\t\t\tif (obj." + nameCol + " == null)");
                                newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"El Parametro " + nameCol + " no puede ser NULO.\");");
                            }

                            //If DBMS = TipoBD.Oracle.ToString() Then
                            //    If formulario.chkParametroPa.Checked Then
                            //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                            //    Else
                            //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & nameParametro & Chr(34) & ");")
                            //    End If
                            //Else
                            //    If formulario.chkParametroPa.Checked Then
                            //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                            //    Else
                            //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & pNameClaseEnt & ".Fields." & nameParametro & ".ToString());")
                            //    End If
                            //End If
                            if (formulario.chkParametroPa.Checked)
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameParametro + "\");");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(" + pNameClaseEnt + ".Fields." + nameParametro + ".ToString());");
                            }

                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ");");
                            newTexto.AppendLine("\t\t\t\t");
                            bParametroIngresado = true;
                        }
                    }

                    if (!bParametroIngresado)
                    {
                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\");");
                        newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"\");");
                    }
                }

                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t//Llamamos al Procedmiento Almacenado");
                newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
                newTexto.AppendLine("\t\t\t\tstring nombreSp = " + paramClaseListadoSp + "." + pNameClase + "." + formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd.ToString();");
                newTexto.AppendLine("\t\t\t\treturn local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);");

                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}\r\n");

                if (miEntorno != TipoEntorno.CSharp_WCF)
                {
                    //UPDATE TRANS
                    newTexto.AppendLine("\t\t/// <summary>");
                    newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                    newTexto.AppendLine("\t\t/// </summary>");
                    newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                    newTexto.AppendLine("\t\t///     <para>");
                    newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                    newTexto.AppendLine("\t\t///     </para>");
                    newTexto.AppendLine("\t\t/// </param>");
                    newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                    newTexto.AppendLine("\t\t///     <para>");
                    newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                    newTexto.AppendLine("\t\t///     </para>");
                    newTexto.AppendLine("\t\t/// </param>");
                    newTexto.AppendLine("\t\t/// <returns>");
                    newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                    newTexto.AppendLine("\t\t/// </returns>");
                    newTexto.AppendLine("\t\tpublic int Update(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans, bool bValidar = true)");
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\ttry");
                    newTexto.AppendLine("\t\t\t{");
                    if (formulario.chkDataAnnotations.Checked)
                    {
                        newTexto.AppendLine("\t\t\t\tif (bValidar)");
                        newTexto.AppendLine("\t\t\t\t\tif (!obj.IsValid())");
                        newTexto.AppendLine("\t\t\t\t\t\tthrow new System.ComponentModel.DataAnnotations.ValidationException(obj.ValidationErrorsString());");
                    }
                    newTexto.AppendLine("\t\t\t\tArrayList arrNombreParam = new ArrayList();");
                    newTexto.AppendLine("\t\t\t\tArrayList arrValoresParam = new ArrayList();");

                    for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
                    {
                        dynamic bParametroIngresado = false;
                        dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                        for (int iColumna = 0; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                        {
                            nameCol = dtColumns.Rows[iColumna][0].ToString();
                            tipoCol = dtColumns.Rows[iColumna][1].ToString();
                            permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"] == "Yes" ? true : false);

                            if (nameCol.ToUpper() == nameParametro.ToUpper())
                            {
                                if (!permiteNull & formulario.chkUsarSpsCheck.Checked)
                                {
                                    newTexto.AppendLine("\t\t\t\tif (obj." + nameCol + " == null)");
                                    newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"El Parametro " + nameCol + " no puede ser NULO.\");");
                                }

                                //If DBMS = TipoBD.Oracle.ToString() Then
                                //    If formulario.chkParametroPa.Checked Then
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                                //    Else
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & nameParametro & Chr(34) & ");")
                                //    End If
                                //Else
                                //    If formulario.chkParametroPa.Checked Then
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                                //    Else
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & pNameClaseEnt & ".Fields." & nameParametro & ".ToString());")
                                //    End If
                                //End If
                                if (formulario.chkParametroPa.Checked)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameParametro + "\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(" + pNameClaseEnt + ".Fields." + nameParametro + ".ToString());");
                                }

                                newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ");");
                                newTexto.AppendLine("\t\t\t\t");
                                bParametroIngresado = true;
                            }
                        }

                        if (!bParametroIngresado)
                        {
                            newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\");");
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"\");");
                        }
                    }

                    newTexto.AppendLine("\t\t\t\t");
                    newTexto.AppendLine("\t\t\t\t//Llamamos al Procedmiento Almacenado");
                    newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
                    newTexto.AppendLine("\t\t\t\tstring nombreSp = " + paramClaseListadoSp + "." + pNameClase + "." + formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd.ToString();");
                    newTexto.AppendLine("\t\t\t\treturn local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);");

                    newTexto.AppendLine("\t\t\t}");
                    newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                    newTexto.AppendLine("\t\t\t{");
                    newTexto.AppendLine("\t\t\t\tthrow exp;");
                    newTexto.AppendLine("\t\t\t}");
                    newTexto.AppendLine("\t\t}\r\n");
                }
            }
            else
            {
                //UPDATE
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t/// </returns>");
                newTexto.AppendLine("\t\tpublic int Update(" + pNameClaseEnt + " obj)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tthrow new Exception(\"No existe el Procedimiento Almacenado " + formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd.\");");
                newTexto.AppendLine("\t\t}\r\n");

                //UPDATE TRANS
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t/// </returns>");
                newTexto.AppendLine("\t\tpublic int Update(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tthrow new Exception(\"No existe el Procedimiento Almacenado " + formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd.\");");
                newTexto.AppendLine("\t\t}\r\n");
            }

            switch (formulario.cmbTipoColumnas.SelectedIndex)
            {
                case 0:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del", TipoColumnas.PrimeraMayuscula);
                    break;

                case 1:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del", TipoColumnas.Minusculas);
                    break;

                case 2:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del", TipoColumnas.Mayusculas);
                    break;
            }

            if (dtParametros.Rows.Count > 0)
            {
                //DELETE
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t/// </returns>");
                newTexto.AppendLine("\t\tpublic int Delete(" + pNameClaseEnt + " obj, bool bValidar = true)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                if (formulario.chkDataAnnotations.Checked)
                {
                    newTexto.AppendLine("\t\t\t\tif (bValidar)");
                    newTexto.AppendLine("\t\t\t\t\tif (!obj.IsValid())");
                    newTexto.AppendLine("\t\t\t\t\t\tthrow new System.ComponentModel.DataAnnotations.ValidationException(obj.ValidationErrorsString());");
                }
                newTexto.AppendLine("\t\t\t\tArrayList arrNombreParam = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresParam = new ArrayList();");

                for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
                {
                    var bParametroIngresado = false;
                    var nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                    for (int iColumna = 0; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                    {
                        nameCol = dtColumns.Rows[iColumna][0].ToString();
                        tipoCol = dtColumns.Rows[iColumna][1].ToString();
                        permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"] == "Yes" ? true : false);

                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            if (!permiteNull & formulario.chkUsarSpsCheck.Checked)
                            {
                                newTexto.AppendLine("\t\t\t\tif (obj." + nameCol + " == null)");
                                newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"El Parametro " + nameCol + " no puede ser NULO.\");");
                            }

                            //If DBMS = TipoBD.Oracle.ToString() Then
                            //    If formulario.chkParametroPa.Checked Then
                            //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                            //    Else
                            //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & nameParametro & Chr(34) & ");")
                            //    End If
                            //Else
                            //    If formulario.chkParametroPa.Checked Then
                            //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                            //    Else
                            //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & pNameClaseEnt & ".Fields." & nameParametro & ".ToString());")
                            //    End If
                            //End If
                            if (formulario.chkParametroPa.Checked)
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameParametro + "\");");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(" + pNameClaseEnt + ".Fields." + nameParametro + ".ToString());");
                            }

                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ");");
                            newTexto.AppendLine("\t\t\t\t");
                            bParametroIngresado = true;
                        }
                    }

                    if (!bParametroIngresado)
                    {
                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\");");
                        newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"\");");
                    }
                }

                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t//Llamamos al Procedmiento Almacenado");
                newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
                newTexto.AppendLine("\t\t\t\tstring nombreSp = " + paramClaseListadoSp + "." + pNameClase + "." + formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del.ToString();");
                newTexto.AppendLine("\t\t\t\treturn local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);");

                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}\r\n");

                if (miEntorno != TipoEntorno.CSharp_WCF)
                {
                    //DELETE TRANS
                    newTexto.AppendLine("\t\t/// <summary>");
                    newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                    newTexto.AppendLine("\t\t/// </summary>");
                    newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                    newTexto.AppendLine("\t\t///     <para>");
                    newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                    newTexto.AppendLine("\t\t///     </para>");
                    newTexto.AppendLine("\t\t/// </param>");
                    newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                    newTexto.AppendLine("\t\t///     <para>");
                    newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                    newTexto.AppendLine("\t\t///     </para>");
                    newTexto.AppendLine("\t\t/// </param>");
                    newTexto.AppendLine("\t\t/// <returns>");
                    newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                    newTexto.AppendLine("\t\t/// </returns>");
                    newTexto.AppendLine("\t\tpublic int Delete(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans, bool bValidar = true)");
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\ttry");
                    newTexto.AppendLine("\t\t\t{");
                    if (formulario.chkDataAnnotations.Checked)
                    {
                        newTexto.AppendLine("\t\t\t\tif (bValidar)");
                        newTexto.AppendLine("\t\t\t\t\tif (!obj.IsValid())");
                        newTexto.AppendLine("\t\t\t\t\t\tthrow new System.ComponentModel.DataAnnotations.ValidationException(obj.ValidationErrorsString());");
                    }
                    newTexto.AppendLine("\t\t\t\tArrayList arrNombreParam = new ArrayList();");
                    newTexto.AppendLine("\t\t\t\tArrayList arrValoresParam = new ArrayList();");

                    for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
                    {
                        dynamic bParametroIngresado = false;
                        dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                        for (int iColumna = 0; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                        {
                            nameCol = dtColumns.Rows[iColumna][0].ToString();
                            tipoCol = dtColumns.Rows[iColumna][1].ToString();
                            permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"] == "Yes" ? true : false);

                            if (nameCol.ToUpper() == nameParametro.ToUpper())
                            {
                                if (!permiteNull & formulario.chkUsarSpsCheck.Checked)
                                {
                                    newTexto.AppendLine("\t\t\t\tif (obj." + nameCol + " == null)");
                                    newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"El Parametro " + nameCol + " no puede ser NULO.\");");
                                }

                                //If DBMS = TipoBD.Oracle.ToString() Then
                                //    If formulario.chkParametroPa.Checked Then
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                                //    Else
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & nameParametro & Chr(34) & ");")
                                //    End If
                                //Else
                                //    If formulario.chkParametroPa.Checked Then
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                                //    Else
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & pNameClaseEnt & ".Fields." & nameParametro & ".ToString());")
                                //    End If
                                //End If
                                if (formulario.chkParametroPa.Checked)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameParametro + "\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(" + pNameClaseEnt + ".Fields." + nameParametro + ".ToString());");
                                }

                                newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ");");
                                newTexto.AppendLine("\t\t\t\t");
                                bParametroIngresado = true;
                            }
                        }

                        if (!bParametroIngresado)
                        {
                            newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\");");
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"\");");
                        }
                    }

                    newTexto.AppendLine("\t\t\t\t");
                    newTexto.AppendLine("\t\t\t\t//Llamamos al Procedmiento Almacenado");
                    newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
                    newTexto.AppendLine("\t\t\t\tstring nombreSp = " + paramClaseListadoSp + "." + pNameClase + "." + formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del.ToString();");
                    newTexto.AppendLine("\t\t\t\treturn local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);");

                    newTexto.AppendLine("\t\t\t}");
                    newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                    newTexto.AppendLine("\t\t\t{");
                    newTexto.AppendLine("\t\t\t\tthrow exp;");
                    newTexto.AppendLine("\t\t\t}");
                    newTexto.AppendLine("\t\t}\r\n");
                }
            }
            else
            {
                //DELETE
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t/// </returns>");
                newTexto.AppendLine("\t\tpublic int Delete(" + pNameClaseEnt + " obj)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tthrow new Exception(\"No existe el Procedimiento Almacenado " + formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del.\");");
                newTexto.AppendLine("\t\t}\r\n");

                //DELETE TRANS
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t/// </returns>");
                newTexto.AppendLine("\t\tpublic int Delete(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\tthrow new Exception(\"No existe el Procedimiento Almacenado " + formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del.\");");
                newTexto.AppendLine("\t\t}\r\n");
            }

            switch (formulario.cmbTipoColumnas.SelectedIndex)
            {
                case 0:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "InsUpd", TipoColumnas.PrimeraMayuscula);
                    break;

                case 1:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "InsUpd", TipoColumnas.Minusculas);
                    break;

                case 2:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "InsUpd", TipoColumnas.Mayusculas);
                    break;
            }
            if (dtParametros.Rows.Count > 0)
            {
                //InsUpd
                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t/// </returns>");
                newTexto.AppendLine("\t\tpublic bool InsertUpdate(" + pNameClaseEnt + " obj)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tArrayList arrNombreParam = new ArrayList();");
                newTexto.AppendLine("\t\t\t\tArrayList arrValoresParam = new ArrayList();");

                for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
                {
                    dynamic bParametroIngresado = false;
                    dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                    for (int iColumna = 0; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                    {
                        nameCol = dtColumns.Rows[iColumna][0].ToString();
                        tipoCol = dtColumns.Rows[iColumna][1].ToString();
                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            if (formulario.chkUsarSpsCheck.Checked)
                            {
                                newTexto.AppendLine("\t\t\t\tif (obj." + nameCol + " == null)");
                                newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"El Parametro " + nameCol + " no puede ser NULO.\");");
                            }

                            //If DBMS = TipoBD.Oracle.ToString() Then
                            //    If formulario.chkParametroPa.Checked Then
                            //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                            //    Else
                            //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & nameParametro & Chr(34) & ");")
                            //    End If
                            //Else
                            //    If formulario.chkParametroPa.Checked Then
                            //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(pa" & Chr(34) & nameParametro & Chr(34) & ");")
                            //    Else
                            //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & pNameClaseEnt & ".Fields." & nameParametro & ".ToString());")
                            //    End If
                            //End If
                            if (formulario.chkParametroPa.Checked)
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(pa\"" + nameParametro + "\");");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(" + pNameClaseEnt + ".Fields." + nameParametro + ".ToString());");
                            }

                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ");");
                            newTexto.AppendLine("\t\t\t\t");
                            bParametroIngresado = true;
                        }
                    }

                    if (!bParametroIngresado)
                    {
                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\");");
                        newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"\");");
                    }
                }

                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t//Llamamos al Procedmiento Almacenado");
                newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
                newTexto.AppendLine("\t\t\t\treturn local.ExecStoreProcedure(" + paramClaseListadoSp + "." + pNameTabla + ".sp" + pNameTabla + "InsUpd, arrNombreParam, arrValoresParam);");

                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}\r\n");

                if (miEntorno != TipoEntorno.CSharp_WCF)
                {
                    //InsUpd TRANS
                    newTexto.AppendLine("\t\t/// <summary>");
                    newTexto.AppendLine("\t\t/// \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                    newTexto.AppendLine("\t\t/// </summary>");
                    newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                    newTexto.AppendLine("\t\t///     <para>");
                    newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                    newTexto.AppendLine("\t\t///     </para>");
                    newTexto.AppendLine("\t\t/// </param>");
                    newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                    newTexto.AppendLine("\t\t///     <para>");
                    newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                    newTexto.AppendLine("\t\t///     </para>");
                    newTexto.AppendLine("\t\t/// </param>");
                    newTexto.AppendLine("\t\t/// <returns>");
                    newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                    newTexto.AppendLine("\t\t/// </returns>");
                    newTexto.AppendLine("\t\tpublic bool InsertUpdate(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans)");
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\ttry");
                    newTexto.AppendLine("\t\t\t{");
                    newTexto.AppendLine("\t\t\t\tArrayList arrNombreParam = new ArrayList();");
                    newTexto.AppendLine("\t\t\t\tArrayList arrValoresParam = new ArrayList();");

                    for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
                    {
                        dynamic bParametroIngresado = false;
                        dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                        for (int iColumna = 0; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                        {
                            nameCol = dtColumns.Rows[iColumna][0].ToString();
                            tipoCol = dtColumns.Rows[iColumna][1].ToString();
                            if (nameCol.ToUpper() == nameParametro.ToUpper())
                            {
                                if (formulario.chkUsarSpsCheck.Checked)
                                {
                                    newTexto.AppendLine("\t\t\t\tif (obj." + nameCol + " == null)");
                                    newTexto.AppendLine("\t\t\t\t\tthrow new Exception(\"El Parametro " + nameCol + " no puede ser NULO.\");");
                                }

                                //If DBMS = TipoBD.Oracle.ToString() Then
                                //    If formulario.chkParametroPa.Checked Then
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & "pa" & nameParametro & Chr(34) & ");")
                                //    Else
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & Chr(34) & nameParametro & Chr(34) & ");")
                                //    End If
                                //Else
                                //    If formulario.chkParametroPa.Checked Then
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(pa" & Chr(34) & nameParametro & Chr(34) & ");")
                                //    Else
                                //        newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrNombreParam.Add(" & pNameClaseEnt & ".Fields." & nameParametro & ".ToString());")
                                //    End If
                                //End If
                                if (formulario.chkParametroPa.Checked)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(pa\"" + nameParametro + "\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(" + pNameClaseEnt + ".Fields." + nameParametro + ".ToString());");
                                }

                                newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ");");
                                newTexto.AppendLine("\t\t\t\t");
                                bParametroIngresado = true;
                            }
                        }

                        if (!bParametroIngresado)
                        {
                            newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\");");
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"\");");
                        }
                    }

                    newTexto.AppendLine("\t\t\t\t");
                    newTexto.AppendLine("\t\t\t\t//Llamamos al Procedmiento Almacenado");
                    newTexto.AppendLine("\t\t\t\t" + paramClaseConeccion + " local = new " + paramClaseConeccion + "();");
                    newTexto.AppendLine("\t\t\t\treturn local.ExecStoreProcedure(" + paramClaseListadoSp + "." + pNameTabla + ".sp" + pNameTabla + "InsUpd, arrNombreParam, arrValoresParam, localTrans);");

                    newTexto.AppendLine("\t\t\t}");
                    newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                    newTexto.AppendLine("\t\t\t{");
                    newTexto.AppendLine("\t\t\t\tthrow exp;");
                    newTexto.AppendLine("\t\t\t}");
                    newTexto.AppendLine("\t\t}\r\n");
                }
            }
            else
            {
                //Hacemos el  InsUpd con los procedimientos existentes

                newTexto.AppendLine("\t\t/// <summary>");
                newTexto.AppendLine("\t\t/// \tFuncion que inserta o actualiza un registro un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t/// </summary>");
                newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t///     <para>");
                newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar o actualizar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t///     </para>");
                newTexto.AppendLine("\t\t/// </param>");
                newTexto.AppendLine("\t\t/// <returns>");
                newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t/// </returns>");
                newTexto.AppendLine("\t\tpublic int InsertUpdate(" + pNameClaseEnt + " obj)");
                newTexto.AppendLine("\t\t{");
                newTexto.AppendLine("\t\t\ttry");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tbool esInsertar = true;");
                newTexto.AppendLine("\t\t\t\t");

                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                    {
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        newTexto.AppendLine("\t\t\t\t\tesInsertar = (esInsertar && (obj." + nameCol + " == null));");
                    }
                }
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tif (esInsertar)");
                if ((formulario.chkUsarSpsIdentity.Checked))
                {
                    newTexto.AppendLine("\t\t\t\t\treturn Insert(ref obj) ? 1 : 0;");
                }
                else
                {
                    newTexto.AppendLine("\t\t\t\t\treturn Insert(obj) ? 1 : 0;");
                }
                newTexto.AppendLine("\t\t\t\telse");
                newTexto.AppendLine("\t\t\t\t\treturn Update(obj);");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                newTexto.AppendLine("\t\t\t{");
                newTexto.AppendLine("\t\t\t\tthrow exp;");
                newTexto.AppendLine("\t\t\t}");
                newTexto.AppendLine("\t\t}\r\n");

                if (miEntorno != TipoEntorno.CSharp_WCF)
                {
                    //INSUPD con TRANS
                    newTexto.AppendLine("\t\t/// <summary>");
                    newTexto.AppendLine("\t\t/// \tFuncion que inserta o actualiza un registro un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                    newTexto.AppendLine("\t\t/// </summary>");
                    newTexto.AppendLine("\t\t/// <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                    newTexto.AppendLine("\t\t///     <para>");
                    newTexto.AppendLine("\t\t/// \t\t Clase desde la que se van a insertar o actualizar los valores a la tabla " + pNameTabla);
                    newTexto.AppendLine("\t\t///     </para>");
                    newTexto.AppendLine("\t\t/// </param>");
                    newTexto.AppendLine("\t\t/// <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                    newTexto.AppendLine("\t\t///     <para>");
                    newTexto.AppendLine("\t\t/// \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                    newTexto.AppendLine("\t\t///     </para>");
                    newTexto.AppendLine("\t\t/// </param>");
                    newTexto.AppendLine("\t\t/// <returns>");
                    newTexto.AppendLine("\t\t/// \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                    newTexto.AppendLine("\t\t/// </returns>");
                    newTexto.AppendLine("\t\tpublic int InsertUpdate(" + pNameClaseEnt + " obj, ref " + paramClaseTransaccion + " localTrans)");
                    newTexto.AppendLine("\t\t{");
                    newTexto.AppendLine("\t\t\ttry");
                    newTexto.AppendLine("\t\t\t{");
                    newTexto.AppendLine("\t\t\t\tbool esInsertar = false;");
                    newTexto.AppendLine("\t\t\t\t");

                    for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                    {
                        if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                        {
                            nameCol = dtColumns.Rows[iCol][0].ToString();
                            newTexto.AppendLine("\t\t\t\t\tesInsertar = (esInsertar && (obj." + nameCol + " == null));");
                        }
                    }
                    newTexto.AppendLine("\t\t\t\t");
                    newTexto.AppendLine("\t\t\t\tif (esInsertar)");
                    if (formulario.chkUsarSpsIdentity.Checked)
                    {
                        newTexto.AppendLine("\t\t\t\t\treturn Insert(ref obj, ref localTrans) ? 1 : 0;");
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t\t\t\treturn Insert(obj, ref localTrans) ? 1 : 0;");
                    }
                    newTexto.AppendLine("\t\t\t\telse");
                    newTexto.AppendLine("\t\t\t\t\treturn Update(obj, ref localTrans);");
                    newTexto.AppendLine("\t\t\t}");
                    newTexto.AppendLine("\t\t\tcatch (Exception exp)");
                    newTexto.AppendLine("\t\t\t{");
                    newTexto.AppendLine("\t\t\t\tthrow exp;");
                    newTexto.AppendLine("\t\t\t}");
                    newTexto.AppendLine("\t\t}\r\n");
                }
            }
            return newTexto.ToString();
        }

        public string CrearABMsProcAlmVB(DataTable dtColumns, string pNameTabla, ref FPrincipal formulario)
        {
            TipoEntorno miEntorno;
            Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);
            string paramClaseListadoSp = (string.IsNullOrEmpty(formulario.txtClaseParametrosListadoSp.Text) ? "cListadoSp" : formulario.txtClaseParametrosListadoSp.Text);
            string paramClaseConeccion = (string.IsNullOrEmpty(formulario.txtClaseConn.Text) ? "cConn" : formulario.txtClaseConn.Text);
            string paramClaseTransaccion = (string.IsNullOrEmpty(formulario.txtClaseTransaccion.Text) ? "cTransaccion" : formulario.txtClaseTransaccion.Text);
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string formatoFechaHora = paramClaseParametros + ".parFormatoFechaHora";
            string formatoFecha = paramClaseParametros + ".parFormatoFecha";

            string nameCol = null;
            string tipoCol = null;
            bool permiteNull = false;
            pNameTabla = pNameTabla.Replace("_", "");
            pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() + pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();

            string pNameClaseMod = formulario.txtPrefijoModelo.Text + pNameTabla;
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameTabla;
            string pNameClaseIn = formulario.txtPrefijoInterface.Text + pNameTabla;

            //Los ProcAlm ya deberia star creados, si no esto no funciona bien
            DataTable dtParametros = new DataTable();
            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            int intAux = 0;

            //Obtenemos el nombre del SP en base al prefijo
            string prefijo = pNameTabla;
            try
            {
                DataTable dtPrefijo = new DataTable();

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE tablaSegTablas = '" + pNameTabla + "'");
                        break;

                    case TipoBD.SQLServer2000:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE tablaSegTablas = '" + pNameTabla + "'");
                        break;

                    case TipoBD.Oracle:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT ALIASSTA FROM SEGTABLAS WHERE UPPER(TABLASTA) = '" + pNameTabla.ToUpper() + "'");
                        break;

                    case TipoBD.PostgreSQL:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.aliassta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" + pNameTabla + "')");
                        break;
                }

                if (dtPrefijo.Rows.Count > 0)
                {
                    prefijo = dtPrefijo.Rows[0][0].ToString().ToString();
                    prefijo = prefijo.Substring(0, 1).ToUpper() + prefijo.Substring(1, prefijo.Length - 1);
                }
                else
                {
                    prefijo = pNameTabla;
                }
            }
            catch (Exception ex)
            {
                prefijo = pNameTabla;
            }

            //Para cada operacion obtenemos los parametros del Proc Almacenado
            switch (formulario.cmbTipoColumnas.SelectedIndex)
            {
                case 0:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins", TipoColumnas.PrimeraMayuscula);
                    break;

                case 1:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins", TipoColumnas.Minusculas);
                    break;

                case 2:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Ins", TipoColumnas.Mayusculas);
                    break;
            }
            if (dtParametros.Rows.Count > 0)
            {
                //INSERT
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <returns>");
                newTexto.AppendLine("\t\t''' \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t''' </returns>");

                if (formulario.chkReAlInterface.Checked)
                {
                    if (formulario.chkUsarSpsIdentity.Checked)
                    {
                        newTexto.AppendLine("\t\tPublic Function Insert(ByRef obj As " + pNameClaseEnt + ") As Boolean Implements " + pNameClaseIn + ".Insert");
                    }
                    else
                    {
                        newTexto.AppendLine("\t\tPublic Function Insert(ByVal obj As " + pNameClaseEnt + ") As Boolean Implements " + pNameClaseIn + ".Insert");
                    }
                }
                else
                {
                    if (formulario.chkUsarSpsIdentity.Checked)
                    {
                        newTexto.AppendLine("\t\tPublic Function Insert(ByRef obj As " + pNameClaseEnt + ") As Boolean");
                    }
                    else
                    {
                        newTexto.AppendLine("\t\tPublic Function Insert(ByVal obj As " + pNameClaseEnt + ") As Boolean");
                    }
                }

                newTexto.AppendLine("\t\t\tTry");
                if (formulario.chkDataAnnotations.Checked)
                {
                    newTexto.AppendLine("\t\t\t\tIf Not obj.IsValid() Then");
                    newTexto.AppendLine("\t\t\t\t\tThrow new System.ComponentModel.DataAnnotations.ValidationException(obj.ValidationErrorsString())");
                    newTexto.AppendLine("\t\t\t\tEnd If");
                }
                newTexto.AppendLine("\t\t\t\tDim arrNombreParam As ArrayList = New ArrayList");
                newTexto.AppendLine("\t\t\t\tDim arrValoresParam As ArrayList = New ArrayList");

                for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
                {
                    var bParametroIngresado = false;
                    var nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                    //Para InsertIdentity
                    if (formulario.chkUsarSpsIdentity.Checked)
                    {
                        nameCol = dtColumns.Rows[0][0].ToString();
                        tipoCol = dtColumns.Rows[0][1].ToString();
                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            if (formulario.chkParametroPa.Checked)
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameParametro + "\")");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                            }
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ")");
                            newTexto.AppendLine("\t\t\t\t");
                            bParametroIngresado = true;
                        }
                    }

                    if (formulario.chkUsarSpsIdentity.Checked)
                    {
                        intAux = 1;
                    }
                    else
                    {
                        intAux = 0;
                    }

                    for (int iColumna = intAux; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                    {
                        nameCol = dtColumns.Rows[iColumna][0].ToString();
                        tipoCol = dtColumns.Rows[iColumna][1].ToString();
                        permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"] == "Yes" ? true : false);

                        if (dtColumns.Rows[iColumna]["EsIdentidad"].ToString() == "No")
                        {
                            if (nameCol.ToUpper() == nameParametro.ToUpper())
                            {
                                if (!permiteNull)
                                {
                                    newTexto.AppendLine("\t\t\t\tIf (IsNothing(obj." + nameCol + ")) Then");
                                    newTexto.AppendLine("\t\t\t\t\tThrow new Exception(\"El Parametro " + nameCol + " no puede ser NULO.\")");
                                    newTexto.AppendLine("\t\t\t\tEnd If");
                                }

                                if (formulario.chkParametroPa.Checked)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameParametro + "\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                                }
                                newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ")");
                                newTexto.AppendLine("\t\t\t\t");
                                bParametroIngresado = true;
                            }
                        }
                    }

                    if (!bParametroIngresado)
                    {
                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                        newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(Nothing)");
                    }
                }

                if (formulario.chkUsarSpsIdentity.Checked)
                {
                    nameCol = dtColumns.Rows[0][0].ToString();
                    newTexto.AppendLine("\t\t\t\t");
                    newTexto.AppendLine("\t\t\t\t'Llamamos al Procedmiento Almacenado");
                    newTexto.AppendLine("\t\t\t\tDim local As " + paramClaseConeccion + " = New " + paramClaseConeccion);
                    newTexto.AppendLine("\t\t\t\tDim nombreSp As String = " + paramClaseListadoSp + "." + pNameTabla + ".Sp" + prefijo + "Ins.ToString()");
                    newTexto.AppendLine("\t\t\t\tobj." + nameCol + " = (local.execStoreProcedureIdentity(nombreSp, arrNombreParam, arrValoresParam)).ToString()");
                    newTexto.AppendLine("\t\t\t\tReturn (Not string.IsNullOrEmpty(obj." + nameCol + ".ToString()))");
                }
                else
                {
                    newTexto.AppendLine("\t\t\t\t");
                    newTexto.AppendLine("\t\t\t\t'Llamamos al Procedmiento Almacenado");
                    newTexto.AppendLine("\t\t\t\tDim local As " + paramClaseConeccion + " = New " + paramClaseConeccion);
                    newTexto.AppendLine("\t\t\t\tDim nombreSp As String = " + paramClaseListadoSp + "." + pNameTabla + ".Sp" + prefijo + "Ins.ToString()");
                    newTexto.AppendLine("\t\t\t\tReturn (local.execStoreProcedure(nombreSp, arrNombreParam, arrValoresParam) = 1)");
                }

                newTexto.AppendLine("\t\t\tCatch exp As Exception");
                newTexto.AppendLine("\t\t\t\tThrow exp");
                newTexto.AppendLine("\t\t\tEnd Try");
                newTexto.AppendLine("\t\tEnd Function\r\n");

                //INSERT TRANS
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <returns>");
                newTexto.AppendLine("\t\t''' \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t''' </returns>");
                if (formulario.chkReAlInterface.Checked)
                {
                    if (formulario.chkUsarSpsIdentity.Checked)
                    {
                        newTexto.AppendLine("\t\tPublic Function Insert(ByRef obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Boolean Implements " + pNameClaseIn + ".Insert");
                    }
                    else
                    {
                        newTexto.AppendLine("\t\tPublic Function Insert(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Boolean Implements " + pNameClaseIn + ".Insert");
                    }
                }
                else
                {
                    if (formulario.chkUsarSpsIdentity.Checked)
                    {
                        newTexto.AppendLine("\t\tPublic Function Insert(ByRef obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Boolean");
                    }
                    else
                    {
                        newTexto.AppendLine("\t\tPublic Function Insert(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Boolean");
                    }
                }
                newTexto.AppendLine("\t\t\tTry");
                if (formulario.chkDataAnnotations.Checked)
                {
                    newTexto.AppendLine("\t\t\t\tIf Not obj.IsValid() Then");
                    newTexto.AppendLine("\t\t\t\t\tThrow new System.ComponentModel.DataAnnotations.ValidationException(obj.ValidationErrorsString())");
                    newTexto.AppendLine("\t\t\t\tEnd If");
                }
                newTexto.AppendLine("\t\t\t\tDim arrNombreParam As ArrayList = New ArrayList");
                newTexto.AppendLine("\t\t\t\tDim arrValoresParam As ArrayList = New ArrayList");

                for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
                {
                    dynamic bParametroIngresado = false;
                    dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                    //Para InsertIdentity
                    if (formulario.chkUsarSpsIdentity.Checked)
                    {
                        nameCol = dtColumns.Rows[0][0].ToString();
                        tipoCol = dtColumns.Rows[0][1].ToString();

                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            if (formulario.chkParametroPa.Checked)
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameParametro + "\")");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                            }
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ")");
                            newTexto.AppendLine("\t\t\t\t");
                            bParametroIngresado = true;
                        }
                    }

                    if (formulario.chkUsarSpsIdentity.Checked)
                    {
                        intAux = 1;
                    }
                    else
                    {
                        intAux = 0;
                    }

                    for (int iColumna = intAux; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                    {
                        nameCol = dtColumns.Rows[iColumna][0].ToString();
                        tipoCol = dtColumns.Rows[iColumna][1].ToString();
                        permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"] == "Yes" ? true : false);
                        if (dtColumns.Rows[iColumna]["EsIdentidad"].ToString() == "No")
                        {
                            if (nameCol.ToUpper() == nameParametro.ToUpper())
                            {
                                if (!permiteNull)
                                {
                                    newTexto.AppendLine("\t\t\t\tIf (IsNothing(obj." + nameCol + ")) Then");
                                    newTexto.AppendLine("\t\t\t\t\tThrow new Exception(\"El Parametro " + nameCol + " no puede ser NULO.\")");
                                    newTexto.AppendLine("\t\t\t\tEnd If");
                                }

                                if (formulario.chkParametroPa.Checked)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameParametro + "\")");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                                }
                                newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ")");
                                newTexto.AppendLine("\t\t\t\t");
                                bParametroIngresado = true;
                            }
                        }
                    }

                    if (!bParametroIngresado)
                    {
                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                        newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"\")");
                    }
                }

                if (formulario.chkUsarSpsIdentity.Checked)
                {
                    nameCol = dtColumns.Rows[0][0].ToString();
                    newTexto.AppendLine("\t\t\t\t");
                    newTexto.AppendLine("\t\t\t\t'Llamamos al Procedmiento Almacenado");
                    newTexto.AppendLine("\t\t\t\tDim local As " + paramClaseConeccion + " = New " + paramClaseConeccion);
                    newTexto.AppendLine("\t\t\t\tDim nombreSp As String = " + paramClaseListadoSp + "." + pNameTabla + ".Sp" + prefijo + "Ins.ToString()");
                    newTexto.AppendLine("\t\t\t\tobj." + nameCol + " = (local.execStoreProcedureIdentity(nombreSp, arrNombreParam, arrValoresParam, localTrans)).ToString()");
                    newTexto.AppendLine("\t\t\t\treturn (Not string.IsNullOrEmpty(obj." + nameCol + ".ToString()))");
                }
                else
                {
                    newTexto.AppendLine("\t\t\t\t");
                    newTexto.AppendLine("\t\t\t\t'Llamamos al Procedmiento Almacenado");
                    newTexto.AppendLine("\t\t\t\tDim local As " + paramClaseConeccion + " = New " + paramClaseConeccion);
                    newTexto.AppendLine("\t\t\t\tDim nombreSp As String = " + paramClaseListadoSp + "." + pNameTabla + ".Sp" + prefijo + "Ins.ToString()");
                    newTexto.AppendLine("\t\t\t\tReturn (local.execStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, localTrans) = 1)");
                }

                newTexto.AppendLine("\t\t\tCatch exp As Exception");
                newTexto.AppendLine("\t\t\t\tThrow exp");
                newTexto.AppendLine("\t\t\tEnd Try");
                newTexto.AppendLine("\t\tEnd Function\r\n");
            }
            else
            {
                //No existen los Proc Almacenados
                //INSERT
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <returns>");
                newTexto.AppendLine("\t\t''' \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t''' </returns>");
                newTexto.AppendLine("\t\tPublic Function Insert(ByVal obj As " + pNameClaseEnt + ") As Boolean");
                newTexto.AppendLine("\t\t\tThrow new Exception(\"No existe el Procedimiento Almacenado SP" + prefijo + "Ins.\")");
                newTexto.AppendLine("\t\tEnd Function\r\n");

                //INSERT TRANS
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <returns>");
                newTexto.AppendLine("\t\t''' \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t''' </returns>");
                newTexto.AppendLine("\t\tPublic Function Insert(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Boolean");
                newTexto.AppendLine("\t\t\tThrow new Exception(\"No existe el Procedimiento Almacenado SP" + prefijo + "Ins.\")");
                newTexto.AppendLine("\t\tEnd Function\r\n");
            }

            switch (formulario.cmbTipoColumnas.SelectedIndex)
            {
                case 0:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd", TipoColumnas.PrimeraMayuscula);
                    break;

                case 1:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd", TipoColumnas.Minusculas);
                    break;

                case 2:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Upd", TipoColumnas.Mayusculas);
                    break;
            }
            if (dtParametros.Rows.Count > 0)
            {
                //UPDATE
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <returns>");
                newTexto.AppendLine("\t\t''' \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t''' </returns>");
                if (formulario.chkReAlInterface.Checked)
                {
                    newTexto.AppendLine("\t\tPublic Function Update(ByVal obj As " + pNameClaseEnt + ") As Integer Implements " + pNameClaseIn + ".Update");
                }
                else
                {
                    newTexto.AppendLine("\t\tPublic Function Update(ByVal obj As " + pNameClaseEnt + ") As Integer");
                }
                newTexto.AppendLine("\t\t\tTry");
                if (formulario.chkDataAnnotations.Checked)
                {
                    newTexto.AppendLine("\t\t\t\tIf Not obj.IsValid() Then");
                    newTexto.AppendLine("\t\t\t\t\tThrow new System.ComponentModel.DataAnnotations.ValidationException(obj.ValidationErrorsString())");
                    newTexto.AppendLine("\t\t\t\tEnd If");
                }
                newTexto.AppendLine("\t\t\t\tDim arrNombreParam As ArrayList = New ArrayList");
                newTexto.AppendLine("\t\t\t\tDim arrValoresParam As ArrayList = New ArrayList");

                for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
                {
                    var bParametroIngresado = false;
                    var nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                    for (int iColumna = 0; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                    {
                        nameCol = dtColumns.Rows[iColumna][0].ToString();
                        tipoCol = dtColumns.Rows[iColumna][1].ToString();
                        permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"] == "Yes" ? true : false);

                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            if (!permiteNull)
                            {
                                newTexto.AppendLine("\t\t\t\tIf (IsNothing(obj." + nameCol + ")) Then");
                                newTexto.AppendLine("\t\t\t\t\tThrow new Exception(\"El Parametro " + nameCol + " no puede ser NULO.\")");
                                newTexto.AppendLine("\t\t\t\tEnd If");
                            }

                            if (formulario.chkParametroPa.Checked)
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameParametro + "\")");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                            }
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ")");
                            newTexto.AppendLine("\t\t\t\t");
                            bParametroIngresado = true;
                        }
                    }

                    if (!bParametroIngresado)
                    {
                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                        newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"\")");
                    }
                }

                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t'Llamamos al Procedmiento Almacenado");
                newTexto.AppendLine("\t\t\t\tDim local As " + paramClaseConeccion + " = New " + paramClaseConeccion);
                newTexto.AppendLine("\t\t\t\tDim nombreSp As String = " + paramClaseListadoSp + "." + pNameTabla + ".Sp" + prefijo + "Upd.ToString()");
                newTexto.AppendLine("\t\t\t\tReturn local.execStoreProcedure(nombreSp, arrNombreParam, arrValoresParam)");
                newTexto.AppendLine("\t\t\tCatch exp As Exception");
                newTexto.AppendLine("\t\t\t\tThrow exp");
                newTexto.AppendLine("\t\t\tEnd Try");
                newTexto.AppendLine("\t\tEnd Function\r\n");

                //UPDATE TRANS
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <returns>");
                newTexto.AppendLine("\t\t''' \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t''' </returns>");
                if (formulario.chkReAlInterface.Checked)
                {
                    newTexto.AppendLine("\t\tPublic Function Update(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Integer Implements " + pNameClaseIn + ".Update");
                }
                else
                {
                    newTexto.AppendLine("\t\tPublic Function Update(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Integer");
                }
                newTexto.AppendLine("\t\t\tTry");
                if (formulario.chkDataAnnotations.Checked)
                {
                    newTexto.AppendLine("\t\t\t\tIf Not obj.IsValid() Then");
                    newTexto.AppendLine("\t\t\t\t\tThrow new System.ComponentModel.DataAnnotations.ValidationException(obj.ValidationErrorsString())");
                    newTexto.AppendLine("\t\t\t\tEnd If");
                }
                newTexto.AppendLine("\t\t\t\tDim arrNombreParam As ArrayList = New ArrayList");
                newTexto.AppendLine("\t\t\t\tDim arrValoresParam As ArrayList = New ArrayList");

                for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
                {
                    var bParametroIngresado = false;
                    var nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                    for (int iColumna = 0; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                    {
                        nameCol = dtColumns.Rows[iColumna][0].ToString();
                        tipoCol = dtColumns.Rows[iColumna][1].ToString();
                        permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"] == "Yes" ? true : false);

                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            if (!permiteNull)
                            {
                                newTexto.AppendLine("\t\t\t\tIf (IsNothing(obj." + nameCol + " )) Then");
                                newTexto.AppendLine("\t\t\t\t\tThrow new Exception(\"El Parametro " + nameCol + " no puede ser NULO.\")");
                                newTexto.AppendLine("\t\t\t\tEnd If");
                            }

                            if (formulario.chkParametroPa.Checked)
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameParametro + "\")");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                            }
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ")");
                            newTexto.AppendLine("\t\t\t\t");
                            bParametroIngresado = true;
                        }
                    }

                    if (!bParametroIngresado)
                    {
                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                        newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"\")");
                    }
                }

                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t'Llamamos al Procedmiento Almacenado");
                newTexto.AppendLine("\t\t\t\tDim local As " + paramClaseConeccion + " = New " + paramClaseConeccion);
                newTexto.AppendLine("\t\t\t\tDim nombreSp As String = " + paramClaseListadoSp + "." + pNameTabla + ".Sp" + prefijo + "Upd.ToString()");
                newTexto.AppendLine("\t\t\t\tReturn local.execStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, localTrans)");
                newTexto.AppendLine("\t\t\tCatch exp As Exception");
                newTexto.AppendLine("\t\t\t\tThrow exp");
                newTexto.AppendLine("\t\t\tEnd Try");
                newTexto.AppendLine("\t\tEnd Function\r\n");
            }
            else
            {
                //UPDATE
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <returns>");
                newTexto.AppendLine("\t\t''' \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t''' </returns>");
                newTexto.AppendLine("\t\tPublic Function Update(ByVal obj As " + pNameClaseEnt + ") As Integer");
                newTexto.AppendLine("\t\t\tThrow new Exception(\"No existe el Procedimiento Almacenado SP" + prefijo + "Upd.\")");
                newTexto.AppendLine("\t\tEnd Function\r\n");

                //UPDATE TRANS
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <returns>");
                newTexto.AppendLine("\t\t''' \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t''' </returns>");
                newTexto.AppendLine("\t\tPublic Function Update(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Integer");
                newTexto.AppendLine("\t\t\tThrow new Exception(\"No existe el Procedimiento Almacenado SP" + prefijo + "Upd.\")");
                newTexto.AppendLine("\t\tEnd Function\r\n");
            }

            switch (formulario.cmbTipoColumnas.SelectedIndex)
            {
                case 0:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del", TipoColumnas.PrimeraMayuscula);
                    break;

                case 1:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del", TipoColumnas.Minusculas);
                    break;

                case 2:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "Del", TipoColumnas.Mayusculas);
                    break;
            }
            if (dtParametros.Rows.Count > 0)
            {
                //DELETE
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <returns>");
                newTexto.AppendLine("\t\t''' \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t''' </returns>");
                if (formulario.chkReAlInterface.Checked)
                {
                    newTexto.AppendLine("\t\tPublic Function Delete(ByVal obj As " + pNameClaseEnt + ") As Integer Implements " + pNameClaseIn + ".Delete");
                }
                else
                {
                    newTexto.AppendLine("\t\tPublic Function Delete(ByVal obj As " + pNameClaseEnt + ") As Integer");
                }
                newTexto.AppendLine("\t\t\tTry");
                if (formulario.chkDataAnnotations.Checked)
                {
                    newTexto.AppendLine("\t\t\t\tIf Not obj.IsValid() Then");
                    newTexto.AppendLine("\t\t\t\t\tThrow new System.ComponentModel.DataAnnotations.ValidationException(obj.ValidationErrorsString())");
                    newTexto.AppendLine("\t\t\t\tEnd If");
                }
                newTexto.AppendLine("\t\t\t\tDim arrNombreParam As ArrayList = New ArrayList");
                newTexto.AppendLine("\t\t\t\tDim arrValoresParam As ArrayList = New ArrayList");

                for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
                {
                    dynamic bParametroIngresado = false;
                    dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                    for (int iColumna = 0; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                    {
                        nameCol = dtColumns.Rows[iColumna][0].ToString();
                        tipoCol = dtColumns.Rows[iColumna][1].ToString();
                        permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"] == "Yes" ? true : false);

                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            if (!permiteNull)
                            {
                                newTexto.AppendLine("\t\t\t\tIf (IsNothing(obj." + nameCol + ")) Then");
                                newTexto.AppendLine("\t\t\t\t\tThrow new Exception(\"El Parametro " + nameCol + " no puede ser NULO.\")");
                                newTexto.AppendLine("\t\t\t\tEnd If");
                            }

                            if (formulario.chkParametroPa.Checked)
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameParametro + "\")");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                            }
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ")");
                            newTexto.AppendLine("\t\t\t\t");
                            bParametroIngresado = true;
                        }
                    }

                    if (!bParametroIngresado)
                    {
                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                        newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"\")");
                    }
                }

                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t'Llamamos al Procedmiento Almacenado");
                newTexto.AppendLine("\t\t\t\tDim local As " + paramClaseConeccion + " = New " + paramClaseConeccion);
                newTexto.AppendLine("\t\t\t\tDim nombreSp As String = " + paramClaseListadoSp + "." + pNameTabla + ".Sp" + prefijo + "Del.ToString()");
                newTexto.AppendLine("\t\t\t\tReturn local.execStoreProcedure(nombreSp, arrNombreParam, arrValoresParam)");
                newTexto.AppendLine("\t\t\tCatch exp As Exception");
                newTexto.AppendLine("\t\t\t\tThrow exp");
                newTexto.AppendLine("\t\t\tEnd Try");
                newTexto.AppendLine("\t\tEnd Function\r\n");

                //DELETE TRANS
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <returns>");
                newTexto.AppendLine("\t\t''' \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t''' </returns>");
                if (formulario.chkReAlInterface.Checked)
                {
                    newTexto.AppendLine("\t\tPublic Function Delete(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Integer Implements " + pNameClaseIn + ".Delete");
                }
                else
                {
                    newTexto.AppendLine("\t\tPublic Function Delete(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Integer");
                }
                newTexto.AppendLine("\t\t\tTry");
                if (formulario.chkDataAnnotations.Checked)
                {
                    newTexto.AppendLine("\t\t\t\tIf Not obj.IsValid() Then");
                    newTexto.AppendLine("\t\t\t\t\tThrow new System.ComponentModel.DataAnnotations.ValidationException(obj.ValidationErrorsString())");
                    newTexto.AppendLine("\t\t\t\tEnd If");
                }
                newTexto.AppendLine("\t\t\t\tDim arrNombreParam As ArrayList = New ArrayList");
                newTexto.AppendLine("\t\t\t\tDim arrValoresParam As ArrayList = New ArrayList");

                for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
                {
                    dynamic bParametroIngresado = false;
                    dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                    for (int iColumna = 0; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                    {
                        nameCol = dtColumns.Rows[iColumna][0].ToString();
                        tipoCol = dtColumns.Rows[iColumna][1].ToString();
                        permiteNull = (dtColumns.Rows[iColumna]["PermiteNull"] == "Yes" ? true : false);

                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            if (!permiteNull)
                            {
                                newTexto.AppendLine("\t\t\t\tIf (IsNothing(obj." + nameCol + ")) Then");
                                newTexto.AppendLine("\t\t\t\t\tThrow new Exception(\"El Parametro " + nameCol + " no puede ser NULO.\")");
                                newTexto.AppendLine("\t\t\t\tEnd If");
                            }

                            if (formulario.chkParametroPa.Checked)
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameParametro + "\")");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                            }
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ")");
                            newTexto.AppendLine("\t\t\t\t");
                            bParametroIngresado = true;
                        }
                    }

                    if (!bParametroIngresado)
                    {
                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                        newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"\")");
                    }
                }

                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t'Llamamos al Procedmiento Almacenado");
                newTexto.AppendLine("\t\t\t\tDim local As " + paramClaseConeccion + " = New " + paramClaseConeccion);
                newTexto.AppendLine("\t\t\t\tDim nombreSp As String = " + paramClaseListadoSp + "." + pNameTabla + ".Sp" + prefijo + "Del.ToString()");
                newTexto.AppendLine("\t\t\t\tReturn local.execStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, localTrans)");
                newTexto.AppendLine("\t\t\tCatch exp As Exception");
                newTexto.AppendLine("\t\t\t\tThrow exp");
                newTexto.AppendLine("\t\t\tEnd Try");
                newTexto.AppendLine("\t\tEnd Function\r\n");
            }
            else
            {
                //DELETE
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <returns>");
                newTexto.AppendLine("\t\t''' \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t''' </returns>");
                newTexto.AppendLine("\t\tPublic Function Delete(ByVal obj As " + pNameClaseEnt + ") As Integer");
                newTexto.AppendLine("\t\t\tThrow new Exception(\"No existe el Procedimiento Almacenado SP" + prefijo + "Del.\")");
                newTexto.AppendLine("\t\tEnd Function\r\n");

                //DELETE TRANS
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <returns>");
                newTexto.AppendLine("\t\t''' \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t''' </returns>");
                newTexto.AppendLine("\t\tPublic Function Delete(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Integer");
                newTexto.AppendLine("\t\t\tThrow new Exception(\"No existe el Procedimiento Almacenado SP" + prefijo + "Del.\")");
                newTexto.AppendLine("\t\tEnd Function\r\n");
            }

            switch (formulario.cmbTipoColumnas.SelectedIndex)
            {
                case 0:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "InsUpd", TipoColumnas.PrimeraMayuscula);
                    break;

                case 1:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "InsUpd", TipoColumnas.Minusculas);
                    break;

                case 2:
                    dtParametros = CTablasColumnas.ObtenerListadoParametrosSP(formulario.txtSPprev.Text + prefijo + formulario.txtSPsig.Text + "InsUpd", TipoColumnas.Mayusculas);
                    break;
            }
            if (dtParametros.Rows.Count > 0)
            {
                //InsUpd
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <returns>");
                newTexto.AppendLine("\t\t''' \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t''' </returns>");
                if (formulario.chkReAlInterface.Checked)
                {
                    newTexto.AppendLine("\t\tPublic Function InsertUpdate(ByVal obj As " + pNameClaseEnt + ") As Boolean Implements " + pNameClaseIn + ".InsertUpdate");
                }
                else
                {
                    newTexto.AppendLine("\t\tPublic Function InsertUpdate(ByVal obj As " + pNameClaseEnt + ") As Boolean");
                }
                newTexto.AppendLine("\t\t\tTry");

                newTexto.AppendLine("\t\t\t\tDim arrNombreParam As ArrayList = New ArrayList");
                newTexto.AppendLine("\t\t\t\tDim arrValoresParam As ArrayList = New ArrayList");

                for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
                {
                    dynamic bParametroIngresado = false;
                    dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                    for (int iColumna = 0; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                    {
                        nameCol = dtColumns.Rows[iColumna][0].ToString();
                        tipoCol = dtColumns.Rows[iColumna][1].ToString();
                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            newTexto.AppendLine("\t\t\t\tIf (IsNothing(obj." + nameCol + ")) Then");
                            newTexto.AppendLine("\t\t\t\t\tThrow new Exception(\"El Parametro " + nameCol + " no puede ser NULO.\")");
                            newTexto.AppendLine("\t\t\t\tEnd If");

                            if (formulario.chkParametroPa.Checked)
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(pa\"" + nameParametro + "\")");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                            }
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ")");
                            newTexto.AppendLine("\t\t\t\t");
                            bParametroIngresado = true;
                        }
                    }

                    if (!bParametroIngresado)
                    {
                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                        newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"\")");
                    }
                }

                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t'Llamamos al Procedmiento Almacenado");
                newTexto.AppendLine("\t\t\t\tDim local As " + paramClaseConeccion + " = New " + paramClaseConeccion);
                newTexto.AppendLine("\t\t\t\tReturn local.execStoreProcedure(" + paramClaseListadoSp + "." + pNameTabla + ".sp" + pNameTabla + "InsUpd, arrNombreParam, arrValoresParam)");
                newTexto.AppendLine("\t\t\tCatch exp As Exception");
                newTexto.AppendLine("\t\t\t\tThrow exp");
                newTexto.AppendLine("\t\t\tEnd Try");
                newTexto.AppendLine("\t\tEnd Function\r\n");

                //InsUpd TRANS
                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' \tFuncion que inserta un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se van a insertar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <returns>");
                newTexto.AppendLine("\t\t''' \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t''' </returns>");
                if (formulario.chkReAlInterface.Checked)
                {
                    newTexto.AppendLine("\t\tPublic Function InsertUpdate(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Boolean Implements " + pNameClaseIn + ".InsertUpdate");
                }
                else
                {
                    newTexto.AppendLine("\t\tPublic Function InsertUpdate(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Boolean");
                }
                newTexto.AppendLine("\t\t\tTry");

                newTexto.AppendLine("\t\t\t\tDim arrNombreParam As ArrayList = New ArrayList");
                newTexto.AppendLine("\t\t\t\tDim arrValoresParam As ArrayList = New ArrayList");

                for (int iParametro = 0; iParametro <= dtParametros.Rows.Count - 1; iParametro++)
                {
                    dynamic bParametroIngresado = false;
                    dynamic nameParametro = dtParametros.Rows[iParametro]["ColName"].ToString().Replace("@", "");

                    for (int iColumna = 0; iColumna <= dtColumns.Rows.Count - 1; iColumna++)
                    {
                        nameCol = dtColumns.Rows[iColumna][0].ToString();
                        tipoCol = dtColumns.Rows[iColumna][1].ToString();
                        if (nameCol.ToUpper() == nameParametro.ToUpper())
                        {
                            newTexto.AppendLine("\t\t\t\tIf (IsNothing(obj." + nameCol + ")) Then");
                            newTexto.AppendLine("\t\t\t\t\tThrow new Exception(\"El Parametro " + nameCol + " no puede ser NULO.\")");
                            newTexto.AppendLine("\t\t\t\tEnd If");
                            newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");

                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ")");
                            newTexto.AppendLine("\t\t\t\t");
                            bParametroIngresado = true;
                        }
                    }

                    if (!bParametroIngresado)
                    {
                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameParametro + "\")");
                        newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"\")");
                    }
                }

                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\t'Llamamos al Procedmiento Almacenado");
                newTexto.AppendLine("\t\t\t\tDim local As " + paramClaseConeccion + " = New " + paramClaseConeccion);
                newTexto.AppendLine("\t\t\t\tReturn local.execStoreProcedure(" + paramClaseListadoSp + "." + pNameTabla + ".sp" + pNameTabla + "InsUpd, arrNombreParam, arrValoresParam, localTrans)");
                newTexto.AppendLine("\t\t\tCatch exp As Exception");
                newTexto.AppendLine("\t\t\t\tThrow exp");
                newTexto.AppendLine("\t\t\tEnd Try");
                newTexto.AppendLine("\t\tEnd Function\r\n");
            }
            else
            {
                //Hacemos el  InsUpd con los procedimientos existentes

                newTexto.AppendLine("\t\t''' <summary>");
                newTexto.AppendLine("\t\t''' \tFuncion que inserta o actualiza un registro un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                newTexto.AppendLine("\t\t''' </summary>");
                newTexto.AppendLine("\t\t''' <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                newTexto.AppendLine("\t\t'''     <para>");
                newTexto.AppendLine("\t\t''' \t\t Clase desde la que se van a insertar o actualizar los valores a la tabla " + pNameTabla);
                newTexto.AppendLine("\t\t'''     </para>");
                newTexto.AppendLine("\t\t''' </param>");
                newTexto.AppendLine("\t\t''' <returns>");
                newTexto.AppendLine("\t\t''' \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                newTexto.AppendLine("\t\t''' </returns>");
                if (formulario.chkReAlInterface.Checked)
                {
                    newTexto.AppendLine("\t\tPublic Function InsertUpdate(ByVal obj As " + pNameClaseEnt + ") As Integer Implements " + pNameClaseIn + ".InsertUpdate");
                }
                else
                {
                    newTexto.AppendLine("\t\tPublic Function InsertUpdate(ByVal obj As " + pNameClaseEnt + ") As Integer");
                }
                newTexto.AppendLine("\t\t\tTry");

                newTexto.AppendLine("\t\t\t\tDim esInsertar As Boolean = True");
                newTexto.AppendLine("\t\t\t\t");

                for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                {
                    if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                    {
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        newTexto.AppendLine("\t\t\t\t\tesInsertar = (esInsertar AndAlso IsNothing(obj." + nameCol + "))");
                    }
                }
                newTexto.AppendLine("\t\t\t\t");
                newTexto.AppendLine("\t\t\t\tIf (esInsertar) Then");
                if ((formulario.chkUsarSpsIdentity.Checked))
                {
                    newTexto.AppendLine("\t\t\t\t\tReturn IIf(Insert(obj), 1, 0)");
                }
                else
                {
                    newTexto.AppendLine("\t\t\t\t\tReturn IIf(Insert(obj), 1, 0)");
                }
                newTexto.AppendLine("\t\t\t\tElse");
                newTexto.AppendLine("\t\t\t\t\tReturn Update(obj)");
                newTexto.AppendLine("\t\t\t\tEnd If");
                newTexto.AppendLine("\t\t\tCatch exp As Exception");
                newTexto.AppendLine("\t\t\t\tThrow exp");
                newTexto.AppendLine("\t\t\tEnd Try");
                newTexto.AppendLine("\t\tEnd Function\r\n");

                if (miEntorno != TipoEntorno.CSharp_WCF)
                {
                    //INSUPD con TRANS
                    newTexto.AppendLine("\t\t''' <summary>");
                    newTexto.AppendLine("\t\t''' \tFuncion que inserta o actualiza un registro un nuevo registro en la tabla " + pNameTabla + " a partir de una clase del tipo E" + pNameTabla);
                    newTexto.AppendLine("\t\t''' </summary>");
                    newTexto.AppendLine("\t\t''' <param name=\"obj\" type=\"" + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + "\">");
                    newTexto.AppendLine("\t\t'''     <para>");
                    newTexto.AppendLine("\t\t''' \t\t Clase desde la que se van a insertar o actualizar los valores a la tabla " + pNameTabla);
                    newTexto.AppendLine("\t\t'''     </para>");
                    newTexto.AppendLine("\t\t''' </param>");
                    newTexto.AppendLine("\t\t''' <param name=\"localTrans\" type=\"" + (miEntorno == TipoEntorno.CSharp_WinForms ? "Clases" : "App_Class") + ".Conexion." + paramClaseTransaccion + "\">");
                    newTexto.AppendLine("\t\t'''     <para>");
                    newTexto.AppendLine("\t\t''' \t\t Clase desde la que se va a utilizar una transaccion " + pNameTabla);
                    newTexto.AppendLine("\t\t'''     </para>");
                    newTexto.AppendLine("\t\t''' </param>");
                    newTexto.AppendLine("\t\t''' <returns>");
                    newTexto.AppendLine("\t\t''' \t Valor TRUE or FALSE que indica el exito de la operacion" + pNameTabla);
                    newTexto.AppendLine("\t\t''' </returns>");
                    if (formulario.chkReAlInterface.Checked)
                    {
                        newTexto.AppendLine("\t\tPublic Function InsertUpdate(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Integer Implements " + pNameClaseIn + ".InsertUpdate");
                    }
                    else
                    {
                        newTexto.AppendLine("\t\tPublic Function InsertUpdate(ByVal obj As " + pNameClaseEnt + ", ByRef localTrans As " + paramClaseTransaccion + ") As Integer");
                    }
                    newTexto.AppendLine("\t\t\tTry");
                    newTexto.AppendLine("\t\t\t\tDim esInsertar As Boolean = True");
                    newTexto.AppendLine("\t\t\t\t");

                    for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                    {
                        if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                        {
                            nameCol = dtColumns.Rows[iCol][0].ToString();
                            newTexto.AppendLine("\t\t\t\t\tesInsertar = (esInsertar AndAlso IsNothing(obj." + nameCol + "))");
                        }
                    }
                    newTexto.AppendLine("\t\t\t\t");
                    newTexto.AppendLine("\t\t\t\tIf (esInsertar) Then");
                    if (formulario.chkUsarSpsIdentity.Checked)
                    {
                        newTexto.AppendLine("\t\t\t\t\tReturn IIf(Insert(obj, localTrans), 1, 0)");
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t\t\t\tReturn IIf(Insert(obj, localTrans), 1, 0)");
                    }
                    newTexto.AppendLine("\t\t\t\tElse");
                    newTexto.AppendLine("\t\t\t\t\treturn Update(obj, localTrans)");
                    newTexto.AppendLine("\t\t\t\tEnd If");
                    newTexto.AppendLine("\t\t\tCatch exp As Exception");
                    newTexto.AppendLine("\t\t\t\tThrow exp");
                    newTexto.AppendLine("\t\t\tEnd Try");
                    newTexto.AppendLine("\t\tEnd Function\r\n");
                }
            }
            return newTexto.ToString();
        }

        public void CrearEnumApi(ref FPrincipal formulario)
        {
            DataTable dt = new DataTable();
            bool bPrimerItem = true;
            string paramClaseApi = (string.IsNullOrEmpty(formulario.txtClaseParametrosClaseApi.Text) ? "cApi" : formulario.txtClaseParametrosClaseApi.Text);

            StringBuilder newTexto = new StringBuilder();
            newTexto.AppendLine("namespace " + formulario.txtInfProyectoDal.Text);
            newTexto.AppendLine("{");
            newTexto.AppendLine("\tpublic static class " + paramClaseApi);
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tpublic enum Estado");
            newTexto.AppendLine("\t\t{");
            switch (Principal.DBMS)
            {
                case TipoBD.SQLServer:
                    dt = CFuncionesBDs.CargarDataTable("SELECT DISTINCT se.EstadoSegEstados FROM SegEstados se ORDER BY se.EstadoSegEstados");
                    break;

                case TipoBD.SQLServer2000:
                    dt = CFuncionesBDs.CargarDataTable("SELECT DISTINCT se.EstadoSegEstados FROM SegEstados se ORDER BY se.EstadoSegEstados");
                    break;

                case TipoBD.PostgreSQL:
                    dt = CFuncionesBDs.CargarDataTable("SELECT DISTINCT se.EstadoSes FROM SegEstados se ORDER BY se.EstadoSes");
                    break;

                case TipoBD.Oracle:
                    dt = CFuncionesBDs.CargarDataTable("SELECT DISTINCT se.EstadoSes FROM SegEstados se ORDER BY se.EstadoSes");
                    break;
            }

            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                if (bPrimerItem)
                {
                    newTexto.AppendLine("\t\t\t" + dt.Rows[i][0].ToString());
                    bPrimerItem = false;
                }
                else
                {
                    newTexto.AppendLine("\t\t\t," + dt.Rows[i][0].ToString());
                }
            }
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("");
            newTexto.AppendLine("\t\tpublic enum Transaccion");
            newTexto.AppendLine("\t\t{");
            switch (Principal.DBMS)
            {
                case TipoBD.SQLServer:
                    dt = CFuncionesBDs.CargarDataTable("SELECT DISTINCT se.EstadoSegEstados FROM SegEstados se ORDER BY se.EstadoSegEstados");
                    break;

                case TipoBD.SQLServer2000:
                    dt = CFuncionesBDs.CargarDataTable("SELECT DISTINCT se.EstadoSegEstados FROM SegEstados se ORDER BY se.EstadoSegEstados");
                    break;

                case TipoBD.PostgreSQL:
                    dt = CFuncionesBDs.CargarDataTable("SELECT DISTINCT st.transaccionstr  FROM segtransacciones st ORDER BY st.transaccionstr");
                    break;

                case TipoBD.Oracle:
                    dt = CFuncionesBDs.CargarDataTable("SELECT DISTINCT st.transaccionstr  FROM segtransacciones st ORDER BY st.transaccionstr");
                    break;
            }

            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                if (bPrimerItem)
                {
                    newTexto.AppendLine("\t\t\t" + dt.Rows[i][0].ToString());
                    bPrimerItem = false;
                }
                else
                {
                    newTexto.AppendLine("\t\t\t," + dt.Rows[i][0].ToString());
                }
            }
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("}");

            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoDal.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoDal.Text;
            }

            if (!Directory.Exists(PathDesktop))
            {
                Directory.CreateDirectory(PathDesktop);
            }
            CFunciones.CrearArchivo(newTexto.ToString(), paramClaseApi + ".cs", PathDesktop + "\\");
        }

        public void CrearEnumApiVB(ref FPrincipal formulario)
        {
            DataTable dt = new DataTable();
            string paramClaseApi = (string.IsNullOrEmpty(formulario.txtClaseParametrosClaseApi.Text) ? "cApi" : formulario.txtClaseParametrosClaseApi.Text);
            StringBuilder strResultado = new StringBuilder();

            strResultado.AppendLine("Public Class " + paramClaseApi);
            dt = CFuncionesBDs.CargarDataTable("SELECT DISTINCT se.EstadoSegEstados FROM SegEstados se ORDER BY se.EstadoSegEstados");
            strResultado.AppendLine("\tPublic Enum Estado");
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                strResultado.AppendLine("\t\t," + dt.Rows[i][0].ToString());
            }
            strResultado.AppendLine("\tEnd Enum");
            strResultado.AppendLine("");

            dt = CFuncionesBDs.CargarDataTable("SELECT DISTINCT se.EstadoSegEstados FROM SegEstados se ORDER BY se.EstadoSegEstados");
            strResultado.AppendLine("\tPublic Enum Transaccion");
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                strResultado.AppendLine("\t\t," + dt.Rows[i][0].ToString());
            }
            strResultado.AppendLine("\tEnd Enum");
            strResultado.AppendLine("End Class");

            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            if (!Directory.Exists(PathDesktop))
            {
                Directory.CreateDirectory(PathDesktop);
            }
            CFunciones.CrearArchivo(strResultado.ToString(), paramClaseApi + ".vb", PathDesktop + "\\");
        }

        public void CrearEnumSP(ref FPrincipal formulario)
        {
            string paramClaseListadoSp = (string.IsNullOrEmpty(formulario.txtClaseParametrosListadoSp.Text) ? "cListadoSp" : formulario.txtClaseParametrosListadoSp.Text);
            //Elegimos el origen : Tablas o Vistas
            ReAlListView MyReAlListView = new ReAlListView();
            switch (formulario.tabBDObjects.SelectedIndex)
            {
                case 0:
                    MyReAlListView = formulario.ReAlListView1;
                    break;

                case 1:
                    MyReAlListView = formulario.ReAlListView2;
                    break;

                case 2:
                    throw new Exception("No se puede Crear Entidades de los SPs");
            }

            string strListadoSP = "namespace " + formulario.txtInfProyectoDal.Text + "\r\n{\r\n\tpublic class " + paramClaseListadoSp + "\r\n\t{\r\n";

            for (int i = 0; i <= MyReAlListView.Items.Count - 1; i++)
            {
                if (MyReAlListView.Items[i].Checked == true)
                {
                    string idTabla = MyReAlListView.Items[i].SubItems[1].Text;
                    string pNameTabla = MyReAlListView.Items[i].SubItems[0].Text;
                    string pNameClase = pNameTabla.Replace("_", "").Substring(0, 1).ToUpper() + pNameTabla.Replace("_", "").Substring(1, 2).ToLower() + pNameTabla.Replace("_", "").Substring(3, 1).ToUpper() + pNameTabla.Replace("_", "").Substring(4).ToLower();
                    strListadoSP = strListadoSP + CrearListadoSPs(Languaje.CSharp, pNameTabla, pNameClase, formulario.txtSPprev.Text, formulario.txtSPsig.Text);
                }
            }

            strListadoSP = strListadoSP + "\t}\r\n}\r\n";

            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoDal.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoDal.Text;
            }

            if (!Directory.Exists(PathDesktop))
            {
                Directory.CreateDirectory(PathDesktop);
            }
            CFunciones.CrearArchivo(strListadoSP, "" + paramClaseListadoSp + ".cs", PathDesktop + "\\");
        }

        public void CrearEnumSPVB(ref FPrincipal formulario)
        {
            string paramClaseListadoSp = (string.IsNullOrEmpty(formulario.txtClaseParametrosListadoSp.Text) ? "cListadoSp" : formulario.txtClaseParametrosListadoSp.Text);
            //Elegimos el origen : Tablas o Vistas
            ReAlListView MyReAlListView = new ReAlListView();
            switch (formulario.tabBDObjects.SelectedIndex)
            {
                case 0:
                    MyReAlListView = formulario.ReAlListView1;
                    break;

                case 1:
                    MyReAlListView = formulario.ReAlListView2;
                    break;

                case 2:
                    throw new Exception("No se puede Crear Entidades de los SPs");
            }

            string strListadoSP = "\tPublic Class " + paramClaseListadoSp + "\r\n";

            for (int i = 0; i <= MyReAlListView.Items.Count - 1; i++)
            {
                if (MyReAlListView.Items[i].Checked == true)
                {
                    string idTabla = MyReAlListView.Items[i].SubItems[1].Text;
                    string pNameTabla = MyReAlListView.Items[i].SubItems[0].Text;
                    string pNameClase = pNameTabla.Replace("_", "").Substring(0, 1).ToUpper() + pNameTabla.Replace("_", "").Substring(1, 2).ToLower() + pNameTabla.Replace("_", "").Substring(3, 1).ToUpper() + pNameTabla.Replace("_", "").Substring(4).ToLower();
                    strListadoSP = strListadoSP + CrearListadoSPs(Languaje.VbNet, pNameTabla, pNameClase, formulario.txtSPprev.Text, formulario.txtSPsig.Text);
                }
            }

            strListadoSP = strListadoSP + "\tEnd Class\r\n";

            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            if (!Directory.Exists(PathDesktop))
            {
                Directory.CreateDirectory(PathDesktop);
            }
            CFunciones.CrearArchivo(strListadoSP, "" + paramClaseListadoSp + ".vb", PathDesktop + "\\");
        }

        private string CrearListadoSPs(Languaje lang, string pNameTabla, string pNameClase, string strSPprev, string strSPsig)
        {
            //Obtenemos el nombre del SP en base al prefijo
            string prefijo = pNameTabla;
            try
            {
                DataTable dtPrefijo = new DataTable();

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.SQLServer2000:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + pNameTabla + "')");
                        break;

                    case TipoBD.Oracle:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT UPPER(SUBSTR(ALIASSTA,1,1)) || LOWER(SUBSTR(ALIASSTA,2,LENGTH(ALIASSTA)-1)) FROM SEGTABLAS WHERE UPPER(TABLASTA) = UPPER('" + pNameTabla.ToUpper() + "')");
                        break;

                    case TipoBD.PostgreSQL:
                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.aliassta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" + pNameTabla + "')");
                        if (dtPrefijo.Rows.Count == 0)
                        {
                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.alias FROM seg_tablas s WHERE UPPER(s.nombretabla) = UPPER('" + pNameTabla + "')");
                        }
                        break;
                }

                if (dtPrefijo.Rows.Count > 0)
                {
                    prefijo = dtPrefijo.Rows[0][0].ToString().ToString();
                    prefijo = prefijo.Substring(0, 1).ToUpper() + prefijo.Substring(1, prefijo.Length - 1);
                }
                else
                {
                    try
                    {
                        switch (Principal.DBMS)
                        {
                            case TipoBD.SQLServer:
                                dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE UPPER(TablaSegTablas) = UPPER('" + pNameTabla.ToUpper() + "')");
                                break;

                            case TipoBD.SQLServer2000:
                                dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE UPPER(TablaSegTablas) = UPPER('" + pNameTabla.ToUpper() + "')");
                                break;

                            case TipoBD.Oracle:
                                dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT UPPER(SUBSTR(AliasSegTablas,1,1)) || LOWER(SUBSTR(AliasSegTablas,2,LENGTH(AliasSegTablas)-1)) FROM SEGTABLAS WHERE UPPER(TablaSegTablas) = '" + pNameTabla.ToUpper() + "'");
                                break;

                            case TipoBD.PostgreSQL:
                                dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.AliasSegTablas FROM segtablas s WHERE UPPER(s.TablaSegTablas) = UPPER('" + pNameTabla.ToUpper() + "')");
                                if (dtPrefijo.Rows.Count == 0)
                                {
                                    dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.AliasSegTablas FROM seg_tablas s WHERE UPPER(s.TablaSegTablas) = UPPER('" + pNameTabla + "')");
                                }
                                break;
                        }

                        if (dtPrefijo.Rows.Count > 0)
                        {
                            prefijo = dtPrefijo.Rows[0][0].ToString().ToString();
                            prefijo = prefijo.Substring(0, 1).ToUpper() + prefijo.Substring(1, prefijo.Length - 1);
                        }
                        else
                        {
                            prefijo = pNameTabla;
                        }
                    }
                    catch (Exception ex2)
                    {
                        prefijo = pNameTabla;
                    }
                }
            }
            catch (Exception ex)
            {
                prefijo = pNameTabla;
            }

            //Creamos un listado de los SP
            if (lang == Languaje.CSharp)
            {
                return "\t\tpublic enum " + pNameClase + "\r\n\t\t{\r\n\t" +
                       "\t\t/// <summary>\r\n\t\t\t" +
                       "///     Proc para Insertar datos en la tabla " + pNameTabla + "\r\n\t\t\t" +
                       "/// </summary>\r\n\t\t\t" + strSPprev + prefijo + strSPsig + "Ins," +
                       "\r\n\t\t\t\r\n\t\t\t/// <summary>\r\n" +
                       "\t\t\t///     Proc para Actualizar datos en la tabla " + pNameTabla + "\r\n" +
                       "\t\t\t/// </summary>\r\n\t\t\t" + strSPprev + prefijo +
                       strSPsig + "Upd,\r\n\t\t\t\r\n\t\t\t" +
                       "/// <summary>\r\n\t\t\t///     Proc para Eliminar datos en la tabla " +
                       pNameTabla + "\r\n\t\t\t/// </summary>\r\n\t\t\t" +
                       strSPprev + prefijo + strSPsig + "Del\r\n\t\t}\r\n\r\n";
            }

            if (lang == Languaje.VbNet)
            {
                return "\t\tPublic Enum " + pNameClase + "\r\n\t\t\t''' <summary>" +
                       "\r\n\t\t\t'''     Proc para Insertar datos en la tabla " + pNameTabla +
                       "\r\n\t\t\t''' </summary>\r\n\t\t\tSp" + prefijo +
                       "Ins\r\n\t\t\t\r\n\t\t\t''' <summary>" +
                       "\r\n\t\t\t'''     Proc para Actualizar datos en la tabla " + pNameTabla +
                       "\r\n\t\t\t''' </summary>\r\n\t\t\tSp" + prefijo +
                       "Upd\r\n\t\t\t\r\n\t\t\t''' <summary>" +
                       "\r\n\t\t\t'''     Proc para Eliminar datos en la tabla " + pNameTabla +
                       "\r\n\t\t\t''' </summary>\r\n\t\t\tSp" + prefijo +
                       "Del\r\n\t\t\t\r\n\t\t\t''' <summary>" +
                       "\r\n\t\t\t'''     Proc para Insertar y/o Actualizar datos en la tabla " +
                       pNameTabla + "\r\n\t\t\t''' </summary>\r\n\t\t\t" +
                       "sp" + prefijo + "InsUpd\r\n\t\tEnd Enum\r\n\r\n";
            }
            return "";
        }

        #endregion Methods
    }
}