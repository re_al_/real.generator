﻿using ReAl.Generator.App.AppCore;
using System;
using System.Data;
using System.IO;

namespace ReAl.Generator.App.AppGenerators
{
    public class cReglaNegociosAngular
    {
        public string CrearServicesAngular(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyecto.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyecto.Text;
            }
            if (Directory.Exists(PathDesktop + "\\app\\services") == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\app\\services");
            }

            string pNameTabla = pNameClase.Replace("_", "").ToLower();
            pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() +
                         pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();

            dynamic pNameClaseRn = pNameTabla + "Service";

            //Empezamos con el archivo
            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            newTexto.AppendLine("import { Observable } from 'rxjs/Rx';");
            newTexto.AppendLine("import { Http, Headers, Response, RequestOptions } from '@angular/http';");
            newTexto.AppendLine("import { Injectable } from '@angular/core';");
            newTexto.AppendLine("import { " + pNameTabla + " } from '../entidades/" + pNameTabla.ToLower() + "';");
            newTexto.AppendLine("import { AppConst } from '../app.const'");
            newTexto.AppendLine("");
            newTexto.AppendLine("@Injectable()");
            newTexto.AppendLine("export class " + pNameClaseRn + " {");
            newTexto.AppendLine("\tprivate _url:string = AppConst.SERVICE_URL + '/" + pNameTabla + "/';");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tconstructor(private _http:Http) { }");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tgetAll(): Observable<ClaUnidadesmedidas[]>{");
            newTexto.AppendLine("\t\tlet headers = new Headers({ 'Content-Type': 'application/json' });");
            newTexto.AppendLine("\t\theaders.append('Accept', 'application/json');");
            newTexto.AppendLine("\t\tlet options = new RequestOptions({ headers: headers });");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn this._http.get(this._url)");
            newTexto.AppendLine("\t\t\t\t.map((res:Response) => res.json())");
            newTexto.AppendLine("\t\t\t\t.catch((error:any) => Observable.throw(error.json().error || 'Server error'));");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), pNameClaseRn.Replace("Service", ".service").ToLower() + ".ts", PathDesktop + "\\app\\services\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\app\\services\\" + pNameClaseRn.Replace("Service", ".service").ToLower() + ".ts";
        }
    }
}