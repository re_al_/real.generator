﻿using ReAl.Generator.App.AppCore;
using System;
using System.Collections;
using System.Data;
using System.IO;

namespace ReAl.Generator.App.AppGenerators
{
    public class cNetCoreWebAPI
    {
        public string CrearNetCoreWebApiCs(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string pNameTabla = pNameClase.Replace("_", "");
            pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() +
                         pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();

            pNameClase = pNameTabla + "Controller";

            string strNombreArchivo = pNameTabla + "Controller";

            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameTabla;
            string pNameClaseRn = formulario.txtPrefijoModelo.Text + pNameTabla;

            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyecto.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyecto.Text;
            }

            //Encontramos la primera columna
            ArrayList llavePrimaria = new ArrayList();
            string strLlavePrimaria = "";
            string strLlavePrimariaObtenerObjeto = "";
            dynamic bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString();

                    if (bEsPrimerElemento)
                    {
                        strLlavePrimaria = strLlavePrimaria + tipoCol + " " + tipoCol + nameCol;
                        strLlavePrimariaObtenerObjeto = strLlavePrimariaObtenerObjeto + tipoCol + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlavePrimaria = strLlavePrimaria + ", " + tipoCol + " " + tipoCol + nameCol;
                        strLlavePrimariaObtenerObjeto = strLlavePrimariaObtenerObjeto + ", " + tipoCol + nameCol;
                    }
                    llavePrimaria.Add(tipoCol + nameCol);
                }
            }
            if (string.IsNullOrEmpty(strLlavePrimaria))
            {
                strLlavePrimaria = dtColumns.Rows[0][1] + " " + dtColumns.Rows[0][1] + dtColumns.Rows[0][0];
                llavePrimaria.Add(dtColumns.Rows[0][1] + dtColumns.Rows[0][0].ToString());
            }

            if (dtColumns.Rows.Count == 0)
                return "";
            if (dtColumns.Columns.Count == 0)
                return "";

            if (Directory.Exists(PathDesktop + "\\Controllers") == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\Controllers");
            }

            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase);
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que crea el Web API para la tabla  " + pNameTabla);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();

            newTexto.AppendLine(CabeceraTmp);

            newTexto.AppendLine("");
            newTexto.AppendLine("#region");
            newTexto.AppendLine("using System;");
            newTexto.AppendLine("using System.Collections.Generic;");
            newTexto.AppendLine("using System.Linq;");
            newTexto.AppendLine("using Microsoft.AspNetCore.Http;");
            newTexto.AppendLine("using Microsoft.AspNetCore.Mvc;");
            newTexto.AppendLine("using " + formulario.txtInfProyectoDal.Text + "; ");
            newTexto.AppendLine("using " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceEntidades.Text + ";");
            newTexto.AppendLine("using " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceNegocios.Text + ";");
            newTexto.AppendLine("#endregion");
            newTexto.AppendLine("");

            newTexto.AppendLine("namespace " + formulario.txtInfProyecto.Text + ".Controllers");
            newTexto.AppendLine("{");
            newTexto.AppendLine("\t/// <summary>");
            newTexto.AppendLine("\t/// KB: https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-web-api");
            newTexto.AppendLine("\t/// </summary>");
            newTexto.AppendLine("\t[Route(\"api/[controller]/[action]\")]");
            newTexto.AppendLine("\tpublic class " + pNameClase + " : Controller");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// GET api/" + pNameTabla + "/Get");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <returns></returns>");
            newTexto.AppendLine("\t\t[HttpGet]");
            newTexto.AppendLine("\t\tpublic IEnumerable<" + pNameClaseEnt + "> Get()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar rn = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\treturn rn.ObtenerLista().AsEnumerable();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// GET /api/" + pNameTabla + "/Get/{id}");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"id\" ></param>");
            newTexto.AppendLine("\t\t/// <returns></returns>");
            newTexto.AppendLine("\t\t[HttpGet(\"{id}\", Name = \"Get" + pNameClase + "\")]");
            newTexto.AppendLine("\t\t[ProducesResponseType(StatusCodes.Status200OK)]");
            newTexto.AppendLine("\t\tpublic IActionResult Get(int id)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar rn = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\tvar objPag = rn.ObtenerObjeto(id);");
            newTexto.AppendLine("\t\t\tif (objPag == null) return NotFound($\"Objeto EntFdgDocumentos={id} no encontrado\");");
            newTexto.AppendLine("\t\t\treturn Ok(objPag);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// POST /api/" + pNameTabla + "/Create");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"item\" ></param>");
            newTexto.AppendLine("\t\t/// <returns></returns>");
            newTexto.AppendLine("\t\t[HttpPost]");
            newTexto.AppendLine("\t\t[ProducesResponseType(StatusCodes.Status200OK)]");
            newTexto.AppendLine("\t\t[ProducesResponseType(StatusCodes.Status201Created)]");
            newTexto.AppendLine("\t\t[ProducesResponseType(StatusCodes.Status400BadRequest)]");
            newTexto.AppendLine("\t\tpublic IActionResult Create([FromBody] " + pNameClaseEnt + " item)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (item == null ) return BadRequest(\"Objeto EntFdgDocumentos no proporcionado\");");
            newTexto.AppendLine("\t\t\tif (!ModelState.IsValid) return BadRequest(ModelState.Values);");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tvar rn = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\t\t//item.feccre = DateTime.Now;");
            newTexto.AppendLine("\t\t\t\trn.Insert(item);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tint intId = rn.FuncionesMax(" + pNameClaseEnt + ".Fields.id_);");
            newTexto.AppendLine("\t\t\t\tvar objInsertado = rn.ObtenerObjeto(intId);");
            newTexto.AppendLine("\t\t\t\treturn CreatedAtRoute(\"Get" + pNameClase + "\", new { id = objInsertado.id_ }, objInsertado);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception e)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\treturn BadRequest(e.Message);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// PUT /api/" + pNameTabla + "/Update/{id}");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"id\"></param>");
            newTexto.AppendLine("\t\t/// <param name=\"item\"></param>");
            newTexto.AppendLine("\t\t/// <returns></returns>");
            newTexto.AppendLine("\t\t[HttpPut(\"{id}\")]");
            newTexto.AppendLine("\t\t[ProducesResponseType(StatusCodes.Status200OK)]");
            newTexto.AppendLine("\t\t[ProducesResponseType(StatusCodes.Status404NotFound)]");
            newTexto.AppendLine("\t\t[ProducesResponseType(StatusCodes.Status400BadRequest)]");
            newTexto.AppendLine("\t\tpublic IActionResult Update(int id, [FromBody] " + pNameClaseEnt + " item)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (item == null ) return BadRequest(\"Objeto " + pNameClaseEnt + " no proporcionado\");");
            newTexto.AppendLine("\t\t\tif (item.id_ != id) return BadRequest($\"Elemento " + pNameClaseEnt + ".id={item.id_} diferente a {id}\");");
            newTexto.AppendLine("\t\t\tif (!ModelState.IsValid) return BadRequest(ModelState.Values);");
            newTexto.AppendLine("\t\t\t");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tvar rn = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\t\tvar obj = rn.ObtenerObjeto(id);");
            newTexto.AppendLine("\t\t\t\tif (obj == null) return NotFound($\"Objeto " + pNameClaseEnt + "={id} no encontrado\");");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t//item.fecmod = DateTime.Now;");
            newTexto.AppendLine("\t\t\t\trn.Update(item);");
            newTexto.AppendLine("\t\t\t\treturn Ok($\"Objeto " + pNameClaseEnt + "={id} actualizado\");");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception e)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\treturn BadRequest(e.Message);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// PUT /api/" + pNameTabla + "/UpdateTransaccion/{apitransaccion}/{id}");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"apitransaccion\"></param>");
            newTexto.AppendLine("\t\t/// <param name=\"id\"></param>");
            newTexto.AppendLine("\t\t/// <param name=\"usumod\"></param>");
            newTexto.AppendLine("\t\t/// <returns></returns>");
            newTexto.AppendLine("\t\t[HttpPut(\"{apitransaccion}/{id}\", Name = \"UpdateTransaccion\")]");
            newTexto.AppendLine("\t\t[ProducesResponseType(StatusCodes.Status200OK)]");
            newTexto.AppendLine("\t\t[ProducesResponseType(StatusCodes.Status404NotFound)]");
            newTexto.AppendLine("\t\t[ProducesResponseType(StatusCodes.Status400BadRequest)]");
            newTexto.AppendLine("\t\tpublic IActionResult UpdateTransaccion(string apitransaccion, int id, [FromBody] string usumod)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tvar rn = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\t\tvar obj = rn.ObtenerObjeto(id);");
            newTexto.AppendLine("\t\t\t\tif (obj == null) return NotFound($\"Objeto " + pNameClaseEnt + "={id} no encontrado\");");
            newTexto.AppendLine("\t\t\t\t//obj.apitransaccion = apitransaccion.ToUpper();");
            newTexto.AppendLine("\t\t\t\t//item.fecmod = DateTime.Now;");
            newTexto.AppendLine("\t\t\t\t//item.usumod = usumod;");
            newTexto.AppendLine("\t\t\t\trn.Update(item);");
            newTexto.AppendLine("\t\t\t\treturn Ok($\"Objeto " + pNameClaseEnt + "={id} actualizado\");");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception e)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\treturn BadRequest(e.Message);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// PUT /api/" + pNameTabla + "/UpdateNull/{apitransaccion}/{id}");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"apitransaccion\"></param>");
            newTexto.AppendLine("\t\t/// <param name=\"id\"></param>");
            newTexto.AppendLine("\t\t/// <returns></returns>");
            newTexto.AppendLine("\t\t[HttpPut(\"{apitransaccion}/{id}\", Name = \"UpdateTransaccion\")]");
            newTexto.AppendLine("\t\t[ProducesResponseType(StatusCodes.Status200OK)]");
            newTexto.AppendLine("\t\t[ProducesResponseType(StatusCodes.Status404NotFound)]");
            newTexto.AppendLine("\t\t[ProducesResponseType(StatusCodes.Status400BadRequest)]");
            newTexto.AppendLine("\t\tpublic IActionResult UpdateNull(string apitransaccion, int id, [FromBody] string[] campos)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tvar rn = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\t\tvar obj = rn.ObtenerObjeto(id);");
            newTexto.AppendLine("\t\t\t\tif (obj == null) return NotFound($\"Objeto " + pNameClaseEnt + "={id} no encontrado\");");
            newTexto.AppendLine("\t\t\t\tforeach (var campo in campos)");
            newTexto.AppendLine("\t\t\t\t{");
            newTexto.AppendLine("\t\t\t\t\trn.SetDato(ref obj, campo, null);");
            newTexto.AppendLine("\t\t\t\t}");
            newTexto.AppendLine("\t\t\t\t//obj.apitransaccion = apitransaccion;");
            newTexto.AppendLine("\t\t\t\t//item.fecmod = DateTime.Now;");
            newTexto.AppendLine("\t\t\t\trn.Update(item);");
            newTexto.AppendLine("\t\t\t\treturn Ok($\"Objeto " + pNameClaseEnt + "={id} actualizado\");");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception e)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\treturn BadRequest(e.Message);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t/// <summary>");
            newTexto.AppendLine("\t\t/// DELETE /api/" + pNameTabla + "/Delete/{id}");
            newTexto.AppendLine("\t\t/// </summary>");
            newTexto.AppendLine("\t\t/// <param name=\"id\" ></param>");
            newTexto.AppendLine("\t\t/// <returns></returns>");
            newTexto.AppendLine("\t\t[HttpDelete(\"{id}\")]");
            newTexto.AppendLine("\t\t[ProducesResponseType(StatusCodes.Status200OK)]");
            newTexto.AppendLine("\t\t[ProducesResponseType(StatusCodes.Status404NotFound)]");
            newTexto.AppendLine("\t\tpublic IActionResult Delete(int id)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\ttry");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\tvar rn = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\t\tvar obj = rn.ObtenerObjeto(id);");
            newTexto.AppendLine("\t\t\t\tif (obj == null) return NotFound($\"Objeto " + pNameClaseEnt + "={id} no encontrado\");");
            newTexto.AppendLine("\t\t\t\trn.Delete(obj);");
            newTexto.AppendLine("\t\t\t\treturn Ok($\"Objeto EntFdgDocumentos={id} eliminado\");");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tcatch (Exception e)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\treturn BadRequest(e.Message);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t}");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), strNombreArchivo + ".cs", PathDesktop + "\\Controllers\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\Controllers\\" + strNombreArchivo + ".cs";
        }

        public string CrearNetCoreWebApiClientCs(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string pNameTabla = pNameClase.Replace("_", "");
            pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() +
                         pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();

            pNameClase = "Cli" + pNameTabla;

            string strNombreArchivo = "Cli" + pNameTabla;

            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameTabla;
            string pNameClaseRn = formulario.txtPrefijoModelo.Text + pNameTabla;

            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyecto.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyecto.Text;
            }

            //Encontramos la primera columna
            ArrayList llavePrimaria = new ArrayList();
            string strLlavePrimaria = "";
            string strLlavePrimariaObtenerObjeto = "";
            dynamic bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString();

                    if (bEsPrimerElemento)
                    {
                        strLlavePrimaria = strLlavePrimaria + tipoCol + " " + tipoCol + nameCol;
                        strLlavePrimariaObtenerObjeto = strLlavePrimariaObtenerObjeto + tipoCol + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlavePrimaria = strLlavePrimaria + ", " + tipoCol + " " + tipoCol + nameCol;
                        strLlavePrimariaObtenerObjeto = strLlavePrimariaObtenerObjeto + ", " + tipoCol + nameCol;
                    }
                    llavePrimaria.Add(tipoCol + nameCol);
                }
            }
            if (string.IsNullOrEmpty(strLlavePrimaria))
            {
                strLlavePrimaria = dtColumns.Rows[0][1] + " " + dtColumns.Rows[0][1] + dtColumns.Rows[0][0];
                llavePrimaria.Add(dtColumns.Rows[0][1] + dtColumns.Rows[0][0].ToString());
            }

            if (dtColumns.Rows.Count == 0)
                return "";
            if (dtColumns.Columns.Count == 0)
                return "";

            if (Directory.Exists(PathDesktop + "\\AppClient") == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\AppClient");
            }

            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase);
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que consume los servicios Web API para la tabla  " + pNameTabla);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();

            newTexto.AppendLine(CabeceraTmp);

            newTexto.AppendLine("");
            newTexto.AppendLine("#region");
            newTexto.AppendLine("using System;");
            newTexto.AppendLine("using System.Collections.Generic;");
            newTexto.AppendLine("using Newtonsoft.Json;");
            newTexto.AppendLine("using " + formulario.txtInfProyectoDal.Text + "; ");
            newTexto.AppendLine("using " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceEntidades.Text + ";");
            newTexto.AppendLine("using " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceNegocios.Text + ";");
            newTexto.AppendLine("using RestSharp;");
            newTexto.AppendLine("#endregion");
            newTexto.AppendLine("");

            newTexto.AppendLine("namespace " + formulario.txtInfProyecto.Text + ".AppClient");
            newTexto.AppendLine("{");
            newTexto.AppendLine("\tpublic class " + pNameClase + "");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\tpublic static List<" + pNameClaseEnt + "> Obtener()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar client = new RestClient(CParametrosApp.UrlWebApi + \"" + pNameTabla + "/Get\") {Timeout = -1};");
            newTexto.AppendLine("\t\t\tvar request = new RestRequest(Method.GET);");
            newTexto.AppendLine("\t\t\tvar response = client.Execute(request);");
            newTexto.AppendLine("\t\t\tvar miLista = JsonConvert.DeserializeObject<List<" + pNameClaseEnt + ">>(response.Content);");
            newTexto.AppendLine("\t\t\treturn miLista;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic static List<" + pNameClaseEnt + "> ObtenerObjeto(int id)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar client = new RestClient(CParametrosApp.UrlWebApi + \"" + pNameTabla + "/Get/\" + id) {Timeout = -1};");
            newTexto.AppendLine("\t\t\tvar request = new RestRequest(Method.GET);");
            newTexto.AppendLine("\t\t\tvar response = client.Execute(request);");
            newTexto.AppendLine("\t\t\tvar miLista = JsonConvert.DeserializeObject<List<" + pNameClaseEnt + ">>(response.Content);");
            newTexto.AppendLine("\t\t\treturn miLista;");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic static void InsertarObjeto(" + pNameClaseEnt + " obj)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar client = new RestClient(CParametrosApp.UrlWebApi + \"" + pNameTabla + "/Create\") {Timeout = -1};");
            newTexto.AppendLine("\t\t\tvar request = new RestRequest(Method.POST);");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"Content-Type\", \"application /json\");");
            newTexto.AppendLine("\t\t\trequest.AddParameter(\"application/json\", ");
            newTexto.AppendLine("\t\t\t\t\"{\" + ");
            newTexto.AppendLine("\t\t\t\t\"\\r\\n  \\\"\" + " + pNameClaseEnt + ".Fields.nombre + \"\\\": \\\"\" + obj.nombre +\"\\\", \" + ");
            newTexto.AppendLine("\t\t\t\t\"\\r\\n  \\\"\" + " + pNameClaseEnt + ".Fields.apitransaccion + \"\\\": \\\"\" + CApi.Transaccion.Crear +\"\\\", \" + ");
            newTexto.AppendLine("\t\t\t\t\"\\r\\n  \\\"\" + " + pNameClaseEnt + ".Fields.usucre + \"\\\"\" + obj.usucre +\"\\\", \" + ");
            newTexto.AppendLine("\t\t\t\t\"\\r\\n}\", ");
            newTexto.AppendLine("\t\t\t\tParameterType.RequestBody);");
            newTexto.AppendLine("\t\t\tvar response = client.Execute(request);");
            newTexto.AppendLine("\t\t\tConsole.WriteLine(response.Content);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic static void ActualizarObjeto(" + pNameClaseEnt + " obj)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar client = new RestClient(CParametrosApp.UrlWebApi + \"" + pNameTabla + "/Update/\" + obj.id_) {Timeout = -1};");
            newTexto.AppendLine("\t\t\tvar request = new RestRequest(Method.PUT);");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"Content-Type\", \"application /json\");");
            newTexto.AppendLine("\t\t\trequest.AddParameter(\"application/json\", ");
            newTexto.AppendLine("\t\t\t\t\"{\" + ");
            newTexto.AppendLine("\t\t\t\t\"\\r\\n  \\\"\" + " + pNameClaseEnt + ".Fields.nombre + \"\\\": \\\"\" + obj.nombre +\"\\\", \" + ");
            newTexto.AppendLine("\t\t\t\t\"\\r\\n  \\\"\" + " + pNameClaseEnt + ".Fields.apitransaccion + \"\\\": \\\"\" + CApi.Transaccion.Modificar +\"\\\", \" + ");
            newTexto.AppendLine("\t\t\t\t\"\\r\\n  \\\"\" + " + pNameClaseEnt + ".Fields.usumod + \"\\\"\" + obj.usumod +\"\\\", \" + ");
            newTexto.AppendLine("\t\t\t\t\"\\r\\n}\", ");
            newTexto.AppendLine("\t\t\t\tParameterType.RequestBody);");
            newTexto.AppendLine("\t\t\tvar response = client.Execute(request);");
            newTexto.AppendLine("\t\t\tConsole.WriteLine(response.Content);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic static void ActualizarEstado(" + pNameClaseEnt + " obj, CApi.Transaccion transaccion, string strUsuMod)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar client = new RestClient(CParametrosApp.UrlWebApi + \"" + pNameTabla + "/UpdateTransaccion/\" + transaccion +\"/\" + obj.id_) {Timeout = -1};");
            newTexto.AppendLine("\t\t\tvar request = new RestRequest(Method.PUT);");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"Content-Type\", \"application /json\");");
            newTexto.AppendLine("\t\t\trequest.AddParameter(\"text/json\", \"\\\" + strUsuMod + \"\\\" ,ParameterType.RequestBody);");
            newTexto.AppendLine("\t\t\tvar response = client.Execute(request);");
            newTexto.AppendLine("\t\t\tConsole.WriteLine(response.Content);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tpublic static void EliminarObjeto(int id)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tvar client = new RestClient(CParametrosApp.UrlWebApi + \"" + pNameTabla + "/Delete/\" + id) {Timeout = -1};");
            newTexto.AppendLine("\t\t\tvar request = new RestRequest(Method.DELETE);");
            newTexto.AppendLine("\t\t\trequest.AddHeader(\"Content-Type\", \"application /json\");");
            newTexto.AppendLine("\t\t\tvar response = client.Execute(request);");
            newTexto.AppendLine("\t\t\tConsole.WriteLine(response.Content);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("");

            newTexto.AppendLine("\t}");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), strNombreArchivo + ".cs", PathDesktop + "\\AppClient\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\AppClient\\" + strNombreArchivo + ".cs";
        }
    }
}