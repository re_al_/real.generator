﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using ReAl.Generator.App.Properties;
using System;
using System.Data;
using System.IO;

namespace ReAl.Generator.App.AppGenerators
{
    public class cReglaNegociosPython
    {
        public string CrearModeloPython(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }
            if (Directory.Exists(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\")) == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\"));
            }

            string pNameTabla = pNameClase.Replace("_", "");
            pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() +
                         pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();

            dynamic pNameClaseRn = formulario.txtPrefijoModelo.Text + pNameTabla;
            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameTabla;

            string strLlaves = "";
            string strLlavesWhere = "";
            bool bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString();

                    strLlavesWhere = strLlavesWhere + Environment.NewLine + "\t\t$this->db->where('" + nameCol + "', $id);";

                    if (bEsPrimerElemento)
                    {
                        strLlaves = strLlaves + "$" + tipoCol + "_" + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlaves = strLlaves + ", $" + tipoCol + "_" + nameCol;
                    }
                }
            }

            string strApiTrans = "";
            string strApiEstado = "";
            string strUsuCre = "";
            string strFecCre = "";
            string strUsuMod = "";
            string strFecMod = "";
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strUsuCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strUsuMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("cre"))
                    strFecCre = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("mod"))
                    strFecMod = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("estado"))
                    strApiEstado = nameCol;
                if (nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(pNameTabla.ToLower(), "").Contains("transaccion"))
                    strApiTrans = nameCol;
            }

            if (string.IsNullOrEmpty(strApiTrans))
                strApiTrans = "----";
            if (string.IsNullOrEmpty(strApiEstado))
                strApiEstado = "----";
            if (string.IsNullOrEmpty(strUsuCre))
                strUsuCre = "----";
            if (string.IsNullOrEmpty(strFecCre))
                strFecCre = "----";
            if (string.IsNullOrEmpty(strUsuMod))
                strUsuMod = "----";
            if (string.IsNullOrEmpty(strFecMod))
                strFecMod = "----";

            string FC = DateTime.Now.ToString("dd/MM/yyyy");
            string Creador = (string.IsNullOrEmpty(formulario.txtInfAutor.Text) ? "Automatico     " : formulario.txtInfAutor.Text);
            Creador = Creador.PadRight(15);
            string CabeceraTmp = "/***********************************************************************************************************\r\n\tNOMBRE:       %PAR_NOMBRE%\r\n\tDESCRIPCION:\r\n\t\t%PAR_DESCRIPCION%\r\n\r\n\tREVISIONES:\r\n\t\tVer        FECHA       Autor            Descripcion \r\n\t\t---------  ----------  ---------------  ------------------------------------\r\n\t\t1.0        " + FC + "  " + Creador + "  Creacion \r\n\r\n*************************************************************************************************************/\r\n";

            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase).Replace("#region", "").Replace("#endregion", "");
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que implementa los metodos y operaciones sobre la Tabla " + pNameTabla);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();
            newTexto.AppendLine("__author__ = '" + Creador + "'");
            newTexto.AppendLine("__copyright__ = 'Copyright " + DateTime.Now.ToString("yyyy") + "'");
            newTexto.AppendLine("__date__ = '" + FC + "'");
            newTexto.AppendLine("__version__ = '1.0.0'");
            newTexto.AppendLine("");
            newTexto.AppendLine("from " + formulario.txtNamespaceEntidades.Text + "." + pNameClaseEnt + " import " + pNameClaseEnt + ", " + pNameClaseEnt + "Fields");
            newTexto.AppendLine("from " + formulario.txtNamespaceNegocios.Text + "." + formulario.txtClaseConn.Text + " import " + formulario.txtClaseConn.Text + "");
            newTexto.AppendLine("");
            newTexto.AppendLine("");
            newTexto.AppendLine("class " + pNameClaseRn + "(object):");
            newTexto.AppendLine("\t\"\"\"Clase que implementa los metodos y operaciones sobre la Tabla " + pNameTabla + "\"\"\"");
            newTexto.AppendLine("\t");
            newTexto.AppendLine("\tdef obtenerObjetoFromField(self, field, value, strParamAdicionales = ''):");
            newTexto.AppendLine("\t\t\"\"\"Funcion que devuelve un objeto del Tipo " + pNameClaseEnt + " a partir de un Field Enum\"\"\"");
            newTexto.AppendLine("\t\tarrColumnasWhere = list()");
            newTexto.AppendLine("\t\tarrColumnasWhere.append(field)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tarrValoresWhere = list()");
            newTexto.AppendLine("\t\tarrValoresWhere.append(value)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn self.obtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales)");
            newTexto.AppendLine("");
            newTexto.AppendLine("");

            newTexto.AppendLine("\tdef obtenerObjeto(self, arrColumnasWhere, arrValoresWhere, strParamAdicionales = ''):");
            newTexto.AppendLine("\t\t\"\"\"Funcion que devuelve un objeto del Tipo " + pNameClaseEnt + " a partir de Listas\"\"\"");
            newTexto.AppendLine("\t\tif arrColumnasWhere is None:");
            newTexto.AppendLine("\t\t\tarrColumnasWhere = list()");
            newTexto.AppendLine("\t\t\tarrColumnasWhere.append('1')");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tif arrValoresWhere is None:");
            newTexto.AppendLine("\t\t\tarrValoresWhere = list()");
            newTexto.AppendLine("\t\t\tarrValoresWhere.append('1')");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tlista = self.obtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tif len(lista) == 0:");
            newTexto.AppendLine("\t\t\treturn None");
            newTexto.AppendLine("\t\telif len(lista) > 1:");
            newTexto.AppendLine("\t\t\traise NameError('Se ha devuelto más de un Objeto')");
            newTexto.AppendLine("\t\telse:");
            newTexto.AppendLine("\t\t\treturn lista[0]");
            newTexto.AppendLine("");
            newTexto.AppendLine("");

            newTexto.AppendLine("\tdef obtenerLista(self, arrColumnasWhere = None, arrValoresWhere = None, strParamAdicionales = ''):");
            newTexto.AppendLine("\t\t\"\"\"Funcion que devuelve una lista de objetos del Tipo " + pNameClaseEnt + " a partir de un Field Enum\"\"\"");
            newTexto.AppendLine("\t\tif arrColumnasWhere is None:");
            newTexto.AppendLine("\t\t\tarrColumnasWhere = list()");
            newTexto.AppendLine("\t\t\tarrColumnasWhere.append('1')");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tif arrValoresWhere is None:");
            newTexto.AppendLine("\t\t\tarrValoresWhere = list()");
            newTexto.AppendLine("\t\t\tarrValoresWhere.append('1')");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tarrColumnas = list()");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tarrColumnas.append(" + pNameClaseEnt + "Fields." + nameCol + ")");
            }

            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tmyCon = cConn()");
            newTexto.AppendLine("\t\tquery = myCon.buildSelectQuery(" + pNameClaseEnt + ".NAME_TABLE, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales)");
            newTexto.AppendLine("\t\tcursor = myCon._dbConn.cursor()");
            newTexto.AppendLine("\t\tcursor.execute(query)");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\ttable = list()");
            newTexto.AppendLine("\t\tfor row in cursor.fetchall():");
            newTexto.AppendLine("\t\t\ttable.append(self.__crearObjeto(row))");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\tcursor.close()");
            newTexto.AppendLine("\t\tmyCon._dbConn.close()");
            newTexto.AppendLine("\t\treturn table");
            newTexto.AppendLine("");
            newTexto.AppendLine("");

            newTexto.AppendLine("\tdef __crearObjeto(self, row):");
            newTexto.AppendLine("\t\tobj = entSegUsuarios()");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string nameCol = dtColumns.Rows[iCol][0].ToString();
                newTexto.AppendLine("\t\tobj." + nameCol + " = row[" + iCol + "]");
            }
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\treturn obj");

            newTexto.AppendLine("");
            newTexto.AppendLine("");

            CFunciones.CrearArchivo(newTexto.ToString(), pNameClaseRn + ".py", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\" + pNameClase + ".py";
        }

        public string crearcParams(ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();

            newTexto.AppendLine("__author__ = '" + formulario.txtInfAutor.Text + "'");
            newTexto.AppendLine("");
            newTexto.AppendLine("class cParametros(object):");
            newTexto.AppendLine("\tN_SERVER = '" + Principal.Servidor + "'");
            newTexto.AppendLine("\tN_PUERTO = '" + Principal.Puerto + "'");
            newTexto.AppendLine("\tN_BD_USER = '" + Principal.UsuarioBD + "'");
            newTexto.AppendLine("\tN_BD_PASS = '" + Principal.PasswordBD + "'");
            newTexto.AppendLine("\tN_BD_NAME = '" + Principal.Catalogo + "'");
            newTexto.AppendLine("\tN_BD_SCHEMA = ''");

            CFunciones.CrearArchivo(newTexto.ToString(), formulario.txtClaseParametros.Text + ".py", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\cParametros.py";
        }

        public string crearConn(ref FPrincipal formulario)
        {
            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoClass.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoClass.Text;
            }

            string newTexto = Resources.WIN_PY_cConn;

            newTexto = newTexto.Replace("%CLASS_CONN%", formulario.txtClaseConn.Text);
            newTexto = newTexto.Replace("%NAMESPACE_NEGOCIOS%", formulario.txtNamespaceNegocios.Text);
            newTexto = newTexto.Replace("%CLASS_PARAMETROS%", formulario.txtClaseParametros.Text);

            CFunciones.CrearArchivo(newTexto.ToString(), formulario.txtClaseConn.Text + ".py", PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\" + formulario.txtNamespaceNegocios.Text.Replace(".", "\\") + "\\" + formulario.txtClaseConn.Text + ".py";
        }
    }
}