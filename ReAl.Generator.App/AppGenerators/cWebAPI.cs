﻿using ReAl.Generator.App.AppCore;
using System;
using System.Collections;
using System.Data;
using System.IO;

namespace ReAl.Generator.App.AppGenerators
{
    public class cWebAPI
    {
        public string CrearWebApiCs(DataTable dtColumns, string pNameClase, ref FPrincipal formulario)
        {
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string pNameTabla = pNameClase.Replace("_", "");
            pNameTabla = pNameTabla.Substring(0, 1).ToUpper() + pNameTabla.Substring(1, 2).ToLower() +
                         pNameTabla.Substring(3, 1).ToUpper() + pNameTabla.Substring(4).ToLower();

            pNameClase = pNameTabla + "Controller";

            string strNombreArchivo = pNameTabla + "Controller";

            string pNameClaseEnt = formulario.txtPrefijoEntidades.Text + pNameTabla;
            string pNameClaseRn = formulario.txtPrefijoModelo.Text + pNameTabla;

            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
            {
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyecto.Text;
            }
            else
            {
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyecto.Text;
            }

            //Encontramos la primera columna
            ArrayList llavePrimaria = new ArrayList();
            string strLlavePrimaria = "";
            string strLlavePrimariaObtenerObjeto = "";
            dynamic bEsPrimerElemento = true;
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    string nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString();

                    if (bEsPrimerElemento)
                    {
                        strLlavePrimaria = strLlavePrimaria + tipoCol + " " + tipoCol + nameCol;
                        strLlavePrimariaObtenerObjeto = strLlavePrimariaObtenerObjeto + tipoCol + nameCol;
                        bEsPrimerElemento = false;
                    }
                    else
                    {
                        strLlavePrimaria = strLlavePrimaria + ", " + tipoCol + " " + tipoCol + nameCol;
                        strLlavePrimariaObtenerObjeto = strLlavePrimariaObtenerObjeto + ", " + tipoCol + nameCol;
                    }
                    llavePrimaria.Add(tipoCol + nameCol);
                }
            }
            if (string.IsNullOrEmpty(strLlavePrimaria))
            {
                strLlavePrimaria = dtColumns.Rows[0][1] + " " + dtColumns.Rows[0][1] + dtColumns.Rows[0][0];
                llavePrimaria.Add(dtColumns.Rows[0][1] + dtColumns.Rows[0][0].ToString());
            }

            if (dtColumns.Rows.Count == 0)
                return "";
            if (dtColumns.Columns.Count == 0)
                return "";

            if (Directory.Exists(PathDesktop + "\\API") == false)
            {
                Directory.CreateDirectory(PathDesktop + "\\API");
            }

            string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
            CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", pNameClase);
            CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que crea el Web API para la tabla  " + pNameTabla);

            System.Text.StringBuilder newTexto = new System.Text.StringBuilder();

            newTexto.AppendLine(CabeceraTmp);

            newTexto.AppendLine("");
            newTexto.AppendLine("#region");
            newTexto.AppendLine("using System;");
            newTexto.AppendLine("using System.Collections.Generic;");
            newTexto.AppendLine("using System.Linq;");
            newTexto.AppendLine("using System.Net;");
            newTexto.AppendLine("using System.Net.Http;");
            newTexto.AppendLine("using System.Web.Http;");
            newTexto.AppendLine("using " + formulario.txtInfProyectoDal.Text + "; ");
            newTexto.AppendLine("using " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceEntidades.Text + ";");
            newTexto.AppendLine("using " + formulario.txtInfProyectoClass.Text + "." + formulario.txtNamespaceNegocios.Text + ";");
            newTexto.AppendLine("#endregion");
            newTexto.AppendLine("");

            newTexto.AppendLine("namespace " + formulario.txtInfProyecto.Text + ".API");
            newTexto.AppendLine("{");
            newTexto.AppendLine("\tpublic class " + pNameClase + " : ApiController");
            newTexto.AppendLine("\t{");
            newTexto.AppendLine("\t\t//KB: https://docs.microsoft.com/en-us/aspnet/web-api/overview/older-versions/creating-a-web-api-that-supports-crud-operations");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t///<summary>");
            newTexto.AppendLine("\t\t/// GET /api/" + pNameClaseEnt.ToLower() + "");
            newTexto.AppendLine("\t\t///</summary>");
            newTexto.AppendLine("\t\t/// <returns>Retorna un conjunto de objetos de tipo " + pNameClaseEnt + "</returns>");
            newTexto.AppendLine("\t\tpublic IQueryable<" + pNameClaseEnt + "> Get()");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t" + pNameClaseRn + " rn = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\treturn rn.ObtenerLista().AsEnumerable();");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t///<summary>");
            newTexto.AppendLine("\t\t/// GET /api/" + pNameClaseEnt.ToLower() + "/{id}");
            newTexto.AppendLine("\t\t///</summary>");
            newTexto.AppendLine("\t\t/// <param name=\"id\">Identificador unico del objeto</param>");
            newTexto.AppendLine("\t\t/// <returns>Retorna un conjunto de objetos de tipo " + pNameClaseEnt + " en base al ID</returns>");
            newTexto.AppendLine("\t\t[Authorize]");
            newTexto.AppendLine("\t\tpublic IHttpActionResult Get(int id)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t" + pNameClaseRn + " rn = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\t" + pNameClaseEnt + " objPag = rn.ObtenerObjeto(id);");
            newTexto.AppendLine("\t\t\tif (objPag == null)");
            newTexto.AppendLine("\t\t\t\treturn NotFound();");
            newTexto.AppendLine("\t\t\treturn Ok(objPag);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t///<summary>");
            newTexto.AppendLine("\t\t/// POST /api/segusuarios");
            newTexto.AppendLine("\t\t///</summary>");
            newTexto.AppendLine("\t\t/// <param name=\"value\">objeto del tipo entSegUsuarios</param>");
            newTexto.AppendLine("\t\t/// <returns>Retorna una respuesta HTTP en base al exito de la operacion</returns>");
            newTexto.AppendLine("\t\tpublic HttpResponseMessage Post(" + pNameClaseEnt + " value)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (ModelState.IsValid)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\t" + pNameClaseRn + " rn = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\t\t" + pNameClaseEnt + " objPag = rn.ObtenerObjeto(value.CodSegUsuarios);");
            newTexto.AppendLine("\t\t\t\tif (objPag == null)");
            newTexto.AppendLine("\t\t\t\t\trn.Insert(value);");
            newTexto.AppendLine("\t\t\t\telse");
            newTexto.AppendLine("\t\t\t\t\trn.Update(value);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t//Created!");
            newTexto.AppendLine("\t\t\t\tvar response = Request.CreateResponse<entSegUsuarios>(HttpStatusCode.Created, value);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\t//Let them know where the new SegUsuarios is");
            newTexto.AppendLine("\t\t\t\tstring uri = Url.Route(null, new { id = value.CodSegUsuarios });");
            newTexto.AppendLine("\t\t\t\tresponse.Headers.Location = new Uri(Request.RequestUri, uri);");
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\treturn response;");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tthrow new HttpResponseException(HttpStatusCode.BadRequest);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t///<summary>");
            newTexto.AppendLine("\t\t/// PUT /api/" + pNameClaseEnt + "/{id}");
            newTexto.AppendLine("\t\t///</summary>");
            newTexto.AppendLine("\t\t/// <param name=\"id\">Identificador unico del objeto</param>");
            newTexto.AppendLine("\t\t/// <param name=\"value\">objeto del tipo entSegUsuarios</param>");
            newTexto.AppendLine("\t\t/// <returns>Retorna una respuesta HTTP en base al exito de la operacion</returns>");
            newTexto.AppendLine("\t\t[Authorize]");
            newTexto.AppendLine("\t\tpublic HttpResponseMessage Put(int id, " + pNameClaseEnt + " value)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\tif (ModelState.IsValid)");
            newTexto.AppendLine("\t\t\t{");
            newTexto.AppendLine("\t\t\t\t" + pNameClaseRn + " rn = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\t\t" + pNameClaseEnt + " objPag = rn.ObtenerObjeto(id);");
            newTexto.AppendLine("\t\t\t\tif (objPag == null)");
            newTexto.AppendLine("\t\t\t\t\trn.Insert(value);");
            newTexto.AppendLine("\t\t\t\telse");
            newTexto.AppendLine("\t\t\t\t\trn.Update(value);");
            newTexto.AppendLine("\t\t\t\treturn new HttpResponseMessage(HttpStatusCode.NoContent);");
            newTexto.AppendLine("\t\t\t}");
            newTexto.AppendLine("\t\t\tthrow new HttpResponseException(HttpStatusCode.BadRequest);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("\t\t///<summary>");
            newTexto.AppendLine("\t\t/// DELETE /api/segusuarios/{id}");
            newTexto.AppendLine("\t\t///</summary>");
            newTexto.AppendLine("\t\t/// <param name=\"id\">Identificador unico del objeto</param>");
            newTexto.AppendLine("\t\t[Authorize]");
            newTexto.AppendLine("\t\tpublic void Delete(int id)");
            newTexto.AppendLine("\t\t{");
            newTexto.AppendLine("\t\t\t" + pNameClaseRn + " rn = new " + pNameClaseRn + "();");
            newTexto.AppendLine("\t\t\t" + pNameClaseEnt + " objPag = rn.ObtenerObjeto(id);");
            newTexto.AppendLine("\t\t\tif (objPag == null)");
            newTexto.AppendLine("\t\t\t\tthrow new HttpResponseException(HttpStatusCode.NotFound);");
            newTexto.AppendLine("\t\t\trn.Delete(objPag);");
            newTexto.AppendLine("\t\t}");
            newTexto.AppendLine("\t\t");
            newTexto.AppendLine("");

            newTexto.AppendLine("\t}");
            newTexto.AppendLine("}");

            CFunciones.CrearArchivo(newTexto.ToString(), strNombreArchivo + ".cs", PathDesktop + "\\API\\");

            //Return newTexto.ToString()
            return PathDesktop + "\\API\\" + strNombreArchivo + ".cs";
        }
    }
}