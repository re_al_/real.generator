﻿namespace ReAl.Generator.App.AppClass
{
    public static class Principal
    {
        public static TipoBD DBMS = TipoBD.PostgreSQL;
        public static bool SegIntegrada = true;
        public static string FBFile = "";
        public static string Servidor = "WSRVERA\\SQLEXPRESS";
        public static string Catalogo = "master";
        public static string Servicio = "AFILIA";
        public static int Puerto = 5432;
        public static string UsuarioBD = "correspondencia";
        public static string PasswordBD = "correspondencia";

        public static int ConnectTimeOut = 60;
        public static string Plantilla = "";
    }
}