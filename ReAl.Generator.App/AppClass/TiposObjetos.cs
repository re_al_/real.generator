﻿namespace ReAl.Generator.App.AppClass
{
    public enum TipoEntorno
    {
        CSharp,
        CSharp_WinForms,
        CSharp_WebForms,
        CSharp_WCF,
        CSharp_WSE,
        CSharp_WebAPI,
        CSharp_NetCore_V2,
        CSharp_NetCore_WebAPI_V2,        
        CSharp_NetCore_WebAPI_V2_Swagger,
        CSharp_NetCore_Ef,
        CSharp_NetCore_V5,
        CSharp_NetCore_V5_WebAPI_Client,
        VB_Net,
        VB_Net_WCF,
        Python,
        PHP_CodeIgniter,
        PHP_CodeIgniter_Models_API,
        PHP_Clasico,
        Java_Android,
        Java_Clasico,
        Angular_CLI,
        Feathers_JS
    }

    public enum TipoBD
    {
        MsAccess,
        SQLServer,
        SQLServer2000,
        Oracle,
        PostgreSQL,
        MySql,
        SQLite,
        FireBird
    }

    public enum TipoColumnas
    {
        PrimeraMayuscula,
        Minusculas,
        Mayusculas,
        PascalCase,
        CamelCase
    }

    public enum Languaje
    {
        CSharp,
        VbNet,
        JavaAndroid,
        Java,
        PHPCodeIgniter,
        PHP,
        Python,
        FeathersJS
    }

    public enum CodeSnippet
    {
        BootStrap_Abm_WebForms,
        BootStrap_Doc_WebForms,
        BootStrap_Datagrid_WebForms,
        NetCore_Scaffoldind_Controllers,
        NetCore_Scaffoldind_Views_List,
        NetCore_Scaffoldind_Views_Details,
        NetCore_Scaffoldind_Views_Edit,
        NetCore_Scaffoldind_Views_Create,
        NetCore_Scaffoldind_Views_Delete,
        INTEGRATE_Niveles_restriccion,
        WebForms_List,
        WebForms_ABM,
        WebForms_Misxtos,
        WebForms_Control_ABM,
        WinForms_Form_ABM,
        WinForms_SubForm_MDI,
        WinForms_DevExpress,
        WinForms_Control_Listado,
    }
}