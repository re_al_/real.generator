﻿namespace ReAl.Generator.App
{
    partial class FPlantillasDoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPlantillasDoc));
            this.cmbEntorno = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.grpParametrosAdicionales = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtClaseParametrosClaseApiObject = new System.Windows.Forms.TextBox();
            this.chkClassParcial = new System.Windows.Forms.CheckBox();
            this.chkEntBaseClass = new System.Windows.Forms.CheckBox();
            this.chkPropertyEvents = new System.Windows.Forms.CheckBox();
            this.chkEntidadesEncriptacion = new System.Windows.Forms.CheckBox();
            this.chkDataAnnotations = new System.Windows.Forms.CheckBox();
            this.chkUsarSpsIdentity = new System.Windows.Forms.CheckBox();
            this.chkUsarSpsCheck = new System.Windows.Forms.CheckBox();
            this.txtClaseParametros = new System.Windows.Forms.TextBox();
            this.txtClaseParametrosListadoSp = new System.Windows.Forms.TextBox();
            this.txtClaseParametrosBaseClass = new System.Windows.Forms.TextBox();
            this.txtClaseParametrosClaseApi = new System.Windows.Forms.TextBox();
            this.chkParametroPa = new System.Windows.Forms.CheckBox();
            this.chkUsarSpsSelect = new System.Windows.Forms.CheckBox();
            this.chkUsarSps = new System.Windows.Forms.CheckBox();
            this.chkUsarSchemaInModelo = new System.Windows.Forms.CheckBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label101 = new System.Windows.Forms.Label();
            this.Label102 = new System.Windows.Forms.Label();
            this.Label103 = new System.Windows.Forms.Label();
            this.txtClaseTransaccion = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.txtClaseConn = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.GroupBox5 = new System.Windows.Forms.GroupBox();
            this.txtNamespaceDocumentacion = new System.Windows.Forms.TextBox();
            this.chkReAlCrearDoc = new System.Windows.Forms.CheckBox();
            this.chkReAlDataSet = new System.Windows.Forms.CheckBox();
            this.txtNombreDataSet = new System.Windows.Forms.TextBox();
            this.TextBox2 = new System.Windows.Forms.TextBox();
            this.txtPrefijoUnitTest = new System.Windows.Forms.TextBox();
            this.Label27 = new System.Windows.Forms.Label();
            this.txtNamespaceUnitTest = new System.Windows.Forms.TextBox();
            this.chkReAlUnitTest = new System.Windows.Forms.CheckBox();
            this.textbox10 = new System.Windows.Forms.TextBox();
            this.txtPrefijoEntidades = new System.Windows.Forms.TextBox();
            this.Label16 = new System.Windows.Forms.Label();
            this.TextBox6 = new System.Windows.Forms.TextBox();
            this.txtPrefijoInterface = new System.Windows.Forms.TextBox();
            this.Label15 = new System.Windows.Forms.Label();
            this.TextBox3 = new System.Windows.Forms.TextBox();
            this.txtPrefijoModelo = new System.Windows.Forms.TextBox();
            this.Label14 = new System.Windows.Forms.Label();
            this.txtNamespaceNegocios = new System.Windows.Forms.TextBox();
            this.txtNamespaceInterface = new System.Windows.Forms.TextBox();
            this.txtNamespaceEntidades = new System.Windows.Forms.TextBox();
            this.chkReAlReglaNegocios = new System.Windows.Forms.CheckBox();
            this.chkReAlInterface = new System.Windows.Forms.CheckBox();
            this.chkReAlEntidades = new System.Windows.Forms.CheckBox();
            this.GroupBox7 = new System.Windows.Forms.GroupBox();
            this.cmbTipoColumnas = new System.Windows.Forms.ComboBox();
            this.GroupBox23 = new System.Windows.Forms.GroupBox();
            this.txtInfAutor = new System.Windows.Forms.TextBox();
            this.GroupBox19 = new System.Windows.Forms.GroupBox();
            this.cmbAPIs = new System.Windows.Forms.ComboBox();
            this.grpGroupBox8 = new System.Windows.Forms.GroupBox();
            this.Label46 = new System.Windows.Forms.Label();
            this.txtInfProyectoConn = new System.Windows.Forms.TextBox();
            this.Label28 = new System.Windows.Forms.Label();
            this.txtInfProyectoUnitTest = new System.Windows.Forms.TextBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.txtInfProyectoDal = new System.Windows.Forms.TextBox();
            this.txtInfProyecto = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.txtInfProyectoClass = new System.Windows.Forms.TextBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.GroupBox4 = new System.Windows.Forms.GroupBox();
            this.rdbSpIfExists = new System.Windows.Forms.RadioButton();
            this.rdbSpAlter = new System.Windows.Forms.RadioButton();
            this.rdbSpCreate = new System.Windows.Forms.RadioButton();
            this.grpSpIns = new System.Windows.Forms.GroupBox();
            this.chkSPInsIdentity = new System.Windows.Forms.CheckBox();
            this.chkSPInsFecCreGetDate = new System.Windows.Forms.CheckBox();
            this.chkSPInsApiEstado = new System.Windows.Forms.CheckBox();
            this.chkSPInsFecCre = new System.Windows.Forms.CheckBox();
            this.chkSPInsUsuCre = new System.Windows.Forms.CheckBox();
            this.grpSpUpd = new System.Windows.Forms.GroupBox();
            this.chkSPUpdFecModGetDate = new System.Windows.Forms.CheckBox();
            this.chkSPUpdFecMod = new System.Windows.Forms.CheckBox();
            this.chkSPUpdUsuMod = new System.Windows.Forms.CheckBox();
            this.txtSPsig = new System.Windows.Forms.TextBox();
            this.TextBox4 = new System.Windows.Forms.TextBox();
            this.txtSPprev = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.txtNombrePlantilla = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtServicio = new System.Windows.Forms.TextBox();
            this.lblServPuerto = new System.Windows.Forms.Label();
            this.cmbDbms = new System.Windows.Forms.ComboBox();
            this.chkSegIntegrada = new System.Windows.Forms.CheckBox();
            this.lblPass = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.lblServidor = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.grpParametrosAdicionales.SuspendLayout();
            this.GroupBox5.SuspendLayout();
            this.GroupBox7.SuspendLayout();
            this.GroupBox23.SuspendLayout();
            this.GroupBox19.SuspendLayout();
            this.grpGroupBox8.SuspendLayout();
            this.GroupBox4.SuspendLayout();
            this.grpSpIns.SuspendLayout();
            this.grpSpUpd.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbEntorno
            // 
            this.cmbEntorno.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEntorno.FormattingEnabled = true;
            this.cmbEntorno.Location = new System.Drawing.Point(104, 34);
            this.cmbEntorno.Name = "cmbEntorno";
            this.cmbEntorno.Size = new System.Drawing.Size(343, 21);
            this.cmbEntorno.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(12, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "ENTORNO:";
            // 
            // grpParametrosAdicionales
            // 
            this.grpParametrosAdicionales.Controls.Add(this.label7);
            this.grpParametrosAdicionales.Controls.Add(this.txtClaseParametrosClaseApiObject);
            this.grpParametrosAdicionales.Controls.Add(this.chkClassParcial);
            this.grpParametrosAdicionales.Controls.Add(this.chkEntBaseClass);
            this.grpParametrosAdicionales.Controls.Add(this.chkPropertyEvents);
            this.grpParametrosAdicionales.Controls.Add(this.chkEntidadesEncriptacion);
            this.grpParametrosAdicionales.Controls.Add(this.chkDataAnnotations);
            this.grpParametrosAdicionales.Controls.Add(this.chkUsarSpsIdentity);
            this.grpParametrosAdicionales.Controls.Add(this.chkUsarSpsCheck);
            this.grpParametrosAdicionales.Controls.Add(this.txtClaseParametros);
            this.grpParametrosAdicionales.Controls.Add(this.txtClaseParametrosListadoSp);
            this.grpParametrosAdicionales.Controls.Add(this.txtClaseParametrosBaseClass);
            this.grpParametrosAdicionales.Controls.Add(this.txtClaseParametrosClaseApi);
            this.grpParametrosAdicionales.Controls.Add(this.chkParametroPa);
            this.grpParametrosAdicionales.Controls.Add(this.chkUsarSpsSelect);
            this.grpParametrosAdicionales.Controls.Add(this.chkUsarSps);
            this.grpParametrosAdicionales.Controls.Add(this.chkUsarSchemaInModelo);
            this.grpParametrosAdicionales.Controls.Add(this.Label10);
            this.grpParametrosAdicionales.Controls.Add(this.Label101);
            this.grpParametrosAdicionales.Controls.Add(this.Label102);
            this.grpParametrosAdicionales.Controls.Add(this.Label103);
            this.grpParametrosAdicionales.Controls.Add(this.txtClaseTransaccion);
            this.grpParametrosAdicionales.Controls.Add(this.Label4);
            this.grpParametrosAdicionales.Controls.Add(this.txtClaseConn);
            this.grpParametrosAdicionales.Controls.Add(this.Label9);
            this.grpParametrosAdicionales.Location = new System.Drawing.Point(12, 195);
            this.grpParametrosAdicionales.Name = "grpParametrosAdicionales";
            this.grpParametrosAdicionales.Size = new System.Drawing.Size(397, 254);
            this.grpParametrosAdicionales.TabIndex = 11;
            this.grpParametrosAdicionales.TabStop = false;
            this.grpParametrosAdicionales.Text = "Parametros Adicionales";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(267, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "API Object:";
            // 
            // txtClaseParametrosClaseApiObject
            // 
            this.txtClaseParametrosClaseApiObject.Location = new System.Drawing.Point(266, 95);
            this.txtClaseParametrosClaseApiObject.Name = "txtClaseParametrosClaseApiObject";
            this.txtClaseParametrosClaseApiObject.Size = new System.Drawing.Size(80, 20);
            this.txtClaseParametrosClaseApiObject.TabIndex = 13;
            this.txtClaseParametrosClaseApiObject.Text = "CApiObject";
            // 
            // chkClassParcial
            // 
            this.chkClassParcial.AutoSize = true;
            this.chkClassParcial.Checked = true;
            this.chkClassParcial.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkClassParcial.Location = new System.Drawing.Point(7, 168);
            this.chkClassParcial.Name = "chkClassParcial";
            this.chkClassParcial.Size = new System.Drawing.Size(174, 17);
            this.chkClassParcial.TabIndex = 20;
            this.chkClassParcial.Text = "Crear Clase Parcial para Textos";
            this.chkClassParcial.UseVisualStyleBackColor = true;
            // 
            // chkEntBaseClass
            // 
            this.chkEntBaseClass.AutoSize = true;
            this.chkEntBaseClass.Location = new System.Drawing.Point(7, 201);
            this.chkEntBaseClass.Name = "chkEntBaseClass";
            this.chkEntBaseClass.Size = new System.Drawing.Size(187, 17);
            this.chkEntBaseClass.TabIndex = 22;
            this.chkEntBaseClass.Text = "Utilizar Clase Base para Entidades";
            this.chkEntBaseClass.UseVisualStyleBackColor = true;
            // 
            // chkPropertyEvents
            // 
            this.chkPropertyEvents.AutoSize = true;
            this.chkPropertyEvents.Location = new System.Drawing.Point(23, 233);
            this.chkPropertyEvents.Name = "chkPropertyEvents";
            this.chkPropertyEvents.Size = new System.Drawing.Size(207, 17);
            this.chkPropertyEvents.TabIndex = 24;
            this.chkPropertyEvents.Text = "Generar Eventos para las propiedades";
            this.chkPropertyEvents.UseVisualStyleBackColor = true;
            // 
            // chkEntidadesEncriptacion
            // 
            this.chkEntidadesEncriptacion.AutoSize = true;
            this.chkEntidadesEncriptacion.Checked = true;
            this.chkEntidadesEncriptacion.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkEntidadesEncriptacion.Location = new System.Drawing.Point(23, 217);
            this.chkEntidadesEncriptacion.Name = "chkEntidadesEncriptacion";
            this.chkEntidadesEncriptacion.Size = new System.Drawing.Size(192, 17);
            this.chkEntidadesEncriptacion.TabIndex = 23;
            this.chkEntidadesEncriptacion.Text = "Generar Algoritmos de Encriptación";
            this.chkEntidadesEncriptacion.UseVisualStyleBackColor = true;
            // 
            // chkDataAnnotations
            // 
            this.chkDataAnnotations.AutoSize = true;
            this.chkDataAnnotations.Checked = true;
            this.chkDataAnnotations.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDataAnnotations.Location = new System.Drawing.Point(7, 185);
            this.chkDataAnnotations.Name = "chkDataAnnotations";
            this.chkDataAnnotations.Size = new System.Drawing.Size(195, 17);
            this.chkDataAnnotations.TabIndex = 21;
            this.chkDataAnnotations.Text = "Usar DataAnnotations en las Clases";
            this.chkDataAnnotations.UseVisualStyleBackColor = true;
            // 
            // chkUsarSpsIdentity
            // 
            this.chkUsarSpsIdentity.AutoSize = true;
            this.chkUsarSpsIdentity.Location = new System.Drawing.Point(7, 152);
            this.chkUsarSpsIdentity.Name = "chkUsarSpsIdentity";
            this.chkUsarSpsIdentity.Size = new System.Drawing.Size(178, 17);
            this.chkUsarSpsIdentity.TabIndex = 18;
            this.chkUsarSpsIdentity.Text = "Usar InsertIdentity para INSERT";
            this.chkUsarSpsIdentity.UseVisualStyleBackColor = true;
            // 
            // chkUsarSpsCheck
            // 
            this.chkUsarSpsCheck.AutoSize = true;
            this.chkUsarSpsCheck.Checked = true;
            this.chkUsarSpsCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUsarSpsCheck.Location = new System.Drawing.Point(220, 134);
            this.chkUsarSpsCheck.Name = "chkUsarSpsCheck";
            this.chkUsarSpsCheck.Size = new System.Drawing.Size(150, 17);
            this.chkUsarSpsCheck.TabIndex = 17;
            this.chkUsarSpsCheck.Text = "Verificar parametros NULL";
            this.chkUsarSpsCheck.UseVisualStyleBackColor = true;
            // 
            // txtClaseParametros
            // 
            this.txtClaseParametros.Location = new System.Drawing.Point(206, 59);
            this.txtClaseParametros.Name = "txtClaseParametros";
            this.txtClaseParametros.Size = new System.Drawing.Size(185, 20);
            this.txtClaseParametros.TabIndex = 5;
            this.txtClaseParametros.Text = "cParametros";
            // 
            // txtClaseParametrosListadoSp
            // 
            this.txtClaseParametrosListadoSp.Location = new System.Drawing.Point(8, 95);
            this.txtClaseParametrosListadoSp.Name = "txtClaseParametrosListadoSp";
            this.txtClaseParametrosListadoSp.Size = new System.Drawing.Size(80, 20);
            this.txtClaseParametrosListadoSp.TabIndex = 10;
            this.txtClaseParametrosListadoSp.Text = "CListadoSP";
            // 
            // txtClaseParametrosBaseClass
            // 
            this.txtClaseParametrosBaseClass.Location = new System.Drawing.Point(94, 95);
            this.txtClaseParametrosBaseClass.Name = "txtClaseParametrosBaseClass";
            this.txtClaseParametrosBaseClass.Size = new System.Drawing.Size(80, 20);
            this.txtClaseParametrosBaseClass.TabIndex = 11;
            this.txtClaseParametrosBaseClass.Text = "CBaseClass";
            // 
            // txtClaseParametrosClaseApi
            // 
            this.txtClaseParametrosClaseApi.Location = new System.Drawing.Point(180, 95);
            this.txtClaseParametrosClaseApi.Name = "txtClaseParametrosClaseApi";
            this.txtClaseParametrosClaseApi.Size = new System.Drawing.Size(80, 20);
            this.txtClaseParametrosClaseApi.TabIndex = 12;
            this.txtClaseParametrosClaseApi.Text = "CApi";
            // 
            // chkParametroPa
            // 
            this.chkParametroPa.AutoSize = true;
            this.chkParametroPa.Enabled = false;
            this.chkParametroPa.Location = new System.Drawing.Point(7, 134);
            this.chkParametroPa.Name = "chkParametroPa";
            this.chkParametroPa.Size = new System.Drawing.Size(182, 17);
            this.chkParametroPa.TabIndex = 16;
            this.chkParametroPa.Text = "Incluir \"pa\" en los parametros SP";
            this.chkParametroPa.UseVisualStyleBackColor = true;
            // 
            // chkUsarSpsSelect
            // 
            this.chkUsarSpsSelect.AutoSize = true;
            this.chkUsarSpsSelect.Location = new System.Drawing.Point(7, 116);
            this.chkUsarSpsSelect.Name = "chkUsarSpsSelect";
            this.chkUsarSpsSelect.Size = new System.Drawing.Size(163, 17);
            this.chkUsarSpsSelect.TabIndex = 14;
            this.chkUsarSpsSelect.Text = "Utilizar SPs para los SELECT";
            this.chkUsarSpsSelect.UseVisualStyleBackColor = true;
            // 
            // chkUsarSps
            // 
            this.chkUsarSps.AutoSize = true;
            this.chkUsarSps.Checked = true;
            this.chkUsarSps.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUsarSps.Location = new System.Drawing.Point(200, 116);
            this.chkUsarSps.Name = "chkUsarSps";
            this.chkUsarSps.Size = new System.Drawing.Size(150, 17);
            this.chkUsarSps.TabIndex = 15;
            this.chkUsarSps.Text = "Utilizar SPs para los ABMs";
            this.chkUsarSps.UseVisualStyleBackColor = true;
            // 
            // chkUsarSchemaInModelo
            // 
            this.chkUsarSchemaInModelo.AutoSize = true;
            this.chkUsarSchemaInModelo.Location = new System.Drawing.Point(200, 152);
            this.chkUsarSchemaInModelo.Name = "chkUsarSchemaInModelo";
            this.chkUsarSchemaInModelo.Size = new System.Drawing.Size(193, 17);
            this.chkUsarSchemaInModelo.TabIndex = 19;
            this.chkUsarSchemaInModelo.Text = "Concatenar SCHEMA en el Modelo";
            this.chkUsarSchemaInModelo.UseVisualStyleBackColor = true;
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(8, 62);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(198, 13);
            this.Label10.TabIndex = 4;
            this.Label10.Text = "Nombre de las Clases:          Parametros:";
            // 
            // Label101
            // 
            this.Label101.AutoSize = true;
            this.Label101.Location = new System.Drawing.Point(8, 82);
            this.Label101.Name = "Label101";
            this.Label101.Size = new System.Drawing.Size(66, 13);
            this.Label101.TabIndex = 7;
            this.Label101.Text = "Listado SPs:";
            // 
            // Label102
            // 
            this.Label102.AutoSize = true;
            this.Label102.Location = new System.Drawing.Point(94, 82);
            this.Label102.Name = "Label102";
            this.Label102.Size = new System.Drawing.Size(63, 13);
            this.Label102.TabIndex = 8;
            this.Label102.Text = "Clase Base:";
            // 
            // Label103
            // 
            this.Label103.AutoSize = true;
            this.Label103.Location = new System.Drawing.Point(180, 82);
            this.Label103.Name = "Label103";
            this.Label103.Size = new System.Drawing.Size(56, 13);
            this.Label103.TabIndex = 9;
            this.Label103.Text = "Clase API:";
            // 
            // txtClaseTransaccion
            // 
            this.txtClaseTransaccion.Location = new System.Drawing.Point(206, 36);
            this.txtClaseTransaccion.Name = "txtClaseTransaccion";
            this.txtClaseTransaccion.Size = new System.Drawing.Size(185, 20);
            this.txtClaseTransaccion.TabIndex = 3;
            this.txtClaseTransaccion.Text = "cTrans";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(8, 39);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(190, 13);
            this.Label4.TabIndex = 2;
            this.Label4.Text = "Nombre de la Clase de Transacciones:";
            // 
            // txtClaseConn
            // 
            this.txtClaseConn.Location = new System.Drawing.Point(206, 13);
            this.txtClaseConn.Name = "txtClaseConn";
            this.txtClaseConn.Size = new System.Drawing.Size(185, 20);
            this.txtClaseConn.TabIndex = 1;
            this.txtClaseConn.Text = "cConn";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(9, 16);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(171, 13);
            this.Label9.TabIndex = 0;
            this.Label9.Text = "Nombre de la Clase de Coneccion:";
            // 
            // GroupBox5
            // 
            this.GroupBox5.Controls.Add(this.txtNamespaceDocumentacion);
            this.GroupBox5.Controls.Add(this.chkReAlCrearDoc);
            this.GroupBox5.Controls.Add(this.chkReAlDataSet);
            this.GroupBox5.Controls.Add(this.txtNombreDataSet);
            this.GroupBox5.Controls.Add(this.TextBox2);
            this.GroupBox5.Controls.Add(this.txtPrefijoUnitTest);
            this.GroupBox5.Controls.Add(this.Label27);
            this.GroupBox5.Controls.Add(this.txtNamespaceUnitTest);
            this.GroupBox5.Controls.Add(this.chkReAlUnitTest);
            this.GroupBox5.Controls.Add(this.textbox10);
            this.GroupBox5.Controls.Add(this.txtPrefijoEntidades);
            this.GroupBox5.Controls.Add(this.Label16);
            this.GroupBox5.Controls.Add(this.TextBox6);
            this.GroupBox5.Controls.Add(this.txtPrefijoInterface);
            this.GroupBox5.Controls.Add(this.Label15);
            this.GroupBox5.Controls.Add(this.TextBox3);
            this.GroupBox5.Controls.Add(this.txtPrefijoModelo);
            this.GroupBox5.Controls.Add(this.Label14);
            this.GroupBox5.Controls.Add(this.txtNamespaceNegocios);
            this.GroupBox5.Controls.Add(this.txtNamespaceInterface);
            this.GroupBox5.Controls.Add(this.txtNamespaceEntidades);
            this.GroupBox5.Controls.Add(this.chkReAlReglaNegocios);
            this.GroupBox5.Controls.Add(this.chkReAlInterface);
            this.GroupBox5.Controls.Add(this.chkReAlEntidades);
            this.GroupBox5.Location = new System.Drawing.Point(453, 12);
            this.GroupBox5.Name = "GroupBox5";
            this.GroupBox5.Size = new System.Drawing.Size(543, 153);
            this.GroupBox5.TabIndex = 6;
            this.GroupBox5.TabStop = false;
            this.GroupBox5.Text = "Parametros";
            // 
            // txtNamespaceDocumentacion
            // 
            this.txtNamespaceDocumentacion.Location = new System.Drawing.Point(213, 106);
            this.txtNamespaceDocumentacion.Name = "txtNamespaceDocumentacion";
            this.txtNamespaceDocumentacion.Size = new System.Drawing.Size(327, 20);
            this.txtNamespaceDocumentacion.TabIndex = 21;
            this.txtNamespaceDocumentacion.Text = "https://bitbucket.org/ine2013/encuestaine/wiki/";
            // 
            // chkReAlCrearDoc
            // 
            this.chkReAlCrearDoc.AutoSize = true;
            this.chkReAlCrearDoc.Location = new System.Drawing.Point(7, 108);
            this.chkReAlCrearDoc.Name = "chkReAlCrearDoc";
            this.chkReAlCrearDoc.Size = new System.Drawing.Size(129, 17);
            this.chkReAlCrearDoc.TabIndex = 20;
            this.chkReAlCrearDoc.Text = "Crear Documentacion";
            this.chkReAlCrearDoc.UseVisualStyleBackColor = true;
            // 
            // chkReAlDataSet
            // 
            this.chkReAlDataSet.AutoSize = true;
            this.chkReAlDataSet.Location = new System.Drawing.Point(7, 130);
            this.chkReAlDataSet.Name = "chkReAlDataSet";
            this.chkReAlDataSet.Size = new System.Drawing.Size(93, 17);
            this.chkReAlDataSet.TabIndex = 22;
            this.chkReAlDataSet.Text = "Crear DataSet";
            this.chkReAlDataSet.UseVisualStyleBackColor = true;
            // 
            // txtNombreDataSet
            // 
            this.txtNombreDataSet.Location = new System.Drawing.Point(213, 128);
            this.txtNombreDataSet.Name = "txtNombreDataSet";
            this.txtNombreDataSet.Size = new System.Drawing.Size(113, 20);
            this.txtNombreDataSet.TabIndex = 23;
            this.txtNombreDataSet.Text = "miDataSet";
            // 
            // TextBox2
            // 
            this.TextBox2.Location = new System.Drawing.Point(423, 83);
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.ReadOnly = true;
            this.TextBox2.Size = new System.Drawing.Size(117, 20);
            this.TextBox2.TabIndex = 19;
            this.TextBox2.TabStop = false;
            this.TextBox2.Text = "[Nombre Clase]";
            // 
            // txtPrefijoUnitTest
            // 
            this.txtPrefijoUnitTest.Location = new System.Drawing.Point(376, 83);
            this.txtPrefijoUnitTest.Name = "txtPrefijoUnitTest";
            this.txtPrefijoUnitTest.Size = new System.Drawing.Size(47, 20);
            this.txtPrefijoUnitTest.TabIndex = 18;
            this.txtPrefijoUnitTest.Text = "ut";
            this.txtPrefijoUnitTest.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Label27
            // 
            this.Label27.AutoSize = true;
            this.Label27.Location = new System.Drawing.Point(331, 86);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(39, 13);
            this.Label27.TabIndex = 17;
            this.Label27.Text = "Prefijo:";
            // 
            // txtNamespaceUnitTest
            // 
            this.txtNamespaceUnitTest.Location = new System.Drawing.Point(213, 83);
            this.txtNamespaceUnitTest.Name = "txtNamespaceUnitTest";
            this.txtNamespaceUnitTest.Size = new System.Drawing.Size(113, 20);
            this.txtNamespaceUnitTest.TabIndex = 16;
            this.txtNamespaceUnitTest.Text = "Test";
            // 
            // chkReAlUnitTest
            // 
            this.chkReAlUnitTest.AutoSize = true;
            this.chkReAlUnitTest.Checked = true;
            this.chkReAlUnitTest.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkReAlUnitTest.Location = new System.Drawing.Point(7, 86);
            this.chkReAlUnitTest.Name = "chkReAlUnitTest";
            this.chkReAlUnitTest.Size = new System.Drawing.Size(99, 17);
            this.chkReAlUnitTest.TabIndex = 15;
            this.chkReAlUnitTest.Text = "Crear UnitTests";
            this.chkReAlUnitTest.UseVisualStyleBackColor = true;
            // 
            // textbox10
            // 
            this.textbox10.Location = new System.Drawing.Point(423, 16);
            this.textbox10.Name = "textbox10";
            this.textbox10.ReadOnly = true;
            this.textbox10.Size = new System.Drawing.Size(117, 20);
            this.textbox10.TabIndex = 4;
            this.textbox10.TabStop = false;
            this.textbox10.Text = "[Nombre Clase]";
            // 
            // txtPrefijoEntidades
            // 
            this.txtPrefijoEntidades.Location = new System.Drawing.Point(376, 16);
            this.txtPrefijoEntidades.Name = "txtPrefijoEntidades";
            this.txtPrefijoEntidades.Size = new System.Drawing.Size(47, 20);
            this.txtPrefijoEntidades.TabIndex = 3;
            this.txtPrefijoEntidades.Text = "ent";
            this.txtPrefijoEntidades.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Location = new System.Drawing.Point(331, 19);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(39, 13);
            this.Label16.TabIndex = 2;
            this.Label16.Text = "Prefijo:";
            // 
            // TextBox6
            // 
            this.TextBox6.Location = new System.Drawing.Point(423, 38);
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.ReadOnly = true;
            this.TextBox6.Size = new System.Drawing.Size(117, 20);
            this.TextBox6.TabIndex = 9;
            this.TextBox6.TabStop = false;
            this.TextBox6.Text = "[Nombre Interface]";
            // 
            // txtPrefijoInterface
            // 
            this.txtPrefijoInterface.Location = new System.Drawing.Point(376, 38);
            this.txtPrefijoInterface.Name = "txtPrefijoInterface";
            this.txtPrefijoInterface.Size = new System.Drawing.Size(47, 20);
            this.txtPrefijoInterface.TabIndex = 8;
            this.txtPrefijoInterface.Text = "in";
            this.txtPrefijoInterface.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Location = new System.Drawing.Point(331, 41);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(39, 13);
            this.Label15.TabIndex = 7;
            this.Label15.Text = "Prefijo:";
            // 
            // TextBox3
            // 
            this.TextBox3.Location = new System.Drawing.Point(423, 60);
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.ReadOnly = true;
            this.TextBox3.Size = new System.Drawing.Size(117, 20);
            this.TextBox3.TabIndex = 14;
            this.TextBox3.TabStop = false;
            this.TextBox3.Text = "[Nombre Clase]";
            // 
            // txtPrefijoModelo
            // 
            this.txtPrefijoModelo.Location = new System.Drawing.Point(376, 60);
            this.txtPrefijoModelo.Name = "txtPrefijoModelo";
            this.txtPrefijoModelo.Size = new System.Drawing.Size(47, 20);
            this.txtPrefijoModelo.TabIndex = 13;
            this.txtPrefijoModelo.Text = "rn";
            this.txtPrefijoModelo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(331, 63);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(39, 13);
            this.Label14.TabIndex = 12;
            this.Label14.Text = "Prefijo:";
            // 
            // txtNamespaceNegocios
            // 
            this.txtNamespaceNegocios.Location = new System.Drawing.Point(213, 60);
            this.txtNamespaceNegocios.Name = "txtNamespaceNegocios";
            this.txtNamespaceNegocios.Size = new System.Drawing.Size(113, 20);
            this.txtNamespaceNegocios.TabIndex = 11;
            this.txtNamespaceNegocios.Text = "Modelo";
            // 
            // txtNamespaceInterface
            // 
            this.txtNamespaceInterface.Location = new System.Drawing.Point(213, 37);
            this.txtNamespaceInterface.Name = "txtNamespaceInterface";
            this.txtNamespaceInterface.Size = new System.Drawing.Size(113, 20);
            this.txtNamespaceInterface.TabIndex = 6;
            this.txtNamespaceInterface.Text = "Interfase";
            // 
            // txtNamespaceEntidades
            // 
            this.txtNamespaceEntidades.Location = new System.Drawing.Point(213, 14);
            this.txtNamespaceEntidades.Name = "txtNamespaceEntidades";
            this.txtNamespaceEntidades.Size = new System.Drawing.Size(113, 20);
            this.txtNamespaceEntidades.TabIndex = 0;
            this.txtNamespaceEntidades.Text = "Entidades";
            // 
            // chkReAlReglaNegocios
            // 
            this.chkReAlReglaNegocios.AutoSize = true;
            this.chkReAlReglaNegocios.Checked = true;
            this.chkReAlReglaNegocios.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkReAlReglaNegocios.Location = new System.Drawing.Point(7, 63);
            this.chkReAlReglaNegocios.Name = "chkReAlReglaNegocios";
            this.chkReAlReglaNegocios.Size = new System.Drawing.Size(192, 17);
            this.chkReAlReglaNegocios.TabIndex = 10;
            this.chkReAlReglaNegocios.Text = "Crear capa de BUSSINESS LOGIC";
            this.chkReAlReglaNegocios.UseVisualStyleBackColor = true;
            // 
            // chkReAlInterface
            // 
            this.chkReAlInterface.AutoSize = true;
            this.chkReAlInterface.Checked = true;
            this.chkReAlInterface.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkReAlInterface.Location = new System.Drawing.Point(7, 40);
            this.chkReAlInterface.Name = "chkReAlInterface";
            this.chkReAlInterface.Size = new System.Drawing.Size(193, 17);
            this.chkReAlInterface.TabIndex = 5;
            this.chkReAlInterface.Text = "Crear capa de INTERFACE (B.L.L.)";
            this.chkReAlInterface.UseVisualStyleBackColor = true;
            // 
            // chkReAlEntidades
            // 
            this.chkReAlEntidades.AutoSize = true;
            this.chkReAlEntidades.Checked = true;
            this.chkReAlEntidades.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkReAlEntidades.Location = new System.Drawing.Point(7, 16);
            this.chkReAlEntidades.Name = "chkReAlEntidades";
            this.chkReAlEntidades.Size = new System.Drawing.Size(208, 17);
            this.chkReAlEntidades.TabIndex = 1;
            this.chkReAlEntidades.Text = "Crear capa de BUSSINESS OBJECTS";
            this.chkReAlEntidades.UseVisualStyleBackColor = true;
            // 
            // GroupBox7
            // 
            this.GroupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBox7.Controls.Add(this.cmbTipoColumnas);
            this.GroupBox7.Location = new System.Drawing.Point(282, 61);
            this.GroupBox7.Name = "GroupBox7";
            this.GroupBox7.Size = new System.Drawing.Size(165, 38);
            this.GroupBox7.TabIndex = 4;
            this.GroupBox7.TabStop = false;
            this.GroupBox7.Text = "Tipo de Columnas";
            // 
            // cmbTipoColumnas
            // 
            this.cmbTipoColumnas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoColumnas.FormattingEnabled = true;
            this.cmbTipoColumnas.Items.AddRange(new object[] {
            "Primera Mayuscula",
            "Minusculas",
            "Mayusculas",
            "camel Case",
            "Pascal Case"});
            this.cmbTipoColumnas.Location = new System.Drawing.Point(6, 12);
            this.cmbTipoColumnas.Name = "cmbTipoColumnas";
            this.cmbTipoColumnas.Size = new System.Drawing.Size(153, 21);
            this.cmbTipoColumnas.TabIndex = 0;
            // 
            // GroupBox23
            // 
            this.GroupBox23.Controls.Add(this.txtInfAutor);
            this.GroupBox23.Location = new System.Drawing.Point(282, 145);
            this.GroupBox23.Name = "GroupBox23";
            this.GroupBox23.Size = new System.Drawing.Size(165, 38);
            this.GroupBox23.TabIndex = 8;
            this.GroupBox23.TabStop = false;
            this.GroupBox23.Text = "Autor";
            // 
            // txtInfAutor
            // 
            this.txtInfAutor.Location = new System.Drawing.Point(6, 14);
            this.txtInfAutor.MaxLength = 15;
            this.txtInfAutor.Name = "txtInfAutor";
            this.txtInfAutor.Size = new System.Drawing.Size(153, 20);
            this.txtInfAutor.TabIndex = 0;
            // 
            // GroupBox19
            // 
            this.GroupBox19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBox19.Controls.Add(this.cmbAPIs);
            this.GroupBox19.Location = new System.Drawing.Point(282, 105);
            this.GroupBox19.Name = "GroupBox19";
            this.GroupBox19.Size = new System.Drawing.Size(165, 38);
            this.GroupBox19.TabIndex = 5;
            this.GroupBox19.TabStop = false;
            this.GroupBox19.Text = "Tipo APIs y Auditoria";
            // 
            // cmbAPIs
            // 
            this.cmbAPIs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAPIs.FormattingEnabled = true;
            this.cmbAPIs.Items.AddRange(new object[] {
            "UsuCreTabla",
            "UsuCre",
            "usu_cre_tabla",
            "usu_cre"});
            this.cmbAPIs.Location = new System.Drawing.Point(6, 13);
            this.cmbAPIs.Name = "cmbAPIs";
            this.cmbAPIs.Size = new System.Drawing.Size(153, 21);
            this.cmbAPIs.TabIndex = 0;
            // 
            // grpGroupBox8
            // 
            this.grpGroupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grpGroupBox8.Controls.Add(this.Label46);
            this.grpGroupBox8.Controls.Add(this.txtInfProyectoConn);
            this.grpGroupBox8.Controls.Add(this.Label28);
            this.grpGroupBox8.Controls.Add(this.txtInfProyectoUnitTest);
            this.grpGroupBox8.Controls.Add(this.Label12);
            this.grpGroupBox8.Controls.Add(this.txtInfProyectoDal);
            this.grpGroupBox8.Controls.Add(this.txtInfProyecto);
            this.grpGroupBox8.Controls.Add(this.Label8);
            this.grpGroupBox8.Controls.Add(this.txtInfProyectoClass);
            this.grpGroupBox8.Controls.Add(this.Label6);
            this.grpGroupBox8.Location = new System.Drawing.Point(12, 61);
            this.grpGroupBox8.Name = "grpGroupBox8";
            this.grpGroupBox8.Size = new System.Drawing.Size(264, 128);
            this.grpGroupBox8.TabIndex = 7;
            this.grpGroupBox8.TabStop = false;
            this.grpGroupBox8.Text = "Proyectos";
            // 
            // Label46
            // 
            this.Label46.AutoSize = true;
            this.Label46.Location = new System.Drawing.Point(8, 82);
            this.Label46.Name = "Label46";
            this.Label46.Size = new System.Drawing.Size(83, 13);
            this.Label46.TabIndex = 6;
            this.Label46.Text = "Proyecto CONN";
            // 
            // txtInfProyectoConn
            // 
            this.txtInfProyectoConn.Location = new System.Drawing.Point(97, 79);
            this.txtInfProyectoConn.MaxLength = 250;
            this.txtInfProyectoConn.Name = "txtInfProyectoConn";
            this.txtInfProyectoConn.Size = new System.Drawing.Size(161, 20);
            this.txtInfProyectoConn.TabIndex = 7;
            // 
            // Label28
            // 
            this.Label28.AutoSize = true;
            this.Label28.Location = new System.Drawing.Point(8, 103);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(67, 13);
            this.Label28.TabIndex = 8;
            this.Label28.Text = "Proyecto UT";
            // 
            // txtInfProyectoUnitTest
            // 
            this.txtInfProyectoUnitTest.Location = new System.Drawing.Point(97, 100);
            this.txtInfProyectoUnitTest.MaxLength = 250;
            this.txtInfProyectoUnitTest.Name = "txtInfProyectoUnitTest";
            this.txtInfProyectoUnitTest.Size = new System.Drawing.Size(161, 20);
            this.txtInfProyectoUnitTest.TabIndex = 9;
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(8, 61);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(73, 13);
            this.Label12.TabIndex = 4;
            this.Label12.Text = "Proyecto DAL";
            // 
            // txtInfProyectoDal
            // 
            this.txtInfProyectoDal.Location = new System.Drawing.Point(97, 58);
            this.txtInfProyectoDal.MaxLength = 250;
            this.txtInfProyectoDal.Name = "txtInfProyectoDal";
            this.txtInfProyectoDal.Size = new System.Drawing.Size(161, 20);
            this.txtInfProyectoDal.TabIndex = 5;
            // 
            // txtInfProyecto
            // 
            this.txtInfProyecto.Location = new System.Drawing.Point(97, 15);
            this.txtInfProyecto.MaxLength = 250;
            this.txtInfProyecto.Name = "txtInfProyecto";
            this.txtInfProyecto.Size = new System.Drawing.Size(161, 20);
            this.txtInfProyecto.TabIndex = 1;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(7, 18);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(74, 13);
            this.Label8.TabIndex = 0;
            this.Label8.Text = "Proyecto App:";
            // 
            // txtInfProyectoClass
            // 
            this.txtInfProyectoClass.Location = new System.Drawing.Point(97, 36);
            this.txtInfProyectoClass.MaxLength = 250;
            this.txtInfProyectoClass.Name = "txtInfProyectoClass";
            this.txtInfProyectoClass.Size = new System.Drawing.Size(161, 20);
            this.txtInfProyectoClass.TabIndex = 3;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(7, 39);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(67, 13);
            this.Label6.TabIndex = 2;
            this.Label6.Text = "Proyecto BO";
            // 
            // GroupBox4
            // 
            this.GroupBox4.Controls.Add(this.rdbSpIfExists);
            this.GroupBox4.Controls.Add(this.rdbSpAlter);
            this.GroupBox4.Controls.Add(this.rdbSpCreate);
            this.GroupBox4.Controls.Add(this.grpSpIns);
            this.GroupBox4.Controls.Add(this.grpSpUpd);
            this.GroupBox4.Controls.Add(this.txtSPsig);
            this.GroupBox4.Controls.Add(this.TextBox4);
            this.GroupBox4.Controls.Add(this.txtSPprev);
            this.GroupBox4.Controls.Add(this.Label3);
            this.GroupBox4.Location = new System.Drawing.Point(453, 171);
            this.GroupBox4.Name = "GroupBox4";
            this.GroupBox4.Size = new System.Drawing.Size(346, 180);
            this.GroupBox4.TabIndex = 9;
            this.GroupBox4.TabStop = false;
            this.GroupBox4.Text = "Datos SP";
            // 
            // rdbSpIfExists
            // 
            this.rdbSpIfExists.AutoSize = true;
            this.rdbSpIfExists.Checked = true;
            this.rdbSpIfExists.Location = new System.Drawing.Point(217, 63);
            this.rdbSpIfExists.Name = "rdbSpIfExists";
            this.rdbSpIfExists.Size = new System.Drawing.Size(109, 17);
            this.rdbSpIfExists.TabIndex = 6;
            this.rdbSpIfExists.TabStop = true;
            this.rdbSpIfExists.Text = "IF EXISTS DROP";
            this.rdbSpIfExists.UseVisualStyleBackColor = true;
            // 
            // rdbSpAlter
            // 
            this.rdbSpAlter.AutoSize = true;
            this.rdbSpAlter.Location = new System.Drawing.Point(118, 63);
            this.rdbSpAlter.Name = "rdbSpAlter";
            this.rdbSpAlter.Size = new System.Drawing.Size(93, 17);
            this.rdbSpAlter.TabIndex = 5;
            this.rdbSpAlter.Text = "ALTER PROC";
            this.rdbSpAlter.UseVisualStyleBackColor = true;
            // 
            // rdbSpCreate
            // 
            this.rdbSpCreate.AutoSize = true;
            this.rdbSpCreate.Location = new System.Drawing.Point(11, 63);
            this.rdbSpCreate.Name = "rdbSpCreate";
            this.rdbSpCreate.Size = new System.Drawing.Size(101, 17);
            this.rdbSpCreate.TabIndex = 4;
            this.rdbSpCreate.Text = "CREATE PROC";
            this.rdbSpCreate.UseVisualStyleBackColor = true;
            // 
            // grpSpIns
            // 
            this.grpSpIns.Controls.Add(this.chkSPInsIdentity);
            this.grpSpIns.Controls.Add(this.chkSPInsFecCreGetDate);
            this.grpSpIns.Controls.Add(this.chkSPInsApiEstado);
            this.grpSpIns.Controls.Add(this.chkSPInsFecCre);
            this.grpSpIns.Controls.Add(this.chkSPInsUsuCre);
            this.grpSpIns.Location = new System.Drawing.Point(6, 86);
            this.grpSpIns.Name = "grpSpIns";
            this.grpSpIns.Size = new System.Drawing.Size(164, 86);
            this.grpSpIns.TabIndex = 7;
            this.grpSpIns.TabStop = false;
            this.grpSpIns.Text = "Parametros Insert";
            // 
            // chkSPInsIdentity
            // 
            this.chkSPInsIdentity.AutoSize = true;
            this.chkSPInsIdentity.Location = new System.Drawing.Point(6, 67);
            this.chkSPInsIdentity.Name = "chkSPInsIdentity";
            this.chkSPInsIdentity.Size = new System.Drawing.Size(147, 17);
            this.chkSPInsIdentity.TabIndex = 4;
            this.chkSPInsIdentity.Text = "Primera PK autogenerada";
            this.chkSPInsIdentity.UseVisualStyleBackColor = true;
            // 
            // chkSPInsFecCreGetDate
            // 
            this.chkSPInsFecCreGetDate.AutoSize = true;
            this.chkSPInsFecCreGetDate.Checked = true;
            this.chkSPInsFecCreGetDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSPInsFecCreGetDate.Location = new System.Drawing.Point(6, 35);
            this.chkSPInsFecCreGetDate.Name = "chkSPInsFecCreGetDate";
            this.chkSPInsFecCreGetDate.Size = new System.Drawing.Size(128, 17);
            this.chkSPInsFecCreGetDate.TabIndex = 2;
            this.chkSPInsFecCreGetDate.Text = "FecCre es GETDATE";
            this.chkSPInsFecCreGetDate.UseVisualStyleBackColor = true;
            // 
            // chkSPInsApiEstado
            // 
            this.chkSPInsApiEstado.AutoSize = true;
            this.chkSPInsApiEstado.Checked = true;
            this.chkSPInsApiEstado.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSPInsApiEstado.Location = new System.Drawing.Point(6, 51);
            this.chkSPInsApiEstado.Name = "chkSPInsApiEstado";
            this.chkSPInsApiEstado.Size = new System.Drawing.Size(158, 17);
            this.chkSPInsApiEstado.TabIndex = 3;
            this.chkSPInsApiEstado.Text = "Estado Inicial ELABORADO";
            this.chkSPInsApiEstado.UseVisualStyleBackColor = true;
            // 
            // chkSPInsFecCre
            // 
            this.chkSPInsFecCre.AutoSize = true;
            this.chkSPInsFecCre.Location = new System.Drawing.Point(91, 19);
            this.chkSPInsFecCre.Name = "chkSPInsFecCre";
            this.chkSPInsFecCre.Size = new System.Drawing.Size(60, 17);
            this.chkSPInsFecCre.TabIndex = 1;
            this.chkSPInsFecCre.Text = "FecCre";
            this.chkSPInsFecCre.UseVisualStyleBackColor = true;
            // 
            // chkSPInsUsuCre
            // 
            this.chkSPInsUsuCre.AutoSize = true;
            this.chkSPInsUsuCre.Checked = true;
            this.chkSPInsUsuCre.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSPInsUsuCre.Location = new System.Drawing.Point(6, 19);
            this.chkSPInsUsuCre.Name = "chkSPInsUsuCre";
            this.chkSPInsUsuCre.Size = new System.Drawing.Size(61, 17);
            this.chkSPInsUsuCre.TabIndex = 0;
            this.chkSPInsUsuCre.Text = "UsuCre";
            this.chkSPInsUsuCre.UseVisualStyleBackColor = true;
            // 
            // grpSpUpd
            // 
            this.grpSpUpd.Controls.Add(this.chkSPUpdFecModGetDate);
            this.grpSpUpd.Controls.Add(this.chkSPUpdFecMod);
            this.grpSpUpd.Controls.Add(this.chkSPUpdUsuMod);
            this.grpSpUpd.Location = new System.Drawing.Point(176, 86);
            this.grpSpUpd.Name = "grpSpUpd";
            this.grpSpUpd.Size = new System.Drawing.Size(160, 56);
            this.grpSpUpd.TabIndex = 8;
            this.grpSpUpd.TabStop = false;
            this.grpSpUpd.Text = "Parametros Update";
            // 
            // chkSPUpdFecModGetDate
            // 
            this.chkSPUpdFecModGetDate.AutoSize = true;
            this.chkSPUpdFecModGetDate.Checked = true;
            this.chkSPUpdFecModGetDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSPUpdFecModGetDate.Location = new System.Drawing.Point(6, 35);
            this.chkSPUpdFecModGetDate.Name = "chkSPUpdFecModGetDate";
            this.chkSPUpdFecModGetDate.Size = new System.Drawing.Size(133, 17);
            this.chkSPUpdFecModGetDate.TabIndex = 2;
            this.chkSPUpdFecModGetDate.Text = "FecMod es GETDATE";
            this.chkSPUpdFecModGetDate.UseVisualStyleBackColor = true;
            // 
            // chkSPUpdFecMod
            // 
            this.chkSPUpdFecMod.AutoSize = true;
            this.chkSPUpdFecMod.Location = new System.Drawing.Point(88, 19);
            this.chkSPUpdFecMod.Name = "chkSPUpdFecMod";
            this.chkSPUpdFecMod.Size = new System.Drawing.Size(65, 17);
            this.chkSPUpdFecMod.TabIndex = 1;
            this.chkSPUpdFecMod.Text = "FecMod";
            this.chkSPUpdFecMod.UseVisualStyleBackColor = true;
            // 
            // chkSPUpdUsuMod
            // 
            this.chkSPUpdUsuMod.AutoSize = true;
            this.chkSPUpdUsuMod.Checked = true;
            this.chkSPUpdUsuMod.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSPUpdUsuMod.Location = new System.Drawing.Point(6, 19);
            this.chkSPUpdUsuMod.Name = "chkSPUpdUsuMod";
            this.chkSPUpdUsuMod.Size = new System.Drawing.Size(66, 17);
            this.chkSPUpdUsuMod.TabIndex = 0;
            this.chkSPUpdUsuMod.Text = "UsuMod";
            this.chkSPUpdUsuMod.UseVisualStyleBackColor = true;
            // 
            // txtSPsig
            // 
            this.txtSPsig.Location = new System.Drawing.Point(173, 32);
            this.txtSPsig.Name = "txtSPsig";
            this.txtSPsig.Size = new System.Drawing.Size(72, 20);
            this.txtSPsig.TabIndex = 3;
            // 
            // TextBox4
            // 
            this.TextBox4.Location = new System.Drawing.Point(85, 32);
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.ReadOnly = true;
            this.TextBox4.Size = new System.Drawing.Size(82, 20);
            this.TextBox4.TabIndex = 2;
            this.TextBox4.TabStop = false;
            this.TextBox4.Text = "[Nombre Tabla]";
            // 
            // txtSPprev
            // 
            this.txtSPprev.Location = new System.Drawing.Point(7, 32);
            this.txtSPprev.Name = "txtSPprev";
            this.txtSPprev.Size = new System.Drawing.Size(72, 20);
            this.txtSPprev.TabIndex = 1;
            this.txtSPprev.Text = "Sp";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(6, 16);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(88, 13);
            this.Label3.TabIndex = 0;
            this.Label3.Text = "Formato Nombre:";
            // 
            // txtNombrePlantilla
            // 
            this.txtNombrePlantilla.Location = new System.Drawing.Point(104, 10);
            this.txtNombrePlantilla.Name = "txtNombrePlantilla";
            this.txtNombrePlantilla.Size = new System.Drawing.Size(343, 20);
            this.txtNombrePlantilla.TabIndex = 1;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(12, 13);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(86, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Nombre Plantilla:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtServicio);
            this.groupBox1.Controls.Add(this.lblServPuerto);
            this.groupBox1.Controls.Add(this.cmbDbms);
            this.groupBox1.Controls.Add(this.chkSegIntegrada);
            this.groupBox1.Controls.Add(this.lblPass);
            this.groupBox1.Controls.Add(this.lblUsuario);
            this.groupBox1.Controls.Add(this.txtPass);
            this.groupBox1.Controls.Add(this.txtUser);
            this.groupBox1.Controls.Add(this.txtServer);
            this.groupBox1.Controls.Add(this.Label5);
            this.groupBox1.Controls.Add(this.lblServidor);
            this.groupBox1.Location = new System.Drawing.Point(805, 171);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(191, 180);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Base de Datos";
            // 
            // txtServicio
            // 
            this.txtServicio.Location = new System.Drawing.Point(63, 85);
            this.txtServicio.Name = "txtServicio";
            this.txtServicio.Size = new System.Drawing.Size(123, 20);
            this.txtServicio.TabIndex = 6;
            // 
            // lblServPuerto
            // 
            this.lblServPuerto.AutoSize = true;
            this.lblServPuerto.Location = new System.Drawing.Point(8, 88);
            this.lblServPuerto.Name = "lblServPuerto";
            this.lblServPuerto.Size = new System.Drawing.Size(48, 13);
            this.lblServPuerto.TabIndex = 5;
            this.lblServPuerto.Text = "Servicio:";
            // 
            // cmbDbms
            // 
            this.cmbDbms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDbms.FormattingEnabled = true;
            this.cmbDbms.Items.AddRange(new object[] {
            "SQLServer",
            "Oracle",
            "PostgreSQL",
            "MySql",
            "SQLServer2000",
            "SQLite",
            "FireBird"});
            this.cmbDbms.Location = new System.Drawing.Point(63, 22);
            this.cmbDbms.Name = "cmbDbms";
            this.cmbDbms.Size = new System.Drawing.Size(122, 21);
            this.cmbDbms.TabIndex = 1;
            this.cmbDbms.SelectedIndexChanged += new System.EventHandler(this.cmbDbms_SelectedIndexChanged);
            // 
            // chkSegIntegrada
            // 
            this.chkSegIntegrada.AutoSize = true;
            this.chkSegIntegrada.Location = new System.Drawing.Point(63, 67);
            this.chkSegIntegrada.Name = "chkSegIntegrada";
            this.chkSegIntegrada.Size = new System.Drawing.Size(122, 17);
            this.chkSegIntegrada.TabIndex = 4;
            this.chkSegIntegrada.Text = "Seguridad Integrada";
            this.chkSegIntegrada.UseVisualStyleBackColor = true;
            // 
            // lblPass
            // 
            this.lblPass.AutoSize = true;
            this.lblPass.Location = new System.Drawing.Point(1, 130);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(56, 13);
            this.lblPass.TabIndex = 9;
            this.lblPass.Text = "Password:";
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Location = new System.Drawing.Point(11, 109);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(46, 13);
            this.lblUsuario.TabIndex = 7;
            this.lblUsuario.Text = "Usuario:";
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(63, 127);
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '*';
            this.txtPass.Size = new System.Drawing.Size(123, 20);
            this.txtPass.TabIndex = 10;
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(63, 106);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(123, 20);
            this.txtUser.TabIndex = 8;
            this.txtUser.Text = "sa";
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(63, 45);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(123, 20);
            this.txtServer.TabIndex = 3;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(11, 25);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(41, 13);
            this.Label5.TabIndex = 0;
            this.Label5.Text = "DBMS:";
            // 
            // lblServidor
            // 
            this.lblServidor.AutoSize = true;
            this.lblServidor.Location = new System.Drawing.Point(8, 48);
            this.lblServidor.Name = "lblServidor";
            this.lblServidor.Size = new System.Drawing.Size(49, 13);
            this.lblServidor.TabIndex = 2;
            this.lblServidor.Text = "Servidor:";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.Location = new System.Drawing.Point(787, 412);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(95, 37);
            this.btnCancelar.TabIndex = 12;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.Location = new System.Drawing.Point(888, 412);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(108, 37);
            this.btnGuardar.TabIndex = 13;
            this.btnGuardar.Text = "&Guardar Plantilla";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // FPlantillasDoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 461);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtNombrePlantilla);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.GroupBox4);
            this.Controls.Add(this.cmbEntorno);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.grpParametrosAdicionales);
            this.Controls.Add(this.GroupBox5);
            this.Controls.Add(this.GroupBox7);
            this.Controls.Add(this.GroupBox23);
            this.Controls.Add(this.GroupBox19);
            this.Controls.Add(this.grpGroupBox8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FPlantillasDoc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Detalle de Plantilla";
            this.Load += new System.EventHandler(this.FPlantillasDoc_Load);
            this.grpParametrosAdicionales.ResumeLayout(false);
            this.grpParametrosAdicionales.PerformLayout();
            this.GroupBox5.ResumeLayout(false);
            this.GroupBox5.PerformLayout();
            this.GroupBox7.ResumeLayout(false);
            this.GroupBox23.ResumeLayout(false);
            this.GroupBox23.PerformLayout();
            this.GroupBox19.ResumeLayout(false);
            this.grpGroupBox8.ResumeLayout(false);
            this.grpGroupBox8.PerformLayout();
            this.GroupBox4.ResumeLayout(false);
            this.GroupBox4.PerformLayout();
            this.grpSpIns.ResumeLayout(false);
            this.grpSpIns.PerformLayout();
            this.grpSpUpd.ResumeLayout(false);
            this.grpSpUpd.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ComboBox cmbEntorno;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.GroupBox grpParametrosAdicionales;
        public System.Windows.Forms.CheckBox chkClassParcial;
        public System.Windows.Forms.CheckBox chkEntBaseClass;
        public System.Windows.Forms.CheckBox chkPropertyEvents;
        public System.Windows.Forms.CheckBox chkEntidadesEncriptacion;
        public System.Windows.Forms.CheckBox chkDataAnnotations;
        internal System.Windows.Forms.CheckBox chkUsarSpsIdentity;
        internal System.Windows.Forms.CheckBox chkUsarSpsCheck;
        internal System.Windows.Forms.TextBox txtClaseParametros;
        internal System.Windows.Forms.TextBox txtClaseParametrosListadoSp;
        internal System.Windows.Forms.TextBox txtClaseParametrosBaseClass;
        internal System.Windows.Forms.TextBox txtClaseParametrosClaseApi;
        public System.Windows.Forms.CheckBox chkParametroPa;
        internal System.Windows.Forms.CheckBox chkUsarSpsSelect;
        internal System.Windows.Forms.CheckBox chkUsarSps;
        internal System.Windows.Forms.CheckBox chkUsarSchemaInModelo;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label101;
        internal System.Windows.Forms.Label Label102;
        internal System.Windows.Forms.Label Label103;
        internal System.Windows.Forms.TextBox txtClaseTransaccion;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox txtClaseConn;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.GroupBox GroupBox5;
        internal System.Windows.Forms.TextBox txtNamespaceDocumentacion;
        internal System.Windows.Forms.CheckBox chkReAlCrearDoc;
        internal System.Windows.Forms.CheckBox chkReAlDataSet;
        internal System.Windows.Forms.TextBox txtNombreDataSet;
        internal System.Windows.Forms.TextBox TextBox2;
        internal System.Windows.Forms.TextBox txtPrefijoUnitTest;
        internal System.Windows.Forms.Label Label27;
        internal System.Windows.Forms.TextBox txtNamespaceUnitTest;
        internal System.Windows.Forms.CheckBox chkReAlUnitTest;
        internal System.Windows.Forms.TextBox textbox10;
        internal System.Windows.Forms.TextBox txtPrefijoEntidades;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.TextBox TextBox6;
        internal System.Windows.Forms.TextBox txtPrefijoInterface;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.TextBox TextBox3;
        internal System.Windows.Forms.TextBox txtPrefijoModelo;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.TextBox txtNamespaceNegocios;
        internal System.Windows.Forms.TextBox txtNamespaceInterface;
        internal System.Windows.Forms.TextBox txtNamespaceEntidades;
        internal System.Windows.Forms.CheckBox chkReAlReglaNegocios;
        internal System.Windows.Forms.CheckBox chkReAlInterface;
        internal System.Windows.Forms.CheckBox chkReAlEntidades;
        internal System.Windows.Forms.GroupBox GroupBox7;
        internal System.Windows.Forms.ComboBox cmbTipoColumnas;
        internal System.Windows.Forms.GroupBox GroupBox23;
        internal System.Windows.Forms.TextBox txtInfAutor;
        internal System.Windows.Forms.GroupBox GroupBox19;
        public System.Windows.Forms.ComboBox cmbAPIs;
        internal System.Windows.Forms.GroupBox grpGroupBox8;
        internal System.Windows.Forms.Label Label46;
        public System.Windows.Forms.TextBox txtInfProyectoConn;
        internal System.Windows.Forms.Label Label28;
        public System.Windows.Forms.TextBox txtInfProyectoUnitTest;
        internal System.Windows.Forms.Label Label12;
        public System.Windows.Forms.TextBox txtInfProyectoDal;
        internal System.Windows.Forms.TextBox txtInfProyecto;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox txtInfProyectoClass;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.GroupBox GroupBox4;
        internal System.Windows.Forms.RadioButton rdbSpIfExists;
        internal System.Windows.Forms.RadioButton rdbSpAlter;
        internal System.Windows.Forms.RadioButton rdbSpCreate;
        internal System.Windows.Forms.GroupBox grpSpIns;
        internal System.Windows.Forms.CheckBox chkSPInsIdentity;
        internal System.Windows.Forms.CheckBox chkSPInsFecCreGetDate;
        internal System.Windows.Forms.CheckBox chkSPInsApiEstado;
        internal System.Windows.Forms.CheckBox chkSPInsFecCre;
        internal System.Windows.Forms.CheckBox chkSPInsUsuCre;
        internal System.Windows.Forms.GroupBox grpSpUpd;
        internal System.Windows.Forms.CheckBox chkSPUpdFecModGetDate;
        internal System.Windows.Forms.CheckBox chkSPUpdFecMod;
        internal System.Windows.Forms.CheckBox chkSPUpdUsuMod;
        internal System.Windows.Forms.TextBox txtSPsig;
        internal System.Windows.Forms.TextBox TextBox4;
        internal System.Windows.Forms.TextBox txtSPprev;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox txtNombrePlantilla;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtServicio;
        private System.Windows.Forms.Label lblServPuerto;
        internal System.Windows.Forms.ComboBox cmbDbms;
        internal System.Windows.Forms.CheckBox chkSegIntegrada;
        private System.Windows.Forms.Label lblPass;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Label Label5;
        private System.Windows.Forms.Label lblServidor;
        internal System.Windows.Forms.Button btnCancelar;
        internal System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.TextBox txtClaseParametrosClaseApiObject;
        internal System.Windows.Forms.Label label7;
    }
}