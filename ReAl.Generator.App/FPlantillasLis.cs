﻿using ReAl.Class.Entidades;
using ReAl.Class.Modelo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace ReAl.Generator.App
{
    public partial class FPlantillasLis : Form
    {
        public FPlantillasLis()
        {
            InitializeComponent();
        }

        private void FPlantillasLis_Load(object sender, EventArgs e)
        {
            CargarListadoPlantilla();
        }

        private void CargarListadoPlantilla()
        {
            try
            {
                rnClaPlantilla rn = new rnClaPlantilla();
                DataTable dt = rn.CargarDataTable();

                dtgPlantillas.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnNuevaPlantilla_Click(object sender, EventArgs e)
        {
            FPlantillasDoc frm = new FPlantillasDoc();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                CargarListadoPlantilla();
            }
        }

        private void btnCopiarPlantilla_Click(object sender, EventArgs e)
        {
            if (dtgPlantillas.RowCount == 0)
                return;
            try
            {
                string CodPlantilla = this.dtgPlantillas[entClaPlantilla.Fields.idPlantilla.ToString(), dtgPlantillas.SelectedRows[0].Index].Value.ToString();

                rnClaPlantilla rn = new rnClaPlantilla();
                rnClaDetalle rnDet = new rnClaDetalle();

                //Copiamos el Padre
                entClaPlantilla obj = rn.ObtenerObjeto(int.Parse(CodPlantilla));
                obj.descPlantilla = "Copia de " + obj.descPlantilla;
                rn.Insert(obj);
                string idPlantilla = rn.FuncionesMax(entClaPlantilla.Fields.idPlantilla, entClaPlantilla.Fields.descPlantilla, "'" + obj.descPlantilla + "'").ToString();

                //Copiamos el detalle
                List<entClaDetalle> lstDet = rnDet.ObtenerLista(entClaDetalle.Fields.idPlantilla, obj.idPlantilla);
                foreach (entClaDetalle miDet in lstDet)
                {
                    miDet.idPlantilla = int.Parse(idPlantilla);
                    rnDet.Insert(miDet);
                }
                CargarListadoPlantilla();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnImportarPlantillas_Click(object sender, EventArgs e)
        {
            string fileOrigen = "";
            string fileDestino = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\ReAlProjects.db";

            //Obtenemos el archivo Backup
            OpenFileDialog fileBrowserDialog1 = new OpenFileDialog();
            fileBrowserDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            fileBrowserDialog1.Filter = "All files (*.*)|*.*|All files (*.*)|*.*";
            if ((fileBrowserDialog1.ShowDialog() == DialogResult.OK))
            {
                fileOrigen = fileBrowserDialog1.FileName;

                //Primero borramos la BD original
                if (File.Exists(fileDestino))
                {
                    File.Delete(fileDestino);
                }

                //Copiamos el backup
                File.Copy(fileOrigen, fileDestino);
                if (File.Exists(fileDestino))
                {
                    MessageBox.Show(this, "Se ha restaurado la BD de plantillas satisfactoriamente", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    CargarListadoPlantilla();
                }
                else
                {
                    MessageBox.Show(this, "No se ha podido restaurar la copia de la BD de plantillas", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void btnEliminarPlantillas_Click(object sender, EventArgs e)
        {
            if (dtgPlantillas.RowCount == 0)
                return;
            try
            {
                if (MessageBox.Show(this, "¿Está seguro que desea ELIMINAR la plantilla seleccionada?",
                        "Eliminar Plantilla", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    string CodPlantilla = this
                        .dtgPlantillas[entClaPlantilla.Fields.idPlantilla.ToString(),
                            dtgPlantillas.SelectedRows[0].Index].Value.ToString();

                    rnClaPlantilla rn = new rnClaPlantilla();
                    rnClaDetalle rnDet = new rnClaDetalle();

                    //Eliminamos los hijos
                    ArrayList arrColWhere = new ArrayList();
                    arrColWhere.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                    ArrayList arrValWhere = new ArrayList();
                    arrValWhere.Add(CodPlantilla);
                    rnDet.Delete(arrColWhere, arrValWhere);
                    rn.Delete(arrColWhere, arrValWhere);

                    MessageBox.Show(this, "Se ha eliminado la plantillas satisfactoriamente", "Exito",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                    CargarListadoPlantilla();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void dtgPlantillas_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (dtgPlantillas.RowCount == 0)
                return;
            try
            {
                string CodPlantilla = this
                    .dtgPlantillas[entClaPlantilla.Fields.idPlantilla.ToString(),
                        dtgPlantillas.SelectedRows[0].Index].Value.ToString();

                rnClaPlantilla rn = new rnClaPlantilla();
                entClaPlantilla obj = rn.ObtenerObjeto(int.Parse(CodPlantilla));

                FPlantillasDoc frm = new FPlantillasDoc(obj);
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    CargarListadoPlantilla();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}