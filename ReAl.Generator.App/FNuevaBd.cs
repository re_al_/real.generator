﻿using System;
using System.Windows.Forms;

namespace ReAl.Generator.App
{
    public partial class FNuevaBd : Form
    {
        public string strNombre = "";
        public FNuevaBd()
        {
            InitializeComponent();
        }

        private void FNuevaBd_Load(object sender, EventArgs e)
        {

        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
