﻿using FirebirdSql.Data.FirebirdClient;
using MySqlConnector;
using Npgsql;
using ReAl.Generator.App.AppClass;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Data.SQLite;

namespace ReAl.Generator.App.AppCore
{
    public class CConeccion
    {
        public OleDbConnection CnSql;
        public OracleConnection CnOra;
        public NpgsqlConnection CnPg;
        public MySqlConnection CnMy;
        public FbConnection CnFb;
        public SQLiteConnection CnLite;

        public CConeccion()
        {
            string sCon = null;

            switch (Principal.DBMS)
            {
                case TipoBD.MsAccess:
                    sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Inetpub\\wwwroot\\RENACIMIENTO32\\Renacimiento.mdb;Persist Security Info=True";

                    break;

                case TipoBD.SQLServer:
                    if (Principal.SegIntegrada)
                    {
                        sCon = "Provider=SQLOLEDB.1; Data Source=" + Principal.Servidor + ";Integrated Security=SSPI; Initial Catalog=" + Principal.Catalogo + ";Trusted_Connection=Yes";
                    }
                    else
                    {
                        sCon = "User ID=" + Principal.UsuarioBD +
                               ";Tag with column collation when possible=False;Data Source=" + Principal.Servidor +
                               ";Password=\"" + Principal.PasswordBD + "\";Initial Catalog=" + Principal.Catalogo +
                               ";Use Procedure for Prepare=1;Auto Transla" +
                               "te=True;Persist Security Info=True;Provider=\"SQLOLEDB.1\";Workstation ID=(local);" +
                               "Use Encryption for Data=False;Packet Size=4096;Connect Timeout=" +
                               Principal.ConnectTimeOut;
                    }
                    CnSql = new OleDbConnection(sCon);

                    break;

                case TipoBD.SQLServer2000:
                    if (Principal.SegIntegrada)
                    {
                        sCon = "Provider=SQLOLEDB.1; Data Source=" + Principal.Servidor +
                               ";Integrated Security=SSPI; Initial Catalog=" + Principal.Catalogo +
                               ";Trusted_Connection=Yes";
                    }
                    else
                    {
                        sCon = "User ID=" + Principal.UsuarioBD +
                               ";Tag with column collation when possible=False;Data Source=" + Principal.Servidor +
                               ";Password=\"" + Principal.PasswordBD + "\";Initial Catalog=" + Principal.Catalogo +
                               ";Use Procedure for Prepare=1;Auto Translate=True;Persist Security Info=True;Provider=\"SQLOLEDB.1\";Workstation ID=(local);" +
                               "Use Encryption for Data=False;Packet Size=4096;Connect Timeout=" +
                               Principal.ConnectTimeOut;
                    }
                    CnSql = new OleDbConnection(sCon);

                    break;

                case TipoBD.Oracle:
                    sCon = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" +
                           Principal.Servidor + ")(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" +
                           Principal.Servicio + ")));User Id=" + Principal.UsuarioBD + ";Password=" +
                           Principal.PasswordBD + ";";
                    CnOra = new OracleConnection(sCon);

                    break;

                case TipoBD.PostgreSQL:
                    sCon = "Server=" + Principal.Servidor + ";Port=" + Principal.Puerto + ";Database=" +
                           Principal.Catalogo + ";User ID=" + Principal.UsuarioBD + ";Password=" +
                           Principal.PasswordBD + ";Pooling=false;CommandTimeout=9000000 ";
                    CnPg = new NpgsqlConnection(sCon);

                    break;

                case TipoBD.MySql:
                    if (string.IsNullOrEmpty(Principal.Catalogo))
                    {
                        sCon = "server=" + Principal.Servidor +
                               ";database=information_schema" +
                               ";uid=" + Principal.UsuarioBD + 
                               ";pwd=" + Principal.PasswordBD + "";
                    } 
                    else
                    {
                        sCon = "server=" + Principal.Servidor + ";database=" +
                               Principal.Catalogo + ";uid=" + Principal.UsuarioBD + ";pwd=" +
                               Principal.PasswordBD + "";

                    }
                    CnMy = new MySqlConnection(sCon);

                    break;

                case TipoBD.SQLite:
                    sCon = "Data Source=" + Principal.FBFile + ";Pooling=true;FailIfMissing=false";
                    CnLite = new SQLiteConnection(sCon);

                    break;

                case TipoBD.FireBird:
                    sCon = "User ID=" + Principal.UsuarioBD + ";Password=" + Principal.PasswordBD + ";Database=" + Principal.FBFile + ";DataSource=localhost;Charset=NONE;";
                    CnFb = new FbConnection(sCon);
                    break;
            }
        }
    }
}