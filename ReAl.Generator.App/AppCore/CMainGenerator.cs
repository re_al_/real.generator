﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppDocumentation;
using ReAl.Generator.App.AppGenerators;
using ReAlFind_Control;
using System;
using System.Data;
using System.IO;
using System.Text;

namespace ReAl.Generator.App.AppCore
{
    public class CMainGenerator
    {
        public void ReAlClass(FPrincipal formulario)
        {
            string Texto = "";
            string TextoTotal = "";
            DataTable dtColumns = new DataTable();

            string FC = DateTime.Now.ToString("dd/MM/yyyy");
            string Creador = (string.IsNullOrEmpty(formulario.txtInfAutor.Text) ? "Automatico     " : formulario.txtInfAutor.Text);
            Creador = Creador.PadRight(15);

            string PathDesktop = null;
            if (formulario.chkMoverResultados.Checked)
                PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoUnitTest.Text;
            else
                PathDesktop = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoUnitTest.Text;

            formulario.strCabeceraPlantillaCodigo = "#region \r\n" +
                                         "/***********************************************************************************************************" +
                                         "\r\n\tNOMBRE:       %PAR_NOMBRE%" +
                                         "\r\n\tDESCRIPCION:" +
                                         "\r\n\t\t%PAR_DESCRIPCION%" +
                                         "\r\n" +
                                         "\r\n\tREVISIONES:" +
                                         "\r\n\t\tVer        FECHA       Autor            Descripcion " +
                                         "\r\n\t\t---------  ----------  ---------------  ------------------------------------" +
                                         "\r\n\t\t1.0        " + FC + "  " + Creador + "  Creacion " +
                                         "\r\n\r\n" +
                                         "*************************************************************************************************************/" +
                                         "\r\n#endregion" +
                                         "\r\n\r\n";

            formulario.strCabecera = "#region" +
                            "\r\n\r\nusing System;\r\n";

            TipoEntorno miEntorno;
            Enum.TryParse(formulario.cmbEntorno.SelectedItem.ToString(), out miEntorno);

            //Elegimos el origen : Tablas o Vistas
            ReAlListView MyReAlListView = new ReAlListView();
            switch (formulario.tabBDObjects.SelectedIndex)
            {
                case 0:
                    MyReAlListView = formulario.ReAlListView1;
                    break;

                case 1:
                    MyReAlListView = formulario.ReAlListView2;
                    break;

                case 2:
                    MyReAlListView = formulario.ReAlListView3;
                    break;
            }

            if (miEntorno == TipoEntorno.CSharp
                || miEntorno == TipoEntorno.CSharp_WCF
                || miEntorno == TipoEntorno.CSharp_WinForms
                || miEntorno == TipoEntorno.CSharp_WebForms
                || miEntorno == TipoEntorno.CSharp_WSE
                || miEntorno == TipoEntorno.CSharp_WebAPI
                || miEntorno == TipoEntorno.VB_Net
                || miEntorno == TipoEntorno.VB_Net_WCF)
            {
                if (miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
                    formulario.strCabecera = formulario.strCabecera + "using System.Windows.Forms;\r\n";
                else
                    formulario.strCabecera = formulario.strCabecera + "using System.Web;" +
                                             "\r\nusing System.Web.Security;" +
                                             "\r\nusing System.Web.UI;" +
                                             "\r\nusing System.Web.UI.HtmlControls;" +
                                             "\r\nusing System.Web.UI.WebControls;" +
                                             "\r\nusing System.Web.UI.WebControls.WebParts;" +
                                             "\r\n";

                formulario.strCabecera = formulario.strCabecera + "using System.Collections;" +
                                         "\r\nusing System.Collections.Generic;" +
                                         "\r\nusing System.Text;" +
                                         "\r\nusing System.Data;" +
                                         "\r\n";

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        formulario.strCabecera = formulario.strCabecera + "using System.Data.SqlClient;\r\n";
                        break;

                    case TipoBD.SQLServer2000:
                        formulario.strCabecera = formulario.strCabecera + "using System.Data.SqlClient;\r\n";
                        break;

                    case TipoBD.Oracle:
                        formulario.strCabecera = formulario.strCabecera + "using System.Data.OracleClient;\r\n";
                        break;

                    case TipoBD.MsAccess:
                        formulario.strCabecera = formulario.strCabecera + "using System.Data.OleDb;\r\n";
                        break;

                    case TipoBD.PostgreSQL:
                        formulario.strCabecera = formulario.strCabecera + "using Npgsql;\r\n";
                        break;
                }

                formulario.strCabecera = formulario.strCabecera + "using System.Drawing;" +
                                         "\r\nusing System.Drawing.Imaging;" +
                                         "\r\nusing System.Configuration;" +
                                         "\r\nusing System.IO;" +
                                         "\r\nusing System.Drawing.Drawing2D;" +
                                         "\r\nusing System.Diagnostics;" +
                                         "\r\nusing System.Reflection;" +
                                         "\r\nusing System.Runtiformulario.Serialization;" +
                                         "\r\nusing System.Runtiformulario.Serialization.Formatters.Binary;" +
                                         "\r\nusing System.Text.RegularExpressions;" +
                                         "\r\n" +
                                         "\r\n#endregion" +
                                         "\r\n\r\n";
            }

            //Definicion de Parametros

            //Tablas y Vistas
            for (int i = 0; i <= MyReAlListView.Items.Count - 1; i++)
            {
                if (MyReAlListView.Items[i].Checked == true)
                {
                    string idTabla = MyReAlListView.Items[i].SubItems[1].Text;
                    string nameTabla = MyReAlListView.Items[i].SubItems[0].Text;
                    string nameSchema = MyReAlListView.Items[i].SubItems[2].Text;
                    string nameClase = nameTabla[0].ToString().ToUpper() + nameTabla.Substring(1, nameTabla.Length - 1);

                    //Obtenemos las columnas
                    dtColumns = CTablasColumnas.ObtenerColumnasTabla(miEntorno, formulario, nameSchema, idTabla);
                    if (dtColumns.Rows.Count > 0)
                    {
                        #region Entidades

                        if (formulario.chkReAlEntidades.Checked)
                        {
                            cEntidades cEnt = new cEntidades();
                            switch (miEntorno)
                            {
                                case TipoEntorno.CSharp:
                                case TipoEntorno.CSharp_WinForms:
                                case TipoEntorno.CSharp_WebForms:
                                case TipoEntorno.CSharp_WCF:
                                case TipoEntorno.CSharp_WSE:
                                case TipoEntorno.CSharp_WebAPI:
                                    cEntidadesParametros misParams = new cEntidadesParametros();
                                    misParams.bUseEventProperties = formulario.chkPropertyEvents.Checked;
                                    misParams.bUseBaseClass = formulario.chkEntBaseClass.Checked;
                                    misParams.bUseDataAnnotations = formulario.chkDataAnnotations.Checked;
                                    misParams.bMoverResultados = formulario.chkMoverResultados.Checked;
                                    misParams.bradWCF = (miEntorno == TipoEntorno.CSharp_WCF);
                                    misParams.bEntidadesEncriptacion = formulario.chkEntidadesEncriptacion.Checked;
                                    misParams.strPathProyecto = formulario.txtPathProyecto.Text;
                                    misParams.strInfProyecto = formulario.txtInfProyecto.Text;
                                    misParams.strInfProyectoClass = formulario.txtInfProyectoClass.Text;
                                    misParams.strNamespaceEntidades = formulario.txtNamespaceEntidades.Text;
                                    misParams.strNamespaceModelo = formulario.txtNamespaceNegocios.Text;
                                    misParams.strPrefijoEntidades = formulario.txtPrefijoEntidades.Text;
                                    misParams.strPrefijoModelo = formulario.txtPrefijoModelo.Text;
                                    misParams.strCabeceraPlantillaCodigo = formulario.strCabeceraPlantillaCodigo;
                                    misParams.strNameTabla = nameTabla;
                                    misParams.strNameClase = misParams.strPrefijoEntidades +
                                                             misParams.strNameTabla.Replace("_", "").Substring(0, 1)
                                                                 .ToUpper() +
                                                             misParams.strNameTabla.Replace("_", "").Substring(1, 2)
                                                                 .ToLower() + misParams.strNameTabla.Replace("_", "")
                                                                 .Substring(3, 1).ToUpper() + misParams.strNameTabla
                                                                 .Replace("_", "").Substring(4).ToLower();
                                    misParams.strNameClaseModelo =
                                        misParams.strPrefijoModelo +
                                        misParams.strNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                        misParams.strNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                        misParams.strNameTabla.Replace("_", "").Substring(3, 1).ToUpper() + misParams
                                            .strNameTabla.Replace("_", "").Substring(4).ToLower();
                                    misParams.strNameSchema = nameSchema;
                                    misParams.strParamListadoSp = (string.IsNullOrEmpty(formulario.txtClaseParametrosListadoSp.Text) ? "cListadoSP" : formulario.txtClaseParametrosListadoSp.Text);
                                    misParams.strParamBaseClass = (string.IsNullOrEmpty(formulario.txtClaseParametrosBaseClass.Text) ? "cBaseClass" : formulario.txtClaseParametrosBaseClass.Text);
                                    misParams.strParamClaseApi = (string.IsNullOrEmpty(formulario.txtClaseParametrosClaseApi.Text) ? "cApi" : formulario.txtClaseParametrosClaseApi.Text);
                                    misParams.strParamClaseApiObject = (string.IsNullOrEmpty(formulario.txtClaseParametrosClaseApiObject.Text) ? "cApiObject" : formulario.txtClaseParametrosClaseApiObject.Text);
                                    misParams.miEntorno = miEntorno;

                                    //La entidad
                                    Texto = Texto + "\r\n" + cEnt.CrearEntidadesCS(dtColumns, ref misParams);
                                    break;

                                case TipoEntorno.CSharp_NetCore_V2:
                                case TipoEntorno.CSharp_NetCore_WebAPI_V2:

                                    cEntidadesParametros misParamsNetCore2 = new cEntidadesParametros();
                                    misParamsNetCore2.bUseEventProperties = false;
                                    misParamsNetCore2.bUseBaseClass = false;
                                    misParamsNetCore2.bUseDataAnnotations = true;
                                    misParamsNetCore2.bMoverResultados = formulario.chkMoverResultados.Checked;
                                    misParamsNetCore2.bradWCF = false;
                                    misParamsNetCore2.bEntidadesEncriptacion = false;
                                    misParamsNetCore2.strPathProyecto = formulario.txtPathProyecto.Text;
                                    misParamsNetCore2.strInfProyecto = formulario.txtInfProyecto.Text;
                                    misParamsNetCore2.strInfProyectoClass = formulario.txtInfProyectoClass.Text;
                                    misParamsNetCore2.strNamespaceEntidades = formulario.txtNamespaceEntidades.Text;
                                    misParamsNetCore2.strNamespaceModelo = formulario.txtNamespaceNegocios.Text;
                                    misParamsNetCore2.strPrefijoEntidades = formulario.txtPrefijoEntidades.Text;
                                    misParamsNetCore2.strPrefijoModelo = formulario.txtPrefijoModelo.Text;
                                    misParamsNetCore2.strCabeceraPlantillaCodigo = formulario.strCabeceraPlantillaCodigo;
                                    misParamsNetCore2.strNameTabla = nameTabla;
                                    misParamsNetCore2.strNameClase = misParamsNetCore2.strPrefijoEntidades +
                                                                     misParamsNetCore2.strNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                                                     misParamsNetCore2.strNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                                                     misParamsNetCore2.strNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                                                     misParamsNetCore2.strNameTabla.Replace("_", "").Substring(4).ToLower();
                                    misParamsNetCore2.strNameClaseModelo =
                                        misParamsNetCore2.strPrefijoModelo +
                                        misParamsNetCore2.strNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                        misParamsNetCore2.strNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                        misParamsNetCore2.strNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                        misParamsNetCore2.strNameTabla.Replace("_", "").Substring(4).ToLower();
                                    misParamsNetCore2.strNameSchema = nameSchema;
                                    misParamsNetCore2.strParamListadoSp = (string.IsNullOrEmpty(formulario.txtClaseParametrosListadoSp.Text) ? "cListadoSP" : formulario.txtClaseParametrosListadoSp.Text);
                                    misParamsNetCore2.strParamBaseClass = (string.IsNullOrEmpty(formulario.txtClaseParametrosBaseClass.Text) ? "cBaseClass" : formulario.txtClaseParametrosBaseClass.Text);
                                    misParamsNetCore2.strParamClaseApi = (string.IsNullOrEmpty(formulario.txtClaseParametrosClaseApi.Text) ? "cApi" : formulario.txtClaseParametrosClaseApi.Text);
                                    misParamsNetCore2.strParamClaseApiObject = (string.IsNullOrEmpty(formulario.txtClaseParametrosClaseApiObject.Text) ? "cApiObject" : formulario.txtClaseParametrosClaseApiObject.Text);
                                    misParamsNetCore2.miEntorno = miEntorno;

                                    //La entidad
                                    Texto = Texto + "\r\n" + cEnt.CrearEntidadesCS(dtColumns, ref misParamsNetCore2);
                                    break;

                                case TipoEntorno.CSharp_NetCore_V5:
                                case TipoEntorno.CSharp_NetCore_V5_WebAPI_Client:
                                case TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger:
                                    cEntidadesParametros misParamsNetCore5 = new cEntidadesParametros();
                                    misParamsNetCore5.bUseEventProperties = false;
                                    misParamsNetCore5.bUseBaseClass = true;
                                    misParamsNetCore5.bUseDataAnnotations = true;
                                    misParamsNetCore5.bMoverResultados = formulario.chkMoverResultados.Checked;
                                    misParamsNetCore5.bradWCF = false;
                                    misParamsNetCore5.bEntidadesEncriptacion = false;
                                    misParamsNetCore5.strPathProyecto = formulario.txtPathProyecto.Text;
                                    misParamsNetCore5.strInfProyecto = formulario.txtInfProyecto.Text;
                                    misParamsNetCore5.strInfProyectoClass = formulario.txtInfProyectoClass.Text;
                                    misParamsNetCore5.strNamespaceEntidades = formulario.txtNamespaceEntidades.Text;
                                    misParamsNetCore5.strNamespaceModelo = formulario.txtNamespaceNegocios.Text;
                                    misParamsNetCore5.strPrefijoEntidades = formulario.txtPrefijoEntidades.Text;
                                    misParamsNetCore5.strPrefijoModelo = formulario.txtPrefijoModelo.Text;
                                    misParamsNetCore5.strCabeceraPlantillaCodigo = formulario.strCabeceraPlantillaCodigo;
                                    misParamsNetCore5.strNameTabla = nameTabla;
                                    misParamsNetCore5.strNameClase = misParamsNetCore5.strPrefijoEntidades +
                                                                     misParamsNetCore5.strNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                                                     misParamsNetCore5.strNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                                                     misParamsNetCore5.strNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                                                     misParamsNetCore5.strNameTabla.Replace("_", "").Substring(4).ToLower();
                                    misParamsNetCore5.strNameClaseModelo =
                                        misParamsNetCore5.strPrefijoModelo +
                                        misParamsNetCore5.strNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                        misParamsNetCore5.strNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                        misParamsNetCore5.strNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                        misParamsNetCore5.strNameTabla.Replace("_", "").Substring(4).ToLower();
                                    misParamsNetCore5.strNameSchema = nameSchema;
                                    misParamsNetCore5.strParamListadoSp = (string.IsNullOrEmpty(formulario.txtClaseParametrosListadoSp.Text) ? "CListadoSP" : formulario.txtClaseParametrosListadoSp.Text);
                                    misParamsNetCore5.strParamBaseClass = (string.IsNullOrEmpty(formulario.txtClaseParametrosBaseClass.Text) ? "CBaseClass" : formulario.txtClaseParametrosBaseClass.Text);
                                    misParamsNetCore5.strParamClaseApi = (string.IsNullOrEmpty(formulario.txtClaseParametrosClaseApi.Text) ? "CApi" : formulario.txtClaseParametrosClaseApi.Text);
                                    misParamsNetCore5.strParamClaseApiObject = (string.IsNullOrEmpty(formulario.txtClaseParametrosClaseApiObject.Text) ? "CApiObject" : formulario.txtClaseParametrosClaseApiObject.Text);
                                    misParamsNetCore5.miEntorno = miEntorno;

                                    //La entidad
                                    Texto = Texto + "\r\n" + cEnt.CrearEntidadesCS(dtColumns, ref misParamsNetCore5);
                                    break;

                                case TipoEntorno.CSharp_NetCore_Ef:

                                    DataTable dtColumnsForaneas = CTablasColumnas.ObtenerColumnasForaneaTabla(miEntorno, formulario, nameSchema, idTabla);

                                    cEntidadesParametros misParamsNetCore3 = new cEntidadesParametros();
                                    misParamsNetCore3.bUseEventProperties = false;
                                    misParamsNetCore3.bUseBaseClass = false;
                                    misParamsNetCore3.bUseDataAnnotations = true;
                                    misParamsNetCore3.bMoverResultados = formulario.chkMoverResultados.Checked;
                                    misParamsNetCore3.bradWCF = false;
                                    misParamsNetCore3.bEntidadesEncriptacion = false;
                                    misParamsNetCore3.strPathProyecto = formulario.txtPathProyecto.Text;
                                    misParamsNetCore3.strInfProyecto = formulario.txtInfProyecto.Text;
                                    misParamsNetCore3.strInfProyectoClass = formulario.txtInfProyectoClass.Text;
                                    misParamsNetCore3.strNamespaceEntidades = formulario.txtNamespaceEntidades.Text;
                                    misParamsNetCore3.strNamespaceModelo = formulario.txtNamespaceNegocios.Text;
                                    misParamsNetCore3.strPrefijoEntidades = formulario.txtPrefijoEntidades.Text;
                                    misParamsNetCore3.strPrefijoModelo = formulario.txtPrefijoModelo.Text;
                                    misParamsNetCore3.strCabeceraPlantillaCodigo = formulario.strCabeceraPlantillaCodigo;
                                    misParamsNetCore3.strNameTabla = nameTabla;
                                    misParamsNetCore3.strNameClase = misParamsNetCore3.strPrefijoEntidades +
                                                                     misParamsNetCore3.strNameTabla.ToPascalCase();
                                    misParamsNetCore3.strNameClaseModelo =
                                        misParamsNetCore3.strPrefijoModelo +
                                        misParamsNetCore3.strNameTabla.ToPascalCase();
                                    misParamsNetCore3.strNameSchema = nameSchema;
                                    misParamsNetCore3.strParamListadoSp = (string.IsNullOrEmpty(formulario.txtClaseParametrosListadoSp.Text) ? "cListadoSP" : formulario.txtClaseParametrosListadoSp.Text);
                                    misParamsNetCore3.strParamBaseClass = (string.IsNullOrEmpty(formulario.txtClaseParametrosBaseClass.Text) ? "cBaseClass" : formulario.txtClaseParametrosBaseClass.Text);
                                    misParamsNetCore3.strParamClaseApi = (string.IsNullOrEmpty(formulario.txtClaseParametrosClaseApi.Text) ? "cApi" : formulario.txtClaseParametrosClaseApi.Text);
                                    misParamsNetCore3.strParamClaseApiObject = (string.IsNullOrEmpty(formulario.txtClaseParametrosClaseApiObject.Text) ? "cApiObject" : formulario.txtClaseParametrosClaseApiObject.Text);
                                    misParamsNetCore3.miEntorno = miEntorno;

                                    //La entidad
                                    Texto = Texto + "\r\n" + cEnt.CrearEnumEntityFramework(dtColumns, dtColumnsForaneas, ref misParamsNetCore3);
                                    break;

                                case TipoEntorno.VB_Net:
                                case TipoEntorno.VB_Net_WCF:
                                    Texto = Texto + "\r\n" + cEnt.CrearEntidadesVB(dtColumns, nameTabla, ref formulario);
                                    break;

                                case TipoEntorno.Java_Clasico:
                                    Texto = Texto + "\r\n" + cEnt.CrearEntidadesJava(dtColumns, nameTabla, ref formulario);
                                    break;

                                case TipoEntorno.Java_Android:
                                    Texto = Texto + "\r\n" + cEnt.CrearEntidadesJavaAndroid(dtColumns, nameTabla, ref formulario);
                                    break;

                                case TipoEntorno.PHP_Clasico:
                                    Texto = Texto + "\r\n" + cEnt.CrearEntidadesPhp(dtColumns, nameTabla, ref formulario);
                                    break;

                                case TipoEntorno.PHP_CodeIgniter:
                                    cEntidades cEntCiPhp = new cEntidades();
                                    Texto = Texto + "\r\n" + cEntCiPhp.CrearEntidadesCodeIgniterClassicPhp(dtColumns, nameTabla, ref formulario);
                                    break;

                                case TipoEntorno.PHP_CodeIgniter_Models_API:
                                    cEntidades cEntCiModelsPhp = new cEntidades();
                                    Texto = Texto + "\r\n" + cEntCiModelsPhp.CrearEntidadesCodeIgniterSparkPhp(dtColumns, nameTabla, ref formulario);
                                    break;

                                case TipoEntorno.Python:
                                    Texto = Texto + "\r\n" + cEnt.CrearEntidadesPython(dtColumns, nameTabla, ref formulario);
                                    break;

                                case TipoEntorno.Angular_CLI:
                                    cEntidadesParametros misParamsNg = new cEntidadesParametros();
                                    misParamsNg.bUseEventProperties = formulario.chkPropertyEvents.Checked;
                                    misParamsNg.bUseBaseClass = formulario.chkEntBaseClass.Checked;
                                    misParamsNg.bUseDataAnnotations = formulario.chkDataAnnotations.Checked;
                                    misParamsNg.bMoverResultados = formulario.chkMoverResultados.Checked;
                                    misParamsNg.bradWCF = miEntorno == TipoEntorno.CSharp_WCF;
                                    misParamsNg.bEntidadesEncriptacion = formulario.chkEntidadesEncriptacion.Checked;
                                    misParamsNg.strPathProyecto = formulario.txtPathProyecto.Text;
                                    misParamsNg.strInfProyecto = formulario.txtInfProyecto.Text;
                                    misParamsNg.strInfProyectoClass = formulario.txtInfProyectoClass.Text;
                                    misParamsNg.strNamespaceEntidades = formulario.txtNamespaceEntidades.Text;
                                    misParamsNg.strNamespaceModelo = formulario.txtNamespaceNegocios.Text;
                                    misParamsNg.strPrefijoEntidades = formulario.txtPrefijoEntidades.Text;
                                    misParamsNg.strPrefijoModelo = formulario.txtPrefijoModelo.Text;
                                    misParamsNg.strCabeceraPlantillaCodigo = formulario.strCabeceraPlantillaCodigo;
                                    misParamsNg.strNameTabla = nameTabla;
                                    misParamsNg.strNameClase = misParamsNg.strPrefijoEntidades +
                                                               misParamsNg.strNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                                               misParamsNg.strNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                                               misParamsNg.strNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                                               misParamsNg.strNameTabla.Replace("_", "").Substring(4).ToLower();
                                    misParamsNg.strNameClaseModelo =
                                        misParamsNg.strPrefijoModelo +
                                        misParamsNg.strNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                        misParamsNg.strNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                        misParamsNg.strNameTabla.Replace("_", "").Substring(3, 1).ToUpper() + misParamsNg
                                            .strNameTabla.Replace("_", "").Substring(4).ToLower();
                                    misParamsNg.strNameSchema = nameSchema;
                                    misParamsNg.strParamListadoSp = (string.IsNullOrEmpty(formulario.txtClaseParametrosListadoSp.Text) ? "cListadoSP" : formulario.txtClaseParametrosListadoSp.Text);
                                    misParamsNg.strParamBaseClass = (string.IsNullOrEmpty(formulario.txtClaseParametrosBaseClass.Text) ? "cBaseClass" : formulario.txtClaseParametrosBaseClass.Text);
                                    misParamsNg.strParamClaseApi = (string.IsNullOrEmpty(formulario.txtClaseParametrosClaseApi.Text) ? "cApi" : formulario.txtClaseParametrosClaseApi.Text);
                                    misParamsNg.strParamClaseApiObject = (string.IsNullOrEmpty(formulario.txtClaseParametrosClaseApiObject.Text) ? "cApiObject" : formulario.txtClaseParametrosClaseApiObject.Text);
                                    misParamsNg.miEntorno = miEntorno;

                                    Texto = Texto + "\r\n" + cEnt.CrearEntidadesAngular(dtColumns, ref misParamsNg);
                                    break;

                                case TipoEntorno.Feathers_JS:
                                    cEntidadesParametros misParamsSeq = new cEntidadesParametros();
                                    misParamsSeq.bUseEventProperties = formulario.chkPropertyEvents.Checked;
                                    misParamsSeq.bUseBaseClass = formulario.chkEntBaseClass.Checked;
                                    misParamsSeq.bUseDataAnnotations = formulario.chkDataAnnotations.Checked;
                                    misParamsSeq.bMoverResultados = formulario.chkMoverResultados.Checked;
                                    misParamsSeq.bradWCF = miEntorno == TipoEntorno.CSharp_WCF;
                                    misParamsSeq.bEntidadesEncriptacion = formulario.chkEntidadesEncriptacion.Checked;
                                    misParamsSeq.strPathProyecto = formulario.txtPathProyecto.Text;
                                    misParamsSeq.strInfProyecto = formulario.txtInfProyecto.Text;
                                    misParamsSeq.strInfProyectoClass = formulario.txtInfProyectoClass.Text;
                                    misParamsSeq.strNamespaceEntidades = formulario.txtNamespaceEntidades.Text;
                                    misParamsSeq.strNamespaceModelo = formulario.txtNamespaceNegocios.Text;
                                    misParamsSeq.strPrefijoEntidades = formulario.txtPrefijoEntidades.Text;
                                    misParamsSeq.strPrefijoModelo = formulario.txtPrefijoModelo.Text;
                                    misParamsSeq.strCabeceraPlantillaCodigo = formulario.strCabeceraPlantillaCodigo;
                                    misParamsSeq.strNameTabla = nameTabla;
                                    misParamsSeq.strNameClase = misParamsSeq.strPrefijoEntidades +
                                                                misParamsSeq.strNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                                                misParamsSeq.strNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                                                misParamsSeq.strNameTabla.Replace("_", "")
                                                                    .Substring(3, 1).ToUpper() + misParamsSeq.strNameTabla
                                                                    .Replace("_", "").Substring(4).ToLower();
                                    misParamsSeq.strNameClaseModelo =
                                        misParamsSeq.strPrefijoModelo +
                                        misParamsSeq.strNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                        misParamsSeq.strNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                        misParamsSeq.strNameTabla.Replace("_", "").Substring(3, 1).ToUpper() + misParamsSeq
                                            .strNameTabla.Replace("_", "").Substring(4).ToLower();
                                    misParamsSeq.strNameSchema = nameSchema;
                                    misParamsSeq.strParamListadoSp = (string.IsNullOrEmpty(formulario.txtClaseParametrosListadoSp.Text) ? "cListadoSP" : formulario.txtClaseParametrosListadoSp.Text);
                                    misParamsSeq.strParamBaseClass = (string.IsNullOrEmpty(formulario.txtClaseParametrosBaseClass.Text) ? "cBaseClass" : formulario.txtClaseParametrosBaseClass.Text);
                                    misParamsSeq.strParamClaseApi = (string.IsNullOrEmpty(formulario.txtClaseParametrosClaseApi.Text) ? "cApi" : formulario.txtClaseParametrosClaseApi.Text);
                                    misParamsSeq.strParamClaseApiObject = (string.IsNullOrEmpty(formulario.txtClaseParametrosClaseApiObject.Text) ? "cApiObject" : formulario.txtClaseParametrosClaseApiObject.Text);
                                    misParamsSeq.miEntorno = miEntorno;

                                    var strWizard = new StringBuilder();
                                    strWizard.AppendLine("C:\\..\\>feathers generate service");
                                    strWizard.AppendLine("? What kind of service is it? Sequelize");
                                    strWizard.AppendLine("? What is the name of the service? " + nameTabla.ToLower());
                                    strWizard.AppendLine("? Which path should the service be registered on? /" + nameTabla.ToLower());
                                    strWizard.AppendLine("? Does the service require authentication? Yes");
                                    strWizard.AppendLine("? Which database are you connecting to? PostgreSQL");

                                    Texto = Texto + "\r\n" + strWizard.ToString();
                                    Texto = Texto + cEnt.CrearEntidadesFeatherJsSequelize(dtColumns, ref misParamsSeq);
                                    Texto = Texto + "\r\n";
                                    break;

                                default:
                                    throw new Exception("Aun no se han definido ENTIDADES para el entorno " + miEntorno);
                            }
                        }

                        #endregion Entidades

                        #region Interfaces

                        if (formulario.chkReAlInterface.Checked)
                        {
                            cInterfaces cInterf = new cInterfaces();
                            switch (miEntorno)
                            {
                                case TipoEntorno.CSharp:
                                case TipoEntorno.CSharp_WinForms:
                                case TipoEntorno.CSharp_WebForms:
                                case TipoEntorno.CSharp_WCF:
                                case TipoEntorno.CSharp_WSE:
                                case TipoEntorno.CSharp_WebAPI:
                                case TipoEntorno.CSharp_NetCore_V2:
                                case TipoEntorno.CSharp_NetCore_WebAPI_V2:
                                case TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger:
                                case TipoEntorno.CSharp_NetCore_V5:
                                    Texto = Texto + "\r\n" + cInterf.CrearInterfacesCS(dtColumns, nameTabla, ref formulario);
                                    break;
                                case TipoEntorno.CSharp_NetCore_V5_WebAPI_Client:
                                    Texto = Texto + "\r\n" + cInterf.CrearInterfacesCSWebClient(dtColumns, nameTabla, ref formulario);
                                    break;
                                case TipoEntorno.VB_Net:
                                case TipoEntorno.VB_Net_WCF:
                                    Texto = Texto + "\r\n" + cInterf.CrearInterfacesVB(dtColumns, nameTabla, ref formulario);
                                    break;
                                case TipoEntorno.Java_Clasico:
                                    Texto = Texto + "\r\n" + cInterf.CrearInterfacesJavaClasico(dtColumns, nameSchema, nameTabla, ref formulario);
                                    break;
                                default:
                                    throw new Exception("Aun no se han definido INTERFACES para el entorno " + miEntorno);
                            }
                        }

                        #endregion Interfaces

                        #region Regla de Negocios

                        if (formulario.chkReAlReglaNegocios.Checked)
                        {
                            switch (miEntorno)
                            {
                                case TipoEntorno.CSharp:
                                case TipoEntorno.CSharp_WinForms:
                                case TipoEntorno.CSharp_WebForms:
                                case TipoEntorno.CSharp_WCF:
                                case TipoEntorno.CSharp_WSE:
                                case TipoEntorno.CSharp_NetCore_V2:
                                case TipoEntorno.CSharp_NetCore_V5:
                                    cReglaNegociosCSharp cRn = new cReglaNegociosCSharp();
                                    Texto = Texto + "\r\n" + cRn.CrearModelo(dtColumns, nameTabla, ref formulario);
                                    if (formulario.chkUsarSps.Checked)
                                    {
                                        cAltasBajasMod cRnSps = new cAltasBajasMod();
                                        cRnSps.CrearEnumSP(ref formulario);
                                        cRnSps.CrearEnumApi(ref formulario);
                                    }
                                    break;

                                case TipoEntorno.CSharp_WebAPI:
                                    cReglaNegociosCSharp cRnWebApi = new cReglaNegociosCSharp();
                                    Texto = Texto + "\r\n" + cRnWebApi.CrearModelo(dtColumns, nameTabla, ref formulario);
                                    if (formulario.chkUsarSps.Checked)
                                    {
                                        cAltasBajasMod cRnSps = new cAltasBajasMod();
                                        cRnSps.CrearEnumSP(ref formulario);
                                        cRnSps.CrearEnumApi(ref formulario);
                                    }
                                    cWebAPI webAPI = new cWebAPI();
                                    Texto = Texto + "\r\n" + webAPI.CrearWebApiCs(dtColumns, nameTabla, ref formulario);
                                    break;

                                case TipoEntorno.CSharp_NetCore_WebAPI_V2:
                                case TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger:
                                    cReglaNegociosCSharp cRnNetCoreWebApi = new cReglaNegociosCSharp();
                                    Texto = Texto + "\r\n" + cRnNetCoreWebApi.CrearModelo(dtColumns, nameTabla, ref formulario);
                                    if (formulario.chkUsarSps.Checked)
                                    {
                                        cAltasBajasMod cRnSps = new cAltasBajasMod();
                                        cRnSps.CrearEnumSP(ref formulario);
                                        cRnSps.CrearEnumApi(ref formulario);
                                    }
                                    cNetCoreWebAPI netCoreWebAPI = new cNetCoreWebAPI();
                                    Texto = Texto + "\r\n" + netCoreWebAPI.CrearNetCoreWebApiCs(dtColumns, nameTabla, ref formulario);

                                    //Tambien creamos los clientes
                                    Texto = Texto + "\r\n" + netCoreWebAPI.CrearNetCoreWebApiClientCs(dtColumns, nameTabla, ref formulario);
                                    break;

                                case TipoEntorno.CSharp_NetCore_V5_WebAPI_Client:
                                    cReglaNegociosCSharp_WebApiClient cRnNetCoreWebApiClient = new cReglaNegociosCSharp_WebApiClient();
                                    Texto = Texto + "\r\n" + cRnNetCoreWebApiClient.CrearModelo(dtColumns, nameTabla, ref formulario);
                                    break;

                                case TipoEntorno.VB_Net:
                                case TipoEntorno.VB_Net_WCF:
                                    cReglaNegociosVB cRnVB = new cReglaNegociosVB();
                                    Texto = Texto + "\r\n" + cRnVB.CrearModeloVB(dtColumns, nameTabla, ref formulario);
                                    if (formulario.chkUsarSps.Checked)
                                    {
                                        cAltasBajasMod cRnSps = new cAltasBajasMod();
                                        cRnSps.CrearEnumSPVB(ref formulario);
                                        cRnSps.CrearEnumApiVB(ref formulario);
                                    }
                                    break;

                                case TipoEntorno.Java_Clasico:
                                    cReglaNegociosJava cRnJava = new cReglaNegociosJava();
                                    Texto = Texto + "\r\n" + cRnJava.CrearModeloJavaClasico(dtColumns, nameTabla, ref formulario);
                                    //Texto = Texto + "\r\n" + cRnJava.CrearModeloJava(dtColumns, nameTabla, ref formulario);
                                    //Las Clases Adicionales
                                    //Texto = Texto + "\r\n" + cRnJava.crearcParams(ref formulario);
                                    //Texto = Texto + "\r\n" + cRnJava.crearConn(ref formulario);
                                    break;

                                case TipoEntorno.Java_Android:
                                    cReglaNegociosJavaAndroid cRnJavaAndroid = new cReglaNegociosJavaAndroid();
                                    Texto = Texto + "\r\n" + cRnJavaAndroid.CrearModeloJavaAndroid(dtColumns, nameTabla, ref formulario);

                                    //Las Clases Adicionales
                                    Texto = Texto + "\r\n" + cRnJavaAndroid.crearcParams(ref MyReAlListView, ref formulario);
                                    Texto = Texto + "\r\n" + cRnJavaAndroid.crearConn(ref formulario.ReAlListView1, ref formulario);
                                    break;

                                case TipoEntorno.PHP_Clasico:
                                    cReglaNegociosPHP cRnPhp = new cReglaNegociosPHP();
                                    Texto = Texto + "\r\n" + cRnPhp.CrearModeloPhp(dtColumns, nameTabla, ref formulario);
                                    Texto = Texto + "\r\n" + cRnPhp.crearConn(ref formulario);
                                    break;

                                case TipoEntorno.PHP_CodeIgniter:
                                    var cRnCiPhp = new cReglaNegociosPHPCodeIgniterClassic();
                                    Texto = Texto + "\r\n" + cRnCiPhp.CrearControllerPhpCodeIgniterClassic(dtColumns, nameTabla, ref formulario);
                                    break;
                                case TipoEntorno.PHP_CodeIgniter_Models_API:
                                    var cRnCiPhpModelsApi = new cReglaNegociosPHPCodeIgniterModelsAPI();

                                    Texto = Texto + "\r\n" + cRnCiPhpModelsApi.CrearControllerPhpCodeIgniterModelsAPI(dtColumns, nameTabla, ref formulario);
                                    Texto = Texto + "\r\n" + cRnCiPhpModelsApi.CrearPhpStormApiTests(dtColumns, nameTabla, ref formulario);
                                    Texto = Texto + "\r\n" + cRnCiPhpModelsApi.CrearOpenAPIDocPhpCodeIgniterModelsAPI(dtColumns, nameTabla, ref formulario);

                                    string pNameTabla = nameTabla.Replace("_", "");
                                    string pNameClaseRn = formulario.txtPrefijoModelo.Text +
                                                            pNameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                                            pNameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                                            pNameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                                            pNameTabla.Replace("_", "").Substring(4).ToLower();
                                    string pNameClaseApi = pNameClaseRn.Remove(0, 3).ToLower();
                                    Texto = Texto + "\r\n$routes->group(\"api\", function ($routes) {";
                                    Texto = Texto + "\r\n\t$routes->get('"+ pNameClaseApi + "', '"+ pNameClaseRn + "::index');";
                                    Texto = Texto + "\r\n\t$routes->post('" + pNameClaseApi + "', '" + pNameClaseRn + "::create');";
                                    Texto = Texto + "\r\n\t$routes->get('" + pNameClaseApi + "/(:segment)', '" + pNameClaseRn + "::show/$1');";
                                    Texto = Texto + "\r\n\t$routes->get('" + pNameClaseApi + "/(:segment)/edit', '" + pNameClaseRn + "::edit/$1');";
                                    Texto = Texto + "\r\n\t$routes->put('" + pNameClaseApi + "/(:segment)', '" + pNameClaseRn + "::update/$1');";
                                    Texto = Texto + "\r\n\t$routes->patch('" + pNameClaseApi + "/(:segment)', '" + pNameClaseRn + "::update/$1');";
                                    Texto = Texto + "\r\n\t$routes->delete('" + pNameClaseApi + "/(:segment)', '" + pNameClaseRn + "::delete/$1');";
                                    Texto = Texto + "\r\n});";
                                    Texto = Texto + "\r\n";
                                    Texto = Texto + "\r\n";


                                    break;

                                case TipoEntorno.Python:
                                    cReglaNegociosPython cRnPy = new cReglaNegociosPython();
                                    Texto = Texto + "\r\n" + cRnPy.CrearModeloPython(dtColumns, nameTabla, ref formulario);

                                    //Las Clases Adicionales
                                    Texto = Texto + "\r\n" + cRnPy.crearcParams(ref formulario);
                                    Texto = Texto + "\r\n" + cRnPy.crearConn(ref formulario);
                                    break;

                                case TipoEntorno.Angular_CLI:
                                    cReglaNegociosAngular cRnNg = new cReglaNegociosAngular();
                                    Texto = Texto + "\r\n" + cRnNg.CrearServicesAngular(dtColumns, nameTabla, ref formulario);
                                    break;

                                default:
                                    throw new Exception("Aun no se han definido REGLA DE NEGOCIOS para el entorno " + miEntorno);
                            }
                        }

                        #endregion Regla de Negocios

                        #region Pruebas Unitarias

                        if (formulario.chkReAlUnitTest.Checked)
                        {
                            cUnitTest cUnitTests = new cUnitTest();
                            switch (miEntorno)
                            {
                                case TipoEntorno.CSharp:
                                case TipoEntorno.CSharp_WinForms:
                                case TipoEntorno.CSharp_WebForms:
                                case TipoEntorno.CSharp_WCF:
                                case TipoEntorno.CSharp_WSE:
                                case TipoEntorno.CSharp_WebAPI:
                                    Texto = Texto + "\r\n" + cUnitTests.CrearUnitTestCS(dtColumns, nameTabla, ref formulario);
                                    break;

                                default:
                                    throw new Exception("Aun no se han definido UNIT TEST para el entorno " + miEntorno);
                            }
                        }

                        #endregion Pruebas Unitarias

                        #region Documentacion

                        if (formulario.chkReAlCrearDoc.Checked)
                        {
                            var miPar = new cDocParams();
                            miPar.dtColumns = dtColumns;
                            miPar.strNameTabla = nameTabla;
                            miPar.strNameClaseEnt = formulario.txtPrefijoEntidades.Text +
                                                    nameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                                    nameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                                    nameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                                    nameTabla.Replace("_", "").Substring(4).ToLower();
                            miPar.strNameClaseRn = formulario.txtPrefijoModelo.Text +
                                                   nameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                                   nameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                                   nameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                                   nameTabla.Replace("_", "").Substring(4).ToLower();
                            miPar.strNameBO = formulario.txtInfProyectoClass.Text;
                            miPar.strNameSpaceEnt = formulario.txtNamespaceEntidades.Text;
                            miPar.strNameSpaceEnt = formulario.txtNamespaceEntidades.Text;
                            miPar.strNameSpaceRn = formulario.txtNamespaceNegocios.Text;
                            miPar.PathDesktop = PathDesktop;

                            switch (miEntorno)
                            {
                                case TipoEntorno.Java_Android:
                                    cDocJavaAndroid cDoc = new cDocJavaAndroid(miPar);
                                    Texto = Texto + "\r\n" + cDoc.crearDocEnt();
                                    Texto = Texto + "\r\n" + cDoc.crearDocRn();
                                    break;

                                default:
                                    throw new Exception("Aun no se han definido DOCUMENTACION para el entorno " + miEntorno);
                            }
                        }

                        #endregion Documentacion

                        TextoTotal = TextoTotal + Texto;
                    }
                }
            }

            //Ahora creamos la clase inicializadora de la COnexion

            #region Pruebas Unitarias

            if (formulario.chkReAlUnitTest.Checked)
            {
                if (miEntorno == TipoEntorno.CSharp
                    || miEntorno == TipoEntorno.CSharp_WinForms
                    || miEntorno == TipoEntorno.CSharp_WebForms
                    || miEntorno == TipoEntorno.CSharp_WCF
                    || miEntorno == TipoEntorno.CSharp_WebAPI
                    || miEntorno == TipoEntorno.CSharp_WSE)
                {
                    StringBuilder strClassConnTest = new StringBuilder();
                    strClassConnTest.AppendLine("namespace " + formulario.txtInfProyectoUnitTest.Text);
                    strClassConnTest.AppendLine("{");
                    strClassConnTest.AppendLine("\tpublic static class cParametrosTest");
                    strClassConnTest.AppendLine("\t{");
                    strClassConnTest.AppendLine("\t\tpublic static bool bChangeUserOnLogon = true;");
                    strClassConnTest.AppendLine("\t\tpublic static bool bUseIntegratedSecurity = false;");
                    strClassConnTest.AppendLine("\t\tpublic static string server = \"" + Principal.Servidor + "\";");
                    strClassConnTest.AppendLine("\t\tpublic static string puerto = \"" + Principal.Puerto + "\";");
                    strClassConnTest.AppendLine("\t\tpublic static string userDefault = \"" + Principal.UsuarioBD + "\";");
                    strClassConnTest.AppendLine("\t\tpublic static string passDefault = \"" + Principal.PasswordBD + "\";");
                    strClassConnTest.AppendLine("\t\tpublic static string bd = \"" + Principal.Catalogo + "\";");
                    strClassConnTest.AppendLine("\t\tpublic static string schema = \";");
                    strClassConnTest.AppendLine("\t}");
                    strClassConnTest.AppendLine("}");
                    CFunciones.CrearArchivo(strClassConnTest.ToString(), "cParametrosTest.cs", PathDesktop + "\\");
                }
            }

            #endregion Pruebas Unitarias

            //DATASET

            #region DataSets

            if (formulario.chkReAlDataSet.Checked)
            {
                cDataSet cDs = new cDataSet();
                if (miEntorno == TipoEntorno.CSharp
                    || miEntorno == TipoEntorno.CSharp_WinForms
                    || miEntorno == TipoEntorno.CSharp_WebForms
                    || miEntorno == TipoEntorno.CSharp_WCF
                    || miEntorno == TipoEntorno.CSharp_WSE)
                {
                    Texto = Texto + cDs.CrearDataSetCS(ref MyReAlListView, ref formulario);
                }
                if (miEntorno == TipoEntorno.VB_Net)
                {
                    //Texto = Texto & cDs.CrearDataSetVB(MyReAlListView, formulario)
                }
                TextoTotal = TextoTotal + Texto;
            }

            #endregion DataSets

            //DOCUMENTACION HOME

            #region Documentacion

            if (formulario.chkReAlCrearDoc.Checked)
            {
                var miPar = new cDocParams();
                miPar.miLista = MyReAlListView;
                miPar.strPrefijoEnt = formulario.txtPrefijoEntidades.Text;
                miPar.strPrefijoRn = formulario.txtPrefijoModelo.Text;
                miPar.strUrlWiki = formulario.txtNamespaceDocumentacion.Text;
                miPar.strNameSpaceEnt = formulario.txtNamespaceEntidades.Text;
                miPar.strNameSpaceEnt = formulario.txtNamespaceEntidades.Text;
                miPar.strNameSpaceRn = formulario.txtNamespaceNegocios.Text;
                miPar.PathDesktop = PathDesktop;
                cDocJavaAndroid cDoc = new cDocJavaAndroid(miPar);
                Texto = Texto + "\r\n" + cDoc.createHome();
            }

            #endregion Documentacion

            //Adicionales para NetCore Web API

            #region Adicionales para NetCore Web API

            if (miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger
                || miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2
                || miEntorno == TipoEntorno.CSharp_NetCore_V5_WebAPI_Client)
            {
                //Crear el Objeto CApiObject.cs
                string CabeceraTmp = formulario.strCabeceraPlantillaCodigo;
                CabeceraTmp = CabeceraTmp.Replace("%PAR_NOMBRE%", formulario.txtClaseParametrosClaseApiObject.Text);
                CabeceraTmp = CabeceraTmp.Replace("%PAR_DESCRIPCION%", "Clase que define el Objeto a ser retornado para Web APIs");

                System.Text.StringBuilder newTextoCApiObject = new System.Text.StringBuilder();

                newTextoCApiObject.AppendLine(CabeceraTmp);

                newTextoCApiObject.AppendLine("");
                newTextoCApiObject.AppendLine("namespace " + formulario.txtInfProyectoDal.Text);
                newTextoCApiObject.AppendLine("{");
                newTextoCApiObject.AppendLine("\tpublic class " + formulario.txtClaseParametrosClaseApiObject.Text);
                newTextoCApiObject.AppendLine("\t{");
                newTextoCApiObject.AppendLine("\t\tpublic string nombre { get; set; }");
                newTextoCApiObject.AppendLine("\t\tpublic dynamic datos { get; set; }");
                newTextoCApiObject.AppendLine("\t}");
                newTextoCApiObject.AppendLine("}");

                string PathDesktopApiObject = null;
                if (formulario.chkMoverResultados.Checked)
                {
                    PathDesktopApiObject = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\" + formulario.txtInfProyectoDal.Text;
                }
                else
                {
                    PathDesktopApiObject = formulario.txtPathProyecto.Text + "\\" + formulario.txtInfProyectoDal.Text;
                }
                if (Directory.Exists(PathDesktopApiObject) == false)
                {
                    Directory.CreateDirectory(PathDesktopApiObject);
                }
                CFunciones.CrearArchivo(newTextoCApiObject.ToString(), "CApiObject.cs", PathDesktopApiObject + "\\");

                //Configurar SWAGGER
                if (miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger)
                {
                    Texto = Texto + "\r\n\r\nConfiguracion en Startup.cs: \r\n" +
                        "public void ConfigureServices(IServiceCollection services)\r\n" +
                        "\tAddSwagger(services);" +
                        "\r\n" +
                        "public void Configure(IApplicationBuilder app, IHostingEnvironment env)\r\n" +
                        "\tapp.UseSwagger();\r\n" +
                        "\tapp.UseSwaggerUI(config =>\r\n" +
                        "\t{\r\n" +
                        "\t\tconfig.SwaggerEndpoint(\"/swagger/v1/swagger.json\",\"Angular API\");\r\n" +
                        "\t});\r\n" +
                        "\tapp.UseMvc();\r\n" +
                        "" +
                        "//KB: https://geeks.ms/jorge/2020/06/01/anadir-swagger-a-una-web-api-con-asp-net-core-3-1/" +
                        "private void AddSwagger(IServiceCollection services)" +
                        "{" +
                        "\tservices.AddSwaggerGen(options =>\r\n" +
                        "\t{\r\n" +
                        "\t\tvar groupName = \"v1\";\r\n" +
                        "\t\toptions.SwaggerDoc(groupName, new OpenApiInfo\r\n" +
                        "\t\t{\r\n" +
                        "\t\t\tTitle = $\"" + formulario.txtInfProyecto.Text + " {groupName}\",\r\n" +
                        "\t\t\tVersion = groupName,\r\n" +
                        "\t\t\tDescription = \"Web API para " + formulario.txtInfProyecto.Text + "\"\r\n" +
                        "\t\t\tContact = new OpenApiContact\r\n" +
                        "\t\t\t{\r\n" +
                        "\t\t\t\tName = \"ReAl\",\r\n" +
                        "\t\t\t\tEmail = \"re-al-@outlook.com\",\r\n" +
                        "\t\t\t\tUrl = new Uri(\"http://real.blog.bo/\"),\r\n" +
                        "\t\t\t}\r\n" +
                        "\t\t});\r\n" +
                        "\t\t\r\n" +
                        "\t\t// Set the comments path for the Swagger JSON and UI.\r\n" +
                        "\t\tvar xmlPath = Path.Combine(AppContext.BaseDirectory, \"" + formulario.txtInfProyecto.Text + ".xml\");\r\n" +
                        "\t\toptions.IncludeXmlComments(xmlPath);\r\n" +
                        "\t});\r\n" +
                        "}";
                }
            }

            #endregion Adicionales para NetCore Web API

            formulario.txtPreview.Text = "Archivos Generados: \r\n" + Texto;
        }
    }
}