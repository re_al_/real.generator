﻿using ReAl.Generator.App.AppClass;
using System.Data;
using System.Text;

namespace ReAl.Generator.App.AppCore
{
    public class CCodeSnippets
    {
        private string prefijoEntidades;
        private string nameClase;
        private string nameTabla;
        private string nameCol;
        private string tipoCol;

        public CCodeSnippets()
        {
        }

        public CCodeSnippets(string _prefijoEntidades, string _nameClase, string _nameTabla)
        {
            this.prefijoEntidades = _prefijoEntidades;
            this.nameClase = _nameClase;
            this.nameTabla = _nameTabla;
        }

        public string CrearAsignacion(DataTable dtColumns, CodeSnippet miSnippet, bool esVista)
        {
            string parUsuCre = "--";
            string parFecCre = "--";
            string parUsuMod = "--";
            string parFecMod = "--";
            string parApiEstado = "--";
            string parApiTransaccion = "--";

            nameClase = nameTabla.Substring(0, 1).ToUpper() + nameTabla.Substring(1, 2).ToLower() + nameTabla.Substring(3, 1).ToUpper() + nameTabla.Substring(4).ToLower();

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                if (nameCol.ToLower().Replace(nameTabla.ToLower(), "").Contains("api") & nameCol.ToLower().Replace(nameTabla.ToLower(), "").Contains("estado"))
                {
                    parApiEstado = nameCol;
                }
                else if (nameCol.ToLower().Replace(nameTabla.ToLower(), "").Contains("api") & nameCol.ToLower().Replace(nameTabla.ToLower(), "").Contains("transaccion"))
                {
                    parApiTransaccion = nameCol;
                }
                else if (nameCol.ToLower().Replace(nameTabla.ToLower(), "").Contains("usu") & nameCol.ToLower().Replace(nameTabla.ToLower(), "").Contains("cre"))
                {
                    parUsuCre = nameCol;
                }
                else if (nameCol.ToLower().Replace(nameTabla.ToLower(), "").Contains("usu") & nameCol.ToLower().Replace(nameTabla.ToLower(), "").Contains("mod"))
                {
                    parUsuMod = nameCol;
                }
                else if (nameCol.ToLower().Replace(nameTabla.ToLower(), "").Contains("fec") & nameCol.ToLower().Replace(nameTabla.ToLower(), "").Contains("cre"))
                {
                    parFecCre = nameCol;
                }
                else if (nameCol.ToLower().Replace(nameTabla.ToLower(), "").Contains("fec") & nameCol.ToLower().Replace(nameTabla.ToLower(), "").Contains("mod"))
                {
                    parFecMod = nameCol;
                }
            }

            if (parUsuCre == "--") parUsuCre = "";
            if (parFecCre == "--") parFecCre = "";
            if (parUsuMod == "--") parUsuMod = "";
            if (parFecMod == "--") parFecMod = "";
            if (parApiEstado == "--") parApiEstado = "";
            if (parApiTransaccion == "--") parApiTransaccion = "";

            string strLimpiarCampos = "//Para limpiar los campos";
            string strCodigoInsertar = "//Para INSERTAR valores en la Tabla " + nameTabla;
            string strCodigoAsignar = "//Para asignar valores desde la tabla " + nameTabla;
            string strResultadoInsert = "//Apropiar los datos para INSERTAR en la tabla " + nameTabla;
            string strResultadoUpdate = "//Apropiar los datos para ACTUALIZAR en la tabla " + nameTabla;
            string strResultadoEdit = "//Apropiar los controles desde la tabla " + nameTabla;

            //ASPX
            string strAspxResultadoDataGrid = "";
            string strAspxResultadoForm = "//Controles bootstrap para el Formulario";
            string strAspxResultadoFormEdit = "//Controles bootstrap para el Formulario de Edicion";
            string strAspxResultadoFormNew = "//Controles bootstrap para el Formulario Nuevo";

            for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
            {
                nameCol = dtColumns.Rows[iFil][0].ToString();
                tipoCol = dtColumns.Rows[iFil][1].ToString();
                bool esForeign = (dtColumns.Rows[iFil]["EsFK"].ToString() == "Yes");
                bool permiteNull = (dtColumns.Rows[iFil]["PermiteNull"].ToString() == "Yes");
                int longitudCol = 0;
                int.TryParse(dtColumns.Rows[iFil]["LONGITUD"].ToString(), out longitudCol);

                if (nameCol.Contains(parUsuCre) ||
                    nameCol.Contains(parFecCre) ||
                    nameCol.Contains(parApiEstado) ||
                    nameCol.Contains(parFecMod))
                {
                }
                else if (nameCol.Contains(parUsuMod) ||
                         nameCol.Contains(parApiTransaccion))
                {
                    strResultadoUpdate = strResultadoUpdate + "\r\n//objPag." + nameCol + " = \"\";";
                }
                else
                {
                    if (dtColumns.Rows[iFil]["EsIdentidad"].ToString() == "Yes")
                    {
                        strResultadoInsert = strResultadoInsert + "\r\n//objPag." + nameCol + " = txtNew_" + nameCol + ".Text;";
                        strResultadoUpdate = strResultadoUpdate + "\r\n//objPag." + nameCol + " = txtEdit_" + nameCol + ".Text;";
                        strResultadoEdit = strResultadoEdit + "\r\n//txtEdit_" + nameCol + ".Text = objPag." + nameCol + ";";
                    }
                    else
                    {
                        strAspxResultadoDataGrid = strAspxResultadoDataGrid + "\r\n<asp:BoundField ReadOnly=" +
                                               "\"True\" DataField=" +
                                               "\"" + nameCol + "\" HeaderText=" +
                                               "\"" + nameCol + "\" ShowHeader=" +
                                               "\"false\" >\r\n" +
                                               "\t<ItemStyle HorizontalAlign=\"" +
                                               "Justify\" ></ItemStyle>\r\n" +
                                               "\t<HeaderStyle HorizontalAlign=\"" +
                                               "Center\" ></HeaderStyle>\r\n" +
                                               "</asp:BoundField>";

                        switch (tipoCol.ToUpper())
                        {
                            case "BOOLEAN":
                                break;

                            case "BOOL":
                                strLimpiarCampos = strLimpiarCampos + "\r\nchk" + nameCol.Replace(nameClase, "") + ".Checked = false;";
                                strCodigoInsertar = strCodigoInsertar + "\r\nobjPag." + nameCol + " = chk" + nameCol.Replace(nameClase, "") + ".Checked;";
                                strCodigoAsignar = strCodigoAsignar + "\r\nchk" + nameCol.Replace(nameClase, "") + ".Checked = objPag." + nameCol + ";";
                                strResultadoInsert = strResultadoInsert + "\r\nobjPag." + nameCol + " = chkNew_" + nameCol + ".Checked;";
                                strResultadoUpdate = strResultadoUpdate + "\r\nobjPag." + nameCol + " = chkEdit_" + nameCol + ".Checked;";
                                strResultadoEdit = strResultadoEdit + "\r\nchkEdit_" + nameCol + ".Checked = objPag." + nameCol + ";";
                                break;

                            case "INT":
                                strLimpiarCampos = strLimpiarCampos + "\r\ntxt" + nameCol.Replace(nameClase, "") + ".Text = \"\";";
                                strCodigoAsignar = strCodigoAsignar + "\r\ntxt" + nameCol.Replace(nameClase, "") + ".Text = objPag." + nameCol + ".ToString();";
                                strResultadoEdit = strResultadoEdit + "\r\ntxtEdit_" + nameCol + ".Text = objPag." + nameCol + ";";

                                strCodigoInsertar = strCodigoInsertar + "\r\nobjPag." + nameCol + " = rnModificar.GetColumnType(txt" + nameCol + ".Text, " + prefijoEntidades + nameClase + ".Fields." + nameCol + ");";
                                strResultadoInsert = strResultadoInsert + "\r\nobjPag." + nameCol + " = rn.GetColumnType(txtNew_" + nameCol + ".Text, " + prefijoEntidades + nameClase + ".Fields." + nameCol + ");";
                                strResultadoUpdate = strResultadoUpdate + "\r\nobjPag." + nameCol + " = rn.GetColumnType(txtEdit_" + nameCol + ".Text, " + prefijoEntidades + nameClase + ".Fields." + nameCol + ");";
                                break;

                            default:
                                strLimpiarCampos = strLimpiarCampos + "\r\ntxt" + nameCol.Replace(nameClase, "") + ".Text = \"\";";
                                strCodigoInsertar = strCodigoInsertar + "\r\nobjPag." + nameCol + " = rnModificar.GetColumnType(txt" + nameCol + ".Text, " + prefijoEntidades + nameClase + ".Fields." + nameCol + ");";
                                strCodigoAsignar = strCodigoAsignar + "\r\ntxt" + nameCol.Replace(nameClase, "") + ".Text = objPag." + nameCol + ".ToString();";
                                strResultadoInsert = strResultadoInsert + "\r\nobjPag." + nameCol + " = rn.GetColumnType(txtNew_" + nameCol + ".Text, " + prefijoEntidades + nameClase + ".Fields." + nameCol + ");";
                                strResultadoUpdate = strResultadoUpdate + "\r\nobjPag." + nameCol + " = rn.GetColumnType(txtEdit_" + nameCol + ".Text, " + prefijoEntidades + nameClase + ".Fields." + nameCol + ");";
                                strResultadoEdit = strResultadoEdit + "\r\ntxtEdit_" + nameCol + ".Text = objPag." + nameCol + ";";
                                break;
                        }

                        //ASPX
                        if (tipoCol.ToUpper().Contains("DATE"))
                        {
                            strAspxResultadoForm = strAspxResultadoForm + "\r\n<div class=\"col-md-12\">\r\n<div class=\"form-group\">\r\n<asp:Literal id=\"lbl_" + nameCol + "\" runat=\"server\" text=\"" + nameCol + ":\"/>\r\n<div class=\"input-group date\" id=\"datetimepicker1\">\r\n    <asp:TextBox id=\"txt_" + nameCol + "\" data-date-format=\"DD/MM/YYYY\" placeholder=\"DD/MM/YYYYY\"\r\n        Class=\"form-control datepicker\" CssClass=\"form-control datepicker\" runat=\"server\"/>\r\n    <span class=\"input-group-addon\">\r\n        <span class=\"glyphicon glyphicon-calendar\"></span>\r\n    </span>\r\n</div>\r\n</div>\r\n</div>\r\n";
                            strAspxResultadoFormNew = strAspxResultadoFormNew + "\r\n<div class=\"col-md-12\">\r\n<div class=\"form-group\">\r\n<asp:Literal id=\"lbl_" + nameCol + "\" runat=\"server\" text=\"" + nameCol + ":\"/>\r\n<div class=\"input-group date\" id=\"datetimepicker1\">\r\n    <asp:TextBox id=\"txtNew_" + nameCol + "\" data-date-format=\"DD/MM/YYYY\" placeholder=\"DD/MM/YYYYY\"\r\n        Class=\"form-control datepicker\" CssClass=\"form-control datepicker\" runat=\"server\"/>\r\n    <span class=\"input-group-addon\">\r\n        <span class=\"glyphicon glyphicon-calendar\"></span>\r\n    </span>\r\n</div>\r\n</div>\r\n</div>\r\n";
                            strAspxResultadoFormEdit = strAspxResultadoFormEdit + "\r\n<div class=\"col-md-12\">\r\n<div class=\"form-group\">\r\n<asp:Literal id=\"lbl_" + nameCol + "\" runat=\"server\" text=\"" + nameCol + ":\"/>\r\n<div class=\"input-group date\" id=\"datetimepicker1\">\r\n    <asp:TextBox id=\"txtEdit_" + nameCol + "\" data-date-format=\"DD/MM/YYYY\" placeholder=\"DD/MM/YYYYY\"\r\n        Class=\"form-control datepicker\" CssClass=\"form-control datepicker\" runat=\"server\"/>\r\n    <span class=\"input-group-addon\">\r\n        <span class=\"glyphicon glyphicon-calendar\"></span>\r\n    </span>\r\n</div>\r\n</div>\r\n</div>\r\n";
                        }
                        else
                        {
                            if (esForeign)
                            {
                                strAspxResultadoForm = strAspxResultadoForm + "\r\n<div class=\"col-md-12\">\r\n<div class=\"form-group\">\r\n    <asp:Literal id=\"lbl_" + nameCol + "\" runat=\"server\" text=\"" + nameCol + ":\"/>\r\n    <asp:DropDownList ID=\"ddl_" + nameCol + "\" runat=\"server\" \r\n        class=\"form-control selectpicker\" data-live-search=\"true\" \r\n        data-select-on-tab=\"true\" data-size=\"10\"\r\n        CssClass=\"form-control\" />\r\n</div>\r\n</div>\r\n";
                                strAspxResultadoFormNew = strAspxResultadoFormNew + "\r\n<div class=\"col-md-12\">\r\n<div class=\"form-group\">\r\n    <asp:Literal id=\"lbl_" + nameCol + "\" runat=\"server\" text=\"" + nameCol + ":\"/>\r\n    <asp:DropDownList ID=\"ddlNew_" + nameCol + "\" runat=\"server\" \r\n        class=\"form-control selectpicker\" data-live-search=\"true\" \r\n        data-select-on-tab=\"true\" data-size=\"10\"\r\n        CssClass=\"form-control\" />\r\n</div>\r\n</div>\r\n";
                                strAspxResultadoFormEdit = strAspxResultadoFormEdit + "\r\n<div class=\"col-md-12\">\r\n<div class=\"form-group\">\r\n    <asp:Literal id=\"lbl_" + nameCol + "\" runat=\"server\" text=\"" + nameCol + ":\"/>\r\n    <asp:DropDownList ID=\"ddlEdit_" + nameCol + "\" runat=\"server\" \r\n        class=\"form-control selectpicker\" data-live-search=\"true\" \r\n        data-select-on-tab=\"true\" data-size=\"10\"\r\n        CssClass=\"form-control\" />\r\n</div>\r\n</div>\r\n";
                            }
                            else
                            {
                                strAspxResultadoForm = strAspxResultadoForm + "\r\n<div class=\"col-md-12\">\r\n<div class=\"form-group\">\r\n    <asp:Literal id=\"lbl_" + nameCol + "\" runat=\"server\" text=\"" + nameCol + ":\"/>\r\n    <asp:TextBox id=\"txt_" + nameCol + "\" \r\n        data-parsley-maxlength=\"" + longitudCol + "\" " + (permiteNull ? "" : "required=\"\"") + "\r\n        data-parsley-group=\"validation-new\"\r\n        Class=\"form-control\" CssClass=\"form-control\"\r\n        runat=\"server\" MaxLength=\"" + longitudCol + "\"/>\r\n</div>\r\n</div>\r\n";
                                strAspxResultadoFormNew = strAspxResultadoFormNew + "\r\n<div class=\"col-md-12\">\r\n<div class=\"form-group\">\r\n    <asp:Literal id=\"lbl_" + nameCol + "\" runat=\"server\" text=\"" + nameCol + ":\"/>\r\n    <asp:TextBox id=\"txtNew_" + nameCol + "\" \r\n        data-parsley-maxlength=\"" + longitudCol + "\" " + (permiteNull ? "" : "required=\"\"") + "\r\n        data-parsley-group=\"validation-new\"\r\n        Class=\"form-control\" CssClass=\"form-control\"\r\n        runat=\"server\" MaxLength=\"" + longitudCol + "\"/>\r\n</div>\r\n</div>\r\n";
                                strAspxResultadoFormEdit = strAspxResultadoFormEdit + "\r\n<div class=\"col-md-12\">\r\n<div class=\"form-group\">\r\n    <asp:Literal id=\"lbl_" + nameCol + "\" runat=\"server\" text=\"" + nameCol + ":\"/>\r\n    <asp:TextBox id=\"txtEdit_" + nameCol + "\" \r\n        data-parsley-maxlength=\"" + longitudCol + "\" " + (permiteNull ? "" : "required=\"\"") + "\r\n        data-parsley-group=\"validation-edit\"\r\n        Class=\"form-control\" CssClass=\"form-control\"\r\n        runat=\"server\" MaxLength=\"" + longitudCol + "\"/>\r\n</div>\r\n</div>\r\n";
                            }
                        }
                    }
                }
            }

            string sbAudit = "<asp:TemplateField HeaderText=\"Acciones\">" +
                        "\r\n	<ItemStyle HorizontalAlign=\"Center\" VerticalAlign=\"Middle\"></ItemStyle>" +
                        "\r\n	<HeaderStyle></HeaderStyle>" +
                        "\r\n	<ItemTemplate>" +
                        "\r\n		<asp:LinkButton ID=\"ImbVer\" CommandName=\"detalles\" CommandArgument='<%# Bind(\"\") %>' runat=\"server\"" +
                        "\r\n						ToolTip=\"Ver detalles del Registro\"" +
                        "\r\n						CssClass=\"btn btn-success btn-circle\" Text=\"<i class='fa fa-indent'></i>\"/>" +
                        "\r\n		<asp:LinkButton ID=\"ImbModificar\" CommandName=\"modificar\" CommandArgument='<%# Bind(\"\") %>' runat=\"server\"" +
                        "\r\n						ToolTip=\"Modificar el registro\" " +
                        "\r\n						CssClass=\"btn btn-primary btn-circle\" Text=\"<i class='fa fa-edit'></i>\"/>" +
                        "\r\n		<asp:LinkButton ID=\"ImbEliminar\" CommandName=\"eliminar\" CommandArgument='<%# Bind(\"\") %>' runat=\"server\"" +
                        "\r\n						ToolTip=\"Eliminar la asignacion\" OnClientClick=\"return confirm('¿Esta seguro que desea eliminar el registro?');\"" +
                        "\r\n						CssClass=\"btn btn-danger btn-circle\" Text=\"<i class='fa fa-eraser'></i>\"/>" +
                        "\r\n		<button type=\"button\" class=\"btn btn-warning btn-circle\" data-container=\"body\"" +
                        "\r\n				data-toggle=\"popover\" data-trigger=\"click\" " +
                        "\r\n				data-placement=\"left\" data-html=\"true\" title=\"Auditoria\"" +
                        "\r\n				data-content='<b>Usuario Creacion:</b> <%# Eval(\"" + parUsuCre + "\") %> <br />" +
                        "\r\n								<b>Fecha Creacion:</b> <%# Eval(\"" + parFecCre + "\") %> <br />" +
                        "\r\n								<b>Usuario Modificacion:</b> <%# Eval(\"" + parUsuMod + "\") %> <br />" +
                        "\r\n								<b>Fecha Modificacion:</b> <%# Eval(\"" + parFecMod + "\") %> <br />" +
                        "\r\n								<b>Estado:</b> <%# Eval(\"" + parApiEstado + "\") %> '>" +
                        "\r\n			<i class='fa fa-search-plus'></i>" +
                        "\r\n		</button>" +
                        "\r\n	</ItemTemplate>" +
                        "\r\n</asp:TemplateField>";

            strAspxResultadoDataGrid = strAspxResultadoDataGrid + "\r\n\r\n" + sbAudit;

            switch (miSnippet)
            {
                case CodeSnippet.BootStrap_Doc_WebForms:
                    return strAspxResultadoForm + "\r\n\r\n\r\n" + strCodigoInsertar + "\r\n\r\n\r\n" + strLimpiarCampos + "\r\n\r\n\r\n" + strCodigoAsignar;
                    break;

                case CodeSnippet.BootStrap_Abm_WebForms:
                    return strAspxResultadoFormNew + "\r\n\r\n\r\n" + strResultadoInsert + "\r\n\r\n\r\n" + strAspxResultadoFormEdit + "\r\n\r\n\r\n" + strResultadoUpdate + "\r\n\r\n\r\n" + strResultadoEdit;
                    break;

                case CodeSnippet.BootStrap_Datagrid_WebForms:
                    return strAspxResultadoDataGrid;
                    break;
            }
            return "";
        }

        public string CrearControlesRestriccion()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("\t<div class=\"row\" runat=\"server\" Visible=\"False\">");
            sb.AppendLine("\t\t<div class=\"col-lg-4 col-md-4\">");
            sb.AppendLine("\t\t\t<div class=\"form-group\">");
            sb.AppendLine("\t\t\t\t<label id=\"Label7\" runat=\"server\">Institucion:</label>");
            sb.AppendLine("\t\t\t\t<asp:DropDownList ID=\"ddl_Institucioncin\" runat=\"server\" AutoPostBack=\"True\"");
            sb.AppendLine("\t\t\t\t  OnSelectedIndexChanged=\"ddl_Institucioncin_OnSelectedIndexChanged\"");
            sb.AppendLine("\t\t\t\t  class=\"form-control selectpicker\" data-live-search=\"true\"");
            sb.AppendLine("\t\t\t\t  data-none-selected-text=\"No existen datos\"");
            sb.AppendLine("\t\t\t\t  data-select-on-tab=\"true\" data-size=\"7\"");
            sb.AppendLine("\t\t\t\t  CssClass=\"form-control\">");
            sb.AppendLine("\t\t\t\t</asp:DropDownList>");
            sb.AppendLine("\t\t\t</div>");
            sb.AppendLine("\t\t</div>");
            sb.AppendLine("\t\t<div class=\"col-lg-4 col-md-4\">");
            sb.AppendLine("\t\t\t<div class=\"form-group\">");
            sb.AppendLine("\t\t\t\t<label id=\"Label2\" runat=\"server\">Gerencia Administrativa:</label>");
            sb.AppendLine("\t\t\t\t<asp:DropDownList ID=\"ddl_Gacga\" runat=\"server\" AutoPostBack=\"True\"");
            sb.AppendLine("\t\t\t\t  OnSelectedIndexChanged=\"ddl_Gacga_OnSelectedIndexChanged\"");
            sb.AppendLine("\t\t\t\t  class=\"form-control selectpicker\" data-live-search=\"true\"");
            sb.AppendLine("\t\t\t\t  data-none-selected-text=\"No existen datos\"");
            sb.AppendLine("\t\t\t\t  data-select-on-tab=\"true\" data-size=\"7\"");
            sb.AppendLine("\t\t\t\t  CssClass=\"form-control\">");
            sb.AppendLine("\t\t\t\t</asp:DropDownList>");
            sb.AppendLine("\t\t\t</div>");
            sb.AppendLine("\t\t</div>");
            sb.AppendLine("\t\t<div class=\"col-lg-4 col-md-4\">");
            sb.AppendLine("\t\t\t<div class=\"form-group\" runat=\"server\" id=\"pnlUeCue\">");
            sb.AppendLine("\t\t\t\t<label id=\"Label5\" runat=\"server\">Unidad Ejecutora:</label>");
            sb.AppendLine("\t\t\t\t<asp:DropDownList ID=\"ddl_Uecue\" runat=\"server\" AutoPostBack=\"True\"");
            sb.AppendLine("\t\t\t\t  OnSelectedIndexChanged=\"ddl_Uecue_OnSelectedIndexChanged\"");
            sb.AppendLine("\t\t\t\t  class=\"form-control selectpicker\" data-live-search=\"true\"");
            sb.AppendLine("\t\t\t\t  data-none-selected-text=\"No existen datos\"");
            sb.AppendLine("\t\t\t\t  data-select-on-tab=\"true\" data-size=\"7\"");
            sb.AppendLine("\t\t\t\t  CssClass=\"form-control\">");
            sb.AppendLine("\t\t\t\t</asp:DropDownList>");
            sb.AppendLine("\t\t\t</div>");
            sb.AppendLine("\t\t</div>");
            sb.AppendLine("\t</div>");
            sb.AppendLine("\t");
            sb.AppendLine("\t/// <summary>");
            sb.AppendLine("\t///     Funcion encargada de obtener los datos de las Instituciones");
            sb.AppendLine("\t/// </summary>");
            sb.AppendLine("\tprivate void CargarDdlInstitucionClaInstituciones()");
            sb.AppendLine("\t{");
            sb.AppendLine("\t    try");
            sb.AppendLine("\t    {");
            sb.AppendLine("\t        CControlHelper.CargarClaInstituciones(ref ddl_Institucioncin);");
            sb.AppendLine("\t    }");
            sb.AppendLine("\t    catch (Exception exp)");
            sb.AppendLine("\t    {");
            sb.AppendLine("\t        var master = (Main)Master;");
            sb.AppendLine("\t        master?.MostrarPopUp(this, exp);");
            sb.AppendLine("\t    }");
            sb.AppendLine("\t}");
            sb.AppendLine("\t");
            sb.AppendLine("\t/// <summary>");
            sb.AppendLine("\t///     Funcion encargada de obtener los datos de las Gerencias Administrativas");
            sb.AppendLine("\t/// </summary>");
            sb.AppendLine("\tprivate void CargarDdlGaClaGerenciasAdministrativas()");
            sb.AppendLine("\t{");
            sb.AppendLine("\t    try");
            sb.AppendLine("\t    {");
            sb.AppendLine("\t        if (ddl_Institucioncin.Items.Count > 0)");
            sb.AppendLine("\t        {");
            sb.AppendLine("\t            CControlHelper.CargarClaGerencias(ref ddl_Gacga, ddl_Institucioncin.SelectedValue);");
            sb.AppendLine("\t        }");
            sb.AppendLine("\t        else");
            sb.AppendLine("\t        {");
            sb.AppendLine("\t            ddl_Gacga.Items.Clear();");
            sb.AppendLine("\t            ddl_Gacga.DataSource = null;");
            sb.AppendLine("\t            ddl_Gacga.DataBind();");
            sb.AppendLine("\t            ddl_Uecue.Items.Clear();");
            sb.AppendLine("\t            ddl_Uecue.DataSource = null;");
            sb.AppendLine("\t            ddl_Uecue.DataBind();");
            sb.AppendLine("\t        }");
            sb.AppendLine("\t    }");
            sb.AppendLine("\t    catch (Exception exp)");
            sb.AppendLine("\t    {");
            sb.AppendLine("\t        var master = (Main)Master;");
            sb.AppendLine("\t        master?.MostrarPopUp(this, exp);");
            sb.AppendLine("\t    }");
            sb.AppendLine("\t}");
            sb.AppendLine("\t");
            sb.AppendLine("\t/// <summary>");
            sb.AppendLine("\t///     Funcion encargada de obtener los datos de las Unidades Ejecutoras");
            sb.AppendLine("\t/// </summary>");
            sb.AppendLine("\tprivate void CargarDdlUeClaUnidadesEjecutoras()");
            sb.AppendLine("\t{");
            sb.AppendLine("\t    try");
            sb.AppendLine("\t    {");
            sb.AppendLine("\t        if (ddl_Gacga.Items.Count > 0)");
            sb.AppendLine("\t        {");
            sb.AppendLine("\t            CControlHelper.CargarClaEjecutoras(ref ddl_Uecue, ddl_Gacga.SelectedValue);");
            sb.AppendLine("\t        }");
            sb.AppendLine("\t        else");
            sb.AppendLine("\t        {");
            sb.AppendLine("\t            ddl_Uecue.Items.Clear();");
            sb.AppendLine("\t            ddl_Uecue.DataSource = null;");
            sb.AppendLine("\t            ddl_Uecue.DataBind();");
            sb.AppendLine("\t            dtgListado.DataSource = null;");
            sb.AppendLine("\t            dtgListado.DataBind();");
            sb.AppendLine("\t        }");
            sb.AppendLine("\t    }");
            sb.AppendLine("\t    catch (Exception exp)");
            sb.AppendLine("\t    {");
            sb.AppendLine("\t        var master = (Main)Master;");
            sb.AppendLine("\t        master?.MostrarPopUp(this, exp);");
            sb.AppendLine("\t    }");
            sb.AppendLine("\t}");
            sb.AppendLine("\t");
            sb.AppendLine("\tprotected void ddl_Institucioncin_OnSelectedIndexChanged(object sender, EventArgs e)");
            sb.AppendLine("\t{");
            sb.AppendLine("\t    CargarDdlGaClaGerenciasAdministrativas();");
            sb.AppendLine("\t    CargarDdlUeClaUnidadesEjecutoras();");
            sb.AppendLine("\t}");
            sb.AppendLine("\t");
            sb.AppendLine("\tprotected void ddl_Gacga_OnSelectedIndexChanged(object sender, EventArgs e)");
            sb.AppendLine("\t{");
            sb.AppendLine("\t    CargarDdlUeClaUnidadesEjecutoras();");
            sb.AppendLine("\t}");
            sb.AppendLine("\t");
            sb.AppendLine("\tprotected void ddl_Uecue_OnSelectedIndexChanged(object sender, EventArgs e)");
            sb.AppendLine("\t{");
            sb.AppendLine("\t    throw new NotImplementedException();");
            sb.AppendLine("\t}");

            return sb.ToString();
        }
    }
}