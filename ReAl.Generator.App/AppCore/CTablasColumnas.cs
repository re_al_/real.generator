﻿using ReAl.Conn;
using ReAl.Generator.App.AppClass;
using ReAlFind_Control;
using System;
using System.Data;
using System.Text;

namespace ReAl.Generator.App.AppCore
{
    public static class CTablasColumnas
    {
        public static DataTable ObtenerColumnasTabla(TipoEntorno miEntorno, FPrincipal formulario, string nameSchema, string idTabla)
        {
            var dtColumns = new DataTable();

            if (miEntorno == TipoEntorno.CSharp
                || miEntorno == TipoEntorno.CSharp_WinForms
                || miEntorno == TipoEntorno.CSharp_WebForms
                || miEntorno == TipoEntorno.CSharp_WCF
                || miEntorno == TipoEntorno.CSharp_WSE
                || miEntorno == TipoEntorno.CSharp_WebAPI
                || miEntorno == TipoEntorno.CSharp_NetCore_Ef
                || miEntorno == TipoEntorno.CSharp_NetCore_V2
                || miEntorno == TipoEntorno.CSharp_NetCore_V5
                || miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2
                || miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger
                || miEntorno == TipoEntorno.CSharp_NetCore_V5_WebAPI_Client)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.PascalCase);
                        break;
                }
            }
            else if (miEntorno == TipoEntorno.VB_Net
                || miEntorno == TipoEntorno.VB_Net_WCF)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.VbNet, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.VbNet, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.VbNet, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.VbNet, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.VbNet, TipoColumnas.PascalCase);
                        break;
                }
            }
            else if (miEntorno == TipoEntorno.Java_Android)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.JavaAndroid, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.JavaAndroid, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.JavaAndroid, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.JavaAndroid, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.JavaAndroid, TipoColumnas.PascalCase);
                        break;
                }
            }
            else if (miEntorno == TipoEntorno.Java_Clasico)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.Java, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.Java, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.Java, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.Java, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.Java, TipoColumnas.PascalCase);
                        break;
                }
            }
            else if (miEntorno == TipoEntorno.PHP_CodeIgniter || miEntorno == TipoEntorno.PHP_CodeIgniter_Models_API)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.PascalCase);
                        break;
                }
            }
            else if (miEntorno == TipoEntorno.PHP_Clasico)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.PascalCase);
                        break;
                }
            }
            else if (miEntorno == TipoEntorno.Python)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.Python, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.Python, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.Python, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.Python, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.Python, TipoColumnas.PascalCase);
                        break;
                }
            }
            else if (miEntorno == TipoEntorno.Angular_CLI)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.PascalCase);
                        break;
                }
            }
            else if (miEntorno == TipoEntorno.Feathers_JS)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.FeathersJS, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.FeathersJS, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.FeathersJS, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.FeathersJS, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnas(nameSchema, idTabla, Languaje.FeathersJS, TipoColumnas.PascalCase);
                        break;
                }
            }
            else
            {
                throw new Exception("No se tiene la DEFINCION DE COLUMNAS para " + miEntorno);
            }
            return dtColumns;
        }

        public static DataTable ObtenerColumnasForaneaTabla(TipoEntorno miEntorno, FPrincipal formulario, string nameSchema, string idTabla)
        {
            var dtColumns = new DataTable();

            if (miEntorno == TipoEntorno.CSharp
                || miEntorno == TipoEntorno.CSharp_WinForms
                || miEntorno == TipoEntorno.CSharp_WebForms
                || miEntorno == TipoEntorno.CSharp_WCF
                || miEntorno == TipoEntorno.CSharp_WSE
                || miEntorno == TipoEntorno.CSharp_WebAPI
                || miEntorno == TipoEntorno.CSharp_NetCore_Ef
                || miEntorno == TipoEntorno.CSharp_NetCore_V2
                || miEntorno == TipoEntorno.CSharp_NetCore_V5
                || miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2
                || miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger
                || miEntorno == TipoEntorno.CSharp_NetCore_V5_WebAPI_Client)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.PascalCase);
                        break;
                }
            }
            else if (miEntorno == TipoEntorno.VB_Net
                || miEntorno == TipoEntorno.VB_Net_WCF)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.VbNet, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.VbNet, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.VbNet, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.VbNet, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.VbNet, TipoColumnas.PascalCase);
                        break;
                }
            }
            else if (miEntorno == TipoEntorno.Java_Android)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.JavaAndroid, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.JavaAndroid, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.JavaAndroid, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.JavaAndroid, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.JavaAndroid, TipoColumnas.PascalCase);
                        break;
                }
            }
            else if (miEntorno == TipoEntorno.Java_Clasico)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.Java, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.Java, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.Java, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.Java, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.Java, TipoColumnas.PascalCase);
                        break;
                }
            }
            else if (miEntorno == TipoEntorno.PHP_CodeIgniter || miEntorno == TipoEntorno.PHP_CodeIgniter_Models_API)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.PascalCase);
                        break;
                }
            }
            else if (miEntorno == TipoEntorno.PHP_Clasico)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.PHPCodeIgniter, TipoColumnas.PascalCase);
                        break;
                }
            }
            else if (miEntorno == TipoEntorno.Python)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.Python, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.Python, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.Python, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.Python, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.Python, TipoColumnas.PascalCase);
                        break;
                }
            }
            else if (miEntorno == TipoEntorno.Angular_CLI)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.PascalCase);
                        break;
                }
            }
            else if (miEntorno == TipoEntorno.Feathers_JS)
            {
                switch (formulario.cmbTipoColumnas.SelectedIndex)
                {
                    case 0:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.FeathersJS, TipoColumnas.PrimeraMayuscula);
                        break;

                    case 1:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.FeathersJS, TipoColumnas.Minusculas);
                        break;

                    case 2:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.FeathersJS, TipoColumnas.Mayusculas);
                        break;

                    case 3:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.FeathersJS, TipoColumnas.CamelCase);
                        break;

                    case 4:
                        dtColumns = ObtenerListadoColumnasForaneas(nameSchema, idTabla, Languaje.FeathersJS, TipoColumnas.PascalCase);
                        break;
                }
            }
            else
            {
                throw new Exception("No se tiene la DEFINCION DE COLUMNAS FORANEAS para " + miEntorno);
            }
            return dtColumns;
        }

        public static void ObtenerListadoTablas(ref ReAlListView ReAlListView1, ref ReAlListView ReAlListView2, ref ReAlListView ReAlListView3, ref ReAlListView ReAlListView4, string strFiltro, bool chkSelectAll)
        {
            CConeccion clsCon = new CConeccion();

            ReAlListView1.Items.Clear();
            string queryTables1 = "";
            string queryTables2 = "";
            string queryTables3 = "";
            string queryTables4 = "";

            switch (Principal.DBMS)
            {
                case TipoBD.SQLServer:
                    queryTables1 = "SELECT name, object_id, 'dbo' as namespace, CASE type WHEN 'U' THEN 'Tabla' WHEN 'V' THEN 'Vista' WHEN 'TR' THEN 'Trigger' ELSE 'Otro' END as Tipo FROM sys.objects WHERE (type = 'U' AND name NOT LIKE 'sys%')";
                    queryTables2 = "SELECT name, object_id, 'dbo' as namespace, CASE type WHEN 'U' THEN 'Tabla' WHEN 'V' THEN 'Vista' WHEN 'TR' THEN 'Trigger' ELSE 'Otro' END as Tipo FROM sys.objects WHERE (type = 'V' AND name NOT LIKE 'sys%')";
                    queryTables3 = "SELECT name, object_id, 'dbo' as namespace, CASE type WHEN 'U' THEN 'Tabla' WHEN 'V' THEN 'Vista' WHEN 'TR' THEN 'Trigger' ELSE 'Otro' END as Tipo FROM sys.objects WHERE (type = 'P' AND name NOT LIKE 'sys%')";
                    queryTables4 = "SELECT name, object_id, 'dbo' as namespace, CASE type WHEN 'U' THEN 'Tabla' WHEN 'V' THEN 'Vista' WHEN 'TR' THEN 'Trigger' ELSE 'Otro' END as Tipo FROM sys.objects WHERE (type = 'TR' AND name NOT LIKE 'sys%')";
                    queryTables1 = queryTables1 + " AND UPPER(name) LIKE '" + strFiltro.ToUpper() + "%' ORDER BY name ASC";
                    queryTables2 = queryTables2 + " AND UPPER(name) LIKE '" + strFiltro.ToUpper() + "%' ORDER BY name ASC";
                    queryTables3 = queryTables3 + " AND UPPER(name) LIKE '" + strFiltro.ToUpper() + "%' ORDER BY name ASC";
                    queryTables4 = queryTables4 + " AND UPPER(name) LIKE '" + strFiltro.ToUpper() + "%' ORDER BY name ASC";

                    break;

                case TipoBD.SQLServer2000:
                    queryTables1 = "SELECT name, object_id, 'dbo' as namespace, CASE type WHEN 'U' THEN 'Tabla' WHEN 'V' THEN 'Vista' WHEN 'TR' THEN 'Trigger' ELSE 'Otro' END as Tipo FROM sysobjects WHERE (type = 'U' AND name NOT LIKE 'sys%')";
                    queryTables2 = "SELECT name, object_id, 'dbo' as namespace, CASE type WHEN 'U' THEN 'Tabla' WHEN 'V' THEN 'Vista' WHEN 'TR' THEN 'Trigger' ELSE 'Otro' END as Tipo FROM sysobjects WHERE (type = 'V' AND name NOT LIKE 'sys%')";
                    queryTables3 = "SELECT name, object_id, 'dbo' as namespace, CASE type WHEN 'U' THEN 'Tabla' WHEN 'V' THEN 'Vista' WHEN 'TR' THEN 'Trigger' ELSE 'Otro' END as Tipo FROM sysobjects WHERE (type = 'P' AND name NOT LIKE 'sys%')";
                    queryTables4 = "SELECT name, object_id, 'dbo' as namespace, CASE type WHEN 'U' THEN 'Tabla' WHEN 'V' THEN 'Vista' WHEN 'TR' THEN 'Trigger' ELSE 'Otro' END as Tipo FROM sysobjects WHERE (type = 'TR' AND name NOT LIKE 'sys%')";
                    queryTables1 = queryTables1 + " AND UPPER(name) LIKE '" + strFiltro.ToUpper() + "%' ORDER BY name ASC";
                    queryTables2 = queryTables2 + " AND UPPER(name) LIKE '" + strFiltro.ToUpper() + "%' ORDER BY name ASC";
                    queryTables3 = queryTables3 + " AND UPPER(name) LIKE '" + strFiltro.ToUpper() + "%' ORDER BY name ASC";
                    queryTables4 = queryTables4 + " AND UPPER(name) LIKE '" + strFiltro.ToUpper() + "%' ORDER BY name ASC";

                    break;

                case TipoBD.Oracle:
                    queryTables1 = "SELECT all_objects.object_name AS name, all_objects.object_name AS object_id, 'public' as namespace,  all_objects.object_type AS TIPO, TO_CHAR(all_objects.created, 'YYYY-MM-DD') as CREADO, TO_CHAR(all_objects.last_ddl_time, 'YYYY-MM-DD') AS ULTIMO_DDL FROM(all_objects)  WHERE all_objects.object_name NOT LIKE 'ALL%'  AND all_objects.object_name NOT LIKE 'USR%'  AND all_objects.object_name NOT LIKE 'DBA%'  AND all_objects.object_name NOT LIKE '%$%'  AND all_objects.status = 'VALID'  AND all_objects.owner = '" + Principal.Catalogo + "'  AND all_objects.object_type IN ('TABLE')";

                    queryTables2 = "SELECT all_objects.object_name AS name, all_objects.object_name AS object_id, 'public' as namespace, all_objects.object_type AS TIPO FROM(all_objects)  WHERE all_objects.object_name NOT LIKE 'ALL%'  AND all_objects.object_name NOT LIKE 'USR%'  AND all_objects.object_name NOT LIKE 'DBA%'  AND all_objects.object_name NOT LIKE '%$%'  AND all_objects.status = 'VALID'  AND all_objects.owner = '" + Principal.Catalogo + "'  AND all_objects.object_type IN ('VIEW')";

                    queryTables3 = "SELECT all_objects.object_name AS name, all_objects.object_name AS object_id, 'public' as namespace, all_objects.object_type AS TIPO FROM(all_objects)  WHERE all_objects.object_name NOT LIKE 'ALL%'  AND all_objects.object_name NOT LIKE 'USR%'  AND all_objects.object_name NOT LIKE 'DBA%'  AND all_objects.object_name NOT LIKE '%$%'  AND all_objects.status = 'VALID'  AND all_objects.owner = '" + Principal.Catalogo + "'  AND all_objects.object_type IN ('PROCEDURE')";

                    queryTables4 = "SELECT all_objects.object_name AS name, all_objects.object_name AS object_id, 'public' as namespace, all_objects.object_type AS TIPO FROM(all_objects)  WHERE all_objects.object_name NOT LIKE 'ALL%'  AND all_objects.object_name NOT LIKE 'USR%'  AND all_objects.object_name NOT LIKE 'DBA%'  AND all_objects.object_name NOT LIKE '%$%'  AND all_objects.status = 'VALID'  AND all_objects.owner = '" + Principal.Catalogo + "'  AND all_objects.object_type IN ('TRIGGER')";

                    queryTables1 = queryTables1 + " AND UPPER(all_objects.object_name) LIKE '" + strFiltro.ToUpper() + "%' ORDER BY all_objects.object_type,all_objects.object_name";
                    queryTables2 = queryTables2 + " AND UPPER(all_objects.object_name) LIKE '" + strFiltro.ToUpper() + "%' ORDER BY all_objects.object_type,all_objects.object_name";
                    queryTables3 = queryTables3 + " AND UPPER(all_objects.object_name) LIKE '" + strFiltro.ToUpper() + "%' ORDER BY all_objects.object_type,all_objects.object_name";
                    queryTables4 = queryTables4 + " AND UPPER(all_objects.object_name) LIKE '" + strFiltro.ToUpper() + "%' ORDER BY all_objects.object_type,all_objects.object_name";

                    break;

                case TipoBD.PostgreSQL:
                    queryTables1 = "SELECT tablename AS name, tablename AS object_id, schemaname AS namespace, 'Tabla' AS Tipo FROM pg_tables WHERE schemaname not in ('information_schema','pg_catalog') AND tablename LIKE '" + strFiltro + "%' ORDER BY tablename ";
                    queryTables2 = "SELECT viewname AS name, viewname AS object_id, schemaname AS namespace, 'Vista' AS Tipo FROM pg_views WHERE schemaname not in ('information_schema','pg_catalog') AND viewname LIKE '" + strFiltro + "%' ORDER BY viewname";
                    queryTables3 = "SELECT proname || '()' AS name, proname || '()' AS object_id, 'Row Function' as Tipo FROM    pg_catalog.pg_namespace n JOIN    pg_catalog.pg_proc p ON      pronamespace = n.oid WHERE   nspname = 'public' AND prorows > 0";
                    queryTables4 = "SELECT viewname AS name, viewname AS object_id, schemaname AS namespace, 'Vista' AS Tipo FROM pg_views WHERE viewname LIKE '" + strFiltro + "%' AND 1 = 2";

                    break;

                case TipoBD.MySql:
                    queryTables1 = "SELECT table_name as name, table_name as object_id, table_schema as namespace, table_type as tipo FROM information_schema.tables WHERE table_type = 'BASE TABLE' AND table_schema = '" + Principal.Catalogo +"' AND table_name LIKE '" + strFiltro  + "%' ORDER BY table_name;";
                    queryTables2 = "SELECT table_name as name, table_name as object_id, table_schema as namespace, table_type as tipo FROM information_schema.tables WHERE table_type = 'VIEW' AND table_schema = '" + Principal.Catalogo +"' AND table_name LIKE '" + strFiltro + "%' ORDER BY table_name;";
                    queryTables3 = "SELECT table_name as name, table_name as object_id, table_schema as namespace, table_type as tipo FROM information_schema.tables WHERE table_type = 'VIEW' AND table_schema = '" + Principal.Catalogo +"' AND table_name LIKE '" + strFiltro + "%' AND 1 = 2";
                    queryTables4 = "SELECT table_name as name, table_name as object_id, table_schema as namespace, table_type as tipo FROM information_schema.tables WHERE table_type = 'VIEW' AND table_schema = '" + Principal.Catalogo +"' AND table_name LIKE '" + strFiltro + "%' AND 1 = 2";

                    break;
                case TipoBD.FireBird:

                    break;

                case TipoBD.SQLite:
                    queryTables1 = "SELECT name AS name, name AS object_id, 'public' as namespace, type AS Tipo FROM sqlite_master WHERE type = 'table' AND name LIKE '" + strFiltro + "%' ORDER BY name";
                    queryTables2 = "SELECT name AS name, name AS object_id, 'public' as namespace, type AS Tipo FROM sqlite_master WHERE type = 'view'  AND name LIKE '" + strFiltro + "%' ORDER BY name ";
                    queryTables3 = "SELECT name AS name, name AS object_id, 'public' as namespace, 'Tabla' AS Tipo FROM sqlite_master WHERE 1 = 2  AND name LIKE '" + strFiltro + "%' ORDER BY name ";
                    queryTables4 = "SELECT name AS name, name AS object_id, 'public' as namespace, 'Tabla' AS Tipo FROM sqlite_master WHERE 1 = 2  AND name LIKE '" + strFiltro + "%' ORDER BY name ";
                    break;
            }

            DataTable dt1 = new DataTable();
            dt1 = CFuncionesBDs.CargarDataTable(queryTables1);
            ReAlListView1.ViewColumn0 = true;
            ReAlListView1.DataSource = dt1;
            if (ReAlListView1.Columns.Count > 0)
                ReAlListView1.Columns[1].Width = 0;

            DataTable dt2 = new DataTable();
            dt2 = CFuncionesBDs.CargarDataTable(queryTables2);
            ReAlListView2.ViewColumn0 = true;
            ReAlListView2.DataSource = dt2;
            if (ReAlListView2.Columns.Count > 0)
                ReAlListView2.Columns[1].Width = 0;

            DataTable dt3 = new DataTable();
            dt3 = CFuncionesBDs.CargarDataTable(queryTables3);
            ReAlListView3.ViewColumn0 = true;
            ReAlListView3.DataSource = dt3;
            if (ReAlListView3.Columns.Count > 0)
                ReAlListView3.Columns[1].Width = 0;

            DataTable dt4 = new DataTable();
            dt4 = CFuncionesBDs.CargarDataTable(queryTables4);
            ReAlListView4.ViewColumn0 = true;
            ReAlListView4.DataSource = dt4;
            if (ReAlListView4.Columns.Count > 0)
                ReAlListView4.Columns[1].Width = 0;

            for (int i = 0; i <= ReAlListView1.Items.Count - 1; i++)
                ReAlListView1.Items[i].Checked = chkSelectAll;

            for (int i = 0; i <= ReAlListView2.Items.Count - 1; i++)
                ReAlListView2.Items[i].Checked = chkSelectAll;

            for (int i = 0; i <= ReAlListView3.Items.Count - 1; i++)
                ReAlListView3.Items[i].Checked = chkSelectAll;

            for (int i = 0; i <= ReAlListView4.Items.Count - 1; i++)
                ReAlListView4.Items[i].Checked = chkSelectAll;
        }

        public static DataTable ObtenerListadoColumnas(string schemaName, string idTable, Languaje lang, TipoColumnas tipoCol, string strColumns = "")
        {
            string query = "";
            var strQuey = new StringBuilder();
            switch (Principal.DBMS)
            {
                case TipoBD.SQLServer:

                    #region "SqlServer"

                    strQuey = new StringBuilder();

                    switch (tipoCol)
                    {
                        case TipoColumnas.PrimeraMayuscula:
                            strQuey.AppendLine(" SELECT UPPER(substring(sc.name,1,1)) + LOWER(substring(sc.name, 2, LEN(sc.name) - 1)) AS name, ");
                            break;

                        case TipoColumnas.Mayusculas:
                            strQuey.AppendLine("SELECT  UPPER(sc.name) AS NAME,");
                            break;

                        case TipoColumnas.Minusculas:
                            strQuey.AppendLine("SELECT  LOWER(sc.name) AS NAME,");
                            break;
                    }

                    strQuey.AppendLine(" CASE st.name ");
                    strQuey.AppendLine(" \t\t\t WHEN 'nvarchar' then 'String' ");
                    strQuey.AppendLine(" \t\t\t WHEN 'varchar' then 'String'  ");
                    strQuey.AppendLine(" \t\t\t WHEN 'text' then 'String'  ");
                    strQuey.AppendLine(" \t\t\t WHEN 'String' then 'String' ");
                    strQuey.AppendLine(" \t\t\t WHEN 'char' then 'String' ");
                    strQuey.AppendLine(" \t\t\t WHEN 'datetime' then 'DateTime' ");
                    strQuey.AppendLine(" \t\t\t WHEN 'timestamp' then 'DateTime' ");
                    strQuey.AppendLine(" \t\t\t WHEN 'date' then 'DateTime' ");
                    strQuey.AppendLine(" \t\t\t WHEN 'time' then 'DateTime' ");
                    strQuey.AppendLine(" \t\t\t WHEN 'smalldatetime' then 'DateTime' ");
                    if (lang == Languaje.CSharp)
                    {
                        strQuey.AppendLine("             WHEN 'bit' then 'bool' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'image' then 'Byte[]' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'numeric' then 'Decimal' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'Int64' then 'int' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'tinyint' then 'int' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'smallint' then 'int' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'bigint' then 'Int64' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'int' then 'int' ");
                    }
                    else if (lang == Languaje.VbNet)
                    {
                        strQuey.AppendLine("             WHEN 'bit' then 'Boolean' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'image' then 'Byte()' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'numeric' then 'Decimal' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'Int64' then 'Integer' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'tinyint' then 'Integer' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'smallint' then 'Integer' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'bigint' then 'Integer' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'int' then 'Integer' ");
                    }
                    strQuey.AppendLine(" \t\t\t ELSE st.name END AS tipo,  ");
                    strQuey.AppendLine(" so.name as Tabla, ");
                    strQuey.AppendLine(" UPPER(LEFT(sc.name,1)) + RIGHT(sc.name, LEN(sc.name) -1) AS name_upper,  ");
                    strQuey.AppendLine(" CASE COLUMNPROPERTY( sc.[object_id] ,sc.name, 'AllowsNull') WHEN 1 THEN 'Yes' ELSE 'No' END AS PermiteNull, ");
                    strQuey.AppendLine(" CASE COLUMNPROPERTY( sc.[object_id] ,sc.name, 'IsIdentity') WHEN 1 THEN 'Yes' ELSE 'No' END AS EsIdentidad, ");
                    strQuey.AppendLine(" CASE COLUMNPROPERTY( sc.[object_id] ,sc.name, 'IsComputed') WHEN 1 THEN 'Yes' ELSE 'No' END AS EsCalculada, ");
                    strQuey.AppendLine(" CASE COLUMNPROPERTY( sc.[object_id] ,sc.name, 'IsRowGuidCol') WHEN 1 THEN 'Yes' ELSE 'No' END AS EsRowGui, ");
                    strQuey.AppendLine(" CASE WHEN (SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B ");
                    strQuey.AppendLine(" \tWHERE A.CONSTRAINT_NAME = B.CONSTRAINT_NAME AND B.COLUMN_NAME = sc.name AND CONSTRAINT_TYPE = 'PRIMARY KEY' ");
                    strQuey.AppendLine(" \tAND upper(so.name) = upper(A.TABLE_NAME)) = 0 THEN 'No' ELSE 'Yes' END as EsPK, ");
                    strQuey.AppendLine(" CASE WHEN (SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B ");
                    strQuey.AppendLine(" \tWHERE A.CONSTRAINT_NAME = B.CONSTRAINT_NAME AND B.COLUMN_NAME = sc.name AND CONSTRAINT_TYPE = 'FOREIGN KEY' ");
                    strQuey.AppendLine(" \tAND upper(so.name) = upper(A.TABLE_NAME)) = 0 THEN 'No' ELSE 'Yes' END as EsFK,\t ");
                    strQuey.AppendLine(" (SELECT TOP 1 SO2.NAME REF_TABLE_NAME FROM SYSFOREIGNKEYS SYSFK ");
                    strQuey.AppendLine(" \tINNER JOIN (SELECT UID, ID, NAME FROM SYSOBJECTS WHERE XTYPE = 'U') SO1 ON SYSFK.FKEYID = SO1.ID ");
                    strQuey.AppendLine(" \tINNER JOIN (SELECT UID, ID, NAME FROM SYSOBJECTS WHERE XTYPE = 'U') SO2 ON SYSFK.RKEYID = SO2.ID ");
                    strQuey.AppendLine(" \tINNER JOIN (select ID, COLID, NAME FROM SYSCOLUMNS) SC1 ON SYSFK.FKEYID = SC1.ID AND SYSFK.FKEY = SC1.COLID ");
                    strQuey.AppendLine(" \tINNER JOIN (select ID, COLID, NAME FROM SYSCOLUMNS) SC2 ON SYSFK.RKEYID = SC2.ID AND SYSFK.RKEY = SC2.COLID ");
                    strQuey.AppendLine(" \tINNER JOIN (SELECT ID, NAME FROM SYSOBJECTS) SO3 ON SYSFK.CONSTID = SO3.ID");
                    strQuey.AppendLine(" \tWHERE SO1.NAME = so.name AND SC1.NAME = sc.name) AS TablaForanea,");
                    strQuey.AppendLine(" st.name AS TIPO_COL, sc.max_length AS LONGITUD, ISNULL(ep.value,'') as COL_DESC ");
                    strQuey.AppendLine(" FROM sys.columns sc ");
                    strQuey.AppendLine(" JOIN sys.types st ON sc.system_type_id = st.system_type_id AND st.name <> 'sysname' AND st.name <> 'uniqueidentifier' AND st.user_type_id <= 256");
                    strQuey.AppendLine(" JOIN sys.objects so ON sc.object_id = so.object_id ");
                    strQuey.AppendLine(" LEFT JOIN sys.extended_properties ep ON ep.name = 'MS_Description' AND sc.column_id = ep.minor_id AND so.object_id = ep.major_id ");
                    strQuey.AppendLine(" WHERE sc.object_id = " + idTable + " ");
                    if (!string.IsNullOrEmpty(strColumns))
                    {
                        strQuey.AppendLine(" AND sc.name IN ('" + strColumns + "')");
                    }
                    strQuey.AppendLine(" ORDER BY sc.column_id ");

                    query = strQuey.ToString();

                    #endregion "SqlServer"

                    return CFuncionesBDs.CargarDataTable(query);

                case TipoBD.SQLServer2000:

                    #region "SqlServer 2000"

                    query = " SELECT UPPER(substring(sc.name,1,1)) + LOWER(substring(sc.name, 2, LEN(sc.name) - 1)) AS name,  CASE st.name WHEN 'bit' then 'Boolean'  \t\t\t WHEN 'nvarchar' then 'String'  \t\t\t WHEN 'varchar' then 'String'   \t\t\t WHEN 'text' then 'String'   \t\t\t WHEN 'String' then 'String'  \t\t\t WHEN 'datetime' then 'DateTime'  \t\t\t WHEN 'timestamp' then 'DateTime'  \t\t\t WHEN 'date' then 'DateTime'  \t\t\t WHEN 'time' then 'DateTime'  \t\t\t WHEN 'smalldatetime' then 'DateTime'  \t\t\t WHEN 'image' then 'Byte[]'  \t\t\t WHEN 'numeric' then 'Decimal'  \t\t\t WHEN 'Int64' then 'int'  \t\t\t WHEN 'tinyint' then 'int'  \t\t\t WHEN 'smallint' then 'int'  \t\t\t WHEN 'bigint' then 'Int64'  \t\t\t WHEN 'int' then 'int'  \t\t\t WHEN 'char' then 'String'  \t\t\t ELSE st.name END AS tipo,   so.name as Tabla,  UPPER(LEFT(sc.name,1)) + RIGHT(sc.name, LEN(sc.name) -1) AS name_upper,   CASE COLUMNPROPERTY( sc.[object_id] ,sc.name, 'AllowsNull') WHEN 1 THEN 'Yes' ELSE 'No' END AS PermiteNull,  CASE COLUMNPROPERTY( sc.[object_id] ,sc.name, 'IsIdentity') WHEN 1 THEN 'Yes' ELSE 'No' END AS EsIdentidad,  CASE COLUMNPROPERTY( sc.[object_id] ,sc.name, 'IsComputed') WHEN 1 THEN 'Yes' ELSE 'No' END AS EsCalculada,  CASE COLUMNPROPERTY( sc.[object_id] ,sc.name, 'IsRowGuidCol') WHEN 1 THEN 'Yes' ELSE 'No' END AS EsRowGui,  CASE WHEN (SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B  \tWHERE A.CONSTRAINT_NAME = B.CONSTRAINT_NAME AND B.COLUMN_NAME = sc.name AND CONSTRAINT_TYPE = 'PRIMARY KEY'  \tAND upper(so.name) = upper(A.TABLE_NAME)) = 0 THEN 'No' ELSE 'Yes' END as EsPK,  CASE WHEN (SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B  \tWHERE A.CONSTRAINT_NAME = B.CONSTRAINT_NAME AND B.COLUMN_NAME = sc.name AND CONSTRAINT_TYPE = 'FOREIGN KEY'  \tAND upper(so.name) = upper(A.TABLE_NAME)) = 0 THEN 'No' ELSE 'Yes' END as EsFK,\t  (SELECT TOP 1 SO2.NAME REF_TABLE_NAME FROM SYSFOREIGNKEYS SYSFK  \tINNER JOIN (SELECT UID, ID, NAME FROM SYSOBJECTS WHERE XTYPE = 'U') SO1 ON SYSFK.FKEYID = SO1.ID  \tINNER JOIN (SELECT UID, ID, NAME FROM SYSOBJECTS WHERE XTYPE = 'U') SO2 ON SYSFK.RKEYID = SO2.ID  \tINNER JOIN (select ID, COLID, NAME FROM SYSCOLUMNS) SC1 ON SYSFK.FKEYID = SC1.ID AND SYSFK.FKEY = SC1.COLID  \tINNER JOIN (select ID, COLID, NAME FROM SYSCOLUMNS) SC2 ON SYSFK.RKEYID = SC2.ID AND SYSFK.RKEY = SC2.COLID  \tINNER JOIN (SELECT ID, NAME FROM SYSOBJECTS) SO3 ON SYSFK.CONSTID = SO3.ID \tWHERE SO1.NAME = so.name AND SC1.NAME = sc.name) AS TablaForanea, st.name AS TIPO_COL, sc.max_length AS LONGITUD, ISNULL(se.value,'') as DESC_COL  FROM syscolumns sc JOIN systypes st ON sc.system_type_id = st.system_type_id AND st.name <> 'sysname' AND st.name <> 'uniqueidentifier' AND st.user_type_id <=256 JOIN sysobjects so ON AND sc.object_id = so.object_id LEFT JOIN sysproperties ep ON ep.name = 'MS_Description' AND sc.colid = ep.smallid AND so.id = ep.major_id WHERE  sc.object_id = " + idTable + " " + (string.IsNullOrEmpty(strColumns) ? "" : " AND sc.name IN ('" + strColumns + "')") + " ORDER BY sc.column_id ";

                    #endregion "SqlServer 2000"

                    return CFuncionesBDs.CargarDataTable(query);

                case TipoBD.Oracle:

                    #region "Oracle"

                    strQuey = new StringBuilder();
                    switch (tipoCol)
                    {
                        case TipoColumnas.PrimeraMayuscula:
                            strQuey.AppendLine("SELECT \tUPPER(SUBSTR(atc.column_name,1,1)) || LOWER(SUBSTR(atc.column_name, 2, LENGTH(atc.column_name) - 1)) AS \"name\" ,  ");
                            break;

                        case TipoColumnas.Mayusculas:
                            strQuey.AppendLine("SELECT \tUPPER(atc.column_name) AS \"name\" ,  ");
                            break;

                        case TipoColumnas.Minusculas:
                            strQuey.AppendLine("SELECT \tLOWER(atc.column_name) AS \"name\" ,  ");
                            break;
                    }

                    strQuey.AppendLine("\t\tCase atc.data_type ");
                    if (lang == Languaje.FeathersJS)
                    {
                        strQuey.AppendLine("\t\t\tWHEN 'CHAR' THEN 'STRING' ");
                        strQuey.AppendLine("\t\t\tWHEN 'VARCHAR' THEN 'STRING' ");
                        strQuey.AppendLine("\t\t\tWHEN 'VARCHAR2' THEN 'STRING' ");
                        strQuey.AppendLine("\t\t\tWHEN 'INTEGER' THEN 'INTEGER' ");
                        strQuey.AppendLine("\t\t\tWHEN 'NUMBER' THEN CASE  atc.data_scale WHEN 0 THEN 'INTEGER' ELSE 'DECIMAL' END ");
                        strQuey.AppendLine("\t\t\tWHEN 'REAL' THEN 'DECIMAL'");
                        strQuey.AppendLine("\t\t\tWHEN 'REAL' THEN 'DECIMAL' ");
                        strQuey.AppendLine("\t\t\tWHEN 'DATE' THEN 'DATE' ");
                        strQuey.AppendLine("\t\t\tWHEN 'DECIMAL' THEN 'DATE'");
                        strQuey.AppendLine("\t\t\tWHEN 'FLOAT' THEN 'DATE' ");
                        strQuey.AppendLine("\t\t\tWHEN 'TIME' THEN 'DATE' ");
                        strQuey.AppendLine("\t\t\tWHEN 'TIME WITH TZ' THEN 'DATE' ");
                        strQuey.AppendLine("\t\t\tWHEN 'TIMESTAMP' THEN 'DATE' ");
                        strQuey.AppendLine("\t\t\tWHEN 'TIMESTAMP(6)' THEN 'DATE' ");
                        strQuey.AppendLine("\t\t\tWHEN 'BLOB' THEN 'BLOB'");
                        strQuey.AppendLine("\t\t\tWHEN 'LONG RAW' THEN 'BLOB' ");
                        strQuey.AppendLine("\t\t\tELSE 'String' ");
                        strQuey.AppendLine("\t\tEND as \"Tipo\",");
                    }
                    else
                    {
                        strQuey.AppendLine("\t\t\tWHEN 'BLOB' THEN 'Byte[]'");
                        strQuey.AppendLine("\t\t\tWHEN 'CHAR' THEN 'String' ");
                        strQuey.AppendLine("\t\t\tWHEN 'DATE' THEN 'DateTime' ");
                        strQuey.AppendLine("\t\t\tWHEN 'DECIMAL' THEN 'Decimal'");
                        strQuey.AppendLine("\t\t\tWHEN 'FLOAT' THEN 'Decimal' ");
                        strQuey.AppendLine("\t\t\tWHEN 'INTEGER' THEN 'int' ");
                        strQuey.AppendLine("\t\t\tWHEN 'NUMBER' THEN CASE  atc.data_scale WHEN 0 THEN 'int' ELSE 'decimal' END ");
                        strQuey.AppendLine("\t\t\tWHEN 'REAL' THEN 'Decimal'");
                        strQuey.AppendLine("\t\t\tWHEN 'REAL' THEN 'Decimal' ");
                        strQuey.AppendLine("\t\t\tWHEN 'TIME' THEN 'DateTime' ");
                        strQuey.AppendLine("\t\t\tWHEN 'TIME WITH TZ' THEN 'DateTime' ");
                        strQuey.AppendLine("\t\t\tWHEN 'TIMESTAMP' THEN 'DateTime' ");
                        strQuey.AppendLine("\t\t\tWHEN 'TIMESTAMP(6)' THEN 'DateTime' ");
                        strQuey.AppendLine("\t\t\tWHEN 'VARCHAR' THEN 'String' ");
                        strQuey.AppendLine("\t\t\tWHEN 'VARCHAR2' THEN 'String' ");
                        strQuey.AppendLine("\t\t\tWHEN 'LONG RAW' THEN 'Byte[]' ");
                        strQuey.AppendLine("\t\t\tELSE 'String' ");
                        strQuey.AppendLine("\t\tEND as \"Tipo\",");
                    }
                    strQuey.AppendLine("\t\tatc.TABLE_NAME as \"Tabla\",");
                    strQuey.AppendLine("\t\tUPPER(SUBSTR(atc.column_name,1,1)) || LOWER(SUBSTR(atc.column_name, 2, LENGTH(atc.column_name) - 1)) AS \"name_upper\",");
                    strQuey.AppendLine("\t\tCASE atc.nullable WHEN 'Y' THEN 'Yes' ELSE 'No' END AS \"PermiteNull\",");
                    strQuey.AppendLine("\t\t'No' AS \"EsIdentidad\",");
                    strQuey.AppendLine("\t\t'No' AS \"EsCalculada\",");
                    strQuey.AppendLine("\t\t'No' AS \"EsRowGui\",");
                    strQuey.AppendLine("\t\t(SELECT CASE WHEN COUNT(ac.CONSTRAINT_NAME) = 0 THEN 'No' ELSE 'Yes' END ");
                    strQuey.AppendLine("\t\t\tFROM all_constraints ac JOIN all_cons_columns acc ON ac.CONSTRAINT_NAME = acc.CONSTRAINT_NAME ");
                    strQuey.AppendLine("\t\t\tWHERE(acc.COLUMN_NAME = atc.COLUMN_NAME And acc.TABLE_NAME = atc.TABLE_NAME)");
                    strQuey.AppendLine("\t\t\tAND ac.CONSTRAINT_TYPE = 'P' AND ac.owner= '" + Principal.Catalogo + "'");
                    strQuey.AppendLine("\t\t\tAND acc.owner= '" + Principal.Catalogo + "') AS  \"EsPK\",");
                    strQuey.AppendLine("\t\t(SELECT CASE WHEN COUNT(ac.CONSTRAINT_NAME) = 0 THEN 'No' ELSE 'Yes' END ");
                    strQuey.AppendLine("\t\t\tFROM all_constraints ac JOIN all_cons_columns acc ON ac.CONSTRAINT_NAME = acc.CONSTRAINT_NAME");
                    strQuey.AppendLine("\t\t\tWHERE(acc.COLUMN_NAME = atc.COLUMN_NAME AND acc.TABLE_NAME = atc.TABLE_NAME)");
                    strQuey.AppendLine("\t\t\tAND ac.CONSTRAINT_TYPE = 'R' AND ac.owner= '" + Principal.Catalogo + "'");
                    strQuey.AppendLine("\t\t\tAND acc.owner= '" + Principal.Catalogo + "') AS  \"EsFK\",");
                    strQuey.AppendLine("\t\t(SELECT DISTINCT parent.table_name");
                    strQuey.AppendLine("\t\t\tFROM user_constraints child , user_constraints PARENT,");
                    strQuey.AppendLine("\t\t\tuser_cons_columns ucc, user_cons_columns ucc2");
                    strQuey.AppendLine("\t\t\tWHERE(child.r_constraint_name = parent.constraint_name)");
                    strQuey.AppendLine("\t\t\tAND child.r_owner = parent.owner ");
                    strQuey.AppendLine("\t\t\tAND child.r_owner = parent.owner");
                    strQuey.AppendLine("\t\t\tAND child.constraint_type in ('R')");
                    strQuey.AppendLine("\t\t\tAND child.constraint_name = ucc.constraint_name");
                    strQuey.AppendLine("\t\t\tAND parent.constraint_name = ucc2.constraint_name");
                    strQuey.AppendLine("\t\t\tAND child.table_name = atc.TABLE_NAME");
                    strQuey.AppendLine("\t\t\tAND ucc.column_name = atc.column_name");
                    strQuey.AppendLine("\t\t\tAND ROWNUM = 1");
                    strQuey.AppendLine("\t\t) AS \"TablaForanea\",");
                    strQuey.AppendLine("\t\tatc.data_type AS \"Tipo_Col\",");
                    strQuey.AppendLine("\t\tCOALESCE(atc.data_precision,atc.DATA_LENGTH) AS \"Longitud\",");
                    strQuey.AppendLine("\t\tNVL(acc.comments,'') AS COL_DESC");
                    strQuey.AppendLine("FROM all_tab_columns atc");
                    strQuey.AppendLine("LEFT JOIN all_col_comments acc ON atc.owner = acc.owner AND atc.table_name = acc.table_name AND atc.column_name = acc.column_name");
                    strQuey.AppendLine("WHERE atc.TABLE_NAME = '" + idTable + "'");
                    strQuey.AppendLine("AND atc.owner = '" + Principal.Catalogo + "'");
                    if (!string.IsNullOrEmpty(strColumns))
                        strQuey.AppendLine("AND atc.column_name IN ('" + strColumns + "')");
                    strQuey.AppendLine("ORDER BY atc.column_id");

                    #endregion "Oracle"

                    return CFuncionesBDs.CargarDataTable(strQuey.ToString());

                case TipoBD.PostgreSQL:

                    #region "PostgreSql"

                    strQuey = new StringBuilder();

                    if (idTable.Contains("()"))
                    {
                        //Cargamos el DataTable para una funcion
                        DataTable dtFuncion = CFuncionesBDs.CargarDataTable("SELECT * FROM " + idTable);

                        DataTable dtPg = new DataTable();
                        DataColumn dcPg = new DataColumn("name", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("tipo", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("tabla", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("name_upper", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("permitenull", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("esidentidad", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("escalculada", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("esrowgui", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("espk", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("esfk", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("tablaforanea", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("tipo_col", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("longitud", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("col_desc", typeof(string));
                        dtPg.Columns.Add(dcPg);

                        foreach (DataColumn column in dtFuncion.Columns)
                        {
                            DataRow dr = dtPg.NewRow();
                            dr["name"] = column.ColumnName;
                            if (column.DataType.ToString().ToUpper().Contains("INTEGER"))
                            {
                                dr["tipo"] = "int";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("INT"))
                            {
                                dr["tipo"] = "int";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("REAL"))
                            {
                                dr["tipo"] = "float";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("LONG"))
                            {
                                dr["tipo"] = "float";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("TEXT"))
                            {
                                dr["tipo"] = "String";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("NVARCHAR"))
                            {
                                dr["tipo"] = "String";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("VARCHAR"))
                            {
                                dr["tipo"] = "String";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("BLOB"))
                            {
                                dr["tipo"] = "Byte[]";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("DATE"))
                            {
                                dr["tipo"] = "long";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("DATETIME"))
                            {
                                dr["tipo"] = "long";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("BOOLEAN"))
                            {
                                dr["tipo"] = "int";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("BOOL"))
                            {
                                dr["tipo"] = "int";
                            }
                            else
                            {
                                dr["tipo"] = "String";
                            }

                            dr["tabla"] = idTable;
                            dr["name_upper"] = column.ColumnName.ToString().ToUpper();
                            dr["permitenull"] = "No";
                            dr["esidentidad"] = "No";
                            dr["escalculada"] = "No";
                            dr["esrowgui"] = "No";
                            dr["espk"] = "No";
                            dr["esfk"] = "No";
                            dr["tablaforanea"] = "";
                            dr["tipo_col"] = column.DataType;
                            dr["longitud"] = "0";
                            dr["col_desc"] = "";

                            dtPg.Rows.Add(dr);
                        }

                        return dtPg;
                    }
                    else
                    {
                        switch (tipoCol)
                        {
                            case TipoColumnas.PrimeraMayuscula:
                                strQuey.AppendLine("SELECT  UPPER(SUBSTRING(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','') ,1,1)) || LOWER(SUBSTRING(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','') ,2,LENGTH(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','')))) AS NAME,");
                                break;

                            case TipoColumnas.Mayusculas:
                                strQuey.AppendLine("SELECT  UPPER(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','')) AS NAME,");
                                break;

                            case TipoColumnas.Minusculas:
                                strQuey.AppendLine("SELECT  LOWER(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','')) AS NAME,");
                                break;

                            case TipoColumnas.PascalCase:
                                strQuey.AppendLine("SELECT  replace(initcap(replace(co.column_name, '_', ' ')), ' ', '') AS NAME,");
                                break;

                            case TipoColumnas.CamelCase:
                                strQuey.AppendLine("SELECT  lower(substring(replace(initcap(replace(co.column_name, '_', ' ')), ' ', ''),1,1)) || substring(replace(initcap(replace(co.column_name, '_', ' ')), ' ', ''),2) AS NAME,");
                                break;
                        }

                        strQuey.AppendLine("\tCASE UPPER(co.data_type)");

                        if (lang == Languaje.CSharp)
                        {
                            strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'Int64'");
                            strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'Decimal'");
                            strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'Decimal'");
                            strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'Decimal'");
                            strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'Decimal'");
                            strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'bool'");
                            strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'bool'");
                            strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte[]'");
                            strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'string'");
                            strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'string'");
                            strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'string'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'string'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'string'");
                            strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'JSONB' THEN 'string'");
                            strQuey.AppendLine("\t\tWHEN 'JSON' THEN 'string'");
                        }
                        else if (lang == Languaje.VbNet)
                        {
                            strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'Int64'");
                            strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'Int64'");
                            strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'Decimal'");
                            strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'Decimal'");
                            strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'Decimal'");
                            strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'Decimal'");
                            strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'Boolean'");
                            strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'Boolean'");
                            strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte()'");
                            strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'JSONB' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'JSON' THEN 'String'");
                        }
                        else if (lang == Languaje.Java)
                        {
                            strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'BigDecimal'");
                            strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'BigDecimal'");
                            strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'BigDecimal'");
                            strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'BigDecimal'");
                            strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte'");
                            strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'JSONB' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'JSON' THEN 'String'");
                        }
                        else if (lang == Languaje.JavaAndroid)
                        {
                            strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte'");
                            strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'JSONB' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'JSON' THEN 'String'");
                            //Default
                        }
                        else if (lang == Languaje.FeathersJS)
                        {
                            strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'BIGINT'");
                            strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'INTEGER'");
                            strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'INTEGER'");
                            strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'INTEGER'");
                            strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'INTEGER'");
                            strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'DECIMAL'");
                            strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'DECIMAL'");
                            strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'DECIMAL'");
                            strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'REAL'");
                            strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'BOOLEAN'");
                            strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'BOOLEAN'");
                            strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'BLOB'");
                            strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'STRING'");
                            strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'STRING'");
                            strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'STRING'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'STRING'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'STRING'");
                            strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'DATE'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'DATE'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'DATE'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'DATE'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'DATE'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'DATE'");
                            strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'DATE'");
                            strQuey.AppendLine("\t\tWHEN 'JSONB' THEN 'STRING'");
                            strQuey.AppendLine("\t\tWHEN 'JSON' THEN 'STRING'");
                        }
                        else
                        {
                            strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'BigDecimal'");
                            strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'BigDecimal'");
                            strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'BigDecimal'");
                            strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'BigDecimal'");
                            strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte'");
                            strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'JSONB' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'JSON' THEN 'String'");
                        }
                        strQuey.AppendLine("\t\tWHEN '' THEN ''");
                        strQuey.AppendLine("\t\tELSE 'Object'");
                        strQuey.AppendLine("\tEND AS tipo, co.table_name AS tabla,");
                        strQuey.AppendLine("\tUPPER(substring(co.COLUMN_NAME ,1,1)) || substring(co.COLUMN_NAME , 2, length(co.COLUMN_NAME ) - 1) AS name_upper,");
                        strQuey.AppendLine("\tCASE co.is_nullable WHEN 'YES' THEN 'Yes' ELSE 'No' END AS permitenull,");
                        //strQuey.AppendLine("	CASE co.is_identity WHEN 'YES' THEN 'Yes' ELSE 'No' END AS esidentidad,")
                        strQuey.AppendLine("\tCASE WHEN (SELECT COUNT(*) FROM information_schema.table_constraints tc ");
                        strQuey.AppendLine("\t\t   JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name");
                        strQuey.AppendLine("\t\t   WHERE co.table_catalog = tc.constraint_catalog");
                        strQuey.AppendLine("\t\t   AND co.table_schema = tc.constraint_schema");
                        strQuey.AppendLine("\t\t   AND co.table_name = tc.table_name");
                        strQuey.AppendLine("\t\t   AND co.column_name = ccu.column_name");
                        strQuey.AppendLine("\t\t   AND tc.constraint_type = 'PRIMARY KEY') > 0 THEN 'Yes'");
                        strQuey.AppendLine("\t\t   ELSE 'No' END AS esidentidad,");
                        strQuey.AppendLine("\tCASE co.is_generated WHEN 'NEVER' THEN 'No' ELSE 'Yes' END AS escalculada,");
                        strQuey.AppendLine("\t'No' AS esrowgui,");
                        strQuey.AppendLine("\tCASE WHEN (SELECT COUNT(*) FROM information_schema.table_constraints tc ");
                        strQuey.AppendLine("\t\t   JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name");
                        strQuey.AppendLine("\t\t   WHERE co.table_catalog = tc.constraint_catalog");
                        strQuey.AppendLine("\t\t   AND co.table_schema = tc.constraint_schema");
                        strQuey.AppendLine("\t\t   AND co.table_name = tc.table_name");
                        strQuey.AppendLine("\t\t   AND co.column_name = ccu.column_name");
                        strQuey.AppendLine("\t\t   AND tc.constraint_type = 'PRIMARY KEY') > 0 THEN 'Yes'");
                        strQuey.AppendLine("\t\t   ELSE 'No' END AS espk,");
                        strQuey.AppendLine("\tCASE WHEN (SELECT COUNT(*) FROM information_schema.table_constraints tc");
                        strQuey.AppendLine("\t\t   JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name");
                        strQuey.AppendLine("\t\t   WHERE co.table_catalog = tc.constraint_catalog");
                        strQuey.AppendLine("\t\t   AND co.table_schema = tc.constraint_schema");
                        strQuey.AppendLine("\t\t   AND co.table_name = tc.table_name");
                        strQuey.AppendLine("\t\t   AND co.column_name = ccu.column_name");
                        strQuey.AppendLine("\t\t   AND tc.constraint_type = 'FOREIGN KEY') > 0 THEN 'Yes'");
                        strQuey.AppendLine("\t\t   ELSE 'No' END AS esfk,");
                        strQuey.AppendLine("    (SELECT ccu.table_name FROM information_schema.table_constraints tc");
                        strQuey.AppendLine("\t JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name");
                        //strQuey.AppendLine("	 JOIN information_schema.constraint_column_usage ccu ON ccu.table_name = tc.table_name and ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name")
                        strQuey.AppendLine("\t WHERE co.table_catalog = tc.constraint_catalog");
                        strQuey.AppendLine("\t AND co.table_schema = tc.constraint_schema");
                        strQuey.AppendLine("\t AND co.table_name = tc.table_name");
                        strQuey.AppendLine("\t AND co.column_name = ccu.column_name");
                        strQuey.AppendLine("\t AND tc.constraint_type = 'FOREIGN KEY' LIMIT 1) AS tablaforanea,");
                        strQuey.AppendLine("\tco.data_type AS tipo_col,");
                        strQuey.AppendLine("\tCOALESCE(co.character_maximum_length::CHARACTER VARYING,'') AS longitud,");
                        strQuey.AppendLine("\tCOALESCE((select pg_catalog.col_description(oid,co.ordinal_position::int) ");
                        strQuey.AppendLine("\t from pg_catalog.pg_class c ");
                        strQuey.AppendLine("\t where c.relname = co.table_name");
                        strQuey.AppendLine("\t order by pg_catalog.col_description(oid,co.ordinal_position::int) asc LIMIT 1),'') as col_desc,");
                        strQuey.AppendLine("\tco.COLUMN_NAME as name_original");
                        strQuey.AppendLine(" FROM information_schema.columns co");
                        strQuey.AppendLine(" WHERE co.table_name = '" + idTable + "' ");
                        strQuey.AppendLine(" AND co.table_schema = '" + schemaName + "' ");
                        strQuey.AppendLine(" ORDER BY co.ordinal_position asc");

                        query = strQuey.ToString();

                        return CFuncionesBDs.CargarDataTable(query);
                    }
                    break;

                #endregion "PostgreSql"

                case TipoBD.MySql:
                    #region "MySql"                    
                    StringBuilder strQueryProperCaseFunction = new StringBuilder();
                    strQueryProperCaseFunction.AppendLine("DROP FUNCTION IF EXISTS `" + schemaName + "`.`proper_case`;");                    
                    strQueryProperCaseFunction.AppendLine("CREATE FUNCTION `" + schemaName + "`.`proper_case`(str varchar(128)) RETURNS varchar(128)");
                    strQueryProperCaseFunction.AppendLine(" READS SQL DATA");
                    strQueryProperCaseFunction.AppendLine("    DETERMINISTIC");
                    strQueryProperCaseFunction.AppendLine("BEGIN");
                    strQueryProperCaseFunction.AppendLine("DECLARE n, pos INT DEFAULT 1;");
                    strQueryProperCaseFunction.AppendLine("DECLARE sub, proper VARCHAR(128) DEFAULT '';");
                    strQueryProperCaseFunction.AppendLine("if length(trim(str)) > 0 then");
                    strQueryProperCaseFunction.AppendLine("    WHILE pos > 0 DO");
                    strQueryProperCaseFunction.AppendLine("        set pos = locate(' ',trim(str),n);");
                    strQueryProperCaseFunction.AppendLine("        if pos = 0 then");
                    strQueryProperCaseFunction.AppendLine("            set sub = lower(trim(substr(trim(str),n)));");
                    strQueryProperCaseFunction.AppendLine("        else");
                    strQueryProperCaseFunction.AppendLine("            set sub = lower(trim(substr(trim(str),n,pos-n)));");
                    strQueryProperCaseFunction.AppendLine("        end if;");
                    strQueryProperCaseFunction.AppendLine("        set proper = concat_ws(' ', proper, concat(upper(left(sub,1)),substr(sub,2)));");
                    strQueryProperCaseFunction.AppendLine("        set n = pos + 1;");
                    strQueryProperCaseFunction.AppendLine("    END WHILE;");
                    strQueryProperCaseFunction.AppendLine("end if;");
                    strQueryProperCaseFunction.AppendLine("RETURN replace(trim(proper),' ','');");
                    strQueryProperCaseFunction.AppendLine("END;");
                    CFuncionesBDs.EjecutarQuery(strQueryProperCaseFunction.ToString());

                    StringBuilder strQueryCamelCaseFunction = new StringBuilder();
                    strQueryCamelCaseFunction.AppendLine("DROP FUNCTION IF EXISTS `" + schemaName + "`.`camel_case`;");
                    strQueryCamelCaseFunction.AppendLine("CREATE FUNCTION `" + schemaName + "`.`camel_case`(str varchar(128)) RETURNS varchar(128)");
                    strQueryCamelCaseFunction.AppendLine(" READS SQL DATA");
                    strQueryCamelCaseFunction.AppendLine("    DETERMINISTIC");
                    strQueryCamelCaseFunction.AppendLine("BEGIN");
                    strQueryCamelCaseFunction.AppendLine("DECLARE n, pos INT DEFAULT 1;");
                    strQueryCamelCaseFunction.AppendLine("DECLARE sub, proper VARCHAR(128) DEFAULT '';");
                    strQueryCamelCaseFunction.AppendLine("if length(trim(str)) > 0 then");
                    strQueryCamelCaseFunction.AppendLine("    WHILE pos > 0 DO");
                    strQueryCamelCaseFunction.AppendLine("        set pos = locate(' ',trim(str),n);");
                    strQueryCamelCaseFunction.AppendLine("        if pos = 0 then");
                    strQueryCamelCaseFunction.AppendLine("            set sub = lower(trim(substr(trim(str),n)));");
                    strQueryCamelCaseFunction.AppendLine("        else");
                    strQueryCamelCaseFunction.AppendLine("            set sub = lower(trim(substr(trim(str),n,pos-n)));");
                    strQueryCamelCaseFunction.AppendLine("        end if;");
                    strQueryCamelCaseFunction.AppendLine("\t");
                    strQueryCamelCaseFunction.AppendLine("        if n = 1 then");
                    strQueryCamelCaseFunction.AppendLine("\tset proper = concat_ws(' ', proper, concat(lower(left(sub,1)),substr(sub,2)));");
                    strQueryCamelCaseFunction.AppendLine("        else");
                    strQueryCamelCaseFunction.AppendLine("\tset proper = concat_ws(' ', proper, concat(upper(left(sub,1)),substr(sub,2)));");
                    strQueryCamelCaseFunction.AppendLine("\tend if;");
                    strQueryCamelCaseFunction.AppendLine("        set n = pos + 1;");
                    strQueryCamelCaseFunction.AppendLine("    END WHILE;");
                    strQueryCamelCaseFunction.AppendLine("end if;");
                    strQueryCamelCaseFunction.AppendLine("RETURN replace(trim(proper),' ','');");
                    strQueryCamelCaseFunction.AppendLine("END;");                    
                    CFuncionesBDs.EjecutarQuery(strQueryCamelCaseFunction.ToString());

                    strQuey = new StringBuilder();
                    switch (tipoCol)
                    {
                        case TipoColumnas.PrimeraMayuscula:
                            strQuey.AppendLine("SELECT  UPPER(SUBSTRING(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','') ,1,1)) || LOWER(SUBSTRING(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','') ,2,LENGTH(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','')))) AS NAME,");
                            break;

                        case TipoColumnas.Mayusculas:
                            strQuey.AppendLine("SELECT  UPPER(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','')) AS NAME,");
                            break;

                        case TipoColumnas.Minusculas:
                            strQuey.AppendLine("SELECT  LOWER(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','')) AS NAME,");
                            break;

                        case TipoColumnas.PascalCase:
                            strQuey.AppendLine("SELECT  proper_case(replace(co.column_name,'_',' ')) AS NAME,");
                            break;

                        case TipoColumnas.CamelCase:
                            strQuey.AppendLine("SELECT  camel_case(replace(co.column_name,'_',' ')) AS NAME,");
                            break;
                    }

                    strQuey.AppendLine("\tCASE UPPER(co.data_type)");

                    if (lang == Languaje.CSharp)
                    {
                        strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'Int64'");
                        strQuey.AppendLine("\t\tWHEN 'INT' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'Decimal'");
                        strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'Decimal'");
                        strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'Decimal'");
                        strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'Decimal'");
                        strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'bool'");
                        strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'bool'");
                        strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte[]'");
                        strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'string'");
                        strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'string'");
                        strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'string'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'string'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'string'");
                        strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'JSONB' THEN 'string'");
                        strQuey.AppendLine("\t\tWHEN 'JSON' THEN 'string'");
                    }
                    else if (lang == Languaje.VbNet)
                    {
                        strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'Int64'");
                        strQuey.AppendLine("\t\tWHEN 'INT' THEN 'Int64'");
                        strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'Int64'");
                        strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'Decimal'");
                        strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'Decimal'");
                        strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'Decimal'");
                        strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'Decimal'");
                        strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'Boolean'");
                        strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'Boolean'");
                        strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte()'");
                        strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'JSONB' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'JSON' THEN 'String'");
                    }
                    else if (lang == Languaje.Java)
                    {
                        strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'Long'");
                        strQuey.AppendLine("\t\tWHEN 'INT' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'Double'");
                        strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'Double'");
                        strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'Double'");
                        strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'Double'");
                        strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'Boolean'");
                        strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'Boolean'");
                        strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Blob'");
                        strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'DATETIME' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'JSONB' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'JSON' THEN 'String'");
                    }
                    else if (lang == Languaje.JavaAndroid)
                    {
                        strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'INT' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte'");
                        strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'JSONB' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'JSON' THEN 'String'");
                        //Default
                    }
                    else if (lang == Languaje.FeathersJS)
                    {
                        strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'BIGINT'");
                        strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'INTEGER'");
                        strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'INTEGER'");
                        strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'INTEGER'");
                        strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'INTEGER'");
                        strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'DECIMAL'");
                        strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'DECIMAL'");
                        strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'DECIMAL'");
                        strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'REAL'");
                        strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'BOOLEAN'");
                        strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'BOOLEAN'");
                        strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'BLOB'");
                        strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'STRING'");
                        strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'STRING'");
                        strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'STRING'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'STRING'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'STRING'");
                        strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'DATE'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'DATE'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'DATE'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'DATE'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'DATE'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'DATE'");
                        strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'DATE'");
                        strQuey.AppendLine("\t\tWHEN 'JSONB' THEN 'STRING'");
                        strQuey.AppendLine("\t\tWHEN 'JSON' THEN 'STRING'");
                    }
                    else
                    {
                        strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'BigDecimal'");
                        strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'BigDecimal'");
                        strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'BigDecimal'");
                        strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'BigDecimal'");
                        strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte'");
                        strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'JSONB' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'JSON' THEN 'String'");
                    }
                    strQuey.AppendLine("\t\tWHEN '' THEN ''");
                    strQuey.AppendLine("\t\tELSE 'Object'");
                    strQuey.AppendLine("\tEND AS tipo, co.table_name AS tabla,");
                    strQuey.AppendLine("\tUPPER(substring(co.COLUMN_NAME ,1,1)) || substring(co.COLUMN_NAME , 2, length(co.COLUMN_NAME ) - 1) AS name_upper,");
                    strQuey.AppendLine("\tCASE co.is_nullable WHEN 'YES' THEN 'Yes' ELSE 'No' END AS permitenull,");
                    //strQuey.AppendLine("	CASE co.is_identity WHEN 'YES' THEN 'Yes' ELSE 'No' END AS esidentidad,")
                    strQuey.AppendLine("\tCASE WHEN co.EXTRA like '%auto_increment%' THEN 'Yes'");
                    strQuey.AppendLine("\t\t   ELSE 'No' END AS esidentidad,");
                    strQuey.AppendLine("\t'No' AS escalculada,");
                    strQuey.AppendLine("\t'No' AS esrowgui,");
                    strQuey.AppendLine("\tCASE WHEN co.COLUMN_KEY like '%PRI%' THEN 'Yes'");
                    strQuey.AppendLine("\t\t   ELSE 'No' END AS espk,");
                    strQuey.AppendLine("\tCASE WHEN co.COLUMN_KEY like '%MUL%' THEN 'Yes'");
                    strQuey.AppendLine("\t\t   ELSE 'No' END AS esfk,");
                    strQuey.AppendLine("NULL AS tablaforanea,");
                    strQuey.AppendLine("\tco.data_type AS tipo_col,");
                    strQuey.AppendLine("\tCOALESCE(co.character_maximum_length,'') AS longitud,");
                    strQuey.AppendLine("\tco.COLUMN_COMMENT as col_desc,");
                    strQuey.AppendLine("\tco.COLUMN_NAME as name_original");
                    strQuey.AppendLine(" FROM INFORMATION_SCHEMA.COLUMNS co");
                    strQuey.AppendLine(" WHERE co.TABLE_NAME = '" + idTable + "' ");
                    strQuey.AppendLine(" AND co.TABLE_SCHEMA = '" + schemaName + "' ");
                    strQuey.AppendLine(" ORDER BY co.ORDINAL_POSITION asc");

                    query = strQuey.ToString();

                    return CFuncionesBDs.CargarDataTable(query);
                    break;

                #endregion "MySql"
                case TipoBD.SQLite:

                    #region "SqLite"

                    strQuey = new StringBuilder();
                    strQuey.AppendLine("pragma table_info(" + idTable + ");");
                    query = strQuey.ToString();

                    //Creamos las columnas
                    DataTable dtPragma = CFuncionesBDs.CargarDataTable(query);
                    DataTable dt = new DataTable();
                    DataColumn dc = new DataColumn("name", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("tipo", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("tabla", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("name_upper", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("permitenull", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("esidentidad", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("escalculada", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("esrowgui", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("espk", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("esfk", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("tablaforanea", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("tipo_col", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("longitud", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("col_desc", typeof(string));
                    dt.Columns.Add(dc);

                    foreach (DataRow row in dtPragma.Rows)
                    {
                        DataRow dr = dt.NewRow();
                        dr["name"] = row["name"];
                        if (row["type"].ToString().ToUpper().Contains("INTEGER"))
                            dr["tipo"] = "int";
                        if (row["type"].ToString().ToUpper().Contains("INT"))
                            dr["tipo"] = "int";
                        if (row["type"].ToString().ToUpper().Contains("NUMERIC"))
                            dr["tipo"] = "int";
                        if (row["type"].ToString().ToUpper().Contains("REAL"))
                            dr["tipo"] = "float";
                        if (row["type"].ToString().ToUpper().Contains("LONG"))
                            dr["tipo"] = "float";
                        if (row["type"].ToString().ToUpper().Contains("TEXT"))
                            dr["tipo"] = "String";
                        if (row["type"].ToString().ToUpper().Contains("NVARCHAR"))
                            dr["tipo"] = "String";
                        if (row["type"].ToString().ToUpper().Contains("VARCHAR"))
                            dr["tipo"] = "String";
                        if (row["type"].ToString().ToUpper().Contains("BLOB"))
                            dr["tipo"] = "Byte[]";
                        if (row["type"].ToString().ToUpper().Contains("DATE"))
                            dr["tipo"] = "long";
                        if (row["type"].ToString().ToUpper().Contains("DATETIME"))
                            dr["tipo"] = "long";
                        if (row["type"].ToString().ToUpper().Contains("BOOLEAN"))
                            dr["tipo"] = "int";
                        if (row["type"].ToString().ToUpper().Contains("BOOL"))
                            dr["tipo"] = "int";
                        dr["tabla"] = idTable;
                        dr["name_upper"] = row["name"].ToString().ToUpper();
                        dr["permitenull"] = (row["notnull"] == "1" ? "No" : "Yes");
                        dr["esidentidad"] = "No";
                        dr["escalculada"] = "No";
                        dr["esrowgui"] = "No";
                        dr["espk"] = (row["pk"] == "1" ? "Yes" : "No");
                        dr["esfk"] = "No";
                        dr["tablaforanea"] = "No";
                        dr["tipo_col"] = row["type"];
                        dr["longitud"] = 0;
                        dr["col_desc"] = "Columna de tipo " + row["type"] + " de la tabla " + idTable;
                        dt.Rows.Add(dr);
                    }

                    #endregion "SqLite"

                    return dt;

                default:
                    return new DataTable();
            }
        }

        private static DataTable ObtenerListadoColumnasForaneas(string schemaName, string idTable, Languaje lang, TipoColumnas tipoCol, string strColumns = "")
        {
            string query = "";
            var strQuey = new StringBuilder();
            switch (Principal.DBMS)
            {
                case TipoBD.SQLServer:

                    #region "SqlServer"

                    strQuey = new StringBuilder();

                    strQuey.AppendLine(" SELECT '' as NAME, '' as Tabla");

                    query = strQuey.ToString();

                    #endregion "SqlServer"

                    return CFuncionesBDs.CargarDataTable(query);

                case TipoBD.SQLServer2000:

                    #region "SqlServer 2000"

                    query = " SELECT '' as NAME, '' as Tabla";

                    #endregion "SqlServer 2000"

                    return CFuncionesBDs.CargarDataTable(query);

                case TipoBD.Oracle:

                    #region "Oracle"

                    strQuey = new StringBuilder();
                    strQuey.AppendLine(" SELECT '' as NAME, '' as Tabla FROM DUAL");

                    #endregion "Oracle"

                    return CFuncionesBDs.CargarDataTable(strQuey.ToString());

                case TipoBD.PostgreSQL:

                    #region "PostgreSql"

                    strQuey = new StringBuilder();

                    //strQuey.AppendLine(" SELECT 	UPPER(SUBSTRING(REPLACE(REPLACE(ccu.COLUMN_NAME, ' ','_'),':','') ,1,1)) || LOWER(SUBSTRING(REPLACE(REPLACE(ccu.COLUMN_NAME, ' ','_'),':','') ,2,LENGTH(REPLACE(REPLACE(ccu.COLUMN_NAME, ' ','_'),':','')))) AS NAME, ");
                    //strQuey.AppendLine(" ccu.table_name AS tabla");
                    //strQuey.AppendLine(" FROM information_schema.table_constraints tc");
                    //strQuey.AppendLine(" JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name");
                    //strQuey.AppendLine(" WHERE '"+ schemaName +"' = tc.constraint_schema");
                    //strQuey.AppendLine(" AND '"+ idTable +"' = tc.table_name");
                    //strQuey.AppendLine(" AND tc.constraint_type = 'FOREIGN KEY'");
                    //
                    //query = strQuey.ToString();

                    switch (tipoCol)
                    {
                        case TipoColumnas.PrimeraMayuscula:
                            strQuey.AppendLine("SELECT  UPPER(SUBSTRING(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','') ,1,1)) || LOWER(SUBSTRING(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','') ,2,LENGTH(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','')))) AS NAME,");
                            break;

                        case TipoColumnas.Mayusculas:
                            strQuey.AppendLine("SELECT  UPPER(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','')) AS NAME,");
                            break;

                        case TipoColumnas.Minusculas:
                            strQuey.AppendLine("SELECT  LOWER(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','')) AS NAME,");
                            break;

                        case TipoColumnas.PascalCase:
                            strQuey.AppendLine("SELECT  replace(initcap(replace(co.column_name, '_', ' ')), ' ', '') AS NAME,");
                            break;

                        case TipoColumnas.CamelCase:
                            strQuey.AppendLine("SELECT  lower(substring(replace(initcap(replace(co.column_name, '_', ' ')), ' ', ''),1,1)) || substring(replace(initcap(replace(co.column_name, '_', ' ')), ' ', ''),2) AS NAME,");
                            break;
                    }

                    strQuey.AppendLine("\tCASE UPPER(co.data_type)");

                    if (lang == Languaje.CSharp)
                    {
                        strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'Decimal'");
                        strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'Decimal'");
                        strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'Decimal'");
                        strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'Decimal'");
                        strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'bool'");
                        strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'bool'");
                        strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte[]'");
                        strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'string'");
                        strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'string'");
                        strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'string'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'string'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'string'");
                        strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'DateTime'");
                    }
                    else if (lang == Languaje.VbNet)
                    {
                        strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'Int64'");
                        strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'Int64'");
                        strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'Decimal'");
                        strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'Decimal'");
                        strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'Decimal'");
                        strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'Decimal'");
                        strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'Boolean'");
                        strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'Boolean'");
                        strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte()'");
                        strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'DateTime'");
                        strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'DateTime'");
                    }
                    else if (lang == Languaje.Java)
                    {
                        strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'BigDecimal'");
                        strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'BigDecimal'");
                        strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'BigDecimal'");
                        strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'BigDecimal'");
                        strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte'");
                        strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'Date'");
                    }
                    else if (lang == Languaje.JavaAndroid)
                    {
                        strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'int'");
                        strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte'");
                        strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'long'");
                        strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'long'");
                        //Default
                    }
                    else if (lang == Languaje.FeathersJS)
                    {
                        strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'BIGINT'");
                        strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'INTEGER'");
                        strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'INTEGER'");
                        strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'INTEGER'");
                        strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'INTEGER'");
                        strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'DECIMAL'");
                        strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'DECIMAL'");
                        strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'DECIMAL'");
                        strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'REAL'");
                        strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'BOOLEAN'");
                        strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'BOOLEAN'");
                        strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'BLOB'");
                        strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'STRING'");
                        strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'STRING'");
                        strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'STRING'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'STRING'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'STRING'");
                        strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'DATE'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'DATE'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'DATE'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'DATE'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'DATE'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'DATE'");
                        strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'DATE'");
                    }
                    else
                    {
                        strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'BigDecimal'");
                        strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'BigDecimal'");
                        strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'BigDecimal'");
                        strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'BigDecimal'");
                        strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'Integer'");
                        strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte'");
                        strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                        strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'Date'");
                        strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'Date'");
                    }
                    strQuey.AppendLine("\t\tWHEN '' THEN ''");
                    strQuey.AppendLine("\t\tELSE 'Object'");
                    strQuey.AppendLine("\tEND AS tipo, co.table_name AS tabla,");
                    strQuey.AppendLine("\tUPPER(substring(co.COLUMN_NAME ,1,1)) || substring(co.COLUMN_NAME , 2, length(co.COLUMN_NAME ) - 1) AS name_upper,");
                    strQuey.AppendLine("\tCASE co.is_nullable WHEN 'YES' THEN 'Yes' ELSE 'No' END AS permitenull,");
                    //strQuey.AppendLine("	CASE co.is_identity WHEN 'YES' THEN 'Yes' ELSE 'No' END AS esidentidad,")
                    strQuey.AppendLine("\tCASE WHEN (SELECT COUNT(*) FROM information_schema.table_constraints tc ");
                    strQuey.AppendLine("\t\t   JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name");
                    strQuey.AppendLine("\t\t   WHERE co.table_catalog = tc.constraint_catalog");
                    strQuey.AppendLine("\t\t   AND co.table_schema = tc.constraint_schema");
                    strQuey.AppendLine("\t\t   AND co.table_name = tc.table_name");
                    strQuey.AppendLine("\t\t   AND co.column_name = ccu.column_name");
                    strQuey.AppendLine("\t\t   AND tc.constraint_type = 'PRIMARY KEY') > 0 THEN 'Yes'");
                    strQuey.AppendLine("\t\t   ELSE 'No' END AS esidentidad,");
                    strQuey.AppendLine("\tCASE co.is_generated WHEN 'NEVER' THEN 'No' ELSE 'Yes' END AS escalculada,");
                    strQuey.AppendLine("\t'No' AS esrowgui,");
                    strQuey.AppendLine("\tCASE WHEN (SELECT COUNT(*) FROM information_schema.table_constraints tc ");
                    strQuey.AppendLine("\t\t   JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name");
                    strQuey.AppendLine("\t\t   WHERE co.table_catalog = tc.constraint_catalog");
                    strQuey.AppendLine("\t\t   AND co.table_schema = tc.constraint_schema");
                    strQuey.AppendLine("\t\t   AND co.table_name = tc.table_name");
                    strQuey.AppendLine("\t\t   AND co.column_name = ccu.column_name");
                    strQuey.AppendLine("\t\t   AND tc.constraint_type = 'PRIMARY KEY') > 0 THEN 'Yes'");
                    strQuey.AppendLine("\t\t   ELSE 'No' END AS espk,");
                    strQuey.AppendLine("\tCASE WHEN (SELECT COUNT(*) FROM information_schema.table_constraints tc");
                    strQuey.AppendLine("\t\t   JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name");
                    strQuey.AppendLine("\t\t   WHERE co.table_catalog = tc.constraint_catalog");
                    strQuey.AppendLine("\t\t   AND co.table_schema = tc.constraint_schema");
                    strQuey.AppendLine("\t\t   AND co.table_name = tc.table_name");
                    strQuey.AppendLine("\t\t   AND co.column_name = ccu.column_name");
                    strQuey.AppendLine("\t\t   AND tc.constraint_type = 'FOREIGN KEY') > 0 THEN 'Yes'");
                    strQuey.AppendLine("\t\t   ELSE 'No' END AS esfk,");
                    strQuey.AppendLine("    (SELECT ccu.table_name FROM information_schema.table_constraints tc");
                    strQuey.AppendLine("\t JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name");
                    //strQuey.AppendLine("	 JOIN information_schema.constraint_column_usage ccu ON ccu.table_name = tc.table_name and ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name")
                    strQuey.AppendLine("\t WHERE co.table_catalog = tc.constraint_catalog");
                    strQuey.AppendLine("\t AND co.table_schema = tc.constraint_schema");
                    strQuey.AppendLine("\t AND co.table_name = tc.table_name");
                    strQuey.AppendLine("\t AND co.column_name = ccu.column_name");
                    strQuey.AppendLine("\t AND tc.constraint_type = 'FOREIGN KEY' LIMIT 1) AS tablaforanea,");
                    strQuey.AppendLine("\tco.data_type AS tipo_col,");
                    strQuey.AppendLine("\tCOALESCE(co.character_maximum_length::CHARACTER VARYING,'') AS longitud,");
                    strQuey.AppendLine("\tCOALESCE((select pg_catalog.col_description(oid,co.ordinal_position::int) ");
                    strQuey.AppendLine("\t from pg_catalog.pg_class c ");
                    strQuey.AppendLine("\t where c.relname = co.table_name");
                    strQuey.AppendLine("\t order by pg_catalog.col_description(oid,co.ordinal_position::int) asc LIMIT 1),'') as col_desc");
                    strQuey.AppendLine(" FROM information_schema.columns co");
                    //strQuey.AppendLine(" WHERE co.table_name = '" + idTable + "' ");
                    strQuey.AppendLine(" WHERE   (SELECT ccu.table_name FROM information_schema.table_constraints tc");
                    strQuey.AppendLine("\t JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name");
                    strQuey.AppendLine("\t WHERE co.table_catalog = tc.constraint_catalog");
                    strQuey.AppendLine("\t AND co.table_schema = tc.constraint_schema");
                    strQuey.AppendLine("\t AND co.table_name = tc.table_name");
                    strQuey.AppendLine("\t AND co.column_name = ccu.column_name");
                    strQuey.AppendLine("\t AND tc.constraint_type = 'FOREIGN KEY' LIMIT 1) = '" + idTable + "'");
                    strQuey.AppendLine(" AND co.table_schema = '" + schemaName + "' ");
                    strQuey.AppendLine(" ORDER BY co.ordinal_position asc");

                    query = strQuey.ToString();

                    return CFuncionesBDs.CargarDataTable(query);

                    break;

                #endregion "PostgreSql"

                default:
                    return new DataTable();
            }
        }

        public static DataTable ObtenerListadoParametrosSP(string nombreSP, TipoColumnas tipoCol)
        {
            string query = "";
            var strQuey = new StringBuilder();
            switch (Principal.DBMS)
            {
                case TipoBD.SQLServer:
                    strQuey = new StringBuilder();
                    strQuey.AppendLine("SELECT  dbo.sysobjects.name AS ObjName,  ");
                    strQuey.AppendLine("		dbo.sysobjects.xtype AS ObjType, ");

                    switch (tipoCol)
                    {
                        case TipoColumnas.PrimeraMayuscula:
                            strQuey.AppendLine(" SELECT UPPER(substring(dbo.syscolumns.name,1,1)) + LOWER(substring(dbo.syscolumns.name, 2, LEN(dbo.syscolumns.name) - 1)) AS ColName, ");
                            break;

                        case TipoColumnas.Mayusculas:
                            strQuey.AppendLine("SELECT  UPPER(dbo.syscolumns.name) AS ColName,");
                            break;

                        case TipoColumnas.Minusculas:
                            strQuey.AppendLine("SELECT  LOWER(dbo.syscolumns.name) AS ColName,");
                            break;
                    }

                    strQuey.AppendLine("		dbo.syscolumns.colorder AS ColOrder,  ");
                    strQuey.AppendLine("		dbo.syscolumns.length AS ColLen,  ");
                    strQuey.AppendLine("		dbo.syscolumns.colstat AS ColKey,  ");
                    strQuey.AppendLine("		dbo.systypes.xtype,  ");
                    strQuey.AppendLine("		dbo.systypes.name AS tipo  ");
                    strQuey.AppendLine("FROM dbo.syscolumns ");
                    strQuey.AppendLine("INNER JOIN  dbo.sysobjects ON dbo.syscolumns.id = dbo.sysobjects.id ");
                    strQuey.AppendLine("INNER JOIN  dbo.systypes ON dbo.syscolumns.xtype = dbo.systypes.xtype  ");
                    strQuey.AppendLine("WHERE dbo.sysobjects.name = '" + nombreSP + "'  ");
                    strQuey.AppendLine("AND dbo.systypes.status <> 1  ");
                    strQuey.AppendLine("AND dbo.systypes.usertype <=256  ");
                    strQuey.AppendLine("ORDER BY dbo.sysobjects.name,  dbo.syscolumns.colorder ");

                    query = strQuey.ToString();

                    break;

                case TipoBD.SQLServer2000:
                    strQuey = new StringBuilder();
                    strQuey.AppendLine("SELECT  dbo.sysobjects.name AS ObjName,  ");
                    strQuey.AppendLine("		dbo.sysobjects.xtype AS ObjType, ");

                    switch (tipoCol)
                    {
                        case TipoColumnas.PrimeraMayuscula:
                            strQuey.AppendLine(" SELECT UPPER(substring(dbo.syscolumns.name,1,1)) + LOWER(substring(dbo.syscolumns.name, 2, LEN(dbo.syscolumns.name) - 1)) AS ColName, ");
                            break;

                        case TipoColumnas.Mayusculas:
                            strQuey.AppendLine("SELECT  UPPER(dbo.syscolumns.name) AS ColName,");
                            break;

                        case TipoColumnas.Minusculas:
                            strQuey.AppendLine("SELECT  LOWER(dbo.syscolumns.name) AS ColName,");
                            break;
                    }

                    strQuey.AppendLine("		dbo.syscolumns.colorder AS ColOrder,  ");
                    strQuey.AppendLine("		dbo.syscolumns.length AS ColLen,  ");
                    strQuey.AppendLine("		dbo.syscolumns.colstat AS ColKey,  ");
                    strQuey.AppendLine("		dbo.systypes.xtype,  ");
                    strQuey.AppendLine("		dbo.systypes.name AS tipo  ");
                    strQuey.AppendLine("FROM dbo.syscolumns ");
                    strQuey.AppendLine("INNER JOIN  dbo.sysobjects ON dbo.syscolumns.id = dbo.sysobjects.id ");
                    strQuey.AppendLine("INNER JOIN  dbo.systypes ON dbo.syscolumns.xtype = dbo.systypes.xtype  ");
                    strQuey.AppendLine("WHERE dbo.sysobjects.name = '" + nombreSP + "'  ");
                    strQuey.AppendLine("AND dbo.systypes.status <> 1  ");
                    strQuey.AppendLine("AND dbo.systypes.usertype <=256  ");
                    strQuey.AppendLine("ORDER BY dbo.sysobjects.name,  dbo.syscolumns.colorder ");

                    query = strQuey.ToString();

                    break;

                case TipoBD.Oracle:
                    switch (tipoCol)
                    {
                        case TipoColumnas.PrimeraMayuscula:
                            query =
                                "UPPER(SUBSTR(argument_name,1,1)) || LOWER(SUBSTR(argument_name, 2, LENGTH(argument_name) - 1)) AS \"ColName\" ,  ";
                            break;

                        case TipoColumnas.Mayusculas:
                            query = "UPPER(argument_name) AS \"ColName\" ,  ";
                            break;

                        case TipoColumnas.Minusculas:
                            query = "LOWER(argument_name) AS \"ColName\" ,  ";
                            break;
                    }

                    query = " SELECT object_name AS ObjName,  'P' AS ObjType," + query + " position AS ColOrder," +
                            " default_length AS ColLen, 0 AS ColKey, data_type AS tipo" +
                            " FROM all_arguments aa  WHERE aa.owner = '" + Principal.Catalogo + "' " +
                            " AND object_name = '" + nombreSP.ToUpper() + "' AND data_type <> 'REF CURSOR'" +
                            " ORDER BY position";

                    break;

                case TipoBD.PostgreSQL:
                    query = " SELECT r.routine_name AS objname, 'P' as objtype, p.parameter_name AS colname,        p.ordinal_position AS colorder, 0 as collen, 0 as colkey,         0 as xtype, p.data_type AS tipo  FROM information_schema.routines r      JOIN information_schema.parameters p ON r.specific_name = p.specific_name  WHERE r.specific_schema='public'  AND UPPER(r.routine_name) = UPPER('" + nombreSP + "')  ORDER BY p.ordinal_position; ";
                    break;
            }

            DataTable dtSps = new DataTable();
            dtSps = CFuncionesBDs.CargarDataTable(query);

            return dtSps;
        }
    }
}