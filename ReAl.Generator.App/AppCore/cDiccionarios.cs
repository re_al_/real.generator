﻿using ReAl.Generator.App.AppClass;
using System.Data;
using System.Text;

namespace ReAl.Generator.App.AppCore
{
    public static class cDiccionarios
    {
        #region Fields

        private static bool SetBoolean = false;
        private static bool SetByte = false;
        private static bool SetDateTime = true;
        private static bool SetDecimal = false;
        private static bool SetDefault = true;
        private static bool SetInt = false;
        private static bool SetInt32 = false;
        private static bool SetString = true;

        #endregion Fields

        #region Methods

        //Lineas 62
        public static string arrValInsert(string pNameClaseEnt, string NameTabla, DataTable dtColumns, FPrincipal formulario, TipoEntorno miEntorno)
        {
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string formatoFechaHora = paramClaseParametros + ".parFormatoFechaHora";
            string formatoFecha = paramClaseParametros + ".parFormatoFecha";
            if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                miEntorno == TipoEntorno.CSharp_WebForms ||
                miEntorno == TipoEntorno.CSharp_WebAPI ||
                miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
            {
                formatoFechaHora = paramClaseParametros + ".ParFormatoFechaHora";
                formatoFecha = paramClaseParametros + ".ParFormatoFecha";
            }
            StringBuilder newTexto = new StringBuilder();

            string nameCol = "";
            string strApiTrans = "";
            string strApiEstado = "";
            string strUsuCre = "";
            string strFecCre = "";
            string strUsuMod = "";
            string strFecMod = "";
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("cre"))
                    strUsuCre = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("mod"))
                    strUsuMod = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("cre"))
                    strFecCre = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("mod"))
                    strFecMod = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("estado"))
                    strApiEstado = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("transaccion"))
                    strApiTrans = nameCol;
            }

            if (string.IsNullOrEmpty(strApiTrans))
                strApiTrans = "----";
            if (string.IsNullOrEmpty(strApiEstado))
                strApiEstado = "----";
            if (string.IsNullOrEmpty(strUsuCre))
                strUsuCre = "----";
            if (string.IsNullOrEmpty(strFecCre))
                strFecCre = "----";
            if (string.IsNullOrEmpty(strUsuMod))
                strUsuMod = "----";
            if (string.IsNullOrEmpty(strFecMod))
                strFecMod = "----";

            if (formulario.chkDataAnnotations.Checked)
            {
                newTexto.AppendLine("\t\t\t\tif (!obj.IsValid())");
                newTexto.AppendLine("\t\t\t\t{");
                newTexto.AppendLine("\t\t\t\t\tthrow new System.ComponentModel.DataAnnotations.ValidationException(obj.ValidationErrorsString());");
                newTexto.AppendLine("\t\t\t\t}");
            }
            newTexto.AppendLine("\t\t\t\t");

            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                if (!nameCol.Contains(strUsuMod) & !nameCol.Contains(strFecMod) & !nameCol.Contains(strApiTrans) & !nameCol.Contains(strFecCre))
                {
                    if (dtColumns.Rows[iCol]["EsIdentidad"].ToString() == "Yes")
                    {
                        newTexto.AppendLine("\t\t\t\t//arrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
                    }
                }
            }
            newTexto.AppendLine("\t\t\t\t");

            newTexto.AppendLine("\t\t\t\tArrayList arrValores = new ArrayList();");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string tipoCol = dtColumns.Rows[iCol][1].ToString();
                nameCol = dtColumns.Rows[iCol][0].ToString();

                if (!nameCol.Contains(strUsuMod) & !nameCol.Contains(strFecMod) & !nameCol.Contains(strApiTrans) & !nameCol.Contains(strFecCre))
                {
                    switch (tipoCol.ToUpper())
                    {
                        case "INT":
                            if (SetInt == true)
                            {
                                if (dtColumns.Rows[iCol]["EsIdentidad"].ToString() == "Yes")
                                {
                                    newTexto.AppendLine("\t\t\t\t//arrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                            }
                            else
                            {
                                if (dtColumns.Rows[iCol]["EsIdentidad"].ToString() == "Yes")
                                {
                                    newTexto.AppendLine("\t\t\t\t//arrValores.Add(obj." + nameCol + ");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                                }
                            }
                            break;

                        case "INT32":
                            if (SetInt32 == true)
                            {
                                if (dtColumns.Rows[iCol]["EsIdentidad"].ToString() == "Yes")
                                {
                                    newTexto.AppendLine("\t\t\t\t//arrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                            }
                            else
                            {
                                if (dtColumns.Rows[iCol]["EsIdentidad"].ToString() == "Yes")
                                {
                                    newTexto.AppendLine("\t\t\t\t//arrValores.Add(obj." + nameCol + ");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                                }
                            }
                            break;

                        case "DECIMAL":
                            if (SetDecimal == true)
                            {
                                newTexto.AppendLine("\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                            }
                            break;

                        case "BOOL":
                            if (SetBoolean == true)
                            {
                                newTexto.AppendLine("\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                            }
                            break;

                        case "BOOLEAN":
                            if (SetBoolean == true)
                            {
                                newTexto.AppendLine("\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                            }
                            break;

                        case "BYTE[]":
                            if (SetByte == true)
                            {
                                newTexto.AppendLine("\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                            }
                            break;

                        case "BYTE":
                            if (SetByte == true)
                            {
                                newTexto.AppendLine("\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                            }
                            break;

                        case "DATE":
                            if (SetDateTime == true)
                            {
                                newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : \"'\" + Convert.ToDateTime(obj." + nameCol + ").ToString(" + formatoFecha + ") + \"'\");");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                            }
                            break;

                        case "DATETIME":
                            if (SetDateTime == true)
                            {
                                newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : \"'\" + Convert.ToDateTime(obj." + nameCol + ").ToString(" + formatoFechaHora + ") + \"'\");");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                            }
                            break;

                        case "STRING":
                            if (SetString == true)
                            {
                                newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : \"'\" + obj." + nameCol + " + \"'\");");
                            }
                            else
                            {
                                newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                            }
                            break;

                        default:
                            if (SetDefault == true)
                            {
                                if (dtColumns.Rows[iCol]["EsIdentidad"].ToString() == "Yes")
                                {
                                    newTexto.AppendLine("\t\t\t\t//arrValores.Add(obj." + nameCol + " == null ? null : \"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : \"'\" + obj." + nameCol + " + \"'\");");
                                }
                            }
                            else
                            {
                                if (dtColumns.Rows[iCol]["EsIdentidad"].ToString() == "Yes")
                                {
                                    newTexto.AppendLine("\t\t\t\t//arrValores.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                                }
                            }
                            break;
                    }
                }
            }

            return newTexto.ToString();
        }

        public static string arrValTodo(string pNameClaseEnt, string NameTabla, DataTable dtColumns, FPrincipal formulario, TipoEntorno miEntorno)
        {
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string formatoFechaHora = paramClaseParametros + ".parFormatoFechaHora";
            string formatoFecha = paramClaseParametros + ".parFormatoFecha";

            if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                miEntorno == TipoEntorno.CSharp_WebForms ||
                miEntorno == TipoEntorno.CSharp_WebAPI ||
                miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
            {
                formatoFechaHora = paramClaseParametros + ".ParFormatoFechaHora";
                formatoFecha = paramClaseParametros + ".ParFormatoFecha";
            }

            StringBuilder newTexto = new StringBuilder();

            string nameCol = "";

            if (formulario.chkDataAnnotations.Checked)
            {
                newTexto.AppendLine("\t\t\t\tif (!obj.IsValid())");
                newTexto.AppendLine("\t\t\t\t{");
                newTexto.AppendLine("\t\t\t\t\tthrow new System.ComponentModel.DataAnnotations.ValidationException(obj.ValidationErrorsString());");
                newTexto.AppendLine("\t\t\t\t}");
            }
            newTexto.AppendLine("\t\t\t\t");
            newTexto.AppendLine("\t\t\t\tArrayList arrNombreParam = new ArrayList();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                if (Principal.DBMS == TipoBD.Oracle)
                {
                    if (formulario.chkParametroPa.Checked)
                    {
                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameCol + "\");");
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"" + nameCol.ToUpper() + "\");");
                    }
                }
                else
                {
                    if (formulario.chkParametroPa.Checked)
                    {
                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(\"pa" + nameCol + "\");");
                    }
                    else
                    {
                        newTexto.AppendLine("\t\t\t\tarrNombreParam.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
                    }
                }
            }
            newTexto.AppendLine("\t\t\t\t");

            newTexto.AppendLine("\t\t\t\tArrayList arrValoresParam = new ArrayList();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                string tipoCol = dtColumns.Rows[iCol][1].ToString();
                nameCol = dtColumns.Rows[iCol][0].ToString();

                switch (tipoCol.ToUpper())
                {
                    case "INT":
                        if (SetInt == true)
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"'\" + obj." + nameCol + " + \"'\");");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ");");
                        }
                        break;

                    case "INT32":
                        if (SetInt32 == true)
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"'\" + obj." + nameCol + " + \"'\");");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ");");
                        }
                        break;

                    case "DECIMAL":
                        if (SetDecimal == true)
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"'\" + obj." + nameCol + " + \"'\");");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ");");
                        }
                        break;

                    case "BOOL":
                        if (SetBoolean == true)
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"'\" + obj." + nameCol + " + \"'\");");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ");");
                        }
                        break;

                    case "BOOLEAN":
                        if (SetBoolean == true)
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"'\" + obj." + nameCol + " + \"'\");");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ");");
                        }
                        break;

                    case "BYTE[]":
                        if (SetByte == true)
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"'\" + obj." + nameCol + " + \"'\");");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + ");");
                        }
                        break;

                    case "BYTE":
                        if (SetByte == true)
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(\"'\" + obj." + nameCol + " + \"'\");");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                        }
                        break;

                    case "DATE":
                        if (SetDateTime == true)
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + " == null ? null : \"'\" + Convert.ToDateTime(obj." + nameCol + ").ToString(" + formatoFecha + ") + \"'\");");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                        }
                        break;

                    case "DATETIME":
                        if (SetDateTime == true)
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + " == null ? null : \"'\" + Convert.ToDateTime(obj." + nameCol + ").ToString(" + formatoFechaHora + ") + \"'\");");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                        }
                        break;

                    case "STRING":
                        if (SetString == true)
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + " == null ? null : \"'\" + obj." + nameCol + " + \"'\");");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                        }
                        break;

                    default:
                        if (SetDefault == true)
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + " == null ? null : \"'\" + obj." + nameCol + " + \"'\");");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\t\t\tarrValoresParam.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                        }
                        break;
                }
            }

            return newTexto.ToString();
        }

        public static string arrValUpdate(string pNameClaseEnt, string NameTabla, DataTable dtColumns, FPrincipal formulario, TipoEntorno miEntorno)
        {
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string formatoFechaHora = paramClaseParametros + ".parFormatoFechaHora";
            string formatoFecha = paramClaseParametros + ".parFormatoFecha";
            if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                miEntorno == TipoEntorno.CSharp_WebForms ||
                miEntorno == TipoEntorno.CSharp_WebAPI ||
                miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
            {
                formatoFechaHora = paramClaseParametros + ".ParFormatoFechaHora";
                formatoFecha = paramClaseParametros + ".ParFormatoFecha";
            }
            StringBuilder newTexto = new StringBuilder();

            string nameCol = "";
            string strApiTrans = "";
            string strApiEstado = "";
            string strUsuCre = "";
            string strFecCre = "";
            string strUsuMod = "";
            string strFecMod = "";
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("cre"))
                    strUsuCre = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("mod"))
                    strUsuMod = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("cre"))
                    strFecCre = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("mod"))
                    strFecMod = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("estado"))
                    strApiEstado = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("transaccion"))
                    strApiTrans = nameCol;
            }

            if (string.IsNullOrEmpty(strApiTrans))
                strApiTrans = "----";
            if (string.IsNullOrEmpty(strApiEstado))
                strApiEstado = "----";
            if (string.IsNullOrEmpty(strUsuCre))
                strUsuCre = "----";
            if (string.IsNullOrEmpty(strFecCre))
                strFecCre = "----";
            if (string.IsNullOrEmpty(strUsuMod))
                strUsuMod = "----";
            if (string.IsNullOrEmpty(strFecMod))
                strFecMod = "----";

            if (formulario.chkDataAnnotations.Checked)
            {
                newTexto.AppendLine("\t\t\t\tif (!obj.IsValid())");
                newTexto.AppendLine("\t\t\t\t{");
                newTexto.AppendLine("\t\t\t\t\tthrow new System.ComponentModel.DataAnnotations.ValidationException(obj.ValidationErrorsString());");
                newTexto.AppendLine("\t\t\t\t}");
            }
            newTexto.AppendLine("\t\t\t\t");

            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "NO"))
                {
                    if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()))
                    {
                        nameCol = dtColumns.Rows[iCol][0].ToString();
                        newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
                    }
                }
            }
            newTexto.AppendLine("\t\t\t");

            newTexto.AppendLine("\t\t\t\tArrayList arrValores = new ArrayList();");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "NO"))
                {
                    //newTexto.AppendLine(vbTab & vbTab & vbTab & vbTab & "arrValores.Add(" & Chr(34) & "'" & Chr(34) & " + e" & NameTabla(0) & "." & NameCol & ".ToString() + " & Chr(34) & "'" & Chr(34) & ");" & vbCrLf
                    string tipoCol = dtColumns.Rows[iCol][1].ToString();
                    nameCol = dtColumns.Rows[iCol][0].ToString();

                    if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()))
                    {
                        switch (tipoCol.ToUpper())
                        {
                            case "INT":
                                if (SetInt == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                                }
                                break;

                            case "INT32":
                                if (SetInt32 == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                                }
                                break;

                            case "DECIMAL":
                                if (SetDecimal == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                                }
                                break;

                            case "BOOL":
                                if (SetBoolean == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                                }
                                break;

                            case "BOOLEAN":
                                if (SetBoolean == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                                }
                                break;

                            case "BYTE[]":
                                if (SetByte == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                                }
                                break;

                            case "BYTE":
                                if (SetByte == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                                }
                                break;

                            case "DATE":
                                if (SetDateTime == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : \"'\" + Convert.ToDateTime(obj." + nameCol + ").ToString(" + formatoFecha + ") + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                                }
                                break;

                            case "DATETIME":
                                if (SetDateTime == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : \"'\" + Convert.ToDateTime(obj." + nameCol + ").ToString(" + formatoFechaHora + ") + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                                }
                                break;

                            case "STRING":
                                if (SetString == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : \"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                                }
                                break;

                            default:
                                if (SetDefault == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : \"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                                }
                                break;
                        }
                    }
                }
            }

            return newTexto.ToString();
        }

        public static string arrValUpdateCambios(string pNameClaseEnt, string NameTabla, DataTable dtColumns, FPrincipal formulario, TipoEntorno miEntorno)
        {
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string formatoFechaHora = paramClaseParametros + ".parFormatoFechaHora";
            string formatoFecha = paramClaseParametros + ".parFormatoFecha";
            if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                miEntorno == TipoEntorno.CSharp_WebForms ||
                miEntorno == TipoEntorno.CSharp_WebAPI ||
                miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
            {
                formatoFechaHora = paramClaseParametros + ".ParFormatoFechaHora";
                formatoFecha = paramClaseParametros + ".ParFormatoFecha";
            }
            string nameCol = "";
            string tipoCol = "";

            StringBuilder newTexto = new StringBuilder();

            string strApiTrans = "";
            string strApiEstado = "";
            string strUsuCre = "";
            string strFecCre = "";
            string strUsuMod = "";
            string strFecMod = "";
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("cre"))
                    strUsuCre = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("mod"))
                    strUsuMod = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("cre"))
                    strFecCre = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("mod"))
                    strFecMod = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("estado"))
                    strApiEstado = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("transaccion"))
                    strApiTrans = nameCol;
            }

            if (string.IsNullOrEmpty(strApiTrans))
                strApiTrans = "----";
            if (string.IsNullOrEmpty(strApiEstado))
                strApiEstado = "----";
            if (string.IsNullOrEmpty(strUsuCre))
                strUsuCre = "----";
            if (string.IsNullOrEmpty(strFecCre))
                strFecCre = "----";
            if (string.IsNullOrEmpty(strUsuMod))
                strUsuMod = "----";
            if (string.IsNullOrEmpty(strFecMod))
                strFecMod = "----";

            if (formulario.chkDataAnnotations.Checked)
            {
                newTexto.AppendLine("\t\t\t\tif (!obj.IsValid())");
                newTexto.AppendLine("\t\t\t\t{");
                newTexto.AppendLine("\t\t\t\t\tthrow new System.ComponentModel.DataAnnotations.ValidationException(obj.ValidationErrorsString());");
                newTexto.AppendLine("\t\t\t\t}");
            }
            newTexto.AppendLine("\t\t\t\t");

            newTexto.AppendLine("\t\t\t\tArrayList arrColumnas = new ArrayList();");
            newTexto.AppendLine("\t\t\t\tArrayList arrValores = new ArrayList();");
            newTexto.AppendLine("\t\t\t\t");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                tipoCol = dtColumns.Rows[iCol][1].ToString();
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "NO"))
                {
                    if (nameCol.ToUpper().Contains(strUsuMod.ToUpper()))
                    {
                        newTexto.AppendLine("");
                        newTexto.AppendLine("\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
                        if (SetString == true)
                        {
                            newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : \"'\" + obj." + nameCol + " + \"'\");");
                        }
                        else
                        {
                            newTexto.AppendLine("\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                        }
                        newTexto.AppendLine("");
                    }
                    else if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()))
                    {
                        newTexto.AppendLine("\t\t\t\tif(obj." + nameCol + " != objOriginal." + nameCol + " )");
                        newTexto.AppendLine("\t\t\t\t{");
                        newTexto.AppendLine("\t\t\t\t\tarrColumnas.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
                        if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()))
                        {
                            switch (tipoCol.ToUpper())
                            {
                                case "INT":
                                    if (SetInt == true)
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                    }
                                    else
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                                    }
                                    break;

                                case "INT32":
                                    if (SetInt32 == true)
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                    }
                                    else
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                                    }
                                    break;

                                case "DECIMAL":
                                    if (SetDecimal == true)
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                    }
                                    else
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                                    }
                                    break;

                                case "BOOL":
                                    if (SetBoolean == true)
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                    }
                                    else
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                                    }
                                    break;

                                case "BOOLEAN":
                                    if (SetBoolean == true)
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                    }
                                    else
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                                    }
                                    break;

                                case "BYTE[]":
                                    if (SetByte == true)
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                    }
                                    else
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                                    }
                                    break;

                                case "BYTE":
                                    if (SetByte == true)
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                    }
                                    else
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(obj." + nameCol + ");");
                                    }
                                    break;

                                case "DATE":
                                    if (SetDateTime == true)
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : \"'\" + Convert.ToDateTime(obj." + nameCol + ").ToString(" + formatoFecha + ") + \"'\");");
                                    }
                                    else
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                                    }
                                    break;

                                case "DATETIME":
                                    if (SetDateTime == true)
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : \"'\" + Convert.ToDateTime(obj." + nameCol + ").ToString(" + formatoFechaHora + ") + \"'\");");
                                    }
                                    else
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                                    }
                                    break;

                                case "STRING":
                                    if (SetString == true)
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : \"'\" + obj." + nameCol + " + \"'\");");
                                    }
                                    else
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                                    }
                                    break;

                                default:
                                    if (SetDefault == true)
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : \"'\" + obj." + nameCol + " + \"'\");");
                                    }
                                    else
                                    {
                                        newTexto.AppendLine("\t\t\t\t\tarrValores.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                                    }
                                    break;
                            }
                        }
                        newTexto.AppendLine("\t\t\t\t}");
                    }
                }
            }
            newTexto.AppendLine("\t\t\t");

            return newTexto.ToString();
        }

        public static string arrWhereUpdate(string pNameClaseEnt, string NameTabla, DataTable dtColumns, FPrincipal formulario, TipoEntorno miEntorno)
        {
            string paramClaseParametros = (string.IsNullOrEmpty(formulario.txtClaseParametros.Text) ? "cParametros" : formulario.txtClaseParametros.Text);
            string formatoFechaHora = paramClaseParametros + ".parFormatoFechaHora";
            string formatoFecha = paramClaseParametros + ".parFormatoFecha";
            if (miEntorno == TipoEntorno.CSharp_NetCore_V5 ||
                miEntorno == TipoEntorno.CSharp_NetCore_WebAPI_V2_Swagger ||
                miEntorno == TipoEntorno.CSharp_WebForms ||
                miEntorno == TipoEntorno.CSharp_WebAPI ||
                miEntorno == TipoEntorno.CSharp_WinForms || miEntorno == TipoEntorno.CSharp)
            {
                formatoFechaHora = paramClaseParametros + ".ParFormatoFechaHora";
                formatoFecha = paramClaseParametros + ".ParFormatoFecha";
            }
            StringBuilder newTexto = new StringBuilder();

            string nameCol = "";
            string strApiTrans = "";
            string strApiEstado = "";
            string strUsuCre = "";
            string strFecCre = "";
            string strUsuMod = "";
            string strFecMod = "";
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                nameCol = dtColumns.Rows[iCol][0].ToString();
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("cre"))
                    strUsuCre = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("mod"))
                    strUsuMod = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("cre"))
                    strFecCre = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("mod"))
                    strFecMod = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("estado"))
                    strApiEstado = nameCol;
                if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("transaccion"))
                    strApiTrans = nameCol;
            }

            if (string.IsNullOrEmpty(strApiTrans))
                strApiTrans = "----";
            if (string.IsNullOrEmpty(strApiEstado))
                strApiEstado = "----";
            if (string.IsNullOrEmpty(strUsuCre))
                strUsuCre = "----";
            if (string.IsNullOrEmpty(strFecCre))
                strFecCre = "----";
            if (string.IsNullOrEmpty(strUsuMod))
                strUsuMod = "----";
            if (string.IsNullOrEmpty(strFecMod))
                strFecMod = "----";

            newTexto.AppendLine("\t\t\t\tArrayList arrColumnasWhere = new ArrayList();");
            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    newTexto.AppendLine("\t\t\t\tarrColumnasWhere.Add(" + pNameClaseEnt + ".Fields." + nameCol + ".ToString());");
                }
            }
            newTexto.AppendLine("\t\t\t");

            newTexto.AppendLine("\t\t\t\tArrayList arrValoresWhere = new ArrayList();");

            for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
            {
                if ((dtColumns.Rows[iCol]["EsPK"].ToString().ToUpper() == "YES"))
                {
                    nameCol = dtColumns.Rows[iCol][0].ToString();
                    string tipoCol = dtColumns.Rows[iCol][1].ToString();

                    if (!nameCol.Contains(strUsuMod) & !nameCol.Contains(strFecMod))
                    {
                        switch (tipoCol.ToUpper())
                        {
                            case "INT":
                                if (SetInt == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(obj." + nameCol + ");");
                                }
                                break;

                            case "INT32":
                                if (SetInt32 == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(obj." + nameCol + ");");
                                }
                                break;

                            case "DECIMAL":
                                if (SetDecimal == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(obj." + nameCol + ");");
                                }
                                break;

                            case "BOOL":
                                if (SetBoolean == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(obj." + nameCol + ");");
                                }
                                break;

                            case "BOOLEAN":
                                if (SetBoolean == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(obj." + nameCol + ");");
                                }
                                break;

                            case "BYTE[]":
                                if (SetByte == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(obj." + nameCol + ");");
                                }
                                break;

                            case "BYTE":
                                if (SetByte == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(\"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(obj." + nameCol + ");");
                                }
                                break;

                            case "DATE":
                                if (SetDateTime == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(obj." + nameCol + " == null ? null : \"'\" + Convert.ToDateTime(obj." + nameCol + ").ToString(" + formatoFecha + ") + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                                }
                                break;

                            case "DATETIME":
                                if (SetDateTime == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(obj." + nameCol + " == null ? null : \"'\" + Convert.ToDateTime(obj." + nameCol + ").ToString(" + formatoFechaHora + ") + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                                }
                                break;

                            case "STRING":
                                if (SetString == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(obj." + nameCol + " == null ? null : \"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                                }
                                break;

                            default:
                                if (SetDefault == true)
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(obj." + nameCol + " == null ? null : \"'\" + obj." + nameCol + " + \"'\");");
                                }
                                else
                                {
                                    newTexto.AppendLine("\t\t\t\tarrValoresWhere.Add(obj." + nameCol + " == null ? null : obj." + nameCol + ");");
                                }
                                break;
                        }
                    }
                }
            }

            return newTexto.ToString();
        }

        #endregion Methods
    }
}