﻿using FirebirdSql.Data.FirebirdClient;
using MySqlConnector;
using Npgsql;
using ReAl.Generator.App.AppClass;
using System;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Data.SQLite;
using System.Text;

namespace ReAl.Generator.App.AppCore
{
    public static class CFuncionesBDs
    {
        public static void EjecutarQuery(string Query)
        {
            try
            {
                CConeccion clsCon = new CConeccion();

                DataTable dt = new DataTable();
                dt.Clear();

                switch (Principal.DBMS)
                {
                    case TipoBD.MySql:
                        if (clsCon.CnMy.State == ConnectionState.Closed)
                            clsCon.CnMy.Open();                                                
                        try
                        {
                            var commandMy = new MySqlCommand(Query, clsCon.CnMy);
                            int numReg = commandMy.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            dt.Clear();
                        }
                        if (clsCon.CnMy.State == ConnectionState.Open)
                            clsCon.CnMy.Close();  
                        break;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public static DataTable CargarDataTable(string Query)
        {
            try
            {
                CConeccion clsCon = new CConeccion();

                DataTable dt = new DataTable();
                dt.Clear();

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        OleDbDataAdapter daOleDb = new OleDbDataAdapter();
                        if (clsCon.CnSql.State == ConnectionState.Closed)
                            clsCon.CnSql.Open();
                        try
                        {
                            daOleDb = new OleDbDataAdapter(Query, clsCon.CnSql);
                            daOleDb.Fill(dt);
                        }
                        catch (Exception ex)
                        {
                            dt.Clear();
                            if (!Query.Contains("SegTablas"))
                            {
                                //MsgBox(ex.Message, MsgBoxStyle.Critical)
                            }
                        }
                        if (clsCon.CnSql.State == ConnectionState.Open)
                            clsCon.CnSql.Close();

                        return dt;

                    case TipoBD.SQLServer2000:
                        OleDbDataAdapter daOleDb2000 = new OleDbDataAdapter();
                        if (clsCon.CnSql.State == ConnectionState.Closed)
                            clsCon.CnSql.Open();
                        try
                        {
                            daOleDb2000 = new OleDbDataAdapter(Query, clsCon.CnSql);
                            daOleDb2000.Fill(dt);
                        }
                        catch (Exception ex)
                        {
                            dt.Clear();
                        }
                        if (clsCon.CnSql.State == ConnectionState.Open)
                            clsCon.CnSql.Close();

                        return dt;

                    case TipoBD.Oracle:
                        OracleDataAdapter daOracle = new OracleDataAdapter();
                        if (clsCon.CnOra.State == ConnectionState.Closed)
                            clsCon.CnOra.Open();
                        try
                        {
                            daOracle = new OracleDataAdapter(Query, clsCon.CnOra);
                            daOracle.Fill(dt);
                        }
                        catch (Exception ex)
                        {
                            dt.Clear();
                        }
                        if (clsCon.CnOra.State == ConnectionState.Open)
                            clsCon.CnOra.Close();

                        return dt;

                    case TipoBD.PostgreSQL:
                        NpgsqlDataAdapter daPg = new NpgsqlDataAdapter();
                        if (clsCon.CnPg.State == ConnectionState.Closed)
                            clsCon.CnPg.Open();
                        try
                        {
                            daPg = new NpgsqlDataAdapter(Query, clsCon.CnPg);
                            daPg.Fill(dt);
                        }
                        catch (Exception ex)
                        {
                            dt.Clear();
                        }
                        if (clsCon.CnPg.State == ConnectionState.Open)
                            clsCon.CnPg.Close();

                        return dt;

                    case TipoBD.MySql:
                        MySqlDataAdapter daMy = new MySqlDataAdapter();
                        if (clsCon.CnMy.State == ConnectionState.Closed)
                            clsCon.CnMy.Open();
                        try
                        {
                            daMy = new MySqlDataAdapter(Query, clsCon.CnMy);
                            daMy.Fill(dt);
                        }
                        catch (Exception ex)
                        {
                            dt.Clear();
                        }
                        if (clsCon.CnMy.State == ConnectionState.Open)
                            clsCon.CnMy.Close();

                        return dt;

                    case TipoBD.FireBird:
                        try
                        {
                            dynamic command = new FbCommand(Query, clsCon.CnFb);
                            if (command.Connection.State == ConnectionState.Closed)
                                command.Connection.Open();

                            DbDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection);
                            dt.Load(dr);
                            dr.Close();
                            if (command.Connection.State != ConnectionState.Closed)
                                command.Connection.Close();
                        }
                        catch (FbException ex)
                        {
                            dt.Clear();
                        }
                        catch (Exception ex)
                        {
                            dt.Clear();
                        }
                        if (clsCon.CnFb.State == ConnectionState.Open)
                            clsCon.CnFb.Close();

                        return dt;

                    case TipoBD.SQLite:
                        try
                        {
                            dynamic command = new SQLiteCommand(Query, clsCon.CnLite);
                            if (command.Connection.State == ConnectionState.Closed)
                                command.Connection.Open();

                            DbDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection);
                            dt.Load(dr);
                            dr.Close();
                            if (command.Connection.State != ConnectionState.Closed)
                                command.Connection.Close();
                        }
                        catch (SQLiteException ex)
                        {
                            dt.Clear();
                        }
                        catch (Exception ex)
                        {
                            dt.Clear();
                        }
                        if (clsCon.CnLite.State == ConnectionState.Open)
                            clsCon.CnLite.Close();
                        return dt;

                    default:
                        return new DataTable();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static DataTable ObtenerListadoColumnas(string schemaName, string idTable, Languaje lang, TipoColumnas tipoCol, string strColumns = "")
        {
            StringBuilder strQuey = new StringBuilder();
            string query = "";
            switch (Principal.DBMS)
            {
                case TipoBD.SQLServer:

                    #region "SqlServer"

                    strQuey = new StringBuilder();

                    switch (tipoCol)
                    {
                        case TipoColumnas.PrimeraMayuscula:
                            strQuey.AppendLine(" SELECT UPPER(substring(sc.name,1,1)) + LOWER(substring(sc.name, 2, LEN(sc.name) - 1)) AS name, ");
                            break;

                        case TipoColumnas.Mayusculas:
                            strQuey.AppendLine("SELECT  UPPER(sc.name) AS NAME,");
                            break;

                        case TipoColumnas.Minusculas:
                            strQuey.AppendLine("SELECT  LOWER(sc.name) AS NAME,");
                            break;
                    }

                    strQuey.AppendLine(" CASE st.name ");
                    strQuey.AppendLine(" \t\t\t WHEN 'nvarchar' then 'String' ");
                    strQuey.AppendLine(" \t\t\t WHEN 'varchar' then 'String'  ");
                    strQuey.AppendLine(" \t\t\t WHEN 'text' then 'String'  ");
                    strQuey.AppendLine(" \t\t\t WHEN 'String' then 'String' ");
                    strQuey.AppendLine(" \t\t\t WHEN 'char' then 'String' ");
                    strQuey.AppendLine(" \t\t\t WHEN 'datetime' then 'DateTime' ");
                    strQuey.AppendLine(" \t\t\t WHEN 'timestamp' then 'DateTime' ");
                    strQuey.AppendLine(" \t\t\t WHEN 'date' then 'DateTime' ");
                    strQuey.AppendLine(" \t\t\t WHEN 'time' then 'DateTime' ");
                    strQuey.AppendLine(" \t\t\t WHEN 'smalldatetime' then 'DateTime' ");
                    if (lang == Languaje.CSharp)
                    {
                        strQuey.AppendLine("             WHEN 'bit' then 'bool' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'image' then 'Byte[]' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'numeric' then 'Decimal' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'Int64' then 'int' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'tinyint' then 'int' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'smallint' then 'int' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'bigint' then 'Int64' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'int' then 'int' ");
                    }
                    else if (lang == Languaje.VbNet)
                    {
                        strQuey.AppendLine("             WHEN 'bit' then 'Boolean' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'image' then 'Byte()' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'numeric' then 'Decimal' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'Int64' then 'Integer' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'tinyint' then 'Integer' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'smallint' then 'Integer' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'bigint' then 'Integer' ");
                        strQuey.AppendLine(" \t\t\t WHEN 'int' then 'Integer' ");
                    }
                    strQuey.AppendLine(" \t\t\t ELSE st.name END AS tipo,  ");
                    strQuey.AppendLine(" so.name as Tabla, ");
                    strQuey.AppendLine(" UPPER(LEFT(sc.name,1)) + RIGHT(sc.name, LEN(sc.name) -1) AS name_upper,  ");
                    strQuey.AppendLine(" CASE COLUMNPROPERTY( sc.[object_id] ,sc.name, 'AllowsNull') WHEN 1 THEN 'Yes' ELSE 'No' END AS PermiteNull, ");
                    strQuey.AppendLine(" CASE COLUMNPROPERTY( sc.[object_id] ,sc.name, 'IsIdentity') WHEN 1 THEN 'Yes' ELSE 'No' END AS EsIdentidad, ");
                    strQuey.AppendLine(" CASE COLUMNPROPERTY( sc.[object_id] ,sc.name, 'IsComputed') WHEN 1 THEN 'Yes' ELSE 'No' END AS EsCalculada, ");
                    strQuey.AppendLine(" CASE COLUMNPROPERTY( sc.[object_id] ,sc.name, 'IsRowGuidCol') WHEN 1 THEN 'Yes' ELSE 'No' END AS EsRowGui, ");
                    strQuey.AppendLine(" CASE WHEN (SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B ");
                    strQuey.AppendLine(" \tWHERE A.CONSTRAINT_NAME = B.CONSTRAINT_NAME AND B.COLUMN_NAME = sc.name AND CONSTRAINT_TYPE = 'PRIMARY KEY' ");
                    strQuey.AppendLine(" \tAND upper(so.name) = upper(A.TABLE_NAME)) = 0 THEN 'No' ELSE 'Yes' END as EsPK, ");
                    strQuey.AppendLine(" CASE WHEN (SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B ");
                    strQuey.AppendLine(" \tWHERE A.CONSTRAINT_NAME = B.CONSTRAINT_NAME AND B.COLUMN_NAME = sc.name AND CONSTRAINT_TYPE = 'FOREIGN KEY' ");
                    strQuey.AppendLine(" \tAND upper(so.name) = upper(A.TABLE_NAME)) = 0 THEN 'No' ELSE 'Yes' END as EsFK,\t ");
                    strQuey.AppendLine(" (SELECT TOP 1 SO2.NAME REF_TABLE_NAME FROM SYSFOREIGNKEYS SYSFK ");
                    strQuey.AppendLine(" \tINNER JOIN (SELECT UID, ID, NAME FROM SYSOBJECTS WHERE XTYPE = 'U') SO1 ON SYSFK.FKEYID = SO1.ID ");
                    strQuey.AppendLine(" \tINNER JOIN (SELECT UID, ID, NAME FROM SYSOBJECTS WHERE XTYPE = 'U') SO2 ON SYSFK.RKEYID = SO2.ID ");
                    strQuey.AppendLine(" \tINNER JOIN (select ID, COLID, NAME FROM SYSCOLUMNS) SC1 ON SYSFK.FKEYID = SC1.ID AND SYSFK.FKEY = SC1.COLID ");
                    strQuey.AppendLine(" \tINNER JOIN (select ID, COLID, NAME FROM SYSCOLUMNS) SC2 ON SYSFK.RKEYID = SC2.ID AND SYSFK.RKEY = SC2.COLID ");
                    strQuey.AppendLine(" \tINNER JOIN (SELECT ID, NAME FROM SYSOBJECTS) SO3 ON SYSFK.CONSTID = SO3.ID");
                    strQuey.AppendLine(" \tWHERE SO1.NAME = so.name AND SC1.NAME = sc.name) AS TablaForanea,");
                    strQuey.AppendLine(" st.name AS TIPO_COL, sc.max_length AS LONGITUD, ISNULL(ep.value,'') as COL_DESC ");
                    strQuey.AppendLine(" FROM sys.columns sc ");
                    strQuey.AppendLine(" JOIN sys.types st ON sc.system_type_id = st.system_type_id AND st.name <> 'sysname' AND st.name <> 'uniqueidentifier' AND st.user_type_id <= 256");
                    strQuey.AppendLine(" JOIN sys.objects so ON sc.object_id = so.object_id ");
                    strQuey.AppendLine(" LEFT JOIN sys.extended_properties ep ON ep.name = 'MS_Description' AND sc.column_id = ep.minor_id AND so.object_id = ep.major_id ");
                    strQuey.AppendLine(" WHERE sc.object_id = " + idTable + " ");
                    if (!string.IsNullOrEmpty(strColumns))
                    {
                        strQuey.AppendLine(" AND sc.name IN ('" + strColumns + "')");
                    }
                    strQuey.AppendLine(" ORDER BY sc.column_id ");

                    query = strQuey.ToString();

                    #endregion "SqlServer"

                    return CargarDataTable(query);

                case TipoBD.SQLServer2000:

                    #region "SqlServer 2000"

                    query = " SELECT UPPER(substring(sc.name,1,1)) + LOWER(substring(sc.name, 2, LEN(sc.name) - 1)) AS name,  CASE st.name WHEN 'bit' then 'Boolean'  \t\t\t WHEN 'nvarchar' then 'String'  \t\t\t WHEN 'varchar' then 'String'   \t\t\t WHEN 'text' then 'String'   \t\t\t WHEN 'String' then 'String'  \t\t\t WHEN 'datetime' then 'DateTime'  \t\t\t WHEN 'timestamp' then 'DateTime'  \t\t\t WHEN 'date' then 'DateTime'  \t\t\t WHEN 'time' then 'DateTime'  \t\t\t WHEN 'smalldatetime' then 'DateTime'  \t\t\t WHEN 'image' then 'Byte[]'  \t\t\t WHEN 'numeric' then 'Decimal'  \t\t\t WHEN 'Int64' then 'int'  \t\t\t WHEN 'tinyint' then 'int'  \t\t\t WHEN 'smallint' then 'int'  \t\t\t WHEN 'bigint' then 'Int64'  \t\t\t WHEN 'int' then 'int'  \t\t\t WHEN 'char' then 'String'  \t\t\t ELSE st.name END AS tipo,   so.name as Tabla,  UPPER(LEFT(sc.name,1)) + RIGHT(sc.name, LEN(sc.name) -1) AS name_upper,   CASE COLUMNPROPERTY( sc.[object_id] ,sc.name, 'AllowsNull') WHEN 1 THEN 'Yes' ELSE 'No' END AS PermiteNull,  CASE COLUMNPROPERTY( sc.[object_id] ,sc.name, 'IsIdentity') WHEN 1 THEN 'Yes' ELSE 'No' END AS EsIdentidad,  CASE COLUMNPROPERTY( sc.[object_id] ,sc.name, 'IsComputed') WHEN 1 THEN 'Yes' ELSE 'No' END AS EsCalculada,  CASE COLUMNPROPERTY( sc.[object_id] ,sc.name, 'IsRowGuidCol') WHEN 1 THEN 'Yes' ELSE 'No' END AS EsRowGui,  CASE WHEN (SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B  \tWHERE A.CONSTRAINT_NAME = B.CONSTRAINT_NAME AND B.COLUMN_NAME = sc.name AND CONSTRAINT_TYPE = 'PRIMARY KEY'  \tAND upper(so.name) = upper(A.TABLE_NAME)) = 0 THEN 'No' ELSE 'Yes' END as EsPK,  CASE WHEN (SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B  \tWHERE A.CONSTRAINT_NAME = B.CONSTRAINT_NAME AND B.COLUMN_NAME = sc.name AND CONSTRAINT_TYPE = 'FOREIGN KEY'  \tAND upper(so.name) = upper(A.TABLE_NAME)) = 0 THEN 'No' ELSE 'Yes' END as EsFK,\t  (SELECT TOP 1 SO2.NAME REF_TABLE_NAME FROM SYSFOREIGNKEYS SYSFK  \tINNER JOIN (SELECT UID, ID, NAME FROM SYSOBJECTS WHERE XTYPE = 'U') SO1 ON SYSFK.FKEYID = SO1.ID  \tINNER JOIN (SELECT UID, ID, NAME FROM SYSOBJECTS WHERE XTYPE = 'U') SO2 ON SYSFK.RKEYID = SO2.ID  \tINNER JOIN (select ID, COLID, NAME FROM SYSCOLUMNS) SC1 ON SYSFK.FKEYID = SC1.ID AND SYSFK.FKEY = SC1.COLID  \tINNER JOIN (select ID, COLID, NAME FROM SYSCOLUMNS) SC2 ON SYSFK.RKEYID = SC2.ID AND SYSFK.RKEY = SC2.COLID  \tINNER JOIN (SELECT ID, NAME FROM SYSOBJECTS) SO3 ON SYSFK.CONSTID = SO3.ID \tWHERE SO1.NAME = so.name AND SC1.NAME = sc.name) AS TablaForanea, st.name AS TIPO_COL, sc.max_length AS LONGITUD, ISNULL(se.value,'') as DESC_COL  FROM syscolumns sc JOIN systypes st ON sc.system_type_id = st.system_type_id AND st.name <> 'sysname' AND st.name <> 'uniqueidentifier' AND st.user_type_id <=256 JOIN sysobjects so ON AND sc.object_id = so.object_id LEFT JOIN sysproperties ep ON ep.name = 'MS_Description' AND sc.colid = ep.smallid AND so.id = ep.major_id WHERE  sc.object_id = " + idTable + " " + (string.IsNullOrEmpty(strColumns) ? "" : " AND sc.name IN ('" + strColumns + "')") + " ORDER BY sc.column_id ";

                    #endregion "SqlServer 2000"

                    return CargarDataTable(query);

                case TipoBD.Oracle:

                    #region "Oracle"

                    switch (tipoCol)
                    {
                        case TipoColumnas.PrimeraMayuscula:
                            query = "SELECT \tUPPER(SUBSTR(atc.column_name,1,1)) || LOWER(SUBSTR(atc.column_name, 2, LENGTH(atc.column_name) - 1)) AS \"name\" ,  ";
                            break;

                        case TipoColumnas.Mayusculas:
                            query = "SELECT \tUPPER(atc.column_name) AS \"name\" ,  ";
                            break;

                        case TipoColumnas.Minusculas:
                            query = "SELECT \tLOWER(atc.column_name) AS \"name\" ,  ";
                            break;
                    }

                    query = query + "\r\n       Case atc.data_type \r\n" +
                            " \t\t\tWHEN 'BLOB' THEN 'Byte[]' \r\n \t\t\tWHEN 'CHAR' THEN 'String' " +
                            "\r\n \t\t\tWHEN 'DATE' THEN 'DateTime' \r\n" +
                            " \t\t\tWHEN 'DECIMAL' THEN 'decimal' \r\n \t\t\tWHEN 'FLOAT' THEN 'Decimal' " +
                            "\r\n \t\t\tWHEN 'INTEGER' THEN 'int' \r\n" +
                            " \t\t\tWHEN 'NUMBER' THEN CASE  atc.data_scale WHEN 0 THEN 'int' ELSE 'decimal' END " +
                            "\r\n \t\t\tWHEN 'REAL' THEN 'Decimal' \r\n" +
                            " \t\t\tWHEN 'TIME' THEN 'DateTime' \r\n" +
                            " \t\t\tWHEN 'TIME WITH TZ' THEN 'DateTime' \r\n" +
                            " \t\t\tWHEN 'TIMESTAMP' THEN 'DateTime' \r\n" +
                            " \t\t\tWHEN 'TIMESTAMP(6)' THEN 'DateTime' \r\n" +
                            " \t\t\tWHEN 'VARCHAR' THEN 'String' \r\n \t\t\tWHEN 'VARCHAR2' THEN 'String' " +
                            "\r\n \t\t\tWHEN 'LONG RAW' THEN 'Byte[]' \r\n \t\t\tWHEN '' THEN '' " +
                            "\r\n \t\t\tELSE 'String' \r\n \t\tEND as \"Tipo\",  " +
                            "\r\n \t\tatc.TABLE_NAME as \"Tabla\", \r\n" +
                            " \t\tUPPER(SUBSTR(atc.column_name,1,1)) || LOWER(SUBSTR(atc.column_name, 2, LENGTH(atc.column_name) - 1)) AS " +
                            "\"name_upper\", \r\n" +
                            " \t\tCASE atc.nullable WHEN 'Y' THEN 'Yes' ELSE 'No' END AS \"PermiteNull" +
                            "\",  \r\n                     'No' AS \"EsIdentidad\"" +
                            ", \r\n                     'No' AS \"EsCalculada\", " +
                            "\r\n                     'No' AS \"EsRowGui\", \r\n" +
                            " \t\t(SELECT CASE WHEN COUNT(ac.CONSTRAINT_NAME) = 0 THEN 'No' ELSE 'Yes' END \r\n" +
                            " \t\t\tFROM all_constraints ac JOIN all_cons_columns acc ON ac.CONSTRAINT_NAME = acc.CONSTRAINT_NAME " +
                            "\r\n" +
                            "                     WHERE(acc.COLUMN_NAME = atc.COLUMN_NAME And acc.TABLE_NAME = atc.TABLE_NAME) " +
                            "\r\n \t\t\tAND ac.CONSTRAINT_TYPE = 'P' AND ac.owner= '" + Principal.Catalogo +
                            "' AND acc.owner= '" + Principal.Catalogo + "') AS  \"EsPK\", \r\n" +
                            " \t\t(SELECT CASE WHEN COUNT(ac.CONSTRAINT_NAME) = 0 THEN 'No' ELSE 'Yes' END \r\n" +
                            " \t\t\tFROM all_constraints ac JOIN all_cons_columns acc ON ac.CONSTRAINT_NAME = acc.CONSTRAINT_NAME " +
                            "\r\n" +
                            "                     WHERE(acc.COLUMN_NAME = atc.COLUMN_NAME And acc.TABLE_NAME = atc.TABLE_NAME) " +
                            "\r\n \t\t\tAND ac.CONSTRAINT_TYPE = 'R' AND ac.owner= '" + Principal.Catalogo +
                            "' AND acc.owner= '" + Principal.Catalogo + "') AS  \"EsFK\", \r\n" +
                            " \t\t(SELECT DISTINCT parent.table_name  \r\n" +
                            " \t\t\tFROM user_constraints child , \r\n \t\t\tuser_constraints PARENT,  " +
                            "\r\n \t\t\tuser_cons_columns ucc, \r\n           user_cons_columns ucc2 " +
                            "\r\n           WHERE(child.r_constraint_name = parent.constraint_name) \r\n" +
                            " \t\t\tAND child.r_owner = parent.owner \r\n" +
                            " \t\t\tAND child.constraint_type in ('R') \r\n" +
                            " \t\t\tAND child.constraint_name = ucc.constraint_name \r\n" +
                            " \t\t\tAND parent.constraint_name = ucc2.constraint_name \r\n" +
                            " \t\t\tAND child.table_name = atc.TABLE_NAME \r\n" +
                            " \t\t\tAND ucc.column_name = atc.column_name \r\n \t\t\tAND ROWNUM = 1 " +
                            "\r\n \t\t) AS \"TablaForanea\", \r\n" +
                            " \t\tatc.data_type AS \"Tipo_Col\", \r\n" +
                            " \t\tCOALESCE(atc.data_precision,atc.DATA_LENGTH) AS \"Longitud\", " +
                            "\r\n       NVL(acc.comments,'') AS COL_DESC \r\n" +
                            " FROM all_tab_columns atc \r\n" +
                            " LEFT JOIN all_col_comments acc ON atc.owner = acc.owner AND atc.table_name = acc.table_name AND atc.column_name = acc.column_name " +
                            "\r\n WHERE atc.TABLE_NAME = '" + idTable + "' \r\n AND atc.owner = '" +
                            Principal.Catalogo + "' \r\n" + (string.IsNullOrEmpty(strColumns)
                                ? ""
                                : " AND atc.column_name IN ('" + strColumns + "')") + " ORDER BY atc.column_id ";

                    #endregion "Oracle"

                    return CargarDataTable(query);

                case TipoBD.PostgreSQL:

                    #region "PostgreSql"

                    strQuey = new StringBuilder();

                    if (idTable.Contains("()"))
                    {
                        //Cargamos el DataTable para una funcion
                        DataTable dtFuncion = CargarDataTable("SELECT * FROM " + idTable);

                        DataTable dtPg = new DataTable();
                        DataColumn dcPg = new DataColumn("name", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("tipo", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("tabla", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("name_upper", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("permitenull", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("esidentidad", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("escalculada", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("esrowgui", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("espk", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("esfk", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("tablaforanea", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("tipo_col", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("longitud", typeof(string));
                        dtPg.Columns.Add(dcPg);
                        dcPg = new DataColumn("col_desc", typeof(string));
                        dtPg.Columns.Add(dcPg);

                        foreach (DataColumn column in dtFuncion.Columns)
                        {
                            DataRow dr = dtPg.NewRow();
                            dr["name"] = column.ColumnName;
                            if (column.DataType.ToString().ToUpper().Contains("INTEGER"))
                            {
                                dr["tipo"] = "int";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("INT"))
                            {
                                dr["tipo"] = "int";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("REAL"))
                            {
                                dr["tipo"] = "float";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("LONG"))
                            {
                                dr["tipo"] = "float";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("TEXT"))
                            {
                                dr["tipo"] = "String";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("NVARCHAR"))
                            {
                                dr["tipo"] = "String";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("VARCHAR"))
                            {
                                dr["tipo"] = "String";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("BLOB"))
                            {
                                dr["tipo"] = "Byte[]";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("DATE"))
                            {
                                dr["tipo"] = "long";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("DATETIME"))
                            {
                                dr["tipo"] = "long";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("BOOLEAN"))
                            {
                                dr["tipo"] = "int";
                            }
                            else if (column.DataType.ToString().ToUpper().Contains("BOOL"))
                            {
                                dr["tipo"] = "int";
                            }
                            else
                            {
                                dr["tipo"] = "String";
                            }

                            dr["tabla"] = idTable;
                            dr["name_upper"] = column.ColumnName.ToString().ToUpper();
                            dr["permitenull"] = "No";
                            dr["esidentidad"] = "No";
                            dr["escalculada"] = "No";
                            dr["esrowgui"] = "No";
                            dr["espk"] = "No";
                            dr["esfk"] = "No";
                            dr["tablaforanea"] = "";
                            dr["tipo_col"] = column.DataType;
                            dr["longitud"] = "0";
                            dr["col_desc"] = "";

                            dtPg.Rows.Add(dr);
                        }

                        return dtPg;
                    }
                    else
                    {
                        switch (tipoCol)
                        {
                            case TipoColumnas.PrimeraMayuscula:
                                strQuey.AppendLine("SELECT  UPPER(SUBSTRING(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','') ,1,1)) || LOWER(SUBSTRING(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','') ,2,LENGTH(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','')))) AS NAME,");
                                break;

                            case TipoColumnas.Mayusculas:
                                strQuey.AppendLine("SELECT  UPPER(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','')) AS NAME,");
                                break;

                            case TipoColumnas.Minusculas:
                                strQuey.AppendLine("SELECT  LOWER(REPLACE(REPLACE(co.COLUMN_NAME, ' ','_'),':','')) AS NAME,");
                                break;
                        }

                        strQuey.AppendLine("\tCASE UPPER(co.data_type)");

                        if (lang == Languaje.CSharp)
                        {
                            strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'Int64'");
                            strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'Int64'");
                            strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'Decimal'");
                            strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'Decimal'");
                            strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'Decimal'");
                            strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'Decimal'");
                            strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'bool'");
                            strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'bool'");
                            strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte[]'");
                            strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'DateTime'");
                        }
                        else if (lang == Languaje.VbNet)
                        {
                            strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'Int64'");
                            strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'Int64'");
                            strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'Decimal'");
                            strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'Decimal'");
                            strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'Decimal'");
                            strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'Decimal'");
                            strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'Boolean'");
                            strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'Boolean'");
                            strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte()'");
                            strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'DateTime'");
                            strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'DateTime'");
                        }
                        else if (lang == Languaje.Java)
                        {
                            strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'BigDecimal'");
                            strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'BigDecimal'");
                            strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'BigDecimal'");
                            strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'BigDecimal'");
                            strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte'");
                            strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'Date'");
                        }
                        else if (lang == Languaje.JavaAndroid)
                        {
                            strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'int'");
                            strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte'");
                            strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'long'");
                            strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'long'");
                            //Default
                        }
                        else
                        {
                            strQuey.AppendLine("\t\tWHEN 'BIGINT' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'INT8' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'INTEGER' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'SMALLINT' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'SERIAL' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'NUMERIC' THEN 'BigDecimal'");
                            strQuey.AppendLine("\t\tWHEN 'MONEY' THEN 'BigDecimal'");
                            strQuey.AppendLine("\t\tWHEN 'DOUBLE PRECISION' THEN 'BigDecimal'");
                            strQuey.AppendLine("\t\tWHEN 'REAL' THEN 'BigDecimal'");
                            strQuey.AppendLine("\t\tWHEN 'BOOL' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'BOOLEAN' THEN 'Integer'");
                            strQuey.AppendLine("\t\tWHEN 'BYTEA' THEN 'Byte'");
                            strQuey.AppendLine("\t\tWHEN 'CHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'VARCHAR' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'TEXT' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'CHARACTER VARYING' THEN 'String'");
                            strQuey.AppendLine("\t\tWHEN 'DATE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITHOUT TIME ZONE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIMESTAMP WITH TIME ZONE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITHOUT TIME ZONE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIME WITH TIME ZONE' THEN 'Date'");
                            strQuey.AppendLine("\t\tWHEN 'TIME' THEN 'Date'");
                        }
                        strQuey.AppendLine("\t\tWHEN '' THEN ''");
                        strQuey.AppendLine("\t\tELSE 'Object'");
                        strQuey.AppendLine("\tEND AS tipo, co.table_name AS tabla,");
                        strQuey.AppendLine("\tUPPER(substring(co.COLUMN_NAME ,1,1)) || substring(co.COLUMN_NAME , 2, length(co.COLUMN_NAME ) - 1) AS name_upper,");
                        strQuey.AppendLine("\tCASE co.is_nullable WHEN 'YES' THEN 'Yes' ELSE 'No' END AS permitenull,");
                        //strQuey.AppendLine("	CASE co.is_identity WHEN 'YES' THEN 'Yes' ELSE 'No' END AS esidentidad,")
                        strQuey.AppendLine("\tCASE WHEN (SELECT COUNT(*) FROM information_schema.table_constraints tc ");
                        strQuey.AppendLine("\t\t   JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name");
                        strQuey.AppendLine("\t\t   WHERE co.table_catalog = tc.constraint_catalog");
                        strQuey.AppendLine("\t\t   AND co.table_schema = tc.constraint_schema");
                        strQuey.AppendLine("\t\t   AND co.table_name = tc.table_name");
                        strQuey.AppendLine("\t\t   AND co.column_name = ccu.column_name");
                        strQuey.AppendLine("\t\t   AND tc.constraint_type = 'PRIMARY KEY') > 0 THEN 'Yes'");
                        strQuey.AppendLine("\t\t   ELSE 'No' END AS esidentidad,");
                        strQuey.AppendLine("\tCASE co.is_generated WHEN 'NEVER' THEN 'No' ELSE 'Yes' END AS escalculada,");
                        strQuey.AppendLine("\t'No' AS esrowgui,");
                        strQuey.AppendLine("\tCASE WHEN (SELECT COUNT(*) FROM information_schema.table_constraints tc ");
                        strQuey.AppendLine("\t\t   JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name");
                        strQuey.AppendLine("\t\t   WHERE co.table_catalog = tc.constraint_catalog");
                        strQuey.AppendLine("\t\t   AND co.table_schema = tc.constraint_schema");
                        strQuey.AppendLine("\t\t   AND co.table_name = tc.table_name");
                        strQuey.AppendLine("\t\t   AND co.column_name = ccu.column_name");
                        strQuey.AppendLine("\t\t   AND tc.constraint_type = 'PRIMARY KEY') > 0 THEN 'Yes'");
                        strQuey.AppendLine("\t\t   ELSE 'No' END AS espk,");
                        strQuey.AppendLine("\tCASE WHEN (SELECT COUNT(*) FROM information_schema.table_constraints tc");
                        strQuey.AppendLine("\t\t   JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name");
                        strQuey.AppendLine("\t\t   WHERE co.table_catalog = tc.constraint_catalog");
                        strQuey.AppendLine("\t\t   AND co.table_schema = tc.constraint_schema");
                        strQuey.AppendLine("\t\t   AND co.table_name = tc.table_name");
                        strQuey.AppendLine("\t\t   AND co.column_name = ccu.column_name");
                        strQuey.AppendLine("\t\t   AND tc.constraint_type = 'FOREIGN KEY') > 0 THEN 'Yes'");
                        strQuey.AppendLine("\t\t   ELSE 'No' END AS esfk,");
                        strQuey.AppendLine("    (SELECT ccu.table_name FROM information_schema.table_constraints tc");
                        strQuey.AppendLine("\t JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name");
                        //strQuey.AppendLine("	 JOIN information_schema.constraint_column_usage ccu ON ccu.table_name = tc.table_name and ccu.constraint_catalog = tc.constraint_catalog AND ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name")
                        strQuey.AppendLine("\t WHERE co.table_catalog = tc.constraint_catalog");
                        strQuey.AppendLine("\t AND co.table_schema = tc.constraint_schema");
                        strQuey.AppendLine("\t AND co.table_name = tc.table_name");
                        strQuey.AppendLine("\t AND co.column_name = ccu.column_name");
                        strQuey.AppendLine("\t AND tc.constraint_type = 'FOREIGN KEY' LIMIT 1) AS tablaforanea,");
                        strQuey.AppendLine("\tco.data_type AS tipo_col,");
                        strQuey.AppendLine("\tCOALESCE(co.character_maximum_length::CHARACTER VARYING,'') AS longitud,");
                        strQuey.AppendLine("\tCOALESCE((select pg_catalog.col_description(oid,co.ordinal_position::int) ");
                        strQuey.AppendLine("\t from pg_catalog.pg_class c ");
                        strQuey.AppendLine("\t where c.relname = co.table_name");
                        strQuey.AppendLine("\t order by pg_catalog.col_description(oid,co.ordinal_position::int) asc LIMIT 1),'') as col_desc");
                        strQuey.AppendLine(" FROM information_schema.columns co");
                        strQuey.AppendLine(" WHERE co.table_name = '" + idTable + "' ");
                        strQuey.AppendLine(" AND co.table_schema = '" + schemaName + "' ");
                        strQuey.AppendLine(" ORDER BY co.ordinal_position asc");

                        query = strQuey.ToString();

                        return CargarDataTable(query);
                    }
                    break;

                #endregion "PostgreSql"

                case TipoBD.SQLite:

                    #region "SqLite"

                    strQuey = new StringBuilder();
                    strQuey.AppendLine("pragma table_info(" + idTable + ");");
                    query = strQuey.ToString();

                    //Creamos las columnas
                    DataTable dtPragma = CargarDataTable(query);
                    DataTable dt = new DataTable();
                    DataColumn dc = new DataColumn("name", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("tipo", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("tabla", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("name_upper", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("permitenull", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("esidentidad", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("escalculada", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("esrowgui", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("espk", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("esfk", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("tablaforanea", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("tipo_col", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("longitud", typeof(string));
                    dt.Columns.Add(dc);
                    dc = new DataColumn("col_desc", typeof(string));
                    dt.Columns.Add(dc);

                    foreach (DataRow row in dtPragma.Rows)
                    {
                        DataRow dr = dt.NewRow();
                        dr["name"] = row["name"].ToString();
                        if (row["type"].ToString().ToUpper().Contains("INTEGER"))
                            dr["tipo"] = "int";
                        if (row["type"].ToString().ToUpper().Contains("INT"))
                            dr["tipo"] = "int";
                        if (row["type"].ToString().ToUpper().Contains("NUMERIC"))
                            dr["tipo"] = "int";
                        if (row["type"].ToString().ToUpper().Contains("REAL"))
                            dr["tipo"] = "float";
                        if (row["type"].ToString().ToUpper().Contains("LONG"))
                            dr["tipo"] = "float";
                        if (row["type"].ToString().ToUpper().Contains("TEXT"))
                            dr["tipo"] = "String";
                        if (row["type"].ToString().ToUpper().Contains("NVARCHAR"))
                            dr["tipo"] = "String";
                        if (row["type"].ToString().ToUpper().Contains("VARCHAR"))
                            dr["tipo"] = "String";
                        if (row["type"].ToString().ToUpper().Contains("BLOB"))
                            dr["tipo"] = "Byte[]";
                        if (row["type"].ToString().ToUpper().Contains("DATE"))
                            dr["tipo"] = "long";
                        if (row["type"].ToString().ToUpper().Contains("DATETIME"))
                            dr["tipo"] = "long";
                        if (row["type"].ToString().ToUpper().Contains("BOOLEAN"))
                            dr["tipo"] = "int";
                        if (row["type"].ToString().ToUpper().Contains("BOOL"))
                            dr["tipo"] = "int";
                        dr["tabla"] = idTable;
                        dr["name_upper"] = row["name"].ToString().ToUpper();
                        dr["permitenull"] = (row["notnull"] == "1" ? "No" : "Yes");
                        dr["esidentidad"] = "No";
                        dr["escalculada"] = "No";
                        dr["esrowgui"] = "No";
                        dr["espk"] = (row["pk"] == "1" ? "Yes" : "No");
                        dr["esfk"] = "No";
                        dr["tablaforanea"] = "No";
                        dr["tipo_col"] = row["type"];
                        dr["longitud"] = 0;
                        dr["col_desc"] = "Columna de tipo " + row["type"] + " de la tabla " + idTable;
                        dt.Rows.Add(dr);
                    }

                    #endregion "SqLite"

                    return dt;

                default:
                    return new DataTable();
            }
        }
    }
}