﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using ReAl.Class.Entidades;
using ReAl.Class.Modelo;
using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;

namespace ReAl.Generator.App
{
    public partial class FLogin : Form
    {
        private bool bProcede = false;

        public FLogin()
        {
            InitializeComponent();
        }

        private void FLogin_Load(object sender, EventArgs e)
        {
            var strMiDirectorio = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            var strEscritorio = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            if (!File.Exists(strMiDirectorio + "\\ReAlProjects.db"))
            {
                var strMensaje = "No existe la BD de plantillas en " + strMiDirectorio +
                                     ". ¿Desea Importar una BD con plantillas existentes?";

                if (MessageBox.Show(this, strMensaje, "Error en Base de Datos", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    string fileOrigen = "";
                    string fileDestino = strMiDirectorio + "\\ReAlProjects.db";

                    //Obtenemos el archivo Backup
                    OpenFileDialog fileBrowserDialog1 = new OpenFileDialog();
                    fileBrowserDialog1.InitialDirectory = strEscritorio;
                    fileBrowserDialog1.Filter = "All files (*.*)|*.*|All files (*.*)|*.*";
                    if ((fileBrowserDialog1.ShowDialog() == DialogResult.OK))
                    {
                        try
                        {
                            fileOrigen = fileBrowserDialog1.FileName;

                            //Primero borramos la BD original
                            if (File.Exists(fileDestino))
                            {
                                File.Delete(fileDestino);
                            }

                            //Copiamos el backup
                            File.Copy(fileOrigen, fileDestino);
                            if (File.Exists(fileDestino))
                            {
                                MessageBox.Show(this, "Se ha restaurado la BD de plantillas satisfactoriamente", "Exito!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show(this, "No se ha podido restaurar la copia de la BD de plantillas", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                }
            }

            cargarCmbPlantillas();

            string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            lblVersion.Text = "Version " + version;

            if (cmbDbms.Items.Count > 0)
            {
                cmbDbms.SelectedIndex = 0;
            }

            txtServer.Text = "127.0.0.1";

            //If My.Settings.BDPass <> "" Then txtPass.Text = My.Settings.BDPass

            if (!string.IsNullOrEmpty(txtUser.Text) & !string.IsNullOrEmpty(txtServer.Text) & !string.IsNullOrEmpty(txtPass.Text))
            {
                //CargarBD()
                cmbDatabase.Focus();
            }

            if (!string.IsNullOrEmpty(txtUser.Text) & !string.IsNullOrEmpty(txtServer.Text))
            {
                //CargarBD()
                txtPass.Focus();
            }

            bProcede = true;

            //Cargamos la plantilla seleccionada
            CargarDatosPlantilla();
            TipoBD miBd;
            Enum.TryParse(cmbDbms.SelectedItem.ToString(), out miBd);
            if (miBd == TipoBD.SQLite)
            {
                OpenFileDialog frm = new OpenFileDialog();
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    txtPass.Text = frm.FileName;
                }
                else
                {
                    txtPass.Text = "";
                }
            }
            else if (miBd == TipoBD.Oracle)
            {
                CargarBD();
                cmbDatabase.SelectedValue = txtUser.Text;
            }
            else
            {
                CargarBD();
            }
        }

        private void CargarBD()
        {
            try
            {
                cmbDatabase.DataSource = null;

                Enum.TryParse(cmbDbms.SelectedItem.ToString(), out Principal.DBMS);
                Principal.PasswordBD = txtPass.Text;
                Principal.Servidor = txtServer.Text;
                Principal.UsuarioBD = txtUser.Text;
                Principal.SegIntegrada = chkSegIntegrada.Checked;

                string query = "";

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        if (string.IsNullOrEmpty(Principal.PasswordBD))
                            return;
                        Principal.Catalogo = "master";
                        query = "SELECT name FROM sys.databases ORDER BY name";
                        break;

                    case TipoBD.SQLServer2000:
                        if (string.IsNullOrEmpty(Principal.PasswordBD))
                            return;
                        Principal.Catalogo = "master";
                        query = "SELECT name FROM sysdatabases ORDER BY name";
                        break;

                    case TipoBD.Oracle:
                        Principal.Servicio = txtServicio.Text;
                        Principal.Catalogo = "";
                        query = "SELECT DISTINCT owner AS Schema FROM all_tables ORDER BY owner";
                        break;

                    case TipoBD.MySql:
                        Principal.Puerto = (string.IsNullOrEmpty(txtServicio.Text.Trim()) ? 0 : int.Parse(txtServicio.Text));
                        Principal.Catalogo = "";
                        query = "SELECT DISTINCT schema_name FROM information_schema.schemata ORDER BY schema_name";
                        break;

                    case TipoBD.PostgreSQL:
                        if (string.IsNullOrEmpty(Principal.PasswordBD))
                            return;
                        Principal.Puerto = (string.IsNullOrEmpty(txtServicio.Text.Trim()) ? 0 : int.Parse(txtServicio.Text));
                        Principal.Catalogo = "postgres";
                        query = "SELECT datname FROM pg_database ORDER BY datname";
                        break;
                }

                var dtTemp = CFuncionesBDs.CargarDataTable(query);

                if (dtTemp.Rows.Count > 0)
                {
                    cmbDatabase.DataSource = dtTemp;
                    cmbDatabase.DisplayMember = dtTemp.Columns[0].ColumnName;
                    cmbDatabase.ValueMember = dtTemp.Columns[0].ColumnName;

                    cmbDatabase.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cargarCmbPlantillas()
        {
            try
            {
                rnClaPlantilla rn = new rnClaPlantilla();
                DataTable dt = rn.CargarDataTable();

                if (dt.Columns.Count > 0)
                {
                    dt.Columns.Add(new DataColumn("Descripcion", Type.GetType("System.String"), "descPlantilla + ' (' + ISNULL(servidor,'-') + ')'"));

                    cmbPlantilla.ValueMember = entClaPlantilla.Fields.idPlantilla.ToString();
                    cmbPlantilla.DisplayMember = "Descripcion";
                    cmbPlantilla.DataSource = dt;
                }

                if (cmbPlantilla.Items.Count > 0)
                {
                    cmbPlantilla.SelectedIndex = cmbPlantilla.Items.Count - 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CargarDatosPlantilla()
        {
            txtServer.Text = "";
            txtServicio.Text = "";
            txtUser.Text = "";
            txtPass.Text = "";

            if (bProcede == false)
                return;
            if ((cmbPlantilla.SelectedValue == null))
                return;

            //Obtenemos el Diccionario
            rnClaDetalle rn = new rnClaDetalle();
            DataTable dt = rn.CargarDataTable(entClaDetalle.Fields.idPlantilla, cmbPlantilla.SelectedValue);

            foreach (DataRow dr in dt.Rows)
            {
                Control ctr = this;
                do
                {
                    ctr = this.GetNextControl(ctr, true);
                    if ((ctr != null))
                    {
                        if (dr[entClaDetalle.Fields.propiedad.ToString()].ToString() == ctr.Name)
                        {
                            if (ctr is TextBox)
                            {
                                TextBox txt = (TextBox)ctr;
                                txt.Text = dr[entClaDetalle.Fields.valor.ToString()].ToString();
                            }

                            if (ctr is CheckBox)
                            {
                                CheckBox chk = (CheckBox)ctr;
                                chk.Checked = (dr[entClaDetalle.Fields.valor.ToString()].ToString() == "1");
                            }

                            if (ctr is ComboBox)
                            {
                                ComboBox cmb = (ComboBox)ctr;
                                cmb.SelectedItem = dr[entClaDetalle.Fields.valor.ToString()].ToString();
                            }

                            if (ctr is RadioButton)
                            {
                                RadioButton rdb = (RadioButton)ctr;
                                rdb.Checked = (dr[entClaDetalle.Fields.valor.ToString()].ToString() == "1");
                            }
                        }
                    }
                } while (!(ctr == null));
            }
        }

        private void cmbPlantilla_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!bProcede)
                return;
            CargarDatosPlantilla();

            TipoBD miBd;
            Enum.TryParse(cmbDbms.SelectedItem.ToString(), out miBd);
            if (miBd == TipoBD.SQLite)
            {
                OpenFileDialog frm = new OpenFileDialog();
                if (frm.ShowDialog() == DialogResult.OK)
                    txtPass.Text = frm.FileName;
                else
                    txtPass.Text = "";
            }
            else if (miBd == TipoBD.Oracle)
            {
                CargarBD();
                cmbDatabase.SelectedItem = txtUser.Text;
            }
            else
                CargarBD();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            if (bProcede)
            {
                this.DialogResult = DialogResult.Cancel;
                System.Environment.Exit(0);
            }
            else
            {
                this.Close();
            }
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            Enum.TryParse(cmbDbms.SelectedItem.ToString(), out Principal.DBMS);
            Principal.Catalogo = cmbDatabase.Text;
            Principal.PasswordBD = txtPass.Text;
            Principal.Servidor = txtServer.Text;
            Principal.UsuarioBD = txtUser.Text;
            Principal.SegIntegrada = chkSegIntegrada.Checked;
            Principal.FBFile = txtPass.Text;
            Principal.Plantilla = cmbPlantilla.SelectedValue.ToString();

            try
            {
                string Query = "";
                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                        Query = "select name from sys.objects";
                        break;

                    case TipoBD.SQLServer2000:
                        Query = "select name from sysobjects";
                        break;

                    case TipoBD.Oracle:
                        Principal.Servicio = txtServicio.Text;
                        Query = "select sysdate from dual";
                        break;

                    case TipoBD.PostgreSQL:
                        Principal.Puerto = int.Parse(txtServicio.Text);
                        Query = "SELECT now()::CHARACTER VARYING";
                        break;

                    case TipoBD.MySql:
                        Principal.Puerto = int.Parse(txtServicio.Text);
                        Query = "SELECT now()";
                        break;

                    case TipoBD.FireBird:
                        Principal.PasswordBD = "masterkey";
                        Principal.UsuarioBD = "SYSDBA";
                        Principal.FBFile = txtPass.Text;
                        Query = "select 1";

                        break;

                    case TipoBD.SQLite:
                        Principal.FBFile = txtPass.Text;
                        Query = "select date('now');";
                        break;
                }

                DataTable dtTemp = new DataTable();
                dtTemp = CFuncionesBDs.CargarDataTable(Query);

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FNuevaBd frm = new FNuevaBd();
            try
            {
                if ((frm.ShowDialog() == DialogResult.OK))
                {
                    //Creamos la BD con SQLLite
                    string PathDesktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass";
                    string PathApp = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

                    if (Directory.Exists(PathDesktop + "\\BDs") == false)
                    {
                        Directory.CreateDirectory(PathDesktop + "\\BDs");
                    }
                    string strArguments = PathDesktop + "\\BDs\\" + frm.strNombre + ".db";

                    string newTexto = "echo @echo CREATE TABLE dummy(id INT) ^;^ | sqlite3.exe " + strArguments;
                    CFunciones.CrearArchivo(newTexto.ToString(), "CreateDb" + frm.strNombre + ".bat", PathApp + "\\");

                    //Iniciamos la app
                    Process p = new Process();
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardError = true;
                    p.StartInfo.FileName = "CreateDb" + frm.strNombre + ".bat";
                    //p.StartInfo.Arguments = strArguments
                    p.Start();
                    p.WaitForExit();
                    //Process.Start(strCommand)
                    MessageBox.Show("Se ha creado una BD con SQLite en el Directorio de la Aplicacion en el Escritorio", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Process p2 = new Process();
                    p2.StartInfo.UseShellExecute = false;
                    p2.StartInfo.RedirectStandardError = true;
                    p2.StartInfo.FileName = "Database4.exe";
                    p2.StartInfo.Arguments = strArguments;
                    p2.Start();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (File.Exists("CreateDb" + frm.strNombre + ".bat"))
                    File.Delete("CreateDb" + frm.strNombre + ".bat");
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            TipoBD miBd;
            Enum.TryParse(cmbDbms.SelectedItem.ToString(), out miBd);
            if (miBd == TipoBD.SQLite)
            {
                OpenFileDialog frm = new OpenFileDialog();
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    txtPass.Text = frm.FileName;
                }
                else
                {
                    txtPass.Text = "";
                }
            }
            else
            {
                CargarBD();
            }
        }

        private void txtUser_TextChanged(object sender, EventArgs e)
        {
            cmbDatabase.DataSource = null;
            txtPass.Text = "";
        }

        private void txtPass_TextChanged(object sender, EventArgs e)
        {
            cmbDatabase.DataSource = null;
        }

        private void txtServer_TextChanged(object sender, EventArgs e)
        {
            cmbDatabase.DataSource = null;
            txtPass.Text = "";
        }

        private void txtPass_Leave(object sender, EventArgs e)
        {
            CargarBD();
        }

        private void cmbDbms_SelectedIndexChanged(object sender, EventArgs e)
        {
            TipoBD miBd;
            Enum.TryParse(cmbDbms.SelectedItem.ToString(), out miBd);

            if (miBd == TipoBD.SQLServer)
            {
                txtUser.Text = "sa";
                lblServidor.Visible = true;
                txtServer.Visible = true;
                chkSegIntegrada.Visible = true;
                lblServPuerto.Visible = true;
                txtServicio.Visible = true;
                lblUsuario.Visible = true;
                txtUser.Visible = true;
                txtPass.ReadOnly = false;
                txtPass.PasswordChar = '*';
                lblCatalogo.Visible = true;
                cmbDatabase.Visible = true;

                chkSegIntegrada.Visible = true;
                lblServPuerto.Visible = false;
                txtServicio.Visible = false;
                lblPass.Text = "Servicio";
                txtPass.ReadOnly = false;
            }
            else if (miBd == TipoBD.SQLite)
            {
                lblServidor.Visible = false;
                txtServer.Visible = false;
                chkSegIntegrada.Visible = false;
                lblServPuerto.Visible = false;
                txtServicio.Visible = false;
                lblUsuario.Visible = false;
                txtUser.Visible = false;
                txtPass.ReadOnly = true;
                txtPass.PasswordChar = ' ';
                lblCatalogo.Visible = false;
                cmbDatabase.Visible = false;

                lblPass.Text = "Archivo";
            }
            else
            {
                lblServidor.Visible = true;
                txtServer.Visible = true;
                chkSegIntegrada.Visible = true;
                lblServPuerto.Visible = true;
                txtServicio.Visible = true;
                lblUsuario.Visible = true;
                txtUser.Visible = true;
                txtPass.ReadOnly = false;
                txtPass.PasswordChar = '*';
                lblCatalogo.Visible = true;
                cmbDatabase.Visible = true;

                chkSegIntegrada.Visible = false;
                lblServPuerto.Visible = true;
                txtServicio.Visible = true;
                txtPass.ReadOnly = false;
                if (miBd == TipoBD.Oracle)
                    lblServPuerto.Text = "Servicio:";
                if (miBd == TipoBD.PostgreSQL)
                {
                    txtUser.Text = "postgres";
                    txtServer.Text = "127.0.0.1";
                    lblServPuerto.Text = "Puerto:";
                    txtServicio.Text = "5432";
                }
                if (miBd == TipoBD.MySql)
                {
                    txtUser.Text = "root";
                    txtServer.Text = "127.0.0.1";
                    lblServPuerto.Text = "Puerto:";
                    txtServicio.Text = "3306";
                }
            }
        }

        private void cmbDatabase_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}