﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using ReAlFind_Control;
using System;
using System.Data;

namespace ReAl.Generator.App.AppDataBase
{
    public class cBaseDatosSp
    {
        public string nameTabla;
        public string nameClase;
        public string tipoCol;
        public string nameCol;

        public void CrearSPs(FPrincipal formulario)
        {
            try
            {
                //Elegimos el origen : Tablas o Vistas
                ReAlListView MyReAlListView = new ReAlListView();
                switch (formulario.tabBDObjects.SelectedIndex)
                {
                    case 0:
                        MyReAlListView = formulario.ReAlListView1;
                        break;

                    case 1:
                        MyReAlListView = formulario.ReAlListView2;
                        break;

                    case 2:
                        throw new Exception("No se puede Crear SPs de los SPs");
                }

                string Texto = "";
                DataTable dtColumns = new DataTable();
                string idTabla = null;
                string NameTabla = null;
                string NameTablaAlias = null;
                int longitudCol = 0;
                string strLongitud = null;
                string strNombreSp = null;

                string FC = DateTime.Now.ToString("dd/MM/yyyy");
                string Creador = (string.IsNullOrEmpty(formulario.txtInfAutor.Text) ? "Automatico     " : formulario.txtInfAutor.Text);
                Creador = Creador.PadRight(15);
                string cabecera = null;
                string cabeceraPlantilla = "/***********************************************************************************************************\r\n\tNOMBRE:       %NOMBRE%\r\n\tDESCRIPCION:\r\n\t\t%OPERACION% un registro de la Tabla %TABLA%\r\n\r\n\tPARAMETROS DE ENTRADA:\r\n\t\t[DEFINIR]\r\n\r\n\tPARAMETROS DE SALIDA:\r\n\t\tNinguno\r\n\r\n\tREVISIONES:\r\n\t\tVer        FECHA       Autor            Descripcion \r\n\t\t---------  ----------  ---------------  ------------------------------------\r\n\t\t1.0        " + FC + "  " + Creador + "  Creacion \r\n\r\n*************************************************************************************************************/\r\n\r\n\r\n";

                for (int i = 0; i <= MyReAlListView.Items.Count - 1; i++)
                {
                    if (MyReAlListView.Items[i].Checked == true)
                    {
                        idTabla = MyReAlListView.Items[i].SubItems[1].Text;
                        NameTabla = MyReAlListView.Items[i].SubItems[0].Text;
                        string nameSchema = MyReAlListView.Items[i].SubItems[2].Text;
                        NameTablaAlias = NameTabla;
                        nameClase = NameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                    NameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                    NameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                    NameTabla.Replace("_", "").Substring(4).ToLower();

                        try
                        {
                            DataTable dtPrefijo = new DataTable();
                            switch (Principal.DBMS)
                            {
                                case TipoBD.SQLServer:
                                    dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + NameTabla + "')");
                                    break;

                                case TipoBD.SQLServer2000:
                                    dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT aliassta FROM Segtablas WHERE UPPER(tablasta) = UPPER('" + NameTabla + "')");
                                    break;

                                case TipoBD.Oracle:
                                    dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT ALIASSTA FROM SEGTABLAS WHERE UPPER(TABLASTA) = '" + NameTabla.ToUpper() + "'");
                                    break;

                                case TipoBD.PostgreSQL:
                                    dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.aliassta FROM segtablas s WHERE UPPER(s.tablasta) = UPPER('" + NameTabla.ToUpper() + "')");
                                    if (dtPrefijo.Rows.Count == 0)
                                    {
                                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.alias FROM seg_tablas s WHERE UPPER(s.nombretabla) = UPPER('" + NameTabla + "')");
                                    }
                                    break;
                            }

                            if (dtPrefijo.Rows.Count > 0)
                            {
                                NameTablaAlias = dtPrefijo.Rows[0][0].ToString();
                                NameTablaAlias = NameTablaAlias.Substring(0, 1).ToUpper() + NameTablaAlias.Substring(1, NameTablaAlias.Length - 1);
                            }
                            else
                            {
                                try
                                {
                                    switch (Principal.DBMS)
                                    {
                                        case TipoBD.SQLServer:
                                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE UPPER(TablaSegTablas) = UPPER('" + NameTabla + "')");
                                            break;

                                        case TipoBD.SQLServer2000:
                                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE UPPER(TablaSegTablas) = UPPER('" + NameTabla + "')");
                                            break;

                                        case TipoBD.Oracle:
                                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM SEGTABLAS WHERE UPPER(TablaSegTablas) = '" + NameTabla.ToUpper() + "'");
                                            break;

                                        case TipoBD.PostgreSQL:
                                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.AliasSegTablas FROM segtablas s WHERE UPPER(s.TablaSegTablas) = UPPER('" + NameTabla.ToUpper() + "')");
                                            if (dtPrefijo.Rows.Count == 0)
                                            {
                                                dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.AliasSegTablas FROM seg_tablas s WHERE UPPER(s.TablaSegTablas) = UPPER('" + NameTabla + "')");
                                            }
                                            break;
                                    }

                                    if (dtPrefijo.Rows.Count > 0)
                                    {
                                        NameTablaAlias = dtPrefijo.Rows[0][0].ToString();
                                        NameTablaAlias =
                                            NameTablaAlias.Substring(0, 1).ToUpper() +
                                            NameTablaAlias.Substring(1, NameTablaAlias.Length - 1);
                                    }
                                    else
                                    {
                                        NameTablaAlias = NameTabla;
                                    }

                                    //Ahora lo volvemos en Letra Capital
                                    if (Principal.DBMS == TipoBD.SQLServer)
                                    {
                                        NameTablaAlias = NameTablaAlias[0].ToString().ToUpper() + NameTablaAlias.Substring(1, NameTablaAlias.Length - 1).ToLower();
                                    }
                                }
                                catch (Exception ex2)
                                {
                                    NameTablaAlias = NameTabla;
                                }
                            }

                            //Ahora lo volvemos en Letra Capital
                            if (Principal.DBMS == TipoBD.SQLServer)
                            {
                                NameTablaAlias = NameTablaAlias[0].ToString().ToUpper() + NameTablaAlias.Substring(1, NameTablaAlias.Length - 1).ToLower();
                            }
                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                DataTable dtPrefijo = new DataTable();
                                switch (Principal.DBMS)
                                {
                                    case TipoBD.SQLServer:
                                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE UPPER(TablaSegTablas) = UPPER('" + NameTabla + "')");
                                        break;

                                    case TipoBD.SQLServer2000:
                                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM Segtablas WHERE UPPER(TablaSegTablas) = UPPER('" + NameTabla + "')");
                                        break;

                                    case TipoBD.Oracle:
                                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT AliasSegTablas FROM SEGTABLAS WHERE UPPER(TablaSegTablas) = '" + NameTabla.ToUpper() + "'");
                                        break;

                                    case TipoBD.PostgreSQL:
                                        dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.AliasSegTablas FROM segtablas s WHERE UPPER(s.TablaSegTablas) = UPPER('" + NameTabla.ToUpper() + "')");
                                        if (dtPrefijo.Rows.Count == 0)
                                        {
                                            dtPrefijo = CFuncionesBDs.CargarDataTable("SELECT s.AliasSegTablas FROM seg_tablas s WHERE UPPER(s.TablaSegTablas) = UPPER('" + NameTabla + "')");
                                        }
                                        break;
                                }

                                if (dtPrefijo.Rows.Count > 0)
                                {
                                    NameTablaAlias = dtPrefijo.Rows[0][0].ToString();
                                    NameTablaAlias = NameTablaAlias.Substring(0, 1).ToUpper() + NameTablaAlias.Substring(1, NameTablaAlias.Length - 1);
                                }
                                else
                                {
                                    NameTablaAlias = NameTabla;
                                }

                                //Ahora lo volvemos en Letra Capital
                                if (Principal.DBMS == TipoBD.SQLServer)
                                {
                                    NameTablaAlias = NameTablaAlias[0].ToString().ToUpper() + NameTablaAlias.Substring(1, NameTablaAlias.Length - 1).ToLower();
                                }
                            }
                            catch (Exception ex2)
                            {
                                NameTablaAlias = NameTabla;
                            }
                        }

                        switch (formulario.cmbTipoColumnas.SelectedIndex)
                        {
                            case 0:
                                dtColumns = CTablasColumnas.ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.PrimeraMayuscula);
                                break;

                            case 1:
                                dtColumns = CTablasColumnas.ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.Minusculas);
                                break;

                            case 2:
                                dtColumns = CTablasColumnas.ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.Mayusculas);
                                break;
                        }
                        bool esPrimerElemento = true;

                        string strApiTrans = "";
                        string strApiEstado = "";
                        string strUsuCre = "";
                        string strFecCre = "";
                        string strUsuMod = "";
                        string strFecMod = "";
                        for (int iCol = 0; iCol <= dtColumns.Rows.Count - 1; iCol++)
                        {
                            nameCol = dtColumns.Rows[iCol][0].ToString();
                            if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("cre"))
                                strUsuCre = nameCol;
                            if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("usu") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("mod"))
                                strUsuMod = nameCol;
                            if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("cre"))
                                strFecCre = nameCol;
                            if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("fec") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("mod"))
                                strFecMod = nameCol;
                            if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("estado"))
                                strApiEstado = nameCol;
                            if (nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("api") && nameCol.ToLower().Replace(NameTabla.ToLower(), "").Contains("transaccion"))
                                strApiTrans = nameCol;
                        }

                        if (string.IsNullOrEmpty(strApiTrans))
                            strApiTrans = "----";
                        if (string.IsNullOrEmpty(strApiEstado))
                            strApiEstado = "----";
                        if (string.IsNullOrEmpty(strUsuCre))
                            strUsuCre = "----";
                        if (string.IsNullOrEmpty(strFecCre))
                            strFecCre = "----";
                        if (string.IsNullOrEmpty(strUsuMod))
                            strUsuMod = "----";
                        if (string.IsNullOrEmpty(strFecMod))
                            strFecMod = "----";

                        if (formulario.chkSPdel.Checked)
                        {
                            //DELETE
                            if (dtColumns.Rows.Count > 0)
                            {
                                cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Del");
                                cabecera = cabecera.Replace("%OPERACION%", "Elimina");
                                cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                switch (Principal.DBMS)
                                {
                                    case TipoBD.SQLServer:

                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "IF EXISTS (SELECT * FROM [dbo].[sysobjects] \r\n";
                                            Texto = Texto + "\tWHERE ID = object_id(N'[dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Del]') AND \r\n";
                                            Texto = Texto + "\tOBJECTPROPERTY(id, N'IsProcedure') = 1) \r\n";
                                            Texto = Texto + "DROP PROCEDURE [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Del]\r\n";
                                            Texto = Texto + "GO\r\n\r\n\r\n";
                                        }

                                        Texto = Texto + cabecera;

                                        if (formulario.rdbSpCreate.Checked | formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Del] \r\n";
                                        }

                                        if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "ALTER PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Del] AS \r\n\r\n";
                                        }

                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();
                                            longitudCol = int.Parse(dtColumns.Rows[iFil]["LONGITUD"].ToString());

                                            switch (tipoCol.ToUpper())
                                            {
                                                case "INT":
                                                    strLongitud = "";
                                                    break;

                                                case "BIGINT":
                                                    strLongitud = "";
                                                    break;

                                                case "IMAGE":
                                                    strLongitud = "";
                                                    break;

                                                case "BIT":
                                                    strLongitud = "";
                                                    break;

                                                case "DATE":
                                                    strLongitud = "";
                                                    break;

                                                case "DATETIME":
                                                    strLongitud = "";
                                                    break;

                                                case "SMALLINT":
                                                    strLongitud = "";
                                                    break;

                                                case "SMALLDATETIME":
                                                    strLongitud = "";
                                                    break;

                                                case "MONEY":
                                                    strLongitud = "";
                                                    break;

                                                default:
                                                    strLongitud = "(" + longitudCol + ")";
                                                    break;
                                            }

                                            if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strUsuMod.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()) & !nameCol.ToUpper().Contains(strApiEstado.ToUpper()) & !nameCol.ToUpper().Contains(strApiTrans.ToUpper()))
                                            {
                                                if (dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes")
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ",\r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\nAS \r\n";
                                        Texto = Texto + "\tDELETE " + NameTabla + "\r\n";
                                        Texto = Texto + "\tWHERE \r\n";

                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strUsuMod.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()) & !nameCol.ToUpper().Contains(strApiEstado.ToUpper()) & !nameCol.ToUpper().Contains(strApiTrans.ToUpper()))
                                            {
                                                if (dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes")
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + " AND \r\n\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\nGO";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        break;

                                    case TipoBD.SQLServer2000:

                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "IF EXISTS (SELECT * FROM [dbo].[sysobjects] \r\n";
                                            Texto = Texto + "\tWHERE ID = object_id(N'[dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Del]') AND \r\n";
                                            Texto = Texto + "\tOBJECTPROPERTY(id, N'IsProcedure') = 1) \r\n";
                                            Texto = Texto + "DROP PROCEDURE [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Del]\r\n";
                                            Texto = Texto + "GO\r\n\r\n\r\n";
                                        }

                                        Texto = Texto + cabecera;

                                        if (formulario.rdbSpCreate.Checked | formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Del] \r\n";
                                        }

                                        if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "ALTER PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Del] AS \r\n\r\n";
                                        }

                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();
                                            longitudCol = int.Parse(dtColumns.Rows[iFil]["LONGITUD"].ToString());

                                            switch (tipoCol.ToUpper())
                                            {
                                                case "INT":
                                                    strLongitud = "";
                                                    break;

                                                case "BIGINT":
                                                    strLongitud = "";
                                                    break;

                                                case "IMAGE":
                                                    strLongitud = "";
                                                    break;

                                                case "BIT":
                                                    strLongitud = "";
                                                    break;

                                                case "DATE":
                                                    strLongitud = "";
                                                    break;

                                                case "DATETIME":
                                                    strLongitud = "";
                                                    break;

                                                case "SMALLINT":
                                                    strLongitud = "";
                                                    break;

                                                case "SMALLDATETIME":
                                                    strLongitud = "";
                                                    break;

                                                default:
                                                    strLongitud = "(" + longitudCol + ")";
                                                    break;
                                            }

                                            if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strUsuMod.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()) & !nameCol.ToUpper().Contains(strApiEstado.ToUpper()) & !nameCol.ToUpper().Contains(strApiTrans.ToUpper()))
                                            {
                                                if (dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes")
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ",\r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\nAS \r\n";
                                        Texto = Texto + "\tDELETE " + NameTabla + "\r\n";
                                        Texto = Texto + "\tWHERE \r\n";

                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strUsuMod.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()) & !nameCol.ToUpper().Contains(strApiEstado.ToUpper()) & !nameCol.ToUpper().Contains(strApiTrans.ToUpper()))
                                            {
                                                if (dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes")
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + " AND \r\n\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\nGO";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        break;

                                    case TipoBD.Oracle:
                                        Texto = Texto + "CREATE OR REPLACE PROCEDURE " + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "DEL\r\n";
                                        Texto = Texto + "(\r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strUsuMod.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()) & !nameCol.ToUpper().Contains(strApiEstado.ToUpper()) & !nameCol.ToUpper().Contains(strApiTrans.ToUpper()))
                                            {
                                                if (dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes")
                                                {
                                                    if (formulario.chkSPParametrosPa.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\r\n\tpa" + nameCol + " IN " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ",\r\n\tpa" + nameCol + " IN " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\r\n\t" + nameCol + " IN " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ",\r\n\t" + nameCol + " IN " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n)\r\n";

                                        Texto = Texto + cabecera;
                                        Texto = Texto + "\r\nAS \r\n";
                                        Texto = Texto + "\r\nBEGIN\r\n";
                                        Texto = Texto + "\tDELETE " + NameTabla + "\r\n";
                                        Texto = Texto + "\tWHERE \r\n";

                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strUsuMod.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()) & !nameCol.ToUpper().Contains(strApiEstado.ToUpper()) & !nameCol.ToUpper().Contains(strApiTrans.ToUpper()))
                                            {
                                                if (dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes")
                                                {
                                                    if (formulario.chkSPParametrosPa.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t" + nameCol + " = " + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "DEL.pa" + nameCol.Replace(" ", "_");
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + " AND \r\n\t\t" + nameCol + " = " + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "DEL.pa" + nameCol.Replace(" ", "_");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t" + nameCol + " = " + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "DEL." + nameCol.Replace(" ", "_");
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + " AND \r\n\t\t" + nameCol + " = " + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "DEL." + nameCol.Replace(" ", "_");
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + ";\r\nEND;\r\n/";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        break;

                                    case TipoBD.PostgreSQL:

                                        strNombreSp = (formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Del").ToLower();
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE OR REPLACE FUNCTION " + strNombreSp + " (\r\n";
                                        }
                                        else if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "REPLACE FUNCTION " + strNombreSp + " (\r\n";
                                        }
                                        else
                                        {
                                            Texto = Texto + "CREATE FUNCTION " + strNombreSp + " (\r\n";
                                        }

                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();
                                            strLongitud = "";

                                            if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strUsuMod.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()) & !nameCol.ToUpper().Contains(strApiEstado.ToUpper()) & !nameCol.ToUpper().Contains(strApiTrans.ToUpper()))
                                            {
                                                if (dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes")
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\r\n\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ",\r\n\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + ")\r\n  RETURNS void AS\r\n";
                                        Texto = Texto + "$$\r\n";
                                        Texto = Texto + "BEGIN\r\n";
                                        Texto = Texto + cabecera + "\r\n";
                                        Texto = Texto + "\tDELETE FROM " + NameTabla + "\r\n";
                                        Texto = Texto + "\tWHERE \r\n";

                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strUsuMod.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()) & !nameCol.ToUpper().Contains(strApiEstado.ToUpper()) & !nameCol.ToUpper().Contains(strApiTrans.ToUpper()))
                                            {
                                                if (dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes")
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t" + NameTabla + "." + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + " AND \r\n\t\t" + NameTabla + "." + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + ";\r\n\treturn;";
                                        Texto = Texto + "\r\nend; $$";

                                        Texto = Texto + "\r\n  LANGUAGE plpgsql VOLATILE COST 100;\r\n\r\n";
                                        Texto = Texto + "\r\n\r\n\r\n";
                                        break;
                                }
                            }
                        }

                        if (formulario.chkSPins.Checked)
                        {
                            //INSERT
                            if (dtColumns.Rows.Count > 0)
                            {
                                cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Ins");
                                cabecera = cabecera.Replace("%OPERACION%", "Inserta");
                                cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                switch (Principal.DBMS)
                                {
                                    case TipoBD.SQLServer:
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "IF EXISTS (SELECT * FROM [dbo].[sysobjects] \r\n";
                                            Texto = Texto + "\tWHERE ID = object_id(N'[dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Ins]') AND \r\n";
                                            Texto = Texto + "\tOBJECTPROPERTY(id, N'IsProcedure') = 1) \r\n";
                                            Texto = Texto + "DROP PROCEDURE [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Ins]\r\n";
                                            Texto = Texto + "GO\r\n\r\n\r\n";
                                        }

                                        Texto = Texto + cabecera;

                                        if (formulario.rdbSpCreate.Checked | formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Ins] \r\n";
                                        }

                                        if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "ALTER PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Ins] \r\n";
                                        }

                                        //PARAMETROS
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();
                                            longitudCol = int.Parse(dtColumns.Rows[iFil]["LONGITUD"].ToString());

                                            switch (tipoCol)
                                            {
                                                case "INT":
                                                    strLongitud = "";
                                                    break;

                                                case "BIGINT":
                                                    strLongitud = "";
                                                    break;

                                                case "IMAGE":
                                                    strLongitud = "";
                                                    break;

                                                case "BIT":
                                                    strLongitud = "";
                                                    break;

                                                case "DATE":
                                                    strLongitud = "";
                                                    break;

                                                case "DATETIME":
                                                    strLongitud = "";
                                                    break;

                                                case "SMALLINT":
                                                    strLongitud = "";
                                                    break;

                                                case "SMALLDATETIME":
                                                    strLongitud = "";
                                                    break;

                                                default:
                                                    strLongitud = "(" + longitudCol + ")";
                                                    break;
                                            }

                                            if (dtColumns.Rows[iFil]["EsIdentidad"].ToString() == "No")
                                            {
                                                if (nameCol == strApiEstado)
                                                {
                                                }
                                                else if (nameCol == strApiTrans)
                                                {
                                                }
                                                else if (nameCol == strUsuMod)
                                                {
                                                }
                                                else if (nameCol == strFecMod)
                                                {
                                                }
                                                else if (nameCol == strUsuCre)
                                                {
                                                    if (formulario.chkSPInsUsuCre.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t@" + nameCol + " " + tipoCol + strLongitud;
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strFecCre)
                                                {
                                                    if (formulario.chkSPInsFecCre.Checked & !formulario.chkSPInsFecCreGetDate.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t@" + nameCol + " " + tipoCol + strLongitud;
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t@" + nameCol + " " + tipoCol + strLongitud;
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (formulario.chkSPInsIdentity.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t@" + nameCol + " " + tipoCol + strLongitud + " OUTPUT ";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t@" + nameCol + " " + tipoCol + strLongitud + " OUTPUT ";
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\nAS \r\n";
                                        Texto = Texto + "\tINSERT INTO [" + NameTabla + "] (\r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (dtColumns.Rows[iFil]["EsIdentidad"].ToString() == "No")
                                            {
                                                if (nameCol == strApiEstado)
                                                {
                                                    if (formulario.chkSPInsApiEstado.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t[" + nameCol + "]";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t[" + nameCol + "]";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strApiTrans)
                                                {
                                                    if (formulario.chkSPInsApiEstado.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t[" + nameCol + "]";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t[" + nameCol + "]";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strUsuMod)
                                                {
                                                }
                                                else if (nameCol == strFecMod)
                                                {
                                                }
                                                else if (nameCol == strUsuCre)
                                                {
                                                    if (formulario.chkSPInsUsuCre.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t[" + nameCol + "]";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t[" + nameCol + "]";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strFecCre)
                                                {
                                                    if (formulario.chkSPInsFecCre.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t[" + nameCol + "]";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t[" + nameCol + "]";
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t[" + nameCol + "]";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t[" + nameCol + "]";
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n\t) VALUES (\r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (dtColumns.Rows[iFil]["EsIdentidad"].ToString() == "No")
                                            {
                                                if (nameCol == strApiEstado)
                                                {
                                                    if (formulario.chkSPInsApiEstado.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t'ELABORADO'";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t'ELABORADO'";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strApiTrans)
                                                {
                                                    if (formulario.chkSPInsApiEstado.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t'CREAR'";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t'CREAR'";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strUsuMod)
                                                {
                                                }
                                                else if (nameCol == strFecMod)
                                                {
                                                }
                                                else if (nameCol == strUsuCre)
                                                {
                                                    if (formulario.chkSPInsUsuCre.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t@" + dtColumns.Rows[0][0].ToString().Replace(" ", "_");
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t@" + dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strFecCre)
                                                {
                                                    if (formulario.chkSPInsFecCre.Checked)
                                                    {
                                                        if (formulario.chkSPInsFecCreGetDate.Checked)
                                                        {
                                                            if (esPrimerElemento)
                                                            {
                                                                Texto = Texto + "\t\tGETDATE()";
                                                                esPrimerElemento = false;
                                                            }
                                                            else
                                                            {
                                                                Texto = Texto + ", \r\n\t\tGETDATE()";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (esPrimerElemento)
                                                            {
                                                                Texto = Texto + "\t\t@" + dtColumns.Rows[0][0].ToString().Replace(" ", "_");
                                                                esPrimerElemento = false;
                                                            }
                                                            else
                                                            {
                                                                Texto = Texto + ", \r\n\t\t@" + dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t@" + dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t@" + dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                                    }
                                                }
                                            }
                                        }

                                        if (formulario.chkSPInsIdentity.Checked)
                                        {
                                            nameCol = dtColumns.Rows[0][0].ToString();
                                            tipoCol = dtColumns.Rows[0]["TIPO_COL"].ToString();

                                            if (dtColumns.Rows[0]["EsIdentidad"].ToString() == "Yes")
                                            {
                                                Texto = Texto + "\r\n\tSELECT @" + nameCol + " = SCOPE_IDENTITY()\r\n";
                                            }
                                        }

                                        Texto = Texto + "\r\n\t) \r\nGO";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        break;

                                    case TipoBD.SQLServer2000:
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "IF EXISTS (SELECT * FROM [dbo].[sysobjects] \r\n";
                                            Texto = Texto + "\tWHERE ID = object_id(N'[dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Ins]') AND \r\n";
                                            Texto = Texto + "\tOBJECTPROPERTY(id, N'IsProcedure') = 1) \r\n";
                                            Texto = Texto + "DROP PROCEDURE [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Ins]\r\n";
                                            Texto = Texto + "GO\r\n\r\n\r\n";
                                        }

                                        Texto = Texto + cabecera;

                                        if (formulario.rdbSpCreate.Checked | formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Ins] \r\n";
                                        }

                                        if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "ALTER PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Ins] \r\n";
                                        }

                                        //PARAMETROS
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();
                                            longitudCol = int.Parse(dtColumns.Rows[iFil]["LONGITUD"].ToString());

                                            switch (tipoCol)
                                            {
                                                case "INT":
                                                    strLongitud = "";
                                                    break;

                                                case "BIGINT":
                                                    strLongitud = "";
                                                    break;

                                                case "IMAGE":
                                                    strLongitud = "";
                                                    break;

                                                case "BIT":
                                                    strLongitud = "";
                                                    break;

                                                case "DATE":
                                                    strLongitud = "";
                                                    break;

                                                case "DATETIME":
                                                    strLongitud = "";
                                                    break;

                                                case "SMALLINT":
                                                    strLongitud = "";
                                                    break;

                                                case "SMALLDATETIME":
                                                    strLongitud = "";
                                                    break;

                                                default:
                                                    strLongitud = "(" + longitudCol + ")";
                                                    break;
                                            }

                                            if (dtColumns.Rows[iFil]["EsIdentidad"].ToString() == "No")
                                            {
                                                if (nameCol == strApiEstado)
                                                {
                                                }
                                                else if (nameCol == strApiTrans)
                                                {
                                                }
                                                else if (nameCol == strUsuMod)
                                                {
                                                }
                                                else if (nameCol == strFecMod)
                                                {
                                                }
                                                else if (nameCol == strUsuCre)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t@" + nameCol + " " + tipoCol + strLongitud;
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                    }
                                                }
                                                else if (nameCol == strFecCre)
                                                {
                                                    if (formulario.chkSPInsFecCre.Checked & !formulario.chkSPInsFecCreGetDate.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t@" + nameCol + " " + tipoCol + strLongitud;
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t@" + nameCol + " " + tipoCol + strLongitud;
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (formulario.chkSPInsIdentity.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t@" + nameCol + " " + tipoCol + strLongitud + " OUTPUT ";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t@" + nameCol + " " + tipoCol + strLongitud + " OUTPUT ";
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\nAS \r\n";
                                        Texto = Texto + "\tINSERT INTO [" + NameTabla + "] (\r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (dtColumns.Rows[iFil]["EsIdentidad"].ToString() == "No")
                                            {
                                                if (nameCol == strApiEstado)
                                                {
                                                    if (formulario.chkSPInsApiEstado.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t[" + nameCol + "]";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t[" + nameCol + "]";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strApiTrans)
                                                {
                                                    if (formulario.chkSPInsApiEstado.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t[" + nameCol + "]";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t[" + nameCol + "]";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strUsuMod)
                                                {
                                                }
                                                else if (nameCol == strFecMod)
                                                {
                                                }
                                                else if (nameCol == strUsuCre)
                                                {
                                                    if (formulario.chkSPInsUsuCre.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t[" + nameCol + "]";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t[" + nameCol + "]";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strFecCre)
                                                {
                                                    if (formulario.chkSPInsFecCre.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t[" + nameCol + "]";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t[" + nameCol + "]";
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t[" + nameCol + "]";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t[" + nameCol + "]";
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n\t) VALUES (\r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (dtColumns.Rows[iFil]["EsIdentidad"].ToString() == "No")
                                            {
                                                if (nameCol == strApiEstado)
                                                {
                                                    if (formulario.chkSPInsApiEstado.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t'ELABORADO'";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t'ELABORADO'";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strApiTrans)
                                                {
                                                    if (formulario.chkSPInsApiEstado.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t'CREAR'";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t'CREAR'";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strUsuMod)
                                                {
                                                }
                                                else if (nameCol == strFecMod)
                                                {
                                                }
                                                else if (nameCol == strUsuCre)
                                                {
                                                    if (formulario.chkSPInsUsuCre.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t@" + dtColumns.Rows[0][0].ToString().Replace(" ", "_");
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t@" + dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strFecCre)
                                                {
                                                    if (formulario.chkSPInsFecCre.Checked)
                                                    {
                                                        if (formulario.chkSPInsFecCreGetDate.Checked)
                                                        {
                                                            if (esPrimerElemento)
                                                            {
                                                                Texto = Texto + "\t\tGETDATE()";
                                                                esPrimerElemento = false;
                                                            }
                                                            else
                                                            {
                                                                Texto = Texto + ", \r\n\t\tGETDATE()";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (esPrimerElemento)
                                                            {
                                                                Texto = Texto + "\t\t@" + dtColumns.Rows[0][0].ToString().Replace(" ", "_");
                                                                esPrimerElemento = false;
                                                            }
                                                            else
                                                            {
                                                                Texto = Texto + ", \r\n\t\t@" + dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t@" + dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t@" + dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                                    }
                                                }
                                            }
                                        }

                                        if (formulario.chkSPInsIdentity.Checked)
                                        {
                                            nameCol = dtColumns.Rows[0][0].ToString();
                                            tipoCol = dtColumns.Rows[0]["TIPO_COL"].ToString();

                                            if (dtColumns.Rows[0]["EsIdentidad"].ToString() == "Yes")
                                            {
                                                Texto = Texto + "\r\n\tSELECT @" + nameCol + " = SCOPE_IDENTITY()\r\n";
                                            }
                                        }

                                        Texto = Texto + "\r\n\t) \r\nGO";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        break;

                                    case TipoBD.Oracle:
                                        Texto = Texto + "CREATE OR REPLACE PROCEDURE " + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "INS \r\n";
                                        Texto = Texto + "(\r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();

                                            if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strUsuMod.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()) & !nameCol.ToUpper().Contains(strApiEstado.ToUpper()) & !nameCol.ToUpper().Contains(strApiTrans.ToUpper()))
                                            {
                                                if (formulario.chkSPParametrosPa.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\tpa" + nameCol + " IN " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\tpa" + nameCol + " IN " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                    }
                                                }
                                                else
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t" + nameCol + " IN " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t" + nameCol + " IN " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n)\r\n";
                                        Texto = Texto + "\r\n" + cabecera;
                                        Texto = Texto + "AS\r\n";
                                        Texto = Texto + " BEGIN\r\n";
                                        Texto = Texto + "\tINSERT INTO " + NameTabla + " (\r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strUsuMod.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()) & !nameCol.ToUpper().Contains(strApiEstado.ToUpper()) & !nameCol.ToUpper().Contains(strApiTrans.ToUpper()))
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    Texto = Texto + "\t\t" + nameCol;
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    Texto = Texto + ", \r\n\t\t" + nameCol;
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n\t) VALUES (\r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[0]["TIPO_COL"].ToString();

                                            if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strUsuMod.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()) & !nameCol.ToUpper().Contains(strApiEstado.ToUpper()) & !nameCol.ToUpper().Contains(strApiTrans.ToUpper()))
                                            {
                                                if (formulario.chkSPParametrosPa.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "INS.pa" + dtColumns.Rows[0][0].ToString().Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "INS.pa" + dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                                    }
                                                }
                                                else
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "INS." + dtColumns.Rows[0][0].ToString().Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "INS." + dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n\t); \r\n END;\r\n/";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        break;

                                    case TipoBD.PostgreSQL:
                                        string strTipoPriCol = "";
                                        strNombreSp = (formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "ins").ToLower();

                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE OR REPLACE FUNCTION " + strNombreSp + " (\r\n";
                                        }
                                        else if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "REPLACE FUNCTION " + strNombreSp + " (\r\n";
                                        }
                                        else
                                        {
                                            Texto = Texto + "CREATE FUNCTION " + strNombreSp + " (\r\n";
                                        }

                                        //PARAMETROS
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();
                                            strLongitud = "";

                                            if (nameCol == strApiEstado)
                                            {
                                            }
                                            else if (nameCol == strApiTrans)
                                            {
                                            }
                                            else if (nameCol == strUsuMod)
                                            {
                                            }
                                            else if (nameCol == strFecMod)
                                            {
                                            }
                                            else if (nameCol == strUsuCre)
                                            {
                                                if (formulario.chkSPInsUsuCre.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                    }
                                                }
                                            }
                                            else if (nameCol == strFecCre)
                                            {
                                                if (formulario.chkSPInsFecCre.Checked & !formulario.chkSPInsFecCreGetDate.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    if (formulario.chkSPInsIdentity.Checked)
                                                    {
                                                        strTipoPriCol = NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE";
                                                        Texto = Texto + "\tINOUT " + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE ";
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + "\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                    }
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    Texto = Texto + ", \r\n\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                }
                                            }
                                        }

                                        if (formulario.chkSPInsIdentity.Checked)
                                        {
                                            Texto = Texto + "\r\n) \r\nRETURNS " + strTipoPriCol + " AS \r\n$$\r\nBEGIN\r\n";

                                            Texto = Texto + cabecera + "\r\n";
                                            nameCol = dtColumns.Rows[0][0].ToString().Replace(" ", "_");
                                            Texto = Texto + "\t" + strNombreSp + "." + nameCol + " := (SELECT coalesce(MAX(aux." + nameCol + "),0) + 1 FROM " + NameTabla + " aux);\r\n";
                                            Texto = Texto + "\t\r\n";
                                        }
                                        else
                                        {
                                            Texto = Texto + "\r\n) \r\nRETURNS void AS \r\n$$\r\nBEGIN\r\n";

                                            Texto = Texto + cabecera + "\r\n";
                                        }

                                        Texto = Texto + "\tINSERT INTO " + NameTabla + " (\r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (dtColumns.Rows[iFil]["EsIdentidad"].ToString() == "No")
                                            {
                                                if (nameCol == strApiEstado)
                                                {
                                                    if (formulario.chkSPInsApiEstado.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t" + nameCol + "";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t" + nameCol + "";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strApiTrans)
                                                {
                                                    if (formulario.chkSPInsApiEstado.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t" + nameCol + "";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t" + nameCol + "";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strUsuMod)
                                                {
                                                }
                                                else if (nameCol == strFecMod)
                                                {
                                                }
                                                else if (nameCol == strUsuCre)
                                                {
                                                    if (formulario.chkSPInsUsuCre.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t" + nameCol + "";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t" + nameCol + "";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strFecCre)
                                                {
                                                    if (formulario.chkSPInsFecCre.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t" + nameCol + "";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t" + nameCol + "";
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t" + nameCol + "";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t" + nameCol + "";
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n\t) VALUES (\r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (dtColumns.Rows[iFil]["EsIdentidad"].ToString() == "No")
                                            {
                                                if (nameCol == strApiEstado)
                                                {
                                                    if (formulario.chkSPInsApiEstado.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t'ELABORADO'";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t'ELABORADO'";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strApiTrans)
                                                {
                                                    if (formulario.chkSPInsApiEstado.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t'CREAR'";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t'CREAR'";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strUsuMod)
                                                {
                                                }
                                                else if (nameCol == strFecMod)
                                                {
                                                }
                                                else if (nameCol == strUsuCre)
                                                {
                                                    if (formulario.chkSPInsUsuCre.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t" + strNombreSp + "." + nameCol + "";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t" + strNombreSp + "." + nameCol + "";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strFecCre)
                                                {
                                                    if (formulario.chkSPInsFecCre.Checked)
                                                    {
                                                        if (formulario.chkSPInsFecCreGetDate.Checked)
                                                        {
                                                            if (esPrimerElemento)
                                                            {
                                                                Texto = Texto + "\t\tnow()";
                                                                esPrimerElemento = false;
                                                            }
                                                            else
                                                            {
                                                                Texto = Texto + ", \r\n\t\tnow()";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (esPrimerElemento)
                                                            {
                                                                Texto = Texto + "\t\t" + strNombreSp + "." + nameCol + "";
                                                                esPrimerElemento = false;
                                                            }
                                                            else
                                                            {
                                                                Texto = Texto + ", \r\n\t\t" + strNombreSp + "." + nameCol + "";
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t" + strNombreSp + "." + nameCol + "";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t" + strNombreSp + "." + nameCol + "";
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n\t); \r\nreturn;\r\nend;\r\n$$";

                                        Texto = Texto + "\r\n  LANGUAGE plpgsql VOLATILE COST 100;\r\n\r\n";

                                        Texto = Texto + "\r\n\r\n\r\n";
                                        break;
                                }
                            }
                        }

                        if (formulario.chkSPupd.Checked)
                        {
                            //UPDATE
                            if (dtColumns.Rows.Count > 0)
                            {
                                cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Upd");
                                cabecera = cabecera.Replace("%OPERACION%", "Modifica");
                                cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                switch (Principal.DBMS)
                                {
                                    case TipoBD.SQLServer:
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "IF EXISTS (SELECT * FROM [dbo].[sysobjects] \r\n";
                                            Texto = Texto + "\tWHERE ID = object_id(N'[dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Upd]') AND \r\n";
                                            Texto = Texto + "\tOBJECTPROPERTY(id, N'IsProcedure') = 1) \r\n";
                                            Texto = Texto + "DROP PROCEDURE [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Upd]\r\n";
                                            Texto = Texto + "GO\r\n\r\n\r\n";
                                        }

                                        Texto = Texto + cabecera;
                                        if (formulario.rdbSpCreate.Checked | formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Upd] \r\n";
                                        }

                                        if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "ALTER PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Upd] \r\n";
                                        }

                                        esPrimerElemento = true;

                                        //Parametros
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();
                                            longitudCol = int.Parse(dtColumns.Rows[iFil]["LONGITUD"].ToString());
                                            switch (tipoCol.ToUpper())
                                            {
                                                case "INT":
                                                    strLongitud = "";
                                                    break;

                                                case "BIGINT":
                                                    strLongitud = "";
                                                    break;

                                                case "IMAGE":
                                                    strLongitud = "";
                                                    break;

                                                case "BIT":
                                                    strLongitud = "";
                                                    break;

                                                case "DATE":
                                                    strLongitud = "";
                                                    break;

                                                case "DATETIME":
                                                    strLongitud = "";
                                                    break;

                                                case "SMALLINT":
                                                    strLongitud = "";
                                                    break;

                                                case "SMALLDATETIME":
                                                    strLongitud = "";
                                                    break;

                                                default:
                                                    strLongitud = "(" + longitudCol + ")";
                                                    break;
                                            }

                                            if (nameCol == strApiEstado)
                                            {
                                            }
                                            else if (nameCol == strApiTrans)
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    Texto = Texto + "\t@" + nameCol + " " + tipoCol + strLongitud;
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    Texto = Texto + ", \r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                }
                                            }
                                            else if (nameCol == strUsuMod)
                                            {
                                                if (formulario.chkSPUpdUsuMod.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t@" + nameCol + " " + tipoCol + strLongitud;
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                    }
                                                }
                                            }
                                            else if (nameCol == strFecMod)
                                            {
                                                if (formulario.chkSPUpdFecMod.Checked & !formulario.chkSPUpdFecModGetDate.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t@" + nameCol + " " + tipoCol + strLongitud;
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                    }
                                                }
                                            }
                                            else if (nameCol == strUsuCre)
                                            {
                                            }
                                            else if (nameCol == strFecCre)
                                            {
                                            }
                                            else
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    Texto = Texto + "\t@" + nameCol + " " + tipoCol + strLongitud;
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    Texto = Texto + ", \r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\nAS \r\n";
                                        Texto = Texto + "\tUPDATE [" + NameTabla + "] SET\r\n";

                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();

                                            if (nameCol == strApiEstado)
                                            {
                                            }
                                            else if (nameCol == strApiTrans)
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    Texto = Texto + "\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    Texto = Texto + ", \r\n\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                }
                                            }
                                            else if (nameCol == strUsuMod)
                                            {
                                                if (formulario.chkSPUpdUsuMod.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                    }
                                                }
                                            }
                                            else if (nameCol == strFecMod)
                                            {
                                                if (formulario.chkSPUpdFecMod.Checked)
                                                {
                                                    if (formulario.chkSPUpdFecModGetDate.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t[" + nameCol + "] = GETDATE()";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t[" + nameCol + "] = GETDATE()";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                        }
                                                    }
                                                }
                                            }
                                            else if (nameCol == strUsuCre)
                                            {
                                            }
                                            else if (nameCol == strFecCre)
                                            {
                                            }
                                            else
                                            {
                                                if (!(dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes"))
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n\tWHERE \r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strUsuMod.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()) & !nameCol.ToUpper().Contains(strApiEstado.ToUpper()) & !nameCol.ToUpper().Contains(strApiTrans.ToUpper()))
                                            {
                                                if (dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes")
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + " AND \r\n\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\nGO";

                                        Texto = Texto + "\r\n\r\n\r\n";

                                        break;

                                    case TipoBD.SQLServer2000:
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "IF EXISTS (SELECT * FROM [dbo].[sysobjects] \r\n";
                                            Texto = Texto + "\tWHERE ID = object_id(N'[dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Upd]') AND \r\n";
                                            Texto = Texto + "\tOBJECTPROPERTY(id, N'IsProcedure') = 1) \r\n";
                                            Texto = Texto + "DROP PROCEDURE [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Upd]\r\n";
                                            Texto = Texto + "GO\r\n\r\n\r\n";
                                        }

                                        Texto = Texto + cabecera;
                                        if (formulario.rdbSpCreate.Checked | formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Upd] \r\n";
                                        }

                                        if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "ALTER PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Upd] \r\n";
                                        }

                                        esPrimerElemento = true;

                                        //Parametros
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();
                                            longitudCol = int.Parse(dtColumns.Rows[iFil]["LONGITUD"].ToString());
                                            switch (tipoCol.ToUpper())
                                            {
                                                case "INT":
                                                    strLongitud = "";
                                                    break;

                                                case "BIGINT":
                                                    strLongitud = "";
                                                    break;

                                                case "IMAGE":
                                                    strLongitud = "";
                                                    break;

                                                case "BIT":
                                                    strLongitud = "";
                                                    break;

                                                case "DATE":
                                                    strLongitud = "";
                                                    break;

                                                case "DATETIME":
                                                    strLongitud = "";
                                                    break;

                                                case "SMALLINT":
                                                    strLongitud = "";
                                                    break;

                                                case "SMALLDATETIME":
                                                    strLongitud = "";
                                                    break;

                                                default:
                                                    strLongitud = "(" + longitudCol + ")";
                                                    break;
                                            }

                                            if (nameCol == strApiEstado)
                                            {
                                            }
                                            else if (nameCol == strApiTrans)
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    Texto = Texto + "\t@" + nameCol + " " + tipoCol + strLongitud;
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    Texto = Texto + ", \r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                }
                                            }
                                            else if (nameCol == strUsuMod)
                                            {
                                                if (formulario.chkSPUpdUsuMod.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t@" + nameCol + " " + tipoCol + strLongitud;
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                    }
                                                }
                                            }
                                            else if (nameCol == strFecMod)
                                            {
                                                if (formulario.chkSPUpdFecMod.Checked & !formulario.chkSPUpdFecModGetDate.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t@" + nameCol + " " + tipoCol + strLongitud;
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                    }
                                                }
                                            }
                                            else if (nameCol == strUsuCre)
                                            {
                                            }
                                            else if (nameCol == strFecCre)
                                            {
                                            }
                                            else
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    Texto = Texto + "\t@" + nameCol + " " + tipoCol + strLongitud;
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    Texto = Texto + ", \r\n\t@" + nameCol + " " + tipoCol + strLongitud;
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\nAS \r\n";
                                        Texto = Texto + "\tUPDATE [" + NameTabla + "] SET\r\n";

                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();

                                            if (nameCol == strApiEstado)
                                            {
                                            }
                                            else if (nameCol == strApiTrans)
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    Texto = Texto + "\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    Texto = Texto + ", \r\n\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                }
                                            }
                                            else if (nameCol == strUsuMod)
                                            {
                                                if (formulario.chkSPUpdUsuMod.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                    }
                                                }
                                            }
                                            else if (nameCol == strFecMod)
                                            {
                                                if (formulario.chkSPUpdFecMod.Checked)
                                                {
                                                    if (formulario.chkSPUpdFecModGetDate.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t[" + nameCol + "] = GETDATE()";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t[" + nameCol + "] = GETDATE()";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                        }
                                                    }
                                                }
                                            }
                                            else if (nameCol == strUsuCre)
                                            {
                                            }
                                            else if (nameCol == strFecCre)
                                            {
                                            }
                                            else
                                            {
                                                if (!(dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes"))
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n\tWHERE \r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strUsuMod.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()) & !nameCol.ToUpper().Contains(strApiEstado.ToUpper()) & !nameCol.ToUpper().Contains(strApiTrans.ToUpper()))
                                            {
                                                if (dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes")
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + " AND \r\n\t\t[" + nameCol + "] = @" + nameCol.Replace(" ", "_");
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\nGO";

                                        Texto = Texto + "\r\n\r\n\r\n";

                                        break;

                                    case TipoBD.Oracle:

                                        Texto = Texto + "CREATE OR REPLACE PROCEDURE " + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "UPD\r\n";
                                        Texto = Texto + "(\r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();

                                            if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strUsuMod.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()) & !nameCol.ToUpper().Contains(strApiEstado.ToUpper()))
                                            {
                                                if (formulario.chkSPParametrosPa.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\tpa" + nameCol + " IN " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\tpa" + nameCol + " IN " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                    }
                                                }
                                                else
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t" + nameCol + " IN " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t" + nameCol + " IN " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n)\r\n";
                                        Texto = Texto + cabecera;
                                        Texto = Texto + "\r\nAS \r\n";
                                        Texto = Texto + "\r\nBEGIN \r\n";

                                        Texto = Texto + "\tUPDATE " + NameTabla + " SET\r\n";

                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();

                                            if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strUsuMod.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()) & !nameCol.ToUpper().Contains(strApiEstado.ToUpper()))
                                            {
                                                if (!(dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes"))
                                                {
                                                    if (formulario.chkSPParametrosPa.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t" + nameCol + " = " + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "UPD.pa" + nameCol.Replace(" ", "_");
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t" + nameCol + " = " + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "UPD.pa" + nameCol.Replace(" ", "_");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t" + nameCol + " = " + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "UPD." + nameCol.Replace(" ", "_");
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t" + nameCol + " = " + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "UPD." + nameCol.Replace(" ", "_");
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n\tWHERE \r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (!nameCol.ToUpper().Contains(strUsuCre.ToUpper()) & !nameCol.ToUpper().Contains(strFecCre.ToUpper()) & !nameCol.ToUpper().Contains(strUsuMod.ToUpper()) & !nameCol.ToUpper().Contains(strFecMod.ToUpper()) & !nameCol.ToUpper().Contains(strApiEstado.ToUpper()) & !nameCol.ToUpper().Contains(strApiTrans.ToUpper()))
                                            {
                                                if (dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes")
                                                {
                                                    if (formulario.chkSPParametrosPa.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t" + nameCol + " = " + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "UPD.pa" + nameCol.Replace(" ", "_");
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + " AND \r\n\t\t" + nameCol + " = " + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "UPD.pa" + nameCol.Replace(" ", "_");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t" + nameCol + " = " + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "UPD." + nameCol.Replace(" ", "_");
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + " AND \r\n\t\t" + nameCol + " = " + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "UPD." + nameCol.Replace(" ", "_");
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + ";\r\nEND;\r\n/";

                                        Texto = Texto + "\r\n\r\n\r\n";

                                        break;

                                    case TipoBD.PostgreSQL:
                                        strNombreSp = (formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Upd").ToLower();

                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE OR REPLACE FUNCTION " + strNombreSp + " (\r\n";
                                        }
                                        else if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "REPLACE FUNCTION " + strNombreSp + " (\r\n";
                                        }
                                        else
                                        {
                                            Texto = Texto + "CREATE FUNCTION " + strNombreSp + " (\r\n";
                                        }

                                        esPrimerElemento = true;

                                        //Parametros
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();
                                            strLongitud = "";

                                            if (nameCol == strApiEstado)
                                            {
                                            }
                                            else if (nameCol == strApiTrans)
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    Texto = Texto + "\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    Texto = Texto + ", \r\n\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                }
                                            }
                                            else if (nameCol == strUsuMod)
                                            {
                                                if (formulario.chkSPUpdUsuMod.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                    }
                                                }
                                            }
                                            else if (nameCol == strFecMod)
                                            {
                                                if (formulario.chkSPUpdFecMod.Checked & !formulario.chkSPUpdFecModGetDate.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                    }
                                                }
                                            }
                                            else if (nameCol == strUsuCre)
                                            {
                                            }
                                            else if (nameCol == strFecCre)
                                            {
                                            }
                                            else
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    Texto = Texto + "\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    Texto = Texto + ", \r\n\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n) \r\n  RETURNS void AS\r\n$$\r\nBEGIN\r\n";

                                        Texto = Texto + cabecera + "\r\n";

                                        Texto = Texto + "\tUPDATE " + NameTabla + " SET\r\n";

                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();

                                            if (nameCol == strApiEstado)
                                            {
                                            }
                                            else if (nameCol == strApiTrans)
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    Texto = Texto + "\t\t" + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    Texto = Texto + ", \r\n\t\t" + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                }
                                            }
                                            else if (nameCol == strUsuMod)
                                            {
                                                if (formulario.chkSPUpdUsuMod.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t" + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t" + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                    }
                                                }
                                            }
                                            else if (nameCol == strFecMod)
                                            {
                                                if (formulario.chkSPUpdFecMod.Checked)
                                                {
                                                    if (formulario.chkSPUpdFecModGetDate.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t" + nameCol + " = now()";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t" + nameCol + " = now()";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t" + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t" + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                        }
                                                    }
                                                }
                                            }
                                            else if (nameCol == strUsuCre)
                                            {
                                            }
                                            else if (nameCol == strFecCre)
                                            {
                                            }
                                            else
                                            {
                                                if (!(dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes"))
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t" + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t" + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n\tWHERE \r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (!nameCol.Contains(strUsuCre.ToUpper()) & !nameCol.Contains(strFecCre.ToUpper()) & !nameCol.Contains(strUsuMod.ToUpper()) & !nameCol.Contains(strFecMod.ToUpper()) & !nameCol.Contains(strApiEstado.ToUpper()) & !nameCol.Contains(strApiTrans.ToUpper()))
                                            {
                                                if (dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes")
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t" + NameTabla + "." + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + " AND \r\n\t\t" + NameTabla + "." + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + ";\r\nreturn;\r\nend;\r\n$$\r\n";

                                        Texto = Texto + "\r\n  LANGUAGE plpgsql VOLATILE COST 100;\r\n\r\n";
                                        Texto = Texto + "\r\n\r\n\r\n";
                                        break;
                                }
                            }
                        }

                        if (formulario.chkSPsel.Checked)
                        {
                            //SELECT
                            if (dtColumns.Rows.Count > 0)
                            {
                                switch (Principal.DBMS)
                                {
                                    case TipoBD.SQLServer:
                                        //SELECT All
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "IF EXISTS (SELECT * FROM [dbo].[sysobjects] \r\n";
                                            Texto = Texto + "\tWHERE ID = object_id(N'[dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Sel]') AND \r\n";
                                            Texto = Texto + "\tOBJECTPROPERTY(id, N'IsProcedure') = 1) \r\n";
                                            Texto = Texto + "DROP PROCEDURE [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Sel]\r\n";
                                            Texto = Texto + "GO\r\n\r\n\r\n";
                                        }

                                        cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Sel");
                                        cabecera = cabecera.Replace("%OPERACION%", "Selecciona");
                                        cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                        Texto = Texto + cabecera;

                                        if (formulario.rdbSpCreate.Checked | formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Sel] AS \r\n\r\n";
                                        }

                                        if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "ALTER PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Sel] AS \r\n\r\n";
                                        }

                                        Texto = Texto + "\tSELECT * FROM " + NameTabla + "\r\nGO";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        //SELECT Pick
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "IF EXISTS (SELECT * FROM [dbo].[sysobjects] \r\n";
                                            Texto = Texto + "\tWHERE ID = object_id(N'[dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPick]') AND \r\n";
                                            Texto = Texto + "\tOBJECTPROPERTY(id, N'IsProcedure') = 1) \r\n";
                                            Texto = Texto + "DROP PROCEDURE [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPick]\r\n";
                                            Texto = Texto + "GO\r\n\r\n\r\n";
                                        }

                                        cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPick");
                                        cabecera = cabecera.Replace("%OPERACION%", "Selecciona");
                                        cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                        Texto = Texto + cabecera;

                                        if (formulario.rdbSpCreate.Checked | formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPick] \r\n\r\n";
                                        }

                                        if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "ALTER PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPick]\r\n\r\n";
                                        }

                                        Texto = Texto + "\t@Columnas varchar(8000)\r\n";
                                        Texto = Texto + "AS\r\n";

                                        Texto = Texto + "\tDECLARE @SQL varchar(8000) \r\n";
                                        Texto = Texto + "\tSET @SQL = 'SELECT ' + REPLACE(@Columnas,'''''','''') + ' FROM " + NameTabla + "'\r\n";
                                        Texto = Texto + "\tEXEC(@SQL) \r\nGO";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        //SELECT All Conditions
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "IF EXISTS (SELECT * FROM [dbo].[sysobjects] \r\n";
                                            Texto = Texto + "\tWHERE ID = object_id(N'[dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelCnd]') AND \r\n";
                                            Texto = Texto + "\tOBJECTPROPERTY(id, N'IsProcedure') = 1) \r\n";
                                            Texto = Texto + "DROP PROCEDURE [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelCnd]\r\n";
                                            Texto = Texto + "GO\r\n\r\n\r\n";
                                        }

                                        cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelCnd");
                                        cabecera = cabecera.Replace("%OPERACION%", "Selecciona");
                                        cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                        Texto = Texto + cabecera;

                                        if (formulario.rdbSpCreate.Checked | formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelCnd] \r\n\r\n";
                                        }

                                        if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "ALTER PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelCnd]\r\n\r\n";
                                        }

                                        Texto = Texto + "\t@ColumnasWhere varchar(8000)\r\n";
                                        Texto = Texto + "AS\r\n";

                                        Texto = Texto + "\tDECLARE @SQL varchar(8000) \r\n";
                                        Texto = Texto + "\tSET @SQL = 'SELECT * FROM " + NameTabla + " WHERE ' + REPLACE(@ColumnasWhere,'''''','''') \r\n";
                                        Texto = Texto + "\tEXEC(@SQL) \r\nGO";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        //SELECT Pick Conditions
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "IF EXISTS (SELECT * FROM [dbo].[sysobjects] \r\n";
                                            Texto = Texto + "\tWHERE ID = object_id(N'[dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCnd]') AND \r\n";
                                            Texto = Texto + "\tOBJECTPROPERTY(id, N'IsProcedure') = 1) \r\n";
                                            Texto = Texto + "DROP PROCEDURE [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCnd]\r\n";
                                            Texto = Texto + "GO\r\n\r\n\r\n";
                                        }

                                        cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCnd");
                                        cabecera = cabecera.Replace("%OPERACION%", "Selecciona");
                                        cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                        Texto = Texto + cabecera;

                                        if (formulario.rdbSpCreate.Checked | formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCnd] \r\n\r\n";
                                        }

                                        if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "ALTER PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCnd]\r\n\r\n";
                                        }
                                        Texto = Texto + "\t@Columnas varchar(8000),\r\n";
                                        Texto = Texto + "\t@ColumnasWhere varchar(8000)\r\n";
                                        Texto = Texto + "AS\r\n";

                                        Texto = Texto + "\tDECLARE @SQL varchar(8000) \r\n";
                                        Texto = Texto + "\tSET @SQL = 'SELECT ' + REPLACE(@Columnas,'''''','''') + ' FROM " + NameTabla + " WHERE ' + REPLACE(@ColumnasWhere,'''''','''')\r\n";
                                        Texto = Texto + "\tEXEC(@SQL) \r\nGO";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        //SELECT Pick Conditions Extra
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "IF EXISTS (SELECT * FROM [dbo].[sysobjects] \r\n";
                                            Texto = Texto + "\tWHERE ID = object_id(N'[dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCndExt]') AND \r\n";
                                            Texto = Texto + "\tOBJECTPROPERTY(id, N'IsProcedure') = 1) \r\n";
                                            Texto = Texto + "DROP PROCEDURE [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCndExt]\r\n";
                                            Texto = Texto + "GO\r\n\r\n\r\n";
                                        }

                                        cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCndExt");
                                        cabecera = cabecera.Replace("%OPERACION%", "Selecciona");
                                        cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                        Texto = Texto + cabecera;

                                        if (formulario.rdbSpCreate.Checked | formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCndExt] \r\n\r\n";
                                        }

                                        if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "ALTER PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCndExt]\r\n\r\n";
                                        }
                                        Texto = Texto + "\t@Columnas varchar(8000),\r\n";
                                        Texto = Texto + "\t@ColumnasWhere varchar(8000),\r\n";
                                        Texto = Texto + "\t@Parametros varchar(8000)\r\n";
                                        Texto = Texto + "AS\r\n";

                                        Texto = Texto + "\tDECLARE @SQL varchar(8000) \r\n";
                                        Texto = Texto + "\tSET @SQL = 'SELECT ' + REPLACE(@Columnas,'''''','''') + ' FROM " + NameTabla + " WHERE ' + REPLACE(@ColumnasWhere,'''''','''') + ' ' + @Parametros \r\n";
                                        Texto = Texto + "\tEXEC(@SQL) \r\nGO";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        break;

                                    case TipoBD.SQLServer2000:
                                        //SELECT All
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "IF EXISTS (SELECT * FROM [dbo].[sysobjects] \r\n";
                                            Texto = Texto + "\tWHERE ID = object_id(N'[dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Sel]') AND \r\n";
                                            Texto = Texto + "\tOBJECTPROPERTY(id, N'IsProcedure') = 1) \r\n";
                                            Texto = Texto + "DROP PROCEDURE [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Sel]\r\n";
                                            Texto = Texto + "GO\r\n\r\n\r\n";
                                        }

                                        cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Sel");
                                        cabecera = cabecera.Replace("%OPERACION%", "Selecciona");
                                        cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                        Texto = Texto + cabecera;

                                        if (formulario.rdbSpCreate.Checked | formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Sel] AS \r\n\r\n";
                                        }

                                        if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "ALTER PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Sel] AS \r\n\r\n";
                                        }

                                        Texto = Texto + "\tSELECT * FROM " + NameTabla + "\r\nGO";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        //SELECT Pick
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "IF EXISTS (SELECT * FROM [dbo].[sysobjects] \r\n";
                                            Texto = Texto + "\tWHERE ID = object_id(N'[dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPick]') AND \r\n";
                                            Texto = Texto + "\tOBJECTPROPERTY(id, N'IsProcedure') = 1) \r\n";
                                            Texto = Texto + "DROP PROCEDURE [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPick]\r\n";
                                            Texto = Texto + "GO\r\n\r\n\r\n";
                                        }

                                        cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPick");
                                        cabecera = cabecera.Replace("%OPERACION%", "Selecciona");
                                        cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                        Texto = Texto + cabecera;

                                        if (formulario.rdbSpCreate.Checked | formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPick] \r\n\r\n";
                                        }

                                        if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "ALTER PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPick]\r\n\r\n";
                                        }

                                        Texto = Texto + "\t@Columnas varchar(8000)\r\n";
                                        Texto = Texto + "AS\r\n";

                                        Texto = Texto + "\tDECLARE @SQL varchar(8000) \r\n";
                                        Texto = Texto + "\tSET @SQL = 'SELECT ' + REPLACE(@Columnas,'''''','''') + ' FROM " + NameTabla + "'\r\n";
                                        Texto = Texto + "\tEXEC(@SQL) \r\nGO";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        //SELECT All Conditions
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "IF EXISTS (SELECT * FROM [dbo].[sysobjects] \r\n";
                                            Texto = Texto + "\tWHERE ID = object_id(N'[dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelCnd]') AND \r\n";
                                            Texto = Texto + "\tOBJECTPROPERTY(id, N'IsProcedure') = 1) \r\n";
                                            Texto = Texto + "DROP PROCEDURE [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelCnd]\r\n";
                                            Texto = Texto + "GO\r\n\r\n\r\n";
                                        }

                                        cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelCnd");
                                        cabecera = cabecera.Replace("%OPERACION%", "Selecciona");
                                        cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                        Texto = Texto + cabecera;

                                        if (formulario.rdbSpCreate.Checked | formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelCnd] \r\n\r\n";
                                        }

                                        if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "ALTER PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelCnd]\r\n\r\n";
                                        }

                                        Texto = Texto + "\t@ColumnasWhere varchar(8000)\r\n";
                                        Texto = Texto + "AS\r\n";

                                        Texto = Texto + "\tDECLARE @SQL varchar(8000) \r\n";
                                        Texto = Texto + "\tSET @SQL = 'SELECT * FROM " + NameTabla + " WHERE ' + REPLACE(@ColumnasWhere,'''''','''') \r\n";
                                        Texto = Texto + "\tEXEC(@SQL) \r\nGO";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        //SELECT Pick Conditions
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "IF EXISTS (SELECT * FROM [dbo].[sysobjects] \r\n";
                                            Texto = Texto + "\tWHERE ID = object_id(N'[dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCnd]') AND \r\n";
                                            Texto = Texto + "\tOBJECTPROPERTY(id, N'IsProcedure') = 1) \r\n";
                                            Texto = Texto + "DROP PROCEDURE [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCnd]\r\n";
                                            Texto = Texto + "GO\r\n\r\n\r\n";
                                        }

                                        cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCnd");
                                        cabecera = cabecera.Replace("%OPERACION%", "Selecciona");
                                        cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                        Texto = Texto + cabecera;

                                        if (formulario.rdbSpCreate.Checked | formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCnd] \r\n\r\n";
                                        }

                                        if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "ALTER PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCnd]\r\n\r\n";
                                        }
                                        Texto = Texto + "\t@Columnas varchar(8000),\r\n";
                                        Texto = Texto + "\t@ColumnasWhere varchar(8000)\r\n";
                                        Texto = Texto + "AS\r\n";

                                        Texto = Texto + "\tDECLARE @SQL varchar(8000) \r\n";
                                        Texto = Texto + "\tSET @SQL = 'SELECT ' + REPLACE(@Columnas,'''''','''') + ' FROM " + NameTabla + " WHERE ' + REPLACE(@ColumnasWhere,'''''','''')\r\n";
                                        Texto = Texto + "\tEXEC(@SQL) \r\nGO";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        //SELECT Pick Conditions Extra
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "IF EXISTS (SELECT * FROM [dbo].[sysobjects] \r\n";
                                            Texto = Texto + "\tWHERE ID = object_id(N'[dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCndExt]') AND \r\n";
                                            Texto = Texto + "\tOBJECTPROPERTY(id, N'IsProcedure') = 1) \r\n";
                                            Texto = Texto + "DROP PROCEDURE [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCndExt]\r\n";
                                            Texto = Texto + "GO\r\n\r\n\r\n";
                                        }

                                        cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCndExt");
                                        cabecera = cabecera.Replace("%OPERACION%", "Selecciona");
                                        cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                        Texto = Texto + cabecera;

                                        if (formulario.rdbSpCreate.Checked | formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCndExt] \r\n\r\n";
                                        }

                                        if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "ALTER PROC [dbo].[" + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCndExt]\r\n\r\n";
                                        }
                                        Texto = Texto + "\t@Columnas varchar(8000),\r\n";
                                        Texto = Texto + "\t@ColumnasWhere varchar(8000),\r\n";
                                        Texto = Texto + "\t@Parametros varchar(8000)\r\n";
                                        Texto = Texto + "AS\r\n";

                                        Texto = Texto + "\tDECLARE @SQL varchar(8000) \r\n";
                                        Texto = Texto + "\tSET @SQL = 'SELECT ' + REPLACE(@Columnas,'''''','''') + ' FROM " + NameTabla + " WHERE ' + REPLACE(@ColumnasWhere,'''''','''') + ' ' + @Parametros \r\n";
                                        Texto = Texto + "\tEXEC(@SQL) \r\nGO";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        break;

                                    case TipoBD.Oracle:
                                        Texto = Texto + "CREATE OR REPLACE PROCEDURE " + formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SEL AS \r\n\r\n";
                                        Texto = Texto + "\tSELECT * FROM " + NameTabla + ";\r\nEND;\r\n/";
                                        Texto = Texto + "\r\n\r\n\r\n";

                                        break;

                                    case TipoBD.PostgreSQL:
                                        strNombreSp = (formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text).ToLower();

                                        //SELECT All
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE OR REPLACE FUNCTION " + strNombreSp + "sel()\r\n";
                                        }
                                        else if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "REPLACE FUNCTION " + strNombreSp + "sel()\r\n";
                                        }
                                        else
                                        {
                                            Texto = Texto + "CREATE FUNCTION " + strNombreSp + "sel()\r\n";
                                        }

                                        cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "Sel");
                                        cabecera = cabecera.Replace("%OPERACION%", "Selecciona");
                                        cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                        string txtColumnas = "";
                                        bool bPrimerElemento = true;

                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();

                                            if (bPrimerElemento)
                                            {
                                                txtColumnas = txtColumnas + nameCol + " " + tipoCol;
                                                bPrimerElemento = false;
                                            }
                                            else
                                            {
                                                txtColumnas = txtColumnas + ", " + nameCol + " " + tipoCol;
                                            }
                                        }

                                        Texto = Texto + "RETURNS table (" + txtColumnas + ")\r\n";
                                        Texto = Texto + "AS $$\r\n";
                                        Texto = Texto + "BEGIN\r\n";
                                        Texto = Texto + cabecera;
                                        Texto = Texto + "\tSELECT * FROM " + NameTabla + ";\r\n";
                                        Texto = Texto + "END; $$ language plpgsql;\r\n\r\n";

                                        Texto = Texto + "\r\n\r\n\r\n";

                                        //SELECT Pick
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE OR REPLACE FUNCTION " + strNombreSp + "selpick (columnas CHARACTER VARYING)\r\n";
                                        }
                                        else if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "REPLACE FUNCTION " + strNombreSp + "selpick (columnas CHARACTER VARYING)\r\n";
                                        }
                                        else
                                        {
                                            Texto = Texto + "CREATE FUNCTION " + strNombreSp + "selpick (columnas CHARACTER VARYING)\r\n";
                                        }

                                        cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPick");
                                        cabecera = cabecera.Replace("%OPERACION%", "Selecciona");
                                        cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                        txtColumnas = "";
                                        bPrimerElemento = true;

                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();

                                            if (bPrimerElemento)
                                            {
                                                txtColumnas = txtColumnas + nameCol + " " + tipoCol;
                                                bPrimerElemento = false;
                                            }
                                            else
                                            {
                                                txtColumnas = txtColumnas + ", " + nameCol + " " + tipoCol;
                                            }
                                        }

                                        Texto = Texto + "RETURNS table (" + txtColumnas + ")\r\n";
                                        Texto = Texto + "AS $$\r\n";
                                        Texto = Texto + cabecera + "\r\n";
                                        Texto = Texto + "\tDECLARE pasql TEXT;\r\n\r\n";
                                        Texto = Texto + "\tBEGIN\r\n";
                                        Texto = Texto + "\tpasql := 'SELECT ' || REPLACE(" + strNombreSp + "selpick.columnas,'''''','''') || ' FROM " + NameTabla + " ';\r\n";
                                        Texto = Texto + "\tRETURN QUERY EXECUTE format(pasql);\r\n";
                                        Texto = Texto + "END; $$ language plpgsql;\r\n\r\n";

                                        Texto = Texto + "\r\n\r\n\r\n";

                                        //SELECT All Conditions
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE OR REPLACE FUNCTION " + strNombreSp + "selcnd (columnaswhere CHARACTER VARYING)\r\n";
                                        }
                                        else if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "REPLACE FUNCTION " + strNombreSp + "selcnd (columnaswhere CHARACTER VARYING)\r\n";
                                        }
                                        else
                                        {
                                            Texto = Texto + "CREATE FUNCTION " + strNombreSp + "selcnd (columnaswhere CHARACTER VARYING)\r\n";
                                        }

                                        cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelCnd");
                                        cabecera = cabecera.Replace("%OPERACION%", "Selecciona");
                                        cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                        txtColumnas = "";
                                        bPrimerElemento = true;

                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();

                                            if (bPrimerElemento)
                                            {
                                                txtColumnas = txtColumnas + nameCol + " " + tipoCol;
                                                bPrimerElemento = false;
                                            }
                                            else
                                            {
                                                txtColumnas = txtColumnas + ", " + nameCol + " " + tipoCol;
                                            }
                                        }

                                        Texto = Texto + "RETURNS table (" + txtColumnas + ")\r\n";
                                        Texto = Texto + "AS $$\r\n";
                                        Texto = Texto + cabecera + "\r\n";
                                        Texto = Texto + "\tDECLARE pasql TEXT;\r\n\r\n";
                                        Texto = Texto + "\tBEGIN\r\n";
                                        Texto = Texto + "\tpasql := 'SELECT * FROM " + NameTabla + " WHERE ' || REPLACE(" + strNombreSp + "selcnd.columnaswhere,'''''','''') ;\r\n";
                                        Texto = Texto + "\tRETURN QUERY EXECUTE format(pasql);\r\n";
                                        Texto = Texto + "END; $$ language plpgsql;\r\n\r\n";

                                        Texto = Texto + "\r\n\r\n\r\n";

                                        //SELECT Pick Conditions
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE OR REPLACE FUNCTION " + strNombreSp + "selpickcnd (columnas CHARACTER VARYING, columnaswhere CHARACTER VARYING)\r\n";
                                        }
                                        else if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "REPLACE FUNCTION " + strNombreSp + "selpickcnd (columnas CHARACTER VARYING, columnaswhere CHARACTER VARYING)\r\n";
                                        }
                                        else
                                        {
                                            Texto = Texto + "CREATE FUNCTION " + strNombreSp + "selpickcnd (columnas CHARACTER VARYING, columnaswhere CHARACTER VARYING)\r\n";
                                        }

                                        cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCnd");
                                        cabecera = cabecera.Replace("%OPERACION%", "Selecciona");
                                        cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                        txtColumnas = "";
                                        bPrimerElemento = true;

                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();

                                            if (bPrimerElemento)
                                            {
                                                txtColumnas = txtColumnas + nameCol + " " + tipoCol;
                                                bPrimerElemento = false;
                                            }
                                            else
                                            {
                                                txtColumnas = txtColumnas + ", " + nameCol + " " + tipoCol;
                                            }
                                        }

                                        Texto = Texto + "RETURNS table (" + txtColumnas + ")\r\n";
                                        Texto = Texto + "AS $$\r\n";
                                        Texto = Texto + cabecera + "\r\n";
                                        Texto = Texto + "\tDECLARE pasql TEXT;\r\n\r\n";
                                        Texto = Texto + "\tBEGIN\r\n";
                                        Texto = Texto + "\tpasql := 'SELECT ' || REPLACE(" + strNombreSp + "selpickcnd.columnas,'''''','''') || ' FROM " + NameTabla + " WHERE ' || REPLACE(" + strNombreSp + "selpickcnd.columnaswhere,'''''','''');\r\n";
                                        Texto = Texto + "\tRETURN QUERY EXECUTE format(pasql);\r\n";
                                        Texto = Texto + "END; $$ language plpgsql;\r\n\r\n";

                                        Texto = Texto + "\r\n\r\n\r\n";

                                        //SELECT Pick Conditions Extra
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE OR REPLACE FUNCTION " + strNombreSp + "selpickcndext (columnas CHARACTER VARYING, columnaswhere CHARACTER VARYING, parametros CHARACTER VARYING)\r\n";
                                        }
                                        else if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "REPLACE FUNCTION " + strNombreSp + "selpickcndext (columnas CHARACTER VARYING, columnaswhere CHARACTER VARYING, parametros CHARACTER VARYING)\r\n";
                                        }
                                        else
                                        {
                                            Texto = Texto + "CREATE FUNCTION " + strNombreSp + "selpickcndext (columnas CHARACTER VARYING, columnaswhere CHARACTER VARYING, parametros CHARACTER VARYING)\r\n";
                                        }

                                        cabecera = cabeceraPlantilla.Replace("%NOMBRE%", formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "SelPickCndExt");
                                        cabecera = cabecera.Replace("%OPERACION%", "Selecciona");
                                        cabecera = cabecera.Replace("%TABLA%", NameTabla);

                                        txtColumnas = "";
                                        bPrimerElemento = true;

                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();

                                            if (bPrimerElemento)
                                            {
                                                txtColumnas = txtColumnas + nameCol + " " + tipoCol;
                                                bPrimerElemento = false;
                                            }
                                            else
                                            {
                                                txtColumnas = txtColumnas + ", " + nameCol + " " + tipoCol;
                                            }
                                        }

                                        Texto = Texto + "RETURNS table (" + txtColumnas + ")\r\n";
                                        Texto = Texto + "AS $$\r\n";
                                        Texto = Texto + cabecera + "\r\n";
                                        Texto = Texto + "\tDECLARE pasql TEXT;\r\n\r\n";
                                        Texto = Texto + "\tBEGIN\r\n";
                                        Texto = Texto + "\tpasql := 'SELECT ' || REPLACE(" + strNombreSp + "selpickcndext.columnas,'''''','''') || ' FROM " + NameTabla + " WHERE ' || REPLACE(" + strNombreSp + "selpickcndext.columnaswhere,'''''','''') || ' ' || " + strNombreSp + "selpickcndext.parametros ;\r\n";
                                        Texto = Texto + "\tRETURN QUERY EXECUTE format(pasql);\r\n";
                                        Texto = Texto + "END; $$ language 'sql';\r\n\r\n";
                                        Texto = Texto + "\r\n\r\n\r\n";
                                        break;
                                }
                            }
                        }

                        if (formulario.chkSPInsUpd.Checked)
                        {
                            if (dtColumns.Rows.Count > 0)
                            {
                                switch (Principal.DBMS)
                                {
                                    case TipoBD.PostgreSQL:
                                        strNombreSp = (formulario.txtSPprev.Text + NameTablaAlias + formulario.txtSPsig.Text + "InsUpd").ToLower();
                                        if (formulario.rdbSpIfExists.Checked)
                                        {
                                            Texto = Texto + "CREATE OR REPLACE FUNCTION " + strNombreSp + " (\r\n";
                                        }
                                        else if (formulario.rdbSpAlter.Checked)
                                        {
                                            Texto = Texto + "REPLACE FUNCTION " + strNombreSp + " (\r\n";
                                        }
                                        else
                                        {
                                            Texto = Texto + "CREATE FUNCTION " + strNombreSp + " (\r\n";
                                        }

                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (esPrimerElemento)
                                            {
                                                Texto = Texto + "\r\n\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                                esPrimerElemento = false;
                                            }
                                            else
                                            {
                                                Texto = Texto + ",\r\n\t" + nameCol + " " + NameTabla.ToUpper() + "." + nameCol.ToUpper() + "%TYPE DEFAULT NULL";
                                            }
                                        }

                                        Texto = Texto + ")\r\n  RETURNS void AS\r\n";
                                        Texto = Texto + "$BODY$\r\n";
                                        Texto = Texto + "DECLARE\r\n";
                                        Texto = Texto + "\tvl_contador INTEGER;\r\n";
                                        Texto = Texto + "BEGIN\r\n";
                                        Texto = Texto + "\tSELECT COUNT(*) INTO vl_contador\r\n";
                                        Texto = Texto + "\tFROM " + NameTabla + " t\r\n";
                                        Texto = Texto + "\t--WHERE ;\r\n";
                                        Texto = Texto + "\t\r\n";
                                        Texto = Texto + "\tIF vl_contador = 0 THEN\r\n";
                                        Texto = Texto + "\t\r\n";
                                        Texto = Texto + "\t\tINSERT INTO " + NameTabla + " (\r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (dtColumns.Rows[iFil]["EsIdentidad"].ToString() == "No")
                                            {
                                                if (nameCol == strApiEstado)
                                                {
                                                    if (formulario.chkSPInsApiEstado.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t\t" + nameCol + "";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t\t" + nameCol + "";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strApiTrans)
                                                {
                                                    if (formulario.chkSPInsApiEstado.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t\t" + nameCol + "";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t\t" + nameCol + "";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strUsuMod)
                                                {
                                                }
                                                else if (nameCol == strFecMod)
                                                {
                                                }
                                                else if (nameCol == strUsuCre)
                                                {
                                                    if (formulario.chkSPInsUsuCre.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t\t" + nameCol + "";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t\t" + nameCol + "";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strFecCre)
                                                {
                                                    if (formulario.chkSPInsFecCre.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t\t" + nameCol + "";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t\t" + nameCol + "";
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t\t" + nameCol + "";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t\t" + nameCol + "";
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n\t\t) VALUES (\r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (dtColumns.Rows[iFil]["EsIdentidad"].ToString() == "No")
                                            {
                                                if (nameCol == strApiEstado)
                                                {
                                                    if (formulario.chkSPInsApiEstado.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t\t'ELABORADO'";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t\t'ELABORADO'";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strApiTrans)
                                                {
                                                    if (formulario.chkSPInsApiEstado.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t\t'CREAR'";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t\t'CREAR'";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strUsuMod)
                                                {
                                                }
                                                else if (nameCol == strFecMod)
                                                {
                                                }
                                                else if (nameCol == strUsuCre)
                                                {
                                                    if (formulario.chkSPInsUsuCre.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t\t" + strNombreSp + "." + nameCol + "";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t\t" + strNombreSp + "." + nameCol + "";
                                                        }
                                                    }
                                                }
                                                else if (nameCol == strFecCre)
                                                {
                                                    if (formulario.chkSPInsFecCre.Checked)
                                                    {
                                                        if (formulario.chkSPInsFecCreGetDate.Checked)
                                                        {
                                                            if (esPrimerElemento)
                                                            {
                                                                Texto = Texto + "\t\t\tnow()";
                                                                esPrimerElemento = false;
                                                            }
                                                            else
                                                            {
                                                                Texto = Texto + ", \r\n\t\t\tnow()";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (esPrimerElemento)
                                                            {
                                                                Texto = Texto + "\t\t\t" + strNombreSp + "." + nameCol + "";
                                                                esPrimerElemento = false;
                                                            }
                                                            else
                                                            {
                                                                Texto = Texto + ", \r\n\t\t\t" + strNombreSp + "." + nameCol + "";
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t\t" + strNombreSp + "." + nameCol + "";
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t\t" + strNombreSp + "." + nameCol + "";
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n\t\t); \r\n";
                                        Texto = Texto + "\t\r\n";
                                        Texto = Texto + "\tELSE\r\n";
                                        Texto = Texto + "\t\r\n";
                                        Texto = Texto + "\t\tUPDATE " + NameTabla + " SET\r\n";

                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();

                                            if (nameCol == strApiEstado)
                                            {
                                            }
                                            else if (nameCol == strApiTrans)
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    Texto = Texto + "\t\t\t" + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    Texto = Texto + ", \r\n\t\t\t" + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                }
                                            }
                                            else if (nameCol == strUsuMod)
                                            {
                                                if (formulario.chkSPUpdUsuMod.Checked)
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t\t" + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t\t" + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                    }
                                                }
                                            }
                                            else if (nameCol == strFecMod)
                                            {
                                                if (formulario.chkSPUpdFecMod.Checked)
                                                {
                                                    if (formulario.chkSPUpdFecModGetDate.Checked)
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t" + nameCol + " = now()";
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t\t" + nameCol + " = now()";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (esPrimerElemento)
                                                        {
                                                            Texto = Texto + "\t\t\t" + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                            esPrimerElemento = false;
                                                        }
                                                        else
                                                        {
                                                            Texto = Texto + ", \r\n\t\t\t" + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                        }
                                                    }
                                                }
                                            }
                                            else if (nameCol == strUsuCre)
                                            {
                                            }
                                            else if (nameCol == strFecCre)
                                            {
                                            }
                                            else
                                            {
                                                if (!(dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes"))
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t\t" + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + ", \r\n\t\t\t" + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + "\r\n\t\tWHERE \r\n";
                                        esPrimerElemento = true;
                                        for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                        {
                                            nameCol = dtColumns.Rows[iFil][0].ToString();
                                            tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();

                                            if (!nameCol.Contains(strUsuCre.ToUpper()) & !nameCol.Contains(strFecCre.ToUpper()) & !nameCol.Contains(strUsuMod.ToUpper()) & !nameCol.Contains(strFecMod.ToUpper()) & !nameCol.Contains(strApiEstado.ToUpper()) & !nameCol.Contains(strApiTrans.ToUpper()))
                                            {
                                                if (dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes")
                                                {
                                                    if (esPrimerElemento)
                                                    {
                                                        Texto = Texto + "\t\t\t" + NameTabla + "." + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                        esPrimerElemento = false;
                                                    }
                                                    else
                                                    {
                                                        Texto = Texto + " AND \r\n\t\t\t" + NameTabla + "." + nameCol + " = " + strNombreSp + "." + nameCol.Replace(" ", "_");
                                                    }
                                                }
                                            }
                                        }

                                        Texto = Texto + ";\r\n";
                                        Texto = Texto + "\t\r\n";
                                        Texto = Texto + "\tEND IF;\r\n";
                                        Texto = Texto + "END;\r\n";
                                        Texto = Texto + "$BODY$\r\n";
                                        Texto = Texto + "  LANGUAGE plpgsql VOLATILE\r\n";
                                        Texto = Texto + "  COST 100;\r\n\r\n";

                                        break;
                                }
                            }
                        }
                    }
                }

                if (Principal.DBMS == TipoBD.Oracle)
                    Texto = Texto.ToUpper();

                formulario.txtPreview.Text = Texto;

                CFunciones.CrearArchivo(Texto, "Script.sql", Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\");
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}