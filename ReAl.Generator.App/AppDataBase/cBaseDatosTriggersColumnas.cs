﻿using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using ReAl.Generator.App.Properties;
using ReAlFind_Control;
using System;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ReAl.Generator.App.AppDataBase
{
    public class cBaseDatosTriggersColumnas
    {
        public string nameTabla;
        public string nameClase;
        public string tipoCol;
        public string nameCol;

        public string CrearTriggersColumnas(FPrincipal formulario)
        {
            //Elegimos el origen : Tablas o Vistas
            ReAlListView MyReAlListView = new ReAlListView();
            switch (formulario.tabBDObjects.SelectedIndex)
            {
                case 0:
                    MyReAlListView = formulario.ReAlListView1;
                    break;

                case 1:
                    MyReAlListView = formulario.ReAlListView2;
                    break;

                case 2:
                    throw new Exception("No se puede Crear Triigers y/o Columnas adicionales de los SPs");
            }

            string idTabla = null;
            string NameTablaAlias = null;
            string Creador = (string.IsNullOrEmpty(formulario.txtInfAutor.Text) ? "Automatico     " : formulario.txtInfAutor.Text);
            Creador = Creador.PadRight(15);

            StringBuilder strResultado = new StringBuilder();
            string strIdColumna = "id";
            string strOpciones = "";

            switch (Principal.DBMS)
            {
                case TipoBD.SQLServer:
                    if (formulario.chkTriggerEncrypt.Checked)
                    {
                        strOpciones = strOpciones + "WITH ENCRYPTION, ";
                    }
                    else
                    {
                        strOpciones = strOpciones + "WITH ";
                    }

                    for (int i = 0; i <= MyReAlListView.Items.Count - 1; i++)
                    {
                        if (MyReAlListView.Items[i].Checked == true)
                        {
                            idTabla = MyReAlListView.Items[i].SubItems[1].Text;
                            nameTabla = MyReAlListView.Items[i].SubItems[0].Text;
                            string nameSchema = MyReAlListView.Items[i].SubItems[2].Text;
                            NameTablaAlias = nameTabla;
                            nameClase = nameTabla[0].ToString().ToUpper() + nameTabla.Substring(1, nameTabla.Length - 1);
                            strIdColumna = nameTabla.Substring(4, nameTabla.Length - 4).ToLower();

                            if (formulario.chkBDOpcionesCols.Checked)
                            {
                                if (formulario.chkBdColEstado.Checked)
                                {
                                    strResultado.AppendLine("ALTER TABLE " + nameTabla + " ADD ApiEstado VARCHAR(60) NOT NULL ");
                                }

                                if (formulario.chkBdColTrans.Checked)
                                {
                                    strResultado.AppendLine("ALTER TABLE " + nameTabla + " ADD ApiTransaccion VARCHAR(60) NOT NULL");
                                }

                                if (formulario.chkBdColUsuCre.Checked)
                                {
                                    strResultado.AppendLine("ALTER TABLE " + nameTabla + " ADD UsuCre VARCHAR(60) NOT NULL");
                                }

                                if (formulario.chkBdColFecCre.Checked)
                                {
                                    strResultado.AppendLine("ALTER TABLE " + nameTabla + " ADD FecCre DATETIME default (GETDATE()) NOT NULL");
                                }

                                if (formulario.chkBdColUsuMod.Checked)
                                {
                                    strResultado.AppendLine("ALTER TABLE " + nameTabla + " ADD UsuMod VARCHAR(60)");
                                }

                                if (formulario.chkBdColFecMod.Checked)
                                {
                                    strResultado.AppendLine("ALTER TABLE " + nameTabla + " ADD FecMod DATETIME");
                                }
                            }

                            //Triggers "Normales"
                            if (formulario.chkBDOpcionesTriggers.Checked)
                            {
                                if (formulario.chkTriggerInsert.Checked)
                                {
                                    strResultado.Append(Resources.BD_SQL_TrBfIns.Replace("%Tabla%", nameTabla).Replace("%Autor%", Creador).Replace("%Fecha%", DateTime.Now.ToString("dd/MM/yyyy")).Replace("%Opciones%", strOpciones));
                                    strResultado.AppendLine("");
                                    strResultado.AppendLine("/*------------------------------------------------------------*/");
                                }

                                if (formulario.chkTriggerUpdate.Checked)
                                {
                                    strResultado.Append(Resources.BD_SQL_TrBfUpd.Replace("%Tabla%", nameTabla).Replace("%Autor%", Creador).Replace("%Fecha%", DateTime.Now.ToString("dd/MM/yyyy")).Replace("%Opciones%", strOpciones));
                                    strResultado.AppendLine("");
                                    strResultado.AppendLine("/*------------------------------------------------------------*/");
                                }

                                if (formulario.chkTriggerDelete.Checked)
                                {
                                    strResultado.Append(Resources.BD_SQL_TrBfDel.Replace("%Tabla%", nameTabla).Replace("%Autor%", Creador).Replace("%Fecha%", DateTime.Now.ToString("dd/MM/yyyy")).Replace("%Opciones%", strOpciones));
                                    strResultado.AppendLine("");
                                    strResultado.AppendLine("/*------------------------------------------------------------*/");
                                }
                            }

                            //Triggers Simples
                            if (formulario.chkBDOpcionesTriggersSimple.Checked)
                            {
                                if (formulario.chkTriggerUpdate.Checked)
                                {
                                    strResultado.Append(Resources.BD_SQL_TrAfUpd_Simple.Replace("%Tabla%", nameTabla).Replace("%Autor%", Creador).Replace("%Fecha%", DateTime.Now.ToString("dd/MM/yyyy")).Replace("%Opciones%", strOpciones).Replace("%Sufijo%", strIdColumna));
                                    strResultado.AppendLine("");
                                    strResultado.AppendLine("/*------------------------------------------------------------*/");
                                }
                                if (formulario.chkTriggerInsert.Checked)
                                {
                                    strResultado.Append(Resources.BD_SQL_TrAfIns_Simple.Replace("%Tabla%", nameTabla).Replace("%Autor%", Creador).Replace("%Fecha%", DateTime.Now.ToString("dd/MM/yyyy")).Replace("%Opciones%", strOpciones).Replace("%Sufijo%", strIdColumna));
                                    strResultado.AppendLine("");
                                    strResultado.AppendLine("/*------------------------------------------------------------*/");
                                }
                            }

                            //Triggers Instead OF
                            if (formulario.chkBDOpcionesTriggersInstead.Checked)
                            {
                                string strColumnasIO = "";
                                string strAsignarColumnasIO = "";
                                string strQueryIO = "";
                                bool esPrimerElemento = false;
                                string longitudCol = "";
                                string strLongitud = "";
                                DataTable dtColumns = default(DataTable);
                                switch (formulario.cmbTipoColumnas.SelectedIndex)
                                {
                                    case 0:
                                        dtColumns = CTablasColumnas.ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.PrimeraMayuscula);
                                        break;

                                    case 1:
                                        dtColumns = CTablasColumnas.ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.Minusculas);
                                        break;

                                    case 2:
                                        dtColumns = CTablasColumnas.ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.Mayusculas);
                                        break;
                                }

                                if (formulario.chkTriggerInsert.Checked)
                                {
                                    //Asignamos los valores de las Columnas
                                    foreach (DataRow row in dtColumns.Rows)
                                    {
                                        tipoCol = row["TIPO_COL"].ToString().ToUpper();
                                        if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("api") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("estado"))
                                        {
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("api") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("transaccion"))
                                        {
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("usu") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("cre"))
                                        {
                                            //'strAsignarColumnasIO = strAsignarColumnasIO & vbTab & vbTab & vbTab & " " & "@vlUsuCre = " & row["name"] & "," & vbCrLf
                                            strAsignarColumnasIO = strAsignarColumnasIO + "\t\t\t @vlUsuCre = CURRENT_USER,\r\n";
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("fec") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("cre"))
                                        {
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("usu") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("mod"))
                                        {
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("fec") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("mod"))
                                        {
                                        }
                                        else
                                        {
                                            strAsignarColumnasIO = strAsignarColumnasIO + "\t\t\t @vl" + row["name"] + " = " + row["name"] + ",\r\n";
                                        }
                                    }
                                    if (strAsignarColumnasIO.Trim().EndsWith(","))
                                        strAsignarColumnasIO = strAsignarColumnasIO.Trim().Substring(0, strAsignarColumnasIO.Trim().Length - 1);

                                    strColumnasIO = "";
                                    //Obtenemos las columnas y sus tipos de dato
                                    foreach (DataRow row in dtColumns.Rows)
                                    {
                                        tipoCol = row["TIPO_COL"].ToString().ToUpper();
                                        longitudCol = row["LONGITUD"].ToString();
                                        switch (tipoCol)
                                        {
                                            case "INT":
                                                strLongitud = "";
                                                break;

                                            case "BIGINT":
                                                strLongitud = "";
                                                break;

                                            case "IMAGE":
                                                strLongitud = "";
                                                break;

                                            case "BIT":
                                                strLongitud = "";
                                                break;

                                            case "DATE":
                                                strLongitud = "";
                                                break;

                                            case "DATETIME":
                                                strLongitud = "";
                                                break;

                                            case "SMALLINT":
                                                strLongitud = "";
                                                break;

                                            case "SMALLDATETIME":
                                                strLongitud = "";
                                                break;

                                            default:
                                                strLongitud = "(" + longitudCol + ")";
                                                break;
                                        }
                                        if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("api") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("estado"))
                                        {
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("api") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("transaccion"))
                                        {
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("usu") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("cre"))
                                        {
                                            strColumnasIO = strColumnasIO + "\t  @vlUsuCre\t" + tipoCol + strLongitud + ",\r\n";
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("fec") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("cre"))
                                        {
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("usu") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("mod"))
                                        {
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("fec") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("mod"))
                                        {
                                        }
                                        else
                                        {
                                            strColumnasIO = strColumnasIO + "\t  @vl" + row["name"] + "\t" + tipoCol + strLongitud + ",\r\n";
                                        }
                                    }
                                    if (strColumnasIO.Trim().EndsWith(","))
                                        strColumnasIO = strColumnasIO.Trim().Substring(0, strColumnasIO.Trim().Length - 1);

                                    strQueryIO = "\tINSERT INTO [" + nameTabla + "] (\r\n";
                                    esPrimerElemento = true;
                                    for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                    {
                                        nameCol = dtColumns.Rows[iFil][0].ToString();

                                        if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("api") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("estado"))
                                        {
                                            if (esPrimerElemento)
                                            {
                                                strQueryIO = strQueryIO + "\t\t\t\t\t\t[" + nameCol + "]";
                                                esPrimerElemento = false;
                                            }
                                            else
                                            {
                                                strQueryIO = strQueryIO + ", \r\n\t\t\t\t\t\t[" + nameCol + "]";
                                            }
                                        }
                                        else if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("api") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("transaccion"))
                                        {
                                            if (esPrimerElemento)
                                            {
                                                strQueryIO = strQueryIO + "\t\t\t\t\t\t[" + nameCol + "]";
                                                esPrimerElemento = false;
                                            }
                                            else
                                            {
                                                strQueryIO = strQueryIO + ", \r\n\t\t\t\t\t\t[" + nameCol + "]";
                                            }
                                        }
                                        else if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("usu") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("cre"))
                                        {
                                            if (esPrimerElemento)
                                            {
                                                strQueryIO = strQueryIO + "\t\t\t\t\t\t[" + nameCol + "]";
                                                esPrimerElemento = false;
                                            }
                                            else
                                            {
                                                strQueryIO = strQueryIO + ", \r\n\t\t\t\t\t\t[" + nameCol + "]";
                                            }
                                        }
                                        else if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("fec") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("cre"))
                                        {
                                            if (esPrimerElemento)
                                            {
                                                strQueryIO = strQueryIO + "\t\t\t\t\t\t[" + nameCol + "]";
                                                esPrimerElemento = false;
                                            }
                                            else
                                            {
                                                strQueryIO = strQueryIO + ", \r\n\t\t\t\t\t\t[" + nameCol + "]";
                                            }
                                        }
                                        else if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("usu") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("mod"))
                                        {
                                        }
                                        else if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("fec") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("mod"))
                                        {
                                        }
                                        else
                                        {
                                            if (dtColumns.Rows[0]["EsIdentidad"] == "No")
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    strQueryIO = strQueryIO + "\t\t\t\t\t\t[" + nameCol + "]";
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    strQueryIO = strQueryIO + ", \r\n\t\t\t\t\t\t[" + nameCol + "]";
                                                }
                                            }
                                        }
                                    }

                                    strQueryIO = strQueryIO + "\r\n\t) VALUES (\r\n";
                                    esPrimerElemento = true;
                                    for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                    {
                                        nameCol = dtColumns.Rows[iFil][0].ToString();
                                        if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("api") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("estado"))
                                        {
                                            if (esPrimerElemento)
                                            {
                                                strQueryIO = strQueryIO + "\t\t\t\t\t\t@vlEstadoFin";
                                                esPrimerElemento = false;
                                            }
                                            else
                                            {
                                                strQueryIO = strQueryIO + ", \r\n\t\t\t\t\t\t@vlEstadoFin";
                                            }
                                        }
                                        else if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("api") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("transaccion"))
                                        {
                                            if (esPrimerElemento)
                                            {
                                                strQueryIO = strQueryIO + "\t\t\t\t\t\t@vlTransaccion";
                                                esPrimerElemento = false;
                                            }
                                            else
                                            {
                                                strQueryIO = strQueryIO + ", \r\n\t\t\t\t\t\t@vlTransaccion";
                                            }
                                        }
                                        else if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("usu") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("cre"))
                                        {
                                            if (esPrimerElemento)
                                            {
                                                strQueryIO = strQueryIO + "\t\t\t\t\t\t@vlUsuCre";
                                                esPrimerElemento = false;
                                            }
                                            else
                                            {
                                                strQueryIO = strQueryIO + ", \r\n\t\t\t\t\t\t@vlUsuCre";
                                            }
                                        }
                                        else if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("fec") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("cre"))
                                        {
                                            if (esPrimerElemento)
                                            {
                                                strQueryIO = strQueryIO + "\t\t\t\t\t\tGETDATE()";
                                                esPrimerElemento = false;
                                            }
                                            else
                                            {
                                                strQueryIO = strQueryIO + ", \r\n\t\t\t\t\t\tGETDATE()";
                                            }
                                        }
                                        else if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("usu") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("mod"))
                                        {
                                        }
                                        else if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("fec") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("mod"))
                                        {
                                        }
                                        else
                                        {
                                            if (dtColumns.Rows[0]["EsIdentidad"] == "No")
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    strQueryIO = strQueryIO + "\t\t\t\t\t\t@vl" + dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    strQueryIO = strQueryIO + ", \r\n\t\t\t\t\t\t@vl" + dtColumns.Rows[iFil][0].ToString().Replace(" ", "_");
                                                }
                                            }
                                        }
                                    }

                                    strQueryIO = strQueryIO + "\r\n\t) \r\n";

                                    strResultado.Append(Resources.BD_SQL_TrIoIns_V2.Replace("%Tabla%", nameTabla).Replace("%Autor%", Creador).Replace("%Fecha%", DateTime.Now.ToString("dd/MM/yyyy")).Replace("%Opciones%", strOpciones).Replace("%Columnas%", strColumnasIO).Replace("%AsignarColumnas%", strAsignarColumnasIO).Replace("%Query%", strQueryIO));
                                    strResultado.AppendLine("");
                                    strResultado.AppendLine("/*------------------------------------------------------------*/");
                                }

                                if (formulario.chkTriggerUpdate.Checked)
                                {
                                    strAsignarColumnasIO = "";
                                    //Asignamos los valores de las Columnas
                                    foreach (DataRow row in dtColumns.Rows)
                                    {
                                        tipoCol = row["TIPO_COL"].ToString().ToUpper();
                                        if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("api") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("estado"))
                                        {
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("api") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("transaccion"))
                                        {
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("usu") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("cre"))
                                        {
                                            strAsignarColumnasIO = strAsignarColumnasIO + "\t\t\t @vlUsuCre = " + row["name"] + ",\r\n";
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("fec") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("cre"))
                                        {
                                            strAsignarColumnasIO = strAsignarColumnasIO + "\t\t\t @vlFecCre = " + row["name"] + ",\r\n";
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("usu") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("mod"))
                                        {
                                            strAsignarColumnasIO = strAsignarColumnasIO + "\t\t\t @vlUsuMod = CURRENT_USER,\r\n";
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("fec") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("mod"))
                                        {
                                            strAsignarColumnasIO = strAsignarColumnasIO + "\t\t\t @vlFecMod = " + row["name"] + ",\r\n";
                                        }
                                        else
                                        {
                                            strAsignarColumnasIO = strAsignarColumnasIO + "\t\t\t @vl" + row["name"] + " = " + row["name"] + ",\r\n";
                                        }
                                    }
                                    if (strAsignarColumnasIO.Trim().EndsWith(","))
                                        strAsignarColumnasIO = strAsignarColumnasIO.Trim().Substring(0, strAsignarColumnasIO.Trim().Length - 1);

                                    strColumnasIO = "";
                                    //Obtenemos las columnas y sus tipos de dato
                                    foreach (DataRow row in dtColumns.Rows)
                                    {
                                        tipoCol = row["TIPO_COL"].ToString().ToUpper();
                                        longitudCol = row["LONGITUD"].ToString();
                                        switch (tipoCol)
                                        {
                                            case "INT":
                                                strLongitud = "";
                                                break;

                                            case "BIGINT":
                                                strLongitud = "";
                                                break;

                                            case "IMAGE":
                                                strLongitud = "";
                                                break;

                                            case "BIT":
                                                strLongitud = "";
                                                break;

                                            case "DATE":
                                                strLongitud = "";
                                                break;

                                            case "DATETIME":
                                                strLongitud = "";
                                                break;

                                            case "SMALLINT":
                                                strLongitud = "";
                                                break;

                                            case "SMALLDATETIME":
                                                strLongitud = "";
                                                break;

                                            default:
                                                strLongitud = "(" + longitudCol + ")";
                                                break;
                                        }
                                        if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("api") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("estado"))
                                        {
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("api") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("transaccion"))
                                        {
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("usu") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("cre"))
                                        {
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("fec") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("cre"))
                                        {
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("usu") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("mod"))
                                        {
                                        }
                                        else if (row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("fec") & row["name"].ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("mod"))
                                        {
                                        }
                                        else
                                        {
                                            strColumnasIO = strColumnasIO + "\t  @vl" + row["name"] + "\t" + tipoCol + strLongitud + ",\r\n";
                                        }
                                    }
                                    if (strColumnasIO.Trim().EndsWith(","))
                                        strColumnasIO = strColumnasIO.Trim().Substring(0, strColumnasIO.Trim().Length - 1);

                                    strQueryIO = "\tUPDATE [" + nameTabla + "] SET\r\n";

                                    esPrimerElemento = true;
                                    for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                    {
                                        nameCol = dtColumns.Rows[iFil][0].ToString();
                                        tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString().ToUpper();

                                        if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("api") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("estado"))
                                        {
                                            if (dtColumns.Rows[iFil]["EsPK"].ToString() == "No")
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    strQueryIO = strQueryIO + "\t\t\t\t[" + nameCol + "] = @vlEstadoFin";
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    strQueryIO = strQueryIO + ", \r\n\t\t\t\t[" + nameCol + "] = @vlEstadoFin";
                                                }
                                            }
                                        }
                                        else if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("api") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("transaccion"))
                                        {
                                            if (dtColumns.Rows[iFil]["EsPK"].ToString() == "No")
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    strQueryIO = strQueryIO + "\t\t\t\t[" + nameCol + "] = @vlTransaccion";
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    strQueryIO = strQueryIO + ", \r\n\t\t[" + nameCol + "] = @vlTransaccion";
                                                }
                                            }
                                        }
                                        else if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("usu") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("cre"))
                                        {
                                        }
                                        else if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("fec") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("cre"))
                                        {
                                        }
                                        else if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("usu") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("mod"))
                                        {
                                            if (dtColumns.Rows[iFil]["EsPK"].ToString() == "No")
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    strQueryIO = strQueryIO + "\t\t\t\t[" + nameCol + "] = @vlUsuMod";
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    strQueryIO = strQueryIO + ", \r\n\t\t\t\t[" + nameCol + "] = @vlUsuMod";
                                                }
                                            }
                                        }
                                        else if (nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("fec") & nameCol.ToString().ToLower().Replace(nameTabla.ToLower(), "").Contains("mod"))
                                        {
                                            if (dtColumns.Rows[iFil]["EsPK"].ToString() == "No")
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    strQueryIO = strQueryIO + "\t\t\t\t[" + nameCol + "] = GETDATE()";
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    strQueryIO = strQueryIO + ", \r\n\t\t\t\t[" + nameCol + "] = GETDATE()";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (dtColumns.Rows[iFil]["EsPK"].ToString() == "No")
                                            {
                                                if (esPrimerElemento)
                                                {
                                                    strQueryIO = strQueryIO + "\t\t\t\t[" + nameCol + "] = @vl" + nameCol.Replace(" ", "_");
                                                    esPrimerElemento = false;
                                                }
                                                else
                                                {
                                                    strQueryIO = strQueryIO + ", \r\n\t\t\t\t[" + nameCol + "] = @vl" + nameCol.Replace(" ", "_");
                                                }
                                            }
                                        }
                                    }

                                    strQueryIO = strQueryIO + "\r\n\tWHERE \r\n";
                                    esPrimerElemento = true;
                                    for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                    {
                                        nameCol = dtColumns.Rows[iFil][0].ToString();
                                        tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();
                                        if (dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes")
                                        {
                                            if (esPrimerElemento)
                                            {
                                                strQueryIO = strQueryIO + "\t\t\t\t[" + nameCol + "] = @vl" + nameCol.Replace(" ", "_");
                                                esPrimerElemento = false;
                                            }
                                            else
                                            {
                                                strQueryIO = strQueryIO + " AND \r\n\t\t\t\t[" + nameCol + "] = @vl" + nameCol.Replace(" ", "_");
                                            }
                                        }
                                    }

                                    strResultado.Append(Resources.BD_SQL_TrIoUpd_V2.Replace("%Tabla%", nameTabla).Replace("%Autor%", Creador).Replace("%Fecha%", DateTime.Now.ToString("dd/MM/yyyy")).Replace("%Opciones%", strOpciones).Replace("%Columnas%", strColumnasIO).Replace("%AsignarColumnas%", strAsignarColumnasIO).Replace("%Query%", strQueryIO));
                                    strResultado.AppendLine("");
                                    strResultado.AppendLine("/*------------------------------------------------------------*/");
                                }

                                if (formulario.chkTriggerDelete.Checked)
                                {
                                    strAsignarColumnasIO = "";
                                    //Asignamos los valores de las Columnas
                                    foreach (DataRow row in dtColumns.Rows)
                                    {
                                        tipoCol = row["TIPO_COL"].ToString().ToUpper();
                                        if (row["EsPK"].ToString() == "Yes")
                                        {
                                            strAsignarColumnasIO = strAsignarColumnasIO + "\t\t\t @vl" + row["name"] + " = " + row["name"] + ",\r\n";
                                        }
                                    }
                                    if (strAsignarColumnasIO.Trim().EndsWith(","))
                                        strAsignarColumnasIO = strAsignarColumnasIO.Trim().Substring(0, strAsignarColumnasIO.Trim().Length - 1);

                                    strColumnasIO = "";
                                    //Obtenemos las columnas y sus tipos de dato
                                    foreach (DataRow row in dtColumns.Rows)
                                    {
                                        tipoCol = row["TIPO_COL"].ToString().ToUpper();
                                        longitudCol = row["LONGITUD"].ToString();
                                        switch (tipoCol)
                                        {
                                            case "INT":
                                                strLongitud = "";
                                                break;

                                            case "BIGINT":
                                                strLongitud = "";
                                                break;

                                            case "IMAGE":
                                                strLongitud = "";
                                                break;

                                            case "BIT":
                                                strLongitud = "";
                                                break;

                                            case "DATE":
                                                strLongitud = "";
                                                break;

                                            case "DATETIME":
                                                strLongitud = "";
                                                break;

                                            case "SMALLINT":
                                                strLongitud = "";
                                                break;

                                            case "SMALLDATETIME":
                                                strLongitud = "";
                                                break;

                                            default:
                                                strLongitud = "(" + longitudCol + ")";
                                                break;
                                        }
                                        if (row["EsPK"].ToString() == "Yes")
                                        {
                                            strColumnasIO = strColumnasIO + "\t  @vl" + row["name"] + "\t" + tipoCol + strLongitud + ",\r\n";
                                        }
                                    }
                                    if (strColumnasIO.Trim().EndsWith(","))
                                        strColumnasIO = strColumnasIO.Trim().Substring(0, strColumnasIO.Trim().Length - 1);

                                    strQueryIO = "\tDELETE " + nameTabla + "\r\n";
                                    strQueryIO = strQueryIO + "\tWHERE \r\n";

                                    esPrimerElemento = true;
                                    for (int iFil = 0; iFil <= dtColumns.Rows.Count - 1; iFil++)
                                    {
                                        nameCol = dtColumns.Rows[iFil][0].ToString();
                                        tipoCol = dtColumns.Rows[iFil]["TIPO_COL"].ToString();
                                        if (dtColumns.Rows[iFil]["EsPK"].ToString() == "Yes")
                                        {
                                            if (esPrimerElemento)
                                            {
                                                strQueryIO = strQueryIO + "\t\t\t\t[" + nameCol + "] = @vl" + nameCol.Replace(" ", "_");
                                                esPrimerElemento = false;
                                            }
                                            else
                                            {
                                                strQueryIO = strQueryIO + " AND \r\n\t\t\t\t[" + nameCol + "] = @vl" + nameCol.Replace(" ", "_");
                                            }
                                        }
                                    }

                                    strResultado.Append(Resources.BD_SQL_TrIoDel_V2.Replace("%Tabla%", nameTabla).Replace("%Autor%", Creador).Replace("%Fecha%", DateTime.Now.ToString("dd/MM/yyyy")).Replace("%Opciones%", strOpciones).Replace("%Columnas%", strColumnasIO).Replace("%AsignarColumnas%", strAsignarColumnasIO).Replace("%Query%", strQueryIO));
                                    strResultado.AppendLine("");
                                    strResultado.AppendLine("/*------------------------------------------------------------*/");
                                }
                            }
                        }
                    }

                    break;

                case TipoBD.Oracle:
                    MessageBox.Show("Funcion no implementada");

                    break;

                case TipoBD.PostgreSQL:
                    string strApiEstado = null;
                    string strApiTrans = null;
                    string strUsuCre = null;
                    string strFecCre = null;
                    string strUsuMod = null;
                    string strFecMod = null;

                    for (int i = 0; i <= MyReAlListView.Items.Count - 1; i++)
                    {
                        if (MyReAlListView.Items[i].Checked == true)
                        {
                            idTabla = MyReAlListView.Items[i].SubItems[1].Text;
                            nameTabla = MyReAlListView.Items[i].SubItems[0].Text;
                            NameTablaAlias = nameTabla;
                            nameClase = nameTabla[0].ToString().ToUpper() + nameTabla.Substring(1, nameTabla.Length - 1);
                            string nameSchema = MyReAlListView.Items[i].SubItems[2].Text;

                            if (formulario.chkBDOpcionesCols.Checked)
                            {
                                if (formulario.chkBdColEstado.Checked)
                                {
                                    strResultado.AppendLine("ALTER TABLE " + nameTabla + " ADD apiestado CHARACTER VARYING(60) NOT NULL DEFAULT 'ELABORADO'::character varying;");
                                }

                                if (formulario.chkBdColTrans.Checked)
                                {
                                    strResultado.AppendLine("ALTER TABLE " + nameTabla + " ADD apitransaccion CHARACTER VARYING(60) NOT NULL DEFAULT 'CREAR'::character varying;");
                                }

                                if (formulario.chkBdColUsuCre.Checked)
                                {
                                    strResultado.AppendLine("ALTER TABLE " + nameTabla + " ADD usucre CHARACTER VARYING(60) NOT NULL DEFAULT \"current_user\"();");
                                }

                                if (formulario.chkBdColFecCre.Checked)
                                {
                                    strResultado.AppendLine("ALTER TABLE " + nameTabla + " ADD feccre timestamp without time zone NOT NULL DEFAULT now();");
                                }

                                if (formulario.chkBdColUsuMod.Checked)
                                {
                                    strResultado.AppendLine("ALTER TABLE " + nameTabla + " ADD usumod CHARACTER VARYING(60);");
                                }

                                if (formulario.chkBdColFecMod.Checked)
                                {
                                    strResultado.AppendLine("ALTER TABLE " + nameTabla + " ADD fecmod timestamp without time ZONE;");
                                }
                            }

                            //Ahora obtenemos las columnas
                            DataTable dtColumns = default(DataTable);
                            switch (formulario.cmbTipoColumnas.SelectedIndex)
                            {
                                case 0:
                                    dtColumns = CTablasColumnas.ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.PrimeraMayuscula);
                                    break;

                                case 1:
                                    dtColumns = CTablasColumnas.ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.Minusculas);
                                    break;

                                case 2:
                                    dtColumns = CTablasColumnas.ObtenerListadoColumnas(nameSchema, idTabla, Languaje.CSharp, TipoColumnas.Mayusculas);
                                    break;
                            }

                            foreach (DataRow row in dtColumns.Rows)
                            {
                                if (row["name"].ToString().ToUpper().Contains("ESTADO") & row["name"].ToString().ToUpper().Contains("API"))
                                {
                                    strApiEstado = row["name"].ToString();
                                }
                                if (row["name"].ToString().ToUpper().Contains("TRANSACCION") & row["name"].ToString().ToUpper().Contains("API"))
                                {
                                    strApiTrans = row["name"].ToString();
                                }
                                if (row["name"].ToString().ToUpper().Contains("USU") & row["name"].ToString().ToUpper().Contains("CRE"))
                                {
                                    strUsuCre = row["name"].ToString();
                                }
                                if (row["name"].ToString().ToUpper().Contains("FEC") & row["name"].ToString().ToUpper().Contains("CRE"))
                                {
                                    strFecCre = row["name"].ToString();
                                }
                                if (row["name"].ToString().ToUpper().Contains("USU") & row["name"].ToString().ToUpper().Contains("MOD"))
                                {
                                    strUsuMod = row["name"].ToString();
                                }
                                if (row["name"].ToString().ToUpper().Contains("FEC") & row["name"].ToString().ToUpper().Contains("MOD"))
                                {
                                    strFecMod = row["name"].ToString();
                                }
                            }

                            //Ahora las transacciones
                            string strRdnIns = "";
                            string strRdnUpd = "";
                            string strRdnDel = "";
                            try
                            {
                                //Obtenemos los ciclos de vida
                                string strQuery = "SELECT * FROM segtransacciones WHERE UPPER(tablasta) = UPPER('" + nameTabla + "')  ";
                                DataTable dtRdn = new DataTable();
                                dtRdn = CFuncionesBDs.CargarDataTable(strQuery);

                                if (dtRdn.Rows.Count > 0)
                                {
                                    foreach (DataRow row in dtRdn.Rows)
                                    {
                                        if (row["sentenciastr"].ToString() == "INSERT")
                                        {
                                            strRdnIns = strRdnIns + "  IF NEW." + strApiTrans + " = '" + row["transaccionstr"] + "' THEN\r\n\r\n  END IF;\r\n\r\n";
                                        }
                                        else if (row["sentenciastr"].ToString() == "UPDATE")
                                        {
                                            strRdnUpd = strRdnUpd + "  IF NEW." + strApiTrans + " = '" + row["transaccionstr"] + "' THEN\r\n\r\n  END IF;\r\n\r\n";
                                        }
                                        else
                                        {
                                            strRdnDel = strRdnDel + "  IF NEW." + strApiTrans + " = '" + row["transaccionstr"] + "' THEN\r\n\r\n  END IF;\r\n\r\n";
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                            }

                            //Ahora los triggers
                            if (formulario.chkBDOpcionesTriggersSimple.Checked)
                            {
                                if (formulario.chkTriggerInsert.Checked)
                                {
                                    strResultado.Append(Resources.BD_PG_TrBfIns.Replace("%TABLA%", nameTabla).Replace("%Autor%", Creador).Replace("%Fecha%", DateTime.Now.ToString("dd/MM/yyyy")).Replace("%APIESTADO%", strApiEstado).Replace("%APITRANS%", strApiTrans).Replace("%USUCRE%", strUsuCre).Replace("%FECCRE%", strFecCre).Replace("%TRANSACCIONES%", strRdnIns));
                                    strResultado.AppendLine("");
                                    strResultado.AppendLine("/*------------------------------------------------------------*/");
                                }
                                if (formulario.chkTriggerUpdate.Checked)
                                {
                                    strResultado.Append(Resources.BD_PG_TrBfUpd.Replace("%TABLA%", nameTabla).Replace("%Autor%", Creador).Replace("%Fecha%", DateTime.Now.ToString("dd/MM/yyyy")).Replace("%APIESTADO%", strApiEstado).Replace("%APITRANS%", strApiTrans).Replace("%USUMOD%", strUsuMod).Replace("%FECMOD%", strFecMod).Replace("%USUCRE%", strUsuCre).Replace("%FECCRE%", strFecCre).Replace("%TRANSACCIONES%", strRdnUpd));
                                    strResultado.AppendLine("");
                                    strResultado.AppendLine("/*------------------------------------------------------------*/");
                                }
                                if (formulario.chkTriggerDelete.Checked)
                                {
                                    strResultado.Append(Resources.BD_PG_TrBfDel.Replace("%TABLA%", nameTabla).Replace("%Autor%", Creador).Replace("%Fecha%", DateTime.Now.ToString("dd/MM/yyyy")).Replace("%APIESTADO%", strApiEstado).Replace("%APITRANS%", strApiTrans).Replace("%TRANSACCIONES%", strRdnDel));
                                    strResultado.AppendLine("");
                                    strResultado.AppendLine("/*------------------------------------------------------------*/");
                                }
                            }

                            if (formulario.chkBDOpcionesTriggersInstead.Checked)
                            {
                                if (formulario.chkTriggerInsert.Checked)
                                {
                                    strResultado.Append(Resources.BD_TrBfIns_Pg_Simple.Replace("%TABLA%", nameTabla).Replace("%Autor%", Creador).Replace("%Fecha%", DateTime.Now.ToString("dd/MM/yyyy")).Replace("%APIESTADO%", strApiEstado).Replace("%APITRANS%", strApiTrans).Replace("%USUCRE%", strUsuCre).Replace("%FECCRE%", strFecCre).Replace("%TRANSACCIONES%", strRdnIns));
                                    strResultado.AppendLine("");
                                    strResultado.AppendLine("/*------------------------------------------------------------*/");
                                }
                                if (formulario.chkTriggerUpdate.Checked)
                                {
                                    strResultado.Append(Resources.BD_TrBfUpd_Pg_Simple.Replace("%TABLA%", nameTabla).Replace("%Autor%", Creador).Replace("%Fecha%", DateTime.Now.ToString("dd/MM/yyyy")).Replace("%APIESTADO%", strApiEstado).Replace("%APITRANS%", strApiTrans).Replace("%USUMOD%", strUsuMod).Replace("%FECMOD%", strFecMod).Replace("%USUCRE%", strUsuCre).Replace("%FECCRE%", strFecCre).Replace("%TRANSACCIONES%", strRdnUpd));
                                    strResultado.AppendLine("");
                                    strResultado.AppendLine("/*------------------------------------------------------------*/");
                                }
                            }
                        }
                    }

                    break;
            }

            return strResultado.ToString();
        }
    }
}