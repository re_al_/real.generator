using System;
using System.Data;
using System.Windows.Forms;
using %PROYECTO_CLASS_MODELO%;
using %PROYECTO_CLASS_ENTIDADES%;
using %PROYECTO_CLASS%;

namespace %NAMESPACE%
{
    public partial class %NAME_FORM% : DevExpress.XtraEditors.XtraForm
    {
        private %OBJETO% _myObj = null;

        public %NAME_FORM%(%OBJETO% obj)
        {
            InitializeComponent();

            _myObj = obj;
        }

        public %NAME_FORM%()
        {
            InitializeComponent();
        }

        private void %NAME_FORM%_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            %FUNCIONES_EXTRAS%

            %LABEL_TEXT%
            CargarListado();

            if (_myObj != null)
                CargarDatosEntrar();
        }

        private void CargarDatosEntrar()
        {
            try
            {
                %ASIGNAR_VALORES%
                btnAceptar.Text = "&Guardar Cambios";
                btnCancelar.Text = "&Cancelar";
                this.CancelButton = btnCancelar;
            }
            catch (Exception exp)
            {
                FErrores frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void CargarListado()
        {
            %MODELO_OBJETO% rn = new %MODELO_OBJETO%();
            gc%NAME_TABLA%.DataSource = rn.ObtenerDataTable();

            gv%NAME_TABLA%.OptionsBehavior.Editable = false;
            gv%NAME_TABLA%.OptionsBehavior.ReadOnly = true;
            //gv%NAME_TABLA%.ShowFindPanel();

            gv%NAME_TABLA%.Columns[%OBJETO%.Fields.usucre.ToString()].Visible = false;
            gv%NAME_TABLA%.Columns[%OBJETO%.Fields.feccre.ToString()].Visible = false;
            gv%NAME_TABLA%.Columns[%OBJETO%.Fields.usumod.ToString()].Visible = false;
            gv%NAME_TABLA%.Columns[%OBJETO%.Fields.fecmod.ToString()].Visible = false;
            gv%NAME_TABLA%.Columns[%OBJETO%.Fields.apiestado.ToString()].Visible = false;
            gv%NAME_TABLA%.Columns[%OBJETO%.Fields.apitransaccion.ToString()].Visible = false;
        }

        %FUNCIONES_EXTRAS_DESP%

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            %MODELO_OBJETO% rn = new %MODELO_OBJETO%();
            Boolean bProcede = false;
            
            try
            {
                if (_myObj == null)
                {
                    //Insertamos el registro
                    %OBJETO% obj = new %OBJETO%();
			        %OBJETO_INSERT%
                    
                    bProcede = true;
                }
                else
                {
                    //Actualizamos el Registro
                    %OBJETO% objExistente = rn.ObtenerObjeto(%MYOBJ_PK%);

                    if (objExistente != null)
                    {
                        %OBJETO_UPDATE_CONTROLS%
                        objExistente.%API_TRANS% = CApi.Transaccion.MODIFICAR.ToString();
			            objExistente.%USU_MOD% = cParametrosApp.AppUsuario.login;
                        rn.Update(objExistente);

                        bProcede = true;
                    }
                }
            }
            catch (Exception exp)
            {
                FErrores frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }

            if (bProcede)
            {
                this.CargarListado();
                this.DialogResult = DialogResult.OK;
                //this.Close();
            }
        }
        
        private void gc%NAME_TABLA%_KeyDown(object sender, KeyEventArgs e)
        {
            %VALIDACION%
            if (e.KeyCode == Keys.Delete)
            {
                %MODELO_OBJETO% rn = new %MODELO_OBJETO%();

                //Apropiamos los valores del Grid
                %OBJETO_DELETE%

                //Obtenemos el objeto
                %OBJETO% obj = rn.ObtenerObjeto(%STRING_PK%);

                if (obj != null)
                {
                    rn.Delete(obj);
                    this.CargarListado();
                }
            }
        }

        private void gv%NAME_TABLA%_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            %MODELO_OBJETO% rn = new %MODELO_OBJETO%();

            //Apropiamos los valores del Grid
            %OBJETO_DELETE%

            %OBJETO% objExistente = rn.ObtenerObjeto(%STRING_PK%);

            if (objExistente != null)
            {
                %OBJETO_UPDATE%
                objExistente.%API_TRANS% = CApi.Transaccion.MODIFICAR.ToString();
			    objExistente.%USU_MOD% = cParametrosApp.AppUsuario.login;
			    
                //Actualizamos el registro
                rn.Update(objExistente);
                this.CargarListado();
            }
        }

        private void gv%NAME_TABLA%_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            //gv%NAME_TABLA%.SetFocusedRowCellValue(%OBJETO%.Fields.usucre.ToString(),"postgres");
        }

        private void gc%NAME_TABLA%_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            %VALIDACION%
            %MODELO_OBJETO% rn = new %MODELO_OBJETO%();

            //Apropiamos los valores del Grid
            %OBJETO_DELETE%

            //Obtenemos el objeto
            %OBJETO% obj = rn.ObtenerObjeto(%STRING_PK%);

            if (obj != null)
            {
                //f%NAME_TABLA% frm = new f%NAME_TABLA%(obj);
                //if (frm.ShowDialog() == DialogResult.OK)
                //    CargarListado();
            }
        }
    }
}