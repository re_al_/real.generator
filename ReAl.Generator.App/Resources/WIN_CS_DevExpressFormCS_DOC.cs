using System;
using System.Data;
using System.Windows.Forms;
using %PROYECTO_CLASS_MODELO%;
using %PROYECTO_CLASS_ENTIDADES%;
using %PROYECTO_CLASS%;

namespace %NAMESPACE%
{
    public partial class %NAME_FORM% : DevExpress.XtraEditors.XtraForm
    {
        private %OBJETO% _myObj = null;
		private Boolean bProcede = true;

        public %NAME_FORM%(%OBJETO% obj)
        {
            InitializeComponent();

            _myObj = obj;
        }

        public %NAME_FORM%()
        {
            InitializeComponent();
        }

        private void %NAME_FORM%_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            %FUNCIONES_EXTRAS%

            %LABEL_TEXT%

            if (_myObj != null)
                CargarDatosEntrar();
        }

        private void CargarDatosEntrar()
        {
            try
            {
                %ASIGNAR_VALORES%
                btnAceptar.Text = "&Guardar Cambios";
                btnCancelar.Text = "&Cancelar";
                this.CancelButton = btnCancelar;
            }
            catch (Exception exp)
            {
                FErrores frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }
        

        %FUNCIONES_EXTRAS_DESP%

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            %MODELO_OBJETO% rn = new %MODELO_OBJETO%();
			this.Cursor = Cursors.WaitCursor;
            btnAceptar.Enabled = false;
            bProcede = false;
            
            try
            {
                if (_myObj == null)
                {
                    //Insertamos el registro
                    %OBJETO% obj = new %OBJETO%();
			        %OBJETO_INSERT%
					objExistente.%USU_CRE% = cParametrosApp.AppUsuario.login;
					rn.Insert(objExistente);
                    
                    bProcede = true;
                }
                else
                {
                    //Actualizamos el Registro
                    %OBJETO% objExistente = rn.ObtenerObjeto(%MYOBJ_PK%);

                    if (objExistente != null)
                    {
                        %OBJETO_UPDATE_CONTROLS%
                        objExistente.%API_TRANS% = CApi.Transaccion.MODIFICAR.ToString();
			            objExistente.%USU_MOD% = cParametrosApp.AppUsuario.login;
                        rn.Update(objExistente);

                        bProcede = true;
                    }
                }
				
				this.Cursor = Cursors.Default;
                btnAceptar.Enabled = true;
            }
            catch (Exception exp)
            {
				this.Cursor = Cursors.Default;
                btnAceptar.Enabled = true;
                FErrores frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }

            if (bProcede)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
		
		private void %NAME_FORM%_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!bProcede)
            {
                e.Cancel = true;
                bProcede = true;
            }
        }
    }
}