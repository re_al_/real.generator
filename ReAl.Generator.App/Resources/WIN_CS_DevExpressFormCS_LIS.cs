using System;
using System.Data;
using System.Windows.Forms;
using %PROYECTO_CLASS_MODELO%;
using %PROYECTO_CLASS_ENTIDADES%;
using %PROYECTO_CLASS%;

namespace %NAMESPACE%
{
    public partial class %NAME_FORM% : DevExpress.XtraEditors.XtraForm
    {
        private %OBJETO% _myObj = null;

        public %NAME_FORM%(%OBJETO% obj)
        {
            InitializeComponent();

            _myObj = obj;
        }

        public %NAME_FORM%()
        {
            InitializeComponent();
        }

        private void %NAME_FORM%_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            %LABEL_TEXT%
			
            CargarListado();
        }

        public void CargarListado()
        {
            %MODELO_OBJETO% rn = new %MODELO_OBJETO%();
            gc%NAME_TABLA%.DataSource = rn.ObtenerDataTable();

            gv%NAME_TABLA%.OptionsBehavior.Editable = false;
            gv%NAME_TABLA%.OptionsBehavior.ReadOnly = true;
            //gv%NAME_TABLA%.ShowFindPanel();

            gv%NAME_TABLA%.Columns[%OBJETO%.Fields.usucre.ToString()].Visible = false;
            gv%NAME_TABLA%.Columns[%OBJETO%.Fields.feccre.ToString()].Visible = false;
            gv%NAME_TABLA%.Columns[%OBJETO%.Fields.usumod.ToString()].Visible = false;
            gv%NAME_TABLA%.Columns[%OBJETO%.Fields.fecmod.ToString()].Visible = false;
            gv%NAME_TABLA%.Columns[%OBJETO%.Fields.apiestado.ToString()].Visible = false;
            gv%NAME_TABLA%.Columns[%OBJETO%.Fields.apitransaccion.ToString()].Visible = false;
        }        
        
        private void gc%NAME_TABLA%_KeyDown(object sender, KeyEventArgs e)
        {
            %VALIDACION%
            if (e.KeyCode == Keys.Delete)
            {
                EliminarRegistro();
            }
        }

        private void gv%NAME_TABLA%_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            %MODELO_OBJETO% rn = new %MODELO_OBJETO%();

            //Apropiamos los valores del Grid
            %OBJETO_DELETE%

            %OBJETO% objExistente = rn.ObtenerObjeto(%STRING_PK%);

            if (objExistente != null)
            {
                %OBJETO_UPDATE%
                objExistente.%API_TRANS% = CApi.Transaccion.MODIFICAR.ToString();
			    objExistente.%USU_MOD% = cParametrosApp.AppUsuario.login;
			    
                //Actualizamos el registro
                rn.Update(objExistente);
                this.CargarListado();
            }
        }

        private void gv%NAME_TABLA%_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            //gv%NAME_TABLA%.SetFocusedRowCellValue(%OBJETO%.Fields.usucre.ToString(),"postgres");
        }

        private void gc%NAME_TABLA%_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ModificarRegistro();
        }
		
		private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frm = new FClaTiposactivo();
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK) this.CargarListado();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            ModificarRegistro();
        }

        private void btnAuditoría_Click(object sender, EventArgs e)
        {
            try
            {
                if (gv%NAME_TABLA%.RowCount <= 0) return;
                var rn = new %MODELO_OBJETO%();

                //Apropiamos los valores del Grid
                var strId = gv%NAME_TABLA%.GetFocusedRowCellValue(%OBJETO%.Fields.id_.ToString()).ToString();
                int intId = rn.GetColumnType(strId, %OBJETO%.Fields.id_);

                //Obtenemos el objeto
                var obj = rn.ObtenerObjeto(intId);

                if (obj == null) return;
                //Abrimos la AUDITORIA
                var frm = new FAuditaria(obj.usucre, obj.feccre, obj.usumod, obj.fecmod, obj.apiestado);
                frm.ShowDialog();
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }
		
		/// <summary>
        /// Función que se encarga de abrir el formulario para Modificar el registro
        /// </summary>
		private void ModificarRegistro()
		{
			try
            {
                if (gv%NAME_TABLA%.RowCount <= 0) return;
				this.Cursor = Cursors.WaitCursor;
                var rn = new %MODELO_OBJETO%();

                //Apropiamos los valores del Grid
                var strId = gv%NAME_TABLA%.GetFocusedRowCellValue(%OBJETO%.Fields.id_.ToString()).ToString();
                int intId = rn.GetColumnType(strId, %OBJETO%.Fields.id_);

                //Obtenemos el objeto
                var obj = rn.ObtenerObjeto(intId);
				this.Cursor = Cursors.Default;

                if (obj != null) 
				{
					var frm = new D%NAME_TABLA%(obj);
					frm.ShowDialog();
					if (frm.DialogResult == DialogResult.OK) this.CargarListado();
				}
            }
            catch (Exception exp)
            {
				this.Cursor = Cursors.Default;
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
		}
		
		/// <summary>
        /// Función que se encarga de Eliminar el registro
        /// </summary>
		private void EliminarRegistro()
		{
			try
            {
                if (gv%NAME_TABLA%.RowCount <= 0) return;
                this.Cursor = Cursors.WaitCursor;
                var rn = new %MODELO_OBJETO%();

                //Apropiamos los valores del Grid
                var strId = gv%NAME_TABLA%.GetFocusedRowCellValue(%OBJETO%.Fields.id_.ToString()).ToString();
                int intId = rn.GetColumnType(strId, %OBJETO%.Fields.id_antenas);

                //Obtenemos el objeto
                var obj = rn.ObtenerObjeto(intId);
                this.Cursor = Cursors.Default;
                if (obj != null)
                {
                    DialogResult res = MessageBox.Show("¿Está seguro que desea ELIMINAR el registro?",
                        "Eliminar registro",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (res == DialogResult.Yes)
                    {
                        this.Cursor = Cursors.WaitCursor;
                        rn.Delete(obj);
                        this.CargarListado();
                        this.Cursor = Cursors.Default;
                    }
                }
            }
            catch (Exception exp)
            {
                this.Cursor = Cursors.Default;
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
		}
    }
}