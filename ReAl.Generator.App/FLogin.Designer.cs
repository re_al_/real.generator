﻿namespace ReAl.Generator.App
{
    partial class FLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FLogin));
            this.Label25 = new System.Windows.Forms.Label();
            this.txtServicio = new System.Windows.Forms.TextBox();
            this.lblServPuerto = new System.Windows.Forms.Label();
            this.cmbDbms = new System.Windows.Forms.ComboBox();
            this.chkSegIntegrada = new System.Windows.Forms.CheckBox();
            this.btnActualizar = new System.Windows.Forms.Button();
            this.cmbDatabase = new System.Windows.Forms.ComboBox();
            this.lblCatalogo = new System.Windows.Forms.Label();
            this.lblPass = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.lblVersion = new System.Windows.Forms.Label();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.cmbPlantilla = new System.Windows.Forms.ComboBox();
            this.lblServidor = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // Label25
            // 
            this.Label25.AutoSize = true;
            this.Label25.BackColor = System.Drawing.Color.Transparent;
            this.Label25.Location = new System.Drawing.Point(59, 15);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(107, 13);
            this.Label25.TabIndex = 0;
            this.Label25.Text = "PLANTILLA PERFIL:";
            // 
            // txtServicio
            // 
            this.txtServicio.Location = new System.Drawing.Point(228, 76);
            this.txtServicio.Name = "txtServicio";
            this.txtServicio.Size = new System.Drawing.Size(123, 20);
            this.txtServicio.TabIndex = 6;
            // 
            // lblServPuerto
            // 
            this.lblServPuerto.AutoSize = true;
            this.lblServPuerto.Location = new System.Drawing.Point(173, 79);
            this.lblServPuerto.Name = "lblServPuerto";
            this.lblServPuerto.Size = new System.Drawing.Size(48, 13);
            this.lblServPuerto.TabIndex = 5;
            this.lblServPuerto.Text = "Servicio:";
            // 
            // cmbDbms
            // 
            this.cmbDbms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDbms.FormattingEnabled = true;
            this.cmbDbms.Items.AddRange(new object[] {
            "SQLServer",
            "Oracle",
            "PostgreSQL",
            "SQLServer2000",
            "MySql",
            "SQLite",
            "FireBird"});
            this.cmbDbms.Location = new System.Drawing.Point(228, 13);
            this.cmbDbms.Name = "cmbDbms";
            this.cmbDbms.Size = new System.Drawing.Size(194, 21);
            this.cmbDbms.TabIndex = 1;
            this.cmbDbms.SelectedIndexChanged += new System.EventHandler(this.cmbDbms_SelectedIndexChanged);
            // 
            // chkSegIntegrada
            // 
            this.chkSegIntegrada.AutoSize = true;
            this.chkSegIntegrada.Location = new System.Drawing.Point(228, 58);
            this.chkSegIntegrada.Name = "chkSegIntegrada";
            this.chkSegIntegrada.Size = new System.Drawing.Size(122, 17);
            this.chkSegIntegrada.TabIndex = 4;
            this.chkSegIntegrada.Text = "Seguridad Integrada";
            this.chkSegIntegrada.UseVisualStyleBackColor = true;
            // 
            // btnActualizar
            // 
            this.btnActualizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnActualizar.Location = new System.Drawing.Point(393, 138);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(29, 27);
            this.btnActualizar.TabIndex = 13;
            this.btnActualizar.UseVisualStyleBackColor = true;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // cmbDatabase
            // 
            this.cmbDatabase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDatabase.FormattingEnabled = true;
            this.cmbDatabase.Location = new System.Drawing.Point(228, 139);
            this.cmbDatabase.Name = "cmbDatabase";
            this.cmbDatabase.Size = new System.Drawing.Size(159, 21);
            this.cmbDatabase.TabIndex = 12;
            this.cmbDatabase.SelectedIndexChanged += new System.EventHandler(this.cmbDatabase_SelectedIndexChanged);
            // 
            // lblCatalogo
            // 
            this.lblCatalogo.AutoSize = true;
            this.lblCatalogo.Location = new System.Drawing.Point(169, 142);
            this.lblCatalogo.Name = "lblCatalogo";
            this.lblCatalogo.Size = new System.Drawing.Size(52, 13);
            this.lblCatalogo.TabIndex = 11;
            this.lblCatalogo.Text = "Catalogo:";
            // 
            // lblPass
            // 
            this.lblPass.AutoSize = true;
            this.lblPass.Location = new System.Drawing.Point(166, 121);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(56, 13);
            this.lblPass.TabIndex = 9;
            this.lblPass.Text = "Password:";
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Location = new System.Drawing.Point(176, 100);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(46, 13);
            this.lblUsuario.TabIndex = 7;
            this.lblUsuario.Text = "Usuario:";
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(126, 219);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(94, 34);
            this.btnNuevo.TabIndex = 4;
            this.btnNuevo.TabStop = false;
            this.btnNuevo.Text = "&Nueva BD";
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(226, 219);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(105, 34);
            this.btnAceptar.TabIndex = 5;
            this.btnAceptar.Text = "&Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(337, 219);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(105, 34);
            this.btnCancelar.TabIndex = 6;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(11, 230);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(60, 13);
            this.lblVersion.TabIndex = 3;
            this.lblVersion.Text = "Versión 5.2";
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(228, 118);
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '*';
            this.txtPass.Size = new System.Drawing.Size(194, 20);
            this.txtPass.TabIndex = 10;
            this.txtPass.TextChanged += new System.EventHandler(this.txtPass_TextChanged);
            this.txtPass.Leave += new System.EventHandler(this.txtPass_Leave);
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(228, 97);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(194, 20);
            this.txtUser.TabIndex = 8;
            this.txtUser.Text = "sa";
            this.txtUser.TextChanged += new System.EventHandler(this.txtUser_TextChanged);
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(228, 36);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(123, 20);
            this.txtServer.TabIndex = 3;
            this.txtServer.TextChanged += new System.EventHandler(this.txtServer_TextChanged);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(176, 16);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(41, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "DBMS:";
            // 
            // cmbPlantilla
            // 
            this.cmbPlantilla.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbPlantilla.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPlantilla.FormattingEnabled = true;
            this.cmbPlantilla.Items.AddRange(new object[] {
            "-- D E F A U L T --",
            "AP",
            "APS Oracle",
            "ReAl Sistema",
            "Segelic Web Class",
            "Segip Asistencia",
            "Segip Extranjeria",
            "Segip Extranjeria Catalogo",
            "Integrate",
            "Integrate Vb.Net",
            "Demo Vb.Net",
            "GestionMedica",
            "ANDROID GestionMedica",
            "Sistema Analisis Financiero",
            "Integrate.Correspondencia",
            "ANDROID Generador Scripts",
            "Siesis",
            "ANDROID INE",
            "DesktopApp",
            "Python",
            "GEVERO",
            "Erp"});
            this.cmbPlantilla.Location = new System.Drawing.Point(172, 12);
            this.cmbPlantilla.Name = "cmbPlantilla";
            this.cmbPlantilla.Size = new System.Drawing.Size(264, 21);
            this.cmbPlantilla.TabIndex = 1;
            this.cmbPlantilla.SelectedIndexChanged += new System.EventHandler(this.cmbPlantilla_SelectedIndexChanged);
            // 
            // lblServidor
            // 
            this.lblServidor.AutoSize = true;
            this.lblServidor.Location = new System.Drawing.Point(173, 39);
            this.lblServidor.Name = "lblServidor";
            this.lblServidor.Size = new System.Drawing.Size(49, 13);
            this.lblServidor.TabIndex = 2;
            this.lblServidor.Text = "Servidor:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtServicio);
            this.groupBox1.Controls.Add(this.lblServPuerto);
            this.groupBox1.Controls.Add(this.cmbDbms);
            this.groupBox1.Controls.Add(this.chkSegIntegrada);
            this.groupBox1.Controls.Add(this.picLogo);
            this.groupBox1.Controls.Add(this.btnActualizar);
            this.groupBox1.Controls.Add(this.cmbDatabase);
            this.groupBox1.Controls.Add(this.lblCatalogo);
            this.groupBox1.Controls.Add(this.lblPass);
            this.groupBox1.Controls.Add(this.lblUsuario);
            this.groupBox1.Controls.Add(this.txtPass);
            this.groupBox1.Controls.Add(this.txtUser);
            this.groupBox1.Controls.Add(this.txtServer);
            this.groupBox1.Controls.Add(this.Label1);
            this.groupBox1.Controls.Add(this.lblServidor);
            this.groupBox1.Location = new System.Drawing.Point(14, 34);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(428, 179);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // picLogo
            // 
            this.picLogo.Image = global::ReAl.Generator.App.Properties.Resources.automator1;
            this.picLogo.Location = new System.Drawing.Point(4, 9);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(156, 160);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo.TabIndex = 10;
            this.picLogo.TabStop = false;
            // 
            // FLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(452, 264);
            this.Controls.Add(this.Label25);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.cmbPlantilla);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ingreso a Real Generator";
            this.Load += new System.EventHandler(this.FLogin_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label25;
        private System.Windows.Forms.TextBox txtServicio;
        private System.Windows.Forms.Label lblServPuerto;
        internal System.Windows.Forms.ComboBox cmbDbms;
        internal System.Windows.Forms.CheckBox chkSegIntegrada;
        private System.Windows.Forms.Button btnActualizar;
        private System.Windows.Forms.ComboBox cmbDatabase;
        private System.Windows.Forms.Label lblCatalogo;
        private System.Windows.Forms.Label lblPass;
        private System.Windows.Forms.Label lblUsuario;
        internal System.Windows.Forms.Button btnNuevo;
        internal System.Windows.Forms.Button btnAceptar;
        internal System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.ComboBox cmbPlantilla;
        private System.Windows.Forms.Label lblServidor;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.PictureBox picLogo;
    }
}