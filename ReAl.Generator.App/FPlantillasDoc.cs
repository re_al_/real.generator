﻿using ReAl.Class.Entidades;
using ReAl.Class.Modelo;
using ReAl.Generator.App.AppClass;
using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;

namespace ReAl.Generator.App
{
    public partial class FPlantillasDoc : Form
    {
        private entClaPlantilla _myObj = null;

        public FPlantillasDoc()
        {
            InitializeComponent();
        }

        public FPlantillasDoc(entClaPlantilla miObj)
        {
            InitializeComponent();

            _myObj = miObj;
        }

        private void FPlantillasDoc_Load(object sender, EventArgs e)
        {
            foreach (string cs in Enum.GetNames(typeof(TipoEntorno)))
                cmbEntorno.Items.Add(cs);

            cmbTipoColumnas.SelectedIndex = 0;
            cmbAPIs.SelectedIndex = 0;
            if ((_myObj != null))
            {
                CargarDatosEntrar();
            }
        }

        private void CargarDatosEntrar()
        {
            txtNombrePlantilla.Text = _myObj.descPlantilla;

            //Obtenemos el Diccionario
            rnClaDetalle rn = new rnClaDetalle();
            DataTable dt = rn.CargarDataTable(entClaDetalle.Fields.idPlantilla, _myObj.idPlantilla);

            foreach (DataRow dr in dt.Rows)
            {
                Control ctr = this;
                do
                {
                    ctr = this.GetNextControl(ctr, true);
                    if ((ctr != null))
                    {
                        if (dr[entClaDetalle.Fields.propiedad.ToString()].ToString() == ctr.Name)
                        {
                            if (ctr is TextBox)
                            {
                                TextBox txt = (TextBox)ctr;
                                txt.Text = dr[entClaDetalle.Fields.valor.ToString()].ToString();
                            }

                            if (ctr is CheckBox)
                            {
                                CheckBox chk = (CheckBox)ctr;
                                chk.Checked = (dr[entClaDetalle.Fields.valor.ToString()].ToString() == "1");
                            }

                            if (ctr is ComboBox)
                            {
                                ComboBox cmb = (ComboBox)ctr;
                                cmb.SelectedItem = dr[entClaDetalle.Fields.valor.ToString()].ToString();
                                cmb.SelectedItem = dr[entClaDetalle.Fields.valor.ToString()].ToString();
                            }

                            if (ctr is RadioButton)
                            {
                                RadioButton rdb = (RadioButton)ctr;
                                rdb.Checked = (dr[entClaDetalle.Fields.valor.ToString()].ToString() == "1");
                            }
                        }
                    }
                } while (!(ctr == null));
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            bool bProcede = false;
            rnClaDetalle rnDet = new rnClaDetalle();
            rnClaPlantilla rn = new rnClaPlantilla();
            try
            {
                //Guardamos el Maestro
                if (_myObj == null)
                {
                    entClaPlantilla obj = new entClaPlantilla();
                    obj.descPlantilla = txtNombrePlantilla.Text;
                    obj.obsPlantilla = txtNombrePlantilla.Text;
                    obj.dbms = cmbDbms.Text;
                    obj.password = txtPass.Text;
                    obj.servidor = txtServer.Text;
                    obj.usuario = txtUser.Text;
                    obj.seguridadint = (chkSegIntegrada.Checked ? 1 : 0);
                    obj.password = txtPass.Text;

                    rn.Insert(obj);

                    //Obtenemos el ID
                    int idPlantilla = (int)rn.FuncionesMax(entClaPlantilla.Fields.idPlantilla, entClaPlantilla.Fields.descPlantilla, "'" + obj.descPlantilla + "'");
                    _myObj = rn.ObtenerObjeto(idPlantilla);
                }
                else
                {
                    //Actualizamos
                    _myObj.descPlantilla = txtNombrePlantilla.Text;
                    _myObj.obsPlantilla = txtNombrePlantilla.Text;
                    _myObj.dbms = cmbDbms.Text;
                    _myObj.password = txtPass.Text;
                    _myObj.servidor = txtServer.Text;
                    _myObj.usuario = txtUser.Text;
                    _myObj.seguridadint = (chkSegIntegrada.Checked ? 1 : 0);
                    _myObj.password = txtPass.Text;
                    rn.Update(_myObj);
                }

                Control ctr = this;
                do
                {
                    ctr = this.GetNextControl(ctr, true);
                    if ((ctr != null))
                    {
                        ArrayList arrColWhere = new ArrayList();
                        arrColWhere.Add(entClaDetalle.Fields.idPlantilla.ToString());
                        arrColWhere.Add(entClaDetalle.Fields.propiedad.ToString());
                        ArrayList arrValWhere = new ArrayList();
                        arrValWhere.Add(_myObj.idPlantilla);
                        arrValWhere.Add("'" + ctr.Name + "'");

                        if (ctr is TextBox && ctr.Name != "txtNombrePlantilla")
                        {
                            TextBox txt = (TextBox)ctr;
                            entClaDetalle objDet = rnDet.ObtenerObjeto(arrColWhere, arrValWhere);
                            if (objDet == null)
                            {
                                objDet = new entClaDetalle();
                                objDet.idPlantilla = _myObj.idPlantilla;
                                objDet.propiedad = txt.Name;
                                objDet.valor = txt.Text;
                                rnDet.Insert(objDet);
                            }
                            else
                            {
                                objDet.valor = txt.Text;
                                rnDet.Update(objDet);
                            }
                        }

                        if (ctr is CheckBox)
                        {
                            CheckBox chk = (CheckBox)ctr;
                            entClaDetalle objDet = rnDet.ObtenerObjeto(arrColWhere, arrValWhere);
                            if (objDet == null)
                            {
                                objDet = new entClaDetalle();
                                objDet.idPlantilla = _myObj.idPlantilla;
                                objDet.propiedad = chk.Name;
                                objDet.valor = (chk.Checked ? "1" : "0");
                                rnDet.Insert(objDet);
                            }
                            else
                            {
                                objDet.valor = (chk.Checked ? "1" : "0");
                                rnDet.Update(objDet);
                            }
                        }

                        if (ctr is ComboBox)
                        {
                            ComboBox cmb = (ComboBox)ctr;
                            entClaDetalle objDet = rnDet.ObtenerObjeto(arrColWhere, arrValWhere);
                            if (objDet == null)
                            {
                                objDet = new entClaDetalle();
                                objDet.idPlantilla = _myObj.idPlantilla;
                                objDet.propiedad = cmb.Name;
                                objDet.valor = cmb.SelectedItem.ToString();
                                rnDet.Insert(objDet);
                            }
                            else
                            {
                                objDet.valor = cmb.SelectedItem.ToString();
                                rnDet.Update(objDet);
                            }
                        }

                        if (ctr is RadioButton)
                        {
                            RadioButton rdb = (RadioButton)ctr;
                            entClaDetalle objDet = rnDet.ObtenerObjeto(arrColWhere, arrValWhere);
                            if (objDet == null)
                            {
                                objDet = new entClaDetalle();
                                objDet.idPlantilla = _myObj.idPlantilla;
                                objDet.propiedad = rdb.Name;
                                objDet.valor = (rdb.Checked ? "1" : "0");
                                rnDet.Insert(objDet);
                            }
                            else
                            {
                                objDet.valor = (rdb.Checked ? "1" : "0");
                                rnDet.Update(objDet);
                            }
                        }
                    }
                } while (!(ctr == null));

                bProcede = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            if (bProcede)
            {
                this.DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void cmbDbms_SelectedIndexChanged(object sender, EventArgs e)
        {
            TipoBD miBd;
            Enum.TryParse(cmbDbms.SelectedItem.ToString(), out miBd);
            if (miBd == TipoBD.SQLServer)
            {
                lblServidor.Visible = true;
                txtServer.Visible = true;
                chkSegIntegrada.Visible = true;
                lblServPuerto.Visible = true;
                txtServicio.Visible = true;
                lblUsuario.Visible = true;
                txtUser.Visible = true;
                txtPass.ReadOnly = false;
                txtPass.PasswordChar = '*';

                chkSegIntegrada.Visible = true;
                lblServPuerto.Visible = false;
                txtServicio.Visible = false;
                lblPass.Text = "Servicio";
                txtPass.ReadOnly = false;
            }
            else if (miBd == TipoBD.SQLite)
            {
                lblServidor.Visible = false;
                txtServer.Visible = false;
                chkSegIntegrada.Visible = false;
                lblServPuerto.Visible = false;
                txtServicio.Visible = false;
                lblUsuario.Visible = false;
                txtUser.Visible = false;
                txtPass.ReadOnly = true;
                txtPass.PasswordChar = ' ';

                lblPass.Text = "Archivo";
                lblPass.Visible = false;
            }
            else
            {
                lblServidor.Visible = true;
                txtServer.Visible = true;
                chkSegIntegrada.Visible = true;
                lblServPuerto.Visible = true;
                txtServicio.Visible = true;
                lblUsuario.Visible = true;
                txtUser.Visible = true;
                txtPass.ReadOnly = false;
                txtPass.PasswordChar = '*';

                chkSegIntegrada.Visible = false;
                lblServPuerto.Visible = true;
                txtServicio.Visible = true;
                txtPass.ReadOnly = false;
                if (miBd == TipoBD.Oracle)
                {
                    lblServPuerto.Text = "Servicio:";
                }
                if (miBd == TipoBD.PostgreSQL)
                {                    
                    lblServPuerto.Text = "Puerto:";
                }
                if (miBd == TipoBD.MySql)
                {
                    lblServPuerto.Text = "Puerto:";
                }
            }
        }
    }
}