﻿using ReAl.Generator.App.AppCore;
using System;
using System.Data;
using System.IO;
using System.Text;

namespace ReAl.Generator.App.AppDocumentation
{
    public class cDocJavaAndroid
    {
        private cDocParams _myParam;
        private cDocFormato.TipoDocFormato miFormato = cDocFormato.TipoDocFormato.MediaWiki;

        public cDocJavaAndroid(cDocParams misParametros)
        {
            _myParam = misParametros;
        }

        public String crearDocEnt()
        {
            cDocFormato miFor = new cDocFormato(miFormato);
            if (Directory.Exists(_myParam.PathDesktop + "\\Documentacion") == false)
                Directory.CreateDirectory(_myParam.PathDesktop + "\\Documentacion");

            if (Directory.Exists(_myParam.PathDesktop + "\\Documentacion\\" + _myParam.strNameSpaceEnt) == false)
                Directory.CreateDirectory(_myParam.PathDesktop + "\\Documentacion\\" + _myParam.strNameSpaceEnt);

            StringBuilder strTexto = new StringBuilder();

            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "" + _myParam.strNameClaseEnt + " (Clase)" + miFor.fBoldCerrar + miFor.fTitle1Cerrar);
                strTexto.AppendLine("");
            }
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Jerarquia de Herencia" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fBoldAbrir + "Paquete: " + miFor.fBoldAbrir + "" + _myParam.strNameBO + (String.IsNullOrEmpty(_myParam.strNameSpaceEnt) ? "" : "." + _myParam.strNameSpaceEnt));
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fBoldAbrir + "Clase: " + miFor.fBoldAbrir + "" + _myParam.strNameClaseEnt + ".class");
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Sintaxis" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTexto.AppendLine(" " + _myParam.strNameClaseEnt + " obj = new " + _myParam.strNameClaseEnt + "();");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTexto.AppendLine("```");
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Constructores" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + "Nombre" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + "Descripcion" + miFor.fTableTdCerrarFinal);
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgConstructor + " " + _myParam.strNameClaseEnt + "() " + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Inicializa una nueva instancia de la clase. " + miFor.fTableCerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Propiedades" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + "Nombre" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + "Descripcion" + miFor.fTableTdCerrarFinal);
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrarFinal);
            foreach (DataRow row in _myParam.dtColumns.Rows)
            {
                String nameCol = row[0].ToString();
                String tipoCol = row[1].ToString();
                strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgPropiedad + " " + nameCol + " " + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Propiedad publica de tipo " + miFor.fBoldCerrar + tipoCol + miFor.fBoldAbrir + " que representa a la columna " + miFor.fBoldCerrar + nameCol + miFor.fBoldAbrir + " de la Tabla " + _myParam.strNameTabla + "" + miFor.fTableTdCerrarFinal);
            }
            strTexto.Append(miFor.fTableCerrar);

            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Comentarios" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine(_myParam.strNameClaseEnt + " es un Objeto que representa la estructura de la Tabla " + _myParam.strNameTabla + ".");
            strTexto.AppendLine("");
            strTexto.AppendLine("Éste objeto permite la interacción con la BD a través de su respectiva Clase de Regla de Negocios. No se puede realizar ninguna operacion CRUD de éste objeto con su respectiva tabla, a no ser por medio de la Clase " + miFor.fBoldCerrar + _myParam.strNameClaseRn + miFor.fBoldAbrir + ".");
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Ejemplos" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine("El ejemplo de código siguiente muestra cómo instanciar ésta clase y dar valores a sus propiedades:");
            strTexto.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTexto.AppendLine(" " + _myParam.strNameClaseEnt + " obj = new " + _myParam.strNameClaseEnt + "();");
            foreach (DataRow row in _myParam.dtColumns.Rows)
            {
                String nameCol = row[0].ToString();
                strTexto.AppendLine(" obj.set" + nameCol + "(" + miFor.fHrefAbrir + "valor" + miFor.fHrefCerrar + ");");
            }
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTexto.AppendLine("```");
            strTexto.AppendLine("");
            strTexto.AppendLine("El ejemplo de código siguiente muestra cómo recuperar un objeto a partir de su Regla de Negocios:");
            strTexto.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTexto.AppendLine(" " + _myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            strTexto.AppendLine(" " + _myParam.strNameClaseEnt + " obj = rn.ObtenerObjeto(1);");
            strTexto.AppendLine(" if (obj != null)");
            strTexto.AppendLine("     rn.Update(obj);");
            strTexto.AppendLine(" else");
            strTexto.AppendLine("     rn.Insert(obj);");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTexto.AppendLine("```");
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Información de Versión" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine("Versión 1.0");
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Autor" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fHrefAbrir + "R. Alonzo Vera Arias" + miFor.fHrefCerrar + "");
            strTexto.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine(miFor.fHrefAbrir + "constructor" + miFor.fHrefCerrar + ": http://i.msdn.microsoft.com/dynimg/IC32771.gif");
                strTexto.AppendLine(miFor.fHrefAbrir + "propiedad" + miFor.fHrefCerrar + ": http://i.msdn.microsoft.com/dynimg/IC108523.gif");
                strTexto.AppendLine(miFor.fHrefAbrir + "metodo" + miFor.fHrefCerrar + ": http://i.msdn.microsoft.com/dynimg/IC32771.gif");
                strTexto.AppendLine(miFor.fHrefAbrir + "evento" + miFor.fHrefCerrar + ": http://i.msdn.microsoft.com/dynimg/IC98802.gif");
                strTexto.AppendLine(miFor.fHrefAbrir + "R. Alonzo Vera Arias" + miFor.fHrefCerrar + ": http://real7.somee.com");
            }

            CFunciones.CrearArchivo(strTexto.ToString(), _myParam.strNameClaseEnt + ".md", _myParam.PathDesktop + "\\Documentacion\\" + _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\");

            return _myParam.PathDesktop + "\\\\Documentacion\\" + _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\" + _myParam.strNameClaseEnt + ".md";
        }

        public String createHome()
        {
            cDocFormato miFor = new cDocFormato(miFormato);

            StringBuilder strTextoUrl = new StringBuilder();
            StringBuilder strTexto = new StringBuilder();
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "Bienvenido a nuestra Wiki" + miFor.fBoldCerrar + miFor.fTitle1Cerrar);
                strTexto.AppendLine("");
            }
            strTexto.AppendLine("Esta es la página central de la Documentación de éste proyecto.");
            strTexto.AppendLine("");
            strTexto.AppendLine("A continuación podrá ver la documentación de las clases que se han utilizado en éste proyecto:");
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Entidades" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            for (int i = 0; i < _myParam.miLista.Items.Count; i++)
            {
                if (_myParam.miLista.Items[i].Checked)
                {
                    String strNameTabla = _myParam.miLista.Items[i].SubItems[0].Text;
                    String strNameClaseEnt = _myParam.strPrefijoEnt + strNameTabla.Replace("_", "").Substring(0, 1).ToUpper() + strNameTabla.Replace("_", "").Substring(1, 2).ToLower() + strNameTabla.Replace("_", "").Substring(3, 1).ToUpper() + strNameTabla.Replace("_", "").Substring(4).ToLower();
                    strTexto.AppendLine("  - " + miFor.fHrefAbrir + "" + strNameClaseEnt + "" + miFor.fHrefCerrar + "");
                    strTextoUrl.AppendLine(miFor.fHrefAbrir + "" + strNameClaseEnt + "" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + strNameClaseEnt);
                }
            }
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Reglas de Negocio" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            for (int i = 0; i < _myParam.miLista.Items.Count; i++)
            {
                if (_myParam.miLista.Items[i].Checked)
                {
                    String strNameTabla = _myParam.miLista.Items[i].SubItems[0].Text;
                    String strNameClaseRn = _myParam.strPrefijoRn + strNameTabla.Replace("_", "").Substring(0, 1).ToUpper() + strNameTabla.Replace("_", "").Substring(1, 2).ToLower() + strNameTabla.Replace("_", "").Substring(3, 1).ToUpper() + strNameTabla.Replace("_", "").Substring(4).ToLower();
                    strTexto.AppendLine("  - " + miFor.fHrefAbrir + "" + strNameClaseRn + "" + miFor.fHrefCerrar + "");
                    strTextoUrl.AppendLine(miFor.fHrefAbrir + "" + strNameClaseRn + "" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + strNameClaseRn);
                }
            }
            strTexto.AppendLine("");
            strTexto.Append(strTextoUrl);

            CFunciones.CrearArchivo(strTexto.ToString(), "Home.md", _myParam.PathDesktop + "\\Documentacion\\" + _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\");

            return _myParam.PathDesktop + "\\\\Documentacion\\" + _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\Home.md";
        }

        public String crearDocRn()
        {
            cDocFormato miFor = new cDocFormato(miFormato);

            if (Directory.Exists(_myParam.PathDesktop + "\\Documentacion") == false)
                Directory.CreateDirectory(_myParam.PathDesktop + "\\Documentacion");

            if (Directory.Exists(_myParam.PathDesktop + "\\Documentacion\\" + _myParam.strNameSpaceRn) == false)
                Directory.CreateDirectory(_myParam.PathDesktop + "\\Documentacion\\" + _myParam.strNameSpaceRn);

            if (Directory.Exists(_myParam.PathDesktop + "\\Documentacion\\" + (String.IsNullOrEmpty(_myParam.strNameSpaceRn) ? "" : _myParam.strNameSpaceRn + "\\") + _myParam.strNameClaseRn) == false)
                Directory.CreateDirectory(_myParam.PathDesktop + "\\Documentacion\\" + (String.IsNullOrEmpty(_myParam.strNameSpaceRn) ? "" : _myParam.strNameSpaceRn + "\\") + _myParam.strNameClaseRn);

            StringBuilder strTexto = new StringBuilder();
            StringBuilder strTextoUrl = new StringBuilder();

            strTexto.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "" + _myParam.strNameClaseRn + " (Clase)" + miFor.fBoldCerrar + miFor.fTitle1Cerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Jerarquia de Herencia" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fBoldAbrir + "Paquete: " + miFor.fBoldAbrir + "" + _myParam.strNameBO + (String.IsNullOrEmpty(_myParam.strNameSpaceEnt) ? "" : "." + _myParam.strNameSpaceEnt));
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fBoldAbrir + "Clase: " + miFor.fBoldAbrir + "" + _myParam.strNameClaseRn + ".class");
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Sintaxis" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTexto.AppendLine(" " + _myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTexto.AppendLine("```");
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Constructores" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Nombre " + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrarFinal + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrarFinal + "");
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgConstructor + " " + _myParam.strNameClaseRn + "(Context) " + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Inicializa una nueva instancia de la clase." + miFor.fTableCerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Propiedades" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Nombre " + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrarFinal);
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgPropiedad + " miContexto " + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Propiedad privada de tipo " + miFor.fBoldAbrir + "Context" + miFor.fBoldCerrar + " que va a utilizarse para para instanciar varias clases dentro de ésta clase" + miFor.fTableCerrar + "" + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Métodos" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Nombre " + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrarFinal);
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrarFinal + "");
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "getFromJson(File)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite obtener una Array de Objetos a partir de un Archivo  " + miFor.fBoldAbrir + "JSON" + miFor.fBoldCerrar + "." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "getFromJson(String)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite obtener una Array de Objetos a partir del Path de un Archivo  " + miFor.fBoldAbrir + "JSON" + miFor.fBoldCerrar + "." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "setToJson(ArrayList<" + _myParam.strNameClaseEnt + "> )" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite obtener una estructura " + miFor.fBoldAbrir + "JSON" + miFor.fBoldCerrar + " a partir de un Array de Objetos" + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "setToJsonFile(ArrayList<" + _myParam.strNameClaseEnt + ">, File)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite obtener una estructura " + miFor.fBoldAbrir + "JSON" + miFor.fBoldCerrar + " a partir de un Array de Objetos y almacenarla en un Archivo." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "insertData(String, String)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite la inserción de registros a partir de un CSV" + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "getDataFromCsv(String, String, Boolean)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que obtiene un Array de objetos a partir de un CSV" + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "exportToCsv(String)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite exportar todos los registros de la BD a un CSV" + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "insert(" + _myParam.strNameClaseEnt + ")" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite " + miFor.fBoldAbrir + "insertar" + miFor.fBoldCerrar + " un registro en la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + "" + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "update(" + _myParam.strNameClaseEnt + ")" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite " + miFor.fBoldAbrir + "actualizar" + miFor.fBoldCerrar + " un registro en la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + "" + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "delete(" + _myParam.strNameClaseEnt + ")" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite " + miFor.fBoldAbrir + "eliminar" + miFor.fBoldCerrar + " un registro en la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + "" + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "deleteAll()" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite " + miFor.fBoldAbrir + "eliminar" + miFor.fBoldCerrar + " TODOS los registros en la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + "" + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "obtenerObjeto(int)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite obtener un registro de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " a partir de su PK" + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "obtenerObjeto(" + _myParam.strNameClaseEnt + ".Fields, String)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite obtener un registro de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " a partir de una claúsula " + miFor.fBoldAbrir + "WHERE" + miFor.fBoldCerrar + "." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "obtenerObjeto(ArrayList<String>, ArrayList<String>)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite obtener un registro de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " a partir de varias claúsulas " + miFor.fBoldAbrir + "WHERE" + miFor.fBoldCerrar + " agrupadas en ArrayList." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "obtenerLista()" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite obtener un lista de Objetos de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + "." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "obtenerLista(" + _myParam.strNameClaseEnt + ".Fields, String)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite obtener una lista de Objetos de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " a partir de una claúsula " + miFor.fBoldAbrir + "WHERE" + miFor.fBoldCerrar + "." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "obtenerLista(ArrayList<String>, ArrayList<String>)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite obtener una lista de Objetos de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " a partir de varias claúsulas " + miFor.fBoldAbrir + "WHERE" + miFor.fBoldCerrar + " agrupadas en ArrayList." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "obtenerLista(ArrayList<String>, ArrayList<String>, String)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que permite obtener una lista de Objetos de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " a partir de varias claúsulas " + miFor.fBoldAbrir + "WHERE" + miFor.fBoldCerrar + " agrupadas en ArrayList y un parámetro de tipo String adicional." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "funcionesMax(entEncEncuesta.Fields)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que ejecuta la funcion sql " + miFor.fBoldAbrir + "MAX()" + miFor.fBoldCerrar + " sobre un campo \"miMaxField\"." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "funcionesMax(entEncEncuesta.Fields, entEncEncuesta.Fields, String)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que ejecuta la funcion sql " + miFor.fBoldAbrir + "MAX()" + miFor.fBoldCerrar + " sobre un campo \"miMaxField\" filtrado por una claúsula " + miFor.fBoldAbrir + "WHERE" + miFor.fBoldCerrar + "." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "funcionesMax(entEncEncuesta.Fields, ArrayList<String>, ArrayList<String>, String)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que ejecuta la funcion sql " + miFor.fBoldAbrir + "MAX()" + miFor.fBoldCerrar + " sobre un campo \"miMaxField\" filtrado por varias claúsulas " + miFor.fBoldAbrir + "WHERE" + miFor.fBoldCerrar + " agrupadas en ArrayList y un parámetro de tipo String adicional." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "funcionesCount()" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que ejecuta la funcion sql " + miFor.fBoldAbrir + "COUNT()" + miFor.fBoldCerrar + " sobre toda la tabla." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "funcionesCount(entEncEncuesta.Fields, String)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que ejecuta la funcion sql " + miFor.fBoldAbrir + "COUNT()" + miFor.fBoldCerrar + " filtrado por una claúsula " + miFor.fBoldAbrir + "WHERE" + miFor.fBoldCerrar + "." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "funcionesCount(ArrayList<String>, ArrayList<String>)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que ejecuta la funcion sql " + miFor.fBoldAbrir + "COUNT()" + miFor.fBoldCerrar + " sobre un campo \"miMaxField\" filtrado por varias claúsulas " + miFor.fBoldAbrir + "WHERE" + miFor.fBoldCerrar + " agrupadas en ArrayList." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "funcionesCount(ArrayList<String>, ArrayList<String>, String)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método que ejecuta la funcion sql " + miFor.fBoldAbrir + "COUNT()" + miFor.fBoldCerrar + " sobre un campo \"miMaxField\" filtrado por varias claúsulas " + miFor.fBoldAbrir + "WHERE" + miFor.fBoldCerrar + " agrupadas en ArrayList y un parámetro de tipo String adicional." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "getArrayFromCursor(Cursor)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método privado que retorna un Array del tipo String a partir de un Cursor. El Array tendrá tantos elementos como columnas tenga la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + "." + miFor.fTableTdCerrarFinal);
            strTexto.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + miFor.fImgMetodo + " " + miFor.fHrefAbrir + "getObjFromCursor(Cursor)" + miFor.fHrefCerrar + "" + miFor.fTableTdCerrar + "" + miFor.fTableTdAbrir + " Método privado que retorna un Objeto del tipo " + _myParam.strNameClaseEnt + " a partir de un Cursor." + miFor.fTableTdCerrarFinal);
            strTexto.Append(miFor.fTableCerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Comentarios" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine(_myParam.strNameClaseRn + " es un Objeto que representa la estructura de la Tabla " + _myParam.strNameTabla + ".");
            strTexto.AppendLine("");
            strTexto.AppendLine("Éste objeto permite la interacción con la BD partiendo de clases Entidades. No se puede realizar ninguna operacion CRUD con éste objeto en su respectiva tabla, a no ser por medio de la Clase " + miFor.fBoldCerrar + _myParam.strNameClaseEnt + miFor.fBoldAbrir + ".");
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Ejemplos" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine("El ejemplo de código siguiente muestra cómo recuperar un objeto a partir de su Regla de Negocios:");
            strTexto.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTexto.AppendLine(" " + _myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            strTexto.AppendLine(" " + _myParam.strNameClaseRn + " obj = rn.ObtenerObjeto(1);");
            strTexto.AppendLine(" if (obj != null)");
            strTexto.AppendLine("     //Actualizamos el Objeto existente");
            strTexto.AppendLine("     rn.Update(obj);");
            strTexto.AppendLine(" else");
            strTexto.AppendLine("     //Insertamos un nuevo objeto en la BD");
            strTexto.AppendLine("     rn.Insert(obj);");
            strTexto.AppendLine(" ");
            strTexto.AppendLine(" //Eliminamos el Objeto de la BD");
            strTexto.AppendLine(" rn.Delete(obj);");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTexto.AppendLine("```");
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Información de Versión" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine("Versión 1.0");
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Autor" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTexto.AppendLine("");
            strTexto.AppendLine(miFor.fHrefAbrir + "R. Alonzo Vera Arias" + miFor.fHrefCerrar + "");
            strTexto.AppendLine("");

            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine(miFor.fHrefAbrir + "constructor" + miFor.fHrefCerrar + ": http://i.msdn.microsoft.com/dynimg/IC32771.gif");
                strTexto.AppendLine(miFor.fHrefAbrir + "propiedad" + miFor.fHrefCerrar + ": http://i.msdn.microsoft.com/dynimg/IC108523.gif");
                strTexto.AppendLine(miFor.fHrefAbrir + "metodo" + miFor.fHrefCerrar + ": http://i.msdn.microsoft.com/dynimg/IC32771.gif");
                strTexto.AppendLine(miFor.fHrefAbrir + "evento" + miFor.fHrefCerrar + ": http://i.msdn.microsoft.com/dynimg/IC98802.gif");
                strTexto.AppendLine(miFor.fHrefAbrir + "R. Alonzo Vera Arias" + miFor.fHrefCerrar + ": http://real7.somee.com");

                strTextoUrl.AppendLine(miFor.fHrefAbrir + "getFromJson(File)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/getFromJsonFil");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "getFromJson(String)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/getFromJsonStr");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "setToJson(ArrayList<" + _myParam.strNameClaseEnt + "> )" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/setToJson");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "setToJsonFile(ArrayList<" + _myParam.strNameClaseEnt + ">, File)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/setToJsonFile");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "insertData(String, String)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/insertData");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "getDataFromCsv(String, String, Boolean)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/getDataFromCsv");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "exportToCsv(String)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/exportToCsv");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "insert(" + _myParam.strNameClaseEnt + ")" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/insert");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "update(" + _myParam.strNameClaseEnt + ")" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/update");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "delete(" + _myParam.strNameClaseEnt + ")" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/delete");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "deleteAll()" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/deleteAll");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "obtenerObjeto(int)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/obtenerObjetoPk");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "obtenerObjeto(" + _myParam.strNameClaseEnt + ".Fields, String)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/obtenerObjetoFldStr");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "obtenerObjeto(ArrayList<String>, ArrayList<String>)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/obtenerObjetoArrArr");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "obtenerLista()" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/obtenerLista");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "obtenerLista(" + _myParam.strNameClaseEnt + ".Fields, String)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/obtenerListaFldStr");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "obtenerLista(ArrayList<String>, ArrayList<String>)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/obtenerListaArrArr");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "obtenerLista(ArrayList<String>, ArrayList<String>, String)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/obtenerListaArrArrStr");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "funcionesMax(entEncEncuesta.Fields)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/funcionesMaxFld");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "funcionesMax(entEncEncuesta.Fields, entEncEncuesta.Fields, String)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/funcionesMaxFldFldStr");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "funcionesMax(entEncEncuesta.Fields, ArrayList<String>, ArrayList<String>)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/funcionesMaxFldArrArr");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "funcionesMax(entEncEncuesta.Fields, ArrayList<String>, ArrayList<String>, String)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/funcionesMaxFldArrArrStr");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "funcionesCount()" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/funcionesCount");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "funcionesCount(entEncEncuesta.Fields, String)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/funcionesCountFldStr");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "funcionesCount(ArrayList<String>, ArrayList<String>)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/funcionesCountArrArr");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "funcionesCount(ArrayList<String>, ArrayList<String>, String)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/funcionesCountArrArrStr");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "getArrayFromCursor(Cursor)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/getArrayFromCursor");
                strTextoUrl.AppendLine(miFor.fHrefAbrir + "getObjFromCursor(Cursor)" + miFor.fHrefCerrar + ": " + _myParam.strUrlWiki + _myParam.strNameClaseRn + "/getObjFromCursor");

                strTexto.Append(strTextoUrl);
            }

            //Creamos los archivos de cada funcion
            StringBuilder strTextoFuncion = new StringBuilder();
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTextoFuncion.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "Método getFromJson(File)" + miFor.fBoldCerrar + miFor.fTitle1Cerrar);
                strTextoFuncion.AppendLine("");
            }
            strTextoFuncion.AppendLine("Método que permite obtener una Array de Objetos a partir de un Archivo  " + miFor.fBoldAbrir + "JSON" + miFor.fBoldCerrar + ".");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Sintaxis" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTextoFuncion.AppendLine(" " + _myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            strTextoFuncion.AppendLine(" File miArchivo = new File(strPath, strNombreArchivo);");
            strTextoFuncion.AppendLine(" ArrayList<" + _myParam.strNameClaseEnt + "> miLis = rn.getFromJson(miArchivo)");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("```");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle3Abrir + miFor.fBoldAbrir + "Parámetros" + miFor.fBoldCerrar + miFor.fTitle3Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Nombre" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion ");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- ");
            strTextoFuncion.AppendLine(" File" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " miFileJson" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Es el archivo que " + miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Resultado del Método" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion ");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- ");
            strTextoFuncion.AppendLine(" int" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " El método devolverá la cantidad de registros insertados " + miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            CFunciones.CrearArchivo(strTextoFuncion.ToString(), "getFromJsonFil.md", _myParam.PathDesktop + "\\Documentacion\\" + (String.IsNullOrEmpty(_myParam.strNameSpaceEnt) ? "" : _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\") + _myParam.strNameClaseRn + "\\");

            strTextoFuncion = new StringBuilder();
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTextoFuncion.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "Método insertData(String, String)" + miFor.fBoldCerrar + miFor.fTitle1Cerrar);
                strTextoFuncion.AppendLine("");
            }
            strTextoFuncion.AppendLine("Método que permite la inserción de registros a partir de un CSV.");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Sintaxis" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTextoFuncion.AppendLine(" " + _myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            strTextoFuncion.AppendLine(" rn.insertData(myPath, miFileName);");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("```");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle3Abrir + miFor.fBoldAbrir + "Parámetros" + miFor.fBoldCerrar + miFor.fTitle3Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Nombre" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " String" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " strPath" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Es el Path o Ruta donde se encuentra el archivo CSV a ser importado. " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " String" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " fileName" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Es el Nombre del archivo CSV que va a ser importado. " + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Resultado del Método" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " int" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " El método devolverá la cantidad de registros insertados " + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            CFunciones.CrearArchivo(strTextoFuncion.ToString(), "insertData.md", _myParam.PathDesktop + "\\Documentacion\\" + (String.IsNullOrEmpty(_myParam.strNameSpaceEnt) ? "" : _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\") + _myParam.strNameClaseRn + "\\");

            strTextoFuncion = new StringBuilder();
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTextoFuncion.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "Método exportToCsv(String)" + miFor.fBoldCerrar + miFor.fTitle1Cerrar);
                strTextoFuncion.AppendLine("");
            }
            strTextoFuncion.AppendLine("Método que permite exportar todos los registros de la BD a un CSV");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Sintaxis" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTextoFuncion.AppendLine(" " + _myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            strTextoFuncion.AppendLine(" rn.exportToCsv(miFileName);");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("```");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle3Abrir + miFor.fBoldAbrir + "Parámetros" + miFor.fBoldCerrar + miFor.fTitle3Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Nombre" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " String" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " fileName" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Es el Nombre del archivo CSV donde se va a exportar el Contenido de la Tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + ". " + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Resultado del Método" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " Boolean" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " El método devolverá " + miFor.fBoldAbrir + "TRUE" + miFor.fBoldCerrar + " o " + miFor.fBoldAbrir + "FALSE" + miFor.fBoldCerrar + " dependendiendo del éxito de la operacion." + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            CFunciones.CrearArchivo(strTextoFuncion.ToString(), "exportToCsv.md", _myParam.PathDesktop + "\\Documentacion\\" + (String.IsNullOrEmpty(_myParam.strNameSpaceEnt) ? "" : _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\") + _myParam.strNameClaseRn + "\\");

            strTextoFuncion = new StringBuilder();
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTextoFuncion.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "Método insert(" + _myParam.strNameClaseEnt + ")" + miFor.fBoldCerrar + miFor.fTitle1Cerrar);
                strTextoFuncion.AppendLine("");
            }
            strTextoFuncion.AppendLine("Método que permite " + miFor.fBoldAbrir + "insertar" + miFor.fBoldCerrar + " un registro en la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + "");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Sintaxis" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTextoFuncion.AppendLine(" " + _myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            strTextoFuncion.AppendLine(" rn.insert(obj);");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("```");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle3Abrir + miFor.fBoldAbrir + "Parámetros" + miFor.fBoldCerrar + miFor.fTitle3Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Nombre" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + "" + _myParam.strNameClaseEnt + "" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " obj" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Es el Objeto que contiene todas las propiedades que van a ser insertadas en la Tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + "" + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Resultado del Método" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " long" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " El método devolverá la cantidad de registros insertados en la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + "." + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            CFunciones.CrearArchivo(strTextoFuncion.ToString(), "insert.md", _myParam.PathDesktop + "\\Documentacion\\" + (String.IsNullOrEmpty(_myParam.strNameSpaceEnt) ? "" : _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\") + _myParam.strNameClaseRn + "\\");

            strTextoFuncion = new StringBuilder();
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTextoFuncion.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "Método update(" + _myParam.strNameClaseEnt + ")" + miFor.fBoldCerrar + miFor.fTitle1Cerrar);
                strTextoFuncion.AppendLine("");
            }
            strTextoFuncion.AppendLine("Método que permite " + miFor.fBoldAbrir + "actualizar" + miFor.fBoldCerrar + " un registro en la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + "");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Sintaxis" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTextoFuncion.AppendLine(" " + _myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            strTextoFuncion.AppendLine(" rn.update(obj);");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("```");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle3Abrir + miFor.fBoldAbrir + "Parámetros" + miFor.fBoldCerrar + miFor.fTitle3Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Nombre" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " " + _myParam.strNameClaseEnt + "" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " obj" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Es el Objeto que contiene todas las propiedades que van a ser actualizadas en la Tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + ". Para realizar la actualización del resgitro, se recurrirá solo a la PK de la tabla como parámetro de búsqueda. " + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Resultado del Método" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine("Éste método no devuleve ningún resultado.");
            strTextoFuncion.AppendLine("");
            CFunciones.CrearArchivo(strTextoFuncion.ToString(), "update.md", _myParam.PathDesktop + "\\Documentacion\\" + (String.IsNullOrEmpty(_myParam.strNameSpaceEnt) ? "" : _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\") + _myParam.strNameClaseRn + "\\");

            strTextoFuncion = new StringBuilder();
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTextoFuncion.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "Método delete(" + _myParam.strNameClaseEnt + ")" + miFor.fBoldCerrar + miFor.fTitle1Abrir);
                strTextoFuncion.AppendLine("");
            }
            strTextoFuncion.AppendLine("Método que permite " + miFor.fBoldAbrir + "eliminar" + miFor.fBoldCerrar + " un registro en la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + "");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Sintaxis" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTextoFuncion.AppendLine(" " + _myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            strTextoFuncion.AppendLine(" rn.delete(obj);");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("```");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle3Abrir + miFor.fBoldAbrir + "Parámetros" + miFor.fBoldCerrar + miFor.fTitle3Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Nombre" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " " + _myParam.strNameClaseEnt + "" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " obj" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Es el Objeto que contiene que va a ser eliminado de la Tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + ". Para realizar la eliminación se recurrirá solo a la PK de la tabla. " + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Resultado del Método" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " long" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " El método devolverá la cantidad de registros eliminados en la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + "." + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            CFunciones.CrearArchivo(strTextoFuncion.ToString(), "delete.md", _myParam.PathDesktop + "\\Documentacion\\" + (String.IsNullOrEmpty(_myParam.strNameSpaceEnt) ? "" : _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\") + _myParam.strNameClaseRn + "\\");

            strTextoFuncion = new StringBuilder();
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTextoFuncion.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "Método deleteAll()" + miFor.fBoldCerrar + miFor.fTitle1Cerrar);
                strTextoFuncion.AppendLine("");
            }
            strTextoFuncion.AppendLine("Método que permite " + miFor.fBoldAbrir + "eliminar" + miFor.fBoldCerrar + " TODOS los registros en la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + "");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Sintaxis" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTextoFuncion.AppendLine(" " + _myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            strTextoFuncion.AppendLine(" rn.deleteAll();");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("```");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle3Abrir + miFor.fBoldAbrir + "Parámetros" + miFor.fBoldCerrar + miFor.fTitle3Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine("Éste método no recibe ningún parámetro.");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Resultado del Método" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " long" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " El método devolverá la cantidad de registros eliminados en la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + "." + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            CFunciones.CrearArchivo(strTextoFuncion.ToString(), "deleteAll.md", _myParam.PathDesktop + "\\Documentacion\\" + (String.IsNullOrEmpty(_myParam.strNameSpaceEnt) ? "" : _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\") + _myParam.strNameClaseRn + "\\");

            strTextoFuncion = new StringBuilder();
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTextoFuncion.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "Método obtenerObjeto(int)" + miFor.fBoldCerrar + miFor.fTitle1Cerrar);
                strTextoFuncion.AppendLine("");
            }
            strTextoFuncion.AppendLine("Método que permite obtener un registro de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " a partir de su PK");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Sintaxis" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTextoFuncion.AppendLine(" " + _myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            strTextoFuncion.AppendLine(" " + _myParam.strNameClaseEnt + " obj = rn.obtenerObjeto(iPk);");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("```");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle3Abrir + miFor.fBoldAbrir + "Parámetros" + miFor.fBoldCerrar + miFor.fTitle3Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Nombre" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " int" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " intid" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Es el identificador único del registro que se quiere obtener de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " " + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Resultado del Método" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " " + _myParam.strNameClaseEnt + "" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " El método devuelve un objeto que equivale al registro de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " cuya PK es el parámetro ingresado. Si el no existe un registro con la PK especificada, el método devolverá " + miFor.fBoldAbrir + "NULL" + miFor.fBoldCerrar + "." + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            CFunciones.CrearArchivo(strTextoFuncion.ToString(), "obtenerObjetoPk.md", _myParam.PathDesktop + "\\Documentacion\\" + (String.IsNullOrEmpty(_myParam.strNameSpaceEnt) ? "" : _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\") + _myParam.strNameClaseRn + "\\");

            strTextoFuncion = new StringBuilder();
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTextoFuncion.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "Método obtenerObjeto(" + _myParam.strNameClaseEnt + ".Fields, String)" + miFor.fBoldCerrar + miFor.fTitle1Cerrar);
                strTextoFuncion.AppendLine("");
            }
            strTextoFuncion.AppendLine("Método que permite obtener un registro de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " a partir de una claúsula " + miFor.fBoldAbrir + "WHERE" + miFor.fBoldCerrar + ".");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Sintaxis" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTextoFuncion.AppendLine(_myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            strTextoFuncion.AppendLine(_myParam.strNameClaseEnt + " obj = rn.obtenerObjeto(" + _myParam.strNameClaseEnt + ".Fields." + _myParam.dtColumns.Rows[1][0].ToString() + ", String.valueOf(miParam));");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("```");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle3Abrir + miFor.fBoldAbrir + "Parámetros" + miFor.fBoldCerrar + miFor.fTitle3Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Nombre" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " " + _myParam.strNameClaseEnt + ".Fields" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " miField" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Parámetro que puede obtiene los valores del ENUM Fileds, y que representa a las columnas de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + "." + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " String" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " miValue" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Representa el valor para el filtro aplicado sobre el Parámetro " + miFor.fBoldAbrir + "miField" + miFor.fBoldCerrar + "." + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Resultado del Método" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " " + _myParam.strNameClaseEnt + "" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " El método devuelve un objeto que equivale al registro de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " cuya PK es el parámetro ingresado. Si el no existe un registro con la PK especificada, el método devolverá " + miFor.fBoldAbrir + "NULL" + miFor.fBoldCerrar + "." + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            CFunciones.CrearArchivo(strTextoFuncion.ToString(), "obtenerObjetoFldStr.md", _myParam.PathDesktop + "\\Documentacion\\" + (String.IsNullOrEmpty(_myParam.strNameSpaceEnt) ? "" : _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\") + _myParam.strNameClaseRn + "\\");

            strTextoFuncion = new StringBuilder();
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTextoFuncion.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "Método obtenerObjeto(ArrayList<String>, ArrayList<String>)" + miFor.fBoldCerrar + miFor.fTitle1Cerrar);
                strTextoFuncion.AppendLine("");
            }
            strTextoFuncion.AppendLine("Método que permite obtener un registro de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " a partir de varias claúsulas " + miFor.fBoldAbrir + "WHERE" + miFor.fBoldCerrar + " agrupadas en ArrayList.");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Sintaxis" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTextoFuncion.AppendLine(" " + _myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            strTextoFuncion.AppendLine(" ArrayList<String> arrColWhere = new ArrayList<String>()");
            strTextoFuncion.AppendLine(" arrColWhere.add(" + _myParam.strNameClaseEnt + ".Fields." + _myParam.dtColumns.Rows[1][0].ToString() + ".toString())");
            strTextoFuncion.AppendLine(" arrColWhere.add(" + _myParam.strNameClaseEnt + ".Fields." + _myParam.dtColumns.Rows[2][0].ToString() + ".toString())");
            strTextoFuncion.AppendLine(" ArrayList<String> arrValWhere = new ArrayList<String>()");
            strTextoFuncion.AppendLine(" arrValWhere.add(String.valueOf(miParam1))");
            strTextoFuncion.AppendLine(" arrValWhere.add(\"'FILTRO-QUE-QUERAMOS'\")");
            strTextoFuncion.AppendLine(" " + _myParam.strNameClaseEnt + " obj = rn.obtenerObjeto(arrColWhere, arrValWhere);");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("```");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle3Abrir + miFor.fBoldAbrir + "Parámetros" + miFor.fBoldCerrar + miFor.fTitle3Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Nombre" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " ArrayList<String>" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " arrColumnas" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Coleccion de Objetos referidos a las columnas en la claúsula WHERE" + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " ArrayList<String>" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " arrValores" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Colección de Objetos referidos a los valores que deben tomar las columnas en la claúsula WHERE" + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Resultado del Método" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " " + _myParam.strNameClaseEnt + "" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " El método devuelve un objeto que equivale al registro de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " cuya PK es el parámetro ingresado. Si el no existe un registro con la PK especificada, el método devolverá " + miFor.fBoldAbrir + "NULL" + miFor.fBoldCerrar + "." + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            CFunciones.CrearArchivo(strTextoFuncion.ToString(), "obtenerObjetoArrArr.md", _myParam.PathDesktop + "\\Documentacion\\" + (String.IsNullOrEmpty(_myParam.strNameSpaceEnt) ? "" : _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\") + _myParam.strNameClaseRn + "\\");

            strTextoFuncion = new StringBuilder();
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTextoFuncion.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "Método obtenerLista()" + miFor.fBoldCerrar + miFor.fTitle1Cerrar);
                strTextoFuncion.AppendLine("");
            }
            strTextoFuncion.AppendLine("Método que permite obtener un lista de Objetos de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + ".");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Sintaxis" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTextoFuncion.AppendLine(" " + _myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            strTextoFuncion.AppendLine(" ArrayList<" + _myParam.strNameClaseEnt + "> miLis = rn.obtenerLista();");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("```");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle3Abrir + miFor.fBoldAbrir + "Parámetros" + miFor.fBoldCerrar + miFor.fTitle3Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine("Éste método no recibe parámetros de entrada.");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Resultado del Método" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " ArrayList<" + _myParam.strNameClaseEnt + ">" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " El método devuelve una Lista de Objetos que equivale a TODOS los registros de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + ". Si el no existen registros en la tabla, el método devolverá " + miFor.fBoldAbrir + "NULL" + miFor.fBoldCerrar + "." + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            CFunciones.CrearArchivo(strTextoFuncion.ToString(), "obtenerLista.md", _myParam.PathDesktop + "\\Documentacion\\" + (String.IsNullOrEmpty(_myParam.strNameSpaceEnt) ? "" : _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\") + _myParam.strNameClaseRn + "\\");

            strTextoFuncion = new StringBuilder();
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTextoFuncion.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "Método obtenerLista(" + _myParam.strNameClaseEnt + ".Fields, String)" + miFor.fBoldCerrar + miFor.fTitle1Cerrar);
                strTextoFuncion.AppendLine("");
            }
            strTextoFuncion.AppendLine("Método que permite obtener una lista de Objetos de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " a partir de una claúsula " + miFor.fBoldAbrir + "WHERE" + miFor.fBoldCerrar + ".");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Sintaxis" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTextoFuncion.AppendLine(" " + _myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            strTextoFuncion.AppendLine(" ArrayList<" + _myParam.strNameClaseEnt + "> miLis = rn.obtenerLista(" + _myParam.strNameClaseEnt + ".Fields." + _myParam.dtColumns.Rows[1][0].ToString() + ", String.valueOf(miParam));");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("```");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle3Abrir + miFor.fBoldAbrir + "Parámetros" + miFor.fBoldCerrar + miFor.fTitle3Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Nombre" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " " + _myParam.strNameClaseEnt + ".Fields" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " miField" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Parámetro que puede obtiene los valores del ENUM Fileds, y que representa a las columnas de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + "." + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " String" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " miValue" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Representa el valor para el filtro aplicado sobre el Parámetro " + miFor.fBoldAbrir + "miField" + miFor.fBoldCerrar + "." + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Resultado del Método" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " ArrayList<" + _myParam.strNameClaseEnt + ">" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " El método devuelve una Lista de Objetos que equivale a los registros de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " que cumplan la claúsula WHERE creada por los parametros recibidos. Si el no existen registros en la tabla, el método devolverá " + miFor.fBoldAbrir + "NULL" + miFor.fBoldCerrar + "." + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            CFunciones.CrearArchivo(strTextoFuncion.ToString(), "obtenerListaFldStr.md", _myParam.PathDesktop + "\\Documentacion\\" + (String.IsNullOrEmpty(_myParam.strNameSpaceEnt) ? "" : _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\") + _myParam.strNameClaseRn + "\\");

            strTextoFuncion = new StringBuilder();
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTextoFuncion.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "Método obtenerLista(ArrayList<String>, ArrayList<String>)" + miFor.fBoldCerrar + miFor.fTitle1Cerrar);
                strTextoFuncion.AppendLine("");
            }
            strTextoFuncion.AppendLine("Método que permite obtener una lista de Objetos de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " a partir de varias claúsulas " + miFor.fBoldAbrir + "WHERE" + miFor.fBoldCerrar + " agrupadas en ArrayList.");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Sintaxis" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTextoFuncion.AppendLine(" " + _myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            strTextoFuncion.AppendLine(" ArrayList<String> arrColWhere = new ArrayList<String>()");
            strTextoFuncion.AppendLine(" arrColWhere.add(" + _myParam.strNameClaseEnt + ".Fields." + _myParam.dtColumns.Rows[1][0].ToString() + ".toString())");
            strTextoFuncion.AppendLine(" arrColWhere.add(" + _myParam.strNameClaseEnt + ".Fields." + _myParam.dtColumns.Rows[2][0].ToString() + ".toString())");
            strTextoFuncion.AppendLine(" ArrayList<String> arrValWhere = new ArrayList<String>()");
            strTextoFuncion.AppendLine(" arrValWhere.add(String.valueOf(miParam1))");
            strTextoFuncion.AppendLine(" arrValWhere.add(\"'FILTRO-QUE-QUERAMOS'\")");
            strTextoFuncion.AppendLine(" ArrayList<" + _myParam.strNameClaseEnt + "> miLis = rn.obtenerLista(arrColWhere, arrValWhere);");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("```");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle3Abrir + miFor.fBoldAbrir + "Parámetros" + miFor.fBoldCerrar + miFor.fTitle3Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Nombre" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " ArrayList<String>" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " arrColumnas" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Coleccion de Objetos referidos a las columnas en la claúsula WHERE" + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " ArrayList<String>" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " arrValores" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Colección de Objetos referidos a los valores que deben tomar las columnas en la claúsula WHERE" + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Resultado del Método" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " ArrayList<" + _myParam.strNameClaseEnt + ">" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " El método devuelve una Lista de Objetos que equivale a los registros de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " que cumplan la claúsula WHERE creada por los parametros recibidos. Si el no existen registros en la tabla, el método devolverá " + miFor.fBoldAbrir + "NULL" + miFor.fBoldCerrar + "." + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            CFunciones.CrearArchivo(strTextoFuncion.ToString(), "obtenerListaArrArr.md", _myParam.PathDesktop + "\\Documentacion\\" + (String.IsNullOrEmpty(_myParam.strNameSpaceEnt) ? "" : _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\") + _myParam.strNameClaseRn + "\\");

            strTextoFuncion = new StringBuilder();
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTextoFuncion.AppendLine(miFor.fTitle1Abrir + miFor.fBoldAbrir + "Método obtenerLista(ArrayList<String>, ArrayList<String>, String)" + miFor.fBoldCerrar);
                strTextoFuncion.AppendLine("");
            }
            strTextoFuncion.AppendLine("Método que permite obtener una lista de Objetos de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " a partir de varias claúsulas " + miFor.fBoldAbrir + "WHERE" + miFor.fBoldCerrar + " agrupadas en ArrayList y un parámetro de tipo String adicional.");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Sintaxis" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
            {
                strTexto.AppendLine("```");
                strTexto.AppendLine("#!java");
                strTexto.AppendLine("");
            }
            strTextoFuncion.AppendLine(" " + _myParam.strNameClaseRn + " rn = new " + _myParam.strNameClaseRn + "(getApplicationContext());");
            strTextoFuncion.AppendLine(" ArrayList<String> arrColWhere = new ArrayList<String>()");
            strTextoFuncion.AppendLine(" arrColWhere.add(" + _myParam.strNameClaseEnt + ".Fields." + _myParam.dtColumns.Rows[1][0].ToString() + ".toString())");
            strTextoFuncion.AppendLine(" arrColWhere.add(" + _myParam.strNameClaseEnt + ".Fields." + _myParam.dtColumns.Rows[2][0].ToString() + ".toString())");
            strTextoFuncion.AppendLine(" ArrayList<String> arrValWhere = new ArrayList<String>()");
            strTextoFuncion.AppendLine(" arrValWhere.add(String.valueOf(miParam1))");
            strTextoFuncion.AppendLine(" arrValWhere.add(\"'FILTRO-QUE-QUERAMOS'\")");
            strTextoFuncion.AppendLine(" ArrayList<" + _myParam.strNameClaseEnt + "> miLis = rn.obtenerLista(arrColWhere, arrValWhere, \" AND \"" + _myParam.strNameClaseEnt + ".Fields." + _myParam.dtColumns.Rows[2][0].ToString() + ".toString() + \" <> 'VALOR' \" ");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("```");
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle3Abrir + miFor.fBoldAbrir + "Parámetros" + miFor.fBoldCerrar + miFor.fTitle3Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + "" + miFor.fTableTdAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Nombre" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " ArrayList<String>" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " arrColumnas" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Coleccion de Objetos referidos a las columnas en la claúsula WHERE" + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " ArrayList<String>" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " arrValores" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Colección de Objetos referidos a los valores que deben tomar las columnas en la claúsula WHERE" + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " String" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " strParamAdicionales" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Cadena que va a ser agregada al final del QUERY, de tal forma que pueda implementarse nuevas claúsulas o funciones a la consulta." + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTitle2Abrir + miFor.fBoldAbrir + "Resultado del Método" + miFor.fBoldCerrar + miFor.fTitle2Cerrar);
            strTextoFuncion.AppendLine("");
            strTextoFuncion.AppendLine(miFor.fTableAbrir + " Tipo" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " Descripcion " + miFor.fTableTdCerrar + "");
            if (miFor.miTipo == cDocFormato.TipoDocFormato.BitBucketMarkDown)
                strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " --" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " -- " + miFor.fTableTdCerrar + "");
            strTextoFuncion.AppendLine("" + miFor.fTableTrAbrir + miFor.fTableTdAbrir + " ArrayList<" + _myParam.strNameClaseEnt + ">" + miFor.fHrefCerrar + "" + miFor.fTableTdAbrir + " El método devuelve una Lista de Objetos que equivale a los registros de la tabla " + miFor.fBoldCerrar + _myParam.strNameTabla + miFor.fBoldAbrir + " que cumplan la claúsula WHERE creada por los parametros recibidos y a la cadena adicional. Si el no existen registros en la tabla, el método devolverá " + miFor.fBoldAbrir + "NULL" + miFor.fBoldCerrar + "." + miFor.fTableTdCerrar + "");
            strTextoFuncion.Append(miFor.fTableCerrar);
            strTextoFuncion.AppendLine("");
            CFunciones.CrearArchivo(strTextoFuncion.ToString(), "obtenerListaArrArrStr.md", _myParam.PathDesktop + "\\Documentacion\\" + (String.IsNullOrEmpty(_myParam.strNameSpaceEnt) ? "" : _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\") + _myParam.strNameClaseRn + "\\");

            CFunciones.CrearArchivo(strTexto.ToString(), _myParam.strNameClaseRn + ".md", _myParam.PathDesktop + "\\Documentacion\\" + _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\");
            return _myParam.PathDesktop + "\\\\Documentacion\\" + _myParam.strNameSpaceEnt.Replace(".", "\\") + "\\" + _myParam.strNameClaseRn + ".md";
        }
    }
}