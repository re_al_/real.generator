﻿using ReAlFind_Control;
using System;
using System.Data;

namespace ReAl.Generator.App.AppDocumentation
{
    public class cDocParams
    {
        public ReAlListView miLista { get; set; }

        public String strPrefijoEnt { get; set; }

        public String strPrefijoRn { get; set; }

        public String strUrlWiki { get; set; }

        public DataTable dtColumns { get; set; }

        public String strNameTabla { get; set; }

        public String strNameBO { get; set; }

        public String strNameClaseEnt { get; set; }

        public String strNameClaseRn { get; set; }

        public String strNameSpaceEnt { get; set; }

        public String strNameSpaceRn { get; set; }

        public String PathDesktop { get; set; }
    }
}