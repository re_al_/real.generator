﻿using System;

namespace ReAl.Generator.App.AppDocumentation
{
    public class cDocFormato
    {
        public enum TipoDocFormato
        {
            MediaWiki,
            BitBucketMarkDown
        }

        public TipoDocFormato miTipo;
        public string fTitle1Abrir = "";
        public string fTitle1Cerrar = "";
        public string fTitle2Abrir = "";
        public string fTitle2Cerrar = "";
        public string fTitle3Abrir = "";
        public string fTitle3Cerrar = "";
        public string fBoldAbrir = "";
        public string fBoldCerrar = "";
        public string fCursiveAbrir = "";
        public string fCursiveCerrar = "";
        public string fLinea = "";
        public string fPunteo = "";

        public string fTableAbrir = "";
        public string fTableCerrar = "";
        public string fTableTrAbrir = "";
        public string fTableTrCerrar = "";
        public string fTableTdAbrir = "";
        public string fTableTdCerrar = "";
        public string fTableTdCerrarFinal = "";

        public string fHrefAbrir = "";
        public string fHrefCerrar = "";

        public string fImgConstructor = "";
        public string fImgPropiedad = "";
        public string fImgMetodo = "";
        public string fImgEvento = "";

        public cDocFormato(TipoDocFormato tipoDoc)
        {
            miTipo = tipoDoc;
            switch (tipoDoc)
            {
                case TipoDocFormato.MediaWiki:
                    cargarFormatoMediaWiki();
                    break;

                case TipoDocFormato.BitBucketMarkDown:
                    cargarFormatoBitBucketMarkDown();
                    break;
            }
        }

        private void cargarFormatoMediaWiki()
        {
            fTitle1Abrir = "= ";
            fTitle1Cerrar = " =";
            fTitle2Abrir = "== ";
            fTitle2Cerrar = " ==";
            fTitle3Abrir = "=== ";
            fTitle3Cerrar = " ===";
            fBoldAbrir = "'''";
            fBoldCerrar = "'''";
            fCursiveAbrir = "''";
            fCursiveCerrar = "''";
            fLinea = "----";
            fPunteo = "* ";

            fTableAbrir = "{|" + Environment.NewLine;
            fTableCerrar = Environment.NewLine + "|}";
            fTableTrAbrir = "|-" + Environment.NewLine;
            fTableTrCerrar = "";
            fTableTdAbrir = "|";
            fTableTdCerrar = "|";
            fTableTdCerrarFinal = "";

            fHrefAbrir = "[[";
            fHrefCerrar = "]]";

            fImgConstructor = "[[Archivo:javaAndroidConstructor.png]]"; //"[http://i.msdn.microsoft.com/dynimg/IC32771.gif]";
            fImgPropiedad = "[[Archivo:javaAndroidPropiedad.png]]"; //"[http://i.msdn.microsoft.com/dynimg/IC108523.gif]";
            fImgMetodo = "[[Archivo:javaAndroidMetodo.png]]"; //"[http://i.msdn.microsoft.com/dynimg/IC32771.gif]";
            fImgEvento = "[[Archivo:javaAndroidEVento.png]]"; //"[http://i.msdn.microsoft.com/dynimg/IC98802.gif]";
        }

        private void cargarFormatoBitBucketMarkDown()
        {
            fTitle1Abrir = "#";
            fTitle1Cerrar = "";
            fTitle2Abrir = "##";
            fTitle2Cerrar = "";
            fTitle3Abrir = "###";
            fTitle3Cerrar = "";
            fBoldAbrir = "**";
            fBoldCerrar = "**";
            fCursiveAbrir = "*";
            fCursiveCerrar = "*";
            fLinea = "----";
            fPunteo = "* ";

            fTableAbrir = " ";
            fTableCerrar = "";
            fTableTrAbrir = "";
            fTableTrCerrar = "";
            fTableTdAbrir = "|";
            fTableTdCerrar = "";
            fTableTdCerrarFinal = "";

            fHrefAbrir = "[";
            fHrefCerrar = "]";

            fImgConstructor = "!" + fHrefAbrir + "alt text" + fHrefCerrar + fHrefAbrir + "constructor" + fHrefCerrar + "";
            fImgPropiedad = "!" + fHrefAbrir + "alt text" + fHrefCerrar + fHrefAbrir + "propiedad" + fHrefCerrar + "";
            fImgMetodo = "!" + fHrefAbrir + "alt text" + fHrefCerrar + fHrefAbrir + "metodo" + fHrefCerrar + "";
            fImgEvento = "!" + fHrefAbrir + "alt text" + fHrefCerrar + fHrefAbrir + "evento" + fHrefCerrar + "";
        }
    }
}