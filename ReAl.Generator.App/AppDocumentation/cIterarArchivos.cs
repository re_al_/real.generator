﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;

namespace ReAl.Generator.App.AppDocumentation
{
    public static class cIterarArchivos
    {
        public static String strCreateClassHeader(String strNameSpace, String strClase)
        {
            StringBuilder strTexto = new StringBuilder();
            strTexto.AppendLine("#**" + strClase + " (Clase)**");
            strTexto.AppendLine("");
            strTexto.AppendLine("##**Jerarquia de Herencia**");
            strTexto.AppendLine("");
            strTexto.AppendLine("**Paquete:** " + strNameSpace);
            strTexto.AppendLine("");
            strTexto.AppendLine("**Clase:** " + strClase + ".class");
            strTexto.AppendLine("");
            strTexto.AppendLine("##**Sintaxis**");
            strTexto.AppendLine("");
            strTexto.AppendLine("```");
            strTexto.AppendLine("#!java");
            strTexto.AppendLine("");
            strTexto.AppendLine(strClase + " rn = new " + strClase + "();");
            strTexto.AppendLine("```");
            strTexto.AppendLine("");
            strTexto.AppendLine("##**Constructores**");
            strTexto.AppendLine("");
            strTexto.AppendLine("  Nombre | Descripcion ");
            strTexto.AppendLine(" -- | -- ");
            strTexto.AppendLine(" ![alt text][constructor] " + strClase + "() | Inicializa una nueva instancia de la clase. ");
            strTexto.AppendLine("");
            strTexto.AppendLine("##**Métodos**");
            strTexto.AppendLine("");
            strTexto.AppendLine(" Nombre | Descripcion ");
            strTexto.AppendLine(" -- | -- ");

            return strTexto.ToString();
        }

        public static String strCreateClassFooter(String strClase)
        {
            StringBuilder strTexto = new StringBuilder();
            strTexto.AppendLine("");
            strTexto.AppendLine("##**Comentarios**");
            strTexto.AppendLine("");
            strTexto.AppendLine(strClase + " es una Clase que representa...");
            strTexto.AppendLine("");
            strTexto.AppendLine("##**Ejemplos**");
            strTexto.AppendLine("");
            strTexto.AppendLine("No se han proporcionado ejemplos de Código para ésta Clase.");
            strTexto.AppendLine("");
            strTexto.AppendLine("##**Información de Versión**");
            strTexto.AppendLine("");
            strTexto.AppendLine("Versión 1.0");
            strTexto.AppendLine("");
            strTexto.AppendLine("##**Autor**");
            strTexto.AppendLine("");
            strTexto.AppendLine("[R. Alonzo Vera Arias]");
            strTexto.AppendLine("");
            strTexto.AppendLine("[constructor]: http://i.msdn.microsoft.com/dynimg/IC32771.gif");
            strTexto.AppendLine("[propiedad]: http://i.msdn.microsoft.com/dynimg/IC108523.gif");
            strTexto.AppendLine("[metodo]: http://i.msdn.microsoft.com/dynimg/IC32771.gif");
            strTexto.AppendLine("[evento]: http://i.msdn.microsoft.com/dynimg/IC98802.gif");
            strTexto.AppendLine("[R. Alonzo Vera Arias]: http://real7.somee.com");
            return strTexto.ToString();
        }

        public static String strAddFuncion(String strFuncion, String strDescripcion)
        {
            StringBuilder strTexto = new StringBuilder();
            strTexto.Append(" ![alt text][metodo]  [" + strFuncion + "] | " + (String.IsNullOrEmpty(strDescripcion) ? "Éste método aún no posee Descripción." : strDescripcion));
            return strTexto.ToString();
        }

        public static DataTable obtenerFuncionesArchivo(String strPath, String strExtension, Boolean bLeerSubDirectorios)
        {
            DataTable dtFunciones = new DataTable("ReAlClassDocumentacion");
            DataColumn dc = new DataColumn("Clase", typeof(String));
            dtFunciones.Columns.Add(dc);
            dc = new DataColumn("Funcion", typeof(String));
            dtFunciones.Columns.Add(dc);
            dc = new DataColumn("Descripcion", typeof(String));
            dtFunciones.Columns.Add(dc);

            string[] filePaths = null;
            if (bLeerSubDirectorios)
                filePaths = Directory.GetFiles(strPath, "*." + strExtension, SearchOption.AllDirectories);
            else
                filePaths = Directory.GetFiles(strPath, "*." + strExtension, SearchOption.TopDirectoryOnly);

            ArrayList misArchivos = new ArrayList();
            misArchivos.AddRange(filePaths);

            String strLinea = "", strClase = "", strFuncion = "";
            foreach (string archivo in misArchivos)
            {
                StreamReader file = new StreamReader(archivo);
                while ((strLinea = file.ReadLine()) != null)
                {
                    //Procesamos la linea
                    if (strLinea.Trim().ToUpper().StartsWith("PUBLIC CLASS"))
                        strClase = strLinea.ToUpper().Replace("PUBLIC CLASS", "").Trim().ToLower();
                    else
                    {
                        //Buscamos los métodos
                        if (bElegirFunciones(strLinea))
                        {
                            strFuncion = strLinea.ToUpper().Replace("PROTECTED ", "").Replace("PUBLIC ", "").Replace("PRIVATE ", "").Trim().ToLower();

                            //Agregamos al DataTable
                            DataRow dr = dtFunciones.NewRow();
                            dr["Clase"] = strClase;
                            dr["Funcion"] = strFuncion;
                            dr["Descripcion"] = "";
                            dtFunciones.Rows.Add(dr);
                        }
                    }
                }

                file.Close();
            }

            return dtFunciones;
        }

        private static Boolean bElegirFunciones(String strLinea)
        {
            string[] funcNivel = new string[] { "PRIVATE", "PUBLIC", "PROTECTED" }; ;
            ArrayList miFuncNivel = new ArrayList();
            miFuncNivel.AddRange(funcNivel);
            string[] funcTipo = new string[] { "INT", "VOID", "STRING", "STING[]", "ARRAYLIST", "DOUBLE", "FLOAT", "BOOLEAN", "BOOL" }; ;
            ArrayList miFuncTipo = new ArrayList();
            miFuncTipo.AddRange(funcTipo);

            foreach (string strNivel in miFuncNivel)
            {
                foreach (string strTipo in miFuncTipo)
                {
                    if (strLinea.Trim().ToUpper().StartsWith(strNivel + " " + strTipo) && strLinea.Contains("(") && strLinea.Contains(")"))
                        return true;
                }
            }
            return false;
        }
    }
}