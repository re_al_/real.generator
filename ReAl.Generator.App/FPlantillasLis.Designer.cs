﻿namespace ReAl.Generator.App
{
    partial class FPlantillasLis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPlantillasLis));
            this.btnImportarPlantillas = new System.Windows.Forms.Button();
            this.btnExportarPlantillas = new System.Windows.Forms.Button();
            this.btnCopiarPlantilla = new System.Windows.Forms.Button();
            this.btnNuevaPlantilla = new System.Windows.Forms.Button();
            this.dtgPlantillas = new System.Windows.Forms.DataGridView();
            this.btnEliminarPlantillas = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPlantillas)).BeginInit();
            this.SuspendLayout();
            // 
            // btnImportarPlantillas
            // 
            this.btnImportarPlantillas.Location = new System.Drawing.Point(83, 520);
            this.btnImportarPlantillas.Name = "btnImportarPlantillas";
            this.btnImportarPlantillas.Size = new System.Drawing.Size(65, 37);
            this.btnImportarPlantillas.TabIndex = 2;
            this.btnImportarPlantillas.TabStop = false;
            this.btnImportarPlantillas.Text = "Importar Plantillas";
            this.btnImportarPlantillas.UseVisualStyleBackColor = true;
            this.btnImportarPlantillas.Click += new System.EventHandler(this.btnImportarPlantillas_Click);
            // 
            // btnExportarPlantillas
            // 
            this.btnExportarPlantillas.Location = new System.Drawing.Point(12, 520);
            this.btnExportarPlantillas.Name = "btnExportarPlantillas";
            this.btnExportarPlantillas.Size = new System.Drawing.Size(65, 37);
            this.btnExportarPlantillas.TabIndex = 1;
            this.btnExportarPlantillas.TabStop = false;
            this.btnExportarPlantillas.Text = "Exportar Plantillas";
            this.btnExportarPlantillas.UseVisualStyleBackColor = true;
            // 
            // btnCopiarPlantilla
            // 
            this.btnCopiarPlantilla.Location = new System.Drawing.Point(472, 520);
            this.btnCopiarPlantilla.Name = "btnCopiarPlantilla";
            this.btnCopiarPlantilla.Size = new System.Drawing.Size(147, 37);
            this.btnCopiarPlantilla.TabIndex = 4;
            this.btnCopiarPlantilla.Text = "Copiar Plantilla";
            this.btnCopiarPlantilla.UseVisualStyleBackColor = true;
            this.btnCopiarPlantilla.Click += new System.EventHandler(this.btnCopiarPlantilla_Click);
            // 
            // btnNuevaPlantilla
            // 
            this.btnNuevaPlantilla.Location = new System.Drawing.Point(625, 520);
            this.btnNuevaPlantilla.Name = "btnNuevaPlantilla";
            this.btnNuevaPlantilla.Size = new System.Drawing.Size(147, 37);
            this.btnNuevaPlantilla.TabIndex = 5;
            this.btnNuevaPlantilla.Text = "Nueva Plantilla";
            this.btnNuevaPlantilla.UseVisualStyleBackColor = true;
            this.btnNuevaPlantilla.Click += new System.EventHandler(this.btnNuevaPlantilla_Click);
            // 
            // dtgPlantillas
            // 
            this.dtgPlantillas.AllowUserToAddRows = false;
            this.dtgPlantillas.AllowUserToDeleteRows = false;
            this.dtgPlantillas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgPlantillas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPlantillas.Dock = System.Windows.Forms.DockStyle.Top;
            this.dtgPlantillas.Location = new System.Drawing.Point(0, 0);
            this.dtgPlantillas.Name = "dtgPlantillas";
            this.dtgPlantillas.ReadOnly = true;
            this.dtgPlantillas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgPlantillas.Size = new System.Drawing.Size(784, 510);
            this.dtgPlantillas.TabIndex = 0;
            this.dtgPlantillas.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dtgPlantillas_MouseDoubleClick);
            // 
            // btnEliminarPlantillas
            // 
            this.btnEliminarPlantillas.Location = new System.Drawing.Point(154, 520);
            this.btnEliminarPlantillas.Name = "btnEliminarPlantillas";
            this.btnEliminarPlantillas.Size = new System.Drawing.Size(147, 37);
            this.btnEliminarPlantillas.TabIndex = 3;
            this.btnEliminarPlantillas.Text = "&Eliminar Plantilla";
            this.btnEliminarPlantillas.UseVisualStyleBackColor = true;
            this.btnEliminarPlantillas.Click += new System.EventHandler(this.btnEliminarPlantillas_Click);
            // 
            // FPlantillasLis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.btnEliminarPlantillas);
            this.Controls.Add(this.btnImportarPlantillas);
            this.Controls.Add(this.btnExportarPlantillas);
            this.Controls.Add(this.btnCopiarPlantilla);
            this.Controls.Add(this.btnNuevaPlantilla);
            this.Controls.Add(this.dtgPlantillas);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FPlantillasLis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Plantillas";
            this.Load += new System.EventHandler(this.FPlantillasLis_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgPlantillas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnImportarPlantillas;
        internal System.Windows.Forms.Button btnExportarPlantillas;
        internal System.Windows.Forms.Button btnCopiarPlantilla;
        internal System.Windows.Forms.Button btnNuevaPlantilla;
        internal System.Windows.Forms.DataGridView dtgPlantillas;
        private System.Windows.Forms.Button btnEliminarPlantillas;
    }
}