﻿using ClosedXML.Excel;
using FastColoredTextBoxNS;
using GraphVizWrapper;
using GraphVizWrapper.Commands;
using GraphVizWrapper.Queries;
using ReAl.Class.Entidades;
using ReAl.Class.Modelo;
using ReAl.Generator.App.AppClass;
using ReAl.Generator.App.AppCore;
using ReAl.Generator.App.AppDataBase;
using ReAl.Generator.App.Properties;
using ReAlFind_Control;
using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using ReAl.Generator.App.AppGenerators;

namespace ReAl.Generator.App
{
    public partial class FPrincipal : Form
    {
        internal string strCabeceraPlantillaCodigo = "";
        internal string strCabecera = "";

        public FPrincipal()
        {
            InitializeComponent();
        }

        private void FPrincipal_Load(object sender, EventArgs e)
        {
            string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            Text = "ReAl Code Generator - Version " + version;

            FLogin frm = new FLogin();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                CTablasColumnas.ObtenerListadoTablas(ref ReAlListView1, ref ReAlListView2, ref ReAlListView3,
                    ref ReAlListView4, txtListadoBdFiltro.Text, chkSeleccionarTodo.Checked);

                toolStripStatusLabel1.Text = "Base de Datos: " + Principal.Catalogo;
                toolStripStatusLabel2.Text = "Servidor: " + Principal.Servidor;

                cmbAPIs.SelectedIndex = 0;
                cmbPlantilla.SelectedIndex = 0;
                cmbTipoColumnas.SelectedIndex = 0;

                foreach (string cs in Enum.GetNames(typeof(TipoEntorno)))
                    cmbEntorno.Items.Add(cs);

                switch (Principal.DBMS)
                {
                    case TipoBD.SQLServer:
                    case TipoBD.SQLServer2000:
                        grpSpIns.Enabled = true;
                        grpSpUpd.Enabled = true;
                        chkSPInsFecCre.Checked = false;
                        chkSPInsUsuCre.Checked = true;
                        chkSPInsFecCreGetDate.Checked = true;
                        chkSPInsApiEstado.Checked = true;
                        chkSPUpdUsuMod.Checked = true;
                        chkSPUpdFecMod.Checked = false;
                        chkSPUpdFecModGetDate.Checked = true;

                        chkUsarSps.Visible = true;
                        chkUsarSpsCheck.Visible = true;
                        chkUsarSpsSelect.Visible = true;
                        chkParametroPa.Visible = true;
                        chkUsarSpsIdentity.Visible = true;
                        chkDataAnnotations.Visible = true;
                        chkEntidadesEncriptacion.Visible = true;

                        break;

                    case TipoBD.Oracle:
                        chkSPInsFecCre.Checked = false;
                        chkSPInsUsuCre.Checked = false;
                        chkSPInsFecCreGetDate.Checked = false;
                        chkSPInsApiEstado.Checked = false;
                        chkSPUpdUsuMod.Checked = false;
                        chkSPUpdFecMod.Checked = false;
                        chkSPUpdFecModGetDate.Checked = false;
                        grpSpIns.Enabled = false;
                        grpSpUpd.Enabled = false;

                        chkUsarSpsCheck.Checked = false;
                        chkUsarSps.Visible = true;
                        chkUsarSpsCheck.Visible = true;
                        chkUsarSpsSelect.Visible = true;
                        chkParametroPa.Visible = true;
                        chkUsarSpsIdentity.Visible = true;
                        chkDataAnnotations.Visible = true;
                        chkEntidadesEncriptacion.Visible = true;

                        break;

                    case TipoBD.PostgreSQL:
                        grpSpIns.Enabled = true;
                        grpSpUpd.Enabled = true;
                        chkSPInsFecCre.Checked = false;
                        chkSPInsUsuCre.Checked = true;
                        chkSPInsFecCreGetDate.Checked = true;
                        chkSPInsApiEstado.Checked = true;
                        chkSPUpdUsuMod.Checked = true;
                        chkSPUpdFecMod.Checked = false;
                        chkSPUpdFecModGetDate.Checked = true;

                        chkUsarSpsCheck.Checked = false;
                        chkTriggerEncrypt.Checked = false;
                        chkTriggerEncrypt.Visible = false;

                        chkBDOpcionesTriggers.Text = "Triggers";

                        chkUsarSps.Visible = true;
                        chkUsarSpsCheck.Visible = true;
                        chkUsarSpsSelect.Visible = true;
                        chkParametroPa.Visible = true;
                        chkUsarSpsIdentity.Visible = true;
                        chkDataAnnotations.Visible = true;
                        chkEntidadesEncriptacion.Visible = true;

                        break;

                    case TipoBD.MySql:
                        grpSpIns.Enabled = true;
                        grpSpUpd.Enabled = true;
                        chkSPInsFecCre.Checked = false;
                        chkSPInsUsuCre.Checked = true;
                        chkSPInsFecCreGetDate.Checked = true;
                        chkSPInsApiEstado.Checked = true;
                        chkSPUpdUsuMod.Checked = true;
                        chkSPUpdFecMod.Checked = false;
                        chkSPUpdFecModGetDate.Checked = true;

                        chkUsarSpsCheck.Checked = false;
                        chkTriggerEncrypt.Checked = false;
                        chkTriggerEncrypt.Visible = false;

                        chkBDOpcionesTriggers.Text = "Triggers";

                        chkUsarSps.Visible = true;
                        chkUsarSpsCheck.Visible = true;
                        chkUsarSpsSelect.Visible = true;
                        chkParametroPa.Visible = true;
                        chkUsarSpsIdentity.Visible = true;
                        chkDataAnnotations.Visible = true;
                        chkEntidadesEncriptacion.Visible = true;

                        break;

                    case TipoBD.SQLite:
                        grpColumnasTriggers.Enabled = false;
                        //chkUsarSps.Visible = False
                        //chkUsarSpsCheck.Visible = False
                        //chkUsarSpsSelect.Visible = False
                        chkParametroPa.Visible = false;
                        chkUsarSpsIdentity.Visible = false;
                        chkDataAnnotations.Visible = false;
                        chkEntidadesEncriptacion.Visible = false;

                        chkReAlUnitTest.Checked = false;
                        break;
                }

                CargarCmbPlantillas();
                CargarCmbSnippets();
                if (!string.IsNullOrEmpty(Principal.Plantilla))
                    cmbPlantilla.SelectedValue = Principal.Plantilla;
            }
            else
                Application.Exit();
        }

        private void CargarCmbSnippets()
        {
            try
            {
                foreach (string cs in Enum.GetNames(typeof(CodeSnippet)))
                    cmbSnippet.Items.Add(cs);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CargarCmbPlantillas()
        {
            try
            {
                rnClaPlantilla rn = new rnClaPlantilla();
                rn.CargarComboBox(ref cmbPlantilla, entClaPlantilla.Fields.idPlantilla, entClaPlantilla.Fields.descPlantilla);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmbPlantilla_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarDatosPlantilla();
        }

        private void CargarDatosPlantilla()
        {
            if ((cmbPlantilla.SelectedValue == null))
                return;

            //Obtenemos el Diccionario
            rnClaDetalle rn = new rnClaDetalle();
            DataTable dt = rn.CargarDataTable(entClaDetalle.Fields.idPlantilla, cmbPlantilla.SelectedValue);

            foreach (DataRow dr in dt.Rows)
            {
                Control ctr = this;
                do
                {
                    ctr = GetNextControl(ctr, true);
                    if (ctr != null && dr[entClaDetalle.Fields.propiedad.ToString()].ToString() == ctr.Name)
                    {
                        TextBox txt = ctr as TextBox;
                        if (txt != null)
                            txt.Text = dr[entClaDetalle.Fields.valor.ToString()].ToString();

                        CheckBox chk = ctr as CheckBox;
                        if (chk != null)
                            chk.Checked = (dr[entClaDetalle.Fields.valor.ToString()].ToString() == "1");

                        ComboBox cmb = ctr as ComboBox;
                        if (cmb != null)
                            cmb.SelectedItem = dr[entClaDetalle.Fields.valor.ToString()].ToString();

                        RadioButton rdb = ctr as RadioButton;
                        if (rdb != null)
                            rdb.Checked = (dr[entClaDetalle.Fields.valor.ToString()].ToString() == "1");
                    }
                } while (ctr != null);
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            CTablasColumnas.ObtenerListadoTablas(ref ReAlListView1, ref ReAlListView2, ref ReAlListView3,
                ref ReAlListView4, txtListadoBdFiltro.Text, chkSeleccionarTodo.Checked);
        }

        private void chkSeleccionarTodo_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i <= ReAlListView1.Items.Count - 1; i++)
                ReAlListView1.Items[i].Checked = chkSeleccionarTodo.Checked;

            for (int i = 0; i <= ReAlListView2.Items.Count - 1; i++)
                ReAlListView2.Items[i].Checked = chkSeleccionarTodo.Checked;

            for (int i = 0; i <= ReAlListView3.Items.Count - 1; i++)
                ReAlListView3.Items[i].Checked = chkSeleccionarTodo.Checked;

            for (int i = 0; i <= ReAlListView4.Items.Count - 1; i++)
                ReAlListView4.Items[i].Checked = chkSeleccionarTodo.Checked;
        }

        private void btnCLA_Click(object sender, EventArgs e)
        {
            //Generar Clases
            try
            {
                if (!chkMoverResultados.Checked && string.IsNullOrEmpty(txtPathProyecto.Text))
                {
                    Exception exp = new Exception("No se ha especificado el Directorio de Salida");
                    throw exp;
                }

                CMainGenerator cPrincipal = new CMainGenerator();
                cPrincipal.ReAlClass(this);

                tabControl1.SelectedTab = tabMainPage3;
                if (MessageBox.Show(this,
                        "Se han creado satisfactoriamente los archivos\r\n\r\n¿Desea abrir la carpeta de destino?",
                        "Exito!", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\";
                    Process.Start("explorer.exe", path);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message + "\r\n\r\n" + ex.StackTrace, "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
        }

        private void configurarConexionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FLogin frm = new FLogin();
            frm.ShowDialog();
            chkSeleccionarTodo.Checked = false;

            chkSeleccionarTodo.Checked = true;
            CTablasColumnas.ObtenerListadoTablas(ref ReAlListView1, ref ReAlListView2, ref ReAlListView3,
                ref ReAlListView4, txtListadoBdFiltro.Text, chkSeleccionarTodo.Checked);
        }

        private void plantillasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FPlantillasLis frm = new FPlantillasLis();
            frm.ShowDialog();
        }

        private void CopiarPlantillas(string strProjectTemplatesPath, string strItemTemplatesPath)
        {
            if ((!Directory.Exists(strProjectTemplatesPath)))
                Directory.CreateDirectory(strProjectTemplatesPath);

            if ((!File.Exists(strProjectTemplatesPath + "ReAl Bitbucket.zip")))
                File.WriteAllBytes(strProjectTemplatesPath + "ReAl Bitbucket.zip", Resources.ReAl_BitBucket);
            if ((!File.Exists(strProjectTemplatesPath + "ReAl Conn FireBird.zip")))
                File.WriteAllBytes(strProjectTemplatesPath + "ReAl Conn FireBird.zip", Resources.ReAl_Conn_FireBird);
            if ((!File.Exists(strProjectTemplatesPath + "ReAl Conn PostgreSql.zip")))
                File.WriteAllBytes(strProjectTemplatesPath + "ReAl Conn PostgreSql.zip", Resources.ReAl_Conn_PostgreSql);
            if ((!File.Exists(strProjectTemplatesPath + "ReAl Conn SqlServer.zip")))
                File.WriteAllBytes(strProjectTemplatesPath + "ReAl Conn SqlServer.zip", Resources.ReAl_Conn_SqlServer);
            if ((!File.Exists(strProjectTemplatesPath + "ReAl Extension Methods.zip")))
                File.WriteAllBytes(strProjectTemplatesPath + "ReAl Extension Methods.zip", Resources.ReAl_Extension_Methods);
            if ((!File.Exists(strProjectTemplatesPath + "ReAl Utils.zip")))
                File.WriteAllBytes(strProjectTemplatesPath + "ReAl Utils.zip", Resources.ReAl_Utils);
            if ((!File.Exists(strProjectTemplatesPath + "ReAl Conn Oracle.zip")))
                File.WriteAllBytes(strProjectTemplatesPath + "ReAl Conn Oracle.zip", Resources.ReAl_Conn_Oracle);
            if ((!File.Exists(strProjectTemplatesPath + "ReAl Conn SqlLite.zip")))
                File.WriteAllBytes(strProjectTemplatesPath + "ReAl Conn SqlLite.zip", Resources.ReAl_Conn_SqlLite);
            if ((!File.Exists(strProjectTemplatesPath + "ReAl Class for WinForms Projects.zip")))
                File.WriteAllBytes(strProjectTemplatesPath + "ReAl Class for WinForms Projects.zip", Resources.ReAl_Class_for_WinForms_Projects);
            if ((!File.Exists(strProjectTemplatesPath + "ReAl WinForms DevExpress for PostgreSql.zip")))
                File.WriteAllBytes(strProjectTemplatesPath + "ReAl WinForms DevExpress for PostgreSql.zip", Resources.ReAl_WinForms_DevExpress_for_PostgreSql);
            if ((!File.Exists(strProjectTemplatesPath + "ReAl Winforms DevExpress for SqlServer.zip")))
                File.WriteAllBytes(strProjectTemplatesPath + "ReAl Winforms DevExpress for SqlServer.zip", Resources.ReAl_Winforms_DevExpress_for_SqlServer);
            if ((!File.Exists(strProjectTemplatesPath + "ReAl Class for WinForms (Base Class).zip")))
                File.WriteAllBytes(strProjectTemplatesPath + "ReAl Class for WinForms (Base Class).zip", Resources.ReAl_Class_for_WinForms__Base_Class_);
            if ((!File.Exists(strProjectTemplatesPath + "ReAl Class for WebForms (Base Class).zip")))
                File.WriteAllBytes(strProjectTemplatesPath + "ReAl Class for WebForms (Base Class).zip", Resources.ReAl_Class_for_WebForms__Base_Class_);
            if ((!File.Exists(strProjectTemplatesPath + "ReAl WebForms with BootStrap.zip")))
                File.WriteAllBytes(strProjectTemplatesPath + "ReAl WebForms with BootStrap.zip", Resources.ReAl_WebForms_with_BootStrap);
            if ((!File.Exists(strProjectTemplatesPath + "ReAl WebForms with BootStrap MsSqlServer.zip")))
                File.WriteAllBytes(strProjectTemplatesPath + "ReAl WebForms with BootStrap MsSqlServer.zip", Resources.ReAl_WebForms_with_BootStrap_MsSqlServer);

            if ((!File.Exists(strItemTemplatesPath + "INTEGRATE WebBootstrap ABM.zip")))
                File.WriteAllBytes(strItemTemplatesPath + "INTEGRATE WebBootstrap ABM.zip", Resources.INTEGRATE_WebBootstrap_ABM);
        }

        private void vS2010ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string projectPath = Path.Combine(Environment.ExpandEnvironmentVariables("%userprofile%"), "Documents") + "\\Visual Studio 2010\\Templates\\ProjectTemplates\\Visual C#\\";
            string itemPath = Path.Combine(Environment.ExpandEnvironmentVariables("%userprofile%"), "Documents") + "\\Visual Studio 2010\\Templates\\ItemTemplates\\Visual Web Developer\\";
            CopiarPlantillas(projectPath, itemPath);
        }

        private void vS2012ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string projectPath = Path.Combine(Environment.ExpandEnvironmentVariables("%userprofile%"), "Documents") + "\\Visual Studio 2012\\Templates\\ProjectTemplates\\Visual C#\\";
            string itemPath = Path.Combine(Environment.ExpandEnvironmentVariables("%userprofile%"), "Documents") + "\\Visual Studio 2012\\Templates\\ItemTemplates\\Visual Web Developer\\";
            CopiarPlantillas(projectPath, itemPath);
        }

        private void vS2013ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string projectPath = Path.Combine(Environment.ExpandEnvironmentVariables("%userprofile%"), "Documents") + "\\Visual Studio 2013\\Templates\\ProjectTemplates\\Visual C#\\";
            string itemPath = Path.Combine(Environment.ExpandEnvironmentVariables("%userprofile%"), "Documents") + "\\Visual Studio 2013\\Templates\\ItemTemplates\\Visual Web Developer\\";
            CopiarPlantillas(projectPath, itemPath);
        }

        private void vS2015ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string projectPath = Path.Combine(Environment.ExpandEnvironmentVariables("%userprofile%"), "Documents") + "\\Visual Studio 2015\\Templates\\ProjectTemplates\\Visual C#\\";
            string itemPath = Path.Combine(Environment.ExpandEnvironmentVariables("%userprofile%"), "Documents") + "\\Visual Studio 2015\\Templates\\ItemTemplates\\Visual Web Developer\\";
            CopiarPlantillas(projectPath, itemPath);
        }

        private void vS2017ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string projectPath = Path.Combine(Environment.ExpandEnvironmentVariables("%userprofile%"), "Documents") + "\\Visual Studio 2017\\Templates\\ProjectTemplates\\Visual C#\\";
            string itemPath = Path.Combine(Environment.ExpandEnvironmentVariables("%userprofile%"), "Documents") + "\\Visual Studio 2017\\Templates\\ItemTemplates\\Visual Web Developer\\";
            CopiarPlantillas(projectPath, itemPath);
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtListadoBdFiltro_TextChanged(object sender, EventArgs e)
        {
            CTablasColumnas.ObtenerListadoTablas(ref ReAlListView1, ref ReAlListView2, ref ReAlListView3,
                ref ReAlListView4, txtListadoBdFiltro.Text, chkSeleccionarTodo.Checked);
        }

        private void btnSP_Click(object sender, EventArgs e)
        {
            try
            {
                txtPreview.Text = "";
                txtPreview.Language = Language.SQL;
                cBaseDatosSp cSp = new cBaseDatosSp();
                cSp.CrearSPs(this);
                tabControl1.SelectedTab = tabMainPage3;

                MessageBox.Show(this, "Se han creado satisfactoriamente los procedimientos almacenados", "Exito!", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message + "\r\n\r\n" + ex.StackTrace, "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
        }

        private void btnBDCrearTriggers_Click(object sender, EventArgs e)
        {
            try
            {
                cBaseDatosTriggersColumnas clase = new cBaseDatosTriggersColumnas();
                string strResultado = null;
                txtPreview.Language = Language.SQL;

                strResultado = clase.CrearTriggersColumnas(this);
                txtPreview.Text = strResultado;

                tabControl1.SelectedTab = tabMainPage3;

                MessageBox.Show(this, "Se han creado satisfactoriamente los trigger y/o columnas", "Exito!", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message + "\r\n\r\n" + ex.StackTrace, "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
        }

        private void btnGralAbrirCarpeta_Click(object sender, EventArgs e)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\";
            Process.Start("explorer.exe", path);
        }

        private void btnBDExportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\";
                var dtExportar = CFuncionesBDs.CargarDataTable(txtBDExportarExcel.Text);

                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dtExportar, "Reporte");
                    //Para mostrar o no los filtros en la cabecera
                    wb.Worksheets.Worksheet(1).Table("Table1").ShowAutoFilter = true;
                    wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    wb.Style.Font.Bold = true;

                    wb.SaveAs(path + "Reporte.xlsx");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message + "\r\n\r\n" + ex.StackTrace, "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
        }

        private void btnSnippet_Click(object sender, EventArgs e)
        {
            try
            {
                ReAlListView MyReAlListView = new ReAlListView();
                switch (tabBDObjects.SelectedIndex)
                {
                    case 0:
                        MyReAlListView = ReAlListView1;
                        break;

                    case 1:
                        MyReAlListView = ReAlListView2;
                        break;

                    case 2:
                        MyReAlListView = ReAlListView3;
                        break;
                }

                //Los Enums
                CodeSnippet miSnippet;
                if (!Enum.TryParse(cmbSnippet.SelectedItem.ToString(), out miSnippet))
                    throw new Exception("No se pudo convertir el valor para el Snippet");

                TipoEntorno miEntorno;
                if (!Enum.TryParse(cmbEntorno.SelectedItem.ToString(), out miEntorno))
                    throw new Exception("No se pudo convertir el valor para el Entorno");

                var strTexto = new StringBuilder();
                for (int i = 0; i <= MyReAlListView.Items.Count - 1; i++)
                {
                    if (MyReAlListView.Items[i].Checked)
                    {
                        string idTabla = MyReAlListView.Items[i].SubItems[1].Text;
                        string nameTabla = MyReAlListView.Items[i].SubItems[0].Text;
                        string nameSchema = MyReAlListView.Items[i].SubItems[2].Text;
                        //string nameClase = nameTabla[0].ToString().ToUpper() + nameTabla.Substring(1, nameTabla.Length - 1);
                        string nameClase = nameTabla.Replace("_", "").Substring(0, 1).ToUpper() +
                                           nameTabla.Replace("_", "").Substring(1, 2).ToLower() +
                                           nameTabla.Replace("_", "").Substring(3, 1).ToUpper() +
                                           nameTabla.Replace("_", "").Substring(4).ToLower();

                        //Obtenemos las columnas
                        DataTable dtColumns = CTablasColumnas.ObtenerColumnasTabla(miEntorno, this, nameSchema, idTabla);
                        var cWin = new cWinForms(this);

                        switch (miSnippet)
                        {
                            case CodeSnippet.BootStrap_Abm_WebForms:
                            case CodeSnippet.BootStrap_Doc_WebForms:
                            case CodeSnippet.BootStrap_Datagrid_WebForms:
                                var claseSnippet = new CCodeSnippets(txtPrefijoEntidades.Text, nameClase, nameTabla);
                                strTexto.AppendLine(claseSnippet.CrearAsignacion(dtColumns, miSnippet, false));
                                break;

                            case CodeSnippet.NetCore_Scaffoldind_Controllers:
                                strTexto.AppendLine("dotnet aspnet-codegenerator --project . controller --force --controllerName " + nameClase.ToPascalCase() + "Controller --model " + txtInfProyectoClass.Text + "." + txtNamespaceEntidades.Text + "." + nameClase.ToPascalCase() + " --dataContext ReAlDbContext --relativeFolderPath .\\Controllers\\ --useDefaultLayout --referenceScriptLibraries ");
                                strTexto.AppendLine("");
                                break;

                            case CodeSnippet.NetCore_Scaffoldind_Views_Details:
                                strTexto.AppendLine("dotnet aspnet-codegenerator view Details Details -outDir Views/" + nameClase.ToPascalCase() + " --project . --model " + txtInfProyectoClass.Text + "." + txtNamespaceEntidades.Text + "." + nameClase.ToPascalCase() + " --dataContext ReAlDbContext --useDefaultLayout --referenceScriptLibraries --force");
                                strTexto.AppendLine("");
                                break;

                            case CodeSnippet.NetCore_Scaffoldind_Views_List:
                                strTexto.AppendLine("dotnet aspnet-codegenerator view Index List -outDir Views/" + nameClase.ToPascalCase() + " --project . --model " + txtInfProyectoClass.Text + "." + txtNamespaceEntidades.Text + "." + nameClase.ToPascalCase() + " --dataContext ReAlDbContext --useDefaultLayout --referenceScriptLibraries --force");
                                strTexto.AppendLine("");
                                break;

                            case CodeSnippet.NetCore_Scaffoldind_Views_Create:
                                strTexto.AppendLine("dotnet aspnet-codegenerator view Create Create -outDir Views/" + nameClase.ToPascalCase() + " --project . --model " + txtInfProyectoClass.Text + "." + txtNamespaceEntidades.Text + "." + nameClase.ToPascalCase() + " --dataContext ReAlDbContext --useDefaultLayout --referenceScriptLibraries --force");
                                strTexto.AppendLine("");
                                break;

                            case CodeSnippet.NetCore_Scaffoldind_Views_Edit:
                                strTexto.AppendLine("dotnet aspnet-codegenerator view Edit Edit -outDir Views/" + nameClase.ToPascalCase() + " --project . --model " + txtInfProyectoClass.Text + "." + txtNamespaceEntidades.Text + "." + nameClase.ToPascalCase() + " --dataContext ReAlDbContext --useDefaultLayout --referenceScriptLibraries --force");
                                strTexto.AppendLine("");
                                break;

                            case CodeSnippet.NetCore_Scaffoldind_Views_Delete:
                                strTexto.AppendLine("dotnet aspnet-codegenerator view Delete Delete -outDir Views/" + nameClase.ToPascalCase() + " --project . --model " + txtInfProyectoClass.Text + "." + txtNamespaceEntidades.Text + "." + nameClase.ToPascalCase() + " --dataContext ReAlDbContext --useDefaultLayout --referenceScriptLibraries --force");
                                strTexto.AppendLine("");
                                break;

                            case CodeSnippet.WebForms_List:
                            case CodeSnippet.WebForms_ABM:
                            case CodeSnippet.WebForms_Misxtos:
                            case CodeSnippet.WebForms_Control_ABM:

                                break;

                            case CodeSnippet.WinForms_Form_ABM:
                                strTexto.AppendLine("--WinForms Form ABM");
                                strTexto.AppendLine(cWin.CrearFormulario(dtColumns, nameClase, nameTabla));
                                strTexto.AppendLine(cWin.CrearFormularioDesigner(dtColumns, nameClase, nameTabla));
                                strTexto.AppendLine(cWin.CrearFormularioResx(nameClase, nameTabla));
                                strTexto.AppendLine("");
                                break;

                            case CodeSnippet.WinForms_SubForm_MDI:
                                strTexto.AppendLine("--WinForms Form MDI");
                                strTexto.AppendLine(cWin.CrearSubFormularioVB(dtColumns, nameClase, nameTabla));
                                strTexto.AppendLine(cWin.CrearSubFormularioDesignerVB(dtColumns, nameClase, nameTabla));
                                strTexto.AppendLine(cWin.CrearSubFormularioResxVB(nameClase, nameTabla));
                                strTexto.AppendLine("");
                                break;

                            case CodeSnippet.WinForms_DevExpress:

                                strTexto.AppendLine("--WinForms DevExpress Form");
                                strTexto.AppendLine(cWin.CrearDevFormularioDoc(dtColumns, nameClase, nameTabla));
                                strTexto.AppendLine(cWin.CrearDevFormularioDocDesigner(dtColumns, nameClase, nameTabla));
                                strTexto.AppendLine(cWin.CrearDevFormularioDocResx(nameClase, nameTabla));
                                strTexto.AppendLine("");
                                strTexto.AppendLine(cWin.CrearDevFormularioList(dtColumns, nameClase, nameTabla));
                                strTexto.AppendLine(cWin.CrearDevFormularioListDesigner(dtColumns, nameClase, nameTabla));
                                strTexto.AppendLine(cWin.CrearDevFormularioListResx(nameClase, nameTabla));
                                strTexto.AppendLine("");
                                break;

                            case CodeSnippet.WinForms_Control_Listado:
                                strTexto.AppendLine("--WinForms Control Listado");
                                strTexto.AppendLine(cWin.CrearCtr(dtColumns, nameClase, nameTabla));
                                strTexto.AppendLine(cWin.CrearCtrDesigner(nameClase, nameTabla));
                                strTexto.AppendLine(cWin.CrearCtrRecursos(nameClase, nameTabla));
                                strTexto.AppendLine("");
                                break;
                        }
                    }
                }

                switch (miSnippet)
                {
                    case CodeSnippet.INTEGRATE_Niveles_restriccion:
                        var claseSnippet = new CCodeSnippets();
                        strTexto.AppendLine(claseSnippet.CrearControlesRestriccion());
                        txtPreview.Language = Language.CSharp;
                        txtPreview.Text = strTexto.ToString();
                        tabControl1.SelectedTab = tabMainPage3;
                        break;

                    case CodeSnippet.NetCore_Scaffoldind_Controllers:
                        txtPreview.Language = Language.Custom;
                        txtPreview.Text = strTexto.ToString();
                        tabControl1.SelectedTab = tabMainPage3;
                        break;

                    default:
                        txtPreview.Language = Language.CSharp;
                        txtPreview.Text = strTexto.ToString();
                        tabControl1.SelectedTab = tabMainPage3;
                        break;
                }

                MessageBox.Show(this, "Se ha terminado de ejecutar el SNIPPET " + cmbSnippet.SelectedText, "Exito", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message + "\r\n\r\n" + ex.StackTrace, "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
        }

        private void VS2019ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string projectPath = Path.Combine(Environment.ExpandEnvironmentVariables("%userprofile%"), "Documents") + "\\Visual Studio 2019\\Templates\\ProjectTemplates\\Visual C#\\";
            string itemPath = Path.Combine(Environment.ExpandEnvironmentVariables("%userprofile%"), "Documents") + "\\Visual Studio 2019\\Templates\\ItemTemplates\\Visual Web Developer\\";
            CopiarPlantillas(projectPath, itemPath);
        }

        private void btnCrearCicloDeVida_Click(object sender, EventArgs e)
        {
            try
            {
                // These three instances can be injected via the IGetStartProcessQuery,
                //                                               IGetProcessStartInfoQuery and
                //                                               IRegisterLayoutPluginCommand interfaces

                var getStartProcessQuery = new GetStartProcessQuery();
                var getProcessStartInfoQuery = new GetProcessStartInfoQuery();
                var registerLayoutPluginCommand = new RegisterLayoutPluginCommand(getProcessStartInfoQuery, getStartProcessQuery);

                // GraphGeneration can be injected via the IGraphGeneration interface

                //Excel
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\";

                using (XLWorkbook wb = new XLWorkbook())
                {
                    var wrapper = new GraphGeneration(getStartProcessQuery,
                                              getProcessStartInfoQuery,
                                              registerLayoutPluginCommand);

                    //Iteramos las tablas
                    for (int i = 0; i <= ReAlListView1.Items.Count - 1; i++)
                    {
                        if (ReAlListView1.Items[i].Checked)
                        {
                            string idTabla = ReAlListView1.Items[i].SubItems[1].Text;
                            string nameTabla = ReAlListView1.Items[i].SubItems[0].Text;
                            string nameSchema = ReAlListView1.Items[i].SubItems[2].Text;

                            wb.Worksheets.Add(nameTabla);

                            //Cabecera
                            wb.Worksheets.Worksheet(nameTabla).Cell("A2").Value = "BASE DE DATOS:";
                            wb.Worksheets.Worksheet(nameTabla).Cell("B2").Value = Principal.Catalogo;
                            wb.Worksheets.Worksheet(nameTabla).Cell("A3").Value = "TABLA:";
                            wb.Worksheets.Worksheet(nameTabla).Cell("B3").Value = nameTabla;

                            int inicioTablas = 5;

                            //Columnas
                            wb.Worksheets.Worksheet(nameTabla).Cell(inicioTablas, "A").Value = "COLUMNAS";
                            DataTable dtColumns = CTablasColumnas.ObtenerColumnasTabla(TipoEntorno.CSharp, this, nameSchema, idTabla);
                            dtColumns.Columns.Remove("tabla");
                            dtColumns.Columns.Remove("name");
                            dtColumns.Columns.Remove("name_upper");
                            dtColumns.Columns.Remove("tablaforanea");

                            dtColumns.Columns["name_original"].SetOrdinal(0);
                            dtColumns.Columns["col_desc"].SetOrdinal(1);
                            dtColumns.Columns["tipo_col"].SetOrdinal(2);
                            dtColumns.Columns["longitud"].SetOrdinal(3);
                            dtColumns.Columns["tipo"].SetOrdinal(4);

                            wb.Worksheets.Worksheet(nameTabla).Cell(1 + inicioTablas, "A").InsertTable(dtColumns);
                            inicioTablas = 1 + inicioTablas + dtColumns.Rows.Count + 2;

                            //Estados
                            wb.Worksheets.Worksheet(nameTabla).Cell(inicioTablas, "A").Value = "ESTADOS";
                            StringBuilder strEstado = new StringBuilder();
                            strEstado.AppendLine("SELECT se.estado, se.descripcion ");
                            strEstado.AppendLine("FROM seg_estados se");
                            strEstado.AppendLine("JOIN seg_tablas st ON st.id_tablas = se.id_tablas");
                            strEstado.AppendLine("where st.nombretabla = '" + nameTabla + "'");
                            DataTable dtEstados = CFuncionesBDs.CargarDataTable(strEstado.ToString());
                            wb.Worksheets.Worksheet(nameTabla).Cell(1 + inicioTablas, "A").InsertTable(dtEstados);

                            inicioTablas = 1 + inicioTablas + dtEstados.Rows.Count + 2;
                            //Transacciones
                            wb.Worksheets.Worksheet(nameTabla).Cell(inicioTablas, "A").Value = "TRANSACCIONES";
                            StringBuilder strTransaccion = new StringBuilder();
                            strTransaccion.AppendLine("SELECT str.transaccion, str.descripcion, str.sentencia ");
                            strTransaccion.AppendLine("FROM seg_transacciones str");
                            strTransaccion.AppendLine("JOIN seg_tablas st ON st.id_tablas = str.id_tablas");
                            strTransaccion.AppendLine("where st.nombretabla = '" + nameTabla + "'");
                            DataTable dtTransaccion = CFuncionesBDs.CargarDataTable(strTransaccion.ToString());
                            wb.Worksheets.Worksheet(nameTabla).Cell(1 + inicioTablas, "A").InsertTable(dtTransaccion);

                            inicioTablas = 1 + inicioTablas + dtTransaccion.Rows.Count + 2;
                            //Transiciones
                            wb.Worksheets.Worksheet(nameTabla).Cell(inicioTablas, "A").Value = "TRANSICIONES";
                            StringBuilder strTransicion = new StringBuilder();
                            strTransicion.AppendLine("SELECT ini.estado as inicio, tra.transaccion, fin.estado as final ");
                            strTransicion.AppendLine("FROM seg_transiciones tsi");
                            strTransicion.AppendLine("JOIN seg_tablas st ON st.id_tablas = tsi.id_tablas");
                            strTransicion.AppendLine("JOIN seg_estados ini ON tsi.id_estados_ini = ini.id_estados");
                            strTransicion.AppendLine("JOIN seg_estados fin ON tsi.id_estados_fin = fin.id_estados");
                            strTransicion.AppendLine("JOIN seg_transacciones tra ON tra.id_transacciones = tsi.id_transacciones");
                            strTransicion.AppendLine("WHERE st.nombretabla = '" + nameTabla + "'");
                            DataTable dtTransicion = CFuncionesBDs.CargarDataTable(strTransicion.ToString());
                            wb.Worksheets.Worksheet(nameTabla).Cell(1 + inicioTablas, "A").InsertTable(dtTransicion);

                            inicioTablas = 1 + inicioTablas + dtTransicion.Rows.Count + 2;
                            //Grafo
                            var strGrafo = new StringBuilder();
                            foreach (DataRow dr in dtTransicion.Rows)
                            {
                                strGrafo.Append(dr["inicio"].ToString().ToUpper() + "->" + dr["final"].ToString().ToUpper() + " [label=" + dr["transaccion"].ToString().ToLower() + "]; ");
                            }

                            //Imagen
                            wb.Worksheets.Worksheet(nameTabla).Cell(inicioTablas, "A").Value = "DIAGRAMA";
                            byte[] output = wrapper.GenerateGraph("digraph{rankdir=\"LR\"; " + strGrafo + "}", Enums.GraphReturnType.Png);
                            wb.Worksheets.Worksheet(nameTabla)
                                .AddPicture(new MemoryStream(output), ClosedXML.Excel.Drawings.XLPictureFormat.Png)
                                .MoveTo(wb.Worksheets.Worksheet(nameTabla).Cell(1 + inicioTablas, "B"))
                                .Scale(1);
                        }
                    }

                    //Para mostrar o no los filtros en la cabecera
                    wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    wb.Style.Font.Bold = true;

                    wb.SaveAs(path + "Diccionario de Datos.xlsx");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message + "\r\n\r\n" + ex.StackTrace, "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
        }

        private void btnBdDocumentarTodaLaBaseDeDatos_Click(object sender, EventArgs e)
        {
            string strDestino = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ReAlClass\\";
            string strSchemaSpy = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\AppInclude\schemaspy\";
            int exitCode = -1;
            try
            {
                //Armamos el archivo "schemaspy.properties"
                StringBuilder strProperties = new StringBuilder();
                strProperties.AppendLine(@"#");
                strProperties.AppendLine(@"#java -jar .\schemaspy-6.1.0.jar -configFile schemaspy.properties");
                strProperties.AppendLine(@"#");
                strProperties.AppendLine(@"");
                strProperties.AppendLine(@"# database type (pgsql, mssql, ora, mysql)");
                switch (Principal.DBMS)
                {
                    case TipoBD.PostgreSQL:
                        strProperties.AppendLine(@"schemaspy.t=pgsql");
                        break;

                    case TipoBD.SQLServer:
                    case TipoBD.SQLServer2000:
                        strProperties.AppendLine(@"schemaspy.t=mssql");
                        break;

                    case TipoBD.Oracle:
                        strProperties.AppendLine(@"schemaspy.t=ora");
                        break;
                }
                strProperties.AppendLine(@"");
                strProperties.AppendLine(@"# path to the database JDBC driver (postgresql-42.2.14.jar, mssql-jdbc-8.2.2.jre8.jar, ojdbc6.jar, mysql-connector-java-8.0.21.jar)");
                switch (Principal.DBMS)
                {
                    case TipoBD.PostgreSQL:
                        strProperties.AppendLine(@"schemaspy.dp=" + strSchemaSpy + "postgresql-42.2.14.jar");
                        break;

                    case TipoBD.SQLServer:
                    case TipoBD.SQLServer2000:
                        strProperties.AppendLine(@"schemaspy.dp=" + strSchemaSpy + "mssql-jdbc-8.2.2.jre8.jar");
                        break;

                    case TipoBD.Oracle:
                        strProperties.AppendLine(@"schemaspy.dp=" + strSchemaSpy + "ojdbc6.jar");
                        break;
                }
                strProperties.AppendLine(@"");
                strProperties.AppendLine(@"schemaspy.host=" + Principal.Servidor);
                strProperties.AppendLine(@"schemaspy.port=" + Principal.Puerto);
                strProperties.AppendLine(@"");
                strProperties.AppendLine(@"# database name");
                strProperties.AppendLine(@"schemaspy.db=" + Principal.Catalogo);
                strProperties.AppendLine(@"");
                strProperties.AppendLine(@"# database user");
                strProperties.AppendLine(@"schemaspy.u=" + Principal.UsuarioBD);
                strProperties.AppendLine(@"schemaspy.p=" + Principal.PasswordBD);
                strProperties.AppendLine(@"");
                strProperties.AppendLine(@"# output folder for the generated resukt");
                strProperties.AppendLine(@"schemaspy.o=" + strDestino + "" + Principal.Catalogo);
                strProperties.AppendLine(@"");
                strProperties.AppendLine(@"# database schema");
                strProperties.AppendLine(@"schemaspy.s=public");

                if (File.Exists(strSchemaSpy + "schemaspy.properties"))
                    File.Delete(strSchemaSpy + "schemaspy.properties");

                CFunciones.CrearArchivo(strProperties.ToString(), "schemaspy.properties", strSchemaSpy);

                //Ejecutamos el SchemaSpy
                string strCommand = "schemaspy-6.1.0.jar";
                string strArguments = " -configFile " + strSchemaSpy + "schemaspy.properties";

                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    FileName = "java.exe",
                    Arguments = "-jar " + strSchemaSpy + strCommand + strArguments
                };

                Process process;
                if ((process = Process.Start(startInfo)) == null)
                {
                    throw new InvalidOperationException("??");
                }

                process.WaitForExit();

                exitCode = process.ExitCode;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message + "\r\n\r\n" + ex.StackTrace, "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
            finally
            {
                //Eliminamos el archivo "schemaspy.properties"
                if (File.Exists(strSchemaSpy + "schemaspy.properties"))
                    File.Delete(strSchemaSpy + "schemaspy.properties");
            }

            if (exitCode == 0)
                if (MessageBox.Show(this,
                        "Se han creado satisfactoriamente el Catalogo de la Base de Datos '" + Principal.Catalogo + "' usando SchemaSpy\r\n\r\n¿Desea ver el resultado?",
                        "Exito!", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Information) == DialogResult.Yes)
                    Process.Start(strDestino + Principal.Catalogo + "\\index.html");
        }
    }
}