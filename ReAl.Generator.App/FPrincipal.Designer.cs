﻿namespace ReAl.Generator.App
{
    partial class FPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPrincipal));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.sistemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurarConexionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.plantillasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.copiarTemplatesAVisualStudioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vS2010ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vS2012ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vS2013ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vS2015ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vS2017ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vS2019ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabMainPage1 = new System.Windows.Forms.TabPage();
            this.cmbEntorno = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCLA = new System.Windows.Forms.Button();
            this.grpParametrosAdicionales = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtClaseParametrosClaseApiObject = new System.Windows.Forms.TextBox();
            this.chkClassParcial = new System.Windows.Forms.CheckBox();
            this.chkEntBaseClass = new System.Windows.Forms.CheckBox();
            this.chkPropertyEvents = new System.Windows.Forms.CheckBox();
            this.chkEntidadesEncriptacion = new System.Windows.Forms.CheckBox();
            this.chkDataAnnotations = new System.Windows.Forms.CheckBox();
            this.chkUsarSpsIdentity = new System.Windows.Forms.CheckBox();
            this.chkUsarSpsCheck = new System.Windows.Forms.CheckBox();
            this.txtClaseParametros = new System.Windows.Forms.TextBox();
            this.txtClaseParametrosListadoSp = new System.Windows.Forms.TextBox();
            this.txtClaseParametrosBaseClass = new System.Windows.Forms.TextBox();
            this.txtClaseParametrosClaseApi = new System.Windows.Forms.TextBox();
            this.chkParametroPa = new System.Windows.Forms.CheckBox();
            this.chkUsarSpsSelect = new System.Windows.Forms.CheckBox();
            this.chkUsarSps = new System.Windows.Forms.CheckBox();
            this.chkUsarSchemaInModelo = new System.Windows.Forms.CheckBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label101 = new System.Windows.Forms.Label();
            this.Label102 = new System.Windows.Forms.Label();
            this.Label103 = new System.Windows.Forms.Label();
            this.txtClaseTransaccion = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.txtClaseConn = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.GroupBox5 = new System.Windows.Forms.GroupBox();
            this.txtNamespaceDocumentacion = new System.Windows.Forms.TextBox();
            this.chkReAlCrearDoc = new System.Windows.Forms.CheckBox();
            this.chkReAlDataSet = new System.Windows.Forms.CheckBox();
            this.txtNombreDataSet = new System.Windows.Forms.TextBox();
            this.TextBox2 = new System.Windows.Forms.TextBox();
            this.txtPrefijoUnitTest = new System.Windows.Forms.TextBox();
            this.Label27 = new System.Windows.Forms.Label();
            this.txtNamespaceUnitTest = new System.Windows.Forms.TextBox();
            this.chkReAlUnitTest = new System.Windows.Forms.CheckBox();
            this.textbox10 = new System.Windows.Forms.TextBox();
            this.txtPrefijoEntidades = new System.Windows.Forms.TextBox();
            this.Label16 = new System.Windows.Forms.Label();
            this.TextBox6 = new System.Windows.Forms.TextBox();
            this.txtPrefijoInterface = new System.Windows.Forms.TextBox();
            this.Label15 = new System.Windows.Forms.Label();
            this.TextBox3 = new System.Windows.Forms.TextBox();
            this.txtPrefijoModelo = new System.Windows.Forms.TextBox();
            this.Label14 = new System.Windows.Forms.Label();
            this.txtNamespaceNegocios = new System.Windows.Forms.TextBox();
            this.txtNamespaceInterface = new System.Windows.Forms.TextBox();
            this.txtNamespaceEntidades = new System.Windows.Forms.TextBox();
            this.chkReAlReglaNegocios = new System.Windows.Forms.CheckBox();
            this.chkReAlInterface = new System.Windows.Forms.CheckBox();
            this.chkReAlEntidades = new System.Windows.Forms.CheckBox();
            this.btnGralAbrirCarpeta = new System.Windows.Forms.Button();
            this.GroupBox7 = new System.Windows.Forms.GroupBox();
            this.cmbTipoColumnas = new System.Windows.Forms.ComboBox();
            this.grpPathDel = new System.Windows.Forms.GroupBox();
            this.txtPathProyecto = new System.Windows.Forms.TextBox();
            this.chkMoverResultados = new System.Windows.Forms.CheckBox();
            this.btnPathProyecto = new System.Windows.Forms.Button();
            this.GroupBox23 = new System.Windows.Forms.GroupBox();
            this.txtInfAutor = new System.Windows.Forms.TextBox();
            this.GroupBox19 = new System.Windows.Forms.GroupBox();
            this.cmbAPIs = new System.Windows.Forms.ComboBox();
            this.grpGroupBox8 = new System.Windows.Forms.GroupBox();
            this.Label46 = new System.Windows.Forms.Label();
            this.txtInfProyectoConn = new System.Windows.Forms.TextBox();
            this.Label28 = new System.Windows.Forms.Label();
            this.txtInfProyectoUnitTest = new System.Windows.Forms.TextBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.txtInfProyectoDal = new System.Windows.Forms.TextBox();
            this.txtInfProyecto = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.txtInfProyectoClass = new System.Windows.Forms.TextBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.tabMainPage2 = new System.Windows.Forms.TabPage();
            this.groupBoxDiccionarioDeDatos = new System.Windows.Forms.GroupBox();
            this.btnBdDocumentarTodaLaBaseDeDatos = new System.Windows.Forms.Button();
            this.btnBdCrearCicloDeVida = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnBDExportarExcel = new System.Windows.Forms.Button();
            this.txtBDExportarExcel = new System.Windows.Forms.TextBox();
            this.grpColumnasTriggers = new System.Windows.Forms.GroupBox();
            this.GroupBox18 = new System.Windows.Forms.GroupBox();
            this.chkBDOpcionesTriggersInstead = new System.Windows.Forms.CheckBox();
            this.chkBDOpcionesTriggers = new System.Windows.Forms.CheckBox();
            this.chkBDOpcionesTriggersSimple = new System.Windows.Forms.CheckBox();
            this.chkBDOpcionesCols = new System.Windows.Forms.CheckBox();
            this.GroupBox17 = new System.Windows.Forms.GroupBox();
            this.chkTriggerEncrypt = new System.Windows.Forms.CheckBox();
            this.chkTriggerInsert = new System.Windows.Forms.CheckBox();
            this.chkTriggerDelete = new System.Windows.Forms.CheckBox();
            this.chkTriggerUpdate = new System.Windows.Forms.CheckBox();
            this.GroupBox16 = new System.Windows.Forms.GroupBox();
            this.chkBdColEstado = new System.Windows.Forms.CheckBox();
            this.chkBdColUsuCre = new System.Windows.Forms.CheckBox();
            this.chkBdColFecMod = new System.Windows.Forms.CheckBox();
            this.chkBdColUsuMod = new System.Windows.Forms.CheckBox();
            this.chkBdColTrans = new System.Windows.Forms.CheckBox();
            this.chkBdColFecCre = new System.Windows.Forms.CheckBox();
            this.btnBDCrearTriggers = new System.Windows.Forms.Button();
            this.GroupBox4 = new System.Windows.Forms.GroupBox();
            this.rdbSpIfExists = new System.Windows.Forms.RadioButton();
            this.rdbSpAlter = new System.Windows.Forms.RadioButton();
            this.rdbSpCreate = new System.Windows.Forms.RadioButton();
            this.grpSpIns = new System.Windows.Forms.GroupBox();
            this.chkSPInsIdentity = new System.Windows.Forms.CheckBox();
            this.chkSPInsFecCreGetDate = new System.Windows.Forms.CheckBox();
            this.chkSPInsApiEstado = new System.Windows.Forms.CheckBox();
            this.chkSPInsFecCre = new System.Windows.Forms.CheckBox();
            this.chkSPInsUsuCre = new System.Windows.Forms.CheckBox();
            this.grpSpUpd = new System.Windows.Forms.GroupBox();
            this.chkSPUpdFecModGetDate = new System.Windows.Forms.CheckBox();
            this.chkSPUpdFecMod = new System.Windows.Forms.CheckBox();
            this.chkSPUpdUsuMod = new System.Windows.Forms.CheckBox();
            this.btnSP = new System.Windows.Forms.Button();
            this.txtSPsig = new System.Windows.Forms.TextBox();
            this.TextBox4 = new System.Windows.Forms.TextBox();
            this.txtSPprev = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.chkSPInsUpd = new System.Windows.Forms.CheckBox();
            this.chkSPsel = new System.Windows.Forms.CheckBox();
            this.chkSPdel = new System.Windows.Forms.CheckBox();
            this.chkSPupd = new System.Windows.Forms.CheckBox();
            this.chkSPins = new System.Windows.Forms.CheckBox();
            this.chkSPParametrosPa = new System.Windows.Forms.CheckBox();
            this.tabMainPage3 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSnippet = new System.Windows.Forms.Button();
            this.cmbSnippet = new System.Windows.Forms.ComboBox();
            this.txtPreview = new FastColoredTextBoxNS.FastColoredTextBox();
            this.cmbPlantilla = new System.Windows.Forms.ComboBox();
            this.Label25 = new System.Windows.Forms.Label();
            this.Label26 = new System.Windows.Forms.Label();
            this.txtListadoBdFiltro = new System.Windows.Forms.TextBox();
            this.btnActualizar = new System.Windows.Forms.Button();
            this.tabBDObjects = new System.Windows.Forms.TabControl();
            this.tabPageTables = new System.Windows.Forms.TabPage();
            this.ReAlListView1 = new ReAlFind_Control.ReAlListView();
            this.tabPageViews = new System.Windows.Forms.TabPage();
            this.ReAlListView2 = new ReAlFind_Control.ReAlListView();
            this.tabPageSPs = new System.Windows.Forms.TabPage();
            this.ReAlListView3 = new ReAlFind_Control.ReAlListView();
            this.TabPageTriggers = new System.Windows.Forms.TabPage();
            this.ReAlListView4 = new ReAlFind_Control.ReAlListView();
            this.Label1 = new System.Windows.Forms.Label();
            this.chkSeleccionarTodo = new System.Windows.Forms.CheckBox();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabMainPage1.SuspendLayout();
            this.grpParametrosAdicionales.SuspendLayout();
            this.GroupBox5.SuspendLayout();
            this.GroupBox7.SuspendLayout();
            this.grpPathDel.SuspendLayout();
            this.GroupBox23.SuspendLayout();
            this.GroupBox19.SuspendLayout();
            this.grpGroupBox8.SuspendLayout();
            this.tabMainPage2.SuspendLayout();
            this.groupBoxDiccionarioDeDatos.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpColumnasTriggers.SuspendLayout();
            this.GroupBox18.SuspendLayout();
            this.GroupBox17.SuspendLayout();
            this.GroupBox16.SuspendLayout();
            this.GroupBox4.SuspendLayout();
            this.grpSpIns.SuspendLayout();
            this.grpSpUpd.SuspendLayout();
            this.GroupBox3.SuspendLayout();
            this.tabMainPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPreview)).BeginInit();
            this.tabBDObjects.SuspendLayout();
            this.tabPageTables.SuspendLayout();
            this.tabPageViews.SuspendLayout();
            this.tabPageSPs.SuspendLayout();
            this.TabPageTriggers.SuspendLayout();
            this.statusStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sistemaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // sistemaToolStripMenuItem
            // 
            this.sistemaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configurarConexionToolStripMenuItem,
            this.plantillasToolStripMenuItem,
            this.toolStripMenuItem2,
            this.copiarTemplatesAVisualStudioToolStripMenuItem,
            this.toolStripMenuItem1,
            this.salirToolStripMenuItem});
            this.sistemaToolStripMenuItem.Name = "sistemaToolStripMenuItem";
            this.sistemaToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.sistemaToolStripMenuItem.Text = "&Sistema";
            // 
            // configurarConexionToolStripMenuItem
            // 
            this.configurarConexionToolStripMenuItem.Name = "configurarConexionToolStripMenuItem";
            this.configurarConexionToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.configurarConexionToolStripMenuItem.Text = "&Configurar Conexion";
            this.configurarConexionToolStripMenuItem.Click += new System.EventHandler(this.configurarConexionToolStripMenuItem_Click);
            // 
            // plantillasToolStripMenuItem
            // 
            this.plantillasToolStripMenuItem.Name = "plantillasToolStripMenuItem";
            this.plantillasToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.plantillasToolStripMenuItem.Text = "&Plantillas";
            this.plantillasToolStripMenuItem.Click += new System.EventHandler(this.plantillasToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(239, 6);
            // 
            // copiarTemplatesAVisualStudioToolStripMenuItem
            // 
            this.copiarTemplatesAVisualStudioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vS2010ToolStripMenuItem,
            this.vS2012ToolStripMenuItem,
            this.vS2013ToolStripMenuItem,
            this.vS2015ToolStripMenuItem,
            this.vS2017ToolStripMenuItem,
            this.vS2019ToolStripMenuItem});
            this.copiarTemplatesAVisualStudioToolStripMenuItem.Name = "copiarTemplatesAVisualStudioToolStripMenuItem";
            this.copiarTemplatesAVisualStudioToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.copiarTemplatesAVisualStudioToolStripMenuItem.Text = "Copiar Templates a &VisualStudio";
            // 
            // vS2010ToolStripMenuItem
            // 
            this.vS2010ToolStripMenuItem.Name = "vS2010ToolStripMenuItem";
            this.vS2010ToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.vS2010ToolStripMenuItem.Text = "VS201&0";
            this.vS2010ToolStripMenuItem.Click += new System.EventHandler(this.vS2010ToolStripMenuItem_Click);
            // 
            // vS2012ToolStripMenuItem
            // 
            this.vS2012ToolStripMenuItem.Name = "vS2012ToolStripMenuItem";
            this.vS2012ToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.vS2012ToolStripMenuItem.Text = "VS201&2";
            this.vS2012ToolStripMenuItem.Click += new System.EventHandler(this.vS2012ToolStripMenuItem_Click);
            // 
            // vS2013ToolStripMenuItem
            // 
            this.vS2013ToolStripMenuItem.Name = "vS2013ToolStripMenuItem";
            this.vS2013ToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.vS2013ToolStripMenuItem.Text = "VS201&3";
            this.vS2013ToolStripMenuItem.Click += new System.EventHandler(this.vS2013ToolStripMenuItem_Click);
            // 
            // vS2015ToolStripMenuItem
            // 
            this.vS2015ToolStripMenuItem.Name = "vS2015ToolStripMenuItem";
            this.vS2015ToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.vS2015ToolStripMenuItem.Text = "VS201&5";
            this.vS2015ToolStripMenuItem.Click += new System.EventHandler(this.vS2015ToolStripMenuItem_Click);
            // 
            // vS2017ToolStripMenuItem
            // 
            this.vS2017ToolStripMenuItem.Name = "vS2017ToolStripMenuItem";
            this.vS2017ToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.vS2017ToolStripMenuItem.Text = "VS201&7";
            this.vS2017ToolStripMenuItem.Click += new System.EventHandler(this.vS2017ToolStripMenuItem_Click);
            // 
            // vS2019ToolStripMenuItem
            // 
            this.vS2019ToolStripMenuItem.Name = "vS2019ToolStripMenuItem";
            this.vS2019ToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.vS2019ToolStripMenuItem.Text = "VS201&9";
            this.vS2019ToolStripMenuItem.Click += new System.EventHandler(this.VS2019ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(239, 6);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.salirToolStripMenuItem.Text = "&Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabMainPage1);
            this.tabControl1.Controls.Add(this.tabMainPage2);
            this.tabControl1.Controls.Add(this.tabMainPage3);
            this.tabControl1.Location = new System.Drawing.Point(350, 51);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(646, 625);
            this.tabControl1.TabIndex = 9;
            // 
            // tabMainPage1
            // 
            this.tabMainPage1.BackColor = System.Drawing.Color.Silver;
            this.tabMainPage1.Controls.Add(this.cmbEntorno);
            this.tabMainPage1.Controls.Add(this.label2);
            this.tabMainPage1.Controls.Add(this.btnCLA);
            this.tabMainPage1.Controls.Add(this.grpParametrosAdicionales);
            this.tabMainPage1.Controls.Add(this.GroupBox5);
            this.tabMainPage1.Controls.Add(this.btnGralAbrirCarpeta);
            this.tabMainPage1.Controls.Add(this.GroupBox7);
            this.tabMainPage1.Controls.Add(this.grpPathDel);
            this.tabMainPage1.Controls.Add(this.GroupBox23);
            this.tabMainPage1.Controls.Add(this.GroupBox19);
            this.tabMainPage1.Controls.Add(this.grpGroupBox8);
            this.tabMainPage1.Location = new System.Drawing.Point(4, 22);
            this.tabMainPage1.Name = "tabMainPage1";
            this.tabMainPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabMainPage1.Size = new System.Drawing.Size(638, 599);
            this.tabMainPage1.TabIndex = 0;
            this.tabMainPage1.Text = "Codigo Fuente";
            // 
            // cmbEntorno
            // 
            this.cmbEntorno.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEntorno.FormattingEnabled = true;
            this.cmbEntorno.Location = new System.Drawing.Point(120, 6);
            this.cmbEntorno.Name = "cmbEntorno";
            this.cmbEntorno.Size = new System.Drawing.Size(223, 21);
            this.cmbEntorno.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "ENTORNO:";
            // 
            // btnCLA
            // 
            this.btnCLA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCLA.Location = new System.Drawing.Point(409, 556);
            this.btnCLA.Name = "btnCLA";
            this.btnCLA.Size = new System.Drawing.Size(223, 37);
            this.btnCLA.TabIndex = 10;
            this.btnCLA.Text = "&Generar Clases";
            this.btnCLA.UseVisualStyleBackColor = true;
            this.btnCLA.Click += new System.EventHandler(this.btnCLA_Click);
            // 
            // grpParametrosAdicionales
            // 
            this.grpParametrosAdicionales.Controls.Add(this.label7);
            this.grpParametrosAdicionales.Controls.Add(this.txtClaseParametrosClaseApiObject);
            this.grpParametrosAdicionales.Controls.Add(this.chkClassParcial);
            this.grpParametrosAdicionales.Controls.Add(this.chkEntBaseClass);
            this.grpParametrosAdicionales.Controls.Add(this.chkPropertyEvents);
            this.grpParametrosAdicionales.Controls.Add(this.chkEntidadesEncriptacion);
            this.grpParametrosAdicionales.Controls.Add(this.chkDataAnnotations);
            this.grpParametrosAdicionales.Controls.Add(this.chkUsarSpsIdentity);
            this.grpParametrosAdicionales.Controls.Add(this.chkUsarSpsCheck);
            this.grpParametrosAdicionales.Controls.Add(this.txtClaseParametros);
            this.grpParametrosAdicionales.Controls.Add(this.txtClaseParametrosListadoSp);
            this.grpParametrosAdicionales.Controls.Add(this.txtClaseParametrosBaseClass);
            this.grpParametrosAdicionales.Controls.Add(this.txtClaseParametrosClaseApi);
            this.grpParametrosAdicionales.Controls.Add(this.chkParametroPa);
            this.grpParametrosAdicionales.Controls.Add(this.chkUsarSpsSelect);
            this.grpParametrosAdicionales.Controls.Add(this.chkUsarSps);
            this.grpParametrosAdicionales.Controls.Add(this.chkUsarSchemaInModelo);
            this.grpParametrosAdicionales.Controls.Add(this.Label10);
            this.grpParametrosAdicionales.Controls.Add(this.Label101);
            this.grpParametrosAdicionales.Controls.Add(this.Label102);
            this.grpParametrosAdicionales.Controls.Add(this.Label103);
            this.grpParametrosAdicionales.Controls.Add(this.txtClaseTransaccion);
            this.grpParametrosAdicionales.Controls.Add(this.Label4);
            this.grpParametrosAdicionales.Controls.Add(this.txtClaseConn);
            this.grpParametrosAdicionales.Controls.Add(this.Label9);
            this.grpParametrosAdicionales.Location = new System.Drawing.Point(6, 326);
            this.grpParametrosAdicionales.Name = "grpParametrosAdicionales";
            this.grpParametrosAdicionales.Size = new System.Drawing.Size(626, 207);
            this.grpParametrosAdicionales.TabIndex = 9;
            this.grpParametrosAdicionales.TabStop = false;
            this.grpParametrosAdicionales.Text = "Parametros Adicionales";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(246, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "API Object:";
            // 
            // txtClaseParametrosClaseApiObject
            // 
            this.txtClaseParametrosClaseApiObject.Location = new System.Drawing.Point(249, 95);
            this.txtClaseParametrosClaseApiObject.Name = "txtClaseParametrosClaseApiObject";
            this.txtClaseParametrosClaseApiObject.Size = new System.Drawing.Size(73, 20);
            this.txtClaseParametrosClaseApiObject.TabIndex = 18;
            this.txtClaseParametrosClaseApiObject.Text = "CApiObject";
            // 
            // chkClassParcial
            // 
            this.chkClassParcial.AutoSize = true;
            this.chkClassParcial.Checked = true;
            this.chkClassParcial.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkClassParcial.Location = new System.Drawing.Point(336, 67);
            this.chkClassParcial.Name = "chkClassParcial";
            this.chkClassParcial.Size = new System.Drawing.Size(174, 17);
            this.chkClassParcial.TabIndex = 9;
            this.chkClassParcial.Text = "Crear Clase Parcial para Textos";
            this.chkClassParcial.UseVisualStyleBackColor = true;
            // 
            // chkEntBaseClass
            // 
            this.chkEntBaseClass.AutoSize = true;
            this.chkEntBaseClass.Location = new System.Drawing.Point(336, 100);
            this.chkEntBaseClass.Name = "chkEntBaseClass";
            this.chkEntBaseClass.Size = new System.Drawing.Size(187, 17);
            this.chkEntBaseClass.TabIndex = 19;
            this.chkEntBaseClass.Text = "Utilizar Clase Base para Entidades";
            this.chkEntBaseClass.UseVisualStyleBackColor = true;
            // 
            // chkPropertyEvents
            // 
            this.chkPropertyEvents.AutoSize = true;
            this.chkPropertyEvents.Location = new System.Drawing.Point(352, 132);
            this.chkPropertyEvents.Name = "chkPropertyEvents";
            this.chkPropertyEvents.Size = new System.Drawing.Size(207, 17);
            this.chkPropertyEvents.TabIndex = 21;
            this.chkPropertyEvents.Text = "Generar Eventos para las propiedades";
            this.chkPropertyEvents.UseVisualStyleBackColor = true;
            // 
            // chkEntidadesEncriptacion
            // 
            this.chkEntidadesEncriptacion.AutoSize = true;
            this.chkEntidadesEncriptacion.Checked = true;
            this.chkEntidadesEncriptacion.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkEntidadesEncriptacion.Location = new System.Drawing.Point(352, 116);
            this.chkEntidadesEncriptacion.Name = "chkEntidadesEncriptacion";
            this.chkEntidadesEncriptacion.Size = new System.Drawing.Size(192, 17);
            this.chkEntidadesEncriptacion.TabIndex = 20;
            this.chkEntidadesEncriptacion.Text = "Generar Algoritmos de Encriptación";
            this.chkEntidadesEncriptacion.UseVisualStyleBackColor = true;
            // 
            // chkDataAnnotations
            // 
            this.chkDataAnnotations.AutoSize = true;
            this.chkDataAnnotations.Checked = true;
            this.chkDataAnnotations.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDataAnnotations.Location = new System.Drawing.Point(336, 84);
            this.chkDataAnnotations.Name = "chkDataAnnotations";
            this.chkDataAnnotations.Size = new System.Drawing.Size(195, 17);
            this.chkDataAnnotations.TabIndex = 14;
            this.chkDataAnnotations.Text = "Usar DataAnnotations en las Clases";
            this.chkDataAnnotations.UseVisualStyleBackColor = true;
            // 
            // chkUsarSpsIdentity
            // 
            this.chkUsarSpsIdentity.AutoSize = true;
            this.chkUsarSpsIdentity.Location = new System.Drawing.Point(336, 51);
            this.chkUsarSpsIdentity.Name = "chkUsarSpsIdentity";
            this.chkUsarSpsIdentity.Size = new System.Drawing.Size(178, 17);
            this.chkUsarSpsIdentity.TabIndex = 6;
            this.chkUsarSpsIdentity.Text = "Usar InsertIdentity para INSERT";
            this.chkUsarSpsIdentity.UseVisualStyleBackColor = true;
            // 
            // chkUsarSpsCheck
            // 
            this.chkUsarSpsCheck.AutoSize = true;
            this.chkUsarSpsCheck.Checked = true;
            this.chkUsarSpsCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUsarSpsCheck.Location = new System.Drawing.Point(355, 166);
            this.chkUsarSpsCheck.Name = "chkUsarSpsCheck";
            this.chkUsarSpsCheck.Size = new System.Drawing.Size(150, 17);
            this.chkUsarSpsCheck.TabIndex = 23;
            this.chkUsarSpsCheck.Text = "Verificar parametros NULL";
            this.chkUsarSpsCheck.UseVisualStyleBackColor = true;
            // 
            // txtClaseParametros
            // 
            this.txtClaseParametros.Location = new System.Drawing.Point(206, 59);
            this.txtClaseParametros.Name = "txtClaseParametros";
            this.txtClaseParametros.Size = new System.Drawing.Size(114, 20);
            this.txtClaseParametros.TabIndex = 8;
            this.txtClaseParametros.Text = "cParametros";
            // 
            // txtClaseParametrosListadoSp
            // 
            this.txtClaseParametrosListadoSp.Location = new System.Drawing.Point(12, 95);
            this.txtClaseParametrosListadoSp.Name = "txtClaseParametrosListadoSp";
            this.txtClaseParametrosListadoSp.Size = new System.Drawing.Size(73, 20);
            this.txtClaseParametrosListadoSp.TabIndex = 15;
            this.txtClaseParametrosListadoSp.Text = "CListadoSP";
            // 
            // txtClaseParametrosBaseClass
            // 
            this.txtClaseParametrosBaseClass.Location = new System.Drawing.Point(91, 95);
            this.txtClaseParametrosBaseClass.Name = "txtClaseParametrosBaseClass";
            this.txtClaseParametrosBaseClass.Size = new System.Drawing.Size(73, 20);
            this.txtClaseParametrosBaseClass.TabIndex = 16;
            this.txtClaseParametrosBaseClass.Text = "CBaseClass";
            // 
            // txtClaseParametrosClaseApi
            // 
            this.txtClaseParametrosClaseApi.Location = new System.Drawing.Point(170, 95);
            this.txtClaseParametrosClaseApi.Name = "txtClaseParametrosClaseApi";
            this.txtClaseParametrosClaseApi.Size = new System.Drawing.Size(73, 20);
            this.txtClaseParametrosClaseApi.TabIndex = 17;
            this.txtClaseParametrosClaseApi.Text = "CApi";
            // 
            // chkParametroPa
            // 
            this.chkParametroPa.AutoSize = true;
            this.chkParametroPa.Enabled = false;
            this.chkParametroPa.Location = new System.Drawing.Point(336, 33);
            this.chkParametroPa.Name = "chkParametroPa";
            this.chkParametroPa.Size = new System.Drawing.Size(182, 17);
            this.chkParametroPa.TabIndex = 3;
            this.chkParametroPa.Text = "Incluir \"pa\" en los parametros SP";
            this.chkParametroPa.UseVisualStyleBackColor = true;
            // 
            // chkUsarSpsSelect
            // 
            this.chkUsarSpsSelect.AutoSize = true;
            this.chkUsarSpsSelect.Location = new System.Drawing.Point(336, 15);
            this.chkUsarSpsSelect.Name = "chkUsarSpsSelect";
            this.chkUsarSpsSelect.Size = new System.Drawing.Size(163, 17);
            this.chkUsarSpsSelect.TabIndex = 2;
            this.chkUsarSpsSelect.Text = "Utilizar SPs para los SELECT";
            this.chkUsarSpsSelect.UseVisualStyleBackColor = true;
            // 
            // chkUsarSps
            // 
            this.chkUsarSps.AutoSize = true;
            this.chkUsarSps.Checked = true;
            this.chkUsarSps.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUsarSps.Location = new System.Drawing.Point(335, 148);
            this.chkUsarSps.Name = "chkUsarSps";
            this.chkUsarSps.Size = new System.Drawing.Size(150, 17);
            this.chkUsarSps.TabIndex = 22;
            this.chkUsarSps.Text = "Utilizar SPs para los ABMs";
            this.chkUsarSps.UseVisualStyleBackColor = true;
            // 
            // chkUsarSchemaInModelo
            // 
            this.chkUsarSchemaInModelo.AutoSize = true;
            this.chkUsarSchemaInModelo.Location = new System.Drawing.Point(335, 184);
            this.chkUsarSchemaInModelo.Name = "chkUsarSchemaInModelo";
            this.chkUsarSchemaInModelo.Size = new System.Drawing.Size(193, 17);
            this.chkUsarSchemaInModelo.TabIndex = 24;
            this.chkUsarSchemaInModelo.Text = "Concatenar SCHEMA en el Modelo";
            this.chkUsarSchemaInModelo.UseVisualStyleBackColor = true;
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(8, 62);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(198, 13);
            this.Label10.TabIndex = 7;
            this.Label10.Text = "Nombre de las Clases:          Parametros:";
            // 
            // Label101
            // 
            this.Label101.AutoSize = true;
            this.Label101.Location = new System.Drawing.Point(8, 82);
            this.Label101.Name = "Label101";
            this.Label101.Size = new System.Drawing.Size(66, 13);
            this.Label101.TabIndex = 10;
            this.Label101.Text = "Listado SPs:";
            // 
            // Label102
            // 
            this.Label102.AutoSize = true;
            this.Label102.Location = new System.Drawing.Point(88, 82);
            this.Label102.Name = "Label102";
            this.Label102.Size = new System.Drawing.Size(63, 13);
            this.Label102.TabIndex = 11;
            this.Label102.Text = "Clase Base:";
            // 
            // Label103
            // 
            this.Label103.AutoSize = true;
            this.Label103.Location = new System.Drawing.Point(167, 82);
            this.Label103.Name = "Label103";
            this.Label103.Size = new System.Drawing.Size(56, 13);
            this.Label103.TabIndex = 12;
            this.Label103.Text = "Clase API:";
            // 
            // txtClaseTransaccion
            // 
            this.txtClaseTransaccion.Location = new System.Drawing.Point(206, 36);
            this.txtClaseTransaccion.Name = "txtClaseTransaccion";
            this.txtClaseTransaccion.Size = new System.Drawing.Size(114, 20);
            this.txtClaseTransaccion.TabIndex = 5;
            this.txtClaseTransaccion.Text = "cTrans";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(8, 39);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(190, 13);
            this.Label4.TabIndex = 4;
            this.Label4.Text = "Nombre de la Clase de Transacciones:";
            // 
            // txtClaseConn
            // 
            this.txtClaseConn.Location = new System.Drawing.Point(206, 13);
            this.txtClaseConn.Name = "txtClaseConn";
            this.txtClaseConn.Size = new System.Drawing.Size(114, 20);
            this.txtClaseConn.TabIndex = 1;
            this.txtClaseConn.Text = "cConn";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(9, 16);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(171, 13);
            this.Label9.TabIndex = 0;
            this.Label9.Text = "Nombre de la Clase de Coneccion:";
            // 
            // GroupBox5
            // 
            this.GroupBox5.Controls.Add(this.txtNamespaceDocumentacion);
            this.GroupBox5.Controls.Add(this.chkReAlCrearDoc);
            this.GroupBox5.Controls.Add(this.chkReAlDataSet);
            this.GroupBox5.Controls.Add(this.txtNombreDataSet);
            this.GroupBox5.Controls.Add(this.TextBox2);
            this.GroupBox5.Controls.Add(this.txtPrefijoUnitTest);
            this.GroupBox5.Controls.Add(this.Label27);
            this.GroupBox5.Controls.Add(this.txtNamespaceUnitTest);
            this.GroupBox5.Controls.Add(this.chkReAlUnitTest);
            this.GroupBox5.Controls.Add(this.textbox10);
            this.GroupBox5.Controls.Add(this.txtPrefijoEntidades);
            this.GroupBox5.Controls.Add(this.Label16);
            this.GroupBox5.Controls.Add(this.TextBox6);
            this.GroupBox5.Controls.Add(this.txtPrefijoInterface);
            this.GroupBox5.Controls.Add(this.Label15);
            this.GroupBox5.Controls.Add(this.TextBox3);
            this.GroupBox5.Controls.Add(this.txtPrefijoModelo);
            this.GroupBox5.Controls.Add(this.Label14);
            this.GroupBox5.Controls.Add(this.txtNamespaceNegocios);
            this.GroupBox5.Controls.Add(this.txtNamespaceInterface);
            this.GroupBox5.Controls.Add(this.txtNamespaceEntidades);
            this.GroupBox5.Controls.Add(this.chkReAlReglaNegocios);
            this.GroupBox5.Controls.Add(this.chkReAlInterface);
            this.GroupBox5.Controls.Add(this.chkReAlEntidades);
            this.GroupBox5.Location = new System.Drawing.Point(6, 167);
            this.GroupBox5.Name = "GroupBox5";
            this.GroupBox5.Size = new System.Drawing.Size(626, 153);
            this.GroupBox5.TabIndex = 8;
            this.GroupBox5.TabStop = false;
            this.GroupBox5.Text = "Parametros";
            // 
            // txtNamespaceDocumentacion
            // 
            this.txtNamespaceDocumentacion.Location = new System.Drawing.Point(253, 106);
            this.txtNamespaceDocumentacion.Name = "txtNamespaceDocumentacion";
            this.txtNamespaceDocumentacion.Size = new System.Drawing.Size(363, 20);
            this.txtNamespaceDocumentacion.TabIndex = 21;
            this.txtNamespaceDocumentacion.Text = "https://bitbucket.org/ine2013/encuestaine/wiki/";
            // 
            // chkReAlCrearDoc
            // 
            this.chkReAlCrearDoc.AutoSize = true;
            this.chkReAlCrearDoc.Location = new System.Drawing.Point(7, 108);
            this.chkReAlCrearDoc.Name = "chkReAlCrearDoc";
            this.chkReAlCrearDoc.Size = new System.Drawing.Size(129, 17);
            this.chkReAlCrearDoc.TabIndex = 20;
            this.chkReAlCrearDoc.Text = "Crear Documentacion";
            this.chkReAlCrearDoc.UseVisualStyleBackColor = true;
            // 
            // chkReAlDataSet
            // 
            this.chkReAlDataSet.AutoSize = true;
            this.chkReAlDataSet.Location = new System.Drawing.Point(7, 130);
            this.chkReAlDataSet.Name = "chkReAlDataSet";
            this.chkReAlDataSet.Size = new System.Drawing.Size(93, 17);
            this.chkReAlDataSet.TabIndex = 22;
            this.chkReAlDataSet.Text = "Crear DataSet";
            this.chkReAlDataSet.UseVisualStyleBackColor = true;
            // 
            // txtNombreDataSet
            // 
            this.txtNombreDataSet.Location = new System.Drawing.Point(253, 128);
            this.txtNombreDataSet.Name = "txtNombreDataSet";
            this.txtNombreDataSet.Size = new System.Drawing.Size(144, 20);
            this.txtNombreDataSet.TabIndex = 23;
            this.txtNombreDataSet.Text = "miDataSet";
            // 
            // TextBox2
            // 
            this.TextBox2.Location = new System.Drawing.Point(499, 83);
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.ReadOnly = true;
            this.TextBox2.Size = new System.Drawing.Size(117, 20);
            this.TextBox2.TabIndex = 19;
            this.TextBox2.TabStop = false;
            this.TextBox2.Text = "[Nombre Clase]";
            // 
            // txtPrefijoUnitTest
            // 
            this.txtPrefijoUnitTest.Location = new System.Drawing.Point(452, 83);
            this.txtPrefijoUnitTest.Name = "txtPrefijoUnitTest";
            this.txtPrefijoUnitTest.Size = new System.Drawing.Size(47, 20);
            this.txtPrefijoUnitTest.TabIndex = 18;
            this.txtPrefijoUnitTest.Text = "ut";
            this.txtPrefijoUnitTest.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Label27
            // 
            this.Label27.AutoSize = true;
            this.Label27.Location = new System.Drawing.Point(407, 86);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(39, 13);
            this.Label27.TabIndex = 17;
            this.Label27.Text = "Prefijo:";
            // 
            // txtNamespaceUnitTest
            // 
            this.txtNamespaceUnitTest.Location = new System.Drawing.Point(253, 83);
            this.txtNamespaceUnitTest.Name = "txtNamespaceUnitTest";
            this.txtNamespaceUnitTest.Size = new System.Drawing.Size(144, 20);
            this.txtNamespaceUnitTest.TabIndex = 16;
            this.txtNamespaceUnitTest.Text = "Test";
            // 
            // chkReAlUnitTest
            // 
            this.chkReAlUnitTest.AutoSize = true;
            this.chkReAlUnitTest.Checked = true;
            this.chkReAlUnitTest.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkReAlUnitTest.Location = new System.Drawing.Point(7, 86);
            this.chkReAlUnitTest.Name = "chkReAlUnitTest";
            this.chkReAlUnitTest.Size = new System.Drawing.Size(99, 17);
            this.chkReAlUnitTest.TabIndex = 15;
            this.chkReAlUnitTest.Text = "Crear UnitTests";
            this.chkReAlUnitTest.UseVisualStyleBackColor = true;
            // 
            // textbox10
            // 
            this.textbox10.Location = new System.Drawing.Point(499, 16);
            this.textbox10.Name = "textbox10";
            this.textbox10.ReadOnly = true;
            this.textbox10.Size = new System.Drawing.Size(117, 20);
            this.textbox10.TabIndex = 4;
            this.textbox10.TabStop = false;
            this.textbox10.Text = "[Nombre Clase]";
            // 
            // txtPrefijoEntidades
            // 
            this.txtPrefijoEntidades.Location = new System.Drawing.Point(452, 16);
            this.txtPrefijoEntidades.Name = "txtPrefijoEntidades";
            this.txtPrefijoEntidades.Size = new System.Drawing.Size(47, 20);
            this.txtPrefijoEntidades.TabIndex = 3;
            this.txtPrefijoEntidades.Text = "ent";
            this.txtPrefijoEntidades.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Location = new System.Drawing.Point(407, 19);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(39, 13);
            this.Label16.TabIndex = 2;
            this.Label16.Text = "Prefijo:";
            // 
            // TextBox6
            // 
            this.TextBox6.Location = new System.Drawing.Point(499, 38);
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.ReadOnly = true;
            this.TextBox6.Size = new System.Drawing.Size(117, 20);
            this.TextBox6.TabIndex = 9;
            this.TextBox6.TabStop = false;
            this.TextBox6.Text = "[Nombre Interface]";
            // 
            // txtPrefijoInterface
            // 
            this.txtPrefijoInterface.Location = new System.Drawing.Point(452, 38);
            this.txtPrefijoInterface.Name = "txtPrefijoInterface";
            this.txtPrefijoInterface.Size = new System.Drawing.Size(47, 20);
            this.txtPrefijoInterface.TabIndex = 8;
            this.txtPrefijoInterface.Text = "in";
            this.txtPrefijoInterface.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Location = new System.Drawing.Point(407, 41);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(39, 13);
            this.Label15.TabIndex = 7;
            this.Label15.Text = "Prefijo:";
            // 
            // TextBox3
            // 
            this.TextBox3.Location = new System.Drawing.Point(499, 60);
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.ReadOnly = true;
            this.TextBox3.Size = new System.Drawing.Size(117, 20);
            this.TextBox3.TabIndex = 14;
            this.TextBox3.TabStop = false;
            this.TextBox3.Text = "[Nombre Clase]";
            // 
            // txtPrefijoModelo
            // 
            this.txtPrefijoModelo.Location = new System.Drawing.Point(452, 60);
            this.txtPrefijoModelo.Name = "txtPrefijoModelo";
            this.txtPrefijoModelo.Size = new System.Drawing.Size(47, 20);
            this.txtPrefijoModelo.TabIndex = 13;
            this.txtPrefijoModelo.Text = "rn";
            this.txtPrefijoModelo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(407, 63);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(39, 13);
            this.Label14.TabIndex = 12;
            this.Label14.Text = "Prefijo:";
            // 
            // txtNamespaceNegocios
            // 
            this.txtNamespaceNegocios.Location = new System.Drawing.Point(253, 60);
            this.txtNamespaceNegocios.Name = "txtNamespaceNegocios";
            this.txtNamespaceNegocios.Size = new System.Drawing.Size(144, 20);
            this.txtNamespaceNegocios.TabIndex = 11;
            this.txtNamespaceNegocios.Text = "Modelo";
            // 
            // txtNamespaceInterface
            // 
            this.txtNamespaceInterface.Location = new System.Drawing.Point(253, 37);
            this.txtNamespaceInterface.Name = "txtNamespaceInterface";
            this.txtNamespaceInterface.Size = new System.Drawing.Size(144, 20);
            this.txtNamespaceInterface.TabIndex = 6;
            this.txtNamespaceInterface.Text = "Interfase";
            // 
            // txtNamespaceEntidades
            // 
            this.txtNamespaceEntidades.Location = new System.Drawing.Point(253, 14);
            this.txtNamespaceEntidades.Name = "txtNamespaceEntidades";
            this.txtNamespaceEntidades.Size = new System.Drawing.Size(144, 20);
            this.txtNamespaceEntidades.TabIndex = 1;
            this.txtNamespaceEntidades.Text = "Entidades";
            // 
            // chkReAlReglaNegocios
            // 
            this.chkReAlReglaNegocios.AutoSize = true;
            this.chkReAlReglaNegocios.Checked = true;
            this.chkReAlReglaNegocios.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkReAlReglaNegocios.Location = new System.Drawing.Point(7, 63);
            this.chkReAlReglaNegocios.Name = "chkReAlReglaNegocios";
            this.chkReAlReglaNegocios.Size = new System.Drawing.Size(230, 17);
            this.chkReAlReglaNegocios.TabIndex = 10;
            this.chkReAlReglaNegocios.Text = "Crear capa de BUSSINESS LOGIC LAYER";
            this.chkReAlReglaNegocios.UseVisualStyleBackColor = true;
            // 
            // chkReAlInterface
            // 
            this.chkReAlInterface.AutoSize = true;
            this.chkReAlInterface.Checked = true;
            this.chkReAlInterface.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkReAlInterface.Location = new System.Drawing.Point(7, 40);
            this.chkReAlInterface.Name = "chkReAlInterface";
            this.chkReAlInterface.Size = new System.Drawing.Size(193, 17);
            this.chkReAlInterface.TabIndex = 5;
            this.chkReAlInterface.Text = "Crear capa de INTERFACE (B.L.L.)";
            this.chkReAlInterface.UseVisualStyleBackColor = true;
            // 
            // chkReAlEntidades
            // 
            this.chkReAlEntidades.AutoSize = true;
            this.chkReAlEntidades.Checked = true;
            this.chkReAlEntidades.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkReAlEntidades.Location = new System.Drawing.Point(7, 16);
            this.chkReAlEntidades.Name = "chkReAlEntidades";
            this.chkReAlEntidades.Size = new System.Drawing.Size(208, 17);
            this.chkReAlEntidades.TabIndex = 0;
            this.chkReAlEntidades.Text = "Crear capa de BUSSINESS OBJECTS";
            this.chkReAlEntidades.UseVisualStyleBackColor = true;
            // 
            // btnGralAbrirCarpeta
            // 
            this.btnGralAbrirCarpeta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGralAbrirCarpeta.Location = new System.Drawing.Point(449, 120);
            this.btnGralAbrirCarpeta.Name = "btnGralAbrirCarpeta";
            this.btnGralAbrirCarpeta.Size = new System.Drawing.Size(183, 38);
            this.btnGralAbrirCarpeta.TabIndex = 7;
            this.btnGralAbrirCarpeta.Text = "&Abrir Carpeta Contenedora";
            this.btnGralAbrirCarpeta.UseVisualStyleBackColor = true;
            this.btnGralAbrirCarpeta.Click += new System.EventHandler(this.btnGralAbrirCarpeta_Click);
            // 
            // GroupBox7
            // 
            this.GroupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBox7.Controls.Add(this.cmbTipoColumnas);
            this.GroupBox7.Location = new System.Drawing.Point(276, 33);
            this.GroupBox7.Name = "GroupBox7";
            this.GroupBox7.Size = new System.Drawing.Size(165, 38);
            this.GroupBox7.TabIndex = 3;
            this.GroupBox7.TabStop = false;
            this.GroupBox7.Text = "Tipo de Columnas";
            // 
            // cmbTipoColumnas
            // 
            this.cmbTipoColumnas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoColumnas.FormattingEnabled = true;
            this.cmbTipoColumnas.Items.AddRange(new object[] {
            "Primera Mayuscula",
            "Minusculas",
            "Mayusculas",
            "camel Case",
            "Pascal Case"});
            this.cmbTipoColumnas.Location = new System.Drawing.Point(6, 12);
            this.cmbTipoColumnas.Name = "cmbTipoColumnas";
            this.cmbTipoColumnas.Size = new System.Drawing.Size(153, 21);
            this.cmbTipoColumnas.TabIndex = 0;
            // 
            // grpPathDel
            // 
            this.grpPathDel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grpPathDel.Controls.Add(this.txtPathProyecto);
            this.grpPathDel.Controls.Add(this.chkMoverResultados);
            this.grpPathDel.Controls.Add(this.btnPathProyecto);
            this.grpPathDel.Location = new System.Drawing.Point(447, 36);
            this.grpPathDel.Name = "grpPathDel";
            this.grpPathDel.Size = new System.Drawing.Size(185, 79);
            this.grpPathDel.TabIndex = 5;
            this.grpPathDel.TabStop = false;
            this.grpPathDel.Text = "Path del Proyecto";
            // 
            // txtPathProyecto
            // 
            this.txtPathProyecto.Location = new System.Drawing.Point(11, 19);
            this.txtPathProyecto.Name = "txtPathProyecto";
            this.txtPathProyecto.ReadOnly = true;
            this.txtPathProyecto.Size = new System.Drawing.Size(169, 20);
            this.txtPathProyecto.TabIndex = 0;
            this.txtPathProyecto.TabStop = false;
            // 
            // chkMoverResultados
            // 
            this.chkMoverResultados.AutoSize = true;
            this.chkMoverResultados.Checked = true;
            this.chkMoverResultados.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMoverResultados.Location = new System.Drawing.Point(10, 54);
            this.chkMoverResultados.Name = "chkMoverResultados";
            this.chkMoverResultados.Size = new System.Drawing.Size(136, 17);
            this.chkMoverResultados.TabIndex = 1;
            this.chkMoverResultados.Text = "Resultados al Escritorio";
            this.chkMoverResultados.UseVisualStyleBackColor = true;
            // 
            // btnPathProyecto
            // 
            this.btnPathProyecto.Location = new System.Drawing.Point(152, 52);
            this.btnPathProyecto.Name = "btnPathProyecto";
            this.btnPathProyecto.Size = new System.Drawing.Size(27, 22);
            this.btnPathProyecto.TabIndex = 2;
            this.btnPathProyecto.Text = "...";
            this.btnPathProyecto.UseVisualStyleBackColor = true;
            // 
            // GroupBox23
            // 
            this.GroupBox23.Controls.Add(this.txtInfAutor);
            this.GroupBox23.Location = new System.Drawing.Point(278, 116);
            this.GroupBox23.Name = "GroupBox23";
            this.GroupBox23.Size = new System.Drawing.Size(165, 38);
            this.GroupBox23.TabIndex = 6;
            this.GroupBox23.TabStop = false;
            this.GroupBox23.Text = "Autor";
            // 
            // txtInfAutor
            // 
            this.txtInfAutor.Location = new System.Drawing.Point(6, 14);
            this.txtInfAutor.MaxLength = 15;
            this.txtInfAutor.Name = "txtInfAutor";
            this.txtInfAutor.Size = new System.Drawing.Size(153, 20);
            this.txtInfAutor.TabIndex = 0;
            // 
            // GroupBox19
            // 
            this.GroupBox19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBox19.Controls.Add(this.cmbAPIs);
            this.GroupBox19.Location = new System.Drawing.Point(276, 77);
            this.GroupBox19.Name = "GroupBox19";
            this.GroupBox19.Size = new System.Drawing.Size(165, 38);
            this.GroupBox19.TabIndex = 4;
            this.GroupBox19.TabStop = false;
            this.GroupBox19.Text = "Tipo APIs y Auditoria";
            // 
            // cmbAPIs
            // 
            this.cmbAPIs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAPIs.FormattingEnabled = true;
            this.cmbAPIs.Items.AddRange(new object[] {
            "UsuCreTabla",
            "UsuCre",
            "usu_cre_tabla",
            "usu_cre"});
            this.cmbAPIs.Location = new System.Drawing.Point(6, 13);
            this.cmbAPIs.Name = "cmbAPIs";
            this.cmbAPIs.Size = new System.Drawing.Size(153, 21);
            this.cmbAPIs.TabIndex = 0;
            // 
            // grpGroupBox8
            // 
            this.grpGroupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grpGroupBox8.Controls.Add(this.Label46);
            this.grpGroupBox8.Controls.Add(this.txtInfProyectoConn);
            this.grpGroupBox8.Controls.Add(this.Label28);
            this.grpGroupBox8.Controls.Add(this.txtInfProyectoUnitTest);
            this.grpGroupBox8.Controls.Add(this.Label12);
            this.grpGroupBox8.Controls.Add(this.txtInfProyectoDal);
            this.grpGroupBox8.Controls.Add(this.txtInfProyecto);
            this.grpGroupBox8.Controls.Add(this.Label8);
            this.grpGroupBox8.Controls.Add(this.txtInfProyectoClass);
            this.grpGroupBox8.Controls.Add(this.Label6);
            this.grpGroupBox8.Location = new System.Drawing.Point(6, 33);
            this.grpGroupBox8.Name = "grpGroupBox8";
            this.grpGroupBox8.Size = new System.Drawing.Size(264, 128);
            this.grpGroupBox8.TabIndex = 2;
            this.grpGroupBox8.TabStop = false;
            this.grpGroupBox8.Text = "Proyectos";
            // 
            // Label46
            // 
            this.Label46.AutoSize = true;
            this.Label46.Location = new System.Drawing.Point(8, 82);
            this.Label46.Name = "Label46";
            this.Label46.Size = new System.Drawing.Size(83, 13);
            this.Label46.TabIndex = 6;
            this.Label46.Text = "Proyecto CONN";
            // 
            // txtInfProyectoConn
            // 
            this.txtInfProyectoConn.Location = new System.Drawing.Point(97, 79);
            this.txtInfProyectoConn.MaxLength = 250;
            this.txtInfProyectoConn.Name = "txtInfProyectoConn";
            this.txtInfProyectoConn.Size = new System.Drawing.Size(161, 20);
            this.txtInfProyectoConn.TabIndex = 7;
            // 
            // Label28
            // 
            this.Label28.AutoSize = true;
            this.Label28.Location = new System.Drawing.Point(8, 103);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(67, 13);
            this.Label28.TabIndex = 8;
            this.Label28.Text = "Proyecto UT";
            // 
            // txtInfProyectoUnitTest
            // 
            this.txtInfProyectoUnitTest.Location = new System.Drawing.Point(97, 100);
            this.txtInfProyectoUnitTest.MaxLength = 250;
            this.txtInfProyectoUnitTest.Name = "txtInfProyectoUnitTest";
            this.txtInfProyectoUnitTest.Size = new System.Drawing.Size(161, 20);
            this.txtInfProyectoUnitTest.TabIndex = 9;
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(8, 61);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(73, 13);
            this.Label12.TabIndex = 4;
            this.Label12.Text = "Proyecto DAL";
            // 
            // txtInfProyectoDal
            // 
            this.txtInfProyectoDal.Location = new System.Drawing.Point(97, 58);
            this.txtInfProyectoDal.MaxLength = 250;
            this.txtInfProyectoDal.Name = "txtInfProyectoDal";
            this.txtInfProyectoDal.Size = new System.Drawing.Size(161, 20);
            this.txtInfProyectoDal.TabIndex = 5;
            // 
            // txtInfProyecto
            // 
            this.txtInfProyecto.Location = new System.Drawing.Point(97, 15);
            this.txtInfProyecto.MaxLength = 250;
            this.txtInfProyecto.Name = "txtInfProyecto";
            this.txtInfProyecto.Size = new System.Drawing.Size(161, 20);
            this.txtInfProyecto.TabIndex = 1;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(7, 18);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(74, 13);
            this.Label8.TabIndex = 0;
            this.Label8.Text = "Proyecto App:";
            // 
            // txtInfProyectoClass
            // 
            this.txtInfProyectoClass.Location = new System.Drawing.Point(97, 36);
            this.txtInfProyectoClass.MaxLength = 250;
            this.txtInfProyectoClass.Name = "txtInfProyectoClass";
            this.txtInfProyectoClass.Size = new System.Drawing.Size(161, 20);
            this.txtInfProyectoClass.TabIndex = 3;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(7, 39);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(67, 13);
            this.Label6.TabIndex = 2;
            this.Label6.Text = "Proyecto BO";
            // 
            // tabMainPage2
            // 
            this.tabMainPage2.BackColor = System.Drawing.Color.Silver;
            this.tabMainPage2.Controls.Add(this.groupBoxDiccionarioDeDatos);
            this.tabMainPage2.Controls.Add(this.groupBox1);
            this.tabMainPage2.Controls.Add(this.grpColumnasTriggers);
            this.tabMainPage2.Controls.Add(this.GroupBox4);
            this.tabMainPage2.Controls.Add(this.GroupBox3);
            this.tabMainPage2.Location = new System.Drawing.Point(4, 22);
            this.tabMainPage2.Name = "tabMainPage2";
            this.tabMainPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabMainPage2.Size = new System.Drawing.Size(638, 599);
            this.tabMainPage2.TabIndex = 1;
            this.tabMainPage2.Text = "Base de Datos";
            // 
            // groupBoxDiccionarioDeDatos
            // 
            this.groupBoxDiccionarioDeDatos.Controls.Add(this.btnBdDocumentarTodaLaBaseDeDatos);
            this.groupBoxDiccionarioDeDatos.Controls.Add(this.btnBdCrearCicloDeVida);
            this.groupBoxDiccionarioDeDatos.Location = new System.Drawing.Point(6, 510);
            this.groupBoxDiccionarioDeDatos.Name = "groupBoxDiccionarioDeDatos";
            this.groupBoxDiccionarioDeDatos.Size = new System.Drawing.Size(626, 83);
            this.groupBoxDiccionarioDeDatos.TabIndex = 5;
            this.groupBoxDiccionarioDeDatos.TabStop = false;
            this.groupBoxDiccionarioDeDatos.Text = "Diccionario de Datos";
            // 
            // btnBdDocumentarTodaLaBaseDeDatos
            // 
            this.btnBdDocumentarTodaLaBaseDeDatos.Location = new System.Drawing.Point(268, 19);
            this.btnBdDocumentarTodaLaBaseDeDatos.Name = "btnBdDocumentarTodaLaBaseDeDatos";
            this.btnBdDocumentarTodaLaBaseDeDatos.Size = new System.Drawing.Size(255, 35);
            this.btnBdDocumentarTodaLaBaseDeDatos.TabIndex = 5;
            this.btnBdDocumentarTodaLaBaseDeDatos.Text = "Documentar toda la Base de Datos";
            this.btnBdDocumentarTodaLaBaseDeDatos.UseVisualStyleBackColor = true;
            this.btnBdDocumentarTodaLaBaseDeDatos.Click += new System.EventHandler(this.btnBdDocumentarTodaLaBaseDeDatos_Click);
            // 
            // btnBdCrearCicloDeVida
            // 
            this.btnBdCrearCicloDeVida.Location = new System.Drawing.Point(7, 19);
            this.btnBdCrearCicloDeVida.Name = "btnBdCrearCicloDeVida";
            this.btnBdCrearCicloDeVida.Size = new System.Drawing.Size(255, 35);
            this.btnBdCrearCicloDeVida.TabIndex = 4;
            this.btnBdCrearCicloDeVida.Text = "Crear Diccionario y Ciclos de Vida de Tablas seleccionadas";
            this.btnBdCrearCicloDeVida.UseVisualStyleBackColor = true;
            this.btnBdCrearCicloDeVida.Click += new System.EventHandler(this.btnCrearCicloDeVida_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnBDExportarExcel);
            this.groupBox1.Controls.Add(this.txtBDExportarExcel);
            this.groupBox1.Location = new System.Drawing.Point(6, 324);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(626, 180);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Exportar a Excel";
            // 
            // btnBDExportarExcel
            // 
            this.btnBDExportarExcel.Location = new System.Drawing.Point(420, 139);
            this.btnBDExportarExcel.Name = "btnBDExportarExcel";
            this.btnBDExportarExcel.Size = new System.Drawing.Size(200, 35);
            this.btnBDExportarExcel.TabIndex = 1;
            this.btnBDExportarExcel.Text = "Exportar resultado de Query a Excel";
            this.btnBDExportarExcel.UseVisualStyleBackColor = true;
            this.btnBDExportarExcel.Click += new System.EventHandler(this.btnBDExportarExcel_Click);
            // 
            // txtBDExportarExcel
            // 
            this.txtBDExportarExcel.Location = new System.Drawing.Point(6, 19);
            this.txtBDExportarExcel.Multiline = true;
            this.txtBDExportarExcel.Name = "txtBDExportarExcel";
            this.txtBDExportarExcel.Size = new System.Drawing.Size(614, 114);
            this.txtBDExportarExcel.TabIndex = 0;
            // 
            // grpColumnasTriggers
            // 
            this.grpColumnasTriggers.Controls.Add(this.GroupBox18);
            this.grpColumnasTriggers.Controls.Add(this.GroupBox17);
            this.grpColumnasTriggers.Controls.Add(this.GroupBox16);
            this.grpColumnasTriggers.Controls.Add(this.btnBDCrearTriggers);
            this.grpColumnasTriggers.Location = new System.Drawing.Point(6, 166);
            this.grpColumnasTriggers.Name = "grpColumnasTriggers";
            this.grpColumnasTriggers.Size = new System.Drawing.Size(626, 157);
            this.grpColumnasTriggers.TabIndex = 2;
            this.grpColumnasTriggers.TabStop = false;
            this.grpColumnasTriggers.Text = "Columnas y Triggers de control transaccional";
            // 
            // GroupBox18
            // 
            this.GroupBox18.Controls.Add(this.chkBDOpcionesTriggersInstead);
            this.GroupBox18.Controls.Add(this.chkBDOpcionesTriggers);
            this.GroupBox18.Controls.Add(this.chkBDOpcionesTriggersSimple);
            this.GroupBox18.Controls.Add(this.chkBDOpcionesCols);
            this.GroupBox18.Location = new System.Drawing.Point(268, 19);
            this.GroupBox18.Name = "GroupBox18";
            this.GroupBox18.Size = new System.Drawing.Size(352, 91);
            this.GroupBox18.TabIndex = 2;
            this.GroupBox18.TabStop = false;
            this.GroupBox18.Text = "Opciones";
            // 
            // chkBDOpcionesTriggersInstead
            // 
            this.chkBDOpcionesTriggersInstead.AutoSize = true;
            this.chkBDOpcionesTriggersInstead.Checked = true;
            this.chkBDOpcionesTriggersInstead.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBDOpcionesTriggersInstead.Location = new System.Drawing.Point(6, 33);
            this.chkBDOpcionesTriggersInstead.Name = "chkBDOpcionesTriggersInstead";
            this.chkBDOpcionesTriggersInstead.Size = new System.Drawing.Size(201, 17);
            this.chkBDOpcionesTriggersInstead.TabIndex = 1;
            this.chkBDOpcionesTriggersInstead.Text = "Triggers INSTEAD OF (Solo SqlServ)";
            this.chkBDOpcionesTriggersInstead.UseVisualStyleBackColor = true;
            // 
            // chkBDOpcionesTriggers
            // 
            this.chkBDOpcionesTriggers.AutoSize = true;
            this.chkBDOpcionesTriggers.Location = new System.Drawing.Point(6, 51);
            this.chkBDOpcionesTriggers.Name = "chkBDOpcionesTriggers";
            this.chkBDOpcionesTriggers.Size = new System.Drawing.Size(64, 17);
            this.chkBDOpcionesTriggers.TabIndex = 2;
            this.chkBDOpcionesTriggers.Text = "Triggers";
            this.chkBDOpcionesTriggers.UseVisualStyleBackColor = true;
            // 
            // chkBDOpcionesTriggersSimple
            // 
            this.chkBDOpcionesTriggersSimple.AutoSize = true;
            this.chkBDOpcionesTriggersSimple.Location = new System.Drawing.Point(6, 71);
            this.chkBDOpcionesTriggersSimple.Name = "chkBDOpcionesTriggersSimple";
            this.chkBDOpcionesTriggersSimple.Size = new System.Drawing.Size(103, 17);
            this.chkBDOpcionesTriggersSimple.TabIndex = 3;
            this.chkBDOpcionesTriggersSimple.Text = "Triggers Simples";
            this.chkBDOpcionesTriggersSimple.UseVisualStyleBackColor = true;
            // 
            // chkBDOpcionesCols
            // 
            this.chkBDOpcionesCols.AutoSize = true;
            this.chkBDOpcionesCols.Location = new System.Drawing.Point(6, 15);
            this.chkBDOpcionesCols.Name = "chkBDOpcionesCols";
            this.chkBDOpcionesCols.Size = new System.Drawing.Size(113, 17);
            this.chkBDOpcionesCols.TabIndex = 0;
            this.chkBDOpcionesCols.Text = "Generar Columnas";
            this.chkBDOpcionesCols.UseVisualStyleBackColor = true;
            // 
            // GroupBox17
            // 
            this.GroupBox17.Controls.Add(this.chkTriggerEncrypt);
            this.GroupBox17.Controls.Add(this.chkTriggerInsert);
            this.GroupBox17.Controls.Add(this.chkTriggerDelete);
            this.GroupBox17.Controls.Add(this.chkTriggerUpdate);
            this.GroupBox17.Location = new System.Drawing.Point(123, 19);
            this.GroupBox17.Name = "GroupBox17";
            this.GroupBox17.Size = new System.Drawing.Size(139, 122);
            this.GroupBox17.TabIndex = 1;
            this.GroupBox17.TabStop = false;
            this.GroupBox17.Text = "Triggers";
            // 
            // chkTriggerEncrypt
            // 
            this.chkTriggerEncrypt.AutoSize = true;
            this.chkTriggerEncrypt.Checked = true;
            this.chkTriggerEncrypt.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTriggerEncrypt.Location = new System.Drawing.Point(6, 103);
            this.chkTriggerEncrypt.Name = "chkTriggerEncrypt";
            this.chkTriggerEncrypt.Size = new System.Drawing.Size(82, 17);
            this.chkTriggerEncrypt.TabIndex = 3;
            this.chkTriggerEncrypt.Text = "Encriptados";
            this.chkTriggerEncrypt.UseVisualStyleBackColor = true;
            // 
            // chkTriggerInsert
            // 
            this.chkTriggerInsert.AutoSize = true;
            this.chkTriggerInsert.Checked = true;
            this.chkTriggerInsert.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTriggerInsert.Location = new System.Drawing.Point(6, 15);
            this.chkTriggerInsert.Name = "chkTriggerInsert";
            this.chkTriggerInsert.Size = new System.Drawing.Size(52, 17);
            this.chkTriggerInsert.TabIndex = 0;
            this.chkTriggerInsert.Text = "Insert";
            this.chkTriggerInsert.UseVisualStyleBackColor = true;
            // 
            // chkTriggerDelete
            // 
            this.chkTriggerDelete.AutoSize = true;
            this.chkTriggerDelete.Checked = true;
            this.chkTriggerDelete.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTriggerDelete.Location = new System.Drawing.Point(6, 51);
            this.chkTriggerDelete.Name = "chkTriggerDelete";
            this.chkTriggerDelete.Size = new System.Drawing.Size(57, 17);
            this.chkTriggerDelete.TabIndex = 2;
            this.chkTriggerDelete.Text = "Delete";
            this.chkTriggerDelete.UseVisualStyleBackColor = true;
            // 
            // chkTriggerUpdate
            // 
            this.chkTriggerUpdate.AutoSize = true;
            this.chkTriggerUpdate.Checked = true;
            this.chkTriggerUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTriggerUpdate.Location = new System.Drawing.Point(6, 33);
            this.chkTriggerUpdate.Name = "chkTriggerUpdate";
            this.chkTriggerUpdate.Size = new System.Drawing.Size(61, 17);
            this.chkTriggerUpdate.TabIndex = 1;
            this.chkTriggerUpdate.Text = "Update";
            this.chkTriggerUpdate.UseVisualStyleBackColor = true;
            // 
            // GroupBox16
            // 
            this.GroupBox16.Controls.Add(this.chkBdColEstado);
            this.GroupBox16.Controls.Add(this.chkBdColUsuCre);
            this.GroupBox16.Controls.Add(this.chkBdColFecMod);
            this.GroupBox16.Controls.Add(this.chkBdColUsuMod);
            this.GroupBox16.Controls.Add(this.chkBdColTrans);
            this.GroupBox16.Controls.Add(this.chkBdColFecCre);
            this.GroupBox16.Location = new System.Drawing.Point(7, 19);
            this.GroupBox16.Name = "GroupBox16";
            this.GroupBox16.Size = new System.Drawing.Size(111, 124);
            this.GroupBox16.TabIndex = 0;
            this.GroupBox16.TabStop = false;
            this.GroupBox16.Text = "Columnas";
            // 
            // chkBdColEstado
            // 
            this.chkBdColEstado.AutoSize = true;
            this.chkBdColEstado.Checked = true;
            this.chkBdColEstado.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBdColEstado.Location = new System.Drawing.Point(6, 15);
            this.chkBdColEstado.Name = "chkBdColEstado";
            this.chkBdColEstado.Size = new System.Drawing.Size(74, 17);
            this.chkBdColEstado.TabIndex = 0;
            this.chkBdColEstado.Text = "ApiEstado";
            this.chkBdColEstado.UseVisualStyleBackColor = true;
            // 
            // chkBdColUsuCre
            // 
            this.chkBdColUsuCre.AutoSize = true;
            this.chkBdColUsuCre.Checked = true;
            this.chkBdColUsuCre.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBdColUsuCre.Location = new System.Drawing.Point(6, 51);
            this.chkBdColUsuCre.Name = "chkBdColUsuCre";
            this.chkBdColUsuCre.Size = new System.Drawing.Size(61, 17);
            this.chkBdColUsuCre.TabIndex = 2;
            this.chkBdColUsuCre.Text = "UsuCre";
            this.chkBdColUsuCre.UseVisualStyleBackColor = true;
            // 
            // chkBdColFecMod
            // 
            this.chkBdColFecMod.AutoSize = true;
            this.chkBdColFecMod.Checked = true;
            this.chkBdColFecMod.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBdColFecMod.Location = new System.Drawing.Point(6, 105);
            this.chkBdColFecMod.Name = "chkBdColFecMod";
            this.chkBdColFecMod.Size = new System.Drawing.Size(65, 17);
            this.chkBdColFecMod.TabIndex = 5;
            this.chkBdColFecMod.Text = "FecMod";
            this.chkBdColFecMod.UseVisualStyleBackColor = true;
            // 
            // chkBdColUsuMod
            // 
            this.chkBdColUsuMod.AutoSize = true;
            this.chkBdColUsuMod.Checked = true;
            this.chkBdColUsuMod.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBdColUsuMod.Location = new System.Drawing.Point(6, 87);
            this.chkBdColUsuMod.Name = "chkBdColUsuMod";
            this.chkBdColUsuMod.Size = new System.Drawing.Size(66, 17);
            this.chkBdColUsuMod.TabIndex = 4;
            this.chkBdColUsuMod.Text = "UsuMod";
            this.chkBdColUsuMod.UseVisualStyleBackColor = true;
            // 
            // chkBdColTrans
            // 
            this.chkBdColTrans.AutoSize = true;
            this.chkBdColTrans.Checked = true;
            this.chkBdColTrans.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBdColTrans.Location = new System.Drawing.Point(6, 33);
            this.chkBdColTrans.Name = "chkBdColTrans";
            this.chkBdColTrans.Size = new System.Drawing.Size(100, 17);
            this.chkBdColTrans.TabIndex = 1;
            this.chkBdColTrans.Text = "ApiTransaccion";
            this.chkBdColTrans.UseVisualStyleBackColor = true;
            // 
            // chkBdColFecCre
            // 
            this.chkBdColFecCre.AutoSize = true;
            this.chkBdColFecCre.Checked = true;
            this.chkBdColFecCre.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBdColFecCre.Location = new System.Drawing.Point(6, 69);
            this.chkBdColFecCre.Name = "chkBdColFecCre";
            this.chkBdColFecCre.Size = new System.Drawing.Size(60, 17);
            this.chkBdColFecCre.TabIndex = 3;
            this.chkBdColFecCre.Text = "FecCre";
            this.chkBdColFecCre.UseVisualStyleBackColor = true;
            // 
            // btnBDCrearTriggers
            // 
            this.btnBDCrearTriggers.Location = new System.Drawing.Point(420, 116);
            this.btnBDCrearTriggers.Name = "btnBDCrearTriggers";
            this.btnBDCrearTriggers.Size = new System.Drawing.Size(200, 35);
            this.btnBDCrearTriggers.TabIndex = 3;
            this.btnBDCrearTriggers.Text = "Crear Columnas y/o &Triggers";
            this.btnBDCrearTriggers.UseVisualStyleBackColor = true;
            this.btnBDCrearTriggers.Click += new System.EventHandler(this.btnBDCrearTriggers_Click);
            // 
            // GroupBox4
            // 
            this.GroupBox4.Controls.Add(this.rdbSpIfExists);
            this.GroupBox4.Controls.Add(this.rdbSpAlter);
            this.GroupBox4.Controls.Add(this.rdbSpCreate);
            this.GroupBox4.Controls.Add(this.grpSpIns);
            this.GroupBox4.Controls.Add(this.grpSpUpd);
            this.GroupBox4.Controls.Add(this.btnSP);
            this.GroupBox4.Controls.Add(this.txtSPsig);
            this.GroupBox4.Controls.Add(this.TextBox4);
            this.GroupBox4.Controls.Add(this.txtSPprev);
            this.GroupBox4.Controls.Add(this.Label3);
            this.GroupBox4.Location = new System.Drawing.Point(184, 6);
            this.GroupBox4.Name = "GroupBox4";
            this.GroupBox4.Size = new System.Drawing.Size(448, 154);
            this.GroupBox4.TabIndex = 1;
            this.GroupBox4.TabStop = false;
            this.GroupBox4.Text = "Datos SP";
            // 
            // rdbSpIfExists
            // 
            this.rdbSpIfExists.AutoSize = true;
            this.rdbSpIfExists.Checked = true;
            this.rdbSpIfExists.Location = new System.Drawing.Point(255, 37);
            this.rdbSpIfExists.Name = "rdbSpIfExists";
            this.rdbSpIfExists.Size = new System.Drawing.Size(109, 17);
            this.rdbSpIfExists.TabIndex = 6;
            this.rdbSpIfExists.TabStop = true;
            this.rdbSpIfExists.Text = "IF EXISTS DROP";
            this.rdbSpIfExists.UseVisualStyleBackColor = true;
            // 
            // rdbSpAlter
            // 
            this.rdbSpAlter.AutoSize = true;
            this.rdbSpAlter.Location = new System.Drawing.Point(133, 37);
            this.rdbSpAlter.Name = "rdbSpAlter";
            this.rdbSpAlter.Size = new System.Drawing.Size(93, 17);
            this.rdbSpAlter.TabIndex = 5;
            this.rdbSpAlter.Text = "ALTER PROC";
            this.rdbSpAlter.UseVisualStyleBackColor = true;
            // 
            // rdbSpCreate
            // 
            this.rdbSpCreate.AutoSize = true;
            this.rdbSpCreate.Location = new System.Drawing.Point(9, 37);
            this.rdbSpCreate.Name = "rdbSpCreate";
            this.rdbSpCreate.Size = new System.Drawing.Size(101, 17);
            this.rdbSpCreate.TabIndex = 4;
            this.rdbSpCreate.Text = "CREATE PROC";
            this.rdbSpCreate.UseVisualStyleBackColor = true;
            // 
            // grpSpIns
            // 
            this.grpSpIns.Controls.Add(this.chkSPInsIdentity);
            this.grpSpIns.Controls.Add(this.chkSPInsFecCreGetDate);
            this.grpSpIns.Controls.Add(this.chkSPInsApiEstado);
            this.grpSpIns.Controls.Add(this.chkSPInsFecCre);
            this.grpSpIns.Controls.Add(this.chkSPInsUsuCre);
            this.grpSpIns.Location = new System.Drawing.Point(6, 55);
            this.grpSpIns.Name = "grpSpIns";
            this.grpSpIns.Size = new System.Drawing.Size(194, 86);
            this.grpSpIns.TabIndex = 7;
            this.grpSpIns.TabStop = false;
            this.grpSpIns.Text = "Parametros Insert";
            // 
            // chkSPInsIdentity
            // 
            this.chkSPInsIdentity.AutoSize = true;
            this.chkSPInsIdentity.Location = new System.Drawing.Point(6, 67);
            this.chkSPInsIdentity.Name = "chkSPInsIdentity";
            this.chkSPInsIdentity.Size = new System.Drawing.Size(147, 17);
            this.chkSPInsIdentity.TabIndex = 4;
            this.chkSPInsIdentity.Text = "Primera PK autogenerada";
            this.chkSPInsIdentity.UseVisualStyleBackColor = true;
            // 
            // chkSPInsFecCreGetDate
            // 
            this.chkSPInsFecCreGetDate.AutoSize = true;
            this.chkSPInsFecCreGetDate.Checked = true;
            this.chkSPInsFecCreGetDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSPInsFecCreGetDate.Location = new System.Drawing.Point(6, 35);
            this.chkSPInsFecCreGetDate.Name = "chkSPInsFecCreGetDate";
            this.chkSPInsFecCreGetDate.Size = new System.Drawing.Size(128, 17);
            this.chkSPInsFecCreGetDate.TabIndex = 2;
            this.chkSPInsFecCreGetDate.Text = "FecCre es GETDATE";
            this.chkSPInsFecCreGetDate.UseVisualStyleBackColor = true;
            // 
            // chkSPInsApiEstado
            // 
            this.chkSPInsApiEstado.AutoSize = true;
            this.chkSPInsApiEstado.Checked = true;
            this.chkSPInsApiEstado.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSPInsApiEstado.Location = new System.Drawing.Point(6, 51);
            this.chkSPInsApiEstado.Name = "chkSPInsApiEstado";
            this.chkSPInsApiEstado.Size = new System.Drawing.Size(158, 17);
            this.chkSPInsApiEstado.TabIndex = 3;
            this.chkSPInsApiEstado.Text = "Estado Inicial ELABORADO";
            this.chkSPInsApiEstado.UseVisualStyleBackColor = true;
            // 
            // chkSPInsFecCre
            // 
            this.chkSPInsFecCre.AutoSize = true;
            this.chkSPInsFecCre.Location = new System.Drawing.Point(91, 19);
            this.chkSPInsFecCre.Name = "chkSPInsFecCre";
            this.chkSPInsFecCre.Size = new System.Drawing.Size(60, 17);
            this.chkSPInsFecCre.TabIndex = 1;
            this.chkSPInsFecCre.Text = "FecCre";
            this.chkSPInsFecCre.UseVisualStyleBackColor = true;
            // 
            // chkSPInsUsuCre
            // 
            this.chkSPInsUsuCre.AutoSize = true;
            this.chkSPInsUsuCre.Checked = true;
            this.chkSPInsUsuCre.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSPInsUsuCre.Location = new System.Drawing.Point(6, 19);
            this.chkSPInsUsuCre.Name = "chkSPInsUsuCre";
            this.chkSPInsUsuCre.Size = new System.Drawing.Size(61, 17);
            this.chkSPInsUsuCre.TabIndex = 0;
            this.chkSPInsUsuCre.Text = "UsuCre";
            this.chkSPInsUsuCre.UseVisualStyleBackColor = true;
            // 
            // grpSpUpd
            // 
            this.grpSpUpd.Controls.Add(this.chkSPUpdFecModGetDate);
            this.grpSpUpd.Controls.Add(this.chkSPUpdFecMod);
            this.grpSpUpd.Controls.Add(this.chkSPUpdUsuMod);
            this.grpSpUpd.Location = new System.Drawing.Point(206, 55);
            this.grpSpUpd.Name = "grpSpUpd";
            this.grpSpUpd.Size = new System.Drawing.Size(200, 56);
            this.grpSpUpd.TabIndex = 8;
            this.grpSpUpd.TabStop = false;
            this.grpSpUpd.Text = "Parametros Update";
            // 
            // chkSPUpdFecModGetDate
            // 
            this.chkSPUpdFecModGetDate.AutoSize = true;
            this.chkSPUpdFecModGetDate.Checked = true;
            this.chkSPUpdFecModGetDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSPUpdFecModGetDate.Location = new System.Drawing.Point(6, 35);
            this.chkSPUpdFecModGetDate.Name = "chkSPUpdFecModGetDate";
            this.chkSPUpdFecModGetDate.Size = new System.Drawing.Size(133, 17);
            this.chkSPUpdFecModGetDate.TabIndex = 2;
            this.chkSPUpdFecModGetDate.Text = "FecMod es GETDATE";
            this.chkSPUpdFecModGetDate.UseVisualStyleBackColor = true;
            // 
            // chkSPUpdFecMod
            // 
            this.chkSPUpdFecMod.AutoSize = true;
            this.chkSPUpdFecMod.Location = new System.Drawing.Point(103, 19);
            this.chkSPUpdFecMod.Name = "chkSPUpdFecMod";
            this.chkSPUpdFecMod.Size = new System.Drawing.Size(65, 17);
            this.chkSPUpdFecMod.TabIndex = 1;
            this.chkSPUpdFecMod.Text = "FecMod";
            this.chkSPUpdFecMod.UseVisualStyleBackColor = true;
            // 
            // chkSPUpdUsuMod
            // 
            this.chkSPUpdUsuMod.AutoSize = true;
            this.chkSPUpdUsuMod.Checked = true;
            this.chkSPUpdUsuMod.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSPUpdUsuMod.Location = new System.Drawing.Point(6, 19);
            this.chkSPUpdUsuMod.Name = "chkSPUpdUsuMod";
            this.chkSPUpdUsuMod.Size = new System.Drawing.Size(66, 17);
            this.chkSPUpdUsuMod.TabIndex = 0;
            this.chkSPUpdUsuMod.Text = "UsuMod";
            this.chkSPUpdUsuMod.UseVisualStyleBackColor = true;
            // 
            // btnSP
            // 
            this.btnSP.Location = new System.Drawing.Point(206, 113);
            this.btnSP.Name = "btnSP";
            this.btnSP.Size = new System.Drawing.Size(200, 35);
            this.btnSP.TabIndex = 9;
            this.btnSP.Text = "&Generar Procedimientos Almacenados";
            this.btnSP.UseVisualStyleBackColor = true;
            this.btnSP.Click += new System.EventHandler(this.btnSP_Click);
            // 
            // txtSPsig
            // 
            this.txtSPsig.Location = new System.Drawing.Point(294, 13);
            this.txtSPsig.Name = "txtSPsig";
            this.txtSPsig.Size = new System.Drawing.Size(72, 20);
            this.txtSPsig.TabIndex = 3;
            // 
            // TextBox4
            // 
            this.TextBox4.Location = new System.Drawing.Point(206, 13);
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.ReadOnly = true;
            this.TextBox4.Size = new System.Drawing.Size(82, 20);
            this.TextBox4.TabIndex = 2;
            this.TextBox4.TabStop = false;
            this.TextBox4.Text = "[Nombre Tabla]";
            // 
            // txtSPprev
            // 
            this.txtSPprev.Location = new System.Drawing.Point(128, 13);
            this.txtSPprev.Name = "txtSPprev";
            this.txtSPprev.Size = new System.Drawing.Size(72, 20);
            this.txtSPprev.TabIndex = 1;
            this.txtSPprev.Text = "Sp";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(6, 16);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(88, 13);
            this.Label3.TabIndex = 0;
            this.Label3.Text = "Formato Nombre:";
            // 
            // GroupBox3
            // 
            this.GroupBox3.Controls.Add(this.chkSPInsUpd);
            this.GroupBox3.Controls.Add(this.chkSPsel);
            this.GroupBox3.Controls.Add(this.chkSPdel);
            this.GroupBox3.Controls.Add(this.chkSPupd);
            this.GroupBox3.Controls.Add(this.chkSPins);
            this.GroupBox3.Controls.Add(this.chkSPParametrosPa);
            this.GroupBox3.Location = new System.Drawing.Point(6, 6);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(172, 154);
            this.GroupBox3.TabIndex = 0;
            this.GroupBox3.TabStop = false;
            this.GroupBox3.Text = "Tipos SP";
            // 
            // chkSPInsUpd
            // 
            this.chkSPInsUpd.AutoSize = true;
            this.chkSPInsUpd.Location = new System.Drawing.Point(6, 85);
            this.chkSPInsUpd.Name = "chkSPInsUpd";
            this.chkSPInsUpd.Size = new System.Drawing.Size(92, 17);
            this.chkSPInsUpd.TabIndex = 3;
            this.chkSPInsUpd.Text = "Insert/Update";
            this.chkSPInsUpd.UseVisualStyleBackColor = true;
            // 
            // chkSPsel
            // 
            this.chkSPsel.AutoSize = true;
            this.chkSPsel.Location = new System.Drawing.Point(6, 107);
            this.chkSPsel.Name = "chkSPsel";
            this.chkSPsel.Size = new System.Drawing.Size(56, 17);
            this.chkSPsel.TabIndex = 4;
            this.chkSPsel.Text = "Select";
            this.chkSPsel.UseVisualStyleBackColor = true;
            // 
            // chkSPdel
            // 
            this.chkSPdel.AutoSize = true;
            this.chkSPdel.Checked = true;
            this.chkSPdel.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSPdel.Location = new System.Drawing.Point(6, 63);
            this.chkSPdel.Name = "chkSPdel";
            this.chkSPdel.Size = new System.Drawing.Size(57, 17);
            this.chkSPdel.TabIndex = 2;
            this.chkSPdel.Text = "Delete";
            this.chkSPdel.UseVisualStyleBackColor = true;
            // 
            // chkSPupd
            // 
            this.chkSPupd.AutoSize = true;
            this.chkSPupd.Checked = true;
            this.chkSPupd.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSPupd.Location = new System.Drawing.Point(6, 41);
            this.chkSPupd.Name = "chkSPupd";
            this.chkSPupd.Size = new System.Drawing.Size(61, 17);
            this.chkSPupd.TabIndex = 1;
            this.chkSPupd.Text = "Update";
            this.chkSPupd.UseVisualStyleBackColor = true;
            // 
            // chkSPins
            // 
            this.chkSPins.AutoSize = true;
            this.chkSPins.Checked = true;
            this.chkSPins.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSPins.Location = new System.Drawing.Point(6, 19);
            this.chkSPins.Name = "chkSPins";
            this.chkSPins.Size = new System.Drawing.Size(52, 17);
            this.chkSPins.TabIndex = 0;
            this.chkSPins.Text = "Insert";
            this.chkSPins.UseVisualStyleBackColor = true;
            // 
            // chkSPParametrosPa
            // 
            this.chkSPParametrosPa.AutoSize = true;
            this.chkSPParametrosPa.Location = new System.Drawing.Point(6, 131);
            this.chkSPParametrosPa.Name = "chkSPParametrosPa";
            this.chkSPParametrosPa.Size = new System.Drawing.Size(165, 17);
            this.chkSPParametrosPa.TabIndex = 5;
            this.chkSPParametrosPa.Text = "Incluir \"pa\" en los parametros";
            this.chkSPParametrosPa.UseVisualStyleBackColor = true;
            // 
            // tabMainPage3
            // 
            this.tabMainPage3.BackColor = System.Drawing.Color.Silver;
            this.tabMainPage3.Controls.Add(this.label5);
            this.tabMainPage3.Controls.Add(this.btnSnippet);
            this.tabMainPage3.Controls.Add(this.cmbSnippet);
            this.tabMainPage3.Controls.Add(this.txtPreview);
            this.tabMainPage3.Location = new System.Drawing.Point(4, 22);
            this.tabMainPage3.Name = "tabMainPage3";
            this.tabMainPage3.Size = new System.Drawing.Size(638, 599);
            this.tabMainPage3.TabIndex = 2;
            this.tabMainPage3.Text = "Resultado";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Code Snipets:";
            // 
            // btnSnippet
            // 
            this.btnSnippet.Location = new System.Drawing.Point(527, 3);
            this.btnSnippet.Name = "btnSnippet";
            this.btnSnippet.Size = new System.Drawing.Size(108, 33);
            this.btnSnippet.TabIndex = 2;
            this.btnSnippet.Text = "Ver Snippet";
            this.btnSnippet.UseVisualStyleBackColor = true;
            this.btnSnippet.Click += new System.EventHandler(this.btnSnippet_Click);
            // 
            // cmbSnippet
            // 
            this.cmbSnippet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSnippet.FormattingEnabled = true;
            this.cmbSnippet.Location = new System.Drawing.Point(86, 10);
            this.cmbSnippet.Name = "cmbSnippet";
            this.cmbSnippet.Size = new System.Drawing.Size(435, 21);
            this.cmbSnippet.TabIndex = 1;
            // 
            // txtPreview
            // 
            this.txtPreview.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.txtPreview.AutoScrollMinSize = new System.Drawing.Size(2, 14);
            this.txtPreview.BackBrush = null;
            this.txtPreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPreview.CharHeight = 14;
            this.txtPreview.CharWidth = 8;
            this.txtPreview.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtPreview.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPreview.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtPreview.Font = new System.Drawing.Font("Courier New", 9.75F);
            this.txtPreview.HighlightingRangeType = FastColoredTextBoxNS.HighlightingRangeType.AllTextRange;
            this.txtPreview.IsReplaceMode = false;
            this.txtPreview.Location = new System.Drawing.Point(0, 42);
            this.txtPreview.Name = "txtPreview";
            this.txtPreview.Paddings = new System.Windows.Forms.Padding(0);
            this.txtPreview.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.txtPreview.ServiceColors = ((FastColoredTextBoxNS.ServiceColors)(resources.GetObject("txtPreview.ServiceColors")));
            this.txtPreview.Size = new System.Drawing.Size(638, 557);
            this.txtPreview.TabIndex = 3;
            this.txtPreview.Zoom = 100;
            // 
            // cmbPlantilla
            // 
            this.cmbPlantilla.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbPlantilla.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPlantilla.FormattingEnabled = true;
            this.cmbPlantilla.Items.AddRange(new object[] {
            "-- D E F A U L T --",
            "AP",
            "APS Oracle",
            "ReAl Sistema",
            "Segelic Web Class",
            "Segip Asistencia",
            "Segip Extranjeria",
            "Segip Extranjeria Catalogo",
            "Integrate",
            "Integrate Vb.Net",
            "Demo Vb.Net",
            "GestionMedica",
            "ANDROID GestionMedica",
            "Sistema Analisis Financiero",
            "Integrate.Correspondencia",
            "ANDROID Generador Scripts",
            "Siesis",
            "ANDROID INE",
            "DesktopApp",
            "Python",
            "GEVERO",
            "Erp"});
            this.cmbPlantilla.Location = new System.Drawing.Point(474, 24);
            this.cmbPlantilla.Name = "cmbPlantilla";
            this.cmbPlantilla.Size = new System.Drawing.Size(200, 21);
            this.cmbPlantilla.TabIndex = 4;
            this.cmbPlantilla.SelectedIndexChanged += new System.EventHandler(this.cmbPlantilla_SelectedIndexChanged);
            // 
            // Label25
            // 
            this.Label25.AutoSize = true;
            this.Label25.BackColor = System.Drawing.Color.Transparent;
            this.Label25.Location = new System.Drawing.Point(361, 27);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(107, 13);
            this.Label25.TabIndex = 3;
            this.Label25.Text = "PLANTILLA PERFIL:";
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Location = new System.Drawing.Point(2, 48);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(32, 13);
            this.Label26.TabIndex = 5;
            this.Label26.Text = "Filtro:";
            // 
            // txtListadoBdFiltro
            // 
            this.txtListadoBdFiltro.Location = new System.Drawing.Point(40, 45);
            this.txtListadoBdFiltro.Name = "txtListadoBdFiltro";
            this.txtListadoBdFiltro.Size = new System.Drawing.Size(304, 20);
            this.txtListadoBdFiltro.TabIndex = 6;
            this.txtListadoBdFiltro.TextChanged += new System.EventHandler(this.txtListadoBdFiltro_TextChanged);
            // 
            // btnActualizar
            // 
            this.btnActualizar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnActualizar.Location = new System.Drawing.Point(199, 22);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(145, 23);
            this.btnActualizar.TabIndex = 2;
            this.btnActualizar.Text = "Actualizar";
            this.btnActualizar.UseVisualStyleBackColor = true;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // tabBDObjects
            // 
            this.tabBDObjects.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabBDObjects.Controls.Add(this.tabPageTables);
            this.tabBDObjects.Controls.Add(this.tabPageViews);
            this.tabBDObjects.Controls.Add(this.tabPageSPs);
            this.tabBDObjects.Controls.Add(this.TabPageTriggers);
            this.tabBDObjects.Location = new System.Drawing.Point(4, 66);
            this.tabBDObjects.Name = "tabBDObjects";
            this.tabBDObjects.SelectedIndex = 0;
            this.tabBDObjects.Size = new System.Drawing.Size(344, 590);
            this.tabBDObjects.TabIndex = 7;
            // 
            // tabPageTables
            // 
            this.tabPageTables.Controls.Add(this.ReAlListView1);
            this.tabPageTables.Location = new System.Drawing.Point(4, 22);
            this.tabPageTables.Name = "tabPageTables";
            this.tabPageTables.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTables.Size = new System.Drawing.Size(336, 564);
            this.tabPageTables.TabIndex = 3;
            this.tabPageTables.Text = "Tablas";
            this.tabPageTables.UseVisualStyleBackColor = true;
            // 
            // ReAlListView1
            // 
            this.ReAlListView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReAlListView1.CheckBoxes = true;
            this.ReAlListView1.HideSelection = false;
            this.ReAlListView1.Location = new System.Drawing.Point(6, 3);
            this.ReAlListView1.Name = "ReAlListView1";
            this.ReAlListView1.Size = new System.Drawing.Size(324, 555);
            this.ReAlListView1.TabIndex = 0;
            this.ReAlListView1.UseCompatibleStateImageBehavior = false;
            this.ReAlListView1.ViewColumn0 = false;
            // 
            // tabPageViews
            // 
            this.tabPageViews.Controls.Add(this.ReAlListView2);
            this.tabPageViews.Location = new System.Drawing.Point(4, 22);
            this.tabPageViews.Name = "tabPageViews";
            this.tabPageViews.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageViews.Size = new System.Drawing.Size(336, 564);
            this.tabPageViews.TabIndex = 2;
            this.tabPageViews.Text = "Vistas";
            this.tabPageViews.UseVisualStyleBackColor = true;
            // 
            // ReAlListView2
            // 
            this.ReAlListView2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReAlListView2.CheckBoxes = true;
            this.ReAlListView2.HideSelection = false;
            this.ReAlListView2.Location = new System.Drawing.Point(6, 3);
            this.ReAlListView2.Name = "ReAlListView2";
            this.ReAlListView2.Size = new System.Drawing.Size(324, 539);
            this.ReAlListView2.TabIndex = 0;
            this.ReAlListView2.UseCompatibleStateImageBehavior = false;
            this.ReAlListView2.ViewColumn0 = false;
            // 
            // tabPageSPs
            // 
            this.tabPageSPs.Controls.Add(this.ReAlListView3);
            this.tabPageSPs.Location = new System.Drawing.Point(4, 22);
            this.tabPageSPs.Name = "tabPageSPs";
            this.tabPageSPs.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSPs.Size = new System.Drawing.Size(336, 564);
            this.tabPageSPs.TabIndex = 1;
            this.tabPageSPs.Text = "Procedimientos Almacenados";
            this.tabPageSPs.UseVisualStyleBackColor = true;
            // 
            // ReAlListView3
            // 
            this.ReAlListView3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReAlListView3.CheckBoxes = true;
            this.ReAlListView3.HideSelection = false;
            this.ReAlListView3.Location = new System.Drawing.Point(6, 3);
            this.ReAlListView3.Name = "ReAlListView3";
            this.ReAlListView3.Size = new System.Drawing.Size(324, 542);
            this.ReAlListView3.TabIndex = 0;
            this.ReAlListView3.UseCompatibleStateImageBehavior = false;
            this.ReAlListView3.ViewColumn0 = false;
            // 
            // TabPageTriggers
            // 
            this.TabPageTriggers.Controls.Add(this.ReAlListView4);
            this.TabPageTriggers.Location = new System.Drawing.Point(4, 22);
            this.TabPageTriggers.Name = "TabPageTriggers";
            this.TabPageTriggers.Size = new System.Drawing.Size(336, 564);
            this.TabPageTriggers.TabIndex = 0;
            this.TabPageTriggers.Text = "Triggers";
            this.TabPageTriggers.UseVisualStyleBackColor = true;
            // 
            // ReAlListView4
            // 
            this.ReAlListView4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReAlListView4.CheckBoxes = true;
            this.ReAlListView4.HideSelection = false;
            this.ReAlListView4.Location = new System.Drawing.Point(6, 3);
            this.ReAlListView4.Name = "ReAlListView4";
            this.ReAlListView4.Size = new System.Drawing.Size(324, 542);
            this.ReAlListView4.TabIndex = 0;
            this.ReAlListView4.UseCompatibleStateImageBehavior = false;
            this.ReAlListView4.ViewColumn0 = false;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(1, 27);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(77, 13);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "Base de Datos";
            // 
            // chkSeleccionarTodo
            // 
            this.chkSeleccionarTodo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkSeleccionarTodo.AutoSize = true;
            this.chkSeleccionarTodo.Location = new System.Drawing.Point(8, 660);
            this.chkSeleccionarTodo.Name = "chkSeleccionarTodo";
            this.chkSeleccionarTodo.Size = new System.Drawing.Size(110, 17);
            this.chkSeleccionarTodo.TabIndex = 8;
            this.chkSeleccionarTodo.Text = "Seleccionar Todo";
            this.chkSeleccionarTodo.UseVisualStyleBackColor = true;
            this.chkSeleccionarTodo.CheckedChanged += new System.EventHandler(this.chkSeleccionarTodo_CheckedChanged);
            // 
            // statusStrip2
            // 
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip2.Location = new System.Drawing.Point(0, 679);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip2.TabIndex = 10;
            this.statusStrip2.Text = "statusStrip2";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel2.Text = "toolStripStatusLabel2";
            // 
            // FPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1008, 701);
            this.Controls.Add(this.statusStrip2);
            this.Controls.Add(this.cmbPlantilla);
            this.Controls.Add(this.chkSeleccionarTodo);
            this.Controls.Add(this.Label25);
            this.Controls.Add(this.Label26);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnActualizar);
            this.Controls.Add(this.txtListadoBdFiltro);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.tabBDObjects);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ReAl Generator App";
            this.Load += new System.EventHandler(this.FPrincipal_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabMainPage1.ResumeLayout(false);
            this.tabMainPage1.PerformLayout();
            this.grpParametrosAdicionales.ResumeLayout(false);
            this.grpParametrosAdicionales.PerformLayout();
            this.GroupBox5.ResumeLayout(false);
            this.GroupBox5.PerformLayout();
            this.GroupBox7.ResumeLayout(false);
            this.grpPathDel.ResumeLayout(false);
            this.grpPathDel.PerformLayout();
            this.GroupBox23.ResumeLayout(false);
            this.GroupBox23.PerformLayout();
            this.GroupBox19.ResumeLayout(false);
            this.grpGroupBox8.ResumeLayout(false);
            this.grpGroupBox8.PerformLayout();
            this.tabMainPage2.ResumeLayout(false);
            this.groupBoxDiccionarioDeDatos.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpColumnasTriggers.ResumeLayout(false);
            this.GroupBox18.ResumeLayout(false);
            this.GroupBox18.PerformLayout();
            this.GroupBox17.ResumeLayout(false);
            this.GroupBox17.PerformLayout();
            this.GroupBox16.ResumeLayout(false);
            this.GroupBox16.PerformLayout();
            this.GroupBox4.ResumeLayout(false);
            this.GroupBox4.PerformLayout();
            this.grpSpIns.ResumeLayout(false);
            this.grpSpIns.PerformLayout();
            this.grpSpUpd.ResumeLayout(false);
            this.grpSpUpd.PerformLayout();
            this.GroupBox3.ResumeLayout(false);
            this.GroupBox3.PerformLayout();
            this.tabMainPage3.ResumeLayout(false);
            this.tabMainPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPreview)).EndInit();
            this.tabBDObjects.ResumeLayout(false);
            this.tabPageTables.ResumeLayout(false);
            this.tabPageViews.ResumeLayout(false);
            this.tabPageSPs.ResumeLayout(false);
            this.TabPageTriggers.ResumeLayout(false);
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sistemaToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabMainPage1;
        private System.Windows.Forms.TabPage tabMainPage2;
        private System.Windows.Forms.TabPage tabMainPage3;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.TextBox txtListadoBdFiltro;
        internal System.Windows.Forms.Button btnActualizar;
        internal System.Windows.Forms.TabControl tabBDObjects;
        internal System.Windows.Forms.TabPage tabPageTables;
        internal ReAlFind_Control.ReAlListView ReAlListView1;
        internal System.Windows.Forms.TabPage tabPageViews;
        internal ReAlFind_Control.ReAlListView ReAlListView2;
        internal System.Windows.Forms.TabPage tabPageSPs;
        internal ReAlFind_Control.ReAlListView ReAlListView3;
        internal System.Windows.Forms.TabPage TabPageTriggers;
        internal ReAlFind_Control.ReAlListView ReAlListView4;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.CheckBox chkSeleccionarTodo;
        internal System.Windows.Forms.Button btnGralAbrirCarpeta;
        internal System.Windows.Forms.GroupBox GroupBox7;
        internal System.Windows.Forms.ComboBox cmbTipoColumnas;
        internal System.Windows.Forms.GroupBox grpPathDel;
        internal System.Windows.Forms.TextBox txtPathProyecto;
        internal System.Windows.Forms.CheckBox chkMoverResultados;
        internal System.Windows.Forms.Button btnPathProyecto;
        internal System.Windows.Forms.GroupBox GroupBox23;
        internal System.Windows.Forms.TextBox txtInfAutor;
        internal System.Windows.Forms.GroupBox GroupBox19;
        public System.Windows.Forms.ComboBox cmbAPIs;
        internal System.Windows.Forms.GroupBox grpGroupBox8;
        internal System.Windows.Forms.Label Label46;
        public System.Windows.Forms.TextBox txtInfProyectoConn;
        internal System.Windows.Forms.Label Label28;
        public System.Windows.Forms.TextBox txtInfProyectoUnitTest;
        internal System.Windows.Forms.Label Label12;
        public System.Windows.Forms.TextBox txtInfProyectoDal;
        internal System.Windows.Forms.TextBox txtInfProyecto;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox txtInfProyectoClass;
        internal System.Windows.Forms.Label Label6;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        internal System.Windows.Forms.GroupBox grpParametrosAdicionales;
        public System.Windows.Forms.CheckBox chkClassParcial;
        public System.Windows.Forms.CheckBox chkEntBaseClass;
        public System.Windows.Forms.CheckBox chkPropertyEvents;
        public System.Windows.Forms.CheckBox chkEntidadesEncriptacion;
        public System.Windows.Forms.CheckBox chkDataAnnotations;
        internal System.Windows.Forms.CheckBox chkUsarSpsIdentity;
        internal System.Windows.Forms.CheckBox chkUsarSpsCheck;
        internal System.Windows.Forms.TextBox txtClaseParametros;
        internal System.Windows.Forms.TextBox txtClaseParametrosListadoSp;
        internal System.Windows.Forms.TextBox txtClaseParametrosBaseClass;
        internal System.Windows.Forms.TextBox txtClaseParametrosClaseApi;
        public System.Windows.Forms.CheckBox chkParametroPa;
        internal System.Windows.Forms.CheckBox chkUsarSpsSelect;
        internal System.Windows.Forms.CheckBox chkUsarSps;
        internal System.Windows.Forms.CheckBox chkUsarSchemaInModelo;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label101;
        internal System.Windows.Forms.Label Label102;
        internal System.Windows.Forms.Label Label103;
        internal System.Windows.Forms.TextBox txtClaseTransaccion;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox txtClaseConn;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.GroupBox GroupBox5;
        internal System.Windows.Forms.TextBox txtNamespaceDocumentacion;
        internal System.Windows.Forms.CheckBox chkReAlCrearDoc;
        internal System.Windows.Forms.CheckBox chkReAlDataSet;
        internal System.Windows.Forms.TextBox txtNombreDataSet;
        internal System.Windows.Forms.TextBox TextBox2;
        internal System.Windows.Forms.TextBox txtPrefijoUnitTest;
        internal System.Windows.Forms.Label Label27;
        internal System.Windows.Forms.TextBox txtNamespaceUnitTest;
        internal System.Windows.Forms.CheckBox chkReAlUnitTest;
        internal System.Windows.Forms.TextBox textbox10;
        internal System.Windows.Forms.TextBox txtPrefijoEntidades;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.TextBox TextBox6;
        internal System.Windows.Forms.TextBox txtPrefijoInterface;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.TextBox TextBox3;
        internal System.Windows.Forms.TextBox txtPrefijoModelo;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.TextBox txtNamespaceNegocios;
        internal System.Windows.Forms.TextBox txtNamespaceInterface;
        internal System.Windows.Forms.TextBox txtNamespaceEntidades;
        internal System.Windows.Forms.CheckBox chkReAlReglaNegocios;
        internal System.Windows.Forms.CheckBox chkReAlInterface;
        internal System.Windows.Forms.CheckBox chkReAlEntidades;
        internal System.Windows.Forms.ComboBox cmbEntorno;
        internal System.Windows.Forms.Button btnCLA;
        internal FastColoredTextBoxNS.FastColoredTextBox txtPreview;
        internal System.Windows.Forms.GroupBox grpColumnasTriggers;
        internal System.Windows.Forms.GroupBox GroupBox18;
        internal System.Windows.Forms.CheckBox chkBDOpcionesTriggersInstead;
        internal System.Windows.Forms.CheckBox chkBDOpcionesTriggers;
        internal System.Windows.Forms.CheckBox chkBDOpcionesTriggersSimple;
        internal System.Windows.Forms.CheckBox chkBDOpcionesCols;
        internal System.Windows.Forms.GroupBox GroupBox17;
        internal System.Windows.Forms.CheckBox chkTriggerEncrypt;
        internal System.Windows.Forms.CheckBox chkTriggerInsert;
        internal System.Windows.Forms.CheckBox chkTriggerDelete;
        internal System.Windows.Forms.CheckBox chkTriggerUpdate;
        internal System.Windows.Forms.GroupBox GroupBox16;
        internal System.Windows.Forms.CheckBox chkBdColEstado;
        internal System.Windows.Forms.CheckBox chkBdColUsuCre;
        internal System.Windows.Forms.CheckBox chkBdColFecMod;
        internal System.Windows.Forms.CheckBox chkBdColUsuMod;
        internal System.Windows.Forms.CheckBox chkBdColTrans;
        internal System.Windows.Forms.CheckBox chkBdColFecCre;
        internal System.Windows.Forms.Button btnBDCrearTriggers;
        internal System.Windows.Forms.GroupBox GroupBox4;
        internal System.Windows.Forms.RadioButton rdbSpIfExists;
        internal System.Windows.Forms.RadioButton rdbSpAlter;
        internal System.Windows.Forms.RadioButton rdbSpCreate;
        internal System.Windows.Forms.GroupBox grpSpIns;
        internal System.Windows.Forms.CheckBox chkSPInsIdentity;
        internal System.Windows.Forms.CheckBox chkSPInsFecCreGetDate;
        internal System.Windows.Forms.CheckBox chkSPInsApiEstado;
        internal System.Windows.Forms.CheckBox chkSPInsFecCre;
        internal System.Windows.Forms.CheckBox chkSPInsUsuCre;
        internal System.Windows.Forms.GroupBox grpSpUpd;
        internal System.Windows.Forms.CheckBox chkSPUpdFecModGetDate;
        internal System.Windows.Forms.CheckBox chkSPUpdFecMod;
        internal System.Windows.Forms.CheckBox chkSPUpdUsuMod;
        internal System.Windows.Forms.Button btnSP;
        internal System.Windows.Forms.TextBox txtSPsig;
        internal System.Windows.Forms.TextBox TextBox4;
        internal System.Windows.Forms.TextBox txtSPprev;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.GroupBox GroupBox3;
        internal System.Windows.Forms.CheckBox chkSPInsUpd;
        internal System.Windows.Forms.CheckBox chkSPsel;
        internal System.Windows.Forms.CheckBox chkSPdel;
        internal System.Windows.Forms.CheckBox chkSPupd;
        internal System.Windows.Forms.CheckBox chkSPins;
        public System.Windows.Forms.CheckBox chkSPParametrosPa;
        internal System.Windows.Forms.ComboBox cmbPlantilla;
        internal System.Windows.Forms.Label Label25;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem configurarConexionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem plantillasToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem copiarTemplatesAVisualStudioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vS2010ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vS2012ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vS2013ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vS2015ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vS2017ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBDExportarExcel;
        private System.Windows.Forms.TextBox txtBDExportarExcel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSnippet;
        private System.Windows.Forms.ComboBox cmbSnippet;
        private System.Windows.Forms.ToolStripMenuItem vS2019ToolStripMenuItem;
        private System.Windows.Forms.Label label7;
        internal System.Windows.Forms.TextBox txtClaseParametrosClaseApiObject;
        private System.Windows.Forms.Button btnBdCrearCicloDeVida;
        private System.Windows.Forms.GroupBox groupBoxDiccionarioDeDatos;
        private System.Windows.Forms.Button btnBdDocumentarTodaLaBaseDeDatos;
    }
}

