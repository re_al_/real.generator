﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReAl.Conn
{
    static public class cParametros
    {
        //PRODUCCION 
        public static string bdFile = "ReAlProjects.db";
        public static string schema = "";

        //Otros parametros
        public static string parFormatoFechaHora = "dd/MM/yyyy HH:mm:ss.ffffff";
        public static string parFormatoFecha = "dd/MM/yyyy";
    }
}
