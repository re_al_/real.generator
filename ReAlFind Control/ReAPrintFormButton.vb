﻿Public Class ReAPrintFormButton
    Inherits Button
    Dim fp As FormPrinting.FormPrinting

    Public RealPrintPreview As Boolean = True
    Public ReAlPrintHeader As Boolean = True
    Public ReAlPrintTextBoxBoxes As Boolean = False
    Public ReAlPrintLabelBold As Boolean = False
    Public ReAlPageNumbering As Boolean = False

    Private Sub ReAPrintFormButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Click
        ReAlImprimir(Me.Parent)
    End Sub

    Public Sub ReAlImprimir(ByVal Control As Control)
        CreateFormPrintingObject(Control)
        SetProperties()

        Dim titulo As String = Control.Text
        If ReAlPrintHeader = False Then
            Control.Text = ""
        End If
        ' Print!
        fp.Print()
        If ReAlPrintHeader = False Then
            Control.Text = titulo
        End If
    End Sub

    Private Sub CreateFormPrintingObject(ByVal c As System.Windows.Forms.Control)
        fp = New FormPrinting.FormPrinting(c)
        'FlexGrid (Need a reference to C1.FlexGrid) 
        'Dim fgp As FlexGridPrinting.FlexGridPrinting
        'fp.AddDelegateToPrintControl("FlexGrid", AddressOf fgp.PrintIt)
    End Sub

    Private Sub SetProperties()

        ' Set printing options
        fp.TextBoxBoxed = RealPrintTextBoxBoxes
        fp.TabControlBoxed = True
        fp.LabelInBold = ReAlPrintLabelBold
        fp.PrintPreview = RealPrintPreview
        fp.DisabledControlsInGray = True
        fp.PageNumbering = ReAlPageNumbering


        fp.Orientation = FormPrinting.FormPrinting.OrientationENum.Automatic
        'If Me.RadioButtonPortrait.Checked Then
        '    fp.Orientation = FormPrinting.FormPrinting.OrientationENum.Portrait
        'ElseIf Me.RadioButtonLandscape.Checked Then
        '    fp.Orientation = FormPrinting.FormPrinting.OrientationENum.Lanscape
        'End If


        'If Me.CheckBoxMyOwn.Checked Then
        '    fp.DelegatePrintingReportTitle = AddressOf MrOwnPrintReportTitle
        'Else
        '    fp.DelegatePrintingReportTitle = Nothing
        'End If
    End Sub

End Class
