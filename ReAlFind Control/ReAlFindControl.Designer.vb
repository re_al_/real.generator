<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class ReAlFindControl
    Inherits System.Windows.Forms.UserControl

    'UserControl1 reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtBuscar = New System.Windows.Forms.TextBox
        Me.dtgBuscar = New System.Windows.Forms.DataGridView
        CType(Me.dtgBuscar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtBuscar
        '
        Me.txtBuscar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBuscar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBuscar.Location = New System.Drawing.Point(0, 0)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(255, 20)
        Me.txtBuscar.TabIndex = 0
        '
        'dtgBuscar
        '
        Me.dtgBuscar.AllowUserToAddRows = False
        Me.dtgBuscar.AllowUserToDeleteRows = False
        Me.dtgBuscar.AllowUserToResizeRows = False
        Me.dtgBuscar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtgBuscar.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dtgBuscar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgBuscar.ColumnHeadersVisible = False
        Me.dtgBuscar.Location = New System.Drawing.Point(0, 20)
        Me.dtgBuscar.MultiSelect = False
        Me.dtgBuscar.Name = "dtgBuscar"
        Me.dtgBuscar.ReadOnly = True
        Me.dtgBuscar.RowHeadersVisible = False
        Me.dtgBuscar.RowTemplate.Height = 18
        Me.dtgBuscar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgBuscar.Size = New System.Drawing.Size(255, 179)
        Me.dtgBuscar.TabIndex = 1
        Me.dtgBuscar.TabStop = False
        Me.dtgBuscar.Visible = False
        '
        'ReAlFindControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.dtgBuscar)
        Me.Controls.Add(Me.txtBuscar)
        Me.Name = "ReAlFindControl"
        Me.Size = New System.Drawing.Size(255, 20)
        CType(Me.dtgBuscar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents txtBuscar As System.Windows.Forms.TextBox
    Private WithEvents dtgBuscar As System.Windows.Forms.DataGridView


End Class
