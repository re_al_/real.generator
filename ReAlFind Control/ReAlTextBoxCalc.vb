Public Class ReAlTextBoxCalc
    Inherits TextBox
    Public Function Evaluar() As Double

        Dim nIndex As Integer, nValue As Double
        Try
            If Me.Text <> "" Then
                nIndex = 0
                nValue = EvalAddExpr(Me.Text, nIndex, 0)
                Evaluar = nValue.ToString
            Else
                Evaluar = 0
            End If
        Catch ex As Exception

            ' Print error message
            MsgBox("Error: " + ex.Message)

        End Try

    End Function
    Public Overrides Function PreProcessMessage(ByRef msg As System.Windows.Forms.Message) As Boolean
        If msg.Msg = &H102 Then
            If msg.WParam.Equals(New IntPtr(&H2E)) AndAlso msg.LParam.Equals(New IntPtr(&H530001)) Then
                ' Point in the numeric keypad pressed
                If System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = "," Then
                    ' And the decimal separator is a ',' for the current settings
                    ' Change the message to enter a comma instead of a poin
                    msg.WParam = New IntPtr(&H2C)
                    msg.LParam = New IntPtr(&H330001)
                End If
            End If
        End If

        ' Do the default actions
        Return MyBase.PreProcessMessage(msg)
    End Function

    Function EvalAddExpr(ByVal szExpr As String, ByRef nIndex As Integer, ByVal nDepth As Integer) As Double

        ' Evaluate first operand
        Dim nOperand1 As Double
        nOperand1 = EvalMulExpr(szExpr, nIndex, nDepth)
        While nIndex < szExpr.Length
            If Not Char.IsWhiteSpace(szExpr, nIndex) Then Exit While
            nIndex = nIndex + 1
        End While

        ' Check for end of this expression
        If nIndex >= szExpr.Length Then Return nOperand1
        If (nDepth > 0 And szExpr.Chars(nIndex) = ")"c) Then Return nOperand1

        ' Determine operator
        Dim nOperator As Char
        nOperator = szExpr.Chars(nIndex)
        If nOperator = "+"c Or nOperator = "-"c Then

            ' Evaluate second operand
            Dim nOperand2 As Double
            nIndex = nIndex + 1
            nOperand2 = EvalAddExpr(szExpr, nIndex, nDepth)

            ' Perform calculations
            If nOperator = "-"c Then Return (nOperand1 - nOperand2)
            Return (nOperand1 + nOperand2)

        End If

        ' Throw exception
        Throw New Exception("Unexpected character encountered in expression")

    End Function

    ' Evaluates multiplicative expression
    Function EvalMulExpr(ByVal szExpr As String, ByRef nIndex As Integer, ByVal nDepth As Integer) As Double

        ' Evaluate first operand
        Dim nOperand1 As Double
        nOperand1 = EvalNumExpr(szExpr, nIndex, nDepth)
        While nIndex < szExpr.Length
            If Not Char.IsWhiteSpace(szExpr, nIndex) Then Exit While
            nIndex = nIndex + 1
        End While

        ' Check for end of this expression
        If nIndex >= szExpr.Length Then Return nOperand1
        If (nDepth > 0 And szExpr.Chars(nIndex) = ")"c) Or szExpr.Chars(nIndex) = "+"c Or szExpr.Chars(nIndex) = "-"c Then
            Return nOperand1
        End If

        ' Determine operator
        Dim nOperator As Char
        nOperator = szExpr.Chars(nIndex)
        If nOperator = "*"c Or nOperator = "/"c Or nOperator = "%"c Then

            ' Evaluate second operand
            Dim nOperand2 As Double
            nIndex = nIndex + 1
            nOperand2 = EvalMulExpr(szExpr, nIndex, nDepth)

            ' Perform calculations
            If nOperator = "*"c Then Return (nOperand1 * nOperand2)

            ' Catch divide by zero
            If nOperand2 = 0.0 Then Throw New DivideByZeroException("Integer division by zero")
            If nOperator = "%"c Then Return (nOperand1 Mod nOperand2)
            Return (nOperand1 / nOperand2)

        End If

        ' Throw exception
        Throw New Exception("Unexpected character encountered in expression")

    End Function

    ' Evaluates numeric expression
    Function EvalNumExpr(ByVal szExpr As String, ByRef nIndex As Integer, ByVal nDepth As Integer) As Double

        ' Trim leading whitespace
        While nIndex < szExpr.Length
            If Not Char.IsWhiteSpace(szExpr, nIndex) Then Exit While
            nIndex = nIndex + 1
        End While

        ' Check for end of expression
        If nIndex >= szExpr.Length Then
            Throw New Exception("Unexpected end of expression encountered")
        End If

        ' Handle unary-minus operator
        If szExpr.Chars(nIndex) = "-"c Then
            nIndex = nIndex + 1
            Return -EvalNumExpr(szExpr, nIndex, nDepth)
        End If

        ' Handle parentheses
        If (szExpr.Chars(nIndex) = "("c) Then

            ' Evaluate inner expression
            Dim nValue As Double
            nIndex = nIndex + 1
            nValue = EvalAddExpr(szExpr, nIndex, nDepth + 1)
            If nIndex >= szExpr.Length Then Throw New Exception("Expected closing parentheses not found")
            If szExpr.Chars(nIndex) <> ")"c Then Throw New Exception("Expected closing parentheses not found")

            ' Return computed result
            nIndex = nIndex + 1
            Return nValue

        End If

        ' Handle numbers
        If Char.IsDigit(szExpr, nIndex) Then

            Dim nStart As Integer, szValue As String
            nStart = nIndex
            While nIndex < szExpr.Length
                If Not IsFloatDigit(szExpr, nIndex) Then Exit While
                nIndex = nIndex + 1
            End While
            szValue = szExpr.Substring(nStart, nIndex - nStart)
            Return Double.Parse(szValue)

        End If

        ' Throw exception
        Throw New Exception("Unexpected character encountered in expression")

    End Function

    ' Return true if a floating-point character
    Function IsFloatDigit(ByVal szExpr As String, ByVal nIndex As Integer) As Boolean

        ' Identify floating-point digits
        If Char.IsDigit(szExpr, nIndex) Then Return True
        If nIndex > 0 Then
            If (Char.IsDigit(szExpr, nIndex - 1)) Then
                'Return (szExpr.Chars(nIndex) = "."c Or szExpr.Chars(nIndex) = "e"c Or szExpr.Chars(nIndex) = "E"c)
                Return (szExpr.Chars(nIndex) = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator Or szExpr.Chars(nIndex) = "e"c Or szExpr.Chars(nIndex) = "E"c)
            End If
            If ((szExpr.Chars(nIndex) = "e"c Or szExpr.Chars(nIndex) = "E"c)) Then
                Return (szExpr.Chars(nIndex) = "+"c Or szExpr.Chars(nIndex) = "-"c)
            End If
        End If
        Return False

    End Function

    Private Sub ReAlTextBoxCalc_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
        Me.SelectAll()
    End Sub

    Private Sub TextBoxCalc_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Dim filtro As String

        filtro = "1234567890" & System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator

        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(filtro, KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If


        'If Me.Text(0) = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator Then
        '    Me.Text = "0" & Me.Text
        'End If
    End Sub

    Private Function SoloNumeros(ByVal filtro As String, ByVal Keyascii As Short) As Short
        If InStr(filtro, Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function

End Class
