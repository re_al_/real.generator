﻿Imports Microsoft

Public Class ReAlListView
    Inherits ListView
    Dim dt As New DataTable
    Dim VisibleColumn As Boolean = False
    Dim CodItemSeleccionado = -1

    Public Property DataSource() As DataTable
        Get
            Return dt
        End Get
        Set(ByVal dtUser As DataTable)
            dt = dtUser
            CargarReAlListView(Me)
        End Set
    End Property

    Public Property ViewColumn0() As Boolean
        Get
            Return VisibleColumn
        End Get
        Set(ByVal pVisible As Boolean)
            VisibleColumn = pVisible
        End Set
    End Property

    Public ReadOnly Property SelectedValue() As Integer
        Get
            Return CodItemSeleccionado
        End Get
    End Property

    'Public Sub ExportToExcel(Optional ByVal titulo As String = "")
    '    '' Creamos un objeto Excel
    '    If Me.Items.Count = 0 Then
    '        MsgBox("No puede exportar a Excel si no se tiene registros")
    '    Else
    '        Dim m_Excel As Microsoft.Office.Interop.Excel.Application

    '        If Not m_Excel Is Nothing Then
    '            m_Excel.Quit()
    '            m_Excel = Nothing
    '        End If

    '        ' Variable para controlar la ruptura por nombre de categorías
    '        Dim CategoryName As String

    '        '' Creamos un objeto WorkBook 
    '        Dim objLibroExcel As Microsoft.Office.Interop.Excel.Workbook

    '        '' Creamos un objeto WorkSheet
    '        Dim objHojaExcel As Microsoft.Office.Interop.Excel.Worksheet

    '        '' Iniciamos una instancia a Excel
    '        m_Excel = New Microsoft.Office.Interop.Excel.Application
    '        m_Excel.Visible = True

    '        '' Creamos una instancia del Workbooks de Excel
    '        '' Creamos una instancia de la primera hoja de trabajo de Excel
    '        objLibroExcel = m_Excel.Workbooks.Add()
    '        objHojaExcel = objLibroExcel.Worksheets(1)
    '        objHojaExcel.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetVisible

    '        '' Hacemos esta hoja la visible en pantalla 
    '        '' (como seleccionamos la primera esto no es necesario
    '        '' si seleccionamos una diferente a la primera si lo
    '        '' necesitaríamos).
    '        objHojaExcel.Activate()

    '        '' Crear el encabezado de nuestro informe
    '        'tendiramos que contar los registros del datagrid

    '        objHojaExcel.Range("A1:G1").Merge()
    '        objHojaExcel.Range("A1:G1").Value = titulo
    '        objHojaExcel.Range("A1:G1").Font.Size = 15

    '        Dim objCelda As Microsoft.Office.Interop.Excel.Range
    '        Dim i, j, a, b As Integer
    '        i = 3
    '        j = 1
    '        'Imprimir columnas
    '        For column As Integer = 1 To Me.Columns.Count - 1
    '            objHojaExcel.Cells(i, j) = Me.Columns(column).Text
    '            objHojaExcel.Cells(i, j).Font.Size = 15
    '            j += 1
    '        Next
    '        i += 1
    '        j = 1
    '        'Imprimir registros

    '        For fila As Integer = 0 To Me.Items.Count - 1
    '            For columna As Integer = 1 To Me.Columns.Count - 1
    '                Try
    '                    objHojaExcel.Cells(i, j) = Me.Items(fila).SubItems(columna).Text
    '                Catch ex As Exception
    '                    objHojaExcel.Cells(i, j) = "ERROR"
    '                End Try

    '                j += 1
    '            Next
    '            i += 1
    '            j = 1
    '        Next
    '        'objLibroExcel.PrintPreview()
    '    End If
    'End Sub


    Private Sub CargarReAlListView(ByVal ListView As ListView)
        'CargarDT()
        'Me.OwnerDraw = True

        ListView.Items.Clear()
        Me.Items.Clear()
        ListView.Columns.Clear()
        Me.Columns.Clear()

        'Agregamos las columnas
        For j As Integer = 0 To dt.Columns.Count - 1
            ListView.Columns.Add(dt.Columns(j).ColumnName, -1, HorizontalAlignment.Left)
        Next

        If Me.Columns.Count = 0 Then Exit Sub
        ListView.View = View.Details
        ListView.GridLines = True
        ListView.FullRowSelect = True
        ListView.Sorting = SortOrder.Ascending
        ListView.AllowColumnReorder = True
        ListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.None)
        ListView.MultiSelect = False
        ListView.HideSelection = False

        If Me.VisibleColumn = False Then
            ListView.Columns(0).Width = 0 'oculta la columna 0
        End If

        For i As Integer = 0 To dt.Rows.Count - 1
            'Creo el ListViewItem (item)
            Dim item As ListViewItem

            'Creo una nueva instancia de item pasandole el nombre como dato principal
            item = New ListViewItem(dt.Rows(i).Item(0).ToString)

            For j As Integer = 1 To dt.Columns.Count - 1
                'Agrego los demas datos a los SubItems de item
                item.SubItems.Add(dt.Rows(i).Item(j).ToString)
            Next

            'Agrego el item a la colección de listViewItem’s de ListView1
            ListView.Items.Add(item)

        Next

        'Tipos de dato de las columnas
        For j As Integer = 0 To Me.Columns.Count - 1
            Me.Columns(j).Tag = dt.Columns(j).DataType.ToString
        Next
        'Damos Formato a los items
        For fil As Integer = 0 To Me.Items.Count - 1
            For col As Integer = 0 To Me.Columns.Count - 1
                Select Case Me.Columns(col).Tag
                    Case "System.Decimal"
                        Me.Items(fil).SubItems(col).Text = FormatNumber(Me.Items(fil).SubItems(col).Text)
                    Case "System.DateTime"
                        Me.Items(fil).SubItems(col).Text = FormatDateTime(Me.Items(fil).SubItems(col).Text)
                End Select
            Next
        Next
        'Alineamos las celdas
        For col As Integer = 0 To Me.Columns.Count - 1
            Select Case Me.Columns(col).Tag
                Case "System.Decimal"
                    Me.Columns(col).TextAlign = HorizontalAlignment.Right
                Case "System.Int32"
                    Me.Columns(col).TextAlign = HorizontalAlignment.Right
            End Select
        Next


        Dim ColIni As Integer

        If Me.VisibleColumn = True Then ColIni = 0 Else ColIni = 1

        For m As Integer = ColIni To ListView.Columns.Count - 1 'For each column
            Dim a As Integer = 0
            Dim b As Integer = 0
            ListView.Columns(m).Width = -1
            a = ListView.Columns(m).Width
            ListView.Columns(m).Width = -2
            b = ListView.Columns(m).Width
            If a > b Then
                ListView.Columns(m).Width = -1
            Else
                ListView.Columns(m).Width = -2
            End If
        Next m

        If Me.Items.Count > 0 Then
            Me.Items(0).Selected = True
            Me.CodItemSeleccionado = Me.Items(0).SubItems(0).Text
        End If

        colorearListView(ListView)
    End Sub

    'Private Sub ReAlListView_DrawColumnHeader(ByVal sender As Object, ByVal e As System.Windows.Forms.DrawListViewColumnHeaderEventArgs) Handles Me.DrawColumnHeader
    '    e.DrawDefault = True
    '    e.DrawBackground()
    '    e.DrawText()
    'End Sub

    'Private Sub ReAlListView_DrawSubItem(ByVal sender As Object, ByVal e As System.Windows.Forms.DrawListViewSubItemEventArgs) Handles Me.DrawSubItem
    '    Dim flags As TextFormatFlags
    '    flags = TextFormatFlags.Default
    '    If Me.Columns(e.ColumnIndex).Tag = "System.Decimal" Then
    '        flags = TextFormatFlags.Right
    '    End If
    '    e.DrawText(flags)
    'End Sub

    Public Sub colorearListView(ByRef list As ListView)
        Dim bColumnaEstado As Boolean = False
        Dim iColumnIndex As Integer = -1
        For j As Integer = 0 To list.Columns.Count - 1
            If list.Columns(j).Text = "Tipo" Then
                bColumnaEstado = True
                iColumnIndex = j
            End If
        Next

        'Coloreamos
        If bColumnaEstado = True Then
            For i As Integer = 0 To list.Items.Count - 1
                Select Case list.Items(i).SubItems(iColumnIndex).Text
                    Case "Monster"
                        Select Case list.Items(i).SubItems(iColumnIndex + 1).Text
                            Case "Fusion"
                                'list.Items.Item(i).BackColor = Color.MediumPurple
                                list.Items.Item(i).BackColor = Color.MediumOrchid
                                list.Items.Item(i).ForeColor = Color.White
                            Case "Effect"
                                list.Items.Item(i).BackColor = Color.Tan
                                'list.Items.Item(i).BackColor = Color.Goldenrod
                                list.Items.Item(i).ForeColor = Color.White
                            Case "Tunner"
                                list.Items.Item(i).BackColor = Color.Tan
                                'list.Items.Item(i).BackColor = Color.Goldenrod
                                list.Items.Item(i).ForeColor = Color.White
                            Case "Gemini"
                                list.Items.Item(i).BackColor = Color.Tan
                                'list.Items.Item(i).BackColor = Color.Goldenrod
                                list.Items.Item(i).ForeColor = Color.White
                            Case "Syncro"
                                list.Items.Item(i).BackColor = Color.White
                                list.Items.Item(i).ForeColor = Color.Black
                            Case "Ritual"
                                list.Items.Item(i).BackColor = Color.LightSteelBlue
                            Case Else
                                list.Items.Item(i).BackColor = Color.Khaki
                        End Select

                    Case "Magic"
                        'list.Items.Item(i).BackColor = Color.LimeGreen
                        list.Items.Item(i).BackColor = Color.LightGreen
                    Case "Trap"
                        list.Items.Item(i).BackColor = Color.LightPink

                    Case "Anulado"
                        list.Items.Item(i).BackColor = Color.BurlyWood

                    Case "Programado"
                        list.Items.Item(i).BackColor = Color.LemonChiffon 'LightGoldenrodYellow
                    Case "Elaborado"
                        list.Items.Item(i).BackColor = Color.LemonChiffon

                    Case "Efectivo"
                        list.Items.Item(i).BackColor = Color.PowderBlue
                    Case "Revisado"
                        list.Items.Item(i).BackColor = Color.PowderBlue

                    Case "Supervisado"
                        list.Items.Item(i).BackColor = Color.Ivory
                    Case "Concluido"
                        list.Items.Item(i).BackColor = Color.Ivory

                    Case Else
                        list.Items.Item(i).BackColor = Color.White
                End Select
            Next
        End If
    End Sub

    Private Sub ReAlListView_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles Me.ColumnClick
        Me.ListViewItemSorter = New ListViewItemComparer(e.Column)
        Me.Sort()
    End Sub

    Private Sub ReAlListView_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If Me.Items.Count = 0 Then
            Beep()
        Else
            If (e.KeyCode = Keys.A Or e.KeyCode = Keys.B Or e.KeyCode = Keys.C Or e.KeyCode = Keys.D Or e.KeyCode = Keys.E Or e.KeyCode = Keys.F Or e.KeyCode = Keys.G Or e.KeyCode = Keys.H Or e.KeyCode = Keys.I Or e.KeyCode = Keys.J Or e.KeyCode = Keys.K Or e.KeyCode = Keys.L Or e.KeyCode = Keys.M Or e.KeyCode = Keys.N Or e.KeyCode = Keys.O Or e.KeyCode = Keys.P Or e.KeyCode = Keys.Q Or e.KeyCode = Keys.R Or e.KeyCode = Keys.S Or e.KeyCode = Keys.T Or e.KeyCode = Keys.U Or e.KeyCode = Keys.V Or e.KeyCode = Keys.W Or e.KeyCode = Keys.X Or e.KeyCode = Keys.Y Or e.KeyCode = Keys.Z Or e.KeyCode = Keys.NumPad0 Or e.KeyCode = Keys.NumPad1 Or e.KeyCode = Keys.NumPad2 Or e.KeyCode = Keys.NumPad3 Or e.KeyCode = Keys.NumPad4 Or e.KeyCode = Keys.NumPad5 Or e.KeyCode = Keys.NumPad6 Or e.KeyCode = Keys.NumPad7 Or e.KeyCode = Keys.NumPad8 Or e.KeyCode = Keys.NumPad9 Or e.KeyCode = Keys.D1 Or e.KeyCode = Keys.D2 Or e.KeyCode = Keys.D3 Or e.KeyCode = Keys.D4 Or e.KeyCode = Keys.D5 Or e.KeyCode = Keys.D6 Or e.KeyCode = Keys.D7 Or e.KeyCode = Keys.D8 Or e.KeyCode = Keys.D9 Or e.KeyCode = Keys.D0 Or e.KeyCode = Keys.Multiply) Then


                'Dim Busqueda As String = InputBox("", "Busqueda ", "")
                'FindItem(Busqueda, False)
                Buscar(Chr(e.KeyValue))
            End If

        End If
    End Sub

    Private Sub ReAlListView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SelectedIndexChanged
        Try
            Me.CodItemSeleccionado = Me.SelectedItems(0).SubItems(0).Text
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Buscar(ByVal Letra As String)
        'Ordenamos por la prmera columna
        Dim OrderColumnIndex As Integer = 1
        For i As Integer = 1 To Me.Columns.Count - 1
            If Me.Columns(i).DisplayIndex = 1 Then
                OrderColumnIndex = i
            End If
        Next

        Me.ListViewItemSorter = New ListViewItemComparer(OrderColumnIndex)
        Me.Sort()

        ' Buscar el texto indicado en los elementos del listview
        Dim frm As New frmListBuscar
        frm.Letra = Letra
        frm.ShowDialog(Me)
        'Dim Busqueda As String = InputBox("", "Busqueda ", "")


        If 1 = 1 Then
            Dim Busqueda As String = "" & frm.txtBuscar.Text & "*"
            Dim PrimerItm As Integer = -1
            For itm As Integer = 0 To Me.Items.Count - 1
                For subitm As Integer = 0 To Me.Columns.Count - 1
                    If (Me.Items(itm).SubItems(subitm).Text Like Busqueda) And PrimerItm = -1 Then
                        PrimerItm = itm
                        GoTo encontrado
                    End If
                Next
            Next
encontrado:

            If PrimerItm <> -1 Then

                Me.ListViewItemSorter = New ListViewItemComparer(OrderColumnIndex)
                Me.Sorting = SortOrder.Ascending
                Me.Sort()

                Me.Items(PrimerItm).Selected = True
                Me.Items(PrimerItm).Focused = True

                'Me.TopItem = lv.Items[index_you_need]
                Me.Items(PrimerItm).EnsureVisible()
                Me.Select()
            Else
                Me.Items(0).Selected = True
                Me.Items(0).Focused = True
                'Me.TopItem = lv.Items[index_you_need]
                Me.Items(0).EnsureVisible()
                Me.Select()
            End If
        Else
            Dim Busqueda As String = frm.txtBuscar.Text
            If Busqueda <> "" Then
                ' realizamos la búsqueda
                Dim tItem As New ListViewItem
                tItem = Me.FindItemWithText(Busqueda, True, 0, True)
                '
                ' Si tItem es Nothing, es que no existe...
                If Not tItem Is Nothing Then
                    tItem.Selected = True
                    tItem.Focused = True
                    'Me.TopItem = lv.Items[index_you_need]
                    tItem.EnsureVisible()
                    Me.Select()
                Else

                End If
            End If

        End If
    End Sub
End Class
