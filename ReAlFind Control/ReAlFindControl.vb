Imports System.Data.OleDb
Imports System.Data


Public Class ReAlFindControl
    'Dim sCon3 As String = "User ID=sa;Tag with column collation when possible=False;Data Source=VERA;Pas" & _
    '            "sword=REAL;Initial Catalog=PAT_CONTABILIDAD;Use Procedure for Prepare=1;Auto Transla" & _
    '            "te=True;Persist Security Info=True;Provider=""SQLOLEDB.1"";Workstation ID=(local);" & _
    '            "Use Encryption for Data=False;Packet Size=4096;Connect Timeout=600"
    Private ReAlSQLCnControl As OleDbConnection
    Private ReAlSQLQuery As String '= "select cod_plan_cuenta, cuenta_contable, desc_plan_cuenta from plan_cuenta"
    Private ReAlNumCaracteres As Integer = 2
    Private ReAlNumColumnas As Integer = 2
    Private bSwBuscar As Boolean = True
    Private dv As DataView
    Private dtPrincipal As New DataTable
    Public SelectedValue As String = "0"
    Private CambiarFocoEnter As Boolean = True
    Private MantenerUltimoElegido As Boolean = True
    Private iHeight As Integer = 200
    'Alto del Grid 179
    'Alto del Control sin grid 20
    'Alto del Control con Grid 200
    'Si el grid aumenta un poco, el control debe aumentar lo mismo
    'Es decir se debe hallar la diferencia entre el tama�o actual 
    ' y el nuevo tamao para sumar/restar al grid y al control



    Public Property ReAlFocusWithEnter() As Boolean
        Get
            Return CambiarFocoEnter
        End Get
        Set(ByVal Value As Boolean)
            CambiarFocoEnter = Value
        End Set
    End Property

    Public Property ReAlGridScrollBars() As System.Windows.Forms.ScrollBars
        Get
            Return Me.dtgBuscar.ScrollBars
        End Get
        Set(ByVal Value As System.Windows.Forms.ScrollBars)
            Me.dtgBuscar.ScrollBars = Value
        End Set
    End Property

    Public Property ReAlMantenerUltimoElegido() As Boolean
        Get
            Return MantenerUltimoElegido
        End Get
        Set(ByVal Value As Boolean)
            MantenerUltimoElegido = Value
        End Set
    End Property

    Public Property ReAlGridHeight() As Integer
        Get
            Return Me.dtgBuscar.Height
        End Get
        Set(ByVal Value As Integer)
            Dim Dif As Integer = Value - Me.dtgBuscar.Height
            Me.dtgBuscar.Height = Me.dtgBuscar.Height + Dif
            iHeight = Me.dtgBuscar.Height + 21
        End Set
    End Property

    'Public Property ReAlValue() As String
    '    Get
    '        Return Me.SelectedValue
    '    End Get
    '    Set(ByVal Value As String)
    '        Me.ReAlSelectedValue(Value)
    '    End Set
    'End Property

    Public Sub ReAlCargarControl(ByVal Cn As OleDb.OleDbConnection, ByVal Query As String, Optional ByVal NumColumnas As Integer = 2, Optional ByVal NumCaracteres As Integer = 4)
        Me.ReAlSQLCnControl = Cn
        Me.ReAlSQLQuery = Query
        Me.ReAlNumCaracteres = NumCaracteres
        Me.ReAlNumColumnas = NumColumnas
        Query = "SELECT * SIESIS.* FROM ( " & Query & " ) AS SIESIS"

        Dim da As New OleDb.OleDbDataAdapter
        ReAlSQLCnControl.Open()
        Me.dtPrincipal.Clear()
        da = New OleDb.OleDbDataAdapter(ReAlSQLQuery, ReAlSQLCnControl)
        da.Fill(Me.dtPrincipal)
        Me.dtgBuscar.DataSource = Me.dtPrincipal
        ReAlSQLCnControl.Close()
        Me.dtgBuscar.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        Me.dtgBuscar.Columns(0).Visible = False

        'Validamos el numero de columnas
        Dim NumCol As Integer = Me.dtPrincipal.Columns.Count
        If NumColumnas > NumCol Then
            NumColumnas = NumCol
        End If

    End Sub

    Public Sub ReAlCargarControl(ByVal Cn As OleDb.OleDbConnection, ByVal dt As DataTable, Optional ByVal NumColumnas As Integer = 2, Optional ByVal NumCaracteres As Integer = 4)
        Me.ReAlSQLCnControl = Cn
        Me.ReAlNumCaracteres = NumCaracteres
        Me.ReAlNumColumnas = NumColumnas

        Me.dtPrincipal.Clear()
        Me.dtPrincipal = dt
        Me.dtgBuscar.DataSource = Me.dtPrincipal
        ReAlSQLCnControl.Close()
        Me.dtgBuscar.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        Me.dtgBuscar.Columns(0).Visible = False

        'Validamos el numero de columnas
        Dim NumCol As Integer = Me.dtPrincipal.Columns.Count
        If Me.ReAlNumColumnas > NumCol Then
            Me.ReAlNumColumnas = NumCol
        End If

    End Sub

    Private Sub ReAlCargarControl()
        Me.dtgBuscar.DataSource = Me.dtPrincipal
        Me.dtgBuscar.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        Try
            Me.dtgBuscar.Columns(0).Visible = False
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try

        'Validamos el numero de columnas
        Dim NumCol As Integer = Me.dtPrincipal.Columns.Count
        If Me.ReAlNumColumnas > NumCol Then
            Me.ReAlNumColumnas = NumCol
        End If

    End Sub

    Public Sub ReAlSelectedValue(ByVal Valor As String)
        Me.SelectedValue = Valor

        Me.ReAlCargarControl()

        'Validamos el numero de columnas
        Dim NumCol As Integer = Me.dtPrincipal.Columns.Count
        If Me.ReAlNumColumnas > NumCol Then
            Me.ReAlNumColumnas = NumCol
        End If

        Dim bEsInicio As Boolean = True

        Me.bSwBuscar = False
        If Me.SelectedValue <> "0" Then
            For i As Integer = 0 To Me.dtPrincipal.Rows.Count - 1
                If Me.dtPrincipal.Rows(i).Item(0) = Me.SelectedValue Then
                    For j As Integer = 1 To Me.ReAlNumColumnas
                        If bEsInicio Then
                            Me.txtBuscar.Text = IIf(IsDBNull(Me.dtPrincipal.Rows(i).Item(j)), "", Me.dtPrincipal.Rows(i).Item(j))
                            bEsInicio = False
                        Else
                            Me.txtBuscar.Text = Me.txtBuscar.Text & " - " & IIf(IsDBNull(Me.dtPrincipal.Rows(i).Item(j)), "", Me.dtPrincipal.Rows(i).Item(j))
                        End If
                    Next
                End If
            Next
            'Query = Me.ReAlSQLQuery & " WHERE [" & Me.ColumnaMostrar & "] = " & Me.SelectedValue
            'Me.txtBuscar.Text = Me.ContarRegStr(Query)
        End If

        'Me.bSwBuscar = True

        Me.ReAlVer(False)
    End Sub

    Private Sub ReAlVer(ByVal estado As Boolean)
        If estado = False Then
            Me.Height = 20
            Me.dtgBuscar.Visible = False
        Else
            Me.dtgBuscar.Visible = True
            Me.Height = Me.iHeight
        End If
    End Sub


    Private Sub ReAlFindControl_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Enter
        If Me.txtBuscar.ReadOnly = True Then
            'Me.Height = 20
            'Me.dtgBuscar.Visible = False
            ReAlVer(False)
        Else
            'Me.dtgBuscar.Visible = True
            'Me.Height = Me.iHeight
            ReAlVer(True)
            Me.txtBuscar.SelectAll()
        End If
    End Sub

    Private Sub ReAlFindControl_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave
        'Me.Height = 20
        'Me.dtgBuscar.Visible = False
        ReAlVer(False)

        Dim bEsInicio As Boolean = True

        If Me.txtBuscar.Text = "" Then
            Me.SelectedValue = "0"
        Else
            'Comprobamos el ID
            If Me.SelectedValue <> "0" Then
                Me.bSwBuscar = False
                For i As Integer = 0 To Me.dtPrincipal.Rows.Count - 1
                    If Me.dtPrincipal.Rows(i).Item(0) = Me.SelectedValue Then
                        For j As Integer = 1 To Me.ReAlNumColumnas

                            If bEsInicio Then
                                Me.txtBuscar.Text = IIf(IsDBNull(Me.dtPrincipal.Rows(i).Item(j)), "", Me.dtPrincipal.Rows(i).Item(j))
                                bEsInicio = False
                            Else
                                Me.txtBuscar.Text = Me.txtBuscar.Text & " - " & Me.dtPrincipal.Rows(i).Item(j)
                            End If

                        Next
                    End If
                Next
                Me.bSwBuscar = True
                'Dim Query As String = Me.ReAlSQLQuery & " WHERE [" & Me.ColumnaMostrar & "] = " & Me.SelectedValue
                'Me.txtBuscar.Text = Me.ContarRegStr(Query)
            End If
        End If



    End Sub

    Private Sub txtBuscar_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBuscar.GotFocus
        'Me.Height = 20
        'Me.dtgBuscar.Visible = False
        ReAlVer(False)
    End Sub

    Private Sub txtBuscar_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBuscar.KeyDown
        CambiarIndice(sender, e)
    End Sub

    Private Sub txtBuscar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBuscar.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))

        Select Case KeyAscii
            Case 13 'enter
                If Me.dtgBuscar.RowCount = 0 Then Exit Sub

                Dim bEsInicio As Boolean = True
                Me.SelectedValue = Me.dtgBuscar.Item(0, Me.dtgBuscar.SelectedRows(0).Index).Value

                If Me.SelectedValue <> "0" Then
                    For i As Integer = 0 To Me.dtPrincipal.Rows.Count - 1
                        If Me.dtPrincipal.Rows(i).Item(0) = Me.SelectedValue Then
                            Me.bSwBuscar = False
                            For j As Integer = 1 To Me.ReAlNumColumnas
                                If bEsInicio Then
                                    Me.txtBuscar.Text = IIf(IsDBNull(Me.dtPrincipal.Rows(i).Item(j)), "", Me.dtPrincipal.Rows(i).Item(j))
                                    bEsInicio = False
                                Else
                                    Me.txtBuscar.Text = Me.txtBuscar.Text & " - " & Me.txtBuscar.Text = IIf(IsDBNull(Me.dtPrincipal.Rows(i).Item(j)), "", Me.dtPrincipal.Rows(i).Item(j))
                                End If
                            Next
                            Me.bSwBuscar = True
                        End If
                    Next
                    'Dim Query As String = Me.ReAlSQLQuery & " WHERE [" & Me.ColumnaMostrar & "] = " & Me.SelectedValue
                    'Me.txtBuscar.Text = Me.ContarRegStr(Query)
                    Me.ReAlVer(False)
                End If
                Me.txtBuscar.Focus()
                Me.txtBuscar.SelectAll()

                If CambiarFocoEnter = True Then
                    Me.ParentForm.SelectNextControl(Me, True, True, True, True)
                End If

            Case 9 'tab
                If Me.dtgBuscar.RowCount = 0 Then Exit Sub

                Dim bEsInicio As Boolean = True
                Me.SelectedValue = Me.dtgBuscar.Item(0, Me.dtgBuscar.SelectedRows(0).Index).Value

                If Me.SelectedValue <> "0" Then
                    For i As Integer = 0 To Me.dtPrincipal.Rows.Count - 1
                        If Me.dtPrincipal.Rows(i).Item(0) = Me.SelectedValue Then
                            Me.bSwBuscar = False
                            For j As Integer = 1 To Me.ReAlNumColumnas
                                If bEsInicio Then
                                    Me.txtBuscar.Text = Me.dtPrincipal.Rows(i).Item(j)
                                    bEsInicio = False
                                Else
                                    Me.txtBuscar.Text = Me.txtBuscar.Text & " - " & Me.dtPrincipal.Rows(i).Item(j)
                                End If
                            Next
                            Me.bSwBuscar = True
                        End If
                    Next
                    'Dim Query As String = Me.ReAlSQLQuery & " WHERE [" & Me.ColumnaMostrar & "] = " & Me.SelectedValue
                    'Me.txtBuscar.Text = Me.ContarRegStr(Query)
                End If
                Me.txtBuscar.Focus()
                Me.txtBuscar.SelectAll()

            Case Else
                Me.bSwBuscar = True

        End Select




    End Sub

    Private Sub txtBuscar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBuscar.TextChanged
        Me.Text = Me.txtBuscar.Text
        If Me.bSwBuscar = False Then Exit Sub

        'Ocultamos si no hay texto y si no hay resultados
        If Me.txtBuscar.Text = "" Then
            'Me.dtgBuscar.Visible = False
            'Me.Height = 20
            ReAlVer(False)
        Else
            If Me.dtgBuscar.Rows.Count = 0 Then
                'Me.dtgBuscar.Visible = False
                'Me.Height = 20
                ReAlVer(False)
            Else
                'Me.dtgBuscar.Visible = True
                'Me.Height = Me.iHeight
                ReAlVer(True)
            End If
        End If

        'If Me.dtgBuscar.Visible = False And Me.Height = Me.iHeight Then
        '    Me.dtgBuscar.Visible = True
        'End If


        'DETERMINAMOS EL FILTRO SOBRE EL DATATABLE
        If Me.txtBuscar.Text.Length >= Me.ReAlNumCaracteres Or InStr(Me.txtBuscar.Text, "*") > 0 Then
            If IsNothing(Me.dtgBuscar.DataSource) Then
                Me.ReAlCargarControl()
            End If

            Dim PrimerFiltro As Boolean = True

            Dim Filtro As String = ""

            Me.dv = Nothing
            Me.dv = Me.dtPrincipal.DefaultView

            For i As Integer = 1 To Me.dtPrincipal.Columns.Count - 1
                If PrimerFiltro Then
                    Filtro = Filtro & " CONVERT([" & Me.dtgBuscar.Columns(i).Name & "], System.String) LIKE '%" & Me.txtBuscar.Text & "%'"
                    PrimerFiltro = False
                Else
                    Filtro = Filtro & " OR " & " CONVERT([" & Me.dtgBuscar.Columns(i).Name & "], System.String) LIKE '%" & Me.txtBuscar.Text & "%'"
                End If
            Next

            'Intentamos aplicar el filtro
            Try
                dv.Sort() = Me.dtgBuscar.Columns(1).Name
                dv.RowFilter = Filtro
                Me.dtgBuscar.DataSource = Me.dv
            Catch ex1 As Exception
                Beep()
            End Try


            If MantenerUltimoElegido = False Then
                If Me.dtgBuscar.RowCount = 0 Then
                    Me.SelectedValue = "0"
                End If
            End If

        Else
            'No aplicamos filtro
            Try
                dv.RowFilter = ""
                Me.dtgBuscar.DataSource = Me.dv
            Catch ex As Exception

            End Try
        End If

    End Sub

    Private Sub dtgBuscar_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtgBuscar.CellClick
        Me.SelectedValue = Me.dtgBuscar.Item(0, Me.dtgBuscar.SelectedRows(0).Index).Value

        Dim bEsInicio As Boolean = True
        If Me.SelectedValue <> "0" Then
            For i As Integer = 0 To Me.dtPrincipal.Rows.Count - 1
                If Me.dtPrincipal.Rows(i).Item(0) = Me.SelectedValue Then
                    For j As Integer = 1 To Me.ReAlNumColumnas
                        bSwBuscar = False
                        If bEsInicio Then
                            Me.txtBuscar.Text = IIf(IsDBNull(Me.dtPrincipal.Rows(i).Item(j)), "", Me.dtPrincipal.Rows(i).Item(j))
                            bEsInicio = False
                        Else
                            Me.txtBuscar.Text = Me.txtBuscar.Text & " - " & IIf(IsDBNull(Me.dtPrincipal.Rows(i).Item(j)), "", Me.dtPrincipal.Rows(i).Item(j))
                        End If
                        bSwBuscar = True
                    Next
                End If
            Next
            'Dim Query As String = Me.ReAlSQLQuery & " WHERE [" & Me.ColumnaMostrar & "] = " & Me.SelectedValue
            'Me.txtBuscar.Text = Me.ContarRegStr(Query)
        End If
        Me.txtBuscar.Focus()
        Me.txtBuscar.SelectAll()
    End Sub

    Private Sub CambiarIndice(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Try
            Dim IndiceActual As Integer

            If e.KeyCode = Keys.Down Then
                Try
                    IndiceActual = Me.dtgBuscar.SelectedRows(0).Index
                    Me.dtgBuscar.SelectedRows(0).Selected = False
                    Me.dtgBuscar.Rows(IndiceActual + 1).Selected = True
                Catch ex As Exception
                    Me.dtgBuscar.Rows(0).Selected = True
                End Try
                e.Handled = True
            End If

            If e.KeyCode = Keys.Up Then
                Try
                    IndiceActual = Me.dtgBuscar.SelectedRows(0).Index
                    Me.dtgBuscar.SelectedRows(0).Selected = False
                    Me.dtgBuscar.Rows(IndiceActual - 1).Selected = True
                Catch ex As Exception
                    Me.dtgBuscar.Rows(Me.dtgBuscar.RowCount - 1).Selected = True
                End Try
                e.Handled = True
            End If

            Try
                If Me.dtgBuscar.SelectedRows(0).Displayed = False Then
                    Me.dtgBuscar.FirstDisplayedScrollingRowIndex = Me.dtgBuscar.SelectedRows(0).Index
                End If
            Catch ex As Exception

            End Try
        Catch ex As Exception
            Beep()
            If e.KeyCode = Keys.Up Or e.KeyCode = Keys.Down Then
                e.Handled = True
            End If
        End Try

    End Sub

    Private Sub ReAlFindControl_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        Me.txtBuscar.Width = Me.Width
        Me.dtgBuscar.Width = Me.Width
    End Sub

    Private Sub dtgBuscar_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtgBuscar.SelectionChanged
        Try
            If Me.bSwBuscar = True Then
                Me.SelectedValue = Me.dtgBuscar.Item(0, Me.dtgBuscar.SelectedRows(0).Index).Value
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.ReAlVer(False)


    End Sub
End Class
