﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos atributos para modificar la información
' asociada con un ensamblado.

' Revisar los valores de los atributos del ensamblado

<Assembly: AssemblyTitle("ReAlFind Control")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("VERA")> 
<Assembly: AssemblyProduct("ReAlFind Control")> 
<Assembly: AssemblyCopyright("© VERA 2008")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'El siguiente GUID sirve como identificador de la biblioteca de tipos si este proyecto se expone a COM
<Assembly: Guid("050d8587-1d76-4950-84cf-09afc81f081e")> 

' La información de versión de un ensamblado consta de los cuatro valores siguientes:
'
'      Versión principal
'      Versión secundaria 
'      Número de versión de compilación
'      Revisión
'
' Puede especificar todos los valores o establecer como predeterminados los números de versión de compilación y de revisión 
' mediante el asterisco ('*'), como se muestra a continuación:

<Assembly: AssemblyVersion("2.0.*")> 
<Assembly: AssemblyFileVersion("2.0.0.0")> 
