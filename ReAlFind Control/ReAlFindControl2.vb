Imports System.Data.OleDb
Imports System.Data


Public Class ReAlFindControl2
    Dim sCon3 As String = "User ID=sa;Tag with column collation when possible=False;Data Source=REAL\sql2k5;Pas" & "sword=VERA;Initial Catalog=toyosa;Use Procedure for Prepare=1;Auto Transla" & "te=True;Persist Security Info=True;Provider=""SQLOLEDB.1"";Workstation ID=(local);" & "Use Encryption for Data=False;Packet Size=4096;Connect Timeout=600"
    Private ReAlSQLCnControl As OleDbConnection
    Private ReAlSQLQuery As String = "select cod_plan_cuenta, cuenta_contable, desc_plan_cuenta from plan_cuenta"
    Private ReAlNumColumnas As Integer = 2
    Private ReAlForm As Form = Nothing
    Private dtPrincipal As New DataTable


    Private bSwBuscar As Boolean = True
    Public SelectedValue As String = "0"
    Private CambiarFocoEnter As Boolean = True


    Public Property ReAlFocusWithEnter() As Boolean
        Get
            Return CambiarFocoEnter
        End Get
        Set(ByVal Value As Boolean)
            CambiarFocoEnter = Value
        End Set
    End Property

    Private Sub ReAlFindControl_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ReAlSQLCnControl = New OleDb.OleDbConnection(sCon3)
        Me.ReAlCargarControl(Me.ReAlSQLCnControl, Me.ReAlSQLQuery, 2, Nothing)
    End Sub

    Public Sub ReAlCargarControl(ByVal Cn As OleDb.OleDbConnection, ByVal Query As String, Optional ByVal NumColumnas As Integer = 2, Optional ByVal frmForm As Form = Nothing)
        Me.ReAlSQLCnControl = Cn
        Me.ReAlSQLQuery = Query

        Me.ReAlNumColumnas = NumColumnas
        Query = "SELECT * SIESIS.* FROM ( " & Query & " ) AS SIESIS"

        Dim da As New OleDb.OleDbDataAdapter
        ReAlSQLCnControl.Open()
        Me.dtPrincipal.Clear()
        da = New OleDb.OleDbDataAdapter(ReAlSQLQuery, ReAlSQLCnControl)
        da.Fill(Me.dtPrincipal)
        ReAlSQLCnControl.Close()

        'Validamos el numero de columnas
        Dim NumCol As Integer = Me.dtPrincipal.Columns.Count
        If NumColumnas > NumCol Then
            NumColumnas = NumCol
        End If

        Me.ReAlNumColumnas = NumColumnas

    End Sub

    Public Sub ReAlCargarControl(ByVal Cn As OleDb.OleDbConnection, ByVal dt As DataTable, Optional ByVal NumColumnas As Integer = 2, Optional ByVal frmForm As Form = Nothing)
        Me.ReAlSQLCnControl = Cn

        Me.ReAlNumColumnas = NumColumnas

        Me.dtPrincipal.Clear()
        Me.dtPrincipal = dt
        ReAlSQLCnControl.Close()
        

        'Validamos el numero de columnas
        Dim NumCol As Integer = Me.dtPrincipal.Columns.Count
        If Me.ReAlNumColumnas > NumCol Then
            Me.ReAlNumColumnas = NumCol
        End If

    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.


    End Sub

    Private Sub txtBuscar_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBuscar.KeyDown
        If e.KeyValue = Keys.PageDown And Keys.Control Then
            Dim frm As New frmFindControl2
            frm.frmForm = Me.ReAlForm
            frm.dtPrincipal = Me.dtPrincipal
            frm.ShowDialog()
            e.Handled = True
            Me.txtBuscar.Text = frm.txtBuscar.Text
            MsgBox(frm.CodItem)
        End If
    End Sub


End Class
