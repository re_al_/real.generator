Public Class ReAlTextBoxAutocomplete
    Inherits TextBox
    Private dt As New DataTable

    Public SelectedValue As Integer = 0

    Public Property DataSource() As DataTable
        Get
            Return dt
        End Get
        Set(ByVal dtUser As DataTable)
            dt = dtUser
            AutoCompletar(Me)
        End Set
    End Property

    Public Sub New()
        AutoCompletar(Me)
    End Sub

    Private Sub TextBoxAutocomplete_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave
        Dim bExiste As Boolean = False

        Dim Parentesis1 As Integer = 0
        Dim Parentesis2 As Integer = Me.Text.Length - 1
        Dim Cod As String = ""

        'Hallamos el ultimo parentesis (
        For i As Integer = 0 To Me.Text.Length - 1
            If Me.Text(i) = "(" Then
                Parentesis1 = i
                bExiste = True
            End If
        Next

        If bExiste Then
            Try
                'Obtenemos el codigo entre parentesis
                For i As Integer = Parentesis1 To Parentesis2
                    Cod = Cod & Me.Text(i)
                Next
                Me.SelectedValue = CInt(Cod) * -1
            Catch ex As Exception
                Me.SelectedValue = 0
                Beep()
            End Try
        Else
            Me.SelectedValue = 0
        End If
    End Sub

    Public Sub SelectedValueIs(ByVal Value As Integer)
        Dim bExiste As Boolean = False
        Dim Parentesis1 As Integer
        Dim Parentesis2 As Integer
        Dim Cod As String
        Dim TextoFilaActual As String

        For j As Integer = 0 To Me.dt.Rows.Count - 1
            Parentesis1 = -1
            Parentesis2 = Me.dt.Rows(j).Item(0).ToString.Length - 1

            'Hallamos el ultimo parentesis (

            TextoFilaActual = Me.dt.Rows(j).Item(0)
            Dim Longitud As Integer = TextoFilaActual.Trim.Length
            For i As Integer = 0 To Longitud - 2
                If TextoFilaActual(i) = "(" Then
                    Parentesis1 = i
                End If
            Next

            If Parentesis1 <> -1 Then
                'Obtenemos el codigo entre parentesis
                Cod = ""
                For i As Integer = Parentesis1 + 1 To Parentesis2 - 1
                    Cod = Cod & TextoFilaActual(i)
                Next

                'Verificamos si coincide
                If Value = CInt(Cod) Then
                    Me.Text = TextoFilaActual
                    Me.SelectedValue = Value
                Else
                    Me.SelectedValue = Value
                End If
            End If
        Next

    End Sub

    Public Overrides Function PreProcessMessage(ByRef msg As System.Windows.Forms.Message) As Boolean
        If msg.Msg = &H102 Then
            If msg.WParam.Equals(New IntPtr(&H2E)) AndAlso msg.LParam.Equals(New IntPtr(&H530001)) Then
                ' Point in the numeric keypad pressed
                If System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = "," Then
                    ' And the decimal separator is a ',' for the current settings
                    ' Change the message to enter a comma instead of a poin
                    msg.WParam = New IntPtr(&H2C)
                    msg.LParam = New IntPtr(&H330001)
                End If
            End If
        End If

        ' Do the default actions
        Return MyBase.PreProcessMessage(msg)
    End Function

    Private Function AutoCompletar(ByVal Control As TextBox) As AutoCompleteStringCollection
        Dim Coleccion As New AutoCompleteStringCollection

        For i As Integer = 0 To Me.dt.Rows.Count - 1
            Coleccion.AddRange(New String() {Me.dt.Rows(i).Item(0)})
        Next

        'Ajustamos el control TextBox o ComboBox para recibir los datos de la siguiente manera.
        With Control
            .AutoCompleteMode = AutoCompleteMode.Suggest
            .AutoCompleteSource = AutoCompleteSource.CustomSource
            .AutoCompleteCustomSource = Coleccion
        End With
        'Devolvemos los datos recuperados de la base de datos
        Return Coleccion
    End Function

End Class