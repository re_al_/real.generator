Imports System
Imports System.Data
Imports System.Data.OleDb

Public Class frmFindControl2
    Public dtPrincipal As DataTable
    Public frmForm As Object
    Public CodItem As Integer = 0

    Private Sub Vehiculos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.dtgBuscar.DataSource = Me.dtPrincipal
        Me.dtgBuscar.Columns(0).Visible = False

        Me.txtBuscar.Text = Me.dtgBuscar.Item(1, Me.dtgBuscar.SelectedRows(0).Index).Value
        Me.txtBuscar.SelectAll()

    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.CodItem = Me.dtgBuscar.Item(0, Me.dtgBuscar.SelectedRows(0).Index).Value
        Me.Close()
    End Sub

    Private Sub txtBuscar_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBuscar.KeyDown
        CambiarIndice(sender, e)
    End Sub

    Private Sub CambiarIndice(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Try
            Dim IndiceActual As Integer

            If e.KeyCode = Keys.Down Then
                Try
                    IndiceActual = Me.dtgBuscar.SelectedRows(0).Index
                    Me.dtgBuscar.SelectedRows(0).Selected = False
                    Me.dtgBuscar.Rows(IndiceActual + 1).Selected = True
                Catch ex As Exception
                    Me.dtgBuscar.Rows(0).Selected = True
                End Try
                e.Handled = True
            End If

            If e.KeyCode = Keys.Up Then
                Try
                    IndiceActual = Me.dtgBuscar.SelectedRows(0).Index
                    Me.dtgBuscar.SelectedRows(0).Selected = False
                    Me.dtgBuscar.Rows(IndiceActual - 1).Selected = True
                Catch ex As Exception
                    Me.dtgBuscar.Rows(Me.dtgBuscar.RowCount - 1).Selected = True
                End Try
                e.Handled = True
            End If

            Me.txtBuscar.Text = Me.dtgBuscar.Item(1, Me.dtgBuscar.SelectedRows(0).Index).Value
            Me.txtBuscar.SelectAll()

            Try
                If Me.dtgBuscar.SelectedRows(0).Displayed = False Then
                    Me.dtgBuscar.FirstDisplayedScrollingRowIndex = Me.dtgBuscar.SelectedRows(0).Index
                End If
            Catch ex As Exception

            End Try
        Catch ex As Exception
            Beep()
            If e.KeyCode = Keys.Up Or e.KeyCode = Keys.Down Then
                e.Handled = True
            End If
        End Try

    End Sub

    Private Sub txtBuscar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBuscar.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        If KeyAscii = 13 Then
            Me.CodItem = Me.CodItem = Me.dtgBuscar.Item(0, Me.dtgBuscar.SelectedRows(0).Index).Value
            Me.Close()
        End If
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

    End Sub

    Private Sub txtBuscar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBuscar.TextChanged

    End Sub
End Class