<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReAlGridControl
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dtgControl = New System.Windows.Forms.DataGridView
        Me.cboPagina = New System.Windows.Forms.ComboBox
        Me.btnSig = New System.Windows.Forms.Button
        Me.btnUltimo = New System.Windows.Forms.Button
        Me.btnAnt = New System.Windows.Forms.Button
        Me.btnPrimero = New System.Windows.Forms.Button
        Me.lblContador = New System.Windows.Forms.Label
        CType(Me.dtgControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dtgControl
        '
        Me.dtgControl.AllowUserToAddRows = False
        Me.dtgControl.AllowUserToDeleteRows = False
        Me.dtgControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtgControl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgControl.Location = New System.Drawing.Point(3, 3)
        Me.dtgControl.Name = "dtgControl"
        Me.dtgControl.ReadOnly = True
        Me.dtgControl.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgControl.Size = New System.Drawing.Size(642, 474)
        Me.dtgControl.TabIndex = 0
        '
        'cboPagina
        '
        Me.cboPagina.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.cboPagina.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPagina.FormattingEnabled = True
        Me.cboPagina.Location = New System.Drawing.Point(263, 483)
        Me.cboPagina.Name = "cboPagina"
        Me.cboPagina.Size = New System.Drawing.Size(121, 21)
        Me.cboPagina.TabIndex = 1
        '
        'btnSig
        '
        Me.btnSig.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnSig.Location = New System.Drawing.Point(390, 481)
        Me.btnSig.Name = "btnSig"
        Me.btnSig.Size = New System.Drawing.Size(37, 23)
        Me.btnSig.TabIndex = 2
        Me.btnSig.Text = ">"
        Me.btnSig.UseVisualStyleBackColor = True
        '
        'btnUltimo
        '
        Me.btnUltimo.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnUltimo.Location = New System.Drawing.Point(433, 481)
        Me.btnUltimo.Name = "btnUltimo"
        Me.btnUltimo.Size = New System.Drawing.Size(37, 23)
        Me.btnUltimo.TabIndex = 3
        Me.btnUltimo.Text = ">>"
        Me.btnUltimo.UseVisualStyleBackColor = True
        '
        'btnAnt
        '
        Me.btnAnt.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnAnt.Location = New System.Drawing.Point(220, 481)
        Me.btnAnt.Name = "btnAnt"
        Me.btnAnt.Size = New System.Drawing.Size(37, 23)
        Me.btnAnt.TabIndex = 5
        Me.btnAnt.Text = "<"
        Me.btnAnt.UseVisualStyleBackColor = True
        '
        'btnPrimero
        '
        Me.btnPrimero.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnPrimero.Location = New System.Drawing.Point(177, 481)
        Me.btnPrimero.Name = "btnPrimero"
        Me.btnPrimero.Size = New System.Drawing.Size(37, 23)
        Me.btnPrimero.TabIndex = 4
        Me.btnPrimero.Text = "<<"
        Me.btnPrimero.UseVisualStyleBackColor = True
        '
        'lblContador
        '
        Me.lblContador.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblContador.AutoSize = True
        Me.lblContador.Location = New System.Drawing.Point(3, 486)
        Me.lblContador.Name = "lblContador"
        Me.lblContador.Size = New System.Drawing.Size(95, 13)
        Me.lblContador.TabIndex = 6
        Me.lblContador.Text = "(0) filas en pantalla"
        '
        'ReAlGridControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblContador)
        Me.Controls.Add(Me.btnAnt)
        Me.Controls.Add(Me.btnPrimero)
        Me.Controls.Add(Me.btnUltimo)
        Me.Controls.Add(Me.btnSig)
        Me.Controls.Add(Me.cboPagina)
        Me.Controls.Add(Me.dtgControl)
        Me.Name = "ReAlGridControl"
        Me.Size = New System.Drawing.Size(648, 504)
        CType(Me.dtgControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtgControl As System.Windows.Forms.DataGridView
    Private WithEvents cboPagina As System.Windows.Forms.ComboBox
    Private WithEvents btnSig As System.Windows.Forms.Button
    Private WithEvents btnUltimo As System.Windows.Forms.Button
    Private WithEvents btnAnt As System.Windows.Forms.Button
    Private WithEvents btnPrimero As System.Windows.Forms.Button
    Private WithEvents cellTextBox As DataGridViewTextBoxEditingControl
    Friend WithEvents lblContador As System.Windows.Forms.Label
End Class
