Imports System.Data.OleDb
Imports System.Data

Public Class ReAlGridControl
    'Dim sCon3 As String = "User ID=sa;Tag with column collation when possible=False;Data Source=VERA;Pas" & _
    '            "sword=REAL;Initial Catalog=PAT_CONTABILIDAD;Use Procedure for Prepare=1;Auto Transla" & _
    '            "te=True;Persist Security Info=True;Provider=""SQLOLEDB.1"";Workstation ID=(local);" & _
    '            "Use Encryption for Data=False;Packet Size=4096;Connect Timeout=600"
    Public ReAlSQLCnControl As OleDbConnection
    Public ReAlSQLQuery As String '= "select * from pres_doc_gasto"
    Private CadenaBusca(100) As String


    'Variables para la paginacion
    Private dtOriginal As New DataTable
    Private dtPrincipal As New DataTable
    Private dv As New DataView
    Private VistaDefault As Integer = 50
    Private TotalPaginas As Integer
    Private PaginaActual As Integer
    Private ini As Integer
    Private fin As Integer
    Private BuscarGrid As Boolean = True


    Private Sub ReAlGridControl_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'ReAlSQLCnControl = New OleDbConnection(sCon3)



        'paginacion
        cargardatatable()
        If Me.dtPrincipal.Rows.Count > 0 Then
            num_paginas()
            cboPagina.SelectedIndex = 0
            paginar()
        Else
            MessageBox.Show("No hay Datos Encontrados")
        End If
        Me.dtOriginal = Me.dtPrincipal
        Me.dtOriginal = Me.dtPrincipal.Copy
    End Sub


    Private Sub dtgControl_ColumnHeaderMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dtgControl.ColumnHeaderMouseDoubleClick
        For i As Integer = 0 To Me.CadenaBusca.Length - 1
            Me.CadenaBusca(i) = ""
        Next
        Me.dtgControl.DataSource = paginarDataDridView(dtPrincipal, ini, fin)

        paginar()
        Dim dvTemp As DataView
        Me.dtPrincipal.DefaultView.Sort = Nothing
        dvTemp = Me.dtOriginal.DefaultView
        dvTemp.Sort = Me.dtgControl.Columns(e.ColumnIndex).Name
        Me.dtPrincipal = Nothing
        Me.dtPrincipal = dvTemp.ToTable
        Me.dtgControl.DataSource = paginarDataDridView(dtPrincipal, ini, fin)
    End Sub

    Private Sub dtgControl_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dtgControl.DataBindingComplete
        'Debemos cambiar el color del Head en donde exista filtros
        Try
            Dim columnHeaderStyle As New DataGridViewCellStyle
            For i As Integer = 0 To Me.CadenaBusca.Length - 1
                If Me.CadenaBusca(i) <> "" Then
                    columnHeaderStyle.BackColor = Color.Lavender
                    Me.dtgControl.Columns(i).DefaultCellStyle = columnHeaderStyle
                End If
            Next
        Catch ex As Exception

        End Try

        Me.lblContador.Text = "(" & Me.dtgControl.RowCount & ") filas en pantalla"
    End Sub

    Private Sub dtgControl_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dtgControl.KeyDown
        If e.KeyCode = Keys.Escape Then
            Dim fila, columna As Integer
            If Me.dtgControl.Rows.Count > 0 Then
                fila = Me.dtgControl.CurrentCell.RowIndex
                columna = Me.dtgControl.CurrentCell.ColumnIndex
            End If
            For i As Integer = 0 To CadenaBusca.Length - 1
                CadenaBusca(i) = ""
            Next
            Me.dtgControl.DataSource = paginarDataDridView(dtPrincipal, ini, fin)
            If Me.dtgControl.Rows.Count > 0 Then
                Me.dtgControl.CurrentCell = Me.dtgControl.Item(columna, fila)
            End If
            Dim columnHeaderStyle As New DataGridViewCellStyle
            For i As Integer = 0 To Me.CadenaBusca.Length - 2
                columnHeaderStyle.BackColor = Color.White
                Me.dtgControl.Columns(i).DefaultCellStyle = columnHeaderStyle
            Next
            BuscarGrid = False
        End If
    End Sub

    Private Sub dtgBuscar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dtgControl.KeyPress
        If BuscarGrid = False Then
            For i As Integer = 0 To CadenaBusca.Length - 1
                CadenaBusca(i) = ""
            Next
            BuscarGrid = True
            Exit Sub
        End If

        Dim CaracterPresionado As String = e.KeyChar

        Try
            Dim IndiceCol As Integer = Me.dtgControl.CurrentCell.ColumnIndex

            CadenaBusca(IndiceCol) = CadenaBusca(IndiceCol) & CaracterPresionado
            If Me.dtgControl.Rows.Count > 0 Then
                'Dim ColumnaBusqueda As String = Me.dtgControl.Columns(Me.dtgControl.CurrentCell.ColumnIndex).Name
                Me.dv = Nothing
                Me.dv = Me.dtPrincipal.DefaultView
                Dim fila, columna As Integer
                fila = Me.dtgControl.CurrentCell.RowIndex
                columna = Me.dtgControl.CurrentCell.ColumnIndex

                Dim PrimerFiltro As Boolean = True
                Dim Filtro As String = ""
                For i As Integer = 0 To Me.CadenaBusca.Length - 1
                    If Me.CadenaBusca(i) <> "" Then
                        If PrimerFiltro Then
                            If Me.dtgControl.Columns(i).ValueType.ToString = "System.String" Then
                                Filtro = Filtro & " " & Me.dtgControl.Columns(i).Name & " LIKE '%" & Me.CadenaBusca(i) & "%'"
                            Else
                                Filtro = Filtro & " CONVERT(" & Me.dtgControl.Columns(i).Name & ", System.String) LIKE '%" & Me.CadenaBusca(i) & "%'"
                            End If
                            PrimerFiltro = False
                        Else
                            If Me.dtgControl.Columns(i).ValueType.ToString = "System.String" Then
                                Filtro = Filtro & " AND " & " " & Me.dtgControl.Columns(i).Name & " LIKE '%" & Me.CadenaBusca(i) & "%'"
                            Else
                                Filtro = Filtro & " AND " & " CONVERT(" & Me.dtgControl.Columns(i).Name & ", System.String) LIKE '%" & Me.CadenaBusca(i) & "%'"
                            End If
                        End If

                    End If
                Next

                Try
                    dv.RowFilter = Filtro 'ColumnaBusqueda & " like '%" & CadenaBusca & "%'"
                    Me.dtgControl.DataSource = Me.dv
                Catch ex1 As Exception
                    Beep()
                    Me.dtgControl.DataSource = paginarDataDridView(dtPrincipal, ini, fin)
                End Try

                Try
                    Me.dtgControl.CurrentCell = Me.dtgControl.Item(columna, fila)
                Catch ex As Exception

                End Try
            End If
        Catch ex As Exception
            Beep()
            Me.dtgControl.DataSource = paginarDataDridView(dtPrincipal, ini, fin)
        End Try


        
    End Sub

    Private Sub ANTES()
        'Dim CaracterPresionado As String = e.KeyChar
        'Me.CadenaBusca = CadenaBusca & CaracterPresionado
        Dim ColumnaBusqueda As String = Me.dtgControl.Columns(Me.dtgControl.CurrentCell.ColumnIndex).Name
        'Dim ColumnaCodigo As String = Me.dtgControl.Columns(0).Name
        'Dim IndiceActual As Integer = Me.dtgControl.Item(0, Me.dtgControl.CurrentCell.RowIndex).Value

        Dim sSQL As String = "SELECT alo.* FROM (" & Me.ReAlSQLQuery & ") AS alo ORDER BY [" & ColumnaBusqueda & "]"

        'sSQL = "select top 1 alo.* from (" & Me.ReAlSQLQuery & ") AS alo where alo.[" & ColumnaBusqueda & "] like '" & CadenaBusca & "%'"
        Dim i As Integer
        Dim command As OleDbCommand = Me.ReAlSQLCnControl.CreateCommand()
        command.CommandText = sSQL
        Dim CodTabla As Integer = 0
        Try
            Me.ReAlSQLCnControl.Open()
            Dim dataReader As OleDbDataReader = command.ExecuteReader()
            If dataReader.HasRows Then
                dataReader.Read()
                CodTabla = dataReader(0)
            End If
            dataReader.Close()
            Me.ReAlSQLCnControl.Close()

            If CodTabla <> 0 Then
                'Ubicamos la coincidencia
                Dim FilaElegida As Integer
                For i = 0 To Me.dtgControl.RowCount - 1
                    If Me.dtgControl.Item(0, i).Value = CodTabla Then
                        FilaElegida = i
                    End If
                Next
                Me.dtgControl.SelectedRows(0).Selected = False
                Me.dtgControl.Rows(FilaElegida).Selected = True

                If Me.dtgControl.RowCount - 1 > 0 Then
                    Me.dtgControl.FirstDisplayedScrollingRowIndex = Me.dtgControl.SelectedRows(0).Index
                End If

                'Me.dtgControl.Item(ColumnaBusqueda, FilaElegida).Selected = True
                Me.dtgControl.CurrentCell = Me.dtgControl.Item(ColumnaBusqueda, FilaElegida)
            Else
                Beep()

            End If
            'Contamos los Servicios
        Catch ex As Exception
            MsgBox(ex.Message)
            Me.ReAlSQLCnControl.Close()
        End Try

    End Sub

    'Funciones para paginar datagrid
    Public Function cargardatatable()
        Try
            Me.ReAlSQLCnControl.Open()
            Dim Query As String = "SELECT alo.* FROM (" & Me.ReAlSQLQuery & ") AS alo"
            Dim dtaDatos As New OleDb.OleDbDataAdapter(Query, ReAlSQLCnControl)
            dtaDatos.Fill(dtPrincipal)
            ReDim Me.CadenaBusca(dtPrincipal.Columns.Count)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Me.ReAlSQLCnControl.Close()
        End Try
        Return dtPrincipal
    End Function

    Private Sub num_paginas()
        If dtPrincipal.Rows.Count Mod VistaDefault = 0 Then
            TotalPaginas = dtPrincipal.Rows.Count / VistaDefault
        Else
            Dim value As Double = dtPrincipal.Rows.Count / VistaDefault
            TotalPaginas = Convert.ToInt32(value) + 1
        End If
        For i As Integer = 0 To TotalPaginas - 1
            cboPagina.Items.Add("P�gina " & i + 1)
        Next
    End Sub

    Private Sub paginar()

        Me.dtgControl.DataSource = paginarDataDridView(dtPrincipal, ini, fin)

        Dim columnHeaderStyle As New DataGridViewCellStyle

        For i As Integer = 0 To Me.CadenaBusca.Length - 2
            columnHeaderStyle.BackColor = Color.White
            Me.dtgControl.Columns(i).DefaultCellStyle = columnHeaderStyle
        Next
    End Sub

    Private Sub cbxPaginas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPagina.SelectedIndexChanged
        For i As Integer = 0 To CadenaBusca.Length - 1
            CadenaBusca(i) = ""
        Next
        Me.dtgControl.DataSource = paginarDataDridView(dtPrincipal, ini, fin)


        cambiar_pagina()
        paginar()

        'Validaciones de los botones
        If Me.cboPagina.SelectedIndex = 0 Then
            Me.btnPrimero.Enabled = False
            Me.btnAnt.Enabled = False
        Else
            Me.btnPrimero.Enabled = True
            Me.btnAnt.Enabled = True
        End If

        If Me.cboPagina.SelectedIndex = Me.cboPagina.Items.Count - 1 Then
            Me.btnUltimo.Enabled = False
            Me.btnSig.Enabled = False
        Else
            Me.btnUltimo.Enabled = True
            Me.btnSig.Enabled = True
        End If
    End Sub

    Private Sub cambiar_pagina()
        ini = 0
        fin = 0
        If Me.cboPagina.SelectedIndex = 0 Then
            fin = VistaDefault - 1
        Else
            ini = ((Convert.ToInt32(Me.cboPagina.SelectedIndex + 1) - 1) * VistaDefault) - 1
            fin = ini + VistaDefault
        End If

        If fin > dtPrincipal.Rows.Count Then
            fin = dtPrincipal.Rows.Count - 1
        End If
    End Sub

    Public Shared Function paginarDataDridView(ByVal dtPaginar As DataTable, ByVal inicial As Integer, ByVal final As Integer)
        Dim dtnew As New DataTable
        dtnew = dtPaginar.Clone
        For i As Integer = IIf(inicial = 0, inicial, inicial + 1) To final
            dtnew.ImportRow(dtPaginar.Rows(i))
        Next
        Return dtnew
    End Function

    Private Sub btnSig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSig.Click
        For i As Integer = 0 To CadenaBusca.Length - 1
            CadenaBusca(i) = ""
        Next
        Me.dtgControl.DataSource = paginarDataDridView(dtPrincipal, ini, fin)
        Me.cboPagina.SelectedIndex = Me.cboPagina.SelectedIndex + 1
    End Sub

    Private Sub btnAnt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnt.Click
        For i As Integer = 0 To CadenaBusca.Length - 1
            CadenaBusca(i) = ""
        Next
        Me.dtgControl.DataSource = paginarDataDridView(dtPrincipal, ini, fin)
        Me.cboPagina.SelectedIndex = Me.cboPagina.SelectedIndex - 1
    End Sub

    Private Sub btnPrimero_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimero.Click
        For i As Integer = 0 To CadenaBusca.Length - 1
            CadenaBusca(i) = ""
        Next
        Me.dtgControl.DataSource = paginarDataDridView(dtPrincipal, ini, fin)
        Me.cboPagina.SelectedIndex = 0
    End Sub

    Private Sub btnUltimo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUltimo.Click
        For i As Integer = 0 To CadenaBusca.Length - 1
            CadenaBusca(i) = ""
        Next
        Me.dtgControl.DataSource = paginarDataDridView(dtPrincipal, ini, fin)
        Me.cboPagina.SelectedIndex = Me.cboPagina.Items.Count - 1
    End Sub

End Class




