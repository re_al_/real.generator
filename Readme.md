﻿ReAl  Generator
=========

[![Build status](https://ci.appveyor.com/api/projects/status/yaf39ypcrdwk6rxl?svg=true)](https://ci.appveyor.com/project/re_al_/real-generator)

ReAl Generator es una herramienta que ayuda con la generación de capas DAL (Data Access Layer) en distintos lenguajes de Programación.

  - Soporte para CSharp, VB.Net, Java, Java for Android, PHP, PHP con CodeIgniter y Python, Javascript (NodeJS)

  - Soporte para SqlServer, PostgreSql, OracleDB, SqLite y FireBird

  - Crea al menos un archivo por cada entidad del modelo ER de la Base de Datos

  - Crea al menos un archivo para la generacion de Reglas de Negocio (Operaciones CRUD) por cada entidad del modelo ER de la Base de Datos

  - Genera los Procedimiento Almacenados para las Bases de Datos SqlServer, PostgreSql, OracleDB

  - Generacion de Triggers para el control del ciclo de vida de cada tabla en las Bases de Datos SqlServer, PostgreSql

  - Generación de Pruebas unitarias para las operacioes CRUD creadas en la capa DAL.

  - Manejo de plantillas para administracion más facil de proyectos (Almacenados en SqLite)

  - Integración de Plantillas de Proyectos en CSharp para VStudio2010, VStudio2012, VStudio2013, VStudio2015 y VStudio2017

  - Soporte para Sistemas x86 y x64

  - **2016.04.27** Se ha diferenciado las funciones Update y UpdateAll para las actualizaciones en la BD
  
  - **2016.05.21** Se ha añadido el soporte para NUMERIC en SqLite 

  - **2016.06.30** Se han adicionado los métodos para obtener (GetDato) y colocar (SetDato) valores a una propiedad dentro de un objeto instanciado (Reflection)
  
  - **2016.11.03** Se ha añadido la opcion de generar CodeSnippets para las plantillas Bootstrap en la pestaña de Herramientas Base de Datos

  - **2016.11.10** Opcion para crear Web APIs

  - **2017.04.17** Metodo estatico en las Entidades para obtener un objeto en base a su PK

  - **2017.04.17** Metodo ObtenerObjetoInsertado para obtener el ultimo objeto insertado en la BD por un determinado usuario
  
  - **2017.08.08** Se ha simplificado el entorno visual y se cambia al concepto de ENTORNO. Se ha migrado todo el Sistema a CSHARP
  
  - **2019.05.02** Se ha añadido las plantillas y soporte para Swagger en NetCore Web API

  - **2020.03.30** Se han añadido la parametrización para cApiObject en la creación de clases de tipo Entidades (cSharp)

  - **2020.04.02** Se ha añadido el snippet ara scaffold para NetCore MVC

  - **2021.01.18** Se ha añadido y mejorado el soporte para NetCore V5

  - **2021.01.30** Se ha añadido la creación de clientes para WebApi en el caso de Swagger

  - **2022.03.16** Se ha añadido soporte para creación de APIS con Codeigniter usando Models

  - **2022.03.18** Se ha añadido las funiones para crear UnitTest para los APIs con Codeigniter usando Models

  - **2022.03.18** Se ha añadido la generación de código para archivos json de compatibles con Swagger-Editor y el estandar Open API para los servicios generados para CodeIgniter usando Models
  
 
Equipo de Desarrollo
-----------

Reynaldo Alonzo Vera Arias

  - [Correo Electrónico]
  - [Twiter]
  - [About Me]

[About Me]:http://about.me/alonzo.vera
[R. Alonzo Vera A.]:mailto:7.re.al.7@gmail.com?Subject=ReAlCodeGenInstaller
[Correo Electrónico]:mailto:7.re.al.7@gmail.com?Subject=ReAlCodeGen
[Twiter]:https://twitter.com/re_al_
