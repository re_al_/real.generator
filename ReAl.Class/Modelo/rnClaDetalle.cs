﻿#region

/***********************************************************************************************************
	NOMBRE:       rnClaDetalle
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla ClaDetalle

	REVISIONES:
		Ver        FECHA       Autor            Descripción 
		---------  ----------  ---------------  ------------------------------------
		1.0        15/08/2013  R Alonzo Vera A  Creación 

*************************************************************************************************************/

#endregion

#region librerias

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Windows.Forms;
using ReAl.Class.Entidades;
using ReAl.Class.Interface;
using ReAl.Conn;

#endregion

namespace ReAl.Class.Modelo
{
    public class rnClaDetalle : inClaDetalle
    {
        //Debe implementar la Interface (Alt + Shift + F10)

        #region inClaDetalle Members

        /// <summary>
        /// 	Funcion que obtiene la Descripción de Determinado Campo
        /// </summary>
        /// <param name="MYField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo solicitado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo STRING que describe al parametro
        /// </returns>
        public string GetDesc(entClaDetalle.Fields myField)
        {
            try
            {
                if (entClaDetalle.Fields.idDet.ToString() == myField.ToString())
                    return "Columna de tipo integer de la tabla claDetalle";
                if (entClaDetalle.Fields.idPlantilla.ToString() == myField.ToString())
                    return "Columna de tipo nvarchar(50) de la tabla claDetalle";
                if (entClaDetalle.Fields.propiedad.ToString() == myField.ToString())
                    return "Columna de tipo nvarchar(50) de la tabla claDetalle";
                if (entClaDetalle.Fields.valor.ToString() == myField.ToString())
                    return "Columna de tipo nvarchar(50) de la tabla claDetalle";
                return string.Empty;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene el Error de Determinado Campo
        /// </summary>
        /// <param name="myField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo solicitado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo STRING que describe al parametro
        /// </returns>
        public string GetError(entClaDetalle.Fields myField)
        {
            try
            {
                if (entClaDetalle.Fields.idDet.ToString() == myField.ToString())
                    return "int para idDet";
                if (entClaDetalle.Fields.idPlantilla.ToString() == myField.ToString())
                    return "String para idPlantilla";
                if (entClaDetalle.Fields.propiedad.ToString() == myField.ToString())
                    return "String para propiedad";
                if (entClaDetalle.Fields.valor.ToString() == myField.ToString())
                    return "String para valor";
                return string.Empty;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene el Nombre de Determinado Campo
        /// </summary>
        /// <param name="myField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo solicitado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo STRING que describe al parametro
        /// </returns>
        public string GetColumn(entClaDetalle.Fields myField)
        {
            try
            {
                if (entClaDetalle.Fields.idDet.ToString() == myField.ToString())
                    return "idDet:";
                if (entClaDetalle.Fields.idPlantilla.ToString() == myField.ToString())
                    return "idPlantilla:";
                if (entClaDetalle.Fields.propiedad.ToString() == myField.ToString())
                    return "propiedad:";
                if (entClaDetalle.Fields.valor.ToString() == myField.ToString())
                    return "valor:";
                return string.Empty;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public entClaDetalle ObtenerObjeto(int intidDet)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(entClaDetalle.Fields.idDet.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(intidDet);

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                if (table.Rows.Count == 1)
                {
                    entClaDetalle obj = new entClaDetalle();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public entClaDetalle ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                if (table.Rows.Count == 1)
                {
                    entClaDetalle obj = new entClaDetalle();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public entClaDetalle ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count == 1)
                {
                    entClaDetalle obj = new entClaDetalle();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public entClaDetalle ObtenerObjeto(entClaDetalle.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                if (table.Rows.Count == 1)
                {
                    entClaDetalle obj = new entClaDetalle();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public entClaDetalle ObtenerObjeto(entClaDetalle.Fields searchField, object searchValue,
            string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count == 1)
                {
                    entClaDetalle obj = new entClaDetalle();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un Business Object del Tipo entClaDetalle a partir de su llave promaria
        /// </summary>
        /// <returns>
        /// 	Objeto del Tipo entClaDetalle
        /// </returns>
        public entClaDetalle ObtenerObjeto(int intidDet, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(entClaDetalle.Fields.idDet.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(intidDet);

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, ref localTrans);

                if (table.Rows.Count == 1)
                {
                    entClaDetalle obj = new entClaDetalle();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public entClaDetalle ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, ref localTrans);

                if (table.Rows.Count == 1)
                {
                    entClaDetalle obj = new entClaDetalle();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public entClaDetalle ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count == 1)
                {
                    entClaDetalle obj = new entClaDetalle();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public entClaDetalle ObtenerObjeto(entClaDetalle.Fields searchField, object searchValue, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, ref localTrans);

                if (table.Rows.Count == 1)
                {
                    entClaDetalle obj = new entClaDetalle();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public entClaDetalle ObtenerObjeto(entClaDetalle.Fields searchField, object searchValue,
            string strParamAdicionales, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count == 1)
                {
                    entClaDetalle obj = new entClaDetalle();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo List<entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaDetalle> ObtenerLista()
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo List<entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaDetalle> ObtenerLista(ArrayList arrColumnas)
        {
            try
            {
                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo List<entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaDetalle> ObtenerLista(ArrayList arrColumnas, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);


                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo List<entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaDetalle> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo List<entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaDetalle> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo List<entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaDetalle> ObtenerLista(entClaDetalle.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo List<entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaDetalle> ObtenerLista(entClaDetalle.Fields searchField, object searchValue,
            string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo List<entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaDetalle> ObtenerLista(ArrayList arrColumnas, ref cTrans localTrans)
        {
            try
            {
                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTable(cParametros.schema + "ClaDetalle", arrColumnas, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo List<entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaDetalle> ObtenerLista(ArrayList arrColumnas, string strParamAdicionales, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);


                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo List<entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaDetalle> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo List<entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaDetalle> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo List<entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaDetalle> ObtenerLista(entClaDetalle.Fields searchField, object searchValue,
            ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo List<entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaDetalle> ObtenerLista(entClaDetalle.Fields searchField, object searchValue,
            string strParamAdicionales, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo Dictionary<String, entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaDetalle> ObtenerDiccionario()
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Dictionary<String, entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaDetalle> ObtenerDiccionario(ArrayList arrColumnas)
        {
            try
            {
                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Dictionary<String, entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaDetalle> ObtenerDiccionario(ArrayList arrColumnas, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);


                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Dictionary<String, entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaDetalle> ObtenerDiccionario(ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Dictionary<String, entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaDetalle> ObtenerDiccionario(ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Dictionary<String, entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaDetalle> ObtenerDiccionario(entClaDetalle.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Dictionary<String, entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaDetalle> ObtenerDiccionario(entClaDetalle.Fields searchField, object searchValue,
            string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Dictionary<String, entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaDetalle> ObtenerDiccionario(ArrayList arrColumnas, ref cTrans localTrans)
        {
            try
            {
                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTable(cParametros.schema + "ClaDetalle", arrColumnas, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Dictionary<String, entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaDetalle> ObtenerDiccionario(ArrayList arrColumnas, string strParamAdicionales,
            ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);


                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Dictionary<String, entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaDetalle> ObtenerDiccionario(ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Dictionary<String, entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaDetalle> ObtenerDiccionario(ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Dictionary<String, entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaDetalle> ObtenerDiccionario(entClaDetalle.Fields searchField, object searchValue,
            ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaDetalle a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Dictionary<String, entClaDetalle> que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaDetalle> ObtenerDiccionario(entClaDetalle.Fields searchField, object searchValue,
            string strParamAdicionales, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, entClaDetalle>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que obtiene la llave primaria unica de la tabla ClaDetalle a partir de una cadena
        /// </summary>
        /// <param name="cod" type="string">
        ///     <para>
        /// 		 Cadena desde la que se construye el identificador unico de la tabla ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Identificador unico de la tabla ClaDetalle
        /// </returns>
        public string CreatePK(string[] args)
        {
            return args[0];
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla ClaDetalle a partir de una clase del tipo EClaDetalle
        /// </summary>
        /// <param name="obj" type="Entidades.entClaDetalle">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionClaDetalle
        /// </returns>
        public bool Insert(entClaDetalle obj)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.idPlantilla);
                arrValores.Add("'" + obj.propiedad + "'");
                arrValores.Add("'" + obj.valor + "'");

                cConn local = new cConn();
                return local.insertBD("ClaDetalle", arrColumnas, arrValores);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla ClaDetalle a partir de una clase del tipo EClaDetalle
        /// </summary>
        /// <param name="obj" type="Entidades.entClaDetalle">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionClaDetalle
        /// </returns>
        public bool Insert(entClaDetalle obj, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.idDet);
                arrValores.Add("'" + obj.idPlantilla + "'");
                arrValores.Add("'" + obj.propiedad + "'");
                arrValores.Add("'" + obj.valor + "'");

                cConn local = new cConn();
                return local.insertBD("ClaDetalle", arrColumnas, arrValores, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla ClaDetalle a partir de una clase del tipo EClaDetalle
        /// </summary>
        /// <param name="obj" type="Entidades.entClaDetalle">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionClaDetalle
        /// </returns>
        public bool InsertIdentity(entClaDetalle obj)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.idDet);
                arrValores.Add("'" + obj.idPlantilla + "'");
                arrValores.Add("'" + obj.propiedad + "'");
                arrValores.Add("'" + obj.valor + "'");

                cConn local = new cConn();
                int intIdentidad = -1;
                bool res = local.insertBD("ClaDetalle", arrColumnas, arrValores, ref intIdentidad);
                obj.idDet = intIdentidad;
                return res;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla ClaDetalle a partir de una clase del tipo EClaDetalle
        /// </summary>
        /// <param name="obj" type="Entidades.entClaDetalle">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionClaDetalle
        /// </returns>
        public bool InsertIdentity(entClaDetalle obj, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.idDet);
                arrValores.Add("'" + obj.idPlantilla + "'");
                arrValores.Add("'" + obj.propiedad + "'");
                arrValores.Add("'" + obj.valor + "'");

                cConn local = new cConn();
                int intIdentidad = -1;
                bool res = local.insertBD("ClaDetalle", arrColumnas, arrValores, ref intIdentidad, ref localTrans);
                obj.idDet = intIdentidad;
                return res;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla ClaDetalle a partir de una clase del tipo EClaDetalle
        /// </summary>
        /// <param name="obj" type="Entidades.entClaDetalle">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionClaDetalle
        /// </returns>
        public int Update(entClaDetalle obj)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add("'" + obj.idPlantilla + "'");
                arrValores.Add("'" + obj.propiedad + "'");
                arrValores.Add("'" + obj.valor + "'");

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(entClaDetalle.Fields.idDet.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.idDet);

                cConn local = new cConn();
                return local.updateBD("ClaDetalle", arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla ClaDetalle a partir de una clase del tipo eClaDetalle
        /// </summary>
        /// <param name="obj" type="Entidades.entClaDetalle">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla ClaDetalle
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacion
        /// </returns>
        public int Update(entClaDetalle obj, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add("'" + obj.idPlantilla + "'");
                arrValores.Add("'" + obj.propiedad + "'");
                arrValores.Add("'" + obj.valor + "'");

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(entClaDetalle.Fields.idDet.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.idDet);

                cConn local = new cConn();
                return local.updateBD("ClaDetalle", arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere,
                    ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla ClaDetalle a partir de una clase del tipo entClaDetalle y su respectiva PK
        /// </summary>
        /// <param name="obj" type="Entidades.entClaDetalle">
        ///     <para>
        /// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionClaDetalle
        /// </returns>
        public int Delete(entClaDetalle obj)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(entClaDetalle.Fields.idDet.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.idDet);

                cConn local = new cConn();
                return local.deleteBD("ClaDetalle", arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla ClaDetalle a partir de una clase del tipo entClaDetalle y su PK
        /// </summary>
        /// <param name="obj" type="Entidades.eClaDetalle">
        ///     <para>
        /// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla ClaDetalle
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacionClaDetalle
        /// </returns>
        public int Delete(entClaDetalle obj, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(entClaDetalle.Fields.idDet.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.idDet);

                cConn local = new cConn();
                return local.deleteBD("ClaDetalle", arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla ClaDetalle a partir de una clase del tipo eClaDetalle
        /// </summary>
        /// <param name="ArrColumnasWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Columnas en la clausa WHERE 
        ///     </para>
        /// </param>
        /// <param name="ArrValoresWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionClaDetalle
        /// </returns>
        public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                cConn local = new cConn();
                return local.deleteBD("ClaDetalle", arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla ClaDetalle a partir de una clase del tipo eClaDetalle
        /// </summary>
        /// <param name="ArrColumnasWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Columnas en la clausa WHERE 
        ///     </para>
        /// </param>
        /// <param name="ArrValoresWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaDetalle
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacionClaDetalle
        /// </returns>
        public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
        {
            try
            {
                cConn local = new cConn();
                return local.deleteBD("ClaDetalle", arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }


        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla ClaDetalle
        /// </summary>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaDetalle
        /// </returns>
        public DataTable NuevoDataTable()
        {
            try
            {
                DataTable table = new DataTable();
                DataColumn dc;
                dc = new DataColumn(entClaDetalle.Fields.idDet.ToString(),
                    typeof (entClaDetalle).GetProperty(entClaDetalle.Fields.idDet.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(entClaDetalle.Fields.idPlantilla.ToString(),
                    typeof (entClaDetalle).GetProperty(entClaDetalle.Fields.idPlantilla.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(entClaDetalle.Fields.propiedad.ToString(),
                    typeof (entClaDetalle).GetProperty(entClaDetalle.Fields.propiedad.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(entClaDetalle.Fields.valor.ToString(),
                    typeof (entClaDetalle).GetProperty(entClaDetalle.Fields.valor.ToString()).PropertyType);
                table.Columns.Add(dc);


                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que genera un DataTable con determinadas columnas de una ClaDetalle
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaDetalle
        /// </returns>
        public DataTable NuevoDataTable(ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'2'");

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla ClaDetalle
        /// </summary>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaDetalle
        /// </returns>
        public DataTable CargarDataTable()
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());


                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE ClaDetalle
        /// </summary>
        /// <param name="condicionesWhere" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaDetalle
        /// </returns>
        public DataTable CargarDataTable(String condicionesWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());


                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, " AND (" + condicionesWhere + ")");

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaDetalle
        /// </returns>
        public DataTable CargarDataTable(ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaDetalle
        /// </returns>
        public DataTable CargarDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaDetalle
        /// </returns>
        public DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaDetalle
        /// </returns>
        public DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());
                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaDetalle
        /// </returns>
        public DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParametrosAdicionales)
        {
            try
            {
                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaDetalle
        /// </returns>
        public DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());
                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable CargarDataTable(entClaDetalle.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                if (table.Rows.Count > 0)
                {
                    return table;
                }
                else
                    return new DataTable();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable CargarDataTable(entClaDetalle.Fields searchField, object searchValue,
            string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParametrosAdicionales);

                if (table.Rows.Count > 0)
                {
                    return table;
                }
                else
                    return new DataTable();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable CargarDataTable(ArrayList arrColumnas, entClaDetalle.Fields searchField, object searchValue,
            string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);


                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParametrosAdicionales);

                if (table.Rows.Count > 0)
                {
                    return table;
                }
                else
                    return new DataTable();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable CargarDataTable(ArrayList arrColumnas, entClaDetalle.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);


                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                if (table.Rows.Count > 0)
                {
                    return table;
                }
                else
                    return new DataTable();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaDetalle
        /// </returns>
        public DataTable CargarDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());
                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableOr("ClaDetalle", arrColumnas, arrColumnasWhere, arrValoresWhere);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaDetalle
        /// </returns>
        public DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableOr("ClaDetalle", arrColumnas, arrColumnasWhere, arrValoresWhere);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaDetalle
        /// </returns>
        public DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParametrosAdicionales)
        {
            try
            {
                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableOr("ClaDetalle", arrColumnas, arrColumnasWhere, arrValoresWhere,
                    strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaDetalle
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        public void CargarCombo(ref ComboBox cmb)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("idDet");
                arrColumnas.Add("idPlantilla");

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTable("ClaDetalle", arrColumnas);

                cmb.DataSource = table;
                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaDetalle
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarCombo(ref ComboBox cmb, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("idDet");
                arrColumnas.Add("idPlantilla");

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                cmb.DataSource = table;
                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaDetalle
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarCombo(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("idDet");
                arrColumnas.Add("idPlantilla");

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                cmb.DataSource = table;
                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaDetalle
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarCombo(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("idDet");
                arrColumnas.Add("idPlantilla");

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                cmb.DataSource = table;
                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaDetalle
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, entClaDetalle.Fields textField)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTable("ClaDetalle", arrColumnas);

                cmb.DataSource = table;
                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaDetalle
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, entClaDetalle.Fields textField,
            string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                cmb.DataSource = table;
                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaDetalle
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, String textField)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTable("ClaDetalle", arrColumnas);

                cmb.DataSource = table;
                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaDetalle
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, String textField,
            String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                cmb.DataSource = table;
                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaDetalle
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, entClaDetalle.Fields textField,
            entClaDetalle.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                cmb.DataSource = table;
                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaDetalle
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, String textField,
            entClaDetalle.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                cmb.DataSource = table;
                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaDetalle
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, entClaDetalle.Fields textField,
            entClaDetalle.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                cmb.DataSource = table;
                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaDetalle
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, String textField,
            entClaDetalle.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                cmb.DataSource = table;
                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaDetalle
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, entClaDetalle.Fields textField,
            ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                cmb.DataSource = table;
                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaDetalle
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, String textField,
            ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                cmb.DataSource = table;
                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaDetalle
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, entClaDetalle.Fields textField,
            ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                cmb.DataSource = table;
                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaDetalle
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaDetalleClaDetalle
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, String textField,
            ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                DataTable table = new DataTable();
                cConn local = new cConn();
                table = local.cargarDataTableAnd(cParametros.schema + "ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                cmb.DataSource = table;
                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView 
        ///     </para>
        /// </param>
        public void CargarGridView(ref DataGridView dtg)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());


                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                cConn local = new cConn();
                DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + "ClaDetalle", arrColumnas,
                    arrColumnasWhere, arrValoresWhere);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView 
        ///     </para>
        /// </param>
        /// <param name="condicionesWhere" type="String">
        ///     <para>
        /// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarGridView(ref DataGridView dtg, String condicionesWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());


                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                cConn local = new cConn();
                DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + "ClaDetalle", arrColumnas,
                    arrColumnasWhere, arrValoresWhere, " AND (" + condicionesWhere + ")");

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView 
        ///     </para>
        /// </param>
        public void CargarGridView(ref DataGridView dtg, ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                cConn local = new cConn();
                DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + "ClaDetalle", arrColumnas,
                    arrColumnasWhere, arrValoresWhere);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView 
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref DataGridView dtg, ArrayList arrColumnas, String strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                cConn local = new cConn();
                DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + "ClaDetalle", arrColumnas,
                    arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView 
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                cConn local = new cConn();
                DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + "ClaDetalle", arrColumnas,
                    arrColumnasWhere, arrValoresWhere);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView 
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            String strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                cConn local = new cConn();
                DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + "ClaDetalle", arrColumnas,
                    arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView 
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere)
        {
            try
            {
                cConn local = new cConn();
                DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + "ClaDetalle", arrColumnas,
                    arrColumnasWhere, arrValoresWhere);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView 
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                cConn local = new cConn();
                DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + "ClaDetalle", arrColumnas,
                    arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarGridView(ref DataGridView dtg, entClaDetalle.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                cConn local = new cConn();
                DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + "ClaDetalle", arrColumnas,
                    arrColumnasWhere, arrValoresWhere);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref DataGridView dtg, entClaDetalle.Fields searchField, object searchValue,
            String strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaDetalle.Fields.idDet.ToString());
                arrColumnas.Add(entClaDetalle.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaDetalle.Fields.propiedad.ToString());
                arrColumnas.Add(entClaDetalle.Fields.valor.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                cConn local = new cConn();
                DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + "ClaDetalle", arrColumnas,
                    arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarGridView(ref DataGridView dtg, ArrayList arrColumnas, entClaDetalle.Fields searchField,
            object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                cConn local = new cConn();
                DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + "ClaDetalle", arrColumnas,
                    arrColumnasWhere, arrValoresWhere);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref DataGridView dtg, ArrayList arrColumnas, entClaDetalle.Fields searchField,
            object searchValue, String strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                cConn local = new cConn();
                DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + "ClaDetalle", arrColumnas,
                    arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView 
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridViewOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere)
        {
            try
            {
                cConn local = new cConn();
                DbDataReader dsReader = local.cargarDataReaderOr("ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaDetalle
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView 
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridViewOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                cConn local = new cConn();
                DbDataReader dsReader = local.cargarDataReaderOr("ClaDetalle", arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesCount(entClaDetalle.Fields refField)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("count(" + refField + ")");
                DataTable dtTemp = CargarDataTable(arrColumnas);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                return dtTemp.Rows[0][0];
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesCount(entClaDetalle.Fields refField, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("count(" + refField + ")");
                DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                return dtTemp.Rows[0][0];
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesMin(entClaDetalle.Fields refField)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("min(" + refField + ")");
                DataTable dtTemp = CargarDataTable(arrColumnas);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                return dtTemp.Rows[0][0];
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesMin(entClaDetalle.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("min(" + refField + ")");
                DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                return dtTemp.Rows[0][0];
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesMax(entClaDetalle.Fields refField)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("max(" + refField + ")");
                DataTable dtTemp = CargarDataTable(arrColumnas);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                return dtTemp.Rows[0][0];
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesMax(entClaDetalle.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("max(" + refField + ")");
                DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                return dtTemp.Rows[0][0];
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesSum(entClaDetalle.Fields refField)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("sum(" + refField + ")");
                DataTable dtTemp = CargarDataTable(arrColumnas);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                return dtTemp.Rows[0][0];
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesSum(entClaDetalle.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("sum(" + refField + ")");
                DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                return dtTemp.Rows[0][0];
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesAvg(entClaDetalle.Fields refField)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("avg(" + refField + ")");
                DataTable dtTemp = CargarDataTable(arrColumnas);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                return dtTemp.Rows[0][0];
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="entClaDetalle.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaDetalle que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesAvg(entClaDetalle.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("avg(" + refField + ")");
                DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                return dtTemp.Rows[0][0];
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        /// <summary>
        /// 	 Funcion que devuelve un objeto a partir de un DataRow
        /// </summary>
        /// <param name="row" type="System.Data.DataRow">
        ///     <para>
        /// 		 DataRow con el conjunto de Datos recuperados 
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Objeto ClaDetalle
        /// </returns>
        internal entClaDetalle crearObjeto(DataRow row)
        {
            entClaDetalle obj = new entClaDetalle();
            obj.idDet = int.Parse(row[entClaDetalle.Fields.idDet.ToString()].ToString());
            obj.idPlantilla = int.Parse(row[entClaDetalle.Fields.idPlantilla.ToString()].ToString());
            obj.propiedad = row[entClaDetalle.Fields.propiedad.ToString()].ToString();
            obj.valor = row[entClaDetalle.Fields.valor.ToString()].ToString();
            return obj;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtClaDetalle" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados 
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Lista de Objetos ClaDetalle
        /// </returns>
        internal List<entClaDetalle> crearLista(DataTable dtClaDetalle)
        {
            List<entClaDetalle> list = new List<entClaDetalle>();

            entClaDetalle obj = new entClaDetalle();
            foreach (DataRow row in dtClaDetalle.Rows)
            {
                obj = crearObjeto(row);
                list.Add(obj);
            }
            return list;
        }

        /// <summary>
        /// 	 FUncion que crea un Dicionario a partir de un DataTable
        /// </summary>
        /// <param name="dtClaDetalle" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados 
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Diccionario de Objetos ClaDetalle
        /// </returns>
        internal Dictionary<String, entClaDetalle> crearDiccionario(DataTable dtClaDetalle)
        {
            Dictionary<String, entClaDetalle> miDic = new Dictionary<String, entClaDetalle>();

            entClaDetalle obj = new entClaDetalle();
            foreach (DataRow row in dtClaDetalle.Rows)
            {
                obj = crearObjeto(row);
                miDic.Add(obj.idDet.ToString(), obj);
            }
            return miDic;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        protected void Finalize()
        {
            Dispose();
        }
    }
}