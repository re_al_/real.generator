#region

/***********************************************************************************************************
	NOMBRE:       rnClaPlantilla
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla ClaPlantilla

	REVISIONES:
		Ver        FECHA       Autor            Descripcion
		---------  ----------  ---------------  ------------------------------------
		1.0        01/06/2015  R Alonzo Vera A  Creacion

*************************************************************************************************************/

#endregion

#region librerias

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Windows.Forms;
using ReAl.Class.Entidades;
using ReAl.Class.Interface;
using ReAl.Conn;

#endregion

namespace ReAl.Class.Modelo
{
    public class rnClaPlantilla : inClaPlantilla
    {
        //Debe implementar la Interface (Alt + Shift + F10)

        #region inClaPlantilla Members

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public entClaPlantilla ObtenerObjeto(int intidPlantilla)
        {
            ArrayList arrColumnasWhere = new ArrayList
            {
                entClaPlantilla.Fields.idPlantilla.ToString()
            };

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(intidPlantilla);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public entClaPlantilla ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public entClaPlantilla ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + entClaPlantilla.strNombreTabla,
                    arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count == 1)
                {
                    entClaPlantilla obj = new entClaPlantilla();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public entClaPlantilla ObtenerObjeto(entClaPlantilla.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public entClaPlantilla ObtenerObjeto(entClaPlantilla.Fields searchField, object searchValue,
            string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un Business Object del Tipo entClaPlantilla a partir de su llave promaria
        /// </summary>
        /// <returns>
        /// 	Objeto del Tipo entClaPlantilla
        /// </returns>
        public entClaPlantilla ObtenerObjeto(int intidPlantilla, ref cTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(entClaPlantilla.Fields.idPlantilla.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(intidPlantilla);
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public entClaPlantilla ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            ref cTrans localTrans)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public entClaPlantilla ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + entClaPlantilla.strNombreTabla,
                    arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count == 1)
                {
                    entClaPlantilla obj = new entClaPlantilla();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public entClaPlantilla ObtenerObjeto(entClaPlantilla.Fields searchField, object searchValue,
            ref cTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public entClaPlantilla ObtenerObjeto(entClaPlantilla.Fields searchField, object searchValue,
            string strParamAdicionales, ref cTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerLista()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerLista(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + entClaPlantilla.strNombreTabla,
                    arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<entClaPlantilla>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerLista(entClaPlantilla.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerLista(entClaPlantilla.Fields searchField, object searchValue,
            string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            ref cTrans localTrans)
        {
            return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + entClaPlantilla.strNombreTabla,
                    arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<entClaPlantilla>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerLista(entClaPlantilla.Fields searchField, object searchValue,
            ref cTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerLista(entClaPlantilla.Fields searchField, object searchValue,
            string strParamAdicionales, ref cTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerListaDesdeVista(String strVista)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere)
        {
            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<entClaPlantilla>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerListaDesdeVista(String strVista, entClaPlantilla.Fields searchField,
            object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerListaDesdeVista(String strVista, entClaPlantilla.Fields searchField,
            object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, ref cTrans localTrans)
        {
            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + strVista, arrColumnas, arrColumnasWhere,
                    arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<entClaPlantilla>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerListaDesdeVista(String strVista, entClaPlantilla.Fields searchField,
            object searchValue, ref cTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<entClaPlantilla> ObtenerListaDesdeVista(String strVista, entClaPlantilla.Fields searchField,
            object searchValue, string strParamAdicionales, ref cTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales,
                ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<entClaPlantilla> ObtenerCola()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerCola(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<entClaPlantilla> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<entClaPlantilla> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + entClaPlantilla.strNombreTabla,
                    arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearCola(table);
                }
                else
                    return new Queue<entClaPlantilla>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<entClaPlantilla> ObtenerCola(entClaPlantilla.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<entClaPlantilla> ObtenerCola(entClaPlantilla.Fields searchField, object searchValue,
            string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<entClaPlantilla> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            ref cTrans localTrans)
        {
            return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<entClaPlantilla> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + entClaPlantilla.strNombreTabla,
                    arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearCola(table);
                }
                else
                    return new Queue<entClaPlantilla>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<entClaPlantilla> ObtenerCola(entClaPlantilla.Fields searchField, object searchValue,
            ref cTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Queue<entClaPlantilla> ObtenerCola(entClaPlantilla.Fields searchField, object searchValue,
            string strParamAdicionales, ref cTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<entClaPlantilla> ObtenerPila()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerPila(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<entClaPlantilla> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<entClaPlantilla> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + entClaPlantilla.strNombreTabla,
                    arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearPila(table);
                }
                else
                    return new Stack<entClaPlantilla>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<entClaPlantilla> ObtenerPila(entClaPlantilla.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<entClaPlantilla> ObtenerPila(entClaPlantilla.Fields searchField, object searchValue,
            string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<entClaPlantilla> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            ref cTrans localTrans)
        {
            return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<entClaPlantilla> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + entClaPlantilla.strNombreTabla,
                    arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearPila(table);
                }
                else
                    return new Stack<entClaPlantilla>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<entClaPlantilla> ObtenerPila(entClaPlantilla.Fields searchField, object searchValue,
            ref cTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<entClaPlantilla> ObtenerPila(entClaPlantilla.Fields searchField, object searchValue,
            string strParamAdicionales, ref cTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaPlantilla> ObtenerDiccionario()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaPlantilla> ObtenerDiccionario(ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere)
        {
            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaPlantilla> ObtenerDiccionario(ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + entClaPlantilla.strNombreTabla,
                    arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, entClaPlantilla>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaPlantilla> ObtenerDiccionario(entClaPlantilla.Fields searchField,
            object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaPlantilla> ObtenerDiccionario(entClaPlantilla.Fields searchField,
            object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaPlantilla> ObtenerDiccionario(ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, ref cTrans localTrans)
        {
            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaPlantilla> ObtenerDiccionario(ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + entClaPlantilla.strNombreTabla,
                    arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, entClaPlantilla>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaPlantilla> ObtenerDiccionario(entClaPlantilla.Fields searchField,
            object searchValue, ref cTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase entClaPlantilla a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, entClaPlantilla> ObtenerDiccionario(entClaPlantilla.Fields searchField,
            object searchValue, string strParamAdicionales, ref cTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	 Funcion que obtiene la llave primaria unica de la tabla ClaPlantilla a partir de una cadena
        /// </summary>
        /// <param name="args" type="string[]">
        ///     <para>
        /// 		 Cadena desde la que se construye el identificador unico de la tabla ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Identificador unico de la tabla ClaPlantilla
        /// </returns>
        public string CreatePK(string[] args)
        {
            return args[0];
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla ClaPlantilla a partir de una clase del tipo EClaPlantilla
        /// </summary>
        /// <param name="obj" type="Entidades.entClaPlantilla">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionClaPlantilla
        /// </returns>
        public bool Insert(entClaPlantilla obj)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.descPlantilla == null ? null : "'" + obj.descPlantilla + "'");
                arrValores.Add(obj.obsPlantilla == null ? null : "'" + obj.obsPlantilla + "'");
                arrValores.Add(obj.dbms == null ? null : "'" + obj.dbms + "'");
                arrValores.Add(obj.servidor == null ? null : "'" + obj.servidor + "'");
                arrValores.Add(obj.seguridadint);
                arrValores.Add(obj.servicio == null ? null : "'" + obj.servicio + "'");
                arrValores.Add(obj.usuario == null ? null : "'" + obj.usuario + "'");
                arrValores.Add(obj.password == null ? null : "'" + obj.password + "'");
                arrValores.Add(obj.catalogo == null ? null : "'" + obj.catalogo + "'");

                cConn local = new cConn();
                return local.insertBD(entClaPlantilla.strNombreTabla, arrColumnas, arrValores);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla ClaPlantilla a partir de una clase del tipo EClaPlantilla
        /// </summary>
        /// <param name="obj" type="Entidades.entClaPlantilla">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionClaPlantilla
        /// </returns>
        public bool Insert(entClaPlantilla obj, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.descPlantilla == null ? null : "'" + obj.descPlantilla + "'");
                arrValores.Add(obj.obsPlantilla == null ? null : "'" + obj.obsPlantilla + "'");
                arrValores.Add(obj.dbms == null ? null : "'" + obj.dbms + "'");
                arrValores.Add(obj.servidor == null ? null : "'" + obj.servidor + "'");
                arrValores.Add(obj.seguridadint);
                arrValores.Add(obj.servicio == null ? null : "'" + obj.servicio + "'");
                arrValores.Add(obj.usuario == null ? null : "'" + obj.usuario + "'");
                arrValores.Add(obj.password == null ? null : "'" + obj.password + "'");
                arrValores.Add(obj.catalogo == null ? null : "'" + obj.catalogo + "'");

                cConn local = new cConn();
                return local.insertBD(entClaPlantilla.strNombreTabla, arrColumnas, arrValores, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla ClaPlantilla a partir de una clase del tipo EClaPlantilla
        /// </summary>
        /// <param name="obj" type="Entidades.entClaPlantilla">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionClaPlantilla
        /// </returns>
        public bool InsertIdentity(entClaPlantilla obj)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.idPlantilla);
                arrValores.Add(obj.descPlantilla == null ? null : "'" + obj.descPlantilla + "'");
                arrValores.Add(obj.obsPlantilla == null ? null : "'" + obj.obsPlantilla + "'");
                arrValores.Add(obj.dbms == null ? null : "'" + obj.dbms + "'");
                arrValores.Add(obj.servidor == null ? null : "'" + obj.servidor + "'");
                arrValores.Add(obj.seguridadint);
                arrValores.Add(obj.servicio == null ? null : "'" + obj.servicio + "'");
                arrValores.Add(obj.usuario == null ? null : "'" + obj.usuario + "'");
                arrValores.Add(obj.password == null ? null : "'" + obj.password + "'");
                arrValores.Add(obj.catalogo == null ? null : "'" + obj.catalogo + "'");

                cConn local = new cConn();
                int intIdentidad = -1;
                bool res = local.insertBD(entClaPlantilla.strNombreTabla, arrColumnas, arrValores, ref intIdentidad);
                obj.idPlantilla = intIdentidad;
                return res;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla ClaPlantilla a partir de una clase del tipo EClaPlantilla
        /// </summary>
        /// <param name="obj" type="Entidades.entClaPlantilla">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionClaPlantilla
        /// </returns>
        public bool InsertIdentity(entClaPlantilla obj, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.idPlantilla);
                arrValores.Add(obj.descPlantilla == null ? null : "'" + obj.descPlantilla + "'");
                arrValores.Add(obj.obsPlantilla == null ? null : "'" + obj.obsPlantilla + "'");
                arrValores.Add(obj.dbms == null ? null : "'" + obj.dbms + "'");
                arrValores.Add(obj.servidor == null ? null : "'" + obj.servidor + "'");
                arrValores.Add(obj.seguridadint);
                arrValores.Add(obj.servicio == null ? null : "'" + obj.servicio + "'");
                arrValores.Add(obj.usuario == null ? null : "'" + obj.usuario + "'");
                arrValores.Add(obj.password == null ? null : "'" + obj.password + "'");
                arrValores.Add(obj.catalogo == null ? null : "'" + obj.catalogo + "'");

                cConn local = new cConn();
                int intIdentidad = -1;
                bool res = local.insertBD(entClaPlantilla.strNombreTabla, arrColumnas, arrValores, ref intIdentidad,
                    ref localTrans);
                obj.idPlantilla = intIdentidad;
                return res;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla ClaPlantilla a partir de una clase del tipo EClaPlantilla
        /// </summary>
        /// <param name="obj" type="Entidades.entClaPlantilla">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionClaPlantilla
        /// </returns>
        public int Update(entClaPlantilla obj)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.descPlantilla == null ? null : "'" + obj.descPlantilla + "'");
                arrValores.Add(obj.obsPlantilla == null ? null : "'" + obj.obsPlantilla + "'");
                arrValores.Add(obj.dbms == null ? null : "'" + obj.dbms + "'");
                arrValores.Add(obj.servidor == null ? null : "'" + obj.servidor + "'");
                arrValores.Add(obj.seguridadint);
                arrValores.Add(obj.servicio == null ? null : "'" + obj.servicio + "'");
                arrValores.Add(obj.usuario == null ? null : "'" + obj.usuario + "'");
                arrValores.Add(obj.password == null ? null : "'" + obj.password + "'");
                arrValores.Add(obj.catalogo == null ? null : "'" + obj.catalogo + "'");

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(entClaPlantilla.Fields.idPlantilla.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.idPlantilla);

                cConn local = new cConn();
                return local.updateBD(entClaPlantilla.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere,
                    arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla ClaPlantilla a partir de una clase del tipo eClaPlantilla
        /// </summary>
        /// <param name="obj" type="Entidades.entClaPlantilla">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla ClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacion
        /// </returns>
        public int Update(entClaPlantilla obj, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.descPlantilla == null ? null : "'" + obj.descPlantilla + "'");
                arrValores.Add(obj.obsPlantilla == null ? null : "'" + obj.obsPlantilla + "'");
                arrValores.Add(obj.dbms == null ? null : "'" + obj.dbms + "'");
                arrValores.Add(obj.servidor == null ? null : "'" + obj.servidor + "'");
                arrValores.Add(obj.seguridadint);
                arrValores.Add(obj.servicio == null ? null : "'" + obj.servicio + "'");
                arrValores.Add(obj.usuario == null ? null : "'" + obj.usuario + "'");
                arrValores.Add(obj.password == null ? null : "'" + obj.password + "'");
                arrValores.Add(obj.catalogo == null ? null : "'" + obj.catalogo + "'");

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(entClaPlantilla.Fields.idPlantilla.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.idPlantilla);

                cConn local = new cConn();
                return local.updateBD(entClaPlantilla.strNombreTabla, arrColumnas, arrValores, arrColumnasWhere,
                    arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla ClaPlantilla a partir de una clase del tipo entClaPlantilla y su respectiva PK
        /// </summary>
        /// <param name="obj" type="Entidades.entClaPlantilla">
        ///     <para>
        /// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionClaPlantilla
        /// </returns>
        public int Delete(entClaPlantilla obj)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(entClaPlantilla.Fields.idPlantilla.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.idPlantilla);

                cConn local = new cConn();
                return local.deleteBD(entClaPlantilla.strNombreTabla, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla ClaPlantilla a partir de una clase del tipo entClaPlantilla y su PK
        /// </summary>
        /// <param name="obj" type="Entidades.eClaPlantilla">
        ///     <para>
        /// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla ClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacionClaPlantilla
        /// </returns>
        public int Delete(entClaPlantilla obj, ref cTrans localTrans)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(entClaPlantilla.Fields.idPlantilla.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.idPlantilla);

                cConn local = new cConn();
                return local.deleteBD(entClaPlantilla.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla ClaPlantilla a partir de una clase del tipo eClaPlantilla
        /// </summary>
        /// <param name="arrColumnasWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Valores para cada una de las columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionClaPlantilla
        /// </returns>
        public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                cConn local = new cConn();
                return local.deleteBD("ClaPlantilla", arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla ClaPlantilla a partir de una clase del tipo eClaPlantilla
        /// </summary>
        /// <param name="arrColumnasWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Valores para cada una de las columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="Clases.Conexion.cTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion ClaPlantilla
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacionClaPlantilla
        /// </returns>
        public int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans)
        {
            try
            {
                cConn local = new cConn();
                return local.deleteBD(entClaPlantilla.strNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla ClaPlantilla
        /// </summary>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaPlantilla
        /// </returns>
        public DataTable NuevoDataTable()
        {
            try
            {
                DataTable table = new DataTable();
                DataColumn dc;
                dc = new DataColumn(entClaPlantilla.Fields.idPlantilla.ToString(),
                    typeof(entClaPlantilla).GetProperty(entClaPlantilla.Fields.idPlantilla.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(entClaPlantilla.Fields.descPlantilla.ToString(),
                    typeof(entClaPlantilla).GetProperty(entClaPlantilla.Fields.descPlantilla.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(entClaPlantilla.Fields.obsPlantilla.ToString(),
                    typeof(entClaPlantilla).GetProperty(entClaPlantilla.Fields.obsPlantilla.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(entClaPlantilla.Fields.dbms.ToString(),
                    typeof(entClaPlantilla).GetProperty(entClaPlantilla.Fields.dbms.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(entClaPlantilla.Fields.servidor.ToString(),
                    typeof(entClaPlantilla).GetProperty(entClaPlantilla.Fields.servidor.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(entClaPlantilla.Fields.seguridadint.ToString(),
                    typeof(entClaPlantilla).GetProperty(entClaPlantilla.Fields.seguridadint.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(entClaPlantilla.Fields.servicio.ToString(),
                    typeof(entClaPlantilla).GetProperty(entClaPlantilla.Fields.servicio.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(entClaPlantilla.Fields.usuario.ToString(),
                    typeof(entClaPlantilla).GetProperty(entClaPlantilla.Fields.usuario.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(entClaPlantilla.Fields.password.ToString(),
                    typeof(entClaPlantilla).GetProperty(entClaPlantilla.Fields.password.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(entClaPlantilla.Fields.catalogo.ToString(),
                    typeof(entClaPlantilla).GetProperty(entClaPlantilla.Fields.catalogo.ToString()).PropertyType);
                table.Columns.Add(dc);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que genera un DataTable con determinadas columnas de una ClaPlantilla
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaPlantilla
        /// </returns>
        public DataTable NuevoDataTable(ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'2'");

                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + entClaPlantilla.strNombreTabla,
                    arrColumnas, arrColumnasWhere, arrValoresWhere);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla ClaPlantilla
        /// </summary>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaPlantilla
        /// </returns>
        public DataTable CargarDataTable()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return CargarDataTable(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE ClaPlantilla
        /// </summary>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaPlantilla
        /// </returns>
        public DataTable CargarDataTable(String strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return CargarDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaPlantilla
        /// </returns>
        public DataTable CargarDataTable(ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaPlantilla
        /// </returns>
        public DataTable CargarDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaPlantilla
        /// </returns>
        public DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaPlantilla
        /// </returns>
        public DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return CargarDataTable(arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaPlantilla
        /// </returns>
        public DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParametrosAdicionales)
        {
            try
            {
                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + entClaPlantilla.strNombreTabla,
                    arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaPlantilla
        /// </returns>
        public DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable CargarDataTable(entClaPlantilla.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return CargarDataTable(arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable CargarDataTable(entClaPlantilla.Fields searchField, object searchValue,
            string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return CargarDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable CargarDataTable(ArrayList arrColumnas, entClaPlantilla.Fields searchField, object searchValue,
            string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable CargarDataTable(ArrayList arrColumnas, entClaPlantilla.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaPlantilla
        /// </returns>
        public DataTable CargarDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());
                return CargarDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaPlantilla
        /// </returns>
        public DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return CargarDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de ClaPlantilla
        /// </returns>
        public DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParametrosAdicionales)
        {
            try
            {
                cConn local = new cConn();
                DataTable table = local.cargarDataTableOr(cParametros.schema + entClaPlantilla.strNombreTabla,
                    arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, entClaPlantilla.Fields textField)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, entClaPlantilla.Fields textField,
            string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, String textField)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, String textField,
            String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, entClaPlantilla.Fields textField,
            entClaPlantilla.Fields searchField, object searchValue)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, String textField,
            entClaPlantilla.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, entClaPlantilla.Fields textField,
            entClaPlantilla.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, String textField,
            entClaPlantilla.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, entClaPlantilla.Fields textField,
            ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, String textField,
            ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, entClaPlantilla.Fields textField,
            ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="valueField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, String textField,
            ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla ClaPlantilla
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla ClaPlantillaClaPlantilla
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                cConn local = new cConn();
                DataTable table = local.cargarDataTableAnd(cParametros.schema + entClaPlantilla.strNombreTabla,
                    arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                    cmb.DataSource = table;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere)
        {
            try
            {
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                cConn local = new cConn();
                DbDataReader dsReader = local.cargarDataReaderAnd(cParametros.schema + entClaPlantilla.strNombreTabla,
                    arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, entClaPlantilla.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, entClaPlantilla.Fields searchField, object searchValue,
            String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(entClaPlantilla.Fields.idPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.descPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.obsPlantilla.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.dbms.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servidor.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.seguridadint.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.servicio.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.usuario.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.password.ToString());
                arrColumnas.Add(entClaPlantilla.Fields.catalogo.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entClaPlantilla.Fields searchField,
            object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entClaPlantilla.Fields searchField,
            object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere)
        {
            try
            {
                CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla ClaPlantilla
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                cConn local = new cConn();
                DbDataReader dsReader = local.cargarDataReaderOr(cParametros.schema + entClaPlantilla.strNombreTabla,
                    arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesCount(entClaPlantilla.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesCount(entClaPlantilla.Fields refField, entClaPlantilla.Fields whereField,
            object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesCount(entClaPlantilla.Fields refField, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("count(" + refField + ")");
                DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                return dtTemp.Rows[0][0];
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesMin(entClaPlantilla.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesMin(entClaPlantilla.Fields refField, entClaPlantilla.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesMin(entClaPlantilla.Fields refField, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("min(" + refField + ")");
                DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                return dtTemp.Rows[0][0];
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesMax(entClaPlantilla.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesMax(entClaPlantilla.Fields refField, entClaPlantilla.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesMax(entClaPlantilla.Fields refField, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("max(" + refField + ")");
                DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                return dtTemp.Rows[0][0];
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesSum(entClaPlantilla.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesSum(entClaPlantilla.Fields refField, entClaPlantilla.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesSum(entClaPlantilla.Fields refField, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("sum(" + refField + ")");
                DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                return dtTemp.Rows[0][0];
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesAvg(entClaPlantilla.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesAvg(entClaPlantilla.Fields refField, entClaPlantilla.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="entClaPlantilla.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo entClaPlantilla que cumple con los filtros de los parametros
        /// </returns>
        public object FuncionesAvg(entClaPlantilla.Fields refField, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("avg(" + refField + ")");
                DataTable dtTemp = CargarDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                return dtTemp.Rows[0][0];
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        /// <summary>
        /// 	 Funcion que devuelve un objeto a partir de un DataRow
        /// </summary>
        /// <param name="row" type="System.Data.DataRow">
        ///     <para>
        /// 		 DataRow con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Objeto ClaPlantilla
        /// </returns>
        internal entClaPlantilla crearObjeto(DataRow row)
        {
            entClaPlantilla obj = new entClaPlantilla();
            obj.idPlantilla = int.Parse(row[entClaPlantilla.Fields.idPlantilla.ToString()].ToString());
            obj.descPlantilla = row[entClaPlantilla.Fields.descPlantilla.ToString()].ToString();
            obj.obsPlantilla = row[entClaPlantilla.Fields.obsPlantilla.ToString()].ToString();
            obj.dbms = row[entClaPlantilla.Fields.dbms.ToString()].ToString();
            obj.servidor = row[entClaPlantilla.Fields.servidor.ToString()].ToString();
            obj.seguridadint = int.Parse(row[entClaPlantilla.Fields.seguridadint.ToString()].ToString());
            obj.servicio = row[entClaPlantilla.Fields.servicio.ToString()].ToString();
            obj.usuario = row[entClaPlantilla.Fields.usuario.ToString()].ToString();
            obj.password = row[entClaPlantilla.Fields.password.ToString()].ToString();
            obj.catalogo = row[entClaPlantilla.Fields.catalogo.ToString()].ToString();

            obj.EntityState = EntityState.Unchanged;
            return obj;
        }

        /// <summary>
        /// 	 Funcion que devuelve un objeto a partir de un DataRow
        /// </summary>
        /// <param name="row" type="System.Data.DataRow">
        ///     <para>
        /// 		 DataRow con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Objeto ClaPlantilla
        /// </returns>
        internal entClaPlantilla crearObjetoRevisado(DataRow row)
        {
            entClaPlantilla obj = new entClaPlantilla();
            if (row.Table.Columns.Contains(entClaPlantilla.Fields.idPlantilla.ToString()))
                obj.idPlantilla = int.Parse(row[entClaPlantilla.Fields.idPlantilla.ToString()].ToString());
            if (row.Table.Columns.Contains(entClaPlantilla.Fields.descPlantilla.ToString()))
                obj.descPlantilla = row[entClaPlantilla.Fields.descPlantilla.ToString()].ToString();
            if (row.Table.Columns.Contains(entClaPlantilla.Fields.obsPlantilla.ToString()))
                obj.obsPlantilla = row[entClaPlantilla.Fields.obsPlantilla.ToString()].ToString();
            if (row.Table.Columns.Contains(entClaPlantilla.Fields.dbms.ToString()))
                obj.dbms = row[entClaPlantilla.Fields.dbms.ToString()].ToString();
            if (row.Table.Columns.Contains(entClaPlantilla.Fields.servidor.ToString()))
                obj.servidor = row[entClaPlantilla.Fields.servidor.ToString()].ToString();
            if (row.Table.Columns.Contains(entClaPlantilla.Fields.seguridadint.ToString()))
                obj.seguridadint = int.Parse(row[entClaPlantilla.Fields.seguridadint.ToString()].ToString());
            if (row.Table.Columns.Contains(entClaPlantilla.Fields.servicio.ToString()))
                obj.servicio = row[entClaPlantilla.Fields.servicio.ToString()].ToString();
            if (row.Table.Columns.Contains(entClaPlantilla.Fields.usuario.ToString()))
                obj.usuario = row[entClaPlantilla.Fields.usuario.ToString()].ToString();
            if (row.Table.Columns.Contains(entClaPlantilla.Fields.password.ToString()))
                obj.password = row[entClaPlantilla.Fields.password.ToString()].ToString();
            if (row.Table.Columns.Contains(entClaPlantilla.Fields.catalogo.ToString()))
                obj.catalogo = row[entClaPlantilla.Fields.catalogo.ToString()].ToString();

            obj.EntityState = EntityState.Unchanged;
            return obj;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtClaPlantilla" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Lista de Objetos ClaPlantilla
        /// </returns>
        internal List<entClaPlantilla> crearLista(DataTable dtClaPlantilla)
        {
            List<entClaPlantilla> list = new List<entClaPlantilla>();

            foreach (DataRow row in dtClaPlantilla.Rows)
            {
                var obj = crearObjeto(row);
                list.Add(obj);
            }
            return list;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
        /// </summary>
        /// <param name="dtClaPlantilla" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Lista de Objetos ClaPlantilla
        /// </returns>
        internal List<entClaPlantilla> crearListaRevisada(DataTable dtClaPlantilla)
        {
            List<entClaPlantilla> list = new List<entClaPlantilla>();

            entClaPlantilla obj = new entClaPlantilla();
            foreach (DataRow row in dtClaPlantilla.Rows)
            {
                obj = crearObjetoRevisado(row);
                list.Add(obj);
            }
            return list;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtClaPlantilla" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cola de Objetos ClaPlantilla
        /// </returns>
        internal Queue<entClaPlantilla> crearCola(DataTable dtClaPlantilla)
        {
            Queue<entClaPlantilla> cola = new Queue<entClaPlantilla>();

            entClaPlantilla obj = new entClaPlantilla();
            foreach (DataRow row in dtClaPlantilla.Rows)
            {
                obj = crearObjeto(row);
                cola.Enqueue(obj);
            }
            return cola;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtClaPlantilla" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Pila de Objetos ClaPlantilla
        /// </returns>
        internal Stack<entClaPlantilla> crearPila(DataTable dtClaPlantilla)
        {
            Stack<entClaPlantilla> pila = new Stack<entClaPlantilla>();

            entClaPlantilla obj = new entClaPlantilla();
            foreach (DataRow row in dtClaPlantilla.Rows)
            {
                obj = crearObjeto(row);
                pila.Push(obj);
            }
            return pila;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable
        /// </summary>
        /// <param name="dtClaPlantilla" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Diccionario de Objetos ClaPlantilla
        /// </returns>
        internal Dictionary<String, entClaPlantilla> crearDiccionario(DataTable dtClaPlantilla)
        {
            Dictionary<String, entClaPlantilla> miDic = new Dictionary<String, entClaPlantilla>();

            entClaPlantilla obj = new entClaPlantilla();
            foreach (DataRow row in dtClaPlantilla.Rows)
            {
                obj = crearObjeto(row);
                miDic.Add(obj.idPlantilla.ToString(), obj);
            }
            return miDic;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
        /// </summary>
        /// <param name="dtClaPlantilla" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Diccionario de Objetos ClaPlantilla
        /// </returns>
        internal Dictionary<String, entClaPlantilla> crearDiccionarioRevisado(DataTable dtClaPlantilla)
        {
            Dictionary<string, entClaPlantilla> miDic = new Dictionary<string, entClaPlantilla>();

            foreach (DataRow row in dtClaPlantilla.Rows)
            {
                entClaPlantilla obj = crearObjetoRevisado(row);
                miDic.Add(obj.idPlantilla.ToString(), obj);
            }
            return miDic;
        }

        public void Dispose() => GC.SuppressFinalize(this);

        protected void Finalize() => Dispose();
    }
}