#region

/***********************************************************************************************************
	NOMBRE:       entClaPlantilla
	DESCRIPCION:
		Clase que define un objeto para la Tabla ClaPlantilla

	REVISIONES:
		Ver        FECHA       Autor            Descripcion
		---------  ----------  ---------------  ------------------------------------
		1.0        01/06/2015  R Alonzo Vera A  Creacion

*************************************************************************************************************/

#endregion

#region librerias

using System;
using System.Data;
using System.Data.Objects.DataClasses;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;

#endregion

namespace ReAl.Class.Entidades
{
    public class entClaPlantilla : cBaseClass
    {
        public const String strNombreTabla = "ClaPlantilla";
        public const String strAliasTabla = "ClaPlantilla";

        public enum Fields
        {
            idPlantilla
            ,
            descPlantilla
            ,
            obsPlantilla
            ,
            dbms
            ,
            servidor
            ,
            seguridadint
            ,
            servicio
            ,
            usuario
            ,
            password
            ,
            catalogo
        }

        #region Constructoress

        public entClaPlantilla()
        {
            //Inicializacion de Variables
            this.descPlantilla = null;
            this.obsPlantilla = null;
            this.dbms = null;
            this.servidor = null;
            this.seguridadint = null;
            this.servicio = null;
            this.usuario = null;
            this.password = null;
            this.catalogo = null;
        }

        public entClaPlantilla(entClaPlantilla obj)
        {
            this.idPlantilla = obj.idPlantilla;
            this.descPlantilla = obj.descPlantilla;
            this.obsPlantilla = obj.obsPlantilla;
            this.dbms = obj.dbms;
            this.servidor = obj.servidor;
            this.seguridadint = obj.seguridadint;
            this.servicio = obj.servicio;
            this.usuario = obj.usuario;
            this.password = obj.password;
            this.catalogo = obj.catalogo;
        }

        #endregion

        #region Metodos Privados

        /// <summary>
        /// Obtiene el Hash a partir de un array de Bytes
        /// </summary>
        /// <param name="objectAsBytes"></param>
        /// <returns>string</returns>
        private string ComputeHash(byte[] objectAsBytes)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            try
            {
                byte[] result = md5.ComputeHash(objectAsBytes);

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < result.Length; i++)
                {
                    sb.Append(result[i].ToString("X2"));
                }

                return sb.ToString();
            }
            catch (ArgumentNullException)
            {
                return null;
            }
        }

        /// <summary>
        ///     Obtienen el Hash basado en algun algoritmo de Encriptación
        /// </summary>
        /// <typeparam name="T">
        ///     Algoritmo de encriptación
        /// </typeparam>
        /// <param name="cryptoServiceProvider">
        ///     Provvedor de Servicios de Criptografía
        /// </param>
        /// <returns>
        ///     String que representa el Hash calculado
        /// </returns>
        private string computeHash<T>(T cryptoServiceProvider) where T : HashAlgorithm, new()
        {
            DataContractSerializer serializer = new DataContractSerializer(this.GetType());
            using (MemoryStream memoryStream = new MemoryStream())
            {
                serializer.WriteObject(memoryStream, this);
                cryptoServiceProvider.ComputeHash(memoryStream.ToArray());
                return Convert.ToBase64String(cryptoServiceProvider.Hash);
            }
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Devuelve un String que representa al Objeto
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            string hashString;

            //Evitar parametros NULL
            if (this == null)
                throw new ArgumentNullException("Parametro NULL no es valido");

            //Se verifica si el objeto es serializable.
            try
            {
                MemoryStream memStream = new MemoryStream();
                XmlSerializer serializer = new XmlSerializer(typeof(entClaPlantilla));
                serializer.Serialize(memStream, this);

                //Ahora se obtiene el Hash del Objeto.
                hashString = ComputeHash(memStream.ToArray());

                return hashString;
            }
            catch (AmbiguousMatchException ame)
            {
                throw new ApplicationException("El Objeto no es Serializable. Message:" + ame.Message);
            }
        }

        /// <summary>
        /// Verifica que dos objetos sean identicos
        /// </summary>
        public static bool operator ==(entClaPlantilla first, entClaPlantilla second)
        {
            // Verifica si el puntero en memoria es el mismo
            if (Object.ReferenceEquals(first, second))
                return true;

            // Verifica valores nulos
            if ((object)first == null || (object)second == null)
                return false;

            return first.GetHashCode() == second.GetHashCode();
        }

        /// <summary>
        /// Verifica que dos objetos sean distintos
        /// </summary>
        public static bool operator !=(entClaPlantilla first, entClaPlantilla second)
        {
            return !(first == second);
        }

        /// <summary>
        /// Compara este objeto con otro
        /// </summary>
        /// <param name="obj">El objeto a comparar</param>
        /// <returns>Devuelve Verdadero si ambos objetos son iguales</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (obj.GetType() == this.GetType())
                return obj.GetHashCode() == this.GetHashCode();

            return false;
        }

        #endregion

        #region Encriptacion

        /// <summary>
        /// Un valor unico que identifica cada instancia de este objeto
        /// </summary>
        /// <returns>Unico valor entero</returns>
        public override int GetHashCode()
        {
            return (this.GetSHA512Hash().GetHashCode());
        }

        /// <summary>
        ///     Obtiene el Hash de la Instancia Actual
        /// </summary>
        /// <typeparam name="T">
        ///     Tipo de Proveedor de servicios criptográficos
        /// </typeparam>
        /// <returns>
        ///     String en Base64 que representa el Hash
        /// </returns>
        public string GetHash<T>() where T : HashAlgorithm, new()
        {
            T cryptoServiceProvider = new T();
            return computeHash(cryptoServiceProvider);
        }

        /// <summary>
        ///     Obtiene el codigo Hash en Base al Algoritmo SHA1
        /// </summary>
        /// <returns>
        ///     String en Base64 que representa el Hash en SHA1
        /// </returns>
        public string GetSHA1Hash()
        {
            return GetHash<SHA1CryptoServiceProvider>();
        }

        /// <summary>
        ///     Obtiene el codigo Hash en Base al Algoritmo SHA512
        /// </summary>
        /// <returns>
        ///     String en Base64 que representa el Hash en SHA512
        /// </returns>
        public string GetSHA512Hash()
        {
            return GetHash<SHA512CryptoServiceProvider>();
        }

        /// <summary>
        ///     Obtiene el codigo Hash en Base al Algoritmo MD5
        /// </summary>
        /// <returns>
        ///     String en Base64 que representa el Hash en MD5
        /// </returns>
        public string GetMD5Hash()
        {
            return GetHash<MD5CryptoServiceProvider>();
        }

        public void EntityMemberChanging(string entityMemberName)
        {
            throw new NotImplementedException();
        }

        public void EntityMemberChanged(string entityMemberName)
        {
            throw new NotImplementedException();
        }

        public void EntityComplexMemberChanging(string entityMemberName, object complexObject, string complexObjectMemberName)
        {
            throw new NotImplementedException();
        }

        public void EntityComplexMemberChanged(string entityMemberName, object complexObject, string complexObjectMemberName)
        {
            throw new NotImplementedException();
        }

        #endregion

        /// <summary>
        /// 	 Columna de tipo integer de la tabla ClaPlantilla
        /// 	 Permite Null: No
        /// 	 Es Calculada: No
        /// 	 Es RowGui: No
        /// 	 Es PrimaryKey: Yes
        /// 	 Es ForeignKey: No
        /// </summary>
        public int idPlantilla { get; set; }

        /// <summary>
        ///
        /// </summary>
        private string _descPlantilla;

        /// <summary>
        /// 	 Columna de tipo nvarchar(50) de la tabla ClaPlantilla
        /// 	 Permite Null: No
        /// 	 Es Calculada: No
        /// 	 Es RowGui: No
        /// 	 Es PrimaryKey: No
        /// 	 Es ForeignKey: No
        /// </summary>
        public String descPlantilla
        {
            get
            {
                return _descPlantilla;
            }
            set
            {
                if (value != this._descPlantilla)
                {
                    RaisePropertyChanging(entClaPlantilla.Fields.descPlantilla.ToString());
                    _descPlantilla = value;
                    RaisePropertyChanged(entClaPlantilla.Fields.descPlantilla.ToString());
                }
            }
        }

        /// <summary>
        /// 	 Columna de tipo nvarchar(50) de la tabla ClaPlantilla
        /// 	 Permite Null: No
        /// 	 Es Calculada: No
        /// 	 Es RowGui: No
        /// 	 Es PrimaryKey: No
        /// 	 Es ForeignKey: No
        /// </summary>
        public String obsPlantilla { get; set; }

        /// <summary>
        /// 	 Columna de tipo nvarchar(50) de la tabla ClaPlantilla
        /// 	 Permite Null: Yes
        /// 	 Es Calculada: No
        /// 	 Es RowGui: No
        /// 	 Es PrimaryKey: No
        /// 	 Es ForeignKey: No
        /// </summary>
        public String dbms { get; set; }

        /// <summary>
        /// 	 Columna de tipo nvarchar(50) de la tabla ClaPlantilla
        /// 	 Permite Null: Yes
        /// 	 Es Calculada: No
        /// 	 Es RowGui: No
        /// 	 Es PrimaryKey: No
        /// 	 Es ForeignKey: No
        /// </summary>
        public String servidor { get; set; }

        /// <summary>
        /// 	 Columna de tipo int de la tabla ClaPlantilla
        /// 	 Permite Null: Yes
        /// 	 Es Calculada: No
        /// 	 Es RowGui: No
        /// 	 Es PrimaryKey: No
        /// 	 Es ForeignKey: No
        /// </summary>
        public int? seguridadint { get; set; }

        /// <summary>
        /// 	 Columna de tipo nvarchar(50) de la tabla ClaPlantilla
        /// 	 Permite Null: Yes
        /// 	 Es Calculada: No
        /// 	 Es RowGui: No
        /// 	 Es PrimaryKey: No
        /// 	 Es ForeignKey: No
        /// </summary>
        public String servicio { get; set; }

        /// <summary>
        /// 	 Columna de tipo nvarchar(50) de la tabla ClaPlantilla
        /// 	 Permite Null: Yes
        /// 	 Es Calculada: No
        /// 	 Es RowGui: No
        /// 	 Es PrimaryKey: No
        /// 	 Es ForeignKey: No
        /// </summary>
        public String usuario { get; set; }

        /// <summary>
        /// 	 Columna de tipo nvarchar(50) de la tabla ClaPlantilla
        /// 	 Permite Null: Yes
        /// 	 Es Calculada: No
        /// 	 Es RowGui: No
        /// 	 Es PrimaryKey: No
        /// 	 Es ForeignKey: No
        /// </summary>
        public String password { get; set; }

        /// <summary>
        /// 	 Columna de tipo nvarchar(50) de la tabla ClaPlantilla
        /// 	 Permite Null: Yes
        /// 	 Es Calculada: No
        /// 	 Es RowGui: No
        /// 	 Es PrimaryKey: No
        /// 	 Es ForeignKey: No
        /// </summary>
        public String catalogo { get; set; }
    }
}