﻿#region

/***********************************************************************************************************
	NOMBRE:       inClaDetalle
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla ClaDetalle

	REVISIONES:
		Ver        FECHA       Autor            Descripción 
		---------  ----------  ---------------  ------------------------------------
		1.0        15/08/2013  R Alonzo Vera A  Creación 

*************************************************************************************************************/

#endregion

#region librerias

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using ReAl.Class.Entidades;
using ReAl.Conn;

#endregion

namespace ReAl.Class.Interface
{
    public interface inClaDetalle : IDisposable
    {
        string GetDesc(entClaDetalle.Fields myField);
        string GetError(entClaDetalle.Fields myField);
        string GetColumn(entClaDetalle.Fields myField);
        entClaDetalle ObtenerObjeto(int intidDet);
        entClaDetalle ObtenerObjeto(int intidDet, ref cTrans localTrans);
        entClaDetalle ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        entClaDetalle ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
        entClaDetalle ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);

        entClaDetalle ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,
            ref cTrans localTrans);

        entClaDetalle ObtenerObjeto(entClaDetalle.Fields searchField, object searchValue);
        entClaDetalle ObtenerObjeto(entClaDetalle.Fields searchField, object searchValue, ref cTrans localTrans);
        entClaDetalle ObtenerObjeto(entClaDetalle.Fields searchField, object searchValue, string strParamAdicionales);

        entClaDetalle ObtenerObjeto(entClaDetalle.Fields searchField, object searchValue, string strParamAdicionales,
            ref cTrans localTrans);

        Dictionary<String, entClaDetalle> ObtenerDiccionario();
        Dictionary<String, entClaDetalle> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);

        Dictionary<String, entClaDetalle> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            ref cTrans localTrans);

        Dictionary<String, entClaDetalle> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales);

        Dictionary<String, entClaDetalle> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales, ref cTrans localTrans);

        Dictionary<String, entClaDetalle> ObtenerDiccionario(entClaDetalle.Fields searchField, object searchValue);

        Dictionary<String, entClaDetalle> ObtenerDiccionario(entClaDetalle.Fields searchField, object searchValue,
            ref cTrans localTrans);

        Dictionary<String, entClaDetalle> ObtenerDiccionario(entClaDetalle.Fields searchField, object searchValue,
            string strParamAdicionales);

        Dictionary<String, entClaDetalle> ObtenerDiccionario(entClaDetalle.Fields searchField, object searchValue,
            string strParamAdicionales, ref cTrans localTrans);

        Dictionary<String, entClaDetalle> ObtenerDiccionario(ArrayList arrColumnas);
        Dictionary<String, entClaDetalle> ObtenerDiccionario(ArrayList arrColumnas, string strParamAdicionales);
        Dictionary<String, entClaDetalle> ObtenerDiccionario(ArrayList arrColumnas, ref cTrans localTrans);

        Dictionary<String, entClaDetalle> ObtenerDiccionario(ArrayList arrColumnas, string strParamAdicionales,
            ref cTrans localTrans);

        List<entClaDetalle> ObtenerLista();
        List<entClaDetalle> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        List<entClaDetalle> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);

        List<entClaDetalle> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales);

        List<entClaDetalle> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales, ref cTrans localTrans);

        List<entClaDetalle> ObtenerLista(entClaDetalle.Fields searchField, object searchValue);
        List<entClaDetalle> ObtenerLista(entClaDetalle.Fields searchField, object searchValue, ref cTrans localTrans);

        List<entClaDetalle> ObtenerLista(entClaDetalle.Fields searchField, object searchValue,
            string strParamAdicionales);

        List<entClaDetalle> ObtenerLista(entClaDetalle.Fields searchField, object searchValue,
            string strParamAdicionales, ref cTrans localTrans);

        List<entClaDetalle> ObtenerLista(ArrayList arrColumnas);
        List<entClaDetalle> ObtenerLista(ArrayList arrColumnas, string strParamAdicionales);
        List<entClaDetalle> ObtenerLista(ArrayList arrColumnas, ref cTrans localTrans);
        List<entClaDetalle> ObtenerLista(ArrayList arrColumnas, string strParamAdicionales, ref cTrans localTrans);

        string CreatePK(string[] args);

        bool Insert(entClaDetalle obj);
        bool Insert(entClaDetalle obj, ref cTrans localTrans);
        int Update(entClaDetalle obj);
        int Update(entClaDetalle obj, ref cTrans localTrans);
        int Delete(entClaDetalle obj);
        int Delete(entClaDetalle obj, ref cTrans localTrans);
        int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);

        DataTable NuevoDataTable();
        DataTable NuevoDataTable(ArrayList arrColumnas);
        DataTable CargarDataTable();
        DataTable CargarDataTable(String condicionesWhere);
        DataTable CargarDataTable(ArrayList arrColumnas);
        DataTable CargarDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
        DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);

        DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParametrosAdicionales);

        DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
        DataTable CargarDataTable(entClaDetalle.Fields searchField, object searchValue);
        DataTable CargarDataTable(entClaDetalle.Fields searchField, object searchValue, string strParamAdicionales);
        DataTable CargarDataTable(ArrayList arrColumnas, entClaDetalle.Fields searchField, object searchValue);

        DataTable CargarDataTable(ArrayList arrColumnas, entClaDetalle.Fields searchField, object searchValue,
            string strParametrosAdicionales);

        DataTable CargarDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);

        DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParametrosAdicionales);

        void CargarCombo(ref ComboBox cmb);
        void CargarCombo(ref ComboBox cmb, string strParamAdicionales);
        void CargarCombo(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);

        void CargarCombo(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales);

        void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, entClaDetalle.Fields textField);

        void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, entClaDetalle.Fields textField,
            string strParamAdicionales);

        void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, String textField);
        void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, String textField, string strParamAdicionales);

        void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, String textField, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere);

        void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, String textField, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParamAdicionales);

        void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, String textField,
            entClaDetalle.Fields searchField, object searchValue);

        void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, String textField,
            entClaDetalle.Fields searchField, object searchValue, string strParamAdicionales);

        void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, entClaDetalle.Fields textField,
            ArrayList arrColumnasWhere, ArrayList arrValoresWhere);

        void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, entClaDetalle.Fields textField,
            ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);

        void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, entClaDetalle.Fields textField,
            entClaDetalle.Fields searchField, object searchValue);

        void CargarCombo(ref ComboBox cmb, entClaDetalle.Fields valueField, entClaDetalle.Fields textField,
            entClaDetalle.Fields searchField, object searchValue, string strParamAdicionales);

        void CargarGridView(ref DataGridView dtg);
        void CargarGridView(ref DataGridView dtg, String condicionesWhere);
        void CargarGridView(ref DataGridView dtg, ArrayList arrColumnas);
        void CargarGridView(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
        void CargarGridView(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);

        void CargarGridView(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            String condicionesWhere);

        void CargarGridView(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere);

        void CargarGridView(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParametrosAdicionales);

        void CargarGridView(ref DataGridView dtg, entClaDetalle.Fields searchField, object searchValue);

        void CargarGridView(ref DataGridView dtg, entClaDetalle.Fields searchField, object searchValue,
            string strParametrosAdicionales);

        void CargarGridViewOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere);

        void CargarGridViewOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParametrosAdicionales);

        void CargarGridView(ref DataGridView dtg, ArrayList arrColumnas, entClaDetalle.Fields searchField,
            object searchValue);

        void CargarGridView(ref DataGridView dtg, ArrayList arrColumnas, entClaDetalle.Fields searchField,
            object searchValue, string strParametrosAdicionales);

        object FuncionesCount(entClaDetalle.Fields refField);
        object FuncionesCount(entClaDetalle.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        object FuncionesMin(entClaDetalle.Fields refField);
        object FuncionesMin(entClaDetalle.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        object FuncionesMax(entClaDetalle.Fields refField);
        object FuncionesMax(entClaDetalle.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        object FuncionesSum(entClaDetalle.Fields refField);
        object FuncionesSum(entClaDetalle.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        object FuncionesAvg(entClaDetalle.Fields refField);
        object FuncionesAvg(entClaDetalle.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
    }
}