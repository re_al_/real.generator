#region

/***********************************************************************************************************
	NOMBRE:       inClaPlantilla
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla ClaPlantilla

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        01/06/2015  R Alonzo Vera A  Creacion 

*************************************************************************************************************/

#endregion

#region librerias

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using ReAl.Class.Entidades;
using ReAl.Conn;

#endregion

namespace ReAl.Class.Interface
{
    public interface IReglaNegocios : IDisposable
    {
        entClaPlantilla ObtenerObjeto(int intidPlantilla);
        entClaPlantilla ObtenerObjeto(int intidPlantilla, ref cTrans localTrans);
        entClaPlantilla ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        entClaPlantilla ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);
        entClaPlantilla ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);

        entClaPlantilla ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,
            ref cTrans localTrans);

        entClaPlantilla ObtenerObjeto(entClaPlantilla.Fields searchField, object searchValue);
        entClaPlantilla ObtenerObjeto(entClaPlantilla.Fields searchField, object searchValue, ref cTrans localTrans);
        entClaPlantilla ObtenerObjeto(entClaPlantilla.Fields searchField, object searchValue, string strParamAdicionales);

        entClaPlantilla ObtenerObjeto(entClaPlantilla.Fields searchField, object searchValue, string strParamAdicionales,
            ref cTrans localTrans);

        Dictionary<String, entClaPlantilla> ObtenerDiccionario();
        Dictionary<String, entClaPlantilla> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);

        Dictionary<String, entClaPlantilla> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            ref cTrans localTrans);

        Dictionary<String, entClaPlantilla> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales);

        Dictionary<String, entClaPlantilla> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales, ref cTrans localTrans);

        Dictionary<String, entClaPlantilla> ObtenerDiccionario(entClaPlantilla.Fields searchField, object searchValue);

        Dictionary<String, entClaPlantilla> ObtenerDiccionario(entClaPlantilla.Fields searchField, object searchValue,
            ref cTrans localTrans);

        Dictionary<String, entClaPlantilla> ObtenerDiccionario(entClaPlantilla.Fields searchField, object searchValue,
            string strParamAdicionales);

        Dictionary<String, entClaPlantilla> ObtenerDiccionario(entClaPlantilla.Fields searchField, object searchValue,
            string strParamAdicionales, ref cTrans localTrans);

        List<entClaPlantilla> ObtenerLista();
        List<entClaPlantilla> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        List<entClaPlantilla> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);

        List<entClaPlantilla> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales);

        List<entClaPlantilla> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales, ref cTrans localTrans);

        List<entClaPlantilla> ObtenerLista(entClaPlantilla.Fields searchField, object searchValue);
        List<entClaPlantilla> ObtenerLista(entClaPlantilla.Fields searchField, object searchValue, ref cTrans localTrans);

        List<entClaPlantilla> ObtenerLista(entClaPlantilla.Fields searchField, object searchValue,
            string strParamAdicionales);

        List<entClaPlantilla> ObtenerLista(entClaPlantilla.Fields searchField, object searchValue,
            string strParamAdicionales, ref cTrans localTrans);

        List<entClaPlantilla> ObtenerListaDesdeVista(String strVista);

        List<entClaPlantilla> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere);

        List<entClaPlantilla> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, ref cTrans localTrans);

        List<entClaPlantilla> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParamAdicionales);

        List<entClaPlantilla> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParamAdicionales, ref cTrans localTrans);

        List<entClaPlantilla> ObtenerListaDesdeVista(String strVista, entClaPlantilla.Fields searchField,
            object searchValue);

        List<entClaPlantilla> ObtenerListaDesdeVista(String strVista, entClaPlantilla.Fields searchField,
            object searchValue, ref cTrans localTrans);

        List<entClaPlantilla> ObtenerListaDesdeVista(String strVista, entClaPlantilla.Fields searchField,
            object searchValue, string strParamAdicionales);

        List<entClaPlantilla> ObtenerListaDesdeVista(String strVista, entClaPlantilla.Fields searchField,
            object searchValue, string strParamAdicionales, ref cTrans localTrans);

        Queue<entClaPlantilla> ObtenerCola();
        Queue<entClaPlantilla> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        Queue<entClaPlantilla> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);

        Queue<entClaPlantilla> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales);

        Queue<entClaPlantilla> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales, ref cTrans localTrans);

        Queue<entClaPlantilla> ObtenerCola(entClaPlantilla.Fields searchField, object searchValue);
        Queue<entClaPlantilla> ObtenerCola(entClaPlantilla.Fields searchField, object searchValue, ref cTrans localTrans);

        Queue<entClaPlantilla> ObtenerCola(entClaPlantilla.Fields searchField, object searchValue,
            string strParamAdicionales);

        Queue<entClaPlantilla> ObtenerCola(entClaPlantilla.Fields searchField, object searchValue,
            string strParamAdicionales, ref cTrans localTrans);

        Stack<entClaPlantilla> ObtenerPila();
        Stack<entClaPlantilla> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        Stack<entClaPlantilla> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);

        Stack<entClaPlantilla> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales);

        Stack<entClaPlantilla> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales, ref cTrans localTrans);

        Stack<entClaPlantilla> ObtenerPila(entClaPlantilla.Fields searchField, object searchValue);
        Stack<entClaPlantilla> ObtenerPila(entClaPlantilla.Fields searchField, object searchValue, ref cTrans localTrans);

        Stack<entClaPlantilla> ObtenerPila(entClaPlantilla.Fields searchField, object searchValue,
            string strParamAdicionales);

        Stack<entClaPlantilla> ObtenerPila(entClaPlantilla.Fields searchField, object searchValue,
            string strParamAdicionales, ref cTrans localTrans);

        string CreatePK(string[] args);

        bool Insert(entClaPlantilla obj);
        bool Insert(entClaPlantilla obj, ref cTrans localTrans);
        int Update(entClaPlantilla obj);
        int Update(entClaPlantilla obj, ref cTrans localTrans);
        int Delete(entClaPlantilla obj);
        int Delete(entClaPlantilla obj, ref cTrans localTrans);
        int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        int Delete(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref cTrans localTrans);

        DataTable NuevoDataTable();
        DataTable NuevoDataTable(ArrayList arrColumnas);
        DataTable CargarDataTable();
        DataTable CargarDataTable(String condicionesWhere);
        DataTable CargarDataTable(ArrayList arrColumnas);
        DataTable CargarDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
        DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);

        DataTable CargarDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParametrosAdicionales);

        DataTable CargarDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
        DataTable CargarDataTable(entClaPlantilla.Fields searchField, object searchValue);
        DataTable CargarDataTable(entClaPlantilla.Fields searchField, object searchValue, string strParamAdicionales);
        DataTable CargarDataTable(ArrayList arrColumnas, entClaPlantilla.Fields searchField, object searchValue);

        DataTable CargarDataTable(ArrayList arrColumnas, entClaPlantilla.Fields searchField, object searchValue,
            string strParametrosAdicionales);

        DataTable CargarDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);

        DataTable CargarDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParametrosAdicionales);

        void CargarComboBox(ref ComboBox cmb);
        void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
        void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);

        void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            string strParamAdicionales);

        void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, entClaPlantilla.Fields textField);

        void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, entClaPlantilla.Fields textField,
            string strParamAdicionales);

        void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, String textField);

        void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, String textField,
            string strParamAdicionales);

        void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, String textField,
            ArrayList arrColumnasWhere, ArrayList arrValoresWhere);

        void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, String textField,
            ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);

        void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, String textField,
            entClaPlantilla.Fields searchField, object searchValue);

        void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, String textField,
            entClaPlantilla.Fields searchField, object searchValue, string strParamAdicionales);

        void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, entClaPlantilla.Fields textField,
            ArrayList arrColumnasWhere, ArrayList arrValoresWhere);

        void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, entClaPlantilla.Fields textField,
            ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);

        void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, entClaPlantilla.Fields textField,
            entClaPlantilla.Fields searchField, object searchValue);

        void CargarComboBox(ref ComboBox cmb, entClaPlantilla.Fields valueField, entClaPlantilla.Fields textField,
            entClaPlantilla.Fields searchField, object searchValue, string strParamAdicionales);

        void CargarDataGrid(ref DataGridView dtg);
        void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
        void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
        void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
        void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);

        void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere,
            String condicionesWhere);

        void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere);

        void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParametrosAdicionales);

        void CargarDataGrid(ref DataGridView dtg, entClaPlantilla.Fields searchField, object searchValue);

        void CargarDataGrid(ref DataGridView dtg, entClaPlantilla.Fields searchField, object searchValue,
            string strParametrosAdicionales);

        void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere);

        void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere,
            ArrayList arrValoresWhere, string strParametrosAdicionales);

        void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entClaPlantilla.Fields searchField,
            object searchValue);

        void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, entClaPlantilla.Fields searchField,
            object searchValue, string strParametrosAdicionales);

        object FuncionesCount(entClaPlantilla.Fields refField);
        object FuncionesCount(entClaPlantilla.Fields refField, entClaPlantilla.Fields whereField, object valueField);
        object FuncionesCount(entClaPlantilla.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        object FuncionesMin(entClaPlantilla.Fields refField);
        object FuncionesMin(entClaPlantilla.Fields refField, entClaPlantilla.Fields whereField, object valueField);
        object FuncionesMin(entClaPlantilla.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        object FuncionesMax(entClaPlantilla.Fields refField);
        object FuncionesMax(entClaPlantilla.Fields refField, entClaPlantilla.Fields whereField, object valueField);
        object FuncionesMax(entClaPlantilla.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        object FuncionesSum(entClaPlantilla.Fields refField);
        object FuncionesSum(entClaPlantilla.Fields refField, entClaPlantilla.Fields whereField, object valueField);
        object FuncionesSum(entClaPlantilla.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
        object FuncionesAvg(entClaPlantilla.Fields refField);
        object FuncionesAvg(entClaPlantilla.Fields refField, entClaPlantilla.Fields whereField, object valueField);
        object FuncionesAvg(entClaPlantilla.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
    }
}